SOURCES += \
    $$PWD/abstractcontroller.cpp \
    $$PWD/maincontroller.cpp \
    $$PWD/optionscontroller.cpp \
    $$PWD/module/zoomable.cpp \
    $$PWD/tabcontroller.cpp \
    $$PWD/propertycontroller.cpp \
    $$PWD/undocontroller.cpp \
    $$PWD/soundsetcontroller.cpp \
    $$PWD/mainpropertycontroller.cpp \
    $$PWD/miniscenecontroller.cpp \
    $$PWD/layerscontroller.cpp

HEADERS  += \
    $$PWD/abstractcontroller.h \
    $$PWD/maincontroller.h \
    $$PWD/optionscontroller.h \
    $$PWD/module/zoomable.h \
    $$PWD/tabcontroller.h \
    $$PWD/propertycontroller.h \
    $$PWD/undocontroller.h \
    $$PWD/soundsetcontroller.h \
    $$PWD/mainpropertycontroller.h \
    $$PWD/miniscenecontroller.h \
    $$PWD/layerscontroller.h
