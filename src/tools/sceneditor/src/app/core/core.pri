QT += core widgets multimedia

SOURCES += \
    $$PWD/core.cpp \
    $$PWD/document/document.cpp \
    $$PWD/document/base/cachedpixmap.cpp \
    $$PWD/document/base/docutil.cpp \
    $$PWD/document/base/filesystemwatcher.cpp \
    $$PWD/document/base/pixmap.cpp \
    $$PWD/document/base/propertysheet.cpp \
    $$PWD/document/base/propertyitem.cpp \
    $$PWD/document/base/propertyundocommand.cpp \
    $$PWD/document/model/sound/soundset.cpp \
    $$PWD/document/base/cachedsound.cpp \
    $$PWD/document/model/sound/sounditem.cpp \
    $$PWD/document/base/sound.cpp \
    $$PWD/document/model/sound/soundsetundocommand.cpp \
    $$PWD/document/model/sound/sounditemsmodel.cpp \
    $$PWD/document/model/layersmodel.cpp \
    $$PWD/document/model/layer/layer.cpp \
    $$PWD/document/model/layer/imagesprite.cpp \
    $$PWD/document/undocommand/layersundocommand.cpp \
    $$PWD/document/base/propertyobject.cpp \
    $$PWD/document/graphicsscene.cpp \
    $$PWD/document/graphicsitem/mousetracegraphicsitem.cpp \
    $$PWD/document/scenemodel.cpp

HEADERS  += \
    $$PWD/core.h \
    $$PWD/document/document.h \
    $$PWD/document/base/cachedpixmap.h \
    $$PWD/document/base/docutil.h \
    $$PWD/document/base/filesystemwatcher.h \
    $$PWD/document/base/pixmap.h \
    $$PWD/document/base/property.h \
    $$PWD/document/base/propertysheet.h \
    $$PWD/document/base/propertyitem.h \
    $$PWD/document/base/propertyundocommand.h \
    $$PWD/document/model/sound/soundset.h \
    $$PWD/document/base/cachedsound.h \
    $$PWD/document/model/sound/sounditem.h \
    $$PWD/document/base/sound.h \
    $$PWD/document/model/sound/soundsetundocommand.h \
    $$PWD/document/model/sound/sounditemsmodel.h \
    $$PWD/document/model/layersmodel.h \
    $$PWD/document/model/layer/layer.h \
    $$PWD/document/model/layer/imagesprite.h \
    $$PWD/document/undocommand/layersundocommand.h \
    $$PWD/document/base/propertyobject.h \
    $$PWD/document/graphicsscene.h \
    $$PWD/document/graphicsitem/mousetracegraphicsitem.h \
    $$PWD/document/scenemodel.h

