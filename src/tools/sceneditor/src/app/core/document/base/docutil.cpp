#include "docutil.h"

namespace DocUtil {

QRectF toRectF(const TRect &rect)
{
    return QRectF(rect.x, rect.y, rect.w, rect.h);
}

TRect toRect(const QRectF &rect)
{
    return TRect(rect.x(), rect.y(), rect.width(), rect.height());
}

QPointF toPointF(TVector2 vector)
{
    return QPointF(vector.x, vector.y);
}

TVector2 toVector2(const QPointF &pt)
{
    return TVector2(pt.x(), pt.y());
}

QList<QRectF> toRectList(const TRectList &rectList)
{
    QList<QRectF> rl;
    for(TRect rect : rectList)
    {
        rl.append(toRectF(rect));
    }
    return rl;
}

TRectList toRectList(const QList<QRectF> &rectList)
{
    TRectList rl;
    for(QRectF rect : rectList)
    {
        rl.emplace_back(toRect(rect));
    }
    return rl;
}

QJsonArray toJson(const QPointF &pt)
{
    QJsonArray array;
    array.append(pt.x());
    array.append(pt.y());
    return array;
}

IMPLEMENT_TYPE_TO_QSTRING(ModelType)
IMPLEMENT_TYPE_TO_QSTRING(GameEvent)
IMPLEMENT_TYPE_TO_QSTRING(CharacterGender)
IMPLEMENT_TYPE_TO_QSTRING(SpriteType)
IMPLEMENT_TYPE_TO_QSTRING(SpriteState)
IMPLEMENT_TYPE_TO_QSTRING(SpriteDirection)
IMPLEMENT_TYPE_TO_QSTRING(ActionMode)

}
