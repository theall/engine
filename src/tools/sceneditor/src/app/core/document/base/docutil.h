#ifndef DOCUTIL_H
#define DOCUTIL_H

#include <QPoint>
#include <QPointF>
#include <QString>
#include <QRectF>
#include <QList>
#include <QJsonArray>

#include "../../../../../../../shared/gameutils/base/rect.h"
#include "../../../../../../../shared/gameutils/base/variable.h"

#define DEFINE_TYPE_TO_QSTRING(_type) \
    QString _type##ToQString(int ge)

#define IMPLEMENT_TYPE_TO_QSTRING(_type) \
    QString _type##ToQString(int ge) \
    {\
        return QString::fromStdString(_type##ToString((_type)ge));\
    }

namespace DocUtil {
    QJsonArray toJson(const QPointF &pt);
    QPointF toPointF(TVector2 vector);
    TVector2 toVector2(const QPointF &pt);
    QList<QRectF> toRectList(const TRectList &rectList);
    TRectList toRectList(const QList<QRectF> &rectList);
    QRectF toRectF(const TRect &rect);
    TRect toRect(const QRectF &rect);

    DEFINE_TYPE_TO_QSTRING(ModelType);
    DEFINE_TYPE_TO_QSTRING(GameEvent);
    DEFINE_TYPE_TO_QSTRING(CharacterGender);
    DEFINE_TYPE_TO_QSTRING(SpriteType);
    DEFINE_TYPE_TO_QSTRING(SpriteState);
    DEFINE_TYPE_TO_QSTRING(SpriteDirection);
    DEFINE_TYPE_TO_QSTRING(ActionMode);
}

#endif // DOCUTIL_H
