#ifndef SCENEMODEL_H
#define SCENEMODEL_H

#include "graphicsscene.h"
#include "model/layersmodel.h"

#include <gameutils/model/scene.h>

class TSceneModel : public TPropertyObject
{
    Q_OBJECT

public:
    TSceneModel(QObject *parent = nullptr);

    TLayersModel *layersModel() const;
    void loadFromModel(const Model::TScene &sceneModel, void *context);

    TGraphicsScene *graphicsScene() const;

private slots:
    void slotColorChanged(const QVariant &oldValue, const QVariant &newValue);

private:
    TLayersModel *mLayersModel;
    TGraphicsScene *mGraphicsScene;
    TPropertyItem *mColorPropertyItem;

    void initPropertySheet();
};

#endif // SCENEMODEL_H
