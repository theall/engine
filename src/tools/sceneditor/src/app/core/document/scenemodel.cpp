#include "scenemodel.h"

#include <gameutils/model/scene.h>

static const char *P_COLOR = "Color";
static const char *P_CAMERA = "Camera";
static const char *P_FPS = "Fps";
static const char *P_GRAVITY = "Gravity";

TSceneModel::TSceneModel(QObject *parent) :
    TPropertyObject(parent)
  , mLayersModel(new TLayersModel(this))
  , mGraphicsScene(new TGraphicsScene(this))
{
    initPropertySheet();

    mLayersModel->addLayer(tr("Default"));
}

TLayersModel *TSceneModel::layersModel() const
{
    return mLayersModel;
}

void TSceneModel::loadFromModel(const Model::TScene &sceneModel, void *context)
{
    Q_UNUSED(context);

    mLayersModel->clear();
    for(Model::TLayer *layerModel : sceneModel.layerList())
    {
        TLayer *layer = new TLayer(QString(), this);
        layer->loadFromModel(*layerModel, this);
        mLayersModel->addLayer(layer);
    }

    TColor c = sceneModel.backgroundColor();
    mColorPropertyItem->setValue(QColor(c.r,c.g,c.b,c.a));
}

void TSceneModel::slotColorChanged(const QVariant &oldValue, const QVariant &newValue)
{
    Q_UNUSED(oldValue);
    mGraphicsScene->setBackgroundColor(newValue.value<QColor>());
}

TGraphicsScene *TSceneModel::graphicsScene() const
{
    return mGraphicsScene;
}

void TSceneModel::initPropertySheet()
{
    mColorPropertyItem = mPropertySheet->addProperty(PT_COLOR, P_COLOR, PID_SCENE_COLOR);

    connect(mColorPropertyItem,
            SIGNAL(valueChanged(QVariant,QVariant)),
            this,
            SLOT(slotColorChanged(QVariant,QVariant)));
}
