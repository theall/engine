#include "layer.h"
#include "../../document.h"
#include "../../base/docutil.h"

static const char *P_NAME = "Name";
static const char *P_OFFSET = "Offset";

TLayer::TLayer(const QString &name=QString(), QObject *parent) :
    TPropertyObject(parent)
  , mName(name)
  , mDocument(nullptr)
{
    setObjectName("Layer");

    QObject *obj = parent;
    while (obj) {
        mDocument = qobject_cast<TDocument*>(obj);
        if(mDocument)
            break;
        obj = obj->parent();
    }
    if(!mDocument)
        throw QString("File:%1, Line:%2: Parent must be document.").arg(__FILE__).arg(__LINE__);

    initPropertySheet();
}

TLayer::~TLayer()
{

}

Model::TLayer *TLayer::toModel() const
{
    Model::TLayer *layer = new Model::TLayer;

    return layer;
}

void TLayer::loadFromModel(const Model::TLayer &layerModel, void *context)
{
    TDocument *document = static_cast<TDocument*>(context);
    if(!document)
        return;

}

QString TLayer::name() const
{
    return (*mPropertySheet)[P_NAME]->value().toString();
}

void TLayer::setName(const QString &name)
{
    (*mPropertySheet)[P_NAME]->setNewValue(name);
}

QPointF TLayer::offset() const
{
    return (*mPropertySheet)[P_OFFSET]->value().toPointF();
}

void TLayer::setOffset(const QPointF &offset)
{
    (*mPropertySheet)[P_OFFSET]->setNewValue(offset);
}

void TLayer::slotPropertyItemValueChanged(TPropertyItem *item, const QVariant &value)
{
    if(!item)
        return;

    PropertyID command = item->propertyId();
    switch (command) {
    case PID_ACTION_NAME:
        emit nameChanged(value.toString());
        break;
    default:
        break;
    }
}

TPropertySheet *TLayer::propertySheet() const
{
    return mPropertySheet;
}

void TLayer::initPropertySheet()
{
    connect(mPropertySheet,
            SIGNAL(propertyItemValueChanged(TPropertyItem*,QVariant)),
            this,
            SLOT(slotPropertyItemValueChanged(TPropertyItem*,QVariant)));

    TPropertyItem *item;
    item = mPropertySheet->addProperty(PT_STRING, P_NAME, PID_ACTION_NAME, mName);
    item = mPropertySheet->addProperty(PT_VECTOR, P_OFFSET, PID_ACTION_VECTOR);

    Q_UNUSED(item);
}
