#include "soundsetundocommand.h"
#include "sounditemsmodel.h"
#include "sounditem.h"

#include <QCoreApplication>

#define tr(x) QCoreApplication::translate("UndoCommand", x)

const QString g_commandText[SUC_COUNT] = {
    tr("Add %1 sound item(s)"),
    tr("Remove %1 sound item(s)")
};

TSoundSetUndoCommand::TSoundSetUndoCommand(
        SoundSetUndoCommand command,
        TSoundItemsModel *soundItemsModel,
        TSoundItem *soundItem,
        QUndoCommand *parent) :
    QUndoCommand(parent)
  , mSoundItemsModel(soundItemsModel)
  , mCommand(command)
{
    mSoundItemList.append(soundItem);
    setText(g_commandText[command].arg(1));
}

TSoundSetUndoCommand::TSoundSetUndoCommand(
        SoundSetUndoCommand command,
        TSoundItemsModel *soundItemsModel,
        const QList<TSoundItem *> &soundItemList,
        QUndoCommand *parent) :
    QUndoCommand(parent)
  , mSoundItemsModel(soundItemsModel)
  , mCommand(command)
  , mSoundItemList(soundItemList)
{
    setText(g_commandText[command].arg(mSoundItemList.size()));
}

TSoundSetUndoCommand::~TSoundSetUndoCommand()
{

}

void TSoundSetUndoCommand::undo()
{
    if(mCommand==SUC_ADD)
    {
        mIndexList = mSoundItemsModel->removeSoundItemList(mSoundItemList);
    } else if(mCommand==SUC_REMOVE) {
        mSoundItemsModel->addSoundItemList(mSoundItemList, mIndexList);
    }
}

void TSoundSetUndoCommand::redo()
{
    if(mCommand==SUC_ADD)
    {
        mSoundItemsModel->addSoundItemList(mSoundItemList, mIndexList);
    } else if(mCommand==SUC_REMOVE) {
        mIndexList = mSoundItemsModel->removeSoundItemList(mSoundItemList);
    }
}
