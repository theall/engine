#ifndef TSOUNDITEM_H
#define TSOUNDITEM_H

#include <QString>

#include "../../base/sound.h"
#include "../../base/propertyobject.h"

namespace Model {
class TSoundItem;
}

class TSoundItem : public TPropertyObject
{
    Q_OBJECT

public:
    TSoundItem(QObject *parent = nullptr);
    virtual ~TSoundItem();

    void loadFromModel(const Model::TSoundItem &soundItemModel, void *context = nullptr);
    Model::TSoundItem *toModel() const;

    int loopCount() const;
    void setLoopCount(int loopCount);

    int delay() const;
    void setDelay(int delay);

    TSound *source() const;
    void setSource(TSound *source);
    QString sourceFileName() const;
    void setSource(const QString &fileName);

signals:
    void propertyChanged(PropertyID id, const QVariant &value);

private slots:
    void slotPropertyItemValueChanged(TPropertyItem *item, const QVariant &value);

private:
    TDocument *mDocument;
    TSound *mSoundSource;
};
typedef QList<TSoundItem *> TSoundItemList;

QDataStream &operator<<(QDataStream& out, const TSoundItem& soundItem);
QDataStream &operator>>(QDataStream& in, TSoundItem& soundItem);

#endif // TSOUNDITEM_H
