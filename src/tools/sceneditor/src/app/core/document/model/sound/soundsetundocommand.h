#ifndef SOUNDSETUNDOCOMMAND_H
#define SOUNDSETUNDOCOMMAND_H

#include <QUndoCommand>

enum SoundSetUndoCommand
{
    SUC_ADD = 0,
    SUC_REMOVE,
    SUC_COUNT
};

class TSoundItemsModel;
class TSoundItem;

class TSoundSetUndoCommand : public QUndoCommand
{
public:
    TSoundSetUndoCommand(SoundSetUndoCommand command,
                         TSoundItemsModel *soundItemsModel,
                         TSoundItem *soundItem,
                         QUndoCommand *parent = Q_NULLPTR);
    TSoundSetUndoCommand(SoundSetUndoCommand command,
                         TSoundItemsModel *soundItemsModel,
                         const QList<TSoundItem *> &soundItemList,
                         QUndoCommand *parent = Q_NULLPTR);
    ~TSoundSetUndoCommand();

    void undo() Q_DECL_OVERRIDE;
    void redo() Q_DECL_OVERRIDE;

private:
    QList<int> mIndexList;
    TSoundItemsModel *mSoundItemsModel;
    SoundSetUndoCommand mCommand;
    QList<TSoundItem *> mSoundItemList;
};

#endif // SOUNDSETUNDOCOMMAND_H
