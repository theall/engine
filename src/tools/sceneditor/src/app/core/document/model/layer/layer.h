#ifndef DOCUMENT_LAYER_H
#define DOCUMENT_LAYER_H

#include <QPoint>

#include "../../base/propertyobject.h"
#include "../../../../../../../../shared/gameutils/model/scene/layer.h"

class TDocument;

class TLayer : public TPropertyObject
{
    Q_OBJECT

public:
    TLayer(const QString &name, QObject *parent=nullptr);
    ~TLayer();

    Model::TLayer *toModel() const;
    void loadFromModel(const Model::TLayer &layerModel, void *context=nullptr);

    QString name() const;
    void setName(const QString &name);

    QPointF offset() const;
    void setOffset(const QPointF &offset);

    TPropertySheet *propertySheet() const;

signals:
    void nameChanged(const QString &newName);

private slots:
    void slotPropertyItemValueChanged(TPropertyItem *item, const QVariant &value);

private:
    QString mName;
    QPoint mOffset;
    TDocument *mDocument;

    void initPropertySheet();
};


#endif // DOCUMENT_LAYER_H
