#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>

class TGraphicsView : public QGraphicsView
{
public:
    TGraphicsView(QWidget *parent = Q_NULLPTR);
    ~TGraphicsView();

private:
};

#endif // GRAPHICSVIEW_H
