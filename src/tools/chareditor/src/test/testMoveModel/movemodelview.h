#ifndef MOVEMODELVIEW_H
#define MOVEMODELVIEW_H

#include <QGraphicsView>

#include "movemodelgraphicsitem.h"

class TMoveModelScene : public QGraphicsScene
{
public:
    TMoveModelScene(QObject *parent = nullptr);
    ~TMoveModelScene();

    void setSize(int w, int h);
    void moveItem(QGraphicsItem *item, const QPointF &pos);
    void moveItem(QGraphicsItem *item, int x, int y);
    void calcOrbit();
    void play();
    void stop();

    int gravity() const;
    void setGravity(int gravity);

private:
    int mGravity;
    QPoint mOrginPos;
    QGraphicsItem *mMousePosItem;
    TMoveModelKeyFrameItemList mKeyFrameList;
    QMap<TMoveModelKeyFrameItem*, TMoveModelReferenceFrameItemList> mFrameMap;
    QGraphicsPixmapItem *mPixmapItem;
    TMoveModelFrameItemList mAbstractFrameList;
    int mCurrentPlayIndex;
    int mTimerId;

    void addMousePosItem();
    void addFramePixmapItem();

    // QGraphicsScene interface
protected:
    void drawBackground(QPainter *painter, const QRectF &rect) Q_DECL_OVERRIDE;
    void drawForeground(QPainter *painter, const QRectF &rect) Q_DECL_OVERRIDE;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

    // QObject interface
protected:
    void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;
};

class TMoveModelView : public QGraphicsView
{
public:
    TMoveModelView(QWidget *parent = nullptr);
    TMoveModelView(TMoveModelScene *scene, QWidget *parent = nullptr);
    ~TMoveModelView();

private:
    void locatePosition(const QPoint &pos);

    // QWidget interface
protected:
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
};

#endif // MOVEMODELVIEW_H
