#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "movemodelview.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    TMoveModelView *view = new TMoveModelView(this);
    setCentralWidget(view);

    showMaximized();
}

MainWindow::~MainWindow()
{
    delete ui;
}
