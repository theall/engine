#include "movemodelgraphicsitem.h"

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>

TMoveModelFrameItem::TMoveModelFrameItem(const QColor &fillColor, QGraphicsItem *parent) :
    QGraphicsEllipseItem(parent)
  , mFillColor(fillColor)
{
    setRect(0, 0, 5, 5);
    setBrush(QBrush(fillColor));
    //setFlags(QGraphicsItem::ItemIsMovable|QGraphicsItem::ItemIsSelectable);
}

TMoveModelFrameItem::~TMoveModelFrameItem()
{

}

bool TMoveModelFrameItem::isLocked() const
{
    return mIsLocked;
}

void TMoveModelFrameItem::setLocked(bool isLocked)
{
    mIsLocked = isLocked;
}

TMoveModelKeyFrameItem::TMoveModelKeyFrameItem(QGraphicsItem *parent) :
    TMoveModelFrameItem(QColor(Qt::red), parent)
  , mAntiGravity(true)
  , mVector(QPointF(0.0,0.0))
  , mDuration(1)
{

}

TMoveModelKeyFrameItem::~TMoveModelKeyFrameItem()
{

}

QPointF TMoveModelKeyFrameItem::vector() const
{
    return mVector;
}

void TMoveModelKeyFrameItem::setVector(const QPointF &vector)
{
    mVector = vector;
}

int TMoveModelKeyFrameItem::duration() const
{
    return mDuration;
}

void TMoveModelKeyFrameItem::setDuration(int duration)
{
    mDuration = duration;
}

bool TMoveModelKeyFrameItem::antiGravity() const
{
    return mAntiGravity;
}

void TMoveModelKeyFrameItem::setAntiGravity(bool antiGravity)
{
    mAntiGravity = antiGravity;
}

QPixmap TMoveModelKeyFrameItem::pixmap() const
{
    return mPixmap;
}

void TMoveModelKeyFrameItem::setPixmap(const QPixmap &pixmap)
{
    mPixmap = pixmap;
}

TMoveModelReferenceFrameItem::TMoveModelReferenceFrameItem(TMoveModelKeyFrameItem *keyFrameItem, QGraphicsItem *parent) :
    TMoveModelFrameItem(QColor(Qt::green), parent)
  , mKeyFrame(keyFrameItem)
{

}

TMoveModelReferenceFrameItem::~TMoveModelReferenceFrameItem()
{

}

TMoveModelKeyFrameItem *TMoveModelReferenceFrameItem::keyFrame() const
{
    return mKeyFrame;
}

void TMoveModelReferenceFrameItem::setKeyFrame(TMoveModelKeyFrameItem *keyFrame)
{
    mKeyFrame = keyFrame;
}

QPixmap TMoveModelReferenceFrameItem::pixmap() const
{
    if(mKeyFrame)
        return mKeyFrame->pixmap();

    return QPixmap();
}

TMousePosTraceItem::TMousePosTraceItem(QGraphicsItem *parent) :
    QGraphicsTextItem(parent)
{
    setDefaultTextColor(QColor(0,0,250));
}

TMousePosTraceItem::~TMousePosTraceItem()
{

}

void TMousePosTraceItem::setPos(const QPointF &pos, const QPointF &orginPos)
{
    qreal posX = pos.x();
    qreal posY = pos.y();
    setPlainText(QString("%1 %2").arg(posX-orginPos.x()).arg(orginPos.y()-posY));

    QPointF newPos = pos;
    newPos.setX(posX-boundingRect().width()/2);
    newPos.setY(posY-boundingRect().height()-2);
    QGraphicsTextItem::setPos(newPos);
}
