#ifndef MOVEMODELGRAPHICSITEM_H
#define MOVEMODELGRAPHICSITEM_H

#include <QGraphicsTextItem>
#include <QGraphicsEllipseItem>

class TMoveModelFrameItem : public QGraphicsEllipseItem
{
public:
    TMoveModelFrameItem(const QColor &fillColor, QGraphicsItem *parent = nullptr);
    ~TMoveModelFrameItem();

    bool isLocked() const;
    void setLocked(bool isLocked);
    virtual QPixmap pixmap() const = 0;

private:
    bool mIsLocked;
    QColor mFillColor;
};
typedef QList<TMoveModelFrameItem*> TMoveModelFrameItemList;

class TMoveModelKeyFrameItem : public TMoveModelFrameItem
{
public:
    TMoveModelKeyFrameItem(QGraphicsItem *parent = nullptr);
    ~TMoveModelKeyFrameItem();

    QPointF vector() const;
    void setVector(const QPointF &vector);

    int duration() const;
    void setDuration(int duration);

    bool antiGravity() const;
    void setAntiGravity(bool antiGravity);

    QPixmap pixmap() const;
    void setPixmap(const QPixmap &pixmap);

private:
    bool mAntiGravity;
    QPointF mVector;
    int mDuration;
    QPixmap mPixmap;
};

typedef QList<TMoveModelKeyFrameItem*> TMoveModelKeyFrameItemList;

class TMoveModelReferenceFrameItem : public TMoveModelFrameItem
{
public:
    TMoveModelReferenceFrameItem(TMoveModelKeyFrameItem *keyFrameItem, QGraphicsItem *parent = nullptr);
    ~TMoveModelReferenceFrameItem();

    TMoveModelKeyFrameItem *keyFrame() const;
    void setKeyFrame(TMoveModelKeyFrameItem *keyFrame);

    QPixmap pixmap() const;

private:
    TMoveModelKeyFrameItem *mKeyFrame;
};
typedef QList<TMoveModelReferenceFrameItem*> TMoveModelReferenceFrameItemList;

class TMousePosTraceItem : public QGraphicsTextItem
{
public:
    TMousePosTraceItem(QGraphicsItem *parent = nullptr);
    ~TMousePosTraceItem();

    void setPos(const QPointF &pos, const QPointF &orginPos);

};


#endif // MOVEMODELGRAPHICSITEM_H
