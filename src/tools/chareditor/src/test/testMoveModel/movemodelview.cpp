#include "movemodelview.h"

#include <QBitmap>
#include <QWheelEvent>
#include <QGraphicsSceneMouseEvent>

#define TILE_WIDTH 16
#define TILE_HEIGHT 16
#define CALIBRATION_LINE_WIDTH 2
#define CALIBRATION_NUMBER_WITH 48
#define CALIBRATION_NUMBER_FONT_SIZE 8
#define PADING_WIDTH 4

QPixmap loadPixmap(const QString &filePath)
{
    QPixmap pixmap(filePath);
    QBitmap mask = pixmap.createMaskFromColor(QColor(Qt::black));
    pixmap.setMask(mask);
    return pixmap;
}

TMoveModelScene::TMoveModelScene(QObject *parent) :
    QGraphicsScene(parent)
  , mGravity(3)
  , mTimerId(-1)
{
    setSize(800, 640);

    addMousePosItem();
    calcOrbit();
}

TMoveModelScene::~TMoveModelScene()
{
    mKeyFrameList.clear();
}

void TMoveModelScene::setSize(int w, int h)
{
    setSceneRect(0, 0, w, h);
    mOrginPos.setX(w/2);
    mOrginPos.setY(h / 2);
}

void TMoveModelScene::moveItem(QGraphicsItem *item, const QPointF &pos)
{
    moveItem(item, pos.x(), pos.y());
}

void TMoveModelScene::moveItem(QGraphicsItem *item, int x, int y)
{
    if(item)
    {
        int offsetX = item->boundingRect().width()/2;
        int offsetY = item->boundingRect().height()/2;
        item->setPos(x+mOrginPos.x()-offsetX,mOrginPos.y()-y-offsetY);
    }
}

void TMoveModelScene::calcOrbit()
{
    clear();
    addMousePosItem();
    addFramePixmapItem();

    mKeyFrameList.clear();
    mFrameMap.clear();

    // ryu
//    TMoveModelKeyFrameItem *item = new TMoveModelKeyFrameItem;
//    item->setVector(QPointF(.5,0));
//    item->setDuration(2);
//    item->setAntiGravity(true);
//    item->setPixmap(loadPixmap("z:/1/zuspecial1.bmp"));
//    mKeyFrameList.append(item);

//    item = new TMoveModelKeyFrameItem;
//    item->setVector(QPoint(.5,0));
//    item->setDuration(6);
//    item->setPixmap(loadPixmap("z:/1/zuspecial2.bmp"));
//    mKeyFrameList.append(item);

//    item = new TMoveModelKeyFrameItem;
//    item->setVector(QPoint(2,3));
//    item->setDuration(50);
//    item->setPixmap(loadPixmap("z:/1/zuspecial3.bmp"));
//    mKeyFrameList.append(item);

    // Jimmy uppercut
//    TMoveModelKeyFrameItem *item = new TMoveModelKeyFrameItem;
//    item->setVector(QPointF(0,0));
//    item->setDuration(1);
//    item->setPixmap(loadPixmap("z:/jimmy/zuspecial1.bmp"));
//    mKeyFrameList.append(item);

//    item = new TMoveModelKeyFrameItem;
//    item->setVector(QPoint(0,0));
//    item->setDuration(1);
//    item->setPixmap(loadPixmap("z:/jimmy/zuspecial2.bmp"));
//    mKeyFrameList.append(item);

//    item = new TMoveModelKeyFrameItem;
//    item->setVector(QPoint(3,0));
//    item->setDuration(10);
//    item->setPixmap(loadPixmap("z:/jimmy/zuspecial3.bmp"));
//    mKeyFrameList.append(item);

//    item = new TMoveModelKeyFrameItem;
//    item->setVector(QPoint(-3,0));
//    item->setDuration(2);
//    item->setPixmap(loadPixmap("z:/jimmy/zuspecial4.bmp"));
//    mKeyFrameList.append(item);

//    item = new TMoveModelKeyFrameItem;
//    item->setVector(QPoint(1,4));
//    item->setDuration(11);
//    item->setPixmap(loadPixmap("z:/jimmy/zuspecial5.bmp"));
//    mKeyFrameList.append(item);

//    item = new TMoveModelKeyFrameItem;
//    item->setVector(QPoint(0,-1));
//    item->setDuration(5);
//    item->setPixmap(loadPixmap("z:/jimmy/zuspecial6.bmp"));
//    mKeyFrameList.append(item);

//    item = new TMoveModelKeyFrameItem;
//    item->setVector(QPoint(0,-1));
//    item->setDuration(5);
//    item->setPixmap(loadPixmap("z:/jimmy/zuspecial7.bmp"));
//    mKeyFrameList.append(item);

//    item = new TMoveModelKeyFrameItem;
//    item->setVector(QPoint(0,-1));
//    item->setDuration(5);
//    item->setPixmap(loadPixmap("z:/jimmy/zuspecial8.bmp"));
//    mKeyFrameList.append(item);

    // Jimmy rocket kick
    TMoveModelKeyFrameItem *item = new TMoveModelKeyFrameItem;
    item->setVector(QPointF(0,0));
    item->setDuration(9);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial1.bmp"));
    mKeyFrameList.append(item);

    item = new TMoveModelKeyFrameItem;
    item->setVector(QPoint(0,1));
    item->setDuration(2);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial2.bmp"));
    mKeyFrameList.append(item);

    item = new TMoveModelKeyFrameItem;
    item->setVector(QPoint(3,3));
    item->setDuration(2);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial3.bmp"));
    mKeyFrameList.append(item);

    item = new TMoveModelKeyFrameItem;
    item->setVector(QPoint(1,1));
    item->setDuration(2);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial4.bmp"));
    mKeyFrameList.append(item);

    item = new TMoveModelKeyFrameItem;
    item->setVector(QPoint(4,-5));
    item->setDuration(2);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial5.bmp"));
    mKeyFrameList.append(item);

    item = new TMoveModelKeyFrameItem;
    item->setDuration(3);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial6.bmp"));
    mKeyFrameList.append(item);

    item = new TMoveModelKeyFrameItem;
    item->setDuration(3);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial7.bmp"));
    mKeyFrameList.append(item);

    item = new TMoveModelKeyFrameItem;
    item->setDuration(3);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial8.bmp"));
    mKeyFrameList.append(item);

    item = new TMoveModelKeyFrameItem;
    item->setDuration(3);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial9.bmp"));
    mKeyFrameList.append(item);

    item = new TMoveModelKeyFrameItem;
    item->setVector(QPoint(-1,-4));
    item->setDuration(3);
    item->setPixmap(loadPixmap("z:/jimmy/zdspecial10.bmp"));
    mKeyFrameList.append(item);

    QPointF orginPos(0,0);
    QPointF vector;
    for(TMoveModelKeyFrameItem *item : mKeyFrameList)
    {
        vector += item->vector();
        addItem(item);
        moveItem(item, orginPos);
        TMoveModelReferenceFrameItemList refFrameList;
        for(int i=0;i<item->duration();i++)
        {
            TMoveModelReferenceFrameItem *refItem = new TMoveModelReferenceFrameItem(item);
            addItem(refItem);
            orginPos += vector;
            if(!item->antiGravity())
            {
                vector.ry() -= mGravity*i;
            }
            moveItem(refItem, orginPos);
            refFrameList.append(refItem);
        }
        mFrameMap.insert(item, refFrameList);
    }
}

void TMoveModelScene::play()
{
    if(mTimerId != -1)
        killTimer(mTimerId);

    mAbstractFrameList.clear();
    for(TMoveModelKeyFrameItem *item : mKeyFrameList)
    {
        mAbstractFrameList.append(item);
        for(TMoveModelReferenceFrameItem *refItem : mFrameMap[item])
        {
            mAbstractFrameList.append(refItem);
        }
    }

    mCurrentPlayIndex = 0;
    mTimerId = startTimer(1000.0/30);
}

void TMoveModelScene::stop()
{
    if(mTimerId != -1)
    {
        killTimer(mTimerId);
        mTimerId = -1;
    }
}

int TMoveModelScene::gravity() const
{
    return mGravity;
}

void TMoveModelScene::setGravity(int gravity)
{
    mGravity = gravity;
}

void TMoveModelScene::addMousePosItem()
{
    mMousePosItem = (QGraphicsItem*)new TMousePosTraceItem;
    addItem(mMousePosItem);
}

void TMoveModelScene::addFramePixmapItem()
{
    mPixmapItem = new QGraphicsPixmapItem;
    addItem(mPixmapItem);
    mPixmapItem->setZValue(999);
}

void TMoveModelScene::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);

    int sceneWidth = width();
    int sceneHeight = height();
    int startX = mOrginPos.x();
    int startY = mOrginPos.y();
    QColor gridColor(0, 0, 0, 128);

    // Draw axis x
    QPen penAxisX(gridColor);
    QVector<qreal> _x;
    _x.append(2.0);
    _x.append(2.0);
    penAxisX.setDashPattern(_x);
    penAxisX.setCosmetic(true);
    painter->setPen(penAxisX);
    painter->drawLine(0, startY, sceneWidth, startY);

    // Draw axis y
    QPen penAxisY(gridColor);
    penAxisY.setCosmetic(false);
    penAxisY.setWidth(1);
    painter->setPen(penAxisY);
    painter->drawLine(startX, 0, startX, sceneHeight);

    QFont ft = painter->font();
    ft.setPixelSize(CALIBRATION_NUMBER_FONT_SIZE);
    painter->setFont(ft);

    penAxisY.setWidthF(.5);
    painter->setPen(penAxisY);
    // Draw positive calibration line
    for(int y=startY;y>=0;y-=TILE_HEIGHT)
    {
        painter->drawText(startX-CALIBRATION_NUMBER_WITH,
                          y-TILE_HEIGHT/2,
                          CALIBRATION_NUMBER_WITH-PADING_WIDTH,
                          TILE_HEIGHT,
                          Qt::AlignRight|Qt::AlignVCenter,
                          QString::number(startY-y));
        painter->drawLine(startX, y, startX+CALIBRATION_LINE_WIDTH, y);
    }

    // Draw negative calibration line
    for(int y=startY+TILE_HEIGHT;y<=sceneHeight;y+=TILE_HEIGHT)
    {
        painter->drawText(startX-CALIBRATION_NUMBER_WITH,
                          y-TILE_HEIGHT/2,
                          CALIBRATION_NUMBER_WITH-PADING_WIDTH,
                          TILE_HEIGHT,
                          Qt::AlignRight|Qt::AlignVCenter,
                          QString::number(startY-y));
        painter->drawLine(startX, y, startX+CALIBRATION_LINE_WIDTH, y);
    }
}

void TMoveModelScene::drawForeground(QPainter *painter, const QRectF &rect)
{

}

void TMoveModelScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(event->button()==Qt::RightButton)
    {
        if(mTimerId==-1)
            play();
        else
            stop();
    }
}

void TMoveModelScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    TMousePosTraceItem *item = (TMousePosTraceItem*)mMousePosItem;
    if(event->modifiers()&Qt::ControlModifier)
    {
        mMousePosItem->setVisible(true);
    } else {
        mMousePosItem->setVisible(false);
    }
    item->setPos(event->scenePos(), mOrginPos);
}

void TMoveModelScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{

}

void TMoveModelScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{

}

void TMoveModelScene::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers()&Qt::ControlModifier)
        mMousePosItem->setVisible(true);
}

void TMoveModelScene::keyReleaseEvent(QKeyEvent *event)
{
    mMousePosItem->setVisible(false);
}

void TMoveModelScene::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);

    if(mCurrentPlayIndex < 0)
        mCurrentPlayIndex = 0;

    if(mCurrentPlayIndex >= mAbstractFrameList.size())
        mCurrentPlayIndex = 0;

    TMoveModelFrameItem *item = mAbstractFrameList.at(mCurrentPlayIndex);
    if(item && mPixmapItem)
    {
        QPixmap p = item->pixmap();
        if(!p.isNull())
        {
            mPixmapItem->setPixmap(p);
            QPointF pos = item->pos();
            pos.rx() -= (p.width()-item->rect().width())/2;
            pos.ry() -= p.height();
            mPixmapItem->setPos(pos);
        }
    }

    mCurrentPlayIndex++;
}

TMoveModelView::TMoveModelView(QWidget *parent) :
    QGraphicsView(parent)
{
    setScene(new TMoveModelScene(this));
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
}

TMoveModelView::TMoveModelView(TMoveModelScene *scene, QWidget *parent) :
    QGraphicsView(scene, parent)
{
    setScene(scene);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
}

TMoveModelView::~TMoveModelView()
{

}

void TMoveModelView::locatePosition(const QPoint &pos)
{
    // Place the last known mouse scene pos below the mouse again
    QWidget *viewPortWidget = viewport();
    QPointF viewCenterScenePos = mapToScene(viewPortWidget->rect().center());
    QPointF mouseScenePos = mapToScene(viewPortWidget->mapFromGlobal(pos));
    QPointF diff = viewCenterScenePos - mouseScenePos;
    //centerOn(mLastMouseScenePos + diff);
}

void TMoveModelView::wheelEvent(QWheelEvent *event)
{
    int delta = event->angleDelta().y();
    if (event->modifiers()&Qt::ControlModifier and delta!=0)
    {
        if(delta > 0)
            scale(1.5, 1.5);
        else
            scale(0.5, 0.5);
        return;
    }
    QGraphicsView::wheelEvent(event);
}
