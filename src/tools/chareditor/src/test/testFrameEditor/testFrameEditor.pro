#-------------------------------------------------
#
# Project created by QtCreator 2016-11-22T09:48:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testFrameEditor
TEMPLATE = app

INCLUDEPATH += \
    ../../utils \
    ../../gui/component/frameview

SOURCES += main.cpp \
    ../../gui/component/frameview/frameview.cpp \
    ../../gui/component/frameview/framescene.cpp \
    ../../gui/component/frameview/frame/frameobject.cpp \
    ../../gui/component/frameview/frame/area/abstractarea.cpp \
    ../../gui/component/frameview/frame/area/collidearea.cpp \
    ../../gui/component/frameview/frame/area/undertakearea.cpp \
    ../../gui/component/frameview/frame/area/attackarea.cpp \
    ../../gui/component/frameview/frame/buckleobject.cpp \
    ../../gui/component/frameview/frame/clawobject.cpp \
    ../../gui/component/frameview/frame/handobject.cpp \
    ../../gui/component/frameview/graphicitems/base/arrowitem.cpp \
    ../../gui/component/frameview/graphicitems/areaitem.cpp

HEADERS  += \
    ../../gui/component/frameview/frameview.h \
    ../../gui/component/frameview/framescene.h \
    ../../gui/component/frameview/frame/frameobject.h \
    ../../gui/component/frameview/frame/area/abstractarea.h \
    ../../gui/component/frameview/frame/area/collidearea.h \
    ../../gui/component/frameview/frame/area/undertakearea.h \
    ../../gui/component/frameview/frame/area/attackarea.h \
    ../../gui/component/frameview/frame/buckleobject.h \
    ../../gui/component/frameview/frame/clawobject.h \
    ../../gui/component/frameview/frame/handobject.h \
    ../../gui/component/frameview/graphicitems/base/arrowitem.h \
    ../../gui/component/frameview/graphicitems/areaitem.h
