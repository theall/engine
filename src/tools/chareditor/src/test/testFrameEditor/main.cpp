#include <QApplication>
#include "frameview.h"
#include "framescene.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    TFrameScene *scene = new TFrameScene;
    TFrameView *view = new TFrameView;
    view->setScene(scene);

    return a.exec();
}
