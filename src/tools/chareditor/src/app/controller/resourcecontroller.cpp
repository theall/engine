#include "resourcecontroller.h"
#include "../core/base/cachedpixmap.h"
#include "../core/base/cachedsound.h"
#include "../gui/dialogs/pixmapresourcedialog.h"
#include "../gui/dialogs/soundresourcedialog.h"

TResourceController::TResourceController(QObject *parent) :
    TAbstractController(parent)
  , mPixmapResourceDialog(nullptr)
  , mSoundResourceDialog(nullptr)
{

}

TResourceController::~TResourceController()
{

}

bool TResourceController::joint(TMainWindow *mainWindow, TCore *core)
{
    Q_ASSERT(mainWindow);
    Q_ASSERT(core);

    mPixmapResourceDialog = mainWindow->getPixmapResourceDialog();
    mSoundResourceDialog = mainWindow->getSoundResourceDialog();

    return TAbstractController::joint(mainWindow, core);
}

void TResourceController::setCurrentDocument(TDocument *document)
{
    if(!document)
    {
        mPixmapResourceDialog->clear();
        mSoundResourceDialog->clear();
        return;
    }

    if(mDocument==document)
        return;

    if(mDocument) {
        mDocument->getCachedPixmaps()->disconnect(this);
        mDocument->getCachedSounds()->disconnect(this);
    }
    connect(document->getCachedPixmaps(), SIGNAL(updated()), this, SLOT(slotPixmapResourceUpdated()));
    connect(document->getCachedSounds(), SIGNAL(updated()), this, SLOT(slotSoundResourceUpdated()));
    setPixmapResourceSet(document->getCachedPixmaps());
    setSoundResourceSet(document->getCachedSounds());
}

void TResourceController::slotPixmapResourceUpdated()
{
    if(!mDocument)
        return;

    setPixmapResourceSet(mDocument->getCachedPixmaps());
}

void TResourceController::slotSoundResourceUpdated()
{
    if(!mDocument)
        return;

    setSoundResourceSet(mDocument->getCachedSounds());
}

void TResourceController::setPixmapResourceSet(TCachedPixmap *cachedPixmaps)
{
    if(!cachedPixmaps)
        return;

    TPixmapResourceSet resourceSet;
    resourceSet.setResourceRoot(cachedPixmaps->getPath());
    for(TPixmap *pixmap : cachedPixmaps->getPixmapList())
    {
        resourceSet.add(pixmap->thumbnail(), pixmap->fileName());
    }
    mPixmapResourceDialog->setResourceSet(resourceSet);
}

void TResourceController::setSoundResourceSet(TCachedSound *cachedSounds)
{
    if(!cachedSounds)
        return;

    TSoundResourceSet resourceSet;
    resourceSet.setResourceRoot(cachedSounds->getPath());
    for(TSound *sound: cachedSounds->getSoundList())
    {
        resourceSet.add(sound->mediaContent(), sound->fileName());
    }
    mSoundResourceDialog->setResourceSet(resourceSet);
}

void TResourceController::slotTimerEvent()
{

}
