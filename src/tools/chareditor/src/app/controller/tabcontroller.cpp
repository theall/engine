#include "tabcontroller.h"
#include "../core/base/pixmap.h"
#include "../core/base/filesystemwatcher.h"
#include "../core/document/model/action/movemodelscene.h"
#include "../core/document/model/action/framesmodel/frame/framescene/framescene.h"
#include "../gui/component/tabwidget/tabwidget.h"

#include <QMessageBox>

TTabController::TTabController(QObject *parent) :
    TAbstractController(parent)
  , mLastCheckTime(QDateTime::currentDateTime())
  , mLastListCount(0)
  , mLock(false)
{

}

TTabController::~TTabController()
{

}

bool TTabController::joint(TMainWindow *mainWindow, TCore *core)
{
    Q_ASSERT(mainWindow);
    Q_ASSERT(core);

    mTabWidget = mainWindow->tabWidget();
    Q_ASSERT(mTabWidget);

    connect(mTabWidget, SIGNAL(requestCloseDocument(void*)), this, SLOT(slotRequestCloseDocument(void*)));
    connect(mTabWidget, SIGNAL(requestSwitchToDocument(void*)), this, SLOT(slotRequestSwithToDocument(void*)));

    connect(core->fileWatcher(), SIGNAL(fileChanged(QString)), this, SLOT(slotDocumentFileChanged(QString)));

    return TAbstractController::joint(mainWindow, core);
}

void TTabController::setCurrentDocument(TDocument *document)
{
    if(!document || mDocument==document)
        return;

    int index = mTabWidget->findDocumentIndex(document);
    if(index == -1)
    {
        index = addDocument(document);
    } else if(index == mTabWidget->currentIndex()) {
        return;
    }
    mTabWidget->setCurrentIndex(index);
}

void TTabController::removeDocument(TDocument *document)
{
    if(!document)
        return;

    if(mTabWidget->removeTab((void*)document))
    {
        //document->disconnect(this);
    }
}

TDocument *TTabController::currentDocument()
{
    return (TDocument*)mTabWidget->currentDocument();
}

TFrameScene *TTabController::currentFrameScene() const
{
    if(mTabWidget->count() < 1)
        return nullptr;

    return static_cast<TFrameScene*>(mTabWidget->currentContainer()->frameScene());
}

TMoveModelScene *TTabController::currentMoveModelScene() const
{
    if(mTabWidget->count() < 1)
        return nullptr;

    return static_cast<TMoveModelScene*>(mTabWidget->currentContainer()->moveModelScene());
}

void TTabController::setMoveModelScene(TMoveModelScene *scene)
{
    if(mTabWidget->count() < 1)
        return;

    TMoveModelScene *oldScene = static_cast<TMoveModelScene*>(mTabWidget->currentContainer()->moveModelScene());
    if(oldScene && oldScene->isPlaying())
        oldScene->stop();

    mTabWidget->currentContainer()->setMoveModelScene(scene);
}

void TTabController::setFrameScene(TFrameScene *scene)
{
    if(mTabWidget->count() < 1)
        return;

    TFrameScene *oldScene = static_cast<TFrameScene*>(mTabWidget->currentContainer()->frameScene());
    if(oldScene==scene)
        return;

//    if(oldScene)
//        oldScene->setMode(TFrameScene::Normal);

    mTabWidget->currentContainer()->setFrameScene(scene);
    mMainWindow->triggerCurrentSelectedAction();

    if(scene && oldScene)
        scene->setMode(oldScene->getMode());
}

void TTabController::play()
{
    if(mTabWidget->count() < 1)
        return;

    TMoveModelScene *scene = static_cast<TMoveModelScene*>(mTabWidget->currentContainer()->moveModelScene());
    if(scene)
        scene->play();
}

void TTabController::stop()
{
    if(mTabWidget->count() < 1)
        return;

    TMoveModelScene *scene = static_cast<TMoveModelScene*>(mTabWidget->currentContainer()->moveModelScene());
    if(scene)
        scene->stop();
}

int TTabController::addDocument(TDocument *document)
{
    if(!document)
        return -1;

    int index = mTabWidget->addTab(document, document->projectName(), document->icon());
    mTabWidget->setTabToolTip(index, document->fileName());

    connect(document, SIGNAL(dirtyFlagChanged(bool)), this, SLOT(slotDocumentDirtyFlagChanged(bool)));
    connect(document, SIGNAL(iconChanged(TPixmap*)), this, SLOT(slotDocumentIconChanged(TPixmap*)));

    return index;
}

void TTabController::slotRequestSwithToDocument(void *document)
{
    emit requestSwitchToDocument((TDocument*)document);
}

void TTabController::slotRequestCloseDocument(void *document)
{
    emit requestCloseDocument((TDocument*)document);
}

void TTabController::slotDocumentDirtyFlagChanged(bool isDirty)
{
    TDocument *document = static_cast<TDocument*>(sender());
    if(!document)
        return;
    mTabWidget->setDocumentDirty(document, isDirty);
    emit documentDirtyFlagChanged(document, isDirty);
}

void TTabController::slotDocumentIconChanged(TPixmap *newPixmap)
{
    TDocument *document = static_cast<TDocument*>(sender());
    if(!document)
        return;

    QIcon icon;
    if(newPixmap) {
        icon = QIcon(newPixmap->thumbnail());
    }
    mTabWidget->setDocumentIcon(document, icon);
}

void TTabController::slotDocumentFileChanged(const QString &file)
{
    TDocument *document = mCore->find(file);
    if(!document)
        return;

    QFileInfo fileInfo(file);
    // Ignore file save by ourself
    if(document->lastSaveTime().secsTo(fileInfo.lastModified())<1)
        return;

    if(!mChangedDocuments.contains(document))
        mChangedDocuments.append(document);

    startMyTimer();
}

void TTabController::slotTimerEvent()
{
    if(mLock)
        return;

    QDateTime currentTime = QDateTime::currentDateTime();

    int listCount = mChangedDocuments.count();
    if(listCount < 1)
    {
        mLastCheckTime = currentTime;
        mLastListCount = 0;
        stopMyTimer();
        return;
    }
    if(listCount != mLastListCount)
    {
        mLastListCount = listCount;
        mLastCheckTime = currentTime;
        return;
    } else if(mLastCheckTime.msecsTo(currentTime) < 1000) {
        return;
    }

    mLock = true;
    bool reloadAll = false;
    TDocument *currentDocument = (TDocument*)mTabWidget->currentDocument();
    for(TDocument *d : mChangedDocuments)
    {
        QString fileName = d->fileName();
        if(QFile::exists(fileName))
        {
            if(!reloadAll)
            {
                int code = QMessageBox::question(
                            mMainWindow, tr("Question."),
                            tr("Project file is changed outside editor, do you want to reload it?"),
                            QMessageBox::YesToAll|QMessageBox::Yes|QMessageBox::No|QMessageBox::NoToAll);
                if(code == QMessageBox::NoToAll) {
                    mChangedDocuments.clear();
                    mLock = false;
                    return;
                } else if(code == QMessageBox::No) {
                    continue;
                } else if(code == QMessageBox::YesToAll) {
                    reloadAll = true;
                }
            }
            QString errorString;
            if(currentDocument == d)
            {
                int documentOldIndex = mTabWidget->findDocumentIndex(d);
                emit requestCloseDocument(d);
                try {
                    currentDocument = mCore->open(fileName);
                } catch(QString s) {
                    errorString = tr("Fail to open file %1:\n\n%2").arg(fileName).arg(s);
                } catch(...) {
                    errorString = tr("Unknown error!");
                }
                int documentNewIndex = addDocument(currentDocument);
                mTabWidget->moveTab(documentNewIndex, documentOldIndex);
                mTabWidget->setCurrentIndex(documentOldIndex);
            } else {
                try {
                    d->reload();
                } catch(QString s) {
                    errorString = s;
                } catch(...) {
                    errorString = tr("Unknown error!");
                }
            }
            if(!errorString.isEmpty())
            {
                QMessageBox::critical(mMainWindow, tr("Error"), errorString, QMessageBox::Ok);
                if(!d->isDirty())
                    d->setDirty(true);
            }
        } else {
            if(!d->isDirty())
                d->setDirty(true);
        }
    }
    mChangedDocuments.clear();
    emit requestSwitchToDocument(currentDocument);
    mLock = false;
}
