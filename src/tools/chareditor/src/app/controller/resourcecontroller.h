#ifndef RESOURCECONTROLLER_H
#define RESOURCECONTROLLER_H

#include "abstractcontroller.h"

class TResourceController : public TAbstractController
{
    Q_OBJECT

public:
    TResourceController(QObject *parent = 0);
    ~TResourceController();

    bool joint(TMainWindow *mainWindow, TCore *core) Q_DECL_OVERRIDE;
    void setCurrentDocument(TDocument *document) Q_DECL_OVERRIDE;

signals:

private slots:
    void slotPixmapResourceUpdated();
    void slotSoundResourceUpdated();

private:
    TPixmapResourceDialog *mPixmapResourceDialog;
    TSoundResourceDialog *mSoundResourceDialog;

    void setPixmapResourceSet(TCachedPixmap *cachedPixmaps);
    void setSoundResourceSet(TCachedSound *cachedSounds);

protected slots:
    void slotTimerEvent() Q_DECL_OVERRIDE;
};

#endif // RESOURCECONTROLLER_H
