#include "framescontroller.h"
#include "soundsetcontroller.h"
#include "vectortablecontroller.h"
#include "module/framedelegate.h"
#include "../gui/component/tabwidget/tabwidget.h"
#include "../gui/component/actionsdock/animationview.h"
#include "../gui/component/actionsdock/actionscontainer.h"
#include "../core/document/model/action/movemodelscene.h"
#include "../core/document/model/action/action.h"
#include "../core/document/model/action/framesmodel/framesmodel.h"
#include "../core/document/model/action/framesmodel/frame/frame.h"
#include "../core/document/model/action/framesmodel/frame/framescene/framescene.h"

#include <QMimeData>
#include <QClipboard>
#include <QByteArray>
#include <QDataStream>
#include <QGuiApplication>

TFramesController::TFramesController(QObject *parent) :
    TAbstractController(parent)
  , mFrameDelegate(new TFrameDelegate(this))
  , mTabWidget(nullptr)
  , mAnimationView(nullptr)
  , mCurrentAction(nullptr)
  , mSoundSetController(new TSoundSetController(this))
  , mVectorTableController(new TVectorTableController(this))
{

}

TFramesController::~TFramesController()
{

}

void TFramesController::setAction(TAction *action)
{
    if(action==mCurrentAction)
        return;

    mCurrentAction = action;

    if(mAnimationView) {
        mAnimationView->setModel(nullptr);
        TFramesModel *framesModel = action?action->framesModel():nullptr;
        if(framesModel)
            mAnimationView->setModel(framesModel);
        //mAnimationView->selectIndex(0);
    }
}

QList<TFrame *> TFramesController::getSelectedFramesList()
{
    QList<TFrame*> framesList;
    if(mCurrentAction)
    {
        QList<int> selectedIndex = mAnimationView->getSelectedIndexes();
        for(int index : selectedIndex)
        {
            TFrame *frame = mCurrentAction->frameAt(index);
            if(frame)
                framesList.append(frame);
        }
    }
    return framesList;
}

void TFramesController::slotRequestAddFrames(const QStringList &files, int pos)
{
    if(!mCurrentAction)
        return;

    mCurrentAction->cmdAddFrames(files, pos);
}

void TFramesController::slotRequestRemoveFrames(const QList<int> indexList)
{
    if(!mCurrentAction)
        return;

    mCurrentAction->cmdRemoveFrames(indexList);
}

void TFramesController::slotAnimationViewResized()
{

}

void TFramesController::slotIndexSelected(int index)
{
    if(!mCurrentAction)
        return;

    TFrame *frame = mCurrentAction->framesModel()->frameAt(index);
    if(frame) {
        TFrameScene *frameScene = frame->scene();
        connect(frameScene,
                SIGNAL(requestDisplayPropertySheet(TPropertySheet*)),
                this,
                SIGNAL(requestDisplayPropertySheet(TPropertySheet*)));

        mSoundSetController->setSoundSet(frame->soundSet());
        mVectorTableController->setVectorTableModel(frame->vectorTableModel());
        emit requestDisplayPropertySheet(frame->propertySheet());
        emit requestDisplayFrameScene(frameScene);
    }

    if(!mTabWidget)
        return;

    TMoveModelScene *moveModelScene = static_cast<TMoveModelScene*>(mTabWidget->currentContainer()->moveModelScene());
    if(moveModelScene)
    {
        moveModelScene->selectFrame(index);
    }
}

void TFramesController::slotRequestMoveFrames(const QList<int> &indexes, int pos)
{
    if(mCurrentAction)
    {
        mCurrentAction->cmdMoveFrames(indexes, pos);
    }
}

void TFramesController::slotRequestSetStandardFrame(int index)
{
    if(!mCurrentAction)
        return;

    TFrame *frame = mCurrentAction->framesModel()->frameAt(index);
    emit requestSetStandardFrame(frame);
}

void TFramesController::slotRequestCopyFrames(const QList<int> &indexes)
{
    if(!mCurrentAction || indexes.size()<1)
        return;

    TFramesModel *framesModel = mCurrentAction->framesModel();
    TFramesList framesList;
    for(int index : indexes)
    {
        TFrame *frame = framesModel->frameAt(index);
        if(!frame)
            continue;
        framesList.append(frame);
    }
    QByteArray byteArray;
    QDataStream dataStream(&byteArray, QIODevice::WriteOnly);
    dataStream << framesList.size();
    for(TFrame *frame : framesList)
        dataStream << *frame;

    QMimeData *mimeData = new QMimeData;
    QClipboard *clipBoard = qApp->clipboard();
    mimeData->setData(TFrame::mimeType(), byteArray);
    clipBoard->setMimeData(mimeData);
    mMainWindow->setStatusMessage(tr("Copy frames into clipboard."));
}

void TFramesController::slotRequestCloneFrames(const QList<int> &indexes)
{
    if(!mCurrentAction || indexes.size()<1)
        return;

    TFramesModel *framesModel = mCurrentAction->framesModel();
    TFramesList framesList;
    for(int index : indexes)
    {
        framesList.append(framesModel->frameAt(index));
    }
    mCurrentAction->cmdAddFrames(framesList, -1);
}

void TFramesController::slotRequestPasteFrames(int pos)
{
    if(!mCurrentAction)
        return;

    const QMimeData *mimeData = qApp->clipboard()->mimeData();
    QByteArray byteArray = mimeData->data(TFrame::mimeType());
    if(byteArray.isEmpty())
        return;

    int framesCount = 0;
    QDataStream dataStream(&byteArray, QIODevice::ReadOnly);
    dataStream >> framesCount;
    if(framesCount < 1)
        return;

    TFramesList framesList;
    TFramesModel *framesModel = mCurrentAction->framesModel();
    for(int i=0;i<framesCount;i++)
    {
        TFrame *frame = new TFrame(framesModel);
        dataStream >> *frame;
        framesList.append(frame);
    }
    mCurrentAction->cmdAddFrames(framesList, pos);
}

bool TFramesController::joint(TMainWindow *mainWindow, TCore *core)
{
    Q_ASSERT(mainWindow);
    Q_ASSERT(core);

    TActionsContainer *container = mainWindow->actionsContainer();
    connect(container, SIGNAL(requestAddFrames(QStringList,int)), this, SLOT(slotRequestAddFrames(QStringList,int)));
    connect(container, SIGNAL(requestRemoveFrames(QList<int>)), this, SLOT(slotRequestRemoveFrames(QList<int>)));
    connect(container, SIGNAL(requestCopyFrames(QList<int>)), this, SLOT(slotRequestCopyFrames(QList<int>)));
    connect(container, SIGNAL(requestCloneFrames(QList<int>)), this, SLOT(slotRequestCloneFrames(QList<int>)));
    connect(container, SIGNAL(requestPasteFrames(int)), this, SLOT(slotRequestPasteFrames(int)));

    mTabWidget = mainWindow->tabWidget();
    mAnimationView = container->animationView();
    mAnimationView->setItemDelegate(mFrameDelegate);
    mAnimationView->setCheckMimeType(TFrame::mimeType());
    connect(mAnimationView, SIGNAL(indexSelected(int)), this, SLOT(slotIndexSelected(int)));
    connect(mAnimationView, SIGNAL(requestSetStandardFrame(int)), this, SLOT(slotRequestSetStandardFrame(int)));
    connect(mAnimationView, SIGNAL(resized()), this, SLOT(slotAnimationViewResized()));

    connect(mAnimationView,
            SIGNAL(requestMoveFrames(QList<int>,int)),
            this,
            SLOT(slotRequestMoveFrames(QList<int>,int)));

    return TAbstractController::joint(mainWindow, core);
}

void TFramesController::setCurrentDocument(TDocument *document)
{
    mSoundSetController->setCurrentDocument(document);
    mVectorTableController->setCurrentDocument(document);

    if(!document)
        setAction(nullptr);
}

void TFramesController::slotTimerEvent()
{

}
