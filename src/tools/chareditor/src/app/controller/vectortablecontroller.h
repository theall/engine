#ifndef VECTORTABLECONTROLLER_H
#define VECTORTABLECONTROLLER_H

#include "abstractcontroller.h"
#include "propertycontroller.h"

class TVectorItem;
class TVectorView;
class TVectorDock;
class TVectorTableModel;
typedef QList<TVectorItem *> TVectorItemList;

class TVectorTableController : public TAbstractController
{
    Q_OBJECT

public:
    TVectorTableController(QObject *parent = 0);
    ~TVectorTableController();

    bool joint(TMainWindow *mainWindow, TCore *core) Q_DECL_OVERRIDE;
    void setCurrentDocument(TDocument *document) Q_DECL_OVERRIDE;

    TVectorTableModel *vectorTableModel() const;
    void setVectorTableModel(TVectorTableModel *vectorTableModel);

signals:

private slots:
    void slotRequestAddVectorItems(const QPointF &vector, int count);
    void slotRequestRemoveVectorItems(const QList<int> &indexList);
    void slotRowSelected(int index);
    void slotPropertyItemValueChanged(TPropertyItem *propertyItem, const QVariant &newValue);
    void slotVectorItemsAdded(const TVectorItemList &vectorItemList, const QList<int> indexList);
    void slotVectorItemsRemoved(const TVectorItemList &vectorItemList, const QList<int> indexList);

private:
    TVectorTableModel *mVectorTableModel;
    TVectorDock *mVectorDock;
    TVectorView *mVectorView;
    TTabWidget *mTabWidget;
    TPropertyController *mPropertyController;

    void updateClearAction();

protected slots:
    void slotTimerEvent() Q_DECL_OVERRIDE;
};

#endif // VECTORTABLECONTROLLER_H
