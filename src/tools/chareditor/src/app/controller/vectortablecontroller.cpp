#include "vectortablecontroller.h"
#include "../gui/component/vectordock/vectordock.h"
#include "../gui/component/vectordock/vectorview.h"
#include "../gui/component/tabwidget/tabwidget.h"
#include "../core/document/model/action/movemodelscene.h"
#include "../core/document/model/action/framesmodel/frame/vector/vectortablemodel.h"

TVectorTableController::TVectorTableController(QObject *parent) :
    TAbstractController(parent)
  , mVectorTableModel(nullptr)
  , mVectorDock(nullptr)
  , mVectorView(nullptr)
  , mPropertyController(new TPropertyController(this))
{
    connect(mPropertyController,
            SIGNAL(propertyItemValueChanged(TPropertyItem*,QVariant)),
            this,
            SLOT(slotPropertyItemValueChanged(TPropertyItem*,QVariant)));
}

TVectorTableController::~TVectorTableController()
{

}

bool TVectorTableController::joint(TMainWindow *mainWindow, TCore *core)
{
    Q_ASSERT(mainWindow);
    Q_ASSERT(core);

    mTabWidget = mainWindow->tabWidget();
    mVectorDock = mainWindow->vectorDock();
    connect(mVectorDock,
            SIGNAL(requestAddVectors(QPointF,int)),
            this,
            SLOT(slotRequestAddVectorItems(QPointF,int)));
    connect(mVectorDock,
            SIGNAL(requestRemoveVectors(QList<int>)),
            this,
            SLOT(slotRequestRemoveVectorItems(QList<int>)));

    mVectorView = mVectorDock->vectorView();
    connect(mVectorView, SIGNAL(rowSelected(int)), this, SLOT(slotRowSelected(int)));

    mPropertyController->setPropertyBrowser(mVectorDock->propertyBrowser());
    return TAbstractController::joint(mainWindow, core);
}

void TVectorTableController::setCurrentDocument(TDocument *document)
{
    mPropertyController->setCurrentDocument(document);

    if(document == nullptr)
    {
        mVectorView->setModel(nullptr);
        if(mVectorTableModel)
        {
            mVectorTableModel->disconnect(this);
            mVectorTableModel = nullptr;
        }
        updateClearAction();
    }
}

void TVectorTableController::slotRequestAddVectorItems(const QPointF &vector, int count)
{
    if(!mVectorTableModel)
        return;

    QList<QPointF> vectorList;
    for(int i=0;i<count;i++)
    {
        vectorList.append(vector);
    }
    if(vectorList.size() > 0) {
        mVectorTableModel->cmdAddVectorList(vectorList);
    }
}

void TVectorTableController::slotRequestRemoveVectorItems(const QList<int> &indexList)
{
    if(!mVectorView || !mVectorTableModel)
        return;

    TVectorItemList vectorItemList;
    for(int index : indexList)
    {
        TVectorItem *vectorItem = mVectorTableModel->getVectorItem(index);
        if(vectorItem)
            vectorItemList.append(vectorItem);
    }
    if(vectorItemList.size() > 0)
        mVectorTableModel->cmdRemoveVectorList(vectorItemList);
}

void TVectorTableController::slotRowSelected(int index)
{
    if(!mVectorTableModel)
        return;

    TVectorItem *vectorItem = mVectorTableModel->getVectorItem(index);
    mPropertyController->setPropertySheet(vectorItem?vectorItem->propertySheet():nullptr);

    if(!mTabWidget)
        return;

    TMoveModelScene *moveModelScene = static_cast<TMoveModelScene*>(mTabWidget->currentContainer()->moveModelScene());
    if(moveModelScene)
    {
        moveModelScene->selectFrame(index);
    }
}

TVectorTableModel *TVectorTableController::vectorTableModel() const
{
    return mVectorTableModel;
}

void TVectorTableController::setVectorTableModel(TVectorTableModel *vectorTableModel)
{
    if(mVectorTableModel == vectorTableModel)
        return;

    if(mVectorTableModel)
        mVectorTableModel->disconnect(this);

    mVectorTableModel = vectorTableModel;

    if(mVectorTableModel)
    {
        mVectorView->setModel(mVectorTableModel);
        slotRowSelected(-1);
        connect(mVectorTableModel,
                SIGNAL(vectorItemsAdded(TVectorItemList,QList<int>)),
                this,
                SLOT(slotVectorItemsAdded(TVectorItemList,QList<int>)));
        connect(mVectorTableModel,
                SIGNAL(vectorItemsRemoved(TVectorItemList,QList<int>)),
                this,
                SLOT(slotVectorItemsRemoved(TVectorItemList,QList<int>)));
    } else {
        mVectorView->setModel(nullptr);
    }

    updateClearAction();
}

void TVectorTableController::slotPropertyItemValueChanged(TPropertyItem *propertyItem, const QVariant &newValue)
{
    if(!propertyItem || !mDocument)
        return;

    TPropertySheet *propertySheet = qobject_cast<TPropertySheet*>(propertyItem->parent());
    if(!propertySheet)
        return;

    TPropertyUndoCommand *undoCommand = new TPropertyUndoCommand(
                propertyItem,
                propertyItem->propertyId(),
                newValue);
    mDocument->addUndoCommand(undoCommand);
}

void TVectorTableController::slotVectorItemsAdded(const TVectorItemList &vectorItemList, const QList<int> indexList)
{
    Q_UNUSED(vectorItemList);
    Q_UNUSED(indexList);

    updateClearAction();
}

void TVectorTableController::slotVectorItemsRemoved(const TVectorItemList &vectorItemList, const QList<int> indexList)
{
    Q_UNUSED(vectorItemList);
    Q_UNUSED(indexList);
    slotRowSelected(-1);
    updateClearAction();
}

void TVectorTableController::updateClearAction()
{
    if(!mVectorDock)
        return;

    bool enabled = false;
    if(mVectorTableModel)
        enabled = mVectorTableModel->hasVector();
    mVectorDock->setClearActionEnabled(enabled);
}

void TVectorTableController::slotTimerEvent()
{

}
