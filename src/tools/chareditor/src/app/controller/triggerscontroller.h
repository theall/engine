#ifndef TRIGGERSCONTROLLER_H
#define TRIGGERSCONTROLLER_H

#include "abstractcontroller.h"

class TTriggersView;
class TTriggerEditDialog;

class TTriggersController : public TAbstractController
{
    Q_OBJECT

public:
    TTriggersController(QObject *parent = 0);
    ~TTriggersController();

    void setAction(TAction *action);

private:
    TTriggersView *mTriggersView;
    TAction *mCurrentAction;
    TTriggerEditDialog *mTriggerEditDialog;

    void initTriggerEditDialog(QWidget *parent);

private slots:
    void slotEventCurrentIndexChanged(int index);
    void slotRequestCreateActionTrigger();
    void slotRequestRemoveActionTrigger(const QList<int> &rows);
    void slotRequestModifyActionTrigger(int row);

    // TAbstractController interface
public:
    bool joint(TMainWindow *mainWindow, TCore *core) Q_DECL_OVERRIDE;
    void setCurrentDocument(TDocument *document) Q_DECL_OVERRIDE;

protected slots:
    void slotTimerEvent() Q_DECL_OVERRIDE;
};

#endif // TRIGGERSCONTROLLER_H
