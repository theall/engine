SOURCES += \
    $$PWD/abstractcontroller.cpp \
    $$PWD/maincontroller.cpp \
    $$PWD/optionscontroller.cpp \
    $$PWD/module/zoomable.cpp \
    $$PWD/tabcontroller.cpp \
    $$PWD/propertycontroller.cpp \
    $$PWD/undocontroller.cpp \
    $$PWD/actionscontroller.cpp \
    $$PWD/framescontroller.cpp \
    $$PWD/module/framedelegate.cpp \
    $$PWD/resourcecontroller.cpp \
    $$PWD/triggerscontroller.cpp \
    $$PWD/soundsetcontroller.cpp \
    $$PWD/mainpropertycontroller.cpp \
    $$PWD/vectortablecontroller.cpp

HEADERS  += \
    $$PWD/abstractcontroller.h \
    $$PWD/maincontroller.h \
    $$PWD/optionscontroller.h \
    $$PWD/module/zoomable.h \
    $$PWD/tabcontroller.h \
    $$PWD/propertycontroller.h \
    $$PWD/undocontroller.h \
    $$PWD/actionscontroller.h \
    $$PWD/framescontroller.h \
    $$PWD/module/framedelegate.h \
    $$PWD/resourcecontroller.h \
    $$PWD/triggerscontroller.h \
    $$PWD/soundsetcontroller.h \
    $$PWD/mainpropertycontroller.h \
    $$PWD/vectortablecontroller.h
