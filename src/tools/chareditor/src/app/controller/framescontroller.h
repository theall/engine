#ifndef FRAMESCONTROLLER_H
#define FRAMESCONTROLLER_H

#include "abstractcontroller.h"

#include <QAbstractItemModel>
#include <QGraphicsScene>

class TFrameDelegate;
class TFrameScene;
class TSoundSetController;
class TAnimationView;
class TVectorTableController;

class TFramesController : public TAbstractController
{
    Q_OBJECT

public:
    TFramesController(QObject *parent = 0);
    ~TFramesController();

    void setAction(TAction *action);
    QList<TFrame*> getSelectedFramesList();

signals:
    // Send to action controller
    void requestDisplayPropertySheet(TPropertySheet *proertySheet);
    void requestSetStandardFrame(TFrame *frame);

    // Send to tab controller
    void requestDisplayFrameScene(TFrameScene *scene);

private slots:
    void slotRequestAddFrames(const QStringList &files, int pos);
    void slotRequestRemoveFrames(const QList<int> indexList);
    void slotAnimationViewResized();
    void slotIndexSelected(int index);
    void slotRequestMoveFrames(const QList<int> &indexes, int pos);
    void slotRequestSetStandardFrame(int index);
    void slotRequestCopyFrames(const QList<int> &indexes);
    void slotRequestCloneFrames(const QList<int> &indexes);
    void slotRequestPasteFrames(int pos);

private:
    TFrameDelegate *mFrameDelegate;
    TTabWidget *mTabWidget;
    TAnimationView *mAnimationView;
    TAction *mCurrentAction;
    TSoundSetController *mSoundSetController;
    TVectorTableController *mVectorTableController;

    // TAbstractController interface
public:
    bool joint(TMainWindow *mainWindow, TCore *core) Q_DECL_OVERRIDE;
    void setCurrentDocument(TDocument *document) Q_DECL_OVERRIDE;

protected slots:
    void slotTimerEvent() Q_DECL_OVERRIDE;
};

#endif // FRAMESCONTROLLER_H
