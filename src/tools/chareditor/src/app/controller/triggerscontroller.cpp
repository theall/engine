#include "triggerscontroller.h"

#include "../gui/component/actionsdock/actionscontainer.h"
#include "../gui/component/actionsdock/triggersview.h"
#include "../gui/dialogs/triggereditdialog.h"
#include "../core/base/docutil.h"
#include "../core/document/model/action/action.h"
#include "../core/document/model/action/triggersmodel/actiontrigger.h"

TTriggersController::TTriggersController(QObject *parent) :
    TAbstractController(parent)
  , mTriggersView(nullptr)
  , mCurrentAction(nullptr)
  , mTriggerEditDialog(nullptr)
{

}

TTriggersController::~TTriggersController()
{

}

void TTriggersController::setAction(TAction *action)
{
    if(action==mCurrentAction)
        return;

    if(mTriggersView)
        mTriggersView->setModel(action?action->triggersModel():nullptr);

    mCurrentAction = action;
}

void TTriggersController::initTriggerEditDialog(QWidget *parent)
{
    if(mTriggerEditDialog)
        return;

    mTriggerEditDialog = new TTriggerEditDialog(parent);
    QComboBox *eventComboBox = mTriggerEditDialog->eventComboBox();
    for(int i=0;i<GE_COUNT;i++)
    {
        eventComboBox->addItem(DocUtil::GameEventToQString((GameEvent)i), i);
    }
    connect(eventComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(slotEventCurrentIndexChanged(int)));
}

void TTriggersController::slotEventCurrentIndexChanged(int index)
{
    mTriggerEditDialog->setKeyEditButtonVisible((GameEvent)index==GE_KEYDOWN);
}

bool TTriggersController::joint(TMainWindow *mainWindow, TCore *core)
{
    Q_ASSERT(mainWindow);
    Q_ASSERT(core);

    TActionsContainer *container = mainWindow->actionsContainer();
    initTriggerEditDialog(container);
    connect(container, SIGNAL(requestCreateActionTrigger()), this, SLOT(slotRequestCreateActionTrigger()));
    connect(container, SIGNAL(requestRemoveActionTrigger(QList<int>)), this, SLOT(slotRequestRemoveActionTrigger(QList<int>)));
    connect(container, SIGNAL(requestModifyActionTrigger(int)), this, SLOT(slotRequestModifyActionTrigger(int)));

    mTriggersView = container->triggersView();
    return TAbstractController::joint(mainWindow, core);
}

void TTriggersController::setCurrentDocument(TDocument *document)
{
    Q_UNUSED(document);
}

void TTriggersController::slotRequestCreateActionTrigger()
{
    if(!mCurrentAction)
        return;

    mTriggerEditDialog->setValue(QString());
    if(mTriggerEditDialog->exec()!=QDialog::Accepted)
        return;

    TActionTrigger *newActionTrigger = new TActionTrigger(mCurrentAction->triggersModel());
    newActionTrigger->setGameEvent((GameEvent)mTriggerEditDialog->getEvent());
    newActionTrigger->setValue(mTriggerEditDialog->getValue());
    mCurrentAction->cmdAddActionTrigger(newActionTrigger);
}

void TTriggersController::slotRequestRemoveActionTrigger(const QList<int> &rows)
{
    if(!mCurrentAction)
        return;

    mCurrentAction->cmdRemoveActionTriggers(rows);
}

void TTriggersController::slotRequestModifyActionTrigger(int row)
{
    if(!mCurrentAction)
        return;

    TActionTriggersModel *triggerModel = mCurrentAction->triggersModel();
    TActionTrigger *actionTrigger = triggerModel->getActionTrigger(row);
    if(!actionTrigger)
        return;

    mTriggerEditDialog->setEvent((int)actionTrigger->gameEvent());
    mTriggerEditDialog->setValue(actionTrigger->value());
    if(mTriggerEditDialog->exec()!=QDialog::Accepted)
        return;

    TActionTrigger newActionTrigger;
    newActionTrigger.setGameEvent((GameEvent)mTriggerEditDialog->getEvent());
    newActionTrigger.setValue(mTriggerEditDialog->getValue());
    mCurrentAction->cmdModifyActionTrigger(actionTrigger, newActionTrigger);
}

void TTriggersController::slotTimerEvent()
{

}
