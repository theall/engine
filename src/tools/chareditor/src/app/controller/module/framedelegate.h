#ifndef FRAMEDELEGATE_H
#define FRAMEDELEGATE_H

#include <QAbstractItemDelegate>

/*
 * The delegate for drawing frame items in the frames view.
 */
class TFrameDelegate : public QAbstractItemDelegate
{
    Q_OBJECT

public:
    TFrameDelegate(QObject *parent= nullptr);
    ~TFrameDelegate();

private:

    // QAbstractItemDelegate interface
public:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
};

#endif // FRAMEDELEGATE_H
