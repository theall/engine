#include "framedelegate.h"

#include "../../core/core.h"
#include "../../core/document/model/action/framesmodel/framesmodel.h"
#include "../../core/document/model/action/framesmodel/frame/frame.h"

#include <QPainter>

TFrameDelegate::TFrameDelegate(QObject *parent) :
    QAbstractItemDelegate(parent)
{

}

TFrameDelegate::~TFrameDelegate()
{

}

void TFrameDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    TFramesModel *model = (TFramesModel*)qobject_cast<const TFramesModel*>(index.model());
    TFrame *frame = model->frameAt(index);
    if(!frame)
        return;

    const TPixmap *frameImage = frame->pixmap();
    if(frameImage)
    {
        // Draw the frame image
        painter->setRenderHint(QPainter::SmoothPixmapTransform);

        // center draw
        QRect targetRect = option.rect;
        int parentWidth = option.rect.width();
        int parentHeight = option.rect.height();
        int offsetTop = ((float)parentHeight-frameImage->height())/2+0.5;
        if(offsetTop < 0)
            offsetTop = 0;
        int offsetLeft = ((float)parentWidth-frameImage->width())/2+0.5;
        if(offsetLeft < 0)
            offsetLeft = 0;

        targetRect.setTop(option.rect.top() + offsetTop);
        targetRect.setLeft(option.rect.left() + offsetLeft);
        targetRect.setWidth(qMin(frameImage->width(), parentWidth));
        targetRect.setHeight(qMin(frameImage->height(), parentHeight));
        painter->drawPixmap(targetRect, frameImage->pixmap());
    } else {
        painter->fillRect(option.rect, Qt::black);
    }

    int rectLeft = option.rect.left() + 5;
    int rectTop = option.rect.top() + 5;
    int rectWidth = 5;
    painter->save();
    // Draw areas
    if(frame->hasCollideArea())
    {
        painter->setPen(QColor(255, 128, 255));
        painter->drawRect(rectLeft, rectTop, rectWidth, rectWidth);
    }
    rectTop += rectWidth + 5;
    if(frame->hasAttackArea())
    {
        painter->setPen(Qt::red);
        painter->drawRect(rectLeft, rectTop, rectWidth, rectWidth);
    }
    rectTop += rectWidth + 5;
    if(frame->hasUndertakeArea())
    {
        painter->setPen(Qt::blue);
        painter->drawRect(rectLeft, rectTop, rectWidth, rectWidth);
    }
    rectTop += rectWidth + 5;
    if(frame->hasTerrianArea())
    {
        painter->setPen(Qt::green);
        painter->drawRect(rectLeft, rectTop, rectWidth, rectWidth);
    }
    rectTop += rectWidth + 5;
    if(frame->hasSound())
    {
        painter->setPen(Qt::black);
        painter->drawLine(rectLeft, rectTop+2, rectLeft, rectTop+4);
        painter->drawLine(rectLeft+2, rectTop+1, rectLeft+2, rectTop+5);
        painter->drawLine(rectLeft+4, rectTop, rectLeft+4, rectTop+6);
    }
    rectTop += rectWidth + 5;
    if(frame->hasVector())
    {
        painter->setPen(Qt::black);
        int rectRight = rectLeft + 6;
        int rectVCenter = rectTop + 3;
        painter->drawLine(rectRight-2, rectVCenter-2, rectRight, rectVCenter);
        painter->drawLine(rectLeft, rectVCenter, rectRight, rectVCenter);
        painter->drawLine(rectRight-2, rectVCenter+2, rectRight, rectVCenter);
    }
    painter->restore();
    // Overlay with highlight color when selected
    //if (option.state & QStyle.State_Selected):
    if(option.state&QStyle::State_Selected)
    {
        int opacity = painter->opacity();
        painter->setOpacity(0.5);
        painter->fillRect(option.rect, option.palette.highlight());
        painter->setOpacity(opacity);
    } else if(option.state&QStyle::State_HasFocus) {
        int opacity = painter->opacity();
        painter->setOpacity(0.1);
        painter->fillRect(option.rect, option.palette.highlight());
        painter->setOpacity(opacity);
    }
}

QSize TFrameDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    return QSize(64, 64);
}
