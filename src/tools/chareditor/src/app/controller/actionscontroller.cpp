#include "actionscontroller.h"

#include "../gui/component/actionsdock/actionscontainer.h"
#include "../gui/component/actionsdock/actionsview.h"
#include "../core/document/model/actionsmodel.h"
#include "../core/document/model/action/movemodelscene.h"
#include "../core/document/model/action/framesmodel/frame/framescene/framescene.h"

TActionsController::TActionsController(QObject *parent) :
    TAbstractController(parent)
  , mActionsView(nullptr)
  , mActionsContainer(nullptr)
  , mCurrentAction(nullptr)
  , mFramesController(new TFramesController(this))
  , mTriggersController(new TTriggersController(this))
{
    connect(mFramesController,
            SIGNAL(requestDisplayPropertySheet(TPropertySheet*)),
            this,
            SIGNAL(requestDisplayPropertySheet(TPropertySheet*)));
    connect(mFramesController,
            SIGNAL(requestSetStandardFrame(TFrame*)),
            this,
            SLOT(slotRequestSetStandardFrame(TFrame*)));

    connect(mFramesController,
            SIGNAL(requestDisplayFrameScene(TFrameScene*)),
            this,
            SIGNAL(requestDisplayFrameScene(TFrameScene*)));
}

TActionsController::~TActionsController()
{

}

bool TActionsController::joint(TMainWindow *mainWindow, TCore *core)
{
    Q_ASSERT(mainWindow);
    Q_ASSERT(core);

    mActionsContainer = mainWindow->actionsContainer();
    connect(mActionsContainer, SIGNAL(requestAddNewAction(QString)), this, SLOT(slotRequestAddNewAction(QString)));
    connect(mActionsContainer, SIGNAL(requestRemoveActions(QList<int>)), this, SLOT(slotRequestRemoveAction(QList<int>)));
    connect(mActionsContainer, SIGNAL(requestPlayAction()), this, SLOT(slotRequestPlayAction()));
    connect(mActionsContainer, SIGNAL(requestStopPlayAction()), this, SLOT(slotRequestStopPlayAction()));
    connect(mActionsContainer, SIGNAL(requestAdjustFPS(int)), this, SLOT(slotRequestAdjustFPS(int)));

    mActionsView = mActionsContainer->actionsView();
    Q_ASSERT(mActionsView);
    connect(mActionsView, SIGNAL(rowSelected(int)), this, SLOT(slotRowSelected(int)));

    return TAbstractController::joint(mainWindow, core);
}

void TActionsController::setCurrentDocument(TDocument *document)
{
    mFramesController->setCurrentDocument(document);

    if(!document)
    {
        mActionsView->setModel(nullptr);
        return;
    }

    mActionsView->setModel(document->actionsModel());
    // Set default action to framescontroller after new document created
    mActionsView->selectRow(0);
}

void TActionsController::slotRequestAddNewAction(const QString &name)
{
    if(!mDocument)
        return;

    mDocument->cmdAddAction(name);
}

void TActionsController::slotRequestRemoveAction(const QList<int> rows)
{
    for(int row : rows)
    {
        mDocument->cmdRemoveAction(row);
    }
}

void TActionsController::slotRowSelected(int row)
{
    if(!mActionsView)
        return;

    TActionsModel *actionsModel = dynamic_cast<TActionsModel *>(mActionsView->model());
    if(!actionsModel)
        return;

    setCurrentAction(actionsModel->getAction(row));
}

void TActionsController::slotRequestPlayAction()
{
    emit requestPlayAction();
}

void TActionsController::slotRequestStopPlayAction()
{
    emit requestStopPlayAction();
}

void TActionsController::slotRequestAdjustFPS(int fps)
{
    if(mCurrentAction)
        mCurrentAction->moveModelScene()->setFps(fps);
}

void TActionsController::slotRequestSetStandardFrame(TFrame *frame)
{
    if(mDocument)
        mDocument->setStandardFrame(frame);
}

void TActionsController::setCurrentAction(TAction *action)
{
    if(action)
        emit requestDisplayPropertySheet(action->propertySheet());

    if(action == mCurrentAction)
        return;

    if(action)
    {
        emit requestDisplayMoveModelScene(action->moveModelScene());
        mFramesController->setAction(action);
        mTriggersController->setAction(action);
        mActionsContainer->setFPS(action->moveModelScene()->fps());
    }
    mCurrentAction = action;
}

TFramesController *TActionsController::framesController() const
{
    return mFramesController;
}

void TActionsController::slotTimerEvent()
{

}
