#ifndef ACTIONSCONTROLLER_H
#define ACTIONSCONTROLLER_H

#include "framescontroller.h"
#include "triggerscontroller.h"

class TFrameScene;
class TActionsView;
class TMoveModelScene;

class TActionsController : public TAbstractController
{
    Q_OBJECT

public:
    TActionsController(QObject *parent = 0);
    ~TActionsController();

    bool joint(TMainWindow *mainWindow, TCore *core) Q_DECL_OVERRIDE;
    void setCurrentDocument(TDocument *document) Q_DECL_OVERRIDE;

    TFramesController *framesController() const;

signals:
    // Send to property controller
    void requestDisplayPropertySheet(TPropertySheet *proertySheet);

    // Send to tab controller
    void requestDisplayMoveModelScene(TMoveModelScene *scene);
    void requestDisplayFrameScene(TFrameScene *scene);
    void requestPlayAction();
    void requestStopPlayAction();

private slots:
    void slotRequestAddNewAction(const QString &name);
    void slotRequestRemoveAction(const QList<int> rows);
    void slotRowSelected(int row);
    void slotRequestPlayAction();
    void slotRequestStopPlayAction();
    void slotRequestAdjustFPS(int fps);

    // From framescontroller
    void slotRequestSetStandardFrame(TFrame* frame);

private:
    TActionsView *mActionsView;
    TActionsContainer *mActionsContainer;
    TAction *mCurrentAction;
    TFramesController *mFramesController;
    TTriggersController *mTriggersController;

    void setCurrentAction(TAction *action);

protected slots:
    void slotTimerEvent() Q_DECL_OVERRIDE;
};

#endif // ACTIONSCONTROLLER_H
