QT += gui widgets multimedia
CONFIG += qt

INCLUDEPATH += \
    $$PWD/widgets

TRANSLATIONS = \
    $$PWD/resource/ts/en.ts \
    $$PWD/resource/ts/zh_cn.ts \
    $$PWD/resource/ts/zh_tw.ts

SOURCES += \
    $$PWD/mainwindow.cpp \
    $$PWD/widgets/qtcolorbutton.cpp \
    $$PWD/dialogs/aboutdialog.cpp \
    $$PWD/dialogs/abstractdialog.cpp \
    $$PWD/component/tabwidget/tabwidget.cpp \
    $$PWD/dialogs/newprojectdialog.cpp \
    $$PWD/component/undodock/undodock.cpp \
    $$PWD/component/basedock.cpp \
    $$PWD/component/propertydock/propertiesdock.cpp \
    $$PWD/component/propertydock/propertybrowser.cpp \
    $$PWD/component/propertydock/varianteditorfactory.cpp \
    $$PWD/component/propertydock/variantpropertymanager.cpp \
    $$PWD/component/actionsdock/actionsdock.cpp \
    $$PWD/component/actionsdock/actionsview.cpp \
    $$PWD/component/tabwidget/movemodelview/movemodelview.cpp \
    $$PWD/component/propertydock/widget/plaintexteditor.cpp \
    $$PWD/component/propertydock/widget/filepathedit.cpp \
    $$PWD/component/propertydock/widget/stringexedit.cpp \
    $$PWD/component/actionsdock/actionscontainer.cpp \
    $$PWD/component/actionsdock/animationview.cpp \
    $$PWD/component/centralwidget.cpp \
    $$PWD/dialogs/newactiondialog.cpp \
    $$PWD/dialogs/preferencesdialog.cpp \
    $$PWD/widgets/qcomboboxex.cpp \
    $$PWD/widgets/qprogressindicator.cpp \
    $$PWD/widgets/shortcutview.cpp \
    $$PWD/dialogs/preferencesdialog/module/languagemanager.cpp \
    $$PWD/dialogs/preferencesdialog/module/shortkeymanager.cpp \
    $$PWD/dialogs/preferencesdialog/shortcutmodel.cpp \
    $$PWD/component/tabwidget/tabcontainer.cpp \
    $$PWD/widgets/zoomcombobox.cpp \
    $$PWD/component/actionsdock/triggersview.cpp \
    $$PWD/dialogs/keyeditdialog.cpp \
    $$PWD/widgets/keycodeedit.cpp \
    $$PWD/dialogs/triggereditdialog.cpp \
    $$PWD/dialogs/choosedirectiondialog.cpp \
    $$PWD/dialogs/pixmapresourcedialog.cpp \
    $$PWD/dialogs/soundresourcedialog.cpp \
    $$PWD/component/sounddock/sounddock.cpp \
    $$PWD/component/propertydock/widget/pixmapedit.cpp \
    $$PWD/component/propertydock/widget/soundsetedit.cpp \
    $$PWD/component/propertydock/widget/sounditemedit.cpp \
    $$PWD/component/propertydock/widget/sounditemsourceedit.cpp \
    $$PWD/component/sounddock/soundsetview.cpp \
    $$PWD/component/vectordock/vectordock.cpp \
    $$PWD/component/vectordock/vectorview.cpp \
    $$PWD/dialogs/newvectordialog.cpp

HEADERS += \
    $$PWD/mainwindow.h \
    $$PWD/widgets/qtcolorbutton.h \
    $$PWD/dialogs/aboutdialog.h \
    $$PWD/dialogs/abstractdialog.h \
    $$PWD/component/tabwidget/tabwidget.h \
    $$PWD/dialogs/newprojectdialog.h \
    $$PWD/component/undodock/undodock.h \
    $$PWD/component/basedock.h \
    $$PWD/component/propertydock/propertiesdock.h \
    $$PWD/component/propertydock/propertybrowser.h \
    $$PWD/component/propertydock/varianteditorfactory.h \
    $$PWD/component/propertydock/variantpropertymanager.h \
    $$PWD/component/actionsdock/actionsdock.h \
    $$PWD/component/actionsdock/actionsview.h \
    $$PWD/component/tabwidget/movemodelview/movemodelview.h \
    $$PWD/component/propertydock/widget/plaintexteditor.h \
    $$PWD/component/propertydock/widget/filepathedit.h \
    $$PWD/component/propertydock/widget/stringexedit.h \
    $$PWD/component/actionsdock/actionscontainer.h \
    $$PWD/component/actionsdock/animationview.h \
    $$PWD/component/centralwidget.h \
    $$PWD/dialogs/newactiondialog.h \
    $$PWD/dialogs/preferencesdialog.h \
    $$PWD/widgets/qcomboboxex.h \
    $$PWD/widgets/qprogressindicator.h \
    $$PWD/widgets/shortcutview.h \
    $$PWD/dialogs/preferencesdialog/module/languagemanager.h \
    $$PWD/dialogs/preferencesdialog/module/shortkeymanager.h \
    $$PWD/dialogs/preferencesdialog/shortcutmodel.h \
    $$PWD/component/tabwidget/tabcontainer.h \
    $$PWD/widgets/zoomcombobox.h \
    $$PWD/component/actionsdock/triggersview.h \
    $$PWD/dialogs/keyeditdialog.h \
    $$PWD/widgets/keycodeedit.h \
    $$PWD/dialogs/triggereditdialog.h \
    $$PWD/dialogs/choosedirectiondialog.h \
    $$PWD/dialogs/pixmapresourcedialog.h \
    $$PWD/dialogs/soundresourcedialog.h \
    $$PWD/component/sounddock/sounddock.h \
    $$PWD/component/propertydock/widget/pixmapedit.h \
    $$PWD/component/propertydock/widget/soundsetedit.h \
    $$PWD/component/propertydock/widget/sounditemedit.h \
    $$PWD/component/propertydock/widget/sounditemsourceedit.h \
    $$PWD/component/sounddock/soundsetview.h \
    $$PWD/component/vectordock/vectordock.h \
    $$PWD/component/vectordock/vectorview.h \
    $$PWD/dialogs/newvectordialog.h

FORMS += \
    $$PWD/mainwindow.ui \
    $$PWD/dialogs/aboutdialog.ui \
    $$PWD/dialogs/effectsdialog.ui \
    $$PWD/dialogs/eventSelect.ui \
    $$PWD/dialogs/exportdialog.ui \
    $$PWD/dialogs/loadingdialog.ui \
    $$PWD/dialogs/newactiondialog.ui \
    $$PWD/dialogs/preferencesdialog.ui \
    $$PWD/dialogs/newprojectdialog.ui \
    $$PWD/dialogs/keyeditdialog.ui \
    $$PWD/dialogs/triggereditdialog.ui \
    $$PWD/dialogs/choosedirectiondialog.ui \
    $$PWD/dialogs/pixmapresourcedialog.ui \
    $$PWD/dialogs/soundresourcedialog.ui \
    $$PWD/dialogs/newvectordialog.ui

RESOURCES += $$PWD/resource/chreditor.qrc

CONFIG(debug, debug|release) {
    LIBS += -lqtpropertybrowserd
} else {
    LIBS += -lqtpropertybrowser
}
