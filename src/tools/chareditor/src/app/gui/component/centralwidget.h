#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

#include "tabwidget/tabwidget.h"
#include "actionsdock/actionscontainer.h"

class TCentralWidget : public QWidget
{
public:
    TCentralWidget(QWidget *parent = nullptr);
    ~TCentralWidget();

    TTabWidget *tabWidget() const;
    TActionsContainer *actionsContainer() const;

private:
    TTabWidget *mTabWidget;
    TActionsContainer *mActionsContainer;
};

#endif // CENTRALWIDGET_H
