#include "vectorview.h"

#include <QHeaderView>
#include <QMouseEvent>
#include <QContextMenuEvent>

TVectorView::TVectorView(QWidget *parent) :
    QTableView(parent)
  , mContextMenu(new QMenu(this))
{
    setObjectName(QStringLiteral("actionsView"));
    setFrameShape(QFrame::Panel);
    setFrameShadow(QFrame::Sunken);

    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::ExtendedSelection);

    horizontalHeader()->setStretchLastSection(true);
    horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    //horizontalHeader()->setVisible(false);
    verticalHeader()->setVisible(false);

    setSortingEnabled(false);
    setShowGrid(true);

    mActionClone = mContextMenu->addAction(QString(), this, SLOT(slotActionCloneTriggered()));
    mActionCopy = mContextMenu->addAction(QString(), this, SLOT(slotActionCopyTriggered()));
    mActionRemove = mContextMenu->addAction(QString(), this, SLOT(slotActionRemoveTriggered()));
    // mContextMenu->addSeparator();
    // mActionShowGrid = mContextMenu->addAction(QString(), this, SLOT(slotActionShowGridTriggered(bool)));

    retranslateUi();
}

TVectorView::~TVectorView()
{

}

QList<int> TVectorView::getSelectedIndexes()
{
    QSet<int> selectedRows;
    for(QModelIndex index : selectionModel()->selectedIndexes())
    {
        selectedRows.insert(index.row());
    }
    return selectedRows.toList();
}

int TVectorView::currentRow()
{
    return currentIndex().row();
}

void TVectorView::selectRow(int row)
{
    if(model())
    {
        QTableView::selectRow(row);
        emit rowSelected(row);
    }
}

int TVectorView::rowCount()
{
    QAbstractItemModel *m = model();
    return m?m->rowCount():0;
}

void TVectorView::slotActionRemoveTriggered()
{

}

void TVectorView::slotActionShowGridTriggered(bool checked)
{
    setShowGrid(checked);
}

void TVectorView::slotActionCloneTriggered()
{

}

void TVectorView::slotActionCopyTriggered()
{

}

void TVectorView::slotActionRenameTriggered()
{
    edit(currentIndex());
}

void TVectorView::slotSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    Q_UNUSED(selected);
    Q_UNUSED(deselected);

    emit hasSelectionChanged(getSelectedIndexes().size()>0);
}

void TVectorView::retranslateUi()
{
    mActionCopy->setText(tr("Copy"));
    mActionRemove->setText(tr("Remove"));
    mActionClone->setText(tr("Clone"));
    // mActionShowGrid->setText(tr("Show grid"));
}

void TVectorView::setModel(QAbstractItemModel *model)
{
    QTableView::setModel(model);
    emit validChanged(model!=nullptr);

    if(model)
    {
        connect(selectionModel(),
                SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
                this,
                SLOT(slotSelectionChanged(QItemSelection,QItemSelection)));
    }
    emit hasSelectionChanged(false);
}

void TVectorView::mousePressEvent(QMouseEvent *event)
{
    emit rowSelected(indexAt(event->pos()).row());
    QTableView::mousePressEvent(event);
}

void TVectorView::contextMenuEvent(QContextMenuEvent *event)
{
    mContextMenu->popup(event->globalPos());
}
