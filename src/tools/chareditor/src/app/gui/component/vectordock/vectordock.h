#ifndef VECTORDOCK_H
#define VECTORDOCK_H

#include <QAction>
#include <QToolBar>

#include "../basedock.h"

class TVectorView;
class TPropertyBrowser;
class TNewVectorDialog;

class TVectorDock : public TBaseDock
{
    Q_OBJECT

public:
    TVectorDock(QWidget *parent = nullptr);
    ~TVectorDock();

    TVectorView *vectorView() const;
    TPropertyBrowser *propertyBrowser() const;

    void setClearActionEnabled(bool enabled);

signals:
    void requestAddVectors(const QPointF &vector, int count);
    void requestRemoveVectors(const QList<int> &indexList);

private slots:
    void slotActionNewVectorTriggered();
    void slotActionRemoveVectorTriggered();
    void slotActionClearAllVectorsTriggered();
    void slotSoundModelValidChanged(bool valid);
    void slotVectorSelectionChanged(bool hasSelection);

private:
    QAction *mActionNewVector;
    QAction *mActionRemoveVector;
    QAction *mActionClear;

    TVectorView *mVectorView;
    TPropertyBrowser *mPropertyBrowser;
    TNewVectorDialog *mNewVectorDialog;

    // TBaseDock interface
public:
    void retranslateUi() Q_DECL_OVERRIDE;
};

#endif // VECTORDOCK_H
