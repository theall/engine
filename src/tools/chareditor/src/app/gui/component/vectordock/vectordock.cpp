#include "vectordock.h"
#include "vectorview.h"
#include "../propertydock/propertybrowser.h"
#include "../../dialogs/newvectordialog.h"

#include <QSplitter>
#include <QVBoxLayout>
#include <QFileDialog>

TVectorDock::TVectorDock(QWidget *parent) :
    TBaseDock(QLatin1String("VectorDock"), parent)
  , mVectorView(new TVectorView(this))
  , mPropertyBrowser(new TPropertyBrowser(this))
  , mNewVectorDialog(new TNewVectorDialog(this))
{
    CREATE_ACTION(mActionNewVector, ":/actionsdock/images/add.png", slotActionNewVectorTriggered);
    CREATE_ACTION(mActionRemoveVector, ":/actionsdock/images/remove.png", slotActionRemoveVectorTriggered);
    CREATE_ACTION(mActionClear, ":/tools/images/clear.png", slotActionClearAllVectorsTriggered);

    QToolBar *toolBar = new QToolBar(this);
    toolBar->setFloatable(false);
    toolBar->setMovable(false);
    toolBar->setIconSize(QSize(16, 16));
    toolBar->addAction(mActionNewVector);
    toolBar->addAction(mActionRemoveVector);
    toolBar->addSeparator();
    toolBar->addAction(mActionClear);

    QWidget *container = new QWidget(this);
    QSplitter *splitter = new QSplitter(this);
    splitter->setOrientation(Qt::Vertical);
    splitter->addWidget(mVectorView);
    splitter->addWidget(mPropertyBrowser);
    QVBoxLayout *vBoxLayout = new QVBoxLayout;
    vBoxLayout->setContentsMargins(5, 5, 5, 5);
    vBoxLayout->setSpacing(0);
    vBoxLayout->addWidget(toolBar);
    vBoxLayout->addWidget(splitter);
    container->setLayout(vBoxLayout);
    setWidget(container);

    connect(mVectorView,
            SIGNAL(hasSelectionChanged(bool)),
            this,
            SLOT(slotVectorSelectionChanged(bool)));
    connect(mVectorView,
            SIGNAL(validChanged(bool)),
            this,
            SLOT(slotSoundModelValidChanged(bool)));
    retranslateUi();
}

TVectorDock::~TVectorDock()
{

}

void TVectorDock::slotActionNewVectorTriggered()
{
    if(mNewVectorDialog->exec()==QDialog::Rejected)
        return;

    QPointF vector = mNewVectorDialog->getVector();
    int vectorCount = mNewVectorDialog->getCount();
    emit requestAddVectors(vector, vectorCount);
}

void TVectorDock::slotActionRemoveVectorTriggered()
{
    emit requestRemoveVectors(mVectorView->getSelectedIndexes());
}

void TVectorDock::slotActionClearAllVectorsTriggered()
{
    mVectorView->selectAll();
    emit requestRemoveVectors(mVectorView->getSelectedIndexes());
}

void TVectorDock::slotSoundModelValidChanged(bool valid)
{
    mActionNewVector->setEnabled(valid);
}

void TVectorDock::slotVectorSelectionChanged(bool hasSelection)
{
    mActionRemoveVector->setEnabled(hasSelection);
}

TPropertyBrowser *TVectorDock::propertyBrowser() const
{
    return mPropertyBrowser;
}

void TVectorDock::setClearActionEnabled(bool enabled)
{
    mActionClear->setEnabled(enabled);
}

TVectorView *TVectorDock::vectorView() const
{
    return mVectorView;
}

void TVectorDock::retranslateUi()
{
    setWindowTitle(tr("Vector table"));
    setToolTip(tr("Vector edit."));
    mActionNewVector->setToolTip(tr("Add new vector"));
    mActionNewVector->setText(tr("New Vector"));
    mActionRemoveVector->setToolTip(tr("Remove vector."));
    mActionRemoveVector->setText(tr("Remove current vector"));
    mActionClear->setText(tr("Clear"));
    mActionClear->setToolTip(tr("Clear all vectors."));
}
