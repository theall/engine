#include "centralwidget.h"

#include <QSplitter>
#include <QVBoxLayout>

TCentralWidget::TCentralWidget(QWidget *parent) :
    QWidget(parent)
  , mTabWidget(new TTabWidget(this))
  , mActionsContainer(new TActionsContainer(this))
{
    QVBoxLayout *vbl = new QVBoxLayout;
    vbl->setMargin(0);
    QSplitter *splitter = new QSplitter(this);
    splitter->setOrientation(Qt::Vertical);
    splitter->setContentsMargins(3,3,3,3);
    splitter->setChildrenCollapsible(false);
    splitter->addWidget(mTabWidget);
    splitter->addWidget(mActionsContainer);
    vbl->addWidget(splitter);
    setLayout(vbl);
}

TCentralWidget::~TCentralWidget()
{

}

TTabWidget *TCentralWidget::tabWidget() const
{
    return mTabWidget;
}

TActionsContainer *TCentralWidget::actionsContainer() const
{
    return mActionsContainer;
}
