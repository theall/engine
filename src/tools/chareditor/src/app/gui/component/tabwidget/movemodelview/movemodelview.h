#ifndef MOVEMODELVIEW_H
#define MOVEMODELVIEW_H

#include <QGraphicsView>

class TMoveModelView : public QGraphicsView
{
public:
    TMoveModelView(QWidget *parent = nullptr);
    ~TMoveModelView();

private:
    void locatePosition(const QPoint &pos);

    // QWidget interface
protected:
    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
};

#endif // MOVEMODELVIEW_H
