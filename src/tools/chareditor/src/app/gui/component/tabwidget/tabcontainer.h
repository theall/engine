#ifndef TABCONTAINER_H
#define TABCONTAINER_H

#include <QWidget>
#include <QGraphicsView>

#include "movemodelview/movemodelview.h"

class TTabContainer : public QWidget
{
public:
    TTabContainer(QWidget *parent=nullptr);
    ~TTabContainer();

    QGraphicsScene *frameScene();
    QGraphicsScene *moveModelScene();
    void setFrameScene(QGraphicsScene *scene);
    void setMoveModelScene(QGraphicsScene *scene);

    QGraphicsView *frameView() const;
    TMoveModelView *moveModelView() const;

private:
    QGraphicsView *mFrameView;
    TMoveModelView *mMoveModelView;
    QGraphicsView *mActionsView;
};

#endif // TABCONTAINER_H
