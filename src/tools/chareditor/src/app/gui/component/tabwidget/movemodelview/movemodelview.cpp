#include "movemodelview.h"

#include <QWheelEvent>

TMoveModelView::TMoveModelView(QWidget *parent) :
    QGraphicsView(parent)
{
    setFrameShape(QFrame::Panel);
    setFrameShadow(QFrame::Sunken);

    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
}

TMoveModelView::~TMoveModelView()
{

}

void TMoveModelView::locatePosition(const QPoint &pos)
{
    Q_UNUSED(pos);
    // Place the last known mouse scene pos below the mouse again
//    QWidget *viewPortWidget = viewport();
//    QPointF viewCenterScenePos = mapToScene(viewPortWidget->rect().center());
//    QPointF mouseScenePos = mapToScene(viewPortWidget->mapFromGlobal(pos));
//    QPointF diff = viewCenterScenePos - mouseScenePos;
//    centerOn(mLastMouseScenePos + diff);
}

void TMoveModelView::wheelEvent(QWheelEvent *event)
{
    int delta = event->angleDelta().y();
    if (event->modifiers()&Qt::ControlModifier and delta!=0)
    {
        if(delta > 0)
            scale(1.5, 1.5);
        else
            scale(0.5, 0.5);
        return;
    }
    QGraphicsView::wheelEvent(event);
}
