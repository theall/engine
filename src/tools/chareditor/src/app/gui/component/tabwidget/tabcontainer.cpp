#include "tabcontainer.h"

#include <QVBoxLayout>
#include <QSplitter>

#include "../../../utils/preferences.h"

TTabContainer::TTabContainer(QWidget *parent) :
    QWidget(parent)
  , mFrameView(new QGraphicsView)
  , mMoveModelView(new TMoveModelView)
{
    mFrameView->setFrameShape(QFrame::Panel);
    mFrameView->setFrameShadow(QFrame::Sunken);
    mFrameView->setRenderHint(QPainter::SmoothPixmapTransform, true);
    mFrameView->setRenderHint(QPainter::SmoothPixmapTransform, true);

    QVBoxLayout *vboxLayout = new QVBoxLayout;
    QSplitter *splitter = new QSplitter;
    splitter->addWidget(mFrameView);
    splitter->addWidget(mMoveModelView);
    vboxLayout->addWidget(splitter);
    setLayout(vboxLayout);

    TPreferences *prefs = TPreferences::instance();
    qreal frameSceneScale = prefs->frameSceneScale();
    qreal moveSceneScale = prefs->moveSceneScale();
    mFrameView->setTransform(QTransform::fromScale(frameSceneScale, frameSceneScale));
    mMoveModelView->setTransform(QTransform::fromScale(moveSceneScale, moveSceneScale));
}

TTabContainer::~TTabContainer()
{

}

QGraphicsScene *TTabContainer::frameScene()
{
    return mFrameView->scene();
}

QGraphicsScene *TTabContainer::moveModelScene()
{
    return mMoveModelView->scene();
}

void TTabContainer::setFrameScene(QGraphicsScene *scene)
{
    if(!scene)
        return;

    mFrameView->setScene(scene);
    //mFrameView->centerOn(scene->sceneRect().center());
}

void TTabContainer::setMoveModelScene(QGraphicsScene *scene)
{
    if(!scene)
        return;

    mMoveModelView->setScene(scene);
    mMoveModelView->centerOn(scene->sceneRect().center());
}

QGraphicsView *TTabContainer::frameView() const
{
    return mFrameView;
}

TMoveModelView *TTabContainer::moveModelView() const
{
    return mMoveModelView;
}
