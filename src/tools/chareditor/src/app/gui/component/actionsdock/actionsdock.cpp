#include "actionsdock.h"

#include <QVBoxLayout>
#include <QFileDialog>

TActionsDock::TActionsDock(QWidget *parent) :
    TBaseDock(QLatin1String("ActionsDock"), parent)
  , mContainer(new TActionsContainer(this))
{
//    CREATE_ACTION(mActionAdd, ":actionsdock/images/add.png", slotActionAddTriggered);
//    CREATE_ACTION(mActionRemove, ":actionsdock/images/remove.png", slotActionRemoveTriggered);

//    QToolBar *toolBar = new QToolBar(this);
//    toolBar->setFloatable(false);
//    toolBar->setMovable(false);
//    toolBar->setIconSize(QSize(16, 16));
//    toolBar->addAction(mActionAdd);
//    toolBar->addAction(mActionRemove);

    setWidget(mContainer);

    retranslateUi();
}

TActionsDock::~TActionsDock()
{

}

void TActionsDock::slotActionAddTriggered()
{
    emit requestAddAction();
}

void TActionsDock::slotActionRemoveTriggered()
{

}

void TActionsDock::retranslateUi()
{
    setWindowTitle(tr("Actions"));
    setToolTip(tr("Actions list."));
//    mActionAdd->setToolTip(tr("Add action."));
//    mActionAdd->setText(tr("Add"));
//    mActionRemove->setToolTip(tr("Remove action."));
//    mActionRemove->setText(tr("Del"));
}

