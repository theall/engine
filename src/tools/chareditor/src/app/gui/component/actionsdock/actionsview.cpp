#include "actionsview.h"

#include <QHeaderView>
#include <QMouseEvent>

TActionsView::TActionsView(QWidget *parent) :
    QTableView(parent)
  , mContextMenu(new QMenu(this))
{
    setObjectName(QStringLiteral("actionsView"));
    setFrameShape(QFrame::Panel);
    setFrameShadow(QFrame::Sunken);

    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);

    horizontalHeader()->setStretchLastSection(true);
    horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    horizontalHeader()->setVisible(false);
    verticalHeader()->setVisible(false);

    setSortingEnabled(false);
    setShowGrid(true);

    mActionRename = mContextMenu->addAction(QString(), this, SLOT(slotActionRenameTriggered()));
    mActionClone = mContextMenu->addAction(QString(), this, SLOT(slotActionCloneTriggered()));
    mActionCopy = mContextMenu->addAction(QString(), this, SLOT(slotActionCopyTriggered()));
    mActionRemove = mContextMenu->addAction(QString(), this, SLOT(slotActionRemoveTriggered()));
    mContextMenu->addSeparator();
    mActionShowGrid = mContextMenu->addAction(QString(), this, SLOT(slotActionShowGridTriggered(bool)));
    mActionHideColumn = mContextMenu->addAction(QString(), this, SLOT(slotActionHideColumnTriggered(bool)));

    mActionShowGrid->setCheckable(true);
    mActionHideColumn->setCheckable(true);
    retranslateUi();
}

TActionsView::~TActionsView()
{

}

QList<int> TActionsView::getSelectedIndexes()
{
    QSet<int> selectedRows;
    for(QModelIndex index : selectionModel()->selectedIndexes())
    {
        selectedRows.insert(index.row());
    }
    return selectedRows.toList();
}

int TActionsView::currentRow()
{
    return currentIndex().row();
}

void TActionsView::selectRow(int row)
{
    if(model())
    {
        QTableView::selectRow(row);
        emit rowSelected(row);
    }
}

int TActionsView::rowCount()
{
    QAbstractItemModel *m = model();
    return m?m->rowCount():0;
}

void TActionsView::slotActionRemoveTriggered()
{

}

void TActionsView::slotActionShowGridTriggered(bool checked)
{
    setShowGrid(checked);
}

void TActionsView::slotActionHideColumnTriggered(bool checked)
{
    if(checked)
        horizontalHeader()->hideSection(0);
    else
        horizontalHeader()->showSection(0);
}

void TActionsView::slotActionCloneTriggered()
{

}

void TActionsView::slotActionCopyTriggered()
{

}

void TActionsView::slotActionRenameTriggered()
{
    edit(currentIndex());
}

void TActionsView::retranslateUi()
{
    mActionHideColumn->setText(tr("Hide column"));
    mActionRename->setText(tr("Rename"));
    mActionCopy->setText(tr("Copy"));
    mActionRemove->setText(tr("Remove"));
    mActionClone->setText(tr("Clone"));
    mActionShowGrid->setText(tr("Show grid"));
}

void TActionsView::setModel(QAbstractItemModel *model)
{
    QTableView::setModel(model);
}

void TActionsView::mousePressEvent(QMouseEvent *event)
{
    QTableView::mousePressEvent(event);
    emit rowSelected(indexAt(event->pos()).row());
}

void TActionsView::contextMenuEvent(QContextMenuEvent *event)
{
    mContextMenu->popup(event->globalPos());
}
