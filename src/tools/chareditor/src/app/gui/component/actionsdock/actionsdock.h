#ifndef ACTIONSDOCK_H
#define ACTIONSDOCK_H

#include <QAction>
#include <QToolBar>

#include "../basedock.h"
#include "actionscontainer.h"

class TActionsDock : public TBaseDock
{
    Q_OBJECT

public:
    TActionsDock(QWidget *parent = nullptr);
    ~TActionsDock();

signals:
    void requestAddAction();
    void requestRemoveAction();

private slots:
    void slotActionAddTriggered();
    void slotActionRemoveTriggered();

private:
    TActionsContainer *mContainer;
    QAction *mActionAdd;
    QAction *mActionRemove;

    // TBaseDock interface
public:
    void retranslateUi() Q_DECL_OVERRIDE;
};
#endif // ACTIONSDOCK_H
