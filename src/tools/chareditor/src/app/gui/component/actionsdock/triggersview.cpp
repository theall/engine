#include "triggersview.h"

#include <QHeaderView>
#include <QMouseEvent>

TTriggersView::TTriggersView(QWidget *parent) :
    QTableView(parent)
{
    setObjectName(QStringLiteral("triggersView"));
    setFrameShape(QFrame::Panel);
    setFrameShadow(QFrame::Sunken);

    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);

    horizontalHeader()->setStretchLastSection(true);
    horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
    horizontalHeader()->setFrameShape(QFrame::Panel);
    horizontalHeader()->setFrameShadow(QFrame::Sunken);

    verticalHeader()->setFrameShape(QFrame::Panel);
    verticalHeader()->setFrameShadow(QFrame::Sunken);

    setSortingEnabled(false);
    setShowGrid(true);
}

TTriggersView::~TTriggersView()
{

}

void TTriggersView::mouseDoubleClickEvent(QMouseEvent *event)
{
    QModelIndex itemIndex = indexAt(event->pos());
    if(itemIndex.isValid())
    {
        int row = itemIndex.row();
        emit requestEditRow(row);
    }
}
