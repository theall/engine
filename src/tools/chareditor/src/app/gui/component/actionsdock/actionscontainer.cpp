#include "actionscontainer.h"
#include "actionsview.h"
#include "animationview.h"
#include "triggersview.h"
#include "../../dialogs/newactiondialog.h"

#include <QToolBar>
#include <QSplitter>
#include <QVBoxLayout>
#include <QHBoxLayout>

#define CREATE_ACTION(action,image,func) \
    action = toolbar->addAction(QString());\
    action->setIcon(QIcon(image));\
    connect(action, SIGNAL(triggered()), this, SLOT(func()))

TActionsContainer::TActionsContainer(QWidget *parent) :
    QWidget(parent)
{
    setObjectName(QStringLiteral("ActionsContainer"));

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    QVBoxLayout *verticalLayout = new QVBoxLayout(this);
    verticalLayout->setSpacing(1);
    verticalLayout->setMargin(3);
    QHBoxLayout *horizontalLayout = new QHBoxLayout();
    horizontalLayout->setSpacing(0);

    QToolBar *toolbar = new QToolBar(this);
    CREATE_ACTION(mBtnNewAction,":/toolbar/images/new.png",slotNewActionTriggered);
    CREATE_ACTION(mBtnCopyAction,":/toolbar/images/copy.png",slotCopyActionsTriggered);
    CREATE_ACTION(mBtnRemoveAction,":/actionsdock/images/remove.png",slotRemoveActionTriggered);
    CREATE_ACTION(mBtnPlay,":/actionsdock/images/play.png",slotPlayTriggered);
    CREATE_ACTION(mBtnStop,":/actionsdock/images/stop_play.png",slotStopTriggered);
    toolbar->addSeparator();
    CREATE_ACTION(mBtnAddFrame,":/framesdock/images/newimage.png",slotAddFrameTriggered);
    CREATE_ACTION(mBtnRemoveFrame,":/framesdock/images/removeimage.png",slotRemoveFrameTriggered);
    CREATE_ACTION(mBtnMoveUp,":/framesdock/images/up.png",slotMoveUpTriggered);
    CREATE_ACTION(mBtnMoveDown,":/framesdock/images/down.png",slotMoveDownTriggered);
    CREATE_ACTION(mBtnMoveLeft,":/framesdock/images/left.png",slotMoveLeftTriggered);
    CREATE_ACTION(mBtnMoveRight,":/framesdock/images/right.png",slotMoveRightTriggered);
    toolbar->addSeparator();
    CREATE_ACTION(mBtnNewTrigger,":/toolbar/images/new.png",slotNewActionTriggerTriggered);
    CREATE_ACTION(mBtnRemoveTrigger,":/actionsdock/images/remove.png",slotRemoveActionTriggerTriggered);

    horizontalLayout->addWidget(toolbar);

    mSbFps = new QSpinBox(this);
    mSbFps->setMinimumSize(QSize(40, 24));
    mSbFps->setMaximumSize(QSize(40, 24));
    mSbFps->setMinimum(1);
    mSbFps->setMaximum(120);
    mSbFps->setValue(60);
    connect(mSbFps, SIGNAL(valueChanged(int)), this, SIGNAL(requestAdjustFPS(int)));
    horizontalLayout->addWidget(mSbFps);

    QSpacerItem *horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    horizontalLayout->addItem(horizontalSpacer);
    verticalLayout->addLayout(horizontalLayout);

    mActionsView = new TActionsView(this);
    mAnimationView = new TAnimationView(this);
    mTriggersView = new TTriggersView(this);

    QSplitter *verticalSplitter = new QSplitter(this);
    verticalSplitter->setOrientation(Qt::Vertical);
    verticalSplitter->setOpaqueResize(true);
    verticalSplitter->setHandleWidth(2);
    verticalSplitter->setChildrenCollapsible(true);
    verticalSplitter->addWidget(mAnimationView);
    verticalSplitter->addWidget(mTriggersView);
    verticalSplitter->setStretchFactor(0, 5);
    verticalSplitter->setStretchFactor(1, 5);

    QSplitter *horrizontalSplitter = new QSplitter(this);
    horrizontalSplitter->setOrientation(Qt::Horizontal);
    horrizontalSplitter->setOpaqueResize(true);
    horrizontalSplitter->setHandleWidth(2);
    horrizontalSplitter->setChildrenCollapsible(true);
    horrizontalSplitter->addWidget(mActionsView);
    horrizontalSplitter->addWidget(verticalSplitter);
    horrizontalSplitter->setStretchFactor(0, 2);
    horrizontalSplitter->setStretchFactor(1, 8);

    verticalLayout->addWidget(horrizontalSplitter);
    verticalLayout->setStretch(0, 1);
    verticalLayout->setStretch(1, 9);

    connect(mAnimationView, SIGNAL(requestRemoveFrames(QList<int>)), this, SIGNAL(requestRemoveFrames(QList<int>)));
    connect(mAnimationView, SIGNAL(requestCopyFrames(QList<int>)), this, SIGNAL(requestCopyFrames(QList<int>)));
    connect(mAnimationView, SIGNAL(requestCloneFrames(QList<int>)), this, SIGNAL(requestCloneFrames(QList<int>)));
    connect(mAnimationView, SIGNAL(requestPasteFrames(int)), this, SIGNAL(requestPasteFrames(int)));
    connect(mTriggersView, SIGNAL(requestEditRow(int)), this, SLOT(slotRequestEditActionTrigger(int)));

    retranslateUi();
}

TActionsContainer::~TActionsContainer()
{

}

TActionsView *TActionsContainer::actionsView() const
{
    return mActionsView;
}

void TActionsContainer::slotPlayTriggered()
{
    emit requestPlayAction();
}

void TActionsContainer::slotCopyActionsTriggered()
{

}

void TActionsContainer::slotStopTriggered()
{
    emit requestStopPlayAction();
}

void TActionsContainer::slotRemoveActionTriggered()
{
    QList<int> selectedRows = mActionsView->getSelectedIndexes();
    if(selectedRows.size() > 0) {
        emit requestRemoveActions(selectedRows);

        int currentRow = mActionsView->currentRow();
        int rowCount = mActionsView->rowCount();
        if(currentRow == -1)
            currentRow = 0;
        else if(currentRow >= rowCount)
            currentRow = rowCount - 1;
        mActionsView->selectRow(currentRow);
    }
}

void TActionsContainer::slotNewActionTriggered()
{
    TNewActionDialog dlg(mActionsView);
    if(dlg.exec()==QDialog::Rejected)
        return;

    // Select new action added if succeeded.
    int rowCount = mActionsView->rowCount();
    emit requestAddNewAction(dlg.getActionName());
    if((rowCount+1)==mActionsView->rowCount())
        mActionsView->selectRow(rowCount);
}

void TActionsContainer::slotAddFrameTriggered()
{
    QStringList fileList;
    emit getSelectedImages(fileList);
    if(fileList.size() > 0)
        emit requestAddFrames(fileList, -1);
}

void TActionsContainer::slotRemoveFrameTriggered()
{
    QList<int> selectedIndex = mAnimationView->getSelectedIndexes();
    if(selectedIndex.size() > 0)
        emit requestRemoveFrames(selectedIndex);
}

void TActionsContainer::slotCopyFramesTriggered()
{
    QList<int> selectedIndex = mAnimationView->getSelectedIndexes();
    if(selectedIndex.size() > 0)
        emit requestCopyFrames(selectedIndex);
}

void TActionsContainer::slotPasteFramesTriggered()
{

}

void TActionsContainer::slotMoveUpTriggered()
{

}

void TActionsContainer::slotMoveDownTriggered()
{

}

void TActionsContainer::slotMoveLeftTriggered()
{

}

void TActionsContainer::slotMoveRightTriggered()
{

}

void TActionsContainer::slotNewActionTriggerTriggered()
{
    emit requestCreateActionTrigger();
}

void TActionsContainer::slotRemoveActionTriggerTriggered()
{
    QSet<int> rows;
    for(QModelIndex modelIndex : mTriggersView->selectionModel()->selectedRows())
    {
        rows.insert(modelIndex.row());
    }
    emit requestRemoveActionTrigger(rows.toList());
}

TAnimationView *TActionsContainer::animationView() const
{
    return mAnimationView;
}

TTriggersView *TActionsContainer::triggersView() const
{
    return mTriggersView;
}

void TActionsContainer::setFPS(int fps)
{
    mSbFps->blockSignals(true);
    mSbFps->setValue(fps);
    mSbFps->blockSignals(false);
}

void TActionsContainer::slotRequestEditActionTrigger(int row)
{
    emit requestModifyActionTrigger(row);
}

void TActionsContainer::retranslateUi()
{

}
