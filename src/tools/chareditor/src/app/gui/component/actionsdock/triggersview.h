#ifndef TRIGGERSVIEW_H
#define TRIGGERSVIEW_H

#include <QTableView>

class TTriggersView : public QTableView
{
    Q_OBJECT

public:
    TTriggersView(QWidget *parent=nullptr);
    ~TTriggersView();

signals:
    // To parent widget
    void requestEditRow(int row);

private:

    // QWidget interface
protected:
    void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
};

#endif // TRIGGERSVIEW_H
