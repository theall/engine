#ifndef ACTIONSCONTAINER_H
#define ACTIONSCONTAINER_H

#include <QWidget>
#include <QSpinBox>
#include <QAction>

class TActionsView;
class TAnimationView;
class TTriggersView;

class TActionsContainer : public QWidget
{
    Q_OBJECT

public:
    explicit TActionsContainer(QWidget *parent = 0);
    ~TActionsContainer();

    TActionsView *actionsView() const;
    TAnimationView *animationView() const;
    TTriggersView *triggersView() const;

    void setFPS(int fps);

signals:
    // Controller
    void requestAddNewAction(const QString &name);
    void requestRemoveActions(const QList<int> &rows);
    void requestAddFrames(const QStringList &fileList, int pos);
    void requestRemoveFrames(const QList<int> &indexes);
    void requestCopyFrames(const QList<int> &indexes);
    void requestCloneFrames(const QList<int> &indexes);
    void requestPasteFrames(int pos);
    void requestPlayAction();
    void requestStopPlayAction();
    void requestAdjustFPS(int fps);
    void requestCreateActionTrigger();
    void requestRemoveActionTrigger(QList<int> rows);
    void requestModifyActionTrigger(int row);

    // Main window
    void getSelectedImages(QStringList &fileList);

private slots:
    // Trigger view
    void slotRequestEditActionTrigger(int row);

    void slotPlayTriggered();
    void slotCopyActionsTriggered();
    void slotStopTriggered();
    void slotRemoveActionTriggered();
    void slotNewActionTriggered();
    void slotAddFrameTriggered();
    void slotRemoveFrameTriggered();
    void slotCopyFramesTriggered();
    void slotPasteFramesTriggered();
    void slotMoveUpTriggered();
    void slotMoveDownTriggered();
    void slotMoveLeftTriggered();
    void slotMoveRightTriggered();
    void slotNewActionTriggerTriggered();
    void slotRemoveActionTriggerTriggered();

private:
    QAction *mBtnNewAction;
    QAction *mBtnPlay;
    QAction *mBtnCopyAction;
    QAction *mBtnStop;
    QAction *mBtnRemoveAction;
    QAction *mBtnAddFrame;
    QAction *mBtnRemoveFrame;
    QAction *mBtnMoveUp;
    QAction *mBtnMoveDown;
    QAction *mBtnMoveLeft;
    QAction *mBtnMoveRight;
    QAction *mBtnNewTrigger;
    QAction *mBtnRemoveTrigger;

    QSpinBox *mSbFps;
    TActionsView *mActionsView;
    TAnimationView *mAnimationView;
    TTriggersView *mTriggersView;

    void retranslateUi();
};

#endif // ACTIONSCONTAINER_H
