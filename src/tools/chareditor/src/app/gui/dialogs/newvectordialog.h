#ifndef NEWVECTORDIALOG_H
#define NEWVECTORDIALOG_H

#include "abstractdialog.h"

namespace Ui {
class TNewVectorDialog;
}

class TNewVectorDialog : public TAbstractDialog
{
    Q_OBJECT

public:
    explicit TNewVectorDialog(QWidget *parent = 0);
    ~TNewVectorDialog();

    QPointF getVector() const;
    int getCount() const;

private slots:
    void on_btnBox_rejected();
    void on_btnBox_accepted();

private:
    Ui::TNewVectorDialog *ui;

    // TAbstractDialog interface
private:
    void retranslateUi() Q_DECL_OVERRIDE;
};

#endif // NEWVECTORDIALOG_H
