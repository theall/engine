#include "newactiondialog.h"

#include <QShowEvent>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDialogButtonBox>

TNewActionDialog::TNewActionDialog(QWidget *parent) :
    TAbstractDialog(parent)
  , mLabel(new QLabel(this))
  , mEdit(new QLineEdit(this))
{
    Qt::WindowFlags flags = windowFlags();
    flags ^= Qt::WindowContextHelpButtonHint;
    flags |= Qt::MSWindowsFixedSizeDialogHint;
    setWindowFlags(flags);

    QHBoxLayout *hlayout = new QHBoxLayout;
    hlayout->addWidget(mLabel);
    hlayout->addWidget(mEdit);

    QVBoxLayout *vlayout = new QVBoxLayout;
    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal);
    QPushButton *btnOk = buttonBox->button(QDialogButtonBox::Ok);
    btnOk->setDefault(true);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(slotOkClicked()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    vlayout->addLayout(hlayout);
    vlayout->addWidget(buttonBox);
    setLayout(vlayout);

    retranslateUi();
}

TNewActionDialog::~TNewActionDialog()
{

}

QString TNewActionDialog::getActionName() const
{
    return mEdit->text();
}

void TNewActionDialog::slotOkClicked()
{
    if(mEdit->text().isEmpty())
    {
        mEdit->setFocus();
        return;
    }
    accept();
}

void TNewActionDialog::retranslateUi()
{
    mLabel->setText(tr("Name:"));
    mEdit->setPlaceholderText(tr("Input action name."));
    mEdit->setText(tr("New Action"));
}

void TNewActionDialog::showEvent(QShowEvent *event)
{
    event->accept();
    mEdit->selectAll();
}
