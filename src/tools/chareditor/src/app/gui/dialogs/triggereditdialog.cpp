#include "triggereditdialog.h"
#include "ui_triggereditdialog.h"

#include "keyeditdialog.h"

TTriggerEditDialog::TTriggerEditDialog(QWidget *parent) :
    TAbstractDialog(parent),
    ui(new Ui::TTriggerEditDialog)
{
    ui->setupUi(this);
    ui->btnEditInput->setVisible(false);
}

TTriggerEditDialog::~TTriggerEditDialog()
{
    delete ui;
}

int TTriggerEditDialog::getEvent() const
{
    return ui->cbbEvent->itemData(ui->cbbEvent->currentIndex()).toInt();
}

void TTriggerEditDialog::setEvent(int event)
{
    ui->cbbEvent->setCurrentIndex(event);
}

void TTriggerEditDialog::setEvent(const QString &event)
{
    int textIndex = ui->cbbEvent->findText(event);
    if(textIndex==-1)
        textIndex = 0;
    ui->cbbEvent->setCurrentIndex(textIndex);
}

QString TTriggerEditDialog::getValue() const
{
    return ui->leValue->text();
}

void TTriggerEditDialog::setValue(const QString &value)
{
    ui->leValue->setText(value);
}

QComboBox *TTriggerEditDialog::eventComboBox() const
{
    return ui->cbbEvent;
}

void TTriggerEditDialog::setKeyEditButtonVisible(bool visible)
{
    ui->btnEditInput->setVisible(visible);
}

void TTriggerEditDialog::on_btnEditInput_clicked()
{
    TKeyEditDialog dialog(this);
    dialog.setCommandList(ui->leValue->text());
    if(dialog.exec()==QDialog::Accepted)
    {
        ui->leValue->setText(dialog.getCommandList());
    }
}

void TTriggerEditDialog::retranslateUi()
{
    ui->retranslateUi(this);
}
