#include "pixmapresourcedialog.h"
#include "ui_pixmapresourcedialog.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QStandardItemModel>

TPixmapResourceSet::TPixmapResourceSet()
{

}

TPixmapResourceSet::~TPixmapResourceSet()
{

}

void TPixmapResourceSet::add(const TPixmapResourceData &resourceData)
{
    mDataList.append(resourceData);
}

void TPixmapResourceSet::add(const QPixmap &icon, const QString text, const QString &toolTip)
{
    TPixmapResourceData data;
    data.icon = icon;
    data.text = text;
    data.toolTip = toolTip;
    mDataList.append(data);
}

TPixmapResourceDataList TPixmapResourceSet::dataList() const
{
    return mDataList;
}

QString TPixmapResourceSet::resourceRoot() const
{
    return mResourceRoot;
}

void TPixmapResourceSet::setResourceRoot(const QString &resourceRoot)
{
    mResourceRoot = resourceRoot;
}

void TPixmapResourceSet::operator =(const TPixmapResourceSet &resourceSet)
{
    mResourceRoot = resourceSet.resourceRoot();
    mDataList = resourceSet.dataList();
}

TPixmapResourceDialog::TPixmapResourceDialog(QWidget *parent) :
    TAbstractDialog(parent, true),
    ui(new Ui::TPixmapResourceDialog)
{
    ui->setupUi(this);
    ui->lwImage->setIconSize(QSize(64, 64));
    ui->lwImage->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

TPixmapResourceDialog::~TPixmapResourceDialog()
{

}

QStringList TPixmapResourceDialog::getSelectedImages()
{
    QStringList sl;
    ui->lwImage->setSelectionMode(QAbstractItemView::ExtendedSelection);
    if(exec() == QDialog::Accepted)
    {
        QStandardItemModel *itemModel = qobject_cast<QStandardItemModel*>(ui->lwImage->model());
        if(itemModel)
        {
            for(QModelIndex i : ui->lwImage->selectionModel()->selectedRows())
            {
                sl.append(itemModel->data(i).toString());
            }
        }
    }
    return sl;
}

QStandardItem *TPixmapResourceDialog::getSelectedItem()
{
    ui->lwImage->setSelectionMode(QAbstractItemView::SingleSelection);
    if(exec() == QDialog::Accepted)
    {
        QStandardItemModel *itemModel = qobject_cast<QStandardItemModel*>(ui->lwImage->model());
        if(itemModel)
        {
            for(QModelIndex i : ui->lwImage->selectionModel()->selectedRows())
            {
                return itemModel->item(i.row());
                break;
            }
        }
    }
    return nullptr;
}

void TPixmapResourceDialog::setResourceSet(const TPixmapResourceSet &resourceSet)
{
    ui->leFilter->clear();
    mResourceDir.setPath(resourceSet.resourceRoot());
    mResourceSetBackup = resourceSet;

    QStandardItemModel *itemModel = new QStandardItemModel(this);
    for(TPixmapResourceData &data : resourceSet.dataList())
    {
        QStandardItem *item = new QStandardItem(QIcon(data.icon), data.text);
        if(!data.toolTip.isEmpty())
            item->setToolTip(data.toolTip);
        else
            item->setToolTip(data.text);
        itemModel->appendRow(item);
    }
    setListViewModel(itemModel);
}

void TPixmapResourceDialog::clear()
{
    QStandardItemModel *itemModel = qobject_cast<QStandardItemModel*>(ui->lwImage->model());
    if(itemModel)
        itemModel->clear();
    ui->leFilter->clear();
}

void TPixmapResourceDialog::setMultiSelectionEnabled(bool enabled)
{
    ui->lwImage->setSelectionMode(enabled?QAbstractItemView::ExtendedSelection:QAbstractItemView::SingleSelection);
}

void TPixmapResourceDialog::on_btnImport_clicked()
{
    hide();

    QStringList fileNames = QFileDialog::getOpenFileNames(
                this,
                tr("Import images"),
                "",
                tr("Images (*.bmp *.png *.jpg)"));
    if(fileNames.size()>0)
    {
        QString resourcePath = mResourceDir.absolutePath();
        bool overWriteAll = false;
        for(QString fileName : fileNames)
        {
            QFileInfo fi(fileName);
            if(fi.absolutePath()==resourcePath)
                continue;
            QString imageFileName = fi.fileName();
            QString destFileName = mResourceDir.absoluteFilePath(imageFileName);
            bool fileExists = QFile::exists(destFileName);
            if(!overWriteAll && fileExists)
            {
                int code = QMessageBox::question(
                            this, tr("Question."),
                            tr("Destination file \"%1\" already exists, do you want to overwrite?").arg(imageFileName),
                            QMessageBox::YesToAll|QMessageBox::Yes|QMessageBox::No|QMessageBox::NoToAll);
                if(code == QMessageBox::NoToAll) {
                    break;
                } else if(code == QMessageBox::No) {
                    continue;
                } else if(code == QMessageBox::YesToAll) {
                    overWriteAll = true;
                }
            }
            if(fileExists)
                QFile::remove(destFileName);
            QFile::copy(fileName, destFileName);
        }
    }

    show();
}

void TPixmapResourceDialog::on_leFilter_textChanged(const QString &filter)
{
    bool matchAll = filter.isEmpty();
    QStandardItemModel *itemModel = new QStandardItemModel(this);
    for(TPixmapResourceData &data : mResourceSetBackup.dataList())
    {
        if(!matchAll && !data.text.contains(filter, Qt::CaseInsensitive))
            continue;

        QStandardItem *item = new QStandardItem(QIcon(data.icon), data.text);
        if(!data.toolTip.isEmpty())
            item->setToolTip(data.toolTip);
        else
            item->setToolTip(data.text);
        itemModel->appendRow(item);
    }
    setListViewModel(itemModel);
}

void TPixmapResourceDialog::on_btnOk_clicked()
{
    QStringList selectedImages = getSelectedImages();
    if(selectedImages.size() > 0)
        emit imagesChoosed(selectedImages);
    accept();
}

void TPixmapResourceDialog::on_btnCancel_clicked()
{
    reject();
}

void TPixmapResourceDialog::on_lwImage_doubleClicked(const QModelIndex &index)
{
    Q_UNUSED(index);

    on_btnOk_clicked();
}

void TPixmapResourceDialog::retranslateUi()
{
    ui->retranslateUi(this);
}

void TPixmapResourceDialog::on_btnPictureMode_clicked(bool checked)
{
    ui->lwImage->setViewMode(checked?QListView::IconMode:QListView::ListMode);
}

void TPixmapResourceDialog::slotSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    Q_UNUSED(selected);
    Q_UNUSED(deselected);

    QItemSelectionModel *sm = ui->lwImage->selectionModel();
    ui->btnOk->setEnabled(sm && !sm->selectedIndexes().empty());
}

void TPixmapResourceDialog::setListViewModel(QStandardItemModel *model)
{
    if(ui->lwImage->selectionModel())
        ui->lwImage->selectionModel()->disconnect(this);
    ui->lwImage->setModel(model);
    connect(ui->lwImage->selectionModel(),
            SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
            this,
            SLOT(slotSelectionChanged(QItemSelection,QItemSelection)));
}

