#include "keyeditdialog.h"
#include "ui_keyeditdialog.h"

TKeyEditDialog::TKeyEditDialog(QWidget *parent) :
    TAbstractDialog(parent),
    ui(new Ui::TKeyEditDialog)
{
    ui->setupUi(this);

    ui->btnRemoveOne->setEnabled(false);
    ui->btnBackspace->setEnabled(false);
    ui->btnClear->setEnabled(false);

    connect(ui->leCommand,
            SIGNAL(codeChanged(QString,int)),
            this,
            SLOT(slotCommandChanged(QString,int)));
}

TKeyEditDialog::~TKeyEditDialog()
{
    delete ui;
}

void TKeyEditDialog::setCommandList(const QString &commandList)
{
    ui->leCommand->setText(commandList);
}

QString TKeyEditDialog::getCommandList() const
{
    return ui->leCommand->text();
}

void TKeyEditDialog::slotCommandChanged(const QString &text, int lastCode)
{
    bool hasCode = !text.isEmpty();
    bool andMode = ui->btnAnd->isChecked();

    ui->btnA->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::A)));
    ui->btnB->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::B)));
    ui->btnC->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::C)));
    ui->btnX->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::X)));
    ui->btnY->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::Y)));
    ui->btnZ->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::Z)));
    ui->btnL1->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::L1)));
    ui->btnL2->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::L2)));
    ui->btnL3->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::L3)));
    ui->btnR1->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::R1)));
    ui->btnR2->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::R2)));
    ui->btnR3->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::R3)));
    ui->btnUp->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::UP)));
    ui->btnDown->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::DOWN)));
    ui->btnLeft->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::LEFT)));
    ui->btnRight->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::RIGHT)));
    ui->btnLeftUp->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::LEFT_UP)));
    ui->btnUpRight->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::UP_RIGHT)));
    ui->btnLeftDown->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::DOWN_LEFT)));
    ui->btnRightDown->setEnabled(!(andMode&&(lastCode&TKeyCodeEdit::RIGHT_DOWN)));
    ui->btnHold->setEnabled((!text.isEmpty())&&text.right(1)!="~");
    ui->btnRemoveOne->setEnabled(hasCode);
    ui->btnBackspace->setEnabled(hasCode);
    ui->btnClear->setEnabled(hasCode);
}

void TKeyEditDialog::on_btnA_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::A);
}

void TKeyEditDialog::on_btnB_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::B);
}

void TKeyEditDialog::on_btnC_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::C);
}

void TKeyEditDialog::on_btnX_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::X);
}

void TKeyEditDialog::on_btnY_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::Y);
}

void TKeyEditDialog::on_btnZ_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::Z);
}

void TKeyEditDialog::on_btnL1_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::L1);
}

void TKeyEditDialog::on_btnL2_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::L2);
}

void TKeyEditDialog::on_btnL3_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::L3);
}

void TKeyEditDialog::on_btnOk_clicked()
{
    accept();
}

void TKeyEditDialog::on_btnR1_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::R1);
}

void TKeyEditDialog::on_btnR2_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::R2);
}

void TKeyEditDialog::on_btnR3_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::R3);
}

void TKeyEditDialog::on_btnUp_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::UP);
}

void TKeyEditDialog::on_btnDown_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::DOWN);
}

void TKeyEditDialog::on_btnHold_clicked()
{
    ui->leCommand->setHold();
}

void TKeyEditDialog::on_btnLeft_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::LEFT);
}

void TKeyEditDialog::on_btnAnd_clicked(bool checked)
{
    ui->leCommand->setAndMode(checked);
}

void TKeyEditDialog::on_btnRight_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::RIGHT);
}

void TKeyEditDialog::on_btnCancel_clicked()
{
    reject();
}

void TKeyEditDialog::on_btnLeftUp_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::LEFT_UP);
}

void TKeyEditDialog::on_btnUpRight_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::UP_RIGHT);
}

void TKeyEditDialog::on_btnLeftDown_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::DOWN_LEFT);
}

void TKeyEditDialog::on_btnBackspace_clicked()
{
    ui->leCommand->removeLast();
}

void TKeyEditDialog::on_btnRemoveOne_clicked()
{
    ui->leCommand->popup();
}

void TKeyEditDialog::on_btnRightDown_clicked()
{
    ui->leCommand->add(TKeyCodeEdit::RIGHT_DOWN);
}

void TKeyEditDialog::on_btnClear_clicked()
{
    ui->leCommand->clear();
}

void TKeyEditDialog::retranslateUi()
{
    ui->retranslateUi(this);
}
