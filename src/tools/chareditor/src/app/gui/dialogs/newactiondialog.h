#ifndef NEWACTIONDIALOG_H
#define NEWACTIONDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>

#include "abstractdialog.h"

class TNewActionDialog : public TAbstractDialog
{
    Q_OBJECT

public:
    TNewActionDialog(QWidget *parent = nullptr);
    ~TNewActionDialog();

    QString getActionName() const;

private slots:
    void slotOkClicked();

private:
    QLabel *mLabel;
    QLineEdit *mEdit;

    // TAbstractDialog interface
private:
    void retranslateUi() Q_DECL_OVERRIDE;

    // QWidget interface
protected:
    void showEvent(QShowEvent *event) Q_DECL_OVERRIDE;
};

#endif // NEWACTIONDIALOG_H
