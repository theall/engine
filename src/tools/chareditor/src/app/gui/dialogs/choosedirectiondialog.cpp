#include "choosedirectiondialog.h"
#include "ui_choosedirectiondialog.h"

TChooseDirectionDialog::TChooseDirectionDialog(QWidget *parent) :
    TAbstractDialog(parent),
    ui(new Ui::TChooseDirectionDialog)
{
    ui->setupUi(this);
}

TChooseDirectionDialog::~TChooseDirectionDialog()
{
    delete ui;
}

QString TChooseDirectionDialog::getSelectedDirections() const
{
    return ui->btnGroupFrom->checkedButton()->text() + ui->btnGroupTo->checkedButton()->text();
}

void TChooseDirectionDialog::retranslateUi()
{

}
