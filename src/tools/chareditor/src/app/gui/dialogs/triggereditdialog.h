#ifndef TRIGGEREDITDIALOG_H
#define TRIGGEREDITDIALOG_H

#include "abstractdialog.h"

#include <QComboBox>
#include <QLineEdit>

namespace Ui {
class TTriggerEditDialog;
}

class TTriggerEditDialog : public TAbstractDialog
{
    Q_OBJECT

public:
    explicit TTriggerEditDialog(QWidget *parent = 0);
    ~TTriggerEditDialog();

    int getEvent() const;
    void setEvent(int event);
    void setEvent(const QString &event);

    QString getValue() const;
    void setValue(const QString &value);

    QComboBox *eventComboBox() const;

    void setKeyEditButtonVisible(bool visible);

signals:

private slots:
    void on_btnEditInput_clicked();

private:
    Ui::TTriggerEditDialog *ui;

    // TAbstractDialog interface
private:
    void retranslateUi() Q_DECL_OVERRIDE;
};

#endif // TRIGGEREDITDIALOG_H
