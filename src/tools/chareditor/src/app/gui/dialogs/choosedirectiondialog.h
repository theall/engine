#ifndef CHOOSEDIRECTIONDIALOG_H
#define CHOOSEDIRECTIONDIALOG_H

#include "abstractdialog.h"

namespace Ui {
class TChooseDirectionDialog;
}

class TChooseDirectionDialog : public TAbstractDialog
{
    Q_OBJECT

public:
    explicit TChooseDirectionDialog(QWidget *parent = 0);
    ~TChooseDirectionDialog();

    QString getSelectedDirections() const;

private:
    Ui::TChooseDirectionDialog *ui;

    // TAbstractDialog interface
private:
    void retranslateUi() Q_DECL_OVERRIDE;
};

#endif // CHOOSEDIRECTIONDIALOG_H
