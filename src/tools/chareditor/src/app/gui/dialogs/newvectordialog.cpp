#include "newvectordialog.h"
#include "ui_newvectordialog.h"

TNewVectorDialog::TNewVectorDialog(QWidget *parent) :
    TAbstractDialog(parent),
    ui(new Ui::TNewVectorDialog)
{
    ui->setupUi(this);
}

TNewVectorDialog::~TNewVectorDialog()
{
    delete ui;
}

QPointF TNewVectorDialog::getVector() const
{
    return QPointF(ui->sbX->value(), ui->sbY->value());
}

int TNewVectorDialog::getCount() const
{
    return ui->sbCount->value();
}

void TNewVectorDialog::on_btnBox_rejected()
{
    reject();
}

void TNewVectorDialog::on_btnBox_accepted()
{
    accept();
}

void TNewVectorDialog::retranslateUi()
{
    ui->retranslateUi(this);
}
