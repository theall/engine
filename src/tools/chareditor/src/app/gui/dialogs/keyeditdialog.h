#ifndef KEYEDITDIALOG_H
#define KEYEDITDIALOG_H

#include "abstractdialog.h"

namespace Ui {
class TKeyEditDialog;
}

class TKeyEditDialog : public TAbstractDialog
{
    Q_OBJECT

public:
    explicit TKeyEditDialog(QWidget *parent = 0);
    ~TKeyEditDialog();

    void setCommandList(const QString &commandList);
    QString getCommandList() const;

private slots:
    void slotCommandChanged(const QString &text, int lastCode);
    void on_btnA_clicked();
    void on_btnB_clicked();
    void on_btnC_clicked();
    void on_btnX_clicked();
    void on_btnY_clicked();
    void on_btnZ_clicked();
    void on_btnL1_clicked();
    void on_btnL2_clicked();
    void on_btnL3_clicked();
    void on_btnOk_clicked();
    void on_btnR1_clicked();
    void on_btnR2_clicked();
    void on_btnR3_clicked();
    void on_btnUp_clicked();
    void on_btnDown_clicked();
    void on_btnHold_clicked();
    void on_btnLeft_clicked();
    void on_btnAnd_clicked(bool checked);
    void on_btnRight_clicked();
    void on_btnCancel_clicked();
    void on_btnLeftUp_clicked();
    void on_btnUpRight_clicked();
    void on_btnLeftDown_clicked();
    void on_btnBackspace_clicked();
    void on_btnRemoveOne_clicked();
    void on_btnRightDown_clicked();
    void on_btnClear_clicked();

private:
    Ui::TKeyEditDialog *ui;

    // TAbstractDialog interface
private:
    void retranslateUi() Q_DECL_OVERRIDE;
};

#endif // KEYEDITDIALOG_H
