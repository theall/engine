#ifndef RESOURCEDIALOG_H
#define RESOURCEDIALOG_H

#include "abstractdialog.h"

#include <QDir>

class QStandardItem;
class QItemSelection;
class QStandardItemModel;

namespace Ui {
class TPixmapResourceDialog;
}

struct TPixmapResourceData
{
    QPixmap icon;
    QString text;
    QString toolTip;
    void operator =(const TPixmapResourceData &data)
    {
        icon = data.icon;
        text = data.text;
        toolTip = data.toolTip;
    }
};

typedef QList<TPixmapResourceData> TPixmapResourceDataList;

class TPixmapResourceSet
{
public:
    TPixmapResourceSet();
    ~TPixmapResourceSet();

    void add(const TPixmapResourceData &data);
    void add(const QPixmap &icon, const QString text, const QString &toolTip = QString());

    TPixmapResourceDataList dataList() const;

    QString resourceRoot() const;
    void setResourceRoot(const QString &resourceRoot);
    void operator =(const TPixmapResourceSet &resourceSet);

private:
    QString mResourceRoot;
    TPixmapResourceDataList mDataList;
};

class TPixmapResourceDialog : public TAbstractDialog
{
    Q_OBJECT

public:
    explicit TPixmapResourceDialog(QWidget *parent = 0);
    ~TPixmapResourceDialog();

    QStringList getSelectedImages();
    QStandardItem *getSelectedItem();
    void setResourceSet(const TPixmapResourceSet &resourceSet);
    void clear();
    void setMultiSelectionEnabled(bool enabled);

signals:
    void imagesChoosed(const QStringList &list);

private slots:
    void on_btnImport_clicked();
    void on_leFilter_textChanged(const QString &arg1);
    void on_btnOk_clicked();
    void on_btnCancel_clicked();
    void on_lwImage_doubleClicked(const QModelIndex &index);
    void on_btnPictureMode_clicked(bool checked);
    void slotSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    Ui::TPixmapResourceDialog *ui;
    QDir mResourceDir;
    TPixmapResourceSet mResourceSetBackup;

    void setListViewModel(QStandardItemModel *model);

    // TAbstractDialog interface
private:
    void retranslateUi() Q_DECL_OVERRIDE;
};

#endif // RESOURCEDIALOG_H
