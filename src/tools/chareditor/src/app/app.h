#ifndef TAPP_H
#define TAPP_H

#include <QApplication>

class TApp : public QApplication
{
    Q_OBJECT

public:
    TApp(int argc, char *argv[]);
    ~TApp();

    int start();

signals:
    void requestOpenProject(const QString &file);

private:

    // QObject interface
public:
    bool event(QEvent *event) Q_DECL_OVERRIDE;
};

#endif // TAPP_H
