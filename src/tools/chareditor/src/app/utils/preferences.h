#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QDate>
#include <QTime>
#include <QColor>
#include <QString>
#include <QVariant>
#include <QSettings>
#include <QByteArray>
#include <QStringList>

class TPreferences : public QObject
{
    Q_OBJECT

public:
    static const int MAX_RECENT_FILES;
    static const int ICON_SIZE_SMALL;
    static const int ICON_SIZE_MEDIUM;
    static const int ICON_SIZE_LARGE;

public:
    TPreferences(QObject *parent=0);

    ~TPreferences();

    static TPreferences *instance();
    static void deleteInstance();

    void windowGeometryState(QByteArray* g, QByteArray* s);
    void setWindowGeometryState(const QVariant &geometry, const QVariant &windowState);

    bool displayTrayIcon() const;
    void setDisplayTrayIcon(bool displayTrayIcon);

    bool alwaysOnTop() const;
    void setAlwaysOnTop(bool alwaysOnTop);

    int runCount() const;
    void setRunCount(int runCount);

    QString language() const;
    void setLanguage(const QString &language);

    QString lastOpenPath() const;
    void setLastOpenPath(const QString &lastOpenPath);

    QString lastOpenDir() const;
    void setLastOpenDir(const QString &lastOpenDir);

    void save();

    bool saveBeforeExit() const;
    void setSaveBeforeExit(bool saveBeforeExit);

    bool openLastFile() const;
    void setOpenLastFile(bool openLastFile);

    bool fullScreen() const;
    void setFullScreen(bool fullScreen);

    int toolbarIconSize() const;
    void setToolbarIconSize(int toolbarIconSize);

    QStringList recentFiles() const;
    void setRecentFiles(const QStringList &recentFiles);

    bool hideMenuBar() const;
    void setHideMenuBar(bool hideMenuBar);

    bool hideStatusBar() const;
    void setHideStatusBar(bool hideStatusBar);

    QStringList recentOpenedFiles() const;
    void setRecentOpenedFiles(const QStringList &recentOpenedFiles);

    QString lastActiveFile() const;
    void setLastActiveFile(const QString &lastActiveFile);

    QString style() const;
    void setStyle(const QString &style);

    qreal frameSceneScale() const;
    void setFrameSceneScale(const qreal &frameSceneScale);

    qreal moveSceneScale() const;
    void setMoveSceneScale(const qreal &moveSceneScale);

    QString enginePath() const;
    void setEnginePath(const QString &enginePath);

    bool enableDebugMultiInstances() const;
    void setEnableDebugMultiInstances(bool enableDebugMultiInstances);

signals:
    void hideMenuBarChanged(bool);
    void toolbarIconSizeChanged(int);
    void hideStatusBarChanged(bool);
    void styleChanged(const QString &style);

private:
    QSettings *mSettings;
    bool mAlwaysOnTop;
    int mRunCount;
    QString mStyle;
    QString mLanguage;
    QString mLastOpenPath;
    QString mLastOpenDir;
    bool mDisplayTrayIcon;
    bool mSaveBeforeExit;
    bool mOpenLastFile;
    bool mFullScreen;
    int mToolbarIconSize;
    QStringList mRecentFiles;
    bool mHideMenuBar;
    bool mHideStatusBar;
    QStringList mRecentOpenedFiles;
    QString mLastActiveFile;
    qreal mFrameSceneScale;
    qreal mMoveSceneScale;
    QString mEnginePath;
    bool mEnableDebugMultiInstances;

    void setValue(const QString &section, const QVariant &value);
    QVariant value(const QString &section, const QVariant &defValue=QVariant());
    bool boolValue(const QString &key, bool defValue = false);
    QColor colorValue(const QString &key, const QColor &defValue = QColor());
    QString stringValue(const QString &key, const QString &defValue = "");
    int intValue(const QString &key, int defaultValue=0);
    float floatValue(const QString &key, float defaultValue=0.0);
    qreal doubleValue(const QString &key, qreal defaultValue=0.0);
    QDate dateValue(const QString &key, int defaultValue=-1);
    QTime timeValue(const QString &key, int defaultValue=0);
    QStringList listValue(const QString &key);
    static TPreferences* mInstance;
};

#endif // PREFERENCES_H
