#ifndef UTILS_H
#define UTILS_H

#include <QString>

namespace Utils{
    QString microSecToTimeStr(long ms, bool padZero=true);
    QString secToTimeStr(long seconds, bool padZero=true);
    QString absoluteFilePath(QString fileName);
    bool exploreFile(QString fileName);
    void cpy2wchar(wchar_t *dest, QString source);
}

#endif // UTILS_H
