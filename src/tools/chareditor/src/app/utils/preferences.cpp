#include "preferences.h"
#include "utils.h"

const int TPreferences::MAX_RECENT_FILES = 10;
const int TPreferences::ICON_SIZE_SMALL = 16;
const int TPreferences::ICON_SIZE_MEDIUM = 24;
const int TPreferences::ICON_SIZE_LARGE = 32;

#define SETTING_FILE                "setting.ini"

// General
static const char *SEC_GUI = "Gui";
static const char *SEC_GUI_ALWAYS_ON_TOP = "AlwaysOnTop";
static const char *SEC_GUI_LANGUAGE = "Language";
static const char *SEC_GUI_STYLE = "Style";
static const char *SEC_GUI_GEOMETRY = "Geometry";
static const char *SEC_GUI_WINDOW_STATE = "WindowState";
static const char *SEC_GUI_LAST_OPEN_PATH = "LastOpenPath";
static const char *SEC_GUI_LAST_OPEN_DIR = "LastOpenDirectory";
static const char *SEC_GUI_SAVEBEFOREEXIT = "SaveBeforeExit";
static const char *SEC_GUI_OPENLASTFILE = "OpenLastFile";
static const char *SEC_GUI_FULLSCREEN = "FullScreen";
static const char *SEC_GUI_TOOLBARICONSIZE = "ToolbarIconSize";
static const char *SEC_GUI_RECENTFILES = "RecentFiles";
static const char *SEC_GUI_HIDEMENUBAR = "HideMenuBar";
static const char *SEC_GUI_HIDESTATUSBAR = "HideStatusBar";
static const char *SEC_GUI_RECENTOPENEDFILES = "RecentOpenedFiles";
static const char *SEC_GUI_LASTACTIVEFILE = "LastActiveFile";
static const char *SEC_GUI_FRAME_SCENE_SCALE = "FrameSceneScale";
static const char *SEC_GUI_MOVE_SCENE_SCALE = "MoveSceneScale";

// Install
static const char *SEC_INSTALL = "Install";
static const char *SEC_INSTALL_RUN_COUNT = "RunCount";

// Options
static const char *SEC_OPTIONS = "Options";

static const char *SEC_OPTION_GENERAL = "OptionGeneral";
static const char *SEC_OPTION_DISPLAY_TRAY_ICON = "DisplayTrayIcon";

static const char *SEC_OPTION_DEBUG = "OptionDebug";
static const char *SEC_OPTION_DEBUG_ENGINE_PATH = "EnginePath";
static const char *SEC_OPTION_DEBUG_MULTI_INSTANCES = "MultiInstances";

#define SET_VALUE(parent,section,member,value) \
    if(member==value)\
        return;\
    mSettings->beginGroup(parent);\
    mSettings->setValue(section, value);\
    mSettings->endGroup();\
    member = value

#define SET_VALUE2(parent,sub,section,member,value) \
    if(member==value)\
        return;\
    mSettings->beginGroup(parent);\
    mSettings->beginGroup(sub);\
    mSettings->setValue(section, value);\
    mSettings->endGroup();\
    mSettings->endGroup();\
    member = value

TPreferences *TPreferences::mInstance = NULL;

//
// This class holds user preferences and provides a convenient interface to
// access them.
//
TPreferences::TPreferences(QObject *parent):
    QObject(parent)
{
    mSettings = new QSettings(Utils::absoluteFilePath(SETTING_FILE), QSettings::IniFormat);

    // Retrieve gui settings
    mSettings->beginGroup(SEC_GUI);
    mAlwaysOnTop = boolValue(SEC_GUI_ALWAYS_ON_TOP);
    mLastOpenPath = stringValue(SEC_GUI_LAST_OPEN_PATH);
    mLastOpenDir = stringValue(SEC_GUI_LAST_OPEN_DIR);
    mSaveBeforeExit = boolValue(SEC_GUI_SAVEBEFOREEXIT, true);
    mOpenLastFile = boolValue(SEC_GUI_OPENLASTFILE);
    mFullScreen = boolValue(SEC_GUI_FULLSCREEN);
    mToolbarIconSize = intValue(SEC_GUI_TOOLBARICONSIZE, ICON_SIZE_MEDIUM);
    mLanguage = stringValue(SEC_GUI_LANGUAGE, "en");
    mStyle = stringValue(SEC_GUI_STYLE);
    mRecentFiles = listValue(SEC_GUI_RECENTFILES);
    mHideMenuBar = boolValue(SEC_GUI_HIDEMENUBAR);
    mHideStatusBar = boolValue(SEC_GUI_HIDESTATUSBAR);
    mRecentOpenedFiles = listValue(SEC_GUI_RECENTOPENEDFILES);
    mLastActiveFile = stringValue(SEC_GUI_LASTACTIVEFILE);
    mFrameSceneScale = doubleValue(SEC_GUI_FRAME_SCENE_SCALE, 1.0);
    mMoveSceneScale = doubleValue(SEC_GUI_MOVE_SCENE_SCALE, 1.0);
    mSettings->endGroup();

    //// Options
    mSettings->beginGroup(SEC_OPTIONS);
        // general
        mSettings->beginGroup(SEC_OPTION_GENERAL);
        mDisplayTrayIcon = boolValue(SEC_OPTION_DISPLAY_TRAY_ICON, true);
        mSettings->endGroup();

        // debug
        mSettings->beginGroup(SEC_OPTION_DEBUG);
        mEnginePath = stringValue(SEC_OPTION_DEBUG_ENGINE_PATH);
        mSettings->endGroup();

    mSettings->endGroup();

    //// Keeping track of some usage information
    mSettings->beginGroup(SEC_INSTALL);

    // This section wrote by main controller while write trial license
    if(mSettings->contains(SEC_INSTALL_RUN_COUNT))
    {
        mRunCount = intValue(SEC_INSTALL_RUN_COUNT) + 1;
        mSettings->setValue(SEC_INSTALL_RUN_COUNT, mRunCount);
    }

    mSettings->endGroup();
}

TPreferences::~TPreferences()
{
    if(mSettings)
    {
        mSettings->sync();
        delete mSettings;
        mSettings = NULL;
    }
}

TPreferences* TPreferences::instance()
{
    if(mInstance==NULL)
        mInstance = new TPreferences();
    return mInstance;
}

void TPreferences::deleteInstance()
{
    if(mInstance)
    {
        delete mInstance;
        mInstance = NULL;
    }
}

void TPreferences::save()
{
    mSettings->sync();
}

bool TPreferences::saveBeforeExit() const
{
    return mSaveBeforeExit;
}

void TPreferences::setSaveBeforeExit(bool saveBeforeExit)
{
    SET_VALUE(SEC_GUI, SEC_GUI_SAVEBEFOREEXIT, mSaveBeforeExit, saveBeforeExit);
}

bool TPreferences::openLastFile() const
{
    return mOpenLastFile;
}

void TPreferences::setOpenLastFile(bool openLastFile)
{
    SET_VALUE(SEC_GUI, SEC_GUI_OPENLASTFILE, mOpenLastFile, openLastFile);
}

bool TPreferences::fullScreen() const
{
    return mFullScreen;
}

void TPreferences::setFullScreen(bool fullScreen)
{
    SET_VALUE(SEC_GUI, SEC_GUI_FULLSCREEN, mFullScreen, fullScreen);
}

int TPreferences::toolbarIconSize() const
{
    return mToolbarIconSize;
}

void TPreferences::setToolbarIconSize(int toolbarIconSize)
{
    SET_VALUE(SEC_GUI, SEC_GUI_TOOLBARICONSIZE, mToolbarIconSize, toolbarIconSize);
    emit toolbarIconSizeChanged(toolbarIconSize);
}

QStringList TPreferences::recentFiles() const
{
    return mRecentFiles;
}

void TPreferences::setRecentFiles(const QStringList &recentFiles)
{
    SET_VALUE(SEC_GUI, SEC_GUI_RECENTFILES, mRecentFiles, recentFiles);
}

bool TPreferences::hideMenuBar() const
{
    return mHideMenuBar;
}

void TPreferences::setHideMenuBar(bool hideMenuBar)
{
    SET_VALUE(SEC_GUI, SEC_GUI_HIDEMENUBAR, mHideMenuBar, hideMenuBar);
    emit hideMenuBarChanged(hideMenuBar);
}

bool TPreferences::hideStatusBar() const
{
    return mHideStatusBar;
}

void TPreferences::setHideStatusBar(bool hideStatusBar)
{
    SET_VALUE(SEC_GUI, SEC_GUI_HIDESTATUSBAR, mHideStatusBar, hideStatusBar);
    emit hideStatusBarChanged(hideStatusBar);
}

QStringList TPreferences::recentOpenedFiles() const
{
    return mRecentOpenedFiles;
}

void TPreferences::setRecentOpenedFiles(const QStringList &recentOpenedFiles)
{
    SET_VALUE(SEC_GUI, SEC_GUI_RECENTOPENEDFILES, mRecentOpenedFiles, recentOpenedFiles);
}

QString TPreferences::lastActiveFile() const
{
    return mLastActiveFile;
}

void TPreferences::setLastActiveFile(const QString &lastActiveFile)
{
    SET_VALUE(SEC_GUI, SEC_GUI_LASTACTIVEFILE, mLastActiveFile, lastActiveFile);
}

QString TPreferences::style() const
{
    return mStyle;
}

void TPreferences::setStyle(const QString &style)
{
    SET_VALUE(SEC_GUI, SEC_GUI_STYLE, mStyle, style);
    emit styleChanged(style);
}

qreal TPreferences::frameSceneScale() const
{
    return mFrameSceneScale;
}

void TPreferences::setFrameSceneScale(const qreal &frameSceneScale)
{
    SET_VALUE(SEC_GUI, SEC_GUI_FRAME_SCENE_SCALE, mFrameSceneScale, frameSceneScale);
}

qreal TPreferences::moveSceneScale() const
{
    return mMoveSceneScale;
}

void TPreferences::setMoveSceneScale(const qreal &moveSceneScale)
{
    SET_VALUE(SEC_GUI, SEC_GUI_MOVE_SCENE_SCALE, mMoveSceneScale, moveSceneScale);
}

QString TPreferences::enginePath() const
{
    return mEnginePath;
}

void TPreferences::setEnginePath(const QString &enginePath)
{
    SET_VALUE2(SEC_OPTIONS, SEC_OPTION_DEBUG, SEC_OPTION_DEBUG_ENGINE_PATH, mEnginePath, enginePath);
}

bool TPreferences::enableDebugMultiInstances() const
{
    return mEnableDebugMultiInstances;
}

void TPreferences::setEnableDebugMultiInstances(bool enableDebugMultiInstances)
{
    SET_VALUE2(SEC_OPTIONS, SEC_OPTION_DEBUG, SEC_OPTION_DEBUG_MULTI_INSTANCES, mEnableDebugMultiInstances, enableDebugMultiInstances);
}

void TPreferences::windowGeometryState(QByteArray *g, QByteArray *s)
{
    mSettings->beginGroup(SEC_GUI);
    *g = mSettings->value(SEC_GUI_GEOMETRY).toByteArray();
    *s = mSettings->value(SEC_GUI_WINDOW_STATE).toByteArray();
    mSettings->endGroup();
}

void TPreferences::setWindowGeometryState(const QVariant &geometry, const QVariant &windowState)
{
    mSettings->beginGroup(SEC_GUI);
    mSettings->setValue(SEC_GUI_GEOMETRY, geometry);
    mSettings->setValue(SEC_GUI_WINDOW_STATE, windowState);
    mSettings->endGroup();
}

bool TPreferences::displayTrayIcon() const
{
    return mDisplayTrayIcon;
}

void TPreferences::setDisplayTrayIcon(bool displayTrayIcon)
{
    SET_VALUE2(SEC_OPTIONS, SEC_OPTION_GENERAL, SEC_OPTION_DISPLAY_TRAY_ICON, mDisplayTrayIcon, displayTrayIcon);
}

bool TPreferences::alwaysOnTop() const
{
    return mAlwaysOnTop;
}

void TPreferences::setAlwaysOnTop(bool alwaysOnTop)
{
    SET_VALUE(SEC_GUI, SEC_GUI_ALWAYS_ON_TOP, mAlwaysOnTop, alwaysOnTop);
}

int TPreferences::runCount() const
{
    return mRunCount;
}

void TPreferences::setRunCount(int runCount)
{
    mRunCount = runCount;
}

QString TPreferences::language() const
{
    return mLanguage;
}

void TPreferences::setLanguage(const QString &language)
{
    SET_VALUE(SEC_GUI, SEC_GUI_LANGUAGE, mLanguage, language);
}

QString TPreferences::lastOpenPath() const
{
    return mLastOpenPath;
}

void TPreferences::setLastOpenPath(const QString &lastOpenPath)
{
    SET_VALUE(SEC_GUI, SEC_GUI_LAST_OPEN_PATH, mLastOpenPath, lastOpenPath);
}

QString TPreferences::lastOpenDir() const
{
    return mLastOpenDir;
}

void TPreferences::setLastOpenDir(const QString &lastOpenDir)
{
    SET_VALUE(SEC_GUI, SEC_GUI_LAST_OPEN_DIR, mLastOpenDir, lastOpenDir);
}

void TPreferences::setValue(const QString &section, const QVariant &value)
{
    mSettings->setValue(section, value);
}

QVariant TPreferences::value(const QString &section, const QVariant &defValue)
{
    return mSettings->value(section, defValue);
}

bool TPreferences::boolValue(const QString &key, bool defValue)
{
    return mSettings->value(key, defValue).toBool();
}

QColor TPreferences::colorValue(const QString &key, const QColor &defValue)
{
    QString name = mSettings->value(key, defValue.name()).toString();
    if((!QColor::isValidColor(name)))
        return QColor();
    return QColor(name);
}

QString TPreferences::stringValue(const QString &key, const QString &defValue)
{
    return mSettings->value(key, defValue).toString();
}

int TPreferences::intValue(const QString &key, int defaultValue)
{
    bool ok = false;
    int v = mSettings->value(key, defaultValue).toInt(&ok);
    if (ok)
        return v;
    return defaultValue;
}

float TPreferences::floatValue(const QString &key, float defaultValue)
{
    bool ok = false;
    float v = mSettings->value(key, defaultValue).toFloat(&ok);
    if (ok)
        return v;
    return defaultValue;
}

qreal TPreferences::doubleValue(const QString &key, qreal defaultValue)
{
    bool ok = false;
    float v = mSettings->value(key, defaultValue).toDouble(&ok);
    if (ok)
        return v;
    return defaultValue;
}

QStringList TPreferences::listValue(const QString &key)
{
    return mSettings->value(key).toStringList();
}

QDate TPreferences::dateValue(const QString &key, int defaultValue)
{
    if (defaultValue==-1)
        defaultValue = QDate::currentDate().toJulianDay();
    int days = intValue(key, defaultValue);
    return QDate::fromJulianDay(days);
}

QTime TPreferences::timeValue(const QString &key, int defaultValue)
{
    int seconds = intValue(key, defaultValue);
    QTime time = QTime::fromMSecsSinceStartOfDay(seconds);
    if(!time.isValid())
        time = QTime::fromMSecsSinceStartOfDay(0);
    return time;
}
