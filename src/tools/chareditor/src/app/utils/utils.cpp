#include "utils.h"

#include <QDir>
#include <QtMath>
#include <QProcess>
#include <QFileInfo>
#include <QApplication>

QString Utils::microSecToTimeStr(long ms, bool padZero)
{
    return secToTimeStr(qCeil((double)ms/1000), padZero);
}

QString Utils::secToTimeStr(long seconds, bool padZero)
{
    int hour = seconds / 3600;
    if(hour > 99)
        hour = 99;

    seconds %= 3600;

    int minute = seconds / 60;
    if(minute>99)
        minute = 99;

    seconds %= 60;

    int second = seconds % 60;

    QString result;
    if(hour > 0)
        result = QString::asprintf("%2d:%2d:%2d", hour, minute, second);
    else
        result = QString::asprintf("%2d:%2d", minute, second);

    if(padZero)
        result = result.replace(" ", "0");

    return result;
}

QString Utils::absoluteFilePath(QString fileName)
{
    QFileInfo fi(fileName);
    if(fi.isRelative())
    {
        QDir dir(qApp->applicationDirPath());
        fileName = dir.absoluteFilePath(fileName);
    }
    return fileName;
}

bool Utils::exploreFile(QString fileName)
{
    bool ret = false;
#ifdef Q_OS_WIN32
    ret = QProcess::startDetached("explorer /select,"+QDir::toNativeSeparators(fileName));
#elif Q_OS_UNIX
#elif Q_OS_MACX
#endif
    return ret;
}

void Utils::cpy2wchar(wchar_t *dest, QString source)
{
    if(dest)
    {
        std::wstring sourceW = source.toStdWString();
        wcscpy(dest, sourceW.c_str());
    }
}
