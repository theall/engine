TEMPLATE = app
CONFIG += c++11 object_parallel_to_source
CONFIG += app_bundle
CONFIG += qt

SOURCES += main.cpp

include(core.pri)
include(../utils/utils.pri)
include(../../../../../shared/gameutils/gameutils.pri)
