#ifndef FILESYSTEMWATCHER_H
#define FILESYSTEMWATCHER_H

#include <QMap>
#include <QObject>
#include <QFileSystemWatcher>

/**
 * A wrapper around QFileSystemWatcher that deals gracefully with files being
 * watched multiple times. It also doesn't start complaining when a file
 * doesn't exist.
 *
 * It's meant to be used as drop-in replacement for QFileSystemWatcher.
 */
class TFileSystemWatcher : public QObject
{
    Q_OBJECT

public:
    explicit TFileSystemWatcher(QObject *parent = nullptr);

    void addPath(const QString &path);
    void removePath(const QString &path);

signals:
    void fileChanged(const QString &path);
    void directoryChanged(const QString &path);

private slots:
    void onFileChanged(const QString &path);
    void onDirectoryChanged(const QString &path);

private:
    QFileSystemWatcher *mWatcher;
    QMap<QString, int> mWatchCount;
};

#endif // FILESYSTEMWATCHER_H
