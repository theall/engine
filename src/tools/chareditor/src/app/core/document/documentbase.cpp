#include "documentbase.h"
#include "../base/docutil.h"
#include "../base/filesystemwatcher.h"
#include "../base/cachedpixmap.h"
#include "../base/cachedsound.h"

#include <QUuid>

static const char *P_GROUP = "Project";
static const char *P_ICON = "Icon";
static const char *P_AUTHOR = "Author";
static const char *P_EMAIL = "Email";
static const char *P_VERSION = "Version";
static const char *P_COMMENT = "Comment";
static const char *P_CREATE_TIME = "Create time";
static const char *P_UPDATE_TIME = "Update time";
static const char *RES_PATH_IMAGE = "image";
static const char *RES_PATH_SOUND = "sound";

TDocumentBase::TDocumentBase(const QString &file, QObject *parent) :
    TPropertyObject(parent)
  , mGuid(QUuid::createUuid().toString())
  , mCreateTime(QDateTime::currentDateTime())
  , mUpdateTime(mCreateTime)
  , mIsDirty(false)
  , mFileName(file)
  , mUndoStack(new QUndoStack(this))
  , mCachedSounds(new TCachedSound(this))
  , mCachedPixmaps(new TCachedPixmap(this))
  , mFileWatcher(new TFileSystemWatcher(this))
{
    connect(mUndoStack, SIGNAL(cleanChanged(bool)), this, SLOT(slotModificationChanged(bool)));
    mUndoStack->setClean();

    connect(mFileWatcher, SIGNAL(fileChanged(QString)), this, SLOT(slotFileChanged(QString)));
    connect(mFileWatcher, SIGNAL(directoryChanged(QString)), this, SLOT(slotDirectoryChanged(QString)));

    initPropertySheet();
}

TDocumentBase::~TDocumentBase()
{

}

QUndoStack *TDocumentBase::undoStack() const
{
    return mUndoStack;
}

void TDocumentBase::addUndoCommand(QUndoCommand *command)
{
    mUndoStack->push(command);
}

TPixmap *TDocumentBase::getPixmap(const QString &file) const
{
    return mCachedPixmaps->getPixmap(QFileInfo(file).fileName());
}

QString TDocumentBase::getPixmapRelativePath(const QString &file) const
{
    QString path;
    TPixmap *pixmap = mCachedPixmaps->getPixmap(file);
    if(!pixmap)
        return path;

    path = pixmap->fileFullName();
    return path.right(path.size() - mProjectRoot.size());
}

TSound *TDocumentBase::getSound(const QString &file) const
{
    return mCachedSounds->getSound(QFileInfo(file).fileName());
}

QString TDocumentBase::getSoundRelativePath(const QString &file) const
{
    QString path;
    TSound *sound = mCachedSounds->getSound(file);
    if(!sound)
        return path;

    path = sound->fileFullName();
    return path.right(path.size() - mProjectRoot.size());
}

QPixmap TDocumentBase::icon() const
{
    QPixmap ret;
    TPixmap *pixmap = getPixmap(mPropertySheet->getValue(P_ICON).toString());
    if(pixmap)
        ret = pixmap->thumbnail();
    return ret;
}

QString TDocumentBase::baseName() const
{
    return mBaseName;
}

QDir TDocumentBase::projectDir() const
{
    return mProjectDir;
}

bool TDocumentBase::save(const QString &fileName)
{
    if(!fileName.isEmpty())
    {
        setFileName(fileName);
    }
    if(mFileName.isEmpty())
        return false;

    try {
        mUpdateTime = QDateTime::currentDateTime();
        QByteArray byteArray = getSavedData();
        if(!byteArray.isEmpty())
        {
            QFile saveFile(mFileName);
            if (!saveFile.open(QIODevice::WriteOnly)) {
                qWarning("Couldn't open save file.");
                return false;
            }
            saveFile.write(byteArray);
            mUndoStack->setClean();
            mLastSaveTime = QFileInfo(mFileName).lastModified();

            emit saved();
        }
    } catch(...) {
        return false;
    }

    return true;
}

void TDocumentBase::reload()
{
    load(mFileName);
    mUndoStack->setClean();
}

QString TDocumentBase::author()
{
    return mPropertySheet->getValue(P_AUTHOR).toString();
}

void TDocumentBase::setAuthor(const QString &author)
{
    (*mPropertySheet)[P_AUTHOR]->setValue(author);
}

QString TDocumentBase::fileName() const
{
    return mFileName;
}

void TDocumentBase::setFileName(const QString &fileName)
{
    QFileInfo fileInfo(fileName);
    setProjectRoot(fileInfo.absolutePath());

    mFileName = fileName;
    mBaseName = fileInfo.baseName();
    mProjectName = mBaseName;

    if(!mFileName.isEmpty())
        mFileWatcher->addPath(mFileName);
}

void TDocumentBase::setBaseName(const QString &baseName)
{
    mBaseName = baseName;
}

QString TDocumentBase::email()
{
    return mPropertySheet->getValue(P_EMAIL).toString();
}

void TDocumentBase::setEmail(const QString &email)
{
    (*mPropertySheet)[P_EMAIL]->setValue(email);
}

QString TDocumentBase::version()
{
    return mPropertySheet->getValue(P_VERSION).toString();
}

void TDocumentBase::setVersion(const QString &version)
{
    (*mPropertySheet)[P_VERSION]->setValue(version);
}

QString TDocumentBase::comment()
{
    return mPropertySheet->getValue(P_COMMENT).toString();
}

void TDocumentBase::setComment(const QString &comment)
{
    (*mPropertySheet)[P_COMMENT]->setValue(comment);
}

TPropertySheet *TDocumentBase::propertySheet() const
{
    return mPropertySheet;
}

QString TDocumentBase::projectRoot() const
{
    return mProjectRoot;
}

void TDocumentBase::setProjectRoot(const QString &projectRoot)
{
    mProjectDir.setPath(projectRoot);
    mSoundDir.setPath(mProjectDir.absoluteFilePath(RES_PATH_SOUND));

    if(!mProjectDir.exists(RES_PATH_IMAGE))
        mProjectDir.mkdir(RES_PATH_IMAGE);
    if(!mSoundDir.exists())
        mProjectDir.mkdir(RES_PATH_SOUND);

    mProjectRoot = projectRoot;

    // Load image
    mCachedPixmaps->setPath(mProjectDir.absoluteFilePath(RES_PATH_IMAGE));

    // Load sound
    mCachedSounds->setPath(mProjectDir.absoluteFilePath(RES_PATH_SOUND));
}

QString TDocumentBase::projectName() const
{
    return mProjectName;
}

void TDocumentBase::setProjectName(const QString &projectName)
{
    mProjectName = projectName;
}

QString TDocumentBase::projectType() const
{
    return mProjectType;
}

void TDocumentBase::setProjectType(const QString &projectType)
{
    mProjectType = projectType;
}

void TDocumentBase::load(const QString &file)
{
    QFileInfo fileInfo(file);
    if(fileInfo.isFile() && fileInfo.exists())
    {
        QString fileName = fileInfo.canonicalFilePath();
        setFileName(fileName);
        bool success = false;
        try {
            success = loadFromFile(file);
        } catch (std::string s) {
            throw QString::fromStdString(s);
            return;
        }
        if(!success)
            throw tr("Load %1 failed.").arg(file);

        mUndoStack->setClean();
        mLastSaveTime = fileInfo.lastModified();
    } else {
        throw tr("File is not exists.");
    }
}

void TDocumentBase::initPropertySheet()
{
    TPropertyItem *groupItem = mPropertySheet->addProperty(PT_GROUP, P_GROUP, PID_INVALID);
    TPropertyItem *iconItem = mPropertySheet->addProperty(PT_PIXMAP, P_ICON, PID_SPRITE_ICON, QVariant(), groupItem);
    mPropertySheet->addProperty(PT_STRING, P_AUTHOR, PID_PROJECT_AUTHOR, tr("Unknown"), groupItem);
    mPropertySheet->addProperty(PT_STRING, P_EMAIL, PID_PROJECT_CONTACT, tr("Unknown"), groupItem);
    mPropertySheet->addProperty(PT_STRING, P_VERSION, PID_PROJECT_VERSION, "1.0.0.0", groupItem);
    mPropertySheet->addProperty(PT_STRING_EX, P_COMMENT, PID_PROJECT_COMMENT, tr(""), groupItem);
    mPropertySheet->addProperty(QVariant::DateTime, P_CREATE_TIME, PID_INVALID, mCreateTime, groupItem)->setAttribute(PA_READ_ONLY, true);
    mPropertySheet->addProperty(QVariant::DateTime, P_UPDATE_TIME, PID_INVALID, mUpdateTime, groupItem)->setAttribute(PA_READ_ONLY, true);

    connect(iconItem,
            SIGNAL(valueChanged(QVariant)),
            this,
            SLOT(slotIconPropertyItemChanged(QVariant)));
}

bool TDocumentBase::isDirty() const
{
    return mIsDirty;
}

void TDocumentBase::setDirty(bool isDirty)
{
    /**
     * Dont't directly set isDirty to mIsDirty, call slotModificationChanged
     * to ensure the signal dirtyFlagChanged is emitted
     */
    slotModificationChanged(!isDirty);
}

void TDocumentBase::slotModificationChanged(bool isClean)
{
    bool dirty = !isClean;
    if(dirty == mIsDirty)
        return;
    mIsDirty = dirty;
    emit dirtyFlagChanged(mIsDirty);
}

void TDocumentBase::slotFileChanged(const QString &file)
{
    bool fileRemoved = !QFile::exists(file);
    if(mFileName==file)
    {
        // Project file changed
        if(fileRemoved)
            setDirty(true);
        else
            emit projectFileChanged();
    }
}

void TDocumentBase::slotDirectoryChanged(const QString &dir)
{
    Q_UNUSED(dir);
}

void TDocumentBase::slotIconPropertyItemChanged(const QVariant &oldValue)
{
    Q_UNUSED(oldValue);

    TPropertyItem *propertyItem = static_cast<TPropertyItem*>(sender());
    if(!propertyItem)
        return;

    QString newPixmapFile = propertyItem->value().toString();
    emit iconChanged(getPixmap(newPixmapFile));
}

QString TDocumentBase::getGuid() const
{
    return mGuid;
}

void TDocumentBase::setGuid(const QString &guid)
{
    mGuid = guid;
}

TCachedPixmap *TDocumentBase::getCachedPixmaps() const
{
    return mCachedPixmaps;
}

TCachedSound *TDocumentBase::getCachedSounds() const
{
    return mCachedSounds;
}

QDateTime TDocumentBase::lastSaveTime() const
{
    return mLastSaveTime;
}
