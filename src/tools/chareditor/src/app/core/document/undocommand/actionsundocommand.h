#ifndef ACTIONSUNDOCOMMAND_H
#define ACTIONSUNDOCOMMAND_H

#include <QUndoCommand>

enum ActionUndoCommand
{
    AUC_ADD = 0,
    AUC_REMOVE,
    AUC_CLONE,
    AUC_COUNT
};

class TAction;
class TActionsModel;

class TActionsUndoCommand : public QUndoCommand
{
public:
    TActionsUndoCommand(ActionUndoCommand command, TActionsModel *actionsModel, TAction *action, QUndoCommand *parent = Q_NULLPTR);
    ~TActionsUndoCommand();

    void undo() Q_DECL_OVERRIDE;
    void redo() Q_DECL_OVERRIDE;

private:
    TActionsModel *mActionsModel;
    TAction *mAction;
    int mIndex;
    ActionUndoCommand mCommand;
};

#endif // ACTIONSUNDOCOMMAND_H
