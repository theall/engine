#include "actionsundocommand.h"
#include "../model/actionsmodel.h"

#include <QCoreApplication>

#define tr(x) QCoreApplication::translate("UndoCommand", x)

const QString g_commandText[AUC_COUNT] = {
    tr("Add new action"),
    tr("Remove action"),
    tr("Clone action")
};

TActionsUndoCommand::TActionsUndoCommand(
        ActionUndoCommand command,
        TActionsModel *actionsModel,
        TAction *action,
        QUndoCommand *parent) :
    QUndoCommand(parent)
  , mActionsModel(actionsModel)
  , mAction(action)
  , mIndex(-1)
  , mCommand(command)
{
    setText(g_commandText[command]);
}

TActionsUndoCommand::~TActionsUndoCommand()
{

}

void TActionsUndoCommand::undo()
{
    if(mCommand==AUC_ADD)
    {
        mIndex = mActionsModel->removeAction(mAction);
    } else if(mCommand==AUC_REMOVE) {
        mActionsModel->addAction(mAction, mIndex);
    }
}

void TActionsUndoCommand::redo()
{
    if(mCommand==AUC_ADD)
    {
        mActionsModel->addAction(mAction, mIndex);
    } else if(mCommand==AUC_REMOVE) {
        mIndex = mActionsModel->removeAction(mAction);
    }
}
