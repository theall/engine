#ifndef DOCUMENTBASE_H
#define DOCUMENTBASE_H

#include <QDir>
#include <QDateTime>
#include <QUndoStack>

#include "../base/propertyobject.h"
#include "../base/propertyundocommand.h"

class TPixmap;
class TSound;
class TPropertyObject;
class TCachedPixmap;
class TCachedSound;
class TFileSystemWatcher;

class TDocumentBase : public TPropertyObject
{
    Q_OBJECT

public:
    TDocumentBase(const QString &file = QString(), QObject *parent = nullptr);
    ~TDocumentBase();

    QUndoStack *undoStack() const;
    void addUndoCommand(QUndoCommand *command);

    virtual QByteArray getSavedData() const = 0;
    virtual bool loadFromFile(const QString &fileName) = 0;

    TPixmap *getPixmap(const QString &file) const;
    QString getPixmapRelativePath(const QString &file) const;
    TSound *getSound(const QString &file) const;
    QString getSoundRelativePath(const QString &file) const;
    QPixmap icon() const;

    QDir projectDir() const;

    void load(const QString &file);
    bool save(const QString &fileName = QString());
    void reload();

    QString author();
    void setAuthor(const QString &author);

    QString fileName() const;

    QString baseName() const;
    void setBaseName(const QString &baseName);

    QString email();
    void setEmail(const QString &email);

    QString version();
    void setVersion(const QString &version);

    QString comment();
    void setComment(const QString &comment);

    TPropertySheet *propertySheet() const;

    QString projectRoot() const;
    void setProjectRoot(const QString &projectRoot);

    QString projectName() const;
    void setProjectName(const QString &projectName);

    QString projectType() const;
    void setProjectType(const QString &projectType);

    bool isDirty() const;
    void setDirty(bool isDirty);

    QDateTime lastSaveTime() const;

    TCachedPixmap *getCachedPixmaps() const;
    TCachedSound *getCachedSounds() const;

    QString getGuid() const;
    void setGuid(const QString &guid);

signals:
    void projectFileChanged();
    void dirtyFlagChanged(bool isDirty);
    void saved();
    void resourceChanged();
    void iconChanged(TPixmap *newPixmap);

private slots:
    void slotModificationChanged(bool isClean);
    void slotFileChanged(const QString &file);
    void slotDirectoryChanged(const QString &dir);
    void slotIconPropertyItemChanged(const QVariant &oldValue);

protected:
    QString mGuid;
    QDateTime mCreateTime;
    QDateTime mUpdateTime;

private:
    bool mIsDirty;
    QString mLastExportFileName;
    QDir mProjectDir;
    QDir mSoundDir;
    QString mProjectRoot;
    QString mProjectName;
    QString mProjectType;
    QString mFileName;
    QString mBaseName;
    QDateTime mLastSaveTime;
    QUndoStack *mUndoStack;

    TCachedSound *mCachedSounds;
    TCachedPixmap *mCachedPixmaps;
    TFileSystemWatcher *mFileWatcher;

    void setFileName(const QString &fileName);
    void initPropertySheet();
};

#endif // DOCUMENTBASE_H
