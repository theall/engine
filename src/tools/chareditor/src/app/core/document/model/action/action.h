#ifndef DOCUMENT_ACTION_H
#define DOCUMENT_ACTION_H

#include "framesmodel/framesmodel.h"
#include "triggersmodel/actiontriggersmodel.h"
#include "../../../base/propertyobject.h"

class TDocument;
class TMoveModelScene;

class TAction : public TPropertyObject
{
    Q_OBJECT

public:
    TAction(const QString &name, QObject *parent=nullptr);
    ~TAction();

    Model::TAction *toModel() const;
    void loadFromModel(const Model::TAction &actionModel, void *context=nullptr);

    QString name() const;
    void setName(const QString &name);

    QPointF vector() const;
    void setVector(const QPointF &vector);

    TPropertySheet *propertySheet() const;

    TFramesModel *framesModel() const;

    void cmdAddFrame(const QString &imageFile, int pos);
    void cmdRemoveFrame(TFrame* frame);
    void cmdRemoveFrame(int index);

    void cmdAddFrames(const QStringList &imageFiles, int pos);
    void cmdAddFrames(const QList<TFrame*> &framesList, int pos);
    void cmdRemoveFrames(const QList<TFrame*> &framesList);
    void cmdRemoveFrames(const QList<int> &indexList);
    void cmdMoveFrames(const QList<int> &indexList, int pos);

    TFrame *frameAt(int index);
    TFrame *getFrame(int index);
    int framesCount();

    TMoveModelScene *moveModelScene() const;

    TActionTriggersModel *triggersModel() const;
    void cmdAddActionTrigger(TActionTrigger* actionTrigger);
    void cmdRemoveActionTrigger(TActionTrigger* actionTrigger);
    void cmdRemoveActionTrigger(int index);
    void cmdModifyActionTrigger(TActionTrigger* actionTrigger, const TActionTrigger &newActionTrigger);

    void cmdAddActionTriggers(const QList<TActionTrigger*> &actionTriggersList);
    void cmdRemoveActionTriggers(const QList<TActionTrigger*> &actionTriggersList);
    void cmdRemoveActionTriggers(const QList<int> &indexList);
    void cmdMoveActionTriggers(const QList<int> &indexList, int pos);

    void updateFramesBuddy();

signals:
    void nameChanged(const QString &newName);

private slots:
    void slotPropertyItemValueChanged(TPropertyItem *item, const QVariant &value);

private:
    QString mName;
    ActionMode mActionMode;
    TActionTriggersModel *mTriggersModel;
    TFramesModel *mFramesModel;
    TDocument *mDocument;
    TMoveModelScene *mMoveModelScene;

    void initPropertySheet();
};


#endif // DOCUMENT_ACTION_H
