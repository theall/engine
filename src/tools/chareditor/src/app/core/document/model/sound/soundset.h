#ifndef QOBJECTSOUND_H
#define QOBJECTSOUND_H

#include <QList>
#include <QDataStream>

#include "sounditemsmodel.h"
#include "../../../base/propertyobject.h"
#include "../../../../../../../../shared/gameutils/base/variable.h"

namespace Model {
class TSoundSet;
}
class TSoundSet : public TPropertyObject
{
    Q_OBJECT

public:
    TSoundSet(QObject *parent = nullptr);
    ~TSoundSet();

    void loadFromModel(Model::TSoundSet *soundModel, void *context=nullptr);
    Model::TSoundSet *toModel() const;

    bool autoPlay() const;
    void setAutoPlay(bool autoPlay);

    SoundSetModel soundSetModel() const;
    void setSoundSetModel(const SoundSetModel &soundSetModel);

    TSoundItemsModel *soundItemsModel() const;

signals:
    void propertyChanged(PropertyID id, const QVariant &value);

private slots:
    void slotPropertyItemValueChanged(TPropertyItem *item, const QVariant &value);

private:
    TSoundItemsModel *mSoundItemModel;
};
typedef QList<TSoundSet *> TSoundSetList;

QDataStream &operator<<(QDataStream& out, const TSoundSet &soundSet);
QDataStream &operator>>(QDataStream& in, TSoundSet &soundSet);

#endif // QOBJECTSOUND_H
