#ifndef ACTIONTRIGGERSUNDOCOMMAND_H
#define ACTIONTRIGGERSUNDOCOMMAND_H

#include <QUndoCommand>

enum ActionTriggerUndoCommand
{
    ATUC_ADD = 0,
    ATUC_REMOVE,
    ATUC_MODIFY,
    ATUC_MOVE,
    ATUC_CLONE,
    ATUC_COUNT
};

class TActionTrigger;
class TActionTriggersModel;

class TActionTriggersUndoCommand : public QUndoCommand
{
public:
    TActionTriggersUndoCommand(ActionTriggerUndoCommand command,
                       TActionTriggersModel *actionTriggersModel,
                       const QList<TActionTrigger*> &actionTriggerList,
                       int pos = -1,
                       QUndoCommand *parent = nullptr);
    TActionTriggersUndoCommand(ActionTriggerUndoCommand command,
                       TActionTriggersModel *actionTriggersModel,
                       TActionTrigger *actionTrigger,
                       const TActionTrigger &newActionTrigger,
                       QUndoCommand *parent = nullptr);
    ~TActionTriggersUndoCommand();

    void undo() Q_DECL_OVERRIDE;
    void redo() Q_DECL_OVERRIDE;

private:
    TActionTriggersModel *mActionTriggersModel;
    QList<TActionTrigger*> mActionTriggerList;
    QList<TActionTrigger*> mActionTriggerListBackup;
    int mPos;
    TActionTrigger *mActionTrigger;
    TActionTrigger *mNewActionTrigger;
    ActionTriggerUndoCommand mCommand;
};


#endif // ACTIONTRIGGERSUNDOCOMMAND_H
