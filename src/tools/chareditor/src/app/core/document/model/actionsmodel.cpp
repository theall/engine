#include "actionsmodel.h"

#include <QColor>

static const int COLUMN_FRAMES_COUNT = 0;
static const int COLUMN_ACTION_NAME = 1;

TActionsModel::TActionsModel(QObject *parent) :
    QAbstractTableModel(parent)
{

}

TActionsModel::~TActionsModel()
{
    clear();
}

Model::TActionList TActionsModel::toActionList() const
{
    Model::TActionList actionList;
    for(TAction *action : mActionList)
    {
        actionList.emplace_back(action->toModel());
    }
    return actionList;
}

void TActionsModel::clear()
{
    FREE_CONTAINER(mActionList);
}

QList<TAction *> TActionsModel::actionList() const
{
    return mActionList;
}

int TActionsModel::addAction(TAction *action, int index)
{
    if(index<0) {
        mActionList.append(action);
        index = mActionList.size() - 1;
    } else {
        mActionList.insert(index, action);
    }
    connect(action, SIGNAL(nameChanged(QString)), this, SLOT(slotActionNameChanged(QString)));
    emit layoutChanged();
    return index;
}

int TActionsModel::removeAction(TAction *action)
{
    QList<TAction *> actionList;
    actionList.append(action);
    QList<int> indexRemoved = removeActions(actionList);
    if(indexRemoved.size() > 0)
        return indexRemoved.first();
    return -1;
}

int TActionsModel::removeAction(int index)
{
    QList<int> actionIndexList;
    actionIndexList.append(index);
    QList<int> indexRemoved = removeActions(actionIndexList);
    if(indexRemoved.size() > 0)
        return indexRemoved.first();
    return -1;
}

QList<int> TActionsModel::removeActions(const QList<TAction *> &actions)
{
    QList<int> removedIndex;
    for(TAction *action : actions)
    {
        int i = mActionList.indexOf(action);
        if(i != -1) {
            action->disconnect(this);
            mActionList.removeAt(i);
            removedIndex.append(i);
        }
    }
    if(removedIndex.size() > 0)
        emit layoutChanged();

    return removedIndex;
}

QList<int> TActionsModel::removeActions(const QList<int> &actionList)
{
    QList<TAction*> realActionList;
    for(int index : actionList)
    {
        TAction *action = getAction(index);
        if(action)
            realActionList.append(action);
    }
    return removeActions(realActionList);
}

TAction *TActionsModel::getAction(int index)
{
    if(index>=0 && index<mActionList.size())
        return mActionList.at(index);
    return nullptr;
}

int TActionsModel::count()
{
    return mActionList.count();
}

void TActionsModel::slotActionNameChanged(const QString &newName)
{
    Q_UNUSED(newName);

    emit layoutChanged();
}

TAction *TActionsModel::getAction(const QModelIndex &index)
{
    return getAction(index.row());
}

int TActionsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return mActionList.size();
}

int TActionsModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return 2;
}

QVariant TActionsModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if(row>=0 && row<mActionList.size())
    {
        int col = index.column();
        TAction *action = mActionList.at(row);
        if(role==Qt::DisplayRole)
        {
            if(col==COLUMN_FRAMES_COUNT)
                return QString("(%1)").arg(action->framesCount());
            else if(col==COLUMN_ACTION_NAME)
                return action->name();
        } else if (role==Qt::EditRole) {
            return action->name();
        } else if(role==Qt::TextColorRole) {
            if(col==COLUMN_FRAMES_COUNT)
                return QColor(Qt::blue);
        }
    }
    return QVariant();
}

Qt::ItemFlags TActionsModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags f = QAbstractTableModel::flags(index);
    if(index.column() == COLUMN_ACTION_NAME)
        f |= Qt::ItemIsEditable;
    return f;
}

bool TActionsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(index.column()==COLUMN_ACTION_NAME && role==Qt::EditRole)
    {
        TAction *action = getAction(index);
        if(action)
        {
            action->setName(value.toString());
            return true;
        }
    }
    return false;
}
