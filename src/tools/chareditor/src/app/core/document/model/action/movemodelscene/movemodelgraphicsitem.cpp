#include "movemodelgraphicsitem.h"
#include "../framesmodel/frame/frame.h"

#include <QWidget>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

TMoveModelFrameItem::TMoveModelFrameItem(const QColor &fillColor, QGraphicsItem *parent) :
    QGraphicsRectItem(parent)
  , mFillColor(fillColor)
{
    setRect(-1.5, -1.5, 3, 3);
    setBrush(QBrush(fillColor));
    setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsFocusable);
}

TMoveModelFrameItem::~TMoveModelFrameItem()
{

}

bool TMoveModelFrameItem::isLocked() const
{
    return mIsLocked;
}

void TMoveModelFrameItem::setLocked(bool isLocked)
{
    mIsLocked = isLocked;
}

TMoveModelKeyFrameItem::TMoveModelKeyFrameItem(TFrame *frame, QGraphicsItem *parent) :
    TMoveModelFrameItem(QColor(Qt::red), parent)
  , mFrame(frame)
{
    Q_ASSERT(mFrame);
}

TMoveModelKeyFrameItem::~TMoveModelKeyFrameItem()
{

}

QPointF TMoveModelKeyFrameItem::vector(int index) const
{
    return mFrame->vectorTableModel()->vectorAt(index);
}

QPointF TMoveModelKeyFrameItem::anchor() const
{
    return mFrame->anchor();
}

int TMoveModelKeyFrameItem::duration() const
{
    return mFrame->duration();
}

bool TMoveModelKeyFrameItem::antiGravity() const
{
    return mFrame->antiGravity();
}

QPixmap TMoveModelKeyFrameItem::pixmap() const
{
    return mFrame->pixmap()->pixmap();
}

TMoveModelReferenceFrameItem::TMoveModelReferenceFrameItem(TMoveModelKeyFrameItem *keyFrameItem, int index, QGraphicsItem *parent) :
    TMoveModelFrameItem(QColor(Qt::green), parent)
  , mIndex(index)
  , mKeyFrame(keyFrameItem)
{

}

TMoveModelReferenceFrameItem::~TMoveModelReferenceFrameItem()
{

}

TMoveModelKeyFrameItem *TMoveModelReferenceFrameItem::keyFrame() const
{
    return mKeyFrame;
}

void TMoveModelReferenceFrameItem::setKeyFrame(TMoveModelKeyFrameItem *keyFrame)
{
    mKeyFrame = keyFrame;
}

QPointF TMoveModelReferenceFrameItem::vector() const
{
    if(mKeyFrame)
        return mKeyFrame->vector(mIndex);
    return QPointF(0.0,0.0);
}

QPixmap TMoveModelReferenceFrameItem::pixmap() const
{
    if(mKeyFrame)
        return mKeyFrame->pixmap();

    return QPixmap();
}

QPointF TMoveModelReferenceFrameItem::anchor() const
{
    if(mKeyFrame)
        return mKeyFrame->anchor();

    return QPointF();
}

TMousePosTraceItem::TMousePosTraceItem(QGraphicsItem *parent) :
    QGraphicsTextItem(parent)
{
    setDefaultTextColor(QColor(0,0,250));
}

TMousePosTraceItem::~TMousePosTraceItem()
{

}

void TMousePosTraceItem::setPos(const QPointF &pos, const QPointF &orginPos)
{
    qreal posX = pos.x();
    qreal posY = pos.y();
    setPlainText(QString("%1 %2").arg(posX-orginPos.x()).arg(orginPos.y()-posY));

    QPointF newPos = pos;
    newPos.setX(posX-boundingRect().width()/2);
    newPos.setY(posY-boundingRect().height()-2);
    QGraphicsTextItem::setPos(newPos);
}
