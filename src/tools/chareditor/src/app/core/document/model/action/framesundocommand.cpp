#include "framesundocommand.h"
#include "../action/framesmodel/framesmodel.h"

#include <QCoreApplication>

#define tr(x) QCoreApplication::translate("UndoCommand", x)

const QString g_commandText[FUC_COUNT] = {
    tr("Add %1 frame(s)"),
    tr("Remove %1 frame(s)"),
    tr("Move %1 frame(s)"),
    tr("Clone %1 frame(s)")
};

TFramesUndoCommand::TFramesUndoCommand(FrameUndoCommand command,
        TFramesModel *framesModel,
        const QList<TFrame *> &frameList,
        int pos,
        QUndoCommand *parent) :
    QUndoCommand(parent)
  , mFramesModel(framesModel)
  , mFrameList(frameList)
  , mPos(pos)
  , mCommand(command)
{
    setText(g_commandText[command].arg(frameList.size()));

    if(mIndexList.isEmpty())
    {
        for(int i=0;i<mFrameList.size();i++)
        {
            mIndexList.append(-1);
        }
    }
}

TFramesUndoCommand::~TFramesUndoCommand()
{

}

void TFramesUndoCommand::undo()
{
    if(mCommand==FUC_ADD)
    {
        mIndexList = mFramesModel->removeFrames(mFrameList);
    } else if(mCommand==FUC_REMOVE) {
        mFramesModel->addFrames(mFrameList, mIndexList);
    } else if(mCommand==FUC_MOVE) {
        mFramesModel->moveFrames(mFrameList, mIndexList);
    }
}

void TFramesUndoCommand::redo()
{
    if(mCommand==FUC_ADD)
    {
        mFramesModel->addFrames(mFrameList, mIndexList);
    } else if(mCommand==FUC_REMOVE) {
        mIndexList = mFramesModel->removeFrames(mFrameList);
    } else if(mCommand==FUC_MOVE) {
        mIndexList = mFramesModel->getFramesIndexList(mFrameList);
        mFramesModel->moveFrames(mFrameList, mPos);
    }
}

