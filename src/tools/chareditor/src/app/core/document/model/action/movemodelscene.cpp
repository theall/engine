#include "movemodelscene.h"
#include "framesmodel/framesmodel.h"
#include "../../../base/property.h"

#include <QPainter>
#include <QWheelEvent>
#include <QCoreApplication>
#include <QGraphicsSceneMouseEvent>

static const int TILE_WIDTH = 16;
static const int TILE_HEIGHT = 16;
static const int CALIBRATION_LINE_WIDTH = 2;
static const int CALIBRATION_NUMBER_WITH = 48;
static const int CALIBRATION_NUMBER_FONT_SIZE = 8;
static const int PADING_WIDTH = 4;

TMoveModelScene::TMoveModelScene(TFramesModel *framesModel, QObject *parent) :
    QGraphicsScene(parent)
  , mGravity(3)
  , mTimerId(-1)
  , mFps(60)
  , mStepMode(false)
  , mScale(1.0)
  , mFramesModel(framesModel)
{
    setSize(640, 480);

    Q_ASSERT(framesModel);

    connect(framesModel, SIGNAL(framesChanged()), this, SLOT(slotFramesChanged()));
    connect(framesModel, SIGNAL(frameChanged(TFrame*)), this, SLOT(slotFrameChanged(TFrame*)));
    connect(framesModel,
            SIGNAL(framePropertyChanged(TFrame*,int,QVariant)),
            this,
            SLOT(slotFramePropertyChanged(TFrame*,int,QVariant)));

    calcOrbit();
}

TMoveModelScene::~TMoveModelScene()
{

}

void TMoveModelScene::setSize(qreal w, qreal h)
{
    setSceneRect(0.0, 0.0, w, h);
    update();
}

void TMoveModelScene::setSize(const QSizeF &size)
{
    setSize(size.width(), size.height());
}

void TMoveModelScene::moveItem(QGraphicsItem *item, const QPointF &pos)
{
    moveItem(item, pos.x(), pos.y());
}

void TMoveModelScene::moveItem(QGraphicsItem *item, int x, int y)
{
    if(item)
    {
        item->setPos(x+mOrginPos.x(), y+mOrginPos.y());
        item->setToolTip(QString("%1 %2").arg(x).arg(y));
    }
}

void TMoveModelScene::calcOrbit()
{
    stop();
    clear();
    addMousePosItem();
    addFramePixmapItem();

    mOrginPos.setX(width() / 2);
    mOrginPos.setY(height() / 2);


    QPointF orginPos(0,0);
    QPointF vector;
    QRectF mapRect(sceneRect());
    int keyFrameIndex = 1;
    // Create frame items
    mFramesList.clear();
    for(TFrame *frame : mFramesModel->getFramesList())
    {
        TMoveModelKeyFrameItem *keyFrameItem = new TMoveModelKeyFrameItem(frame);
        orginPos += vector;
        addItem(keyFrameItem);
        moveItem(keyFrameItem, orginPos);
        vector += keyFrameItem->vector();
        keyFrameItem->setToolTip(tr("Pos: %1,%2\nVector: %3,%4\nFrame index: %5\nVector index: 1")
                                 .arg(orginPos.x())
                                 .arg(orginPos.y())
                                 .arg(vector.x())
                                 .arg(vector.y())
                                 .arg(keyFrameIndex));
        TMoveModelReferenceFrameItemList refFramesList;
        for(int i=1;i<keyFrameItem->duration();i++)
        {
            TMoveModelReferenceFrameItem *refItem = new TMoveModelReferenceFrameItem(keyFrameItem, i);
            addItem(refItem);
//            if(!item->antiGravity())
//            {
//                vector.ry() += mGravity*i;
//            }
            orginPos += vector;
            moveItem(refItem, orginPos);
            vector += refItem->vector();
            refItem->setToolTip(tr("Pos: %1,%2\nVector: %3,%4\nFrame index: %5\nVector index: %6")
                                .arg(orginPos.x())
                                .arg(orginPos.y())
                                .arg(vector.x())
                                .arg(vector.y())
                                .arg(keyFrameIndex)
                                .arg(i+1));
            refFramesList.append(refItem);

            QSize pixmapSize = refItem->keyFrame()->pixmap().size();
            mapRect = mapRect.united(QRectF(refItem->pos(), pixmapSize));
        }
        mFramesList.append(qMakePair(keyFrameItem, refFramesList));
        keyFrameIndex++;
    }

    setSceneRect(mapRect);
    update();
}

void TMoveModelScene::play()
{
    if(mFramesList.size() < 1)
        return;

    if(mTimerId != -1)
        killTimer(mTimerId);

    mAbstractFramesList.clear();
    for(TKeyRefFramePair keyRefFramePair : mFramesList)
    {
        mAbstractFramesList.append(keyRefFramePair.first);
        for(TMoveModelReferenceFrameItem *refItem : keyRefFramePair.second)
        {
            mAbstractFramesList.append(refItem);
        }
    }
    mStepMode = false;
    //mCurrentPlayIndex = 0;
    mTimerId = startTimer(1000.0/mFps);
}

bool TMoveModelScene::isPlaying()
{
    return mTimerId != -1;
}

void TMoveModelScene::stop()
{
    if(mTimerId != -1)
    {
        killTimer(mTimerId);
        mTimerId = -1;
        //mPixmapItem->setVisible(false);
    }
}

int TMoveModelScene::gravity() const
{
    return mGravity;
}

void TMoveModelScene::setGravity(int gravity)
{
    mGravity = gravity;
}

int TMoveModelScene::fps() const
{
    return mFps;
}

void TMoveModelScene::setFps(int fps)
{
    if(mFps==fps)
        return;

    mFps = fps;

    if(isPlaying()) {
        stop();
        play();
    }
}

qreal TMoveModelScene::scale() const
{
    return mScale;
}

void TMoveModelScene::setScale(const qreal &scale)
{
    mScale = scale;
}

void TMoveModelScene::selectFrame(int frameIndex, int vectorIndex)
{
    if(frameIndex<0 || frameIndex>=mFramesList.size())
        return;

    TKeyRefFramePair framePair = mFramesList.at(frameIndex);
    TMoveModelFrameItem *targetItem = nullptr;
    if(vectorIndex==0)
    {
        targetItem = framePair.first;
    } else if(vectorIndex < framePair.second.size()) {
        targetItem = framePair.second.at(vectorIndex);
    } else {
        return;
    }
    targetItem->setSelected(true);
}

void TMoveModelScene::addMousePosItem()
{
    mMousePosItem = (QGraphicsItem*)new TMousePosTraceItem;
    addItem(mMousePosItem);
}

void TMoveModelScene::addFramePixmapItem()
{
    mPixmapItem = new QGraphicsPixmapItem;
    addItem(mPixmapItem);
    mPixmapItem->setZValue(999);
}

void TMoveModelScene::step()
{
    if(mCurrentPlayIndex < 0)
        mCurrentPlayIndex = 0;

    if(mCurrentPlayIndex >= mAbstractFramesList.size())
        mCurrentPlayIndex = 0;

    TMoveModelFrameItem *item = mAbstractFramesList.at(mCurrentPlayIndex);
    if(item && mPixmapItem)
    {
        QPixmap p = item->pixmap();
        if(!p.isNull())
        {
            mPixmapItem->setPixmap(p);
            QPointF pos = item->pos() - item->anchor();
            mPixmapItem->setPos(pos);
        }
        if(!mPixmapItem->isVisible())
            mPixmapItem->setVisible(true);
    }

    mCurrentPlayIndex++;
}

void TMoveModelScene::slotFramesChanged()
{
    refresh();
}

void TMoveModelScene::slotFrameChanged(TFrame *frame)
{
    Q_UNUSED(frame);
    refresh();
}

void TMoveModelScene::slotFramePropertyChanged(TFrame *frame, int propertyId, const QVariant &value)
{
    Q_UNUSED(frame);
    Q_UNUSED(propertyId);
    Q_UNUSED(value);

    if(propertyId==PID_FRAME_IMAGE
        || propertyId==PID_FRAME_ANCHOR
        || propertyId==PID_FRAME_DURATION
        || propertyId==PID_FRAME_ANTI_GRAVITY
        || propertyId==PID_VECTOR_ITEM_TYPE
        || propertyId==PID_VECTOR_ITEM_VECTOR)
    refresh();
}

void TMoveModelScene::refresh()
{
    bool playing = isPlaying();
    calcOrbit();
    if(playing)
        play();
}

void TMoveModelScene::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);

    int sceneWidth = width();
    int sceneHeight = height();
    int startX = mOrginPos.x();
    int startY = mOrginPos.y();
    QColor gridColor(0, 0, 0, 128);

    // Draw axis x
    QPen penAxisX(gridColor);
    QVector<qreal> _x;
    _x.append(2.0);
    _x.append(2.0);
    penAxisX.setDashPattern(_x);
    penAxisX.setCosmetic(true);
    painter->setPen(penAxisX);
    painter->drawLine(0, startY, sceneWidth, startY);

    // Draw axis y
    QPen penAxisY(gridColor);
    penAxisY.setCosmetic(false);
    penAxisY.setWidth(1);
    painter->setPen(penAxisY);
    painter->drawLine(startX, 0, startX, sceneHeight);

    QFont ft = painter->font();
    ft.setPixelSize(CALIBRATION_NUMBER_FONT_SIZE);
    painter->setFont(ft);

    penAxisY.setWidthF(.5);
    painter->setPen(penAxisY);
    // Draw positive calibration line
    for(int y=startY;y>=0;y-=TILE_HEIGHT)
    {
        painter->drawText(startX-CALIBRATION_NUMBER_WITH,
                          y-TILE_HEIGHT/2,
                          CALIBRATION_NUMBER_WITH-PADING_WIDTH,
                          TILE_HEIGHT,
                          Qt::AlignRight|Qt::AlignVCenter,
                          QString::number(y-startY));
        painter->drawLine(startX, y, startX+CALIBRATION_LINE_WIDTH, y);
    }

    // Draw negative calibration line
    for(int y=startY+TILE_HEIGHT;y<=sceneHeight;y+=TILE_HEIGHT)
    {
        painter->drawText(startX-CALIBRATION_NUMBER_WITH,
                          y-TILE_HEIGHT/2,
                          CALIBRATION_NUMBER_WITH-PADING_WIDTH,
                          TILE_HEIGHT,
                          Qt::AlignRight|Qt::AlignVCenter,
                          QString::number(y-startY));
        painter->drawLine(startX, y, startX+CALIBRATION_LINE_WIDTH, y);
    }
}

void TMoveModelScene::drawForeground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(painter);
    Q_UNUSED(rect);
}

void TMoveModelScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(event->button()==Qt::RightButton)
    {
        if(mTimerId==-1)
            play();
        else
            stop();
    }
}

void TMoveModelScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    TMousePosTraceItem *item = (TMousePosTraceItem*)mMousePosItem;
    if(event->modifiers()&Qt::ControlModifier)
    {
        mMousePosItem->setVisible(true);
    } else {
        mMousePosItem->setVisible(false);
    }
    item->setPos(event->scenePos(), mOrginPos);
}

void TMoveModelScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
}

void TMoveModelScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
}

void TMoveModelScene::keyPressEvent(QKeyEvent *event)
{
    int key = event->key();
    if(key == Qt::Key_Space)
    {
        if(!mStepMode) {
            mStepMode = true;
            if(isPlaying())
                stop();
        } else {
            step();
        }
    } else if(event->modifiers()&Qt::ControlModifier)
        mMousePosItem->setVisible(true);

    QGraphicsScene::keyPressEvent(event);
}

void TMoveModelScene::keyReleaseEvent(QKeyEvent *event)
{
    mMousePosItem->setVisible(false);

    QGraphicsScene::keyReleaseEvent(event);
}

void TMoveModelScene::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);

    step();
}
