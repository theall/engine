#include "actiontriggerundocommand.h"
#include "../action/triggersmodel/actionTriggersmodel.h"

#include <QCoreApplication>

#define tr(x) QCoreApplication::translate("UndoCommand", x)

const QString g_commandText[ATUC_COUNT] = {
    tr("Add %1 actionTrigger(s)"),
    tr("Remove %1 actionTrigger(s)"),
    tr("Modify %1 actionTrigger(s)"),
    tr("Move %1 actionTrigger(s)"),
    tr("Clone %1 actionTrigger(s)")
};

TActionTriggersUndoCommand::TActionTriggersUndoCommand(ActionTriggerUndoCommand command,
        TActionTriggersModel *actionTriggersModel,
        const QList<TActionTrigger *> &actionTriggerList,
        int pos,
        QUndoCommand *parent) :
    QUndoCommand(parent)
  , mActionTriggersModel(actionTriggersModel)
  , mActionTriggerList(actionTriggerList)
  , mPos(pos)
  , mActionTrigger(nullptr)
  , mNewActionTrigger(nullptr)
  , mCommand(command)
{
    setText(g_commandText[command].arg(actionTriggerList.size()));
}

TActionTriggersUndoCommand::TActionTriggersUndoCommand(
        ActionTriggerUndoCommand command,
        TActionTriggersModel *actionTriggersModel,
        TActionTrigger *actionTrigger,
        const TActionTrigger &newActionTrigger,
        QUndoCommand *parent) :
    QUndoCommand(parent)
  , mActionTriggersModel(actionTriggersModel)
  , mActionTrigger(actionTrigger)
  , mNewActionTrigger(new TActionTrigger)
  , mCommand(command)
{
    Q_ASSERT(command==ATUC_MODIFY);
    Q_ASSERT(actionTrigger);
    *mNewActionTrigger = newActionTrigger;

    setText(g_commandText[command].arg(1));
}

TActionTriggersUndoCommand::~TActionTriggersUndoCommand()
{
    mNewActionTrigger->deleteLater();
}

void TActionTriggersUndoCommand::undo()
{
    if(mCommand==ATUC_ADD)
    {
        mActionTriggersModel->removeActionTriggers(mActionTriggerList);
    } else if(mCommand==ATUC_REMOVE) {
        mActionTriggersModel->setActionTriggersList(mActionTriggerListBackup);
    } else if(mCommand==ATUC_MOVE) {
//        mActionTriggersModel->moveActionTriggers(mActionTriggerList);
    } else if(mCommand==ATUC_MODIFY) {
        TActionTrigger oldActionTrigger(*mActionTrigger);
        if(mActionTriggersModel->updateActionTrigger(mActionTrigger, *mNewActionTrigger))
        {
            *mNewActionTrigger = oldActionTrigger;
        }
    }
}

void TActionTriggersUndoCommand::redo()
{
    if(mCommand==ATUC_ADD)
    {
        mActionTriggersModel->addActionTriggers(mActionTriggerList);
    } else if(mCommand==ATUC_REMOVE) {
        mActionTriggerListBackup = mActionTriggersModel->getActionTriggersList();
        mActionTriggersModel->removeActionTriggers(mActionTriggerList);
    } else if(mCommand==ATUC_MOVE) {
//        mActionTriggersModel->getActionTriggersIndexList(mActionTriggerList);
//        mActionTriggersModel->moveActionTriggers(mActionTriggerList, mPos);
    } else if(mCommand==ATUC_MODIFY) {
        TActionTrigger oldActionTrigger(*mActionTrigger);
        if(mActionTriggersModel->updateActionTrigger(mActionTrigger, *mNewActionTrigger))
        {
           * mNewActionTrigger = oldActionTrigger;
        }
    }
}

