#include "actiontriggersmodel.h"

#include "../../../../base/docutil.h"

const int COL_EVENT = 0;
const int COL_VALUE = 1;

TActionTriggersModel::TActionTriggersModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    mActionTriggersList.append(new TActionTrigger(this));
}

TActionTriggersModel::~TActionTriggersModel()
{

}

Model::TActionTriggerList TActionTriggersModel::toTriggerList() const
{
    Model::TActionTriggerList triggerList;
    for(TActionTrigger *actionTrigger : mActionTriggersList)
    {
        triggerList.emplace_back(actionTrigger->toModel());
    }
    return triggerList;
}

void TActionTriggersModel::loadFromModel(const Model::TActionTriggerList actionTriggersList, void *context)
{
    FREE_CONTAINER(mActionTriggersList);
    for(Model::TActionTrigger *actionTriggerModel : actionTriggersList)
    {
        TActionTrigger *actionTrigger = new TActionTrigger(this);
        actionTrigger->loadFromModel(*actionTriggerModel, context);
        internalAddActionTrigger(actionTrigger);
    }
    if(actionTriggersList.size() > 0)
        updateAndFire();
}

int TActionTriggersModel::addActionTrigger(TActionTrigger *actionTrigger, int index)
{
    index = internalAddActionTrigger(actionTrigger, index);
    updateAndFire();
    return index;
}

int TActionTriggersModel::removeActionTrigger(TActionTrigger *actionTrigger)
{
    int i = internalRemoveActionTrigger(actionTrigger);
    if(i != -1)
        updateAndFire();
    return i;
}

int TActionTriggersModel::removeActionTrigger(int index)
{
    TActionTrigger *actionTrigger = getActionTrigger(index);
    if(!actionTrigger)
        return -1;

    return removeActionTrigger(actionTrigger);
}

bool TActionTriggersModel::updateActionTrigger(TActionTrigger *actionTrigger, const TActionTrigger &newActionTrigger)
{
    int i = mActionTriggersList.indexOf(actionTrigger);
    if(i==-1)
        return false;

    *actionTrigger = newActionTrigger;
    emit layoutChanged();
    return true;
}

QList<int> TActionTriggersModel::addActionTriggers(const QList<TActionTrigger *> &actionTriggers, const QList<int> &indexes)
{
    QList<int> indexList = indexes;
    int actionTriggerSize = actionTriggers.size();
    if(actionTriggerSize != indexList.size())
    {
        indexList.clear();
        for(int i=0;i<actionTriggerSize;i++)
        {
            indexList.append(-1);
        }
    }
    for(int i=0;i<actionTriggerSize;i++)
    {
        internalAddActionTrigger(actionTriggers[i], indexList[i]);
    }
    if(actionTriggerSize > 0)
        updateAndFire();

    return indexList;
}

QList<int> TActionTriggersModel::removeActionTriggers(const QList<TActionTrigger *> &actionTriggers)
{
    QList<int> removedIndex;
    for(TActionTrigger *actionTrigger : actionTriggers)
    {
        int i = internalRemoveActionTrigger(actionTrigger);
        if(i != -1)
            removedIndex.append(i);
    }
    if(removedIndex.size() > 0)
        updateAndFire();

    return removedIndex;
}

QList<int> TActionTriggersModel::removeActionTriggers(const QList<int> &actionTriggerList)
{
    QList<int> removedIndex;
    for(int index : actionTriggerList)
    {
        int i = internalRemoveActionTrigger(getActionTrigger(index));
        if(i != -1)
            removedIndex.append(i);
    }
    if(removedIndex.size() > 0)
        updateAndFire();

    return removedIndex;
}

int TActionTriggersModel::internalAddActionTrigger(TActionTrigger *actionTrigger, int index)
{
    if(!actionTrigger)
        return -1;

    if(index<0)
        index = mActionTriggersList.size();

    beginInsertRows(QModelIndex(), index, index);
    mActionTriggersList.insert(index, actionTrigger);
    endInsertRows();

    return index;
}

int TActionTriggersModel::internalRemoveActionTrigger(TActionTrigger *actionTrigger)
{
    int i = mActionTriggersList.indexOf(actionTrigger);
    if(i != -1) {
        beginRemoveRows(QModelIndex(), i, i);
        mActionTriggersList.removeAt(i);
        endRemoveRows();
        actionTrigger->disconnect(this);
    }
    return i;
}

void TActionTriggersModel::updateAndFire()
{
    emit triggersChanged();
    emit layoutChanged();
}

int TActionTriggersModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return mActionTriggersList.size();
}

int TActionTriggersModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);

    return 2;
}

QVariant TActionTriggersModel::data(const QModelIndex &index, int role) const
{
    if(role==Qt::DisplayRole)
    {
        int row = index.row();
        int col = index.column();
        if(row>=0 && row<mActionTriggersList.size())
        {
            TActionTrigger *actionTrigger = mActionTriggersList.at(row);
            switch (col) {
            case COL_EVENT:
                return DocUtil::GameEventToQString(actionTrigger->gameEvent());
                break;
            case COL_VALUE:
                return actionTrigger->value();
                break;
            default:
                break;
            }
        }
    }
    return QVariant();
}

TActionTrigger *TActionTriggersModel::getActionTrigger(int index)
{
    if(index>=0 && index<mActionTriggersList.size())
        return mActionTriggersList.at(index);

    return nullptr;
}

TActionTriggerList TActionTriggersModel::getActionTriggersList() const
{
    return mActionTriggersList;
}

void TActionTriggersModel::setActionTriggersList(const TActionTriggerList &actionTriggersList)
{
    if(mActionTriggersList==actionTriggersList)
        return;

    mActionTriggersList = actionTriggersList;

    updateAndFire();
}

Qt::ItemFlags TActionTriggersModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);

    return QAbstractTableModel::flags(index)
            | Qt::ItemIsSelectable;
}

QVariant TActionTriggersModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role==Qt::DisplayRole)
    {
        if(orientation==Qt::Horizontal)
        {
            switch (section) {
                case COL_EVENT:
                    return tr("Event");
                    break;
                case COL_VALUE:
                    return tr("Value");
                    break;
                default:
                    break;
            };
        } else if(orientation==Qt::Vertical) {
            if(section>=0 && section<mActionTriggersList.size())
                return section+1;
        }
    }

    return QAbstractTableModel::headerData(section, orientation, role);
}
