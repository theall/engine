#ifndef DOCUMENT_ACTIONTRIGGER_H
#define DOCUMENT_ACTIONTRIGGER_H

#include "../../../../../../../../../shared/gameutils/model/character/action.h"

#include <QObject>

class TActionTrigger : public QObject
{
    Q_OBJECT

public:
    TActionTrigger(QObject *parent=nullptr);
    TActionTrigger(const TActionTrigger &actionTrigger);
    ~TActionTrigger();

    TActionTrigger &operator =(const TActionTrigger &actionTrigger);

    Model::TActionTrigger *toModel() const;
    void loadFromModel(const Model::TActionTrigger &actionTriggerModel, void *context=nullptr);

    QString value() const;
    void setValue(const QString &value);

    SpriteState spriteState() const;
    void setSpriteState(const SpriteState &spriteState);

    GameEvent gameEvent() const;
    void setGameEvent(const GameEvent &gameEvent);

signals:

private:
    GameEvent mGameEvent;
    SpriteState mSpriteState;
    QString mValue;
};
typedef QList<TActionTrigger*> TActionTriggerList;

#endif // DOCUMENT_ACTIONTRIGGER_H
