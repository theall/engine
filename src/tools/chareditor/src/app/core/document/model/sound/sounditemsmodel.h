#ifndef SOUNDITEMSMODEL_H
#define SOUNDITEMSMODEL_H

#include <QAbstractTableModel>
#include "sounditem.h"

class TSoundItemsModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    TSoundItemsModel(QObject *parent = nullptr);
    ~TSoundItemsModel();

    TSoundItemList soundItemList() const;
    TSoundItemList getSoundItemList() const;
    TSoundItem *getSoundItem(int index) const;
    int soundItemsCount() const;
    void setSoundItemList(const TSoundItemList &soundItemList);

    int addSoundItem(TSoundItem *soundItem, int index = -1);
    QList<int> addSoundItemList(const TSoundItemList &soundItemList, const QList<int> &indexList = QList<int>());
    int removeSoundItem(TSoundItem *soundItem);
    QList<int> removeSoundItemList(const TSoundItemList &soundItemList);

    void cmdAddSound(const QString &file);
    void cmdAddSound(TSound *sound);
    void cmdAddSound(TSoundItem *soundItem);
    void cmdAddSoundList(const QStringList &soundFileList);
    void cmdAddSoundList(const TSoundList &soundList);
    void cmdAddSoundList(const TSoundItemList &soundItemList);
    void cmdRemoveSound(TSoundItem *soundItem);
    void cmdRemoveSoundList(const TSoundItemList &soundItemList);

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

signals:
    void soundItemsAdded(const TSoundItemList &soundItemList, const QList<int> indexList);
    void soundItemsRemoved(const TSoundItemList &soundItemList, const QList<int> indexList);
    void soundItemsCountChanged(int oldCount, int currentCount);

private slots:
    void slotSoundItemPropertyChanged(PropertyID id, const QVariant &value);

private:
    TDocument *mDocument;
    TSoundItemList mSoundItemList;

    int internalAddSoundItem(TSoundItem *soundItem, int index = -1);
    int internalRemoveSoundItem(TSoundItem *soundItem);
};

QDataStream &operator<<(QDataStream& out, const TSoundItemsModel& soundItemsModel);
QDataStream &operator>>(QDataStream& in, TSoundItemsModel& soundItemsModel);

#endif // SOUNDITEMSMODEL_H
