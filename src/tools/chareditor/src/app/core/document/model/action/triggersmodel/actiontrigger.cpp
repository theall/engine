#include "actiontrigger.h"

#include "../../../../base/docutil.h"

TActionTrigger::TActionTrigger(QObject *parent) :
    QObject(parent)
  , mGameEvent(GE_NONE)
{

}

TActionTrigger::TActionTrigger(const TActionTrigger &actionTrigger) :
    QObject(actionTrigger.parent())
{
    mGameEvent = actionTrigger.gameEvent();
    mValue = actionTrigger.value();
}

TActionTrigger::~TActionTrigger()
{

}

TActionTrigger &TActionTrigger::operator =(const TActionTrigger &actionTrigger)
{
    mGameEvent = actionTrigger.gameEvent();
    mValue = actionTrigger.value();

    return *this;
}

Model::TActionTrigger *TActionTrigger::toModel() const
{
    Model::TActionTrigger *trigger = new Model::TActionTrigger;
    trigger->setEvent(mGameEvent);
    trigger->setValue(mValue.toStdString());

    return trigger;
}

void TActionTrigger::loadFromModel(const Model::TActionTrigger &actionTriggerModel, void *context)
{
    Q_UNUSED(context);

    mGameEvent = actionTriggerModel.event();
    mValue = QString::fromStdString(actionTriggerModel.value());
}

QString TActionTrigger::value() const
{
    return mValue;
}

SpriteState TActionTrigger::spriteState() const
{
    return mSpriteState;
}

GameEvent TActionTrigger::gameEvent() const
{
    return mGameEvent;
}

void TActionTrigger::setGameEvent(const GameEvent &gameEvent)
{
    mGameEvent = gameEvent;
}

void TActionTrigger::setSpriteState(const SpriteState &spriteState)
{
    mSpriteState = spriteState;
}

void TActionTrigger::setValue(const QString &value)
{
    mValue = value;
}
