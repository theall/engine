#ifndef ACTIONTRIGGERSMODEL_H
#define ACTIONTRIGGERSMODEL_H

#include <QAbstractTableModel>

#include "actiontrigger.h"

class TActionTriggersModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    TActionTriggersModel(QObject *parent = nullptr);
    ~TActionTriggersModel();

    Model::TActionTriggerList toTriggerList() const;

    void loadFromModel(const Model::TActionTriggerList actionTriggersList, void *context);

    int addActionTrigger(TActionTrigger *actionTrigger, int index=-1);
    int removeActionTrigger(TActionTrigger *actionTrigger);
    int removeActionTrigger(int index);
    bool updateActionTrigger(TActionTrigger *actionTrigger, const TActionTrigger &newActionTrigger);

    QList<int> addActionTriggers(const QList<TActionTrigger *> &actionTriggers, const QList<int> &indexList=QList<int>());
    QList<int> removeActionTriggers(const QList<TActionTrigger *> &actionTriggers);
    QList<int> removeActionTriggers(const QList<int> &actionTriggerList);

    TActionTrigger *getActionTrigger(int index);
    TActionTriggerList getActionTriggersList() const;
    void setActionTriggersList(const TActionTriggerList &actionTriggersList);

signals:
    void triggersChanged();

private slots:

private:
    TActionTriggerList mActionTriggersList;

    void updateAndFire();
    int internalAddActionTrigger(TActionTrigger *actionTrigger, int index=-1);
    int internalRemoveActionTrigger(TActionTrigger *actionTrigger);

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const Q_DECL_OVERRIDE;
};

#endif // ACTIONTRIGGERSMODEL_H
