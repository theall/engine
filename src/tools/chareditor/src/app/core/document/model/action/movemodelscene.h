#ifndef MOVEMODELSCENE_H
#define MOVEMODELSCENE_H

#include <QPair>
#include <QList>
#include <QGraphicsScene>
#include "movemodelscene/movemodelgraphicsitem.h"

class TFramesModel;

typedef QPair<TMoveModelKeyFrameItem*, TMoveModelReferenceFrameItemList> TKeyRefFramePair;

class TMoveModelScene : public QGraphicsScene
{
    Q_OBJECT

public:
    TMoveModelScene(TFramesModel *framesModel, QObject *parent = nullptr);
    ~TMoveModelScene();

    void setSize(qreal w, qreal h);
    void setSize(const QSizeF &size);
    void moveItem(QGraphicsItem *item, const QPointF &pos);
    void moveItem(QGraphicsItem *item, int x, int y);
    void calcOrbit();
    void play();
    bool isPlaying();
    void stop();

    int gravity() const;
    void setGravity(int gravity);

    int fps() const;
    void setFps(int fps);

    qreal scale() const;
    void setScale(const qreal &scale);
    void selectFrame(int frameIndex, int vectorIndex = 0);

private:
    int mGravity;
    QPointF mOrginPos;
    QGraphicsItem *mMousePosItem;
    QList<TKeyRefFramePair> mFramesList;
    QGraphicsPixmapItem *mPixmapItem;
    TMoveModelFrameItemList mAbstractFramesList;
    int mCurrentPlayIndex;
    int mTimerId;
    int mFps;
    bool mStepMode;
    qreal mScale;
    TFramesModel *mFramesModel;

    void addMousePosItem();
    void addFramePixmapItem();
    void step();

private slots:
    void slotFramesChanged();
    void slotFrameChanged(TFrame *frame);
    void slotFramePropertyChanged(TFrame *frame, int propertyId, const QVariant &value);

    void refresh();

    // QGraphicsScene interface
protected:
    void drawBackground(QPainter *painter, const QRectF &rect) Q_DECL_OVERRIDE;
    void drawForeground(QPainter *painter, const QRectF &rect) Q_DECL_OVERRIDE;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void keyReleaseEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

    // QObject interface
protected:
    void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;
};

#endif // MOVEMODELSCENE_H
