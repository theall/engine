#include "action.h"
#include "framesundocommand.h"
#include "actiontriggerundocommand.h"
#include "movemodelscene.h"
#include "framesmodel/frame/frame.h"

#include "../../document.h"
#include "../../../base/docutil.h"

static const char *P_NAME = "Name";
static const char *P_VECTOR = "Vector";

TAction::TAction(const QString &name=QString(), QObject *parent) :
    TPropertyObject(parent)
  , mName(name)
  , mTriggersModel(new TActionTriggersModel(this))
  , mFramesModel(new TFramesModel(this))
  , mDocument(nullptr)
  , mMoveModelScene(new TMoveModelScene(mFramesModel, this))
{
    setObjectName("Action");

    QObject *obj = parent;
    while (obj) {
        mDocument = qobject_cast<TDocument*>(obj);
        if(mDocument)
            break;
        obj = obj->parent();
    }
    if(!mDocument)
        throw QString("File:%1, Line:%2: Parent must be document.").arg(__FILE__).arg(__LINE__);

    initPropertySheet();
}

TAction::~TAction()
{

}

Model::TAction *TAction::toModel() const
{
    Model::TAction *action = new Model::TAction;
    action->setName(mPropertySheet->getValue(P_NAME).toString().toStdString());
    action->setVector(DocUtil::toVector2(mPropertySheet->getValue(P_VECTOR).toPointF()));
    action->setFramesList(mFramesModel->toFrameList());
    action->setTriggerList(mTriggersModel->toTriggerList());

    return action;
}

void TAction::loadFromModel(const Model::TAction &actionModel, void *context)
{
    TDocument *document = static_cast<TDocument*>(context);
    if(!document)
        return;

    (*mPropertySheet)[P_NAME]->setValue(QString::fromStdString(actionModel.getName()));
    (*mPropertySheet)[P_VECTOR]->setValue(DocUtil::toPointF(actionModel.getVector()));

    mTriggersModel->loadFromModel(actionModel.getTriggerList(), context);
    mFramesModel->loadFromModel(actionModel.getFramesList(), context);
}

QString TAction::name() const
{
    return (*mPropertySheet)[P_NAME]->value().toString();
}

void TAction::setName(const QString &name)
{
    (*mPropertySheet)[P_NAME]->setNewValue(name);
}

QPointF TAction::vector() const
{
    return (*mPropertySheet)[P_VECTOR]->value().toPointF();
}

void TAction::setVector(const QPointF &vector)
{
    (*mPropertySheet)[P_VECTOR]->setNewValue(vector);
}

void TAction::slotPropertyItemValueChanged(TPropertyItem *item, const QVariant &value)
{
    if(!item)
        return;

    PropertyID command = item->propertyId();
    switch (command) {
    case PID_ACTION_NAME:
        emit nameChanged(value.toString());
        break;
    default:
        break;
    }
}

void TAction::updateFramesBuddy()
{
    mFramesModel->updateFramesBuddy();
}

TActionTriggersModel *TAction::triggersModel() const
{
    return mTriggersModel;
}

void TAction::cmdAddActionTrigger(TActionTrigger* actionTrigger)
{
    QList<TActionTrigger*> actionTriggersList;
    actionTriggersList.append(actionTrigger);
    cmdAddActionTriggers(actionTriggersList);
}

void TAction::cmdRemoveActionTrigger(TActionTrigger* actionTrigger)
{
    QList<TActionTrigger*> actionTriggersList;
    actionTriggersList.append(actionTrigger);
    cmdRemoveActionTriggers(actionTriggersList);
}

void TAction::cmdRemoveActionTrigger(int index)
{
    QList<int> indexList;
    indexList.append(index);
    cmdRemoveActionTriggers(indexList);
}

void TAction::cmdModifyActionTrigger(TActionTrigger *actionTrigger, const TActionTrigger &newActionTrigger)
{
    TActionTriggersUndoCommand *command = new TActionTriggersUndoCommand(ATUC_MODIFY, mTriggersModel, actionTrigger, newActionTrigger);
    mDocument->addUndoCommand(command);
}

void TAction::cmdAddActionTriggers(const QList<TActionTrigger*> &actionTriggersList)
{
    TActionTriggersUndoCommand *command = new TActionTriggersUndoCommand(ATUC_ADD, mTriggersModel, actionTriggersList);
    mDocument->addUndoCommand(command);
}

void TAction::cmdRemoveActionTriggers(const QList<TActionTrigger*> &actionTriggersList)
{
    TActionTriggersUndoCommand *command = new TActionTriggersUndoCommand(ATUC_REMOVE, mTriggersModel, actionTriggersList);
    mDocument->addUndoCommand(command);
}

void TAction::cmdRemoveActionTriggers(const QList<int> &indexList)
{
    QList<TActionTrigger*> triggerList;
    for(int i : indexList)
    {
        triggerList.append(mTriggersModel->getActionTrigger(i));
    }
    cmdRemoveActionTriggers(triggerList);
}

void TAction::cmdMoveActionTriggers(const QList<int> &indexList, int pos)
{
    Q_UNUSED(indexList);
    Q_UNUSED(pos);
}

TMoveModelScene *TAction::moveModelScene() const
{
    return mMoveModelScene;
}

TFramesModel *TAction::framesModel() const
{
    return mFramesModel;
}

void TAction::cmdAddFrame(const QString &imageFile, int pos)
{
    QStringList files;
    files.append(imageFile);
    cmdAddFrames(files, pos);
}

void TAction::cmdRemoveFrame(TFrame *frame)
{
    QList<TFrame *> framesList;
    framesList.append(frame);
    cmdRemoveFrames(framesList);
}

void TAction::cmdRemoveFrame(int index)
{
    QList<int> indexList;
    indexList.append(index);
    cmdRemoveFrames(indexList);
}

void TAction::cmdAddFrames(const QStringList &imageFiles, int pos)
{
    TFramesList framesList;
    for(QString file : imageFiles)
    {
        TPixmap *pixmap = mDocument->getPixmap(file);
        TFrame *frame = new TFrame(mFramesModel);
        frame->setPixmap(pixmap);
        framesList.append(frame);
    }
    cmdAddFrames(framesList, pos);
}

void TAction::cmdAddFrames(const QList<TFrame *> &framesList, int pos)
{
    if(framesList.size() < 1)
        return;

    TFramesUndoCommand *command = new TFramesUndoCommand(FUC_ADD, mFramesModel, framesList, pos);
    mDocument->addUndoCommand(command);
}

void TAction::cmdRemoveFrames(const QList<TFrame *> &framesList)
{
    TFramesUndoCommand *command = new TFramesUndoCommand(FUC_REMOVE, mFramesModel, framesList);
    mDocument->addUndoCommand(command);
}

void TAction::cmdRemoveFrames(const QList<int> &indexList)
{
    QList<TFrame *> framesList = mFramesModel->getFramesList(indexList);
    cmdRemoveFrames(framesList);
}

void TAction::cmdMoveFrames(const QList<int> &indexList, int pos)
{
    QList<TFrame *> framesList = mFramesModel->getFramesList(indexList);
    TFramesUndoCommand *command = new TFramesUndoCommand(FUC_MOVE, mFramesModel, framesList, pos);
    mDocument->addUndoCommand(command);
}

TFrame *TAction::frameAt(int index)
{
    return mFramesModel->frameAt(index);
}

TFrame *TAction::getFrame(int index)
{
    return mFramesModel->getFrame(index);
}

int TAction::framesCount()
{
    return mFramesModel->count();
}

TPropertySheet *TAction::propertySheet() const
{
    return mPropertySheet;
}

void TAction::initPropertySheet()
{
    connect(mPropertySheet,
            SIGNAL(propertyItemValueChanged(TPropertyItem*,QVariant)),
            this,
            SLOT(slotPropertyItemValueChanged(TPropertyItem*,QVariant)));

    TPropertyItem *item;
    item = mPropertySheet->addProperty(PT_STRING, P_NAME, PID_ACTION_NAME, mName);
    item = mPropertySheet->addProperty(PT_VECTOR, P_VECTOR, PID_ACTION_VECTOR);

    Q_UNUSED(item);
}
