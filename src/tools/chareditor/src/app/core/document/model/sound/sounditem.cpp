#include "sounditem.h"
#include "../../document.h"
#include "../../../../../../../../shared/gameutils/model/sound/sounditem.h"

#include <QFileInfo>

static const char *P_SOURCE = "Source";
static const char *P_DELAY = "Delay";
static const char *P_LOOP = "Loop";

TSoundItem::TSoundItem(QObject *parent) :
    TPropertyObject(parent)
{
    QObject *obj = parent;
    while (obj) {
        mDocument = qobject_cast<TDocument*>(obj);
        if(mDocument)
            break;
        obj = obj->parent();
    }
    if(!mDocument)
        throw QString("File:%1, Line:%2: Parent must be document.").arg(__FILE__).arg(__LINE__);

    connect(mPropertySheet,
            SIGNAL(propertyItemValueChanged(TPropertyItem*,QVariant)),
            this,
            SLOT(slotPropertyItemValueChanged(TPropertyItem*,QVariant)));

    mPropertySheet->addProperty(PT_SOUND_ITEM_SOURCE, P_SOURCE, PID_SOUND_ITEM_SOURCE, QVariant());
    mPropertySheet->addProperty(PT_INT, P_DELAY, PID_SOUND_ITEM_DELAY, 0);
    mPropertySheet->addProperty(PT_INT, P_LOOP, PID_SOUND_ITEM_LOOP, 0);
}

void TSoundItem::loadFromModel(const Model::TSoundItem &soundItemModel, void *context)
{
    if(!mDocument)
        return;

    Q_UNUSED(context);
    (*mPropertySheet)[P_DELAY]->setValue(soundItemModel.delay());
    (*mPropertySheet)[P_LOOP]->setValue(soundItemModel.loopCount());
    setSource(QString::fromStdString(soundItemModel.fileName()));
}

TSoundItem::~TSoundItem()
{

}

Model::TSoundItem *TSoundItem::toModel() const
{
    Model::TSoundItem *soundItemModel = new Model::TSoundItem;
    soundItemModel->setDelay((*mPropertySheet)[P_DELAY]->value().toInt());
    soundItemModel->setLoopCount((*mPropertySheet)[P_LOOP]->value().toInt());
    soundItemModel->setFileName(mDocument->getSoundRelativePath((*mPropertySheet)[P_SOURCE]->value().toString()).toStdString());
    return soundItemModel;
}

int TSoundItem::loopCount() const
{
    return mPropertySheet->getValue(P_LOOP).toInt();
}

void TSoundItem::setLoopCount(int loopCount)
{
    (*mPropertySheet)[P_LOOP]->setValue(loopCount);
}

int TSoundItem::delay() const
{
    return mPropertySheet->getValue(P_DELAY).toInt();
}

void TSoundItem::setDelay(int delay)
{
    (*mPropertySheet)[P_DELAY]->setValue(delay);
}

QString TSoundItem::sourceFileName() const
{
    return mSoundSource?mSoundSource->fileName():QString();
}

void TSoundItem::setSource(const QString &fileName)
{
    if(!mDocument)
        return;

    setSource(mDocument->getSound(QFileInfo(fileName).fileName()));
}

void TSoundItem::slotPropertyItemValueChanged(TPropertyItem *propertyItem, const QVariant &value)
{
    if(!propertyItem || !mDocument)
        return;

    PropertyID command = propertyItem->propertyId();
    if(command == PID_SOUND_ITEM_SOURCE)
    {
        mSoundSource = mDocument->getSound(propertyItem->value().toString());
    }
    emit propertyChanged(command, value);
}

TSound *TSoundItem::source() const
{
    return mSoundSource;
}

void TSoundItem::setSource(TSound *source)
{
    if(mSoundSource == source)
        return;

    mSoundSource = source;
    mPropertySheet->get(P_SOURCE)->setValue(source?source->fileName():QVariant());
}

QDataStream &operator<<(QDataStream &out, const TSoundItem &soundItem)
{
    out << soundItem.loopCount();
    out << soundItem.delay();
    out << soundItem.sourceFileName();
    return out;
}

QDataStream &operator>>(QDataStream &in, TSoundItem &soundItem)
{
    int loopCount;
    int delay;
    QString soundFileName;
    in >> loopCount;
    in >> delay;
    in >> soundFileName;
    soundItem.setLoopCount(loopCount);
    soundItem.setDelay(delay);
    soundItem.setSource(soundFileName);
    return in;
}
