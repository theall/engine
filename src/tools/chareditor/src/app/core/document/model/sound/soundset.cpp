#include "soundset.h"
#include "../../../../../../../../shared/gameutils/model/sound/soundset.h"

static const char *P_AUTO_PLAY = "Auto play";
static const char *P_PLAY_METHOD = "Play method";

TSoundSet::TSoundSet(QObject *parent) :
    TPropertyObject(parent)
  , mSoundItemModel(new TSoundItemsModel(this))
{
    mPropertySheet->addProperty(PT_BOOL, P_AUTO_PLAY, PID_SOUND_SET_AUTO_PLAY, false);
    TPropertyItem *propertyItem = mPropertySheet->addProperty(PT_ENUM, P_PLAY_METHOD, PID_SOUND_SET_PLAY_METHOD, SSM_ORDERED);
    ADD_ENUM_NAMES(propertyItem,SSM_COUNT,SoundSetModel);
}

TSoundSet::~TSoundSet()
{

}

void TSoundSet::loadFromModel(Model::TSoundSet *soundSetModel, void *context)
{
    Q_UNUSED(context);
    if(!soundSetModel)
        return;

    (*mPropertySheet)[P_AUTO_PLAY]->setValue(soundSetModel->autoPlay());
    (*mPropertySheet)[P_PLAY_METHOD]->setValue(soundSetModel->soundSetModel());

    TSoundItemList soundItemList;
    for(Model::TSoundItem *soundItemModel : soundSetModel->soundItemList())
    {
        TSoundItem *soundItem = new TSoundItem(this);
        soundItem->loadFromModel(*soundItemModel, this);
        soundItemList.append(soundItem);
    }
    mSoundItemModel->setSoundItemList(soundItemList);
}

Model::TSoundSet *TSoundSet::toModel() const
{
    Model::TSoundSet *soundSetModel = new Model::TSoundSet;
    soundSetModel->setAutoPlay((*mPropertySheet)[P_AUTO_PLAY]->value().toBool());
    soundSetModel->setSoundSetModel((SoundSetModel)(*mPropertySheet)[P_PLAY_METHOD]->value().toInt());

    std::vector<Model::TSoundItem*> soundItemModelList;
    for(TSoundItem *soundItem : mSoundItemModel->soundItemList())
        soundItemModelList.emplace_back(soundItem->toModel());
    soundSetModel->setSoundItemList(soundItemModelList);
    return soundSetModel;
}

bool TSoundSet::autoPlay() const
{
    return (*mPropertySheet)[P_AUTO_PLAY]->value().toBool();
}

void TSoundSet::setAutoPlay(bool autoPlay)
{
    (*mPropertySheet)[P_AUTO_PLAY]->setValue(autoPlay);
}

SoundSetModel TSoundSet::soundSetModel() const
{
    return (SoundSetModel)(*mPropertySheet)[P_PLAY_METHOD]->value().toInt();
}

void TSoundSet::setSoundSetModel(const SoundSetModel &soundSetModel)
{
    (*mPropertySheet)[P_PLAY_METHOD]->setValue(soundSetModel);
}

void TSoundSet::slotPropertyItemValueChanged(TPropertyItem *item, const QVariant &value)
{
    if(!item)
        return;

    PropertyID command = item->propertyId();
    emit propertyChanged(command, value);
}

TSoundItemsModel *TSoundSet::soundItemsModel() const
{
    return mSoundItemModel;
}

QDataStream &operator<<(QDataStream &out, const TSoundSet &soundSet)
{
    out << soundSet.autoPlay();
    out << (int)soundSet.soundSetModel();
    out << *soundSet.soundItemsModel();
    return out;
}

QDataStream &operator>>(QDataStream &in, TSoundSet &soundSet)
{
    bool autoPlay;
    int playMethod;

    in >> autoPlay;
    in >> playMethod;
    in >> *soundSet.soundItemsModel();
    soundSet.setAutoPlay(autoPlay);
    soundSet.setSoundSetModel((SoundSetModel)playMethod);
    return in;
}
