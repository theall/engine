#ifndef FRAMESUNDOCOMMAND_H
#define FRAMESUNDOCOMMAND_H

#include <QUndoCommand>

enum FrameUndoCommand
{
    FUC_ADD = 0,
    FUC_REMOVE,
    FUC_MOVE,
    FUC_CLONE,
    FUC_COUNT
};

class TFrame;
class TFramesModel;

class TFramesUndoCommand : public QUndoCommand
{
public:
    TFramesUndoCommand(FrameUndoCommand command,
                       TFramesModel *framesModel,
                       const QList<TFrame*> &frameList,
                       int pos = -1,
                       QUndoCommand *parent = nullptr);
    ~TFramesUndoCommand();

    void undo() Q_DECL_OVERRIDE;
    void redo() Q_DECL_OVERRIDE;

private:
    TFramesModel *mFramesModel;
    QList<TFrame*> mFrameList;
    QList<int> mIndexList;
    int mPos;
    FrameUndoCommand mCommand;
};


#endif // FRAMESUNDOCOMMAND_H
