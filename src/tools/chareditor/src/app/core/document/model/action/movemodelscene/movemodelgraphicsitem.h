#ifndef MOVEMODELGRAPHICSITEM_H
#define MOVEMODELGRAPHICSITEM_H

#include <QGraphicsTextItem>
#include <QGraphicsRectItem>

class TMoveModelFrameItem : public QGraphicsRectItem
{
public:
    TMoveModelFrameItem(const QColor &fillColor, QGraphicsItem *parent = nullptr);
    ~TMoveModelFrameItem();

    bool isLocked() const;
    void setLocked(bool isLocked);
    virtual QPixmap pixmap() const = 0;
    virtual QPointF anchor() const = 0;

private:
    bool mIsLocked;
    QColor mFillColor;
};
typedef QList<TMoveModelFrameItem*> TMoveModelFrameItemList;

class TFrame;
class TMoveModelKeyFrameItem : public TMoveModelFrameItem
{
public:
    TMoveModelKeyFrameItem(TFrame *frame, QGraphicsItem *parent = nullptr);
    ~TMoveModelKeyFrameItem();

    QPointF vector(int index = 0) const;
    int duration() const;
    bool antiGravity() const;

    // TMoveModelFrameItem interface
public:
    QPixmap pixmap() const Q_DECL_OVERRIDE;
    QPointF anchor() const Q_DECL_OVERRIDE;

private:
    TFrame *mFrame;
};

typedef QList<TMoveModelKeyFrameItem*> TMoveModelKeyFrameItemList;

class TMoveModelReferenceFrameItem : public TMoveModelFrameItem
{
public:
    TMoveModelReferenceFrameItem(TMoveModelKeyFrameItem *keyFrameItem, int index, QGraphicsItem *parent = nullptr);
    ~TMoveModelReferenceFrameItem();

    TMoveModelKeyFrameItem *keyFrame() const;
    void setKeyFrame(TMoveModelKeyFrameItem *keyFrame);

    QPointF vector() const;

private:
    int mIndex;
    TMoveModelKeyFrameItem *mKeyFrame;

    // TMoveModelFrameItem interface
public:
    QPixmap pixmap() const Q_DECL_OVERRIDE;
    QPointF anchor() const Q_DECL_OVERRIDE;
};
typedef QList<TMoveModelReferenceFrameItem*> TMoveModelReferenceFrameItemList;

class TMousePosTraceItem : public QGraphicsTextItem
{
public:
    TMousePosTraceItem(QGraphicsItem *parent = nullptr);
    ~TMousePosTraceItem();

    void setPos(const QPointF &pos, const QPointF &orginPos);

};

#endif // MOVEMODELGRAPHICSITEM_H
