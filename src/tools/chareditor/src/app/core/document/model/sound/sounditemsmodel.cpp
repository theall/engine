#include "sounditemsmodel.h"
#include "../../document.h"
#include "soundsetundocommand.h"

TSoundItemsModel::TSoundItemsModel(QObject *parent) :
    QAbstractTableModel(parent)
  , mDocument(nullptr)
{
    QObject *obj = parent;
    while (obj) {
        mDocument = qobject_cast<TDocument*>(obj);
        if(mDocument)
            break;
        obj = obj->parent();
    }
    if(!mDocument)
        throw QString("File:%1, Line:%2: Parent must be document.").arg(__FILE__).arg(__LINE__);
}

TSoundItemsModel::~TSoundItemsModel()
{
    FREE_CONTAINER(mSoundItemList);
}

TSoundItemList TSoundItemsModel::soundItemList() const
{
    return mSoundItemList;
}

TSoundItem *TSoundItemsModel::getSoundItem(int index) const
{
    if(index>=0 && index<mSoundItemList.size())
        return mSoundItemList.at(index);

    return nullptr;
}

int TSoundItemsModel::soundItemsCount() const
{
    return mSoundItemList.size();
}

void TSoundItemsModel::setSoundItemList(const TSoundItemList &soundItemList)
{
    FREE_CONTAINER(mSoundItemList);
    mSoundItemList = soundItemList;
}

int TSoundItemsModel::internalAddSoundItem(TSoundItem *soundItem, int index)
{
    if(!soundItem)
        return -1;

    int ret = index;
    int itemCount = mSoundItemList.size();
    if(ret<0 || ret > itemCount)
        ret = itemCount;

    connect(soundItem,
            SIGNAL(propertyChanged(PropertyID,QVariant)),
            this,
            SLOT(slotSoundItemPropertyChanged(PropertyID,QVariant)));
    beginInsertRows(QModelIndex(), ret, ret);
    mSoundItemList.insert(ret, soundItem);
    endInsertRows();
    return ret;
}

int TSoundItemsModel::internalRemoveSoundItem(TSoundItem *soundItem)
{
    int ret = mSoundItemList.indexOf(soundItem);
    if(ret != -1) {
        soundItem->disconnect(this);
        beginRemoveRows(QModelIndex(), ret, ret);
        mSoundItemList.removeAt(ret);
        endRemoveRows();
    }
    return ret;
}

int TSoundItemsModel::addSoundItem(TSoundItem *soundItem, int index)
{
    if(!soundItem)
        return -1;

    TSoundItemList soundItemList;
    soundItemList.append(soundItem);

    QList<int> indexList;
    indexList.append(index);
    QList<int> ret = addSoundItemList(soundItemList, indexList);
    if(ret.isEmpty())
        return -1;

    return ret.at(0);
}

QList<int> TSoundItemsModel::addSoundItemList(const TSoundItemList &soundItemList, const QList<int> &indexList)
{
    QList<int> ret = indexList;
    int soundItemSize = soundItemList.size();
    if(soundItemSize != indexList.size())
    {
        ret.clear();
        for(int i=0;i<soundItemSize;i++)
        {
            ret.append(-1);
        }
    }
    int oldSoundItemSize = mSoundItemList.size();
    for(int i=0;i<soundItemSize;i++)
    {
        internalAddSoundItem(soundItemList[i], ret[i]);
    }
    if(soundItemSize > 0) {
        emit layoutChanged();
        emit soundItemsAdded(soundItemList, ret);
        emit soundItemsCountChanged(oldSoundItemSize, mSoundItemList.size());
    }
    return indexList;
}

int TSoundItemsModel::removeSoundItem(TSoundItem *soundItem)
{
    if(!soundItem)
        return -1;

    TSoundItemList soundItemList;
    soundItemList.append(soundItem);
    QList<int> ret = removeSoundItemList(soundItemList);
    if(ret.isEmpty())
        return -1;

    return ret.at(0);
}

QList<int> TSoundItemsModel::removeSoundItemList(const TSoundItemList &soundItemList)
{
    QList<int> ret;
    TSoundItemList soundItemsRet;
    int oldSoundItemSize = mSoundItemList.size();
    for(TSoundItem *soundItem : soundItemList)
    {
        if(!soundItem)
            continue;

        int t = internalRemoveSoundItem(soundItem);
        if(t < 0)
            continue;

        ret.append(t);
        soundItemsRet.append(soundItem);
    }
    if(ret.size() > 0) {
        emit layoutChanged();
        emit soundItemsRemoved(soundItemsRet, ret);
        emit soundItemsCountChanged(oldSoundItemSize, mSoundItemList.size());
    }
    return ret;
}

void TSoundItemsModel::cmdAddSound(const QString &file)
{
    cmdAddSound(mDocument->getSound(file));
}

void TSoundItemsModel::cmdAddSound(TSound *sound)
{
    TSoundItem *soundItem = new TSoundItem(this);
    soundItem->setSource(sound);
    cmdAddSound(soundItem);
}

void TSoundItemsModel::cmdAddSound(TSoundItem *soundItem)
{
    TSoundItemList soundItemList;
    soundItemList.append(soundItem);
    cmdAddSoundList(soundItemList);
}

void TSoundItemsModel::cmdAddSoundList(const QStringList &soundFileList)
{
    TSoundList soundList;
    for(QString soundFile : soundFileList)
    {
        TSound *sound = mDocument->getSound(soundFile);
        if(!sound)
            continue;

        soundList.append(sound);
    }
    cmdAddSoundList(soundList);
}

void TSoundItemsModel::cmdAddSoundList(const TSoundList &soundList)
{
    TSoundItemList soundItemList;
    for(TSound *sound : soundList)
    {
        TSoundItem *soundItem = new TSoundItem(this);
        soundItem->setSource(sound);
        soundItemList.append(soundItem);
    }
    cmdAddSoundList(soundItemList);
}

void TSoundItemsModel::cmdAddSoundList(const TSoundItemList &soundItemList)
{
    if(!mDocument || soundItemList.size()<=0)
        return;

    TSoundSetUndoCommand *undoCommand = new TSoundSetUndoCommand(SUC_ADD, this, soundItemList);
    mDocument->addUndoCommand(undoCommand);
}

void TSoundItemsModel::cmdRemoveSound(TSoundItem *soundItem)
{
    if(!soundItem || !mDocument)
        return;

    TSoundSetUndoCommand *undoCommand = new TSoundSetUndoCommand(SUC_REMOVE, this, soundItem);
    mDocument->addUndoCommand(undoCommand);
}

void TSoundItemsModel::cmdRemoveSoundList(const TSoundItemList &soundItemList)
{
    if(!mDocument || soundItemList.size()<=0)
        return;

    TSoundSetUndoCommand *undoCommand = new TSoundSetUndoCommand(SUC_REMOVE, this, soundItemList);
    mDocument->addUndoCommand(undoCommand);
}

int TSoundItemsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mSoundItemList.size();
}

int TSoundItemsModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

QVariant TSoundItemsModel::data(const QModelIndex &index, int role) const
{
    if(role == Qt::DisplayRole)
    {
        int row = index.row();
        if(row>=0 && row<mSoundItemList.size())
        {
            return mSoundItemList.at(row)->sourceFileName();
        }
    }
    return QVariant();
}

Qt::ItemFlags TSoundItemsModel::flags(const QModelIndex &index) const
{
    return QAbstractTableModel::flags(index);
}

void TSoundItemsModel::slotSoundItemPropertyChanged(PropertyID id, const QVariant &value)
{
    Q_UNUSED(value);

    if(id == PID_SOUND_ITEM_SOURCE)
    {
        emit layoutChanged();
    }
}

TSoundItemList TSoundItemsModel::getSoundItemList() const
{
    return mSoundItemList;
}

QDataStream &operator<<(QDataStream &out, const TSoundItemsModel &soundItemsModel)
{
    TSoundItemList soundItemList = soundItemsModel.getSoundItemList();
    out << soundItemList.size();
    for(TSoundItem *soundItem : soundItemList)
        out << *soundItem;
    return out;
}

QDataStream &operator>>(QDataStream &in, TSoundItemsModel &soundItemsModel)
{
    int soundItemCount = 0;
    TSoundItemList soundItemList;
    in >> soundItemCount;
    for(int i=0;i<soundItemCount;i++)
    {
        TSoundItem *soundItem = new TSoundItem(&soundItemsModel);
        in >> *soundItem;
        soundItemList.append(soundItem);
    }
    soundItemsModel.setSoundItemList(soundItemList);
    return in;
}
