#ifndef ACTIONSMODEL_H
#define ACTIONSMODEL_H

#include "action/action.h"

#include <QAbstractTableModel>

class TActionsModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    TActionsModel(QObject *parent = nullptr);
    ~TActionsModel();

    Model::TActionList toActionList() const;

    void clear();

    QList<TAction *> actionList() const;
    int addAction(TAction *action, int index=-1);
    int removeAction(TAction *action);
    int removeAction(int index);
    QList<int> removeActions(const QList<TAction *> &actions);
    QList<int> removeActions(const QList<int> &actionList);

    TAction *getAction(int index);
    int count();

private slots:
    void slotActionNameChanged(const QString &newName);

private:
    QList<TAction*> mActionList;

    TAction *getAction(const QModelIndex &index);

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    bool setData(const QModelIndex &index, const QVariant &value, int role) Q_DECL_OVERRIDE;
};

#endif // ACTIONSMODEL_H
