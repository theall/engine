#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QDir>
#include <QDateTime>
#include "documentbase.h"
#include "../../../../../../shared/gameutils/model/character.h"

class TPixmap;
class TSound;
class TFrame;
class TAction;
class TCachedPixmap;
class TCachedSound;
class TFileSystemWatcher;
class TActionsModel;

class TDocument : public TDocumentBase
{
    Q_OBJECT

public:
    TDocument(const QString &file=QString(), QObject *parent=nullptr);
    ~TDocument();

    static TDocument *create(
            const QString &projectRoot,
            const QString &projectName,
            const QString &projectType,
            const QString &projectVersion,
            const QString &projectAuthor,
            const QString &projectContact,
            const QString &projectComment
            );

    void cmdAddAction(const QString &name);
    void cmdRemoveAction(TAction *action);
    void cmdRemoveAction(int actionIndex);

    TActionsModel *actionsModel() const;
    void setStandardFrame(TFrame *frame);

signals:

private slots:

private:
    TActionsModel *mActionsModel;
    TFileSystemWatcher *mFileWatcher;
    TCachedPixmap *mCachedPixmaps;
    TCachedSound *mCachedSounds;

    void loadFromModel(const Model::TCharacter &characterModel, void *context=nullptr);
    Model::TCharacter *toModel() const;
    void setFileName(const QString &fileName);
    void initPropertySheet();

    // TDocumentBase interface
public:
    QByteArray getSavedData() const Q_DECL_OVERRIDE;
    bool loadFromFile(const QString &fileName) Q_DECL_OVERRIDE;
};

#endif // DOCUMENT_H
