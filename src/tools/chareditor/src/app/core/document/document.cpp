#include "document.h"
#include "documentbase.h"
#include "model/actionsmodel.h"
#include "undocommand/actionsundocommand.h"
#include "model/action/framesmodel/frame/frame.h"
#include "../base/docutil.h"

static const char *P_NAME = "Name";
static const char *P_ICON = "Icon";
static const char *P_AGE = "Age";
static const char *P_WEIGHT = "Weight";
static const char *P_FORCE = "Force";
static const char *P_DEFEND = "Defend";
static const char *P_SCALE = "Scale";
static const char *P_HEIGHT = "Height";
static const char *P_AICODE = "AiCode";
static const char *P_EMAIL = "Email";
static const char *P_VERSION = "Version";
static const char *P_AUTHOR = "Author";
static const char *P_COMMENT = "Comment";
static const char *P_GENDER = "Gender";
//static const char *P_SPRITE_TYPE = "Sprite type";
//static const char *P_CREATE_TIME = "Create time";
//static const char *P_UPDATE_TIME = "Update time";
static const char *ATTR_ENUMNAMES = "enumNames";

TDocument::TDocument(const QString &file, QObject *parent) :
    TDocumentBase(file, parent)
  , mActionsModel(new TActionsModel(this))
{
    setObjectName("Document");
    initPropertySheet();
    load(file);
}

TDocument::~TDocument()
{

}

Model::TCharacter *TDocument::toModel() const
{
    Model::TCharacter *character = new Model::TCharacter;
    character->setGuid(getGuid().toStdString());
    character->setName(mPropertySheet->getValue(P_NAME).toString().toStdString());
    character->setIconPath(getPixmapRelativePath(mPropertySheet->getValue(P_ICON).toString()).toStdString());
    character->setGender((CharacterGender)mPropertySheet->getValue(P_GENDER).toInt());
    character->setAge(mPropertySheet->getValue(P_AGE).toInt());
    character->setWeight(mPropertySheet->getValue(P_WEIGHT).toInt());
    character->setHeight(mPropertySheet->getValue(P_HEIGHT).toInt());
    character->setForce(mPropertySheet->getValue(P_FORCE).toInt());
    character->setDefend(mPropertySheet->getValue(P_DEFEND).toInt());
    //character->setSpriteType((SpriteType)mPropertySheet->getValue(P_SPRITE_TYPE).toInt());
    character->setScale(mPropertySheet->getValue(P_SCALE).toFloat());
    character->setComment(mPropertySheet->getValue(P_COMMENT).toString().toStdString());
    character->setEmail(mPropertySheet->getValue(P_EMAIL).toString().toStdString());
    character->setVersion(mPropertySheet->getValue(P_VERSION).toString().toStdString());
    character->setCreatTime(mCreateTime.toString("yyyy-MM-dd hh:mm:ss").toStdString());
    character->setUpdateTime(mUpdateTime.toString("yyyy-MM-dd hh:mm:ss").toStdString());
    character->setActionList(mActionsModel->toActionList());

    return character;
}

TDocument *TDocument::create(
        const QString &projectRoot,
        const QString &projectName,
        const QString &projectType,
        const QString &projectVersion,
        const QString &projectAuthor,
        const QString &projectContact,
        const QString &projectComment)
{
    TDocument *document = new TDocument;
    QFileInfo fi(projectRoot);
    if(!fi.isDir() || !fi.exists())
    {
        QDir d = fi.absoluteDir();
        d.mkdir(fi.baseName());
    }
    document->setProjectRoot(projectRoot);
    document->setProjectName(projectName);
    document->setVersion(projectVersion);
    document->setAuthor(projectAuthor);
    document->setEmail(projectContact);
    document->setComment(projectComment);
    document->setProjectType(projectType);
    document->actionsModel()->addAction(new TAction(tr("Default"), document));

    QString fillFullName = projectRoot + "/" + projectName + ".json";

    document->save(fillFullName);

    return document;
}

void TDocument::cmdAddAction(const QString &name)
{
    TAction *action = new TAction(name, this);
    TActionsUndoCommand *command = new TActionsUndoCommand(
                AUC_ADD,
                mActionsModel,
                action);
    addUndoCommand(command);
}

void TDocument::cmdRemoveAction(TAction *action)
{
    if(!action)
        return;

    TActionsUndoCommand *command = new TActionsUndoCommand(
                AUC_REMOVE,
                mActionsModel,
                action);
    addUndoCommand(command);
}

void TDocument::cmdRemoveAction(int actionIndex)
{
    TAction *action = mActionsModel->getAction(actionIndex);
    if(!action)
        return;
    cmdRemoveAction(action);
}

void TDocument::loadFromModel(const Model::TCharacter &characterModel, void *context)
{
    TDocument *document = (TDocument*)context;
    if(!document)
        return;

    mGuid = QString::fromStdString(characterModel.getGuid());
    mCreateTime = QDateTime::fromString(QString::fromStdString(characterModel.getCreatTime()));
    mUpdateTime = QDateTime::fromString(QString::fromStdString(characterModel.getUpdateTime()));

    (*mPropertySheet)[P_AGE]->setValue(characterModel.getAge());
    (*mPropertySheet)[P_WEIGHT]->setValue(characterModel.getWeight());
    (*mPropertySheet)[P_FORCE]->setValue(characterModel.getForce());
    (*mPropertySheet)[P_DEFEND]->setValue(characterModel.getDefend());
    (*mPropertySheet)[P_SCALE]->setValue(characterModel.getScale());
    (*mPropertySheet)[P_HEIGHT]->setValue(characterModel.getHeight());
    (*mPropertySheet)[P_COMMENT]->setValue(QString::fromStdString(characterModel.getComment()));
    (*mPropertySheet)[P_EMAIL]->setValue(QString::fromStdString(characterModel.getEmail()));
    (*mPropertySheet)[P_NAME]->setValue(QString::fromStdString(characterModel.getName()));
    (*mPropertySheet)[P_ICON]->setValue(QFileInfo(QString::fromStdString(characterModel.getIconPath())).fileName());
    (*mPropertySheet)[P_AICODE]->setValue(QString::fromStdString(characterModel.getAiCode()));
    (*mPropertySheet)[P_VERSION]->setValue(QString::fromStdString(characterModel.getVersion()));
    (*mPropertySheet)[P_AUTHOR]->setValue(QString::fromStdString(characterModel.getAuthor()));
    (*mPropertySheet)[P_GENDER]->setValue(characterModel.getGender());
    //(*mPropertySheet)[P_SPRITE_TYPE]->setValue(characterModel.getSpriteType());

    TFrame::setStandardFrame(nullptr);
    mActionsModel->clear();
    for(Model::TAction *actionModel : characterModel.getActionList())
    {
        TAction *action = new TAction(QString(), this);
        action->loadFromModel(*actionModel, this);
        mActionsModel->addAction(action);
    }

    QList<TAction *> actionList = mActionsModel->actionList();
    if(actionList.size() > 0)
        setStandardFrame(actionList.at(0)->frameAt(0));
}

void TDocument::initPropertySheet()
{
    TPropertyItem *item;
//    item = mPropertySheet->addProperty(
//                PT_ENUM,
//                P_SPRITE_TYPE,
//                PID_SPRITE_TYPE,
//                DocUtil::SpriteTypeToQString(ST_Character));
//    QStringList typeNames;
//    typeNames.append(DocUtil::SpriteTypeToQString(ST_Character));
//    item->addAttribute(ATTR_ENUMNAMES, typeNames);

    mPropertySheet->addProperty(PT_STRING, P_NAME, PID_SPRITE_NAME, tr("New Character"));
    mPropertySheet->addProperty(PT_INT, P_AGE, PID_SPRITE_AGE, 20);
    item = mPropertySheet->addProperty(
                PT_ENUM,
                P_GENDER,
                PID_SPRITE_GENDER,
                CG_UNKNOWN);
    QStringList genderNames;
    genderNames.append(DocUtil::CharacterGenderToQString(CG_MALE));
    genderNames.append(DocUtil::CharacterGenderToQString(CG_FEMALE));
    genderNames.append(DocUtil::CharacterGenderToQString(CG_UNKNOWN));
    item->addAttribute(ATTR_ENUMNAMES, genderNames);

    TPropertyItem *iconItem = mPropertySheet->addProperty(PT_PIXMAP, P_ICON, PID_SPRITE_ICON);
    mPropertySheet->addProperty(PT_INT, P_WEIGHT, PID_SPRITE_WEIGHT, 75);
    mPropertySheet->addProperty(PT_INT, P_FORCE, PID_SPRITE_FORCE, 100);
    mPropertySheet->addProperty(PT_INT, P_DEFEND, PID_SPRITE_DEFEND, 100);
    mPropertySheet->addProperty(PT_DOUBLE, P_SCALE, PID_SPRITE_SCALE, 1.0);
    mPropertySheet->addProperty(PT_INT, P_HEIGHT, PID_SPRITE_HEIGHT, 180);
    mPropertySheet->addProperty(PT_STRING, P_AICODE, PID_SPRITE_AICODE);
    mPropertySheet->addProperty(PT_STRING, P_EMAIL, PID_PROJECT_CONTACT);
    mPropertySheet->addProperty(PT_STRING, P_VERSION, PID_PROJECT_VERSION, "1.0.0.0");
    mPropertySheet->addProperty(PT_STRING, P_AUTHOR, PID_PROJECT_AUTHOR);
    mPropertySheet->addProperty(PT_STRING_EX, P_COMMENT, PID_PROJECT_COMMENT);
    //mPropertySheet->addProperty(QVariant::DateTime, P_CREATE_TIME, mCreatTime);

    connect(iconItem,
            SIGNAL(valueChanged(QVariant)),
            this,
            SLOT(slotIconPropertyItemChanged(QVariant)));
}

QByteArray TDocument::getSavedData() const
{
    Model::TCharacter *character = toModel();
    std::string jsonString = character->toJsonString();
    delete character;
    return QByteArray(jsonString.c_str(), jsonString.size());
}

bool TDocument::loadFromFile(const QString &fileName)
{
    Model::TCharacter character;
    bool success = character.loadFromFile(fileName.toStdString());
    if(!success)
        throw tr("Load character model %1 failed.").arg(fileName);
    else
        loadFromModel(character, this);
    return success;
}

void TDocument::setStandardFrame(TFrame *frame)
{
    TFrame::setStandardFrame(frame);
    for(TAction *action : mActionsModel->actionList())
        action->updateFramesBuddy();
}

TActionsModel *TDocument::actionsModel() const
{
    return mActionsModel;
}
