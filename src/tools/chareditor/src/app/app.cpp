#include "app.h"

#include "res/version.h"
#include "utils/preferences.h"

#include "controller/maincontroller.h"
#include "core/core.h"
#include "gui/mainwindow.h"

#include <QTextCodec>
#include <QFileOpenEvent>

TApp::TApp(int argc, char *argv[]) :
    QApplication(argc, argv)
{
    TVersionInfo *vi = TVersionInfo::instance();
    setOrganizationDomain(vi->legalCopyright());
    setApplicationDisplayName(vi->fileDescription());
    setOrganizationName(vi->companyName());
    setApplicationName(vi->productName());
    setApplicationVersion(vi->productVersion());

#ifdef Q_OS_MAC
    setAttribute(Qt.AA_DontShowIconsInMenus);
#endif

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("GBK"));

    TPreferences::instance();
}

TApp::~TApp()
{
    TPreferences::deleteInstance();
}

int TApp::start()
{
    TCore core;
    TMainWindow gui;
    TMainController controller;
    if(!controller.joint(&gui, &core))
        return 0;

    connect(this, SIGNAL(requestOpenProject(QString)), &gui, SIGNAL(requestOpenProject(QString)));

    int ret = exec();
    return ret;
}

bool TApp::event(QEvent *event)
{
    if(event->type()==QEvent::FileOpen)
    {
        QFileOpenEvent *fileOpenEvent = static_cast<QFileOpenEvent*>(event);
        emit requestOpenProject(fileOpenEvent->file());
        return true;
    }
    return QApplication::event(event);
}
