#ifndef WIN32_VER_H
#define WIN32_VER_H
#define VERSION_MINOR 0
#define BUILD_TIME "2017-12-22 19:06:13"
#define DOMAIN_NAME "Theall"
#define ORGANIZATION_DOMAIN "https://www.2dcombat.org/"
#define PRODUCT_NAME "CharacterEditor"
#define K_LEGALCOPYRIGHT "LegalCopyright"
#define K_INTERNALNAME "InternalName"
#define PRODUCT_VERSION_STR "0.0.0.0"
#define COMPILE_PLATFORM "MingW 2013, 32 bit"
#define FILE_VERSION 0,0,0,0
#define BUILD_NUMBER "0"
#define PRODUCT_ICON "logo.ico"
#define K_COMPANYNAME "CompanyName"
#define K_FILEVERSION "FileVersion"
#define K_BUILD_TIME "BuildTime"
#define K_ORIGINALFILENAME "OriginalFilename"
#define PRODUCT_VERSION 0,0,0,0
#define K_DOMAIN "Domain"
#define K_FILEDESCRIPTION "FileDescription"
#define ORIGINAL_FILE_NAME "chareditor.exe"
#define LEGAL_COPYRIGHT "Copyright (C) 2016-2017 Bilge Theall, All rights reserved."
#define FILE_DESCRIPTION "CharacterEditor Preview Edition."
#define INTERNAL_NAME "CharacterEditor.exe"
#define K_BUILDNUMBER "BuildNumber"
#define COMPANY_NAME "Bilge Theall"
#define K_PRODUCTVERSION "ProductVersion"
#define FILE_VERSION_STR "0.0.0.0"
#define K_COMPILEPLATFORM "CompilePlatform"
#define K_PRODUCTNAME "ProductName"
#define VERSION_MAJOR 0
#define VERSION_PATCH 0
#define VERSION_BUILD 0
#endif