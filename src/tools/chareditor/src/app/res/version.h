#ifndef VERSION_H
#define VERSION_H

#include <QString>
#include <QObject>

#ifdef Q_OS_WIN32
    #include <windows.h>
#endif

class TVersionInfo : public QObject
{
    Q_OBJECT

public:
    explicit TVersionInfo(QObject *parent=0);
    ~TVersionInfo();

    static TVersionInfo *instance();
    static void deleteInstance();

    int major();
    int minor();
    int patch();
    int build();

    QString domain();
    QString companyName();
    QString fileDescription();
    QString fileVersion();
    QString internalName();
    QString legalCopyright();
    QString originalFilename();
    QString productName();
    QString productVersion();
    QString compilePlatform();
    QString buildTime();

private:
    int mVerMajor;
    int mVerSecond;
    int mVerMin;
    int mBuildNo;

    QString mDomain;
    QString mCompanyName;
    QString mFileDescription;
    QString mFileVersion;
    QString mInternalName;
    QString mLegalCopyright;
    QString mOriginalFilename;
    QString mProductName;
    QString mProductVersion;
    QString mCompilePlatform;
    QString mBuildTime;

    static TVersionInfo *mInstance;
};

#endif
