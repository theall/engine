QT += widgets
TEMPLATE = lib
CONFIG += c++11

INCLUDEPATH += include
HEADERS += \
    src/qtbuttonpropertybrowser.h \
    src/qteditorfactory.h \
    src/qtgroupboxpropertybrowser.h \
    src/qtpropertybrowser.h \
    src/qtpropertybrowserutils_p.h \
    src/qtpropertymanager.h \
    src/qttreepropertybrowser.h \
    src/qtvariantproperty.h
    
SOURCES += \
    src/qtbuttonpropertybrowser.cpp \
    src/qteditorfactory.cpp \
    src/qtgroupboxpropertybrowser.cpp \
    src/qtpropertybrowser.cpp \
    src/qtpropertybrowserutils.cpp \
    src/qtpropertymanager.cpp \
    src/qttreepropertybrowser.cpp \
    src/qtvariantproperty.cpp

CONFIG(debug,debug|release) {
    TARGET = qtpropertybrowserd
} else {
    TARGET = qtpropertybrowser
}
