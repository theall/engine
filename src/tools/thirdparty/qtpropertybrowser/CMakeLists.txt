#
# Copyright (c) Bilge Theall
#
# This software is provided 'as-is', without any express or implied
# warranty.  In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
cmake_minimum_required(VERSION 2.8.13)

# project name: qtpropertybrowser
project(qtpropertybrowser)

# set source files
set(SOURCE
    src/qtbuttonpropertybrowser.cpp
    src/qteditorfactory.cpp
    src/qtgroupboxpropertybrowser.cpp
    src/qtpropertybrowser.cpp
    src/qtpropertybrowserutils.cpp
    src/qtpropertymanager.cpp
    src/qttreepropertybrowser.cpp
    src/qtvariantproperty.cpp
)

# Find the QtWidgets library
find_package(Qt5Widgets REQUIRED)
list(GET Qt5Widgets_INCLUDE_DIRS 0 SYSTEM_INCLUDE_PATH)

# Add the include directories for the Qt 5 Widgets module to
# the compile lines.
include_directories(${CMAKE_CACHEFILE_DIR})

# Use the compile definitions defined in the Qt 5 Widgets module
#add_definitions(${Qt5Widgets_DEFINITIONS})

# Add compiler flags for building executables (-fPIE)
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")

# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

# set make to a shared library
set(TARGET_NAME qtpropertybrowser)
if (CMAKE_BUILD_TYPE MATCHES "Debug")
    set(TARGET_NAME qtpropertybrowserd)
endif()

add_library(${TARGET_NAME} SHARED ${SOURCE})
target_link_libraries(${TARGET_NAME} Qt5::Widgets)

set(CMAKE_INSTALL_PREFIX ${SYSTEM_INCLUDE_PATH}/..)
file(GLOB CLASSES ${CMAKE_SOURCE_DIR}/include/*)
file(GLOB HEADERS ${CMAKE_SOURCE_DIR}/src/*.h)
install(FILES ${CLASSES} DESTINATION include/qtpropertybrowser)
install(FILES ${HEADERS} DESTINATION include/qtpropertybrowser)
install(TARGETS ${TARGET_NAME}
        ARCHIVE DESTINATION lib
        RUNTIME DESTINATION bin)