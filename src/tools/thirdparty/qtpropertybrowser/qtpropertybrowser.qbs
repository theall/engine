import qbs 1.0

DynamicLibrary {
    name: "qtpropertybrowser"

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["widgets"] }

    cpp.includePaths: ["src", "include"]
    cpp.cxxLanguageVersion: "c++11"

    files: [
        "src/qtbuttonpropertybrowser.cpp",
        "include/qtbuttonpropertybrowser.h",
        "src/qteditorfactory.cpp",
        "include/qteditorfactory.h",
        "src/qtgroupboxpropertybrowser.cpp",
        "include/qtgroupboxpropertybrowser.h",
        "src/qtpropertybrowser.cpp",
        "include/qtpropertybrowser.h",
        "src/qtpropertybrowserutils.cpp",
        "include/qtpropertybrowserutils_p.h",
        "src/qtpropertymanager.cpp",
        "include/qtpropertymanager.h",
        "src/qttreepropertybrowser.cpp",
        "include/qttreepropertybrowser.h",
        "src/qtvariantproperty.cpp",
        "include/qtvariantproperty.h",
    ]

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: "src"
    }
}
