@echo off
pushd %temp%
set BUILD_NAME=qtpropertybrowser_build
if not exist %BUILD_NAME% md %BUILD_NAME%
cd %BUILD_NAME%
cmake %~dp0 -DCMAKE_BUILD_TYPE=Debug -G"MinGW Makefiles"
mingw32-make install
popd
@echo on