INCLUDEPATH += \
    $$PWD/../

SOURCES += \
    $$PWD/base/button.cpp \
    $$PWD/base/datetime.cpp \
    $$PWD/base/rect.cpp \
    $$PWD/base/vector.cpp \
    $$PWD/base/utils.cpp \
    $$PWD/base/variable.cpp \
    $$PWD/base/model.cpp \
    $$PWD/model/character/action.cpp \
    $$PWD/model/character/actiontrigger.cpp \
    $$PWD/model/character/fireobject.cpp \
    $$PWD/model/character/frame.cpp \
    $$PWD/model/character/hand.cpp \
    $$PWD/model/scene/layer.cpp \
    $$PWD/model/character.cpp \
    $$PWD/model/charactercollection.cpp \
    $$PWD/model/game.cpp \
    $$PWD/model/scene.cpp \
    $$PWD/model/scene/layer/sprite.cpp \
    $$PWD/model/scene/layer/imagesprite.cpp \
    $$PWD/model/scene/layer/charactersprite.cpp \
    $$PWD/model/scene/layer/buttonsprite.cpp \
    $$PWD/model/scene/layer/textsprite.cpp \
    $$PWD/model/scene/layer/blinktextsprite.cpp \
    $$PWD/model/scene/layer/cursorsprite.cpp \
    $$PWD/base/color.cpp \
    $$PWD/model/sound/sounditem.cpp \
    $$PWD/model/sound/soundset.cpp \
    $$PWD/model/scene/camera.cpp \
    $$PWD/model/move/linesegment.cpp \
    $$PWD/model/move/speedmodel.cpp \
    $$PWD/model/move/movesegment.cpp \
    $$PWD/model/scene/layer/doorsprite.cpp \
    $$PWD/model/move/movemodel.cpp \
    $$PWD/model/move/randomsegment.cpp \
    $$PWD/model/character/vectoritem.cpp \
    $$PWD/model/area/area.cpp \
    $$PWD/model/area/rectarea.cpp \
    $$PWD/model/area/circlearea.cpp \
    $$PWD/model/scene/layer/terriansprite.cpp \
    $$PWD/model/scene/layer/ghostsprite.cpp

HEADERS  += \
    $$PWD/base/button.h \
    $$PWD/base/datetime.h \
    $$PWD/base/rect.h \
    $$PWD/base/vector.h \
    $$PWD/base/utils.h \
    $$PWD/base/variable.h \
    $$PWD/base/model.h \
    $$PWD/base/jsoninterface.h \
    $$PWD/model/character/action.h \
    $$PWD/model/character/actiontrigger.h \
    $$PWD/model/character/fireobject.h \
    $$PWD/model/character/frame.h \
    $$PWD/model/character/hand.h \
    $$PWD/model/scene/layer.h \
    $$PWD/model/character.h \
    $$PWD/model/charactercollection.h \
    $$PWD/model/game.h \
    $$PWD/model/scene.h \
    $$PWD/model/scene/layer/sprite.h \
    $$PWD/model/scene/layer/imagesprite.h \
    $$PWD/model/scene/layer/charactersprite.h \
    $$PWD/model/scene/layer/buttonsprite.h \
    $$PWD/model/scene/layer/textsprite.h \
    $$PWD/model/scene/layer/blinktextsprite.h \
    $$PWD/model/scene/layer/cursorsprite.h \
    $$PWD/base/color.h \
    $$PWD/model/sound/sounditem.h \
    $$PWD/model/sound/soundset.h \
    $$PWD/model/scene/camera.h \
    $$PWD/model/move/linesegment.h \
    $$PWD/model/move/speedmodel.h \
    $$PWD/model/move/movesegment.h \
    $$PWD/model/scene/layer/doorsprite.h \
    $$PWD/model/move/movemodel.h \
    $$PWD/model/move/randomsegment.h \
    $$PWD/model/character/vectoritem.h \
    $$PWD/model/area/area.h \
    $$PWD/model/area/rectarea.h \
    $$PWD/model/area/circlearea.h \
    $$PWD/model/scene/layer/terriansprite.h \
    $$PWD/model/scene/layer/ghostsprite.h
