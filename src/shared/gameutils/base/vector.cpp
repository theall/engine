#include "vector.h"

TVector2::TVector2(float _x, float _y) :
    x(_x)
  , y(_y)
{

}

TVector2 TVector2::fromJson(nlohmann::json j)
{
    TVector2 vec2;
    if(j.size()==2)
    {
        vec2.x = j[0];
        vec2.y = j[1];
    }
    return vec2;
}

nlohmann::json TVector2::toJson() const
{
    nlohmann::json j = nlohmann::json::array();
    j.push_back(x);
    j.push_back(y);
    return j;
}

std::string TVector2::toString(const char *format) const
{
    char buf[256];
    sprintf(buf, format, x, y);
    return std::string(buf);
}

void TVector2::operator =(const TVector2 &vec)
{
    x = vec.x;
    y = vec.y;
}

TVector2 TVector2::operator +(const TVector2 &vec)
{
    TVector2 v;
    v.x = x + vec.x;
    v.y = y + vec.y;
    return v;
}

TVector2 TVector2::operator -(const TVector2 &vec)
{
    TVector2 v;
    v.x = x - vec.x;
    v.y = y - vec.y;
    return v;
}

void TVector2::operator +=(const TVector2 &vec)
{
    x += vec.x;
    y += vec.y;
}

void TVector2::operator -=(const TVector2 &vec)
{
    x -= vec.x;
    y -= vec.y;
}

TVector2 TVector2::operator /(int i)
{
    if(x==0)
        return TVector2();

    return TVector2(x/i, y/i);
}

TVector2 TVector2::operator /=(int i)
{
    if(x != 0)
    {
        x /= i;
        y /= i;
    }
    return *this;
}

bool TVector2::isNull()
{
    return x==0.0 && y==0.0;
}

TVector2 TVector2::mirror(float _x) const
{
    return TVector2(_x*2-x, y);
}

float TVector2::sum() const
{
    return x + y;
}

float TVector2::distance() const
{
    return (float)sqrt(x * x + y * y);
}

float TVector2::area() const
{
    return x * x + y * y;
}

TVector3::TVector3(float _x, float _y, float _z) :
    x(_x)
  , y(_y)
  , z(_z)
{

}

TVector3 TVector3::fromJson(nlohmann::json j)
{
    TVector3 vec3;
    if(j.size()==3)
    {
        vec3.x = j[0];
        vec3.y = j[1];
        vec3.z = j[2];
    }
    return vec3;
}

void TVector3::move(float dx, float dy)
{
    x += dx;
    y += dy;
}

void TVector3::move(float dx, float dy, float dz)
{
    x += dx;
    y += dy;
    z += dz;
}

void TVector3::moveX(float dx)
{
    x += dx;
}

void TVector3::moveY(float dy)
{
    y += dy;
}

void TVector3::moveZ(float dz)
{
    z += dz;
}
