#include "datetime.h"

#include <time.h>

TDateTime::TDateTime() :
    mValue(0)
{

}

TDateTime::TDateTime(const long &t) :
    mValue(t)
{

}

void TDateTime::operator =(long t)
{
    mValue = t;
}

std::string TDateTime::toString()
{
    return std::string(ctime(&mValue));
}

TDateTime TDateTime::fromString(const std::string &s)
{
    (void)s;
    return TDateTime();
}

long TDateTime::getValue() const
{
    return mValue;
}

void TDateTime::setValue(long value)
{
    mValue = value;
}

