#include "variable.h"
#include "utils.h"

static const char *g_modelTypeStr[] = {
    "Unknown",
    "Game",
    "Scene",
    "Character",
    "CharacterCollection",
    ""
};
IMPLEMENT_TYPE_TO_STRING(ModelType,g_modelTypeStr,MT_UNKNOWN)

static const char *g_gameEventStr[] = {
    "None",
    "Key down",
    "Change dir",
    "Idle",
    "Enter in air",
    "Landing",
    "Falling",
    ""
};
IMPLEMENT_TYPE_TO_STRING(GameEvent,g_gameEventStr,GE_NONE)


static const char *g_genderStr[] = {
    "Male",
    "Female",
    "Unknown",
    ""
};
IMPLEMENT_TYPE_TO_STRING(CharacterGender,g_genderStr,CG_UNKNOWN)

static const char *g_spriteTypeStr[] = {
    "Image",
    "Button",
    "Text",
    "BlinkText",
    "Cursor",
    "Character",
    "Effect",
    "Door",
    "Terrian",
    "Ghost",
    ""
};
IMPLEMENT_TYPE_TO_STRING(SpriteType,g_spriteTypeStr,ST_COUNT)

static const char *g_spriteStateStr[] = {
    "None",
    "Born",
    "OnGround",
    "InAir",
    "Spawn",
    "Died",
    ""
};
IMPLEMENT_TYPE_TO_STRING(SpriteState,g_spriteStateStr,SS_NONE)

static const char *g_spriteDirectionStr[] = {
    "None",
    "East",
    "South",
    "West",
    "North",
    "SouthEast",
    "SouthWest",
    "NorthWest",
    "NorthEast",
    ""
};
IMPLEMENT_TYPE_TO_STRING(SpriteDirection,g_spriteDirectionStr,SD_NONE)

static const char *g_actionModeStr[] = {
    "once",
    "recycle",
    "default",
    ""
};
IMPLEMENT_TYPE_TO_STRING(ActionMode,g_actionModeStr,AM_DEFAULT)

static const char *g_soundSetModelStr[] = {
    "Ordered",
    "Random",
    ""
};
IMPLEMENT_TYPE_TO_STRING(SoundSetModel,g_soundSetModelStr,SSM_ORDERED)

static const char *g_gameOperationStr[] = {
    "None",
    "Exit",
    "LeaveScene",
    ""
};
IMPLEMENT_TYPE_TO_STRING(GameOperation,g_gameOperationStr,GO_NONE)

static const char *g_cameraMoveStr[] = {
    "None",
    "X",
    "Y",
    "Z",
    "XY",
    "All",
    ""
};
IMPLEMENT_TYPE_TO_STRING(CameraMove,g_cameraMoveStr,CM_MOVE_ALL)

static const char *g_moveSegmentTypeStr[] = {
    "None",
    "Line",
    "Random",
    ""
};
IMPLEMENT_TYPE_TO_STRING(MoveSegmentType,g_moveSegmentTypeStr,MST_NONE)

static const char *g_interruptTypeStr[] = {
    "Disable",
    "Key event only",
    "All",
    ""
};
IMPLEMENT_TYPE_TO_STRING(InterruptType,g_interruptTypeStr,IT_DISABLE)

static const char *g_vectorTypeStr[] = {
    "Relative",
    "Absolute",
    ""
};
IMPLEMENT_TYPE_TO_STRING(VectorType,g_vectorTypeStr,VT_RELATIVE)
