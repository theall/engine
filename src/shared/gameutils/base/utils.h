#ifndef GAMEUTILS_BASE_UTILS_H
#define GAMEUTILS_BASE_UTILS_H

#define FREE_CONTAINER(x) \
    for(auto a : x) \
        delete a; \
    x.clear()


#include <string>
#include <vector>

std::string readText(const std::string &file);
int getIndexFromStringArray(const char *array[], const std::string &str, int maxCount=99);

#define DEFINE_TYPE_TO_STRING(_type) \
std::string _type##ToString(_type t);\
_type StringTo##_type(const std::string &s);

#define IMPLEMENT_TYPE_TO_STRING(_type,array,defValue) \
std::string _type##ToString(_type t) \
{\
    return array[t];\
}\
_type StringTo##_type(const std::string &s)\
{\
    int i = getIndexFromStringArray(array, s);\
    if(i != -1)\
        return (_type)i;\
    return defValue;\
}

std::string replace(const std::string &s, const std::string &rep, const std::string &to);

std::string getCurrentTimeString(const char *format = "%d/%d/%d %d:%d:%d");
std::string timeToString(time_t time, const char *format = "%d/%d/%d %d:%d:%d");
time_t stringToTime(const std::string & time_string);


std::string extractSuffix(const std::string &fileName);
std::string extractPath(const std::string &fileName);
std::string extractBaseName(const std::string &fileName);
std::string extractFileName(const std::string &fileName);
void readFile(const std::string &fileName, uint8_t **buffer, long *size);
std::string trim(const std::string &s);
std::string trim(std::string s);
void split(const std::string &s, const std::string &delim, std::vector<std::string > *ret);

std::string lower(const std::string &s);
std::string upper(const std::string &s);

std::string join(std::vector<std::string> wsl, const std::string &delim);

bool contains(std::vector<std::string> wsl, const std::string &delim, bool caseSensitive);
const char *extractSuffix(const char *src);
bool contains(std::vector<std::string> sl, const std::string &s, bool caseSensitive = false);

template<typename T>
bool contains(std::vector<T> container, T item);

bool isFileExist(const std::string &filePath);
bool isDirExist(const std::string &filePath);
std::string getCurrentDir();
void setCurrentDirectory(const std::string &path);

void createDir(const std::string &path);

std::string format(const char *fmt, ...);
float getRandValue(int min,int max);

#endif // GAMEUTILS_BASE_UTILS_H
