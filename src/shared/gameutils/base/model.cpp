#include "model.h"

static const char *K_UUID = "uuid";
static const char *K_TYPE = "type";
static const char *K_NAME = "name";
static const char *K_AUTHOR = "author";
static const char *K_EMAIL = "email";
static const char *K_VERSION = "version";
static const char *K_COMMENT = "comment";
static const char *K_CREATE_TIME = "create_time";
static const char *K_UPDATE_TIME = "update_time";

using namespace Model;

TModel::TModel(ModelType modelType) :
    mModelType(modelType)
{

}

TModel::~TModel()
{

}

nlohmann::json TModel::toJson() const
{
    nlohmann::json j = {};
    j.emplace(K_UUID, mUuid);
    j.emplace(K_NAME, mName);
    j.emplace(K_TYPE, ModelTypeToString(mModelType));
    j.emplace(K_AUTHOR, mAuthor);
    j.emplace(K_VERSION, mVersion);
    j.emplace(K_EMAIL, mEmail);
    j.emplace(K_COMMENT, mComment);
    j.emplace(K_CREATE_TIME, mCreatTime);
    j.emplace(K_UPDATE_TIME, mUpdateTime);
    return j;
}

ModelType TModel::modelType() const
{
    return mModelType;
}

bool TModel::loadFromString(const std::string &s)
{
    return loadFromData((void*)s.c_str(), s.size());
}

bool TModel::loadFromFile(const std::string &fileName)
{
    std::string fileData = readText(fileName);
    if(fileData.size() < 1) {
        throw std::string("Failed to read ") + fileName;
        return false;
    }
    bool state = false;
    try {
        state = loadFromString(fileData);
    } catch (std::string s) {
        throw format("Failed to load %s, \n\t%s", fileName.c_str(), s.c_str());
    }
    return state;
}

std::string TModel::toJsonString() const
{
    nlohmann::json jsonObject = toJson();
    jsonObject.emplace(K_TYPE, ModelTypeToString(mModelType));
    return jsonObject.dump(4);
}

void TModel::setModelType(const ModelType &modelType)
{
    mModelType = modelType;
}

bool TModel::loadFromData(void *content, int size)
{
    nlohmann::json jsonObject = {};
    try {
        char *data = (char*)content;
        jsonObject = nlohmann::json::parse(data, data+size);
    } catch(nlohmann::detail::exception e) {
        throw std::string(e.what());
    } catch(...) {
        throw std::string("Unknow error while parsing json data.");
    }

    try {
        ModelType modelType = StringToModelType(jsonObject[K_TYPE].get<std::string>());
        if(modelType != mModelType)
        {
            throw format("Invalid model type, need '%s', actual model type is '%s'", ModelTypeToString(mModelType).c_str(), ModelTypeToString(modelType).c_str());
            return false;
        }
        mUuid = jsonObject[K_UUID].get<std::string>();
        mName = jsonObject.value(K_NAME, "unknown");
        mAuthor = jsonObject.value(K_AUTHOR, "unknown");
        mEmail = jsonObject.value(K_EMAIL, "unknown");
        mVersion = jsonObject[K_VERSION].get<std::string>();
        mComment = jsonObject.value(K_COMMENT, "");
        mCreatTime = jsonObject[K_CREATE_TIME].get<std::string>();
        mUpdateTime = jsonObject[K_UPDATE_TIME].get<std::string>();

        loadFromJson(jsonObject);
    } catch(nlohmann::detail::exception e) {
        throw std::string(e.what());
        return false;
    }
    return true;
}

std::string TModel::getName() const
{
    return mName;
}

void TModel::setName(const std::string &name)
{
    mName = name;
}

std::string TModel::getGuid() const
{
    return mUuid;
}

void TModel::setGuid(const std::string &guid)
{
    mUuid = guid;
}

std::string TModel::getUuid() const
{
    return mUuid;
}

void TModel::setUuid(const std::string &uuid)
{
    mUuid = uuid;
}

std::string TModel::getAuthor() const
{
    return mAuthor;
}

void TModel::setAuthor(const std::string &author)
{
    mAuthor = author;
}

std::string TModel::getVersion() const
{
    return mVersion;
}

void TModel::setVersion(const std::string &version)
{
    mVersion = version;
}

std::string TModel::getEmail() const
{
    return mEmail;
}

void TModel::setEmail(const std::string &email)
{
    mEmail = email;
}

std::string TModel::getComment() const
{
    return mComment;
}

void TModel::setComment(const std::string &comment)
{
    mComment = comment;
}

std::string TModel::getCreatTime() const
{
    return mCreatTime;
}

void TModel::setCreatTime(const std::string &creatTime)
{
    mCreatTime = creatTime;
}

std::string TModel::getUpdateTime() const
{
    return mUpdateTime;
}

void TModel::setUpdateTime(const std::string &updateTime)
{
    mUpdateTime = updateTime;
}
