#ifndef MODEL_H
#define MODEL_H

#include "jsoninterface.h"
#include "variable.h"

namespace Model {

class TModel : public TJsonReader, TJsonWriter
{
public:
    TModel(ModelType modelType);
    ~TModel();

    virtual nlohmann::json toJson() const override;

    ModelType modelType() const;
    void setModelType(const ModelType &modelType);

    bool loadFromData(void *content, int size);
    bool loadFromString(const std::string &s);
    bool loadFromFile(const std::string &fileName);

    std::string toJsonString() const;

    std::string getName() const;
    void setName(const std::string &name);

    std::string getGuid() const;
    void setGuid(const std::string &guid);

    std::string getUuid() const;
    void setUuid(const std::string &uuid);

    std::string getAuthor() const;
    void setAuthor(const std::string &author);

    std::string getVersion() const;
    void setVersion(const std::string &version);

    std::string getEmail() const;
    void setEmail(const std::string &email);

    std::string getComment() const;
    void setComment(const std::string &comment);

    std::string getCreatTime() const;
    void setCreatTime(const std::string &creatTime);

    std::string getUpdateTime() const;
    void setUpdateTime(const std::string &updateTime);

protected:
    std::string mUuid;
    std::string mName;
    std::string mVersion;
    std::string mAuthor;
    std::string mEmail;
    std::string mComment;
    std::string mCreatTime;
    std::string mUpdateTime;
    ModelType mModelType;
};

}

#endif // MODEL_H
