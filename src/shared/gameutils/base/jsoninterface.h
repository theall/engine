#ifndef JSONINTERFACE_H
#define JSONINTERFACE_H

#include "../../../thirdparty/json/json.hpp"

class TJsonWriter
{
public:
    virtual nlohmann::json toJson() const = 0;
    virtual ~TJsonWriter() {}
};

class TJsonReader
{
public:
    virtual void loadFromJson(nlohmann::json j, void *context = nullptr) = 0;
    virtual ~TJsonReader() {}
};

#endif // JSONINTERFACE_H
