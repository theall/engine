#include "color.h"

TColor::TColor() :
    r(0)
  , g(0)
  , b(0)
  , a(255)
{

}

TColor::TColor(uchar _r, uchar _g, uchar _b, uchar _a) :
    r(_r)
  , g(_g)
  , b(_b)
  , a(_a)
{

}

TColor TColor::fromJson(nlohmann::json j)
{
    TColor color;
    int size = j.size();
    if(size >= 3)
    {
        color.r = j[0].get<uchar>();
        color.g = j[1].get<uchar>();
        color.b = j[2].get<uchar>();
        if(size==4)
            color.a = j[3].get<uchar>();
    }
    return color;
}

nlohmann::json TColor::toJson() const
{
    nlohmann::json j = nlohmann::json::array();
    j.push_back(r);
    j.push_back(g);
    j.push_back(b);
    j.push_back(a);
    return j;
}

std::string TColor::toString(const char *format) const
{
    char buf[256];
    sprintf(buf, format, r, g, b, a);
    return std::string(buf);
}

void TColor::operator =(const TColor &color)
{
    r = color.r;
    g = color.g;
    b = color.b;
    a = color.a;
}
