#ifndef DATETIME_H
#define DATETIME_H

#include <string>

class TDateTime
{
public:
    TDateTime();
    TDateTime(const long &t);

    void operator =(long t);
    std::string toString();
    static TDateTime fromString(const std::string &s);

    long getValue() const;
    void setValue(long value);

private:
    long mValue;
};

#endif // DATETIME_H
