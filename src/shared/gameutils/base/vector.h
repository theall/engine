#ifndef TVECTOR_H
#define TVECTOR_H

#include "jsoninterface.h"

class TVector2
{
public:
    float x;
    float y;

    TVector2(float _x=0.0, float _y=0.0);
    static TVector2 fromJson(nlohmann::json j);
    nlohmann::json toJson() const;
    std::string toString(const char *format = "%.2f,%.2f") const;

    void operator =(const TVector2 &vec);
    TVector2 operator +(const TVector2 &vec);
    TVector2 operator -(const TVector2 &vec);
    void operator +=(const TVector2 &vec);
    void operator -=(const TVector2 &vec);
    TVector2 operator /(int i);
    TVector2 operator /=(int i);

    bool isNull();
    TVector2 mirror(float _x = 0) const;
    float sum() const;
    float distance() const;
    float area() const;
};

class TVector3
{
public:
    float x;
    float y;
    float z;

    TVector3(float _x=0.0, float _y=0.0, float _z=0.0);
    static TVector3 fromJson(nlohmann::json j);

    void move(float dx, float dy);
    void move(float dx, float dy, float dz);
    void moveX(float dx);
    void moveY(float dy);
    void moveZ(float dz);

};

#endif // TVECTOR_H
