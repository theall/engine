#include "rect.h"

TRect::TRect(float _x, float _y, float _w, float _h) :
    x(_x)
  , y(_y)
  , w(_w)
  , h(_h)
{

}

TRect::TRect(TVector2 leftTop, TVector2 size)
{
    x = leftTop.x;
    y = leftTop.y;
    w = size.x;
    h = size.y;
}

TRect::TRect(const TRect &rect)
{
    x = rect.x;
    y = rect.y;
    w = rect.w;
    h = rect.h;
}

void TRect::operator =(const TRect &rect)
{
    x = rect.x;
    y = rect.y;
    w = rect.w;
    h = rect.h;
}

bool TRect::isValid() const
{
    return w>0 && h>0;
}

bool TRect::isEmpty() const
{
    return w<=0 || h<=0;
}

bool TRect::isNull() const
{
    return w==0&&h==0;
}

bool TRect::contain(float _x, float _y) const
{
    return _x>=x&&_x<=(x+w)&&_y>=y&&_y<=(y+h);
}

bool TRect::contain(const TVector2 &point) const
{
    return contain(point.x, point.y);
}

TRect TRect::mirror(float _x) const
{
    return TRect(_x*2-x-w, y, w, h);
}

float TRect::vcenter(float subHeight) const
{
    return y + (h - subHeight) / 2;
}

float TRect::hcenter(float subWidth) const
{
    return x + (w - subWidth) / 2;
}

TRect TRect::offset(const TVector2 &position) const
{
    TRect ret;
    ret.x = x + position.x;
    ret.y = y + position.y;
    ret.w = w;
    ret.h = h;
    return ret;
}

TVector2 TRect::leftTop() const
{
    return TVector2(x, y);
}

void TRect::setLeftTop(float left, float top)
{
    x = left;
    y = top;
}

void TRect::setLeftTop(const TVector2 &leftTop)
{
    x = leftTop.x;
    y = leftTop.y;
}

TVector2 TRect::bottomRight() const
{
    return TVector2(y+h, x+w);
}

TVector2 TRect::rightBottom() const
{
    return TVector2(x+w, y+h);
}

void TRect::setBottomRight(float bottom, float right)
{
    w = right - x;
    if(w < 0)
        w = 0;
    h = bottom - y;
    if(h < 0)
        h = 0;
}

void TRect::setBottomRight(const TVector2 &bottomRight)
{
    w = bottomRight.y - x;
    if(w < 0)
        w = 0;
    h = bottomRight.x - y;
    if(h < 0)
        h = 0;
}

float TRect::bottom() const
{
    return y + h;
}

float TRect::right() const
{
    return x + w;
}

TVector2 TRect::size() const
{
    return TVector2(w, h);
}

void TRect::setSize(float width, float height)
{
    w = width;
    h = height;
}

void TRect::move(float dx, float dy)
{
    x += dx;
    y += dy;
}

void TRect::move(const TVector2 &dxy)
{
    x += dxy.x;
    y += dxy.y;
}

TRect TRect::intersection(const TRect &rect) const
{
    TRect ret;
    ret.x = x>rect.x?x:rect.x;
    ret.y = y>rect.y?y:rect.y;

    float bottom1 = y + h;
    float bottom2 = rect.y + rect.h;
    float bottom = bottom1<bottom2?bottom1:bottom2;
    float right1 = x + w;
    float right2 = rect.x + rect.w;
    float right = right1<right2?right1:right2;
    ret.w = right - ret.x;
    ret.h = bottom - ret.y;
    return ret;
}

TRect TRect::unionWith(const TRect &rect) const
{
    TRect ret;
    ret.x = x<rect.x?x:rect.x;
    ret.y = y<rect.y?y:rect.y;

    float bottom1 = y + h;
    float bottom2 = rect.y + rect.h;
    float bottom = bottom1>bottom2?bottom1:bottom2;
    float right1 = x + w;
    float right2 = rect.x + rect.w;
    float right = right1>right2?right1:right2;
    ret.w = right - ret.x;
    ret.h = bottom - ret.y;
    return ret;
}

bool TRect::operator ==(const TRect &rect) const
{
    return x==rect.x && y==rect.y && w==rect.w && h==rect.h;
}

std::string TRect::toString(const char *format) const
{
    char buf[256];
    sprintf(buf, format, x, y, w, h);
    return std::string(buf);
}

TRect TRect::fromJson(nlohmann::json j)
{
    TRect rect;
    if(j.size() == 4)
    {
        rect.x = j[0];
        rect.y = j[1];
        rect.w = j[2];
        rect.h = j[3];
    }
    return rect;
}

nlohmann::json TRect::toJson() const
{
    nlohmann::json j = nlohmann::json::array();
    j.push_back(x);
    j.push_back(y);
    j.push_back(w);
    j.push_back(h);
    return j;
}

TRectList Model::mirror(const TRectList &rectList, float x)
{
    TRectList newRectList;
    for(TRect rect : rectList)
        newRectList.emplace_back(rect.mirror(x));

    return newRectList;
}

nlohmann::json Model::toJson(const TRectList &rectList)
{
    nlohmann::json j = nlohmann::json::array();
    for(TRect rect : rectList)
    {
        j.push_back(rect.toJson());
    }
    return j;
}
