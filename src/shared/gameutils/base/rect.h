#ifndef RECT_H
#define RECT_H

#include <vector>
#include "vector.h"

class TRect
{
public:
    float x;
    float y;
    float w;
    float h;

    TRect(float _x=0, float _y=0, float _w=0, float _h=0);
    TRect(TVector2 leftTop, TVector2 size);
    TRect(const TRect &rect);
    static TRect fromJson(nlohmann::json j);
    nlohmann::json toJson() const;
    void operator =(const TRect &rect);

    bool isValid() const;
    bool isEmpty() const;
    bool isNull() const;

    bool contain(float x, float y) const;
    bool contain(const TVector2 &point) const;
    TRect mirror(float _x) const;
    float vcenter(float subHeight = 0.0) const;
    float hcenter(float subWidth = 0.0) const;
    TRect offset(const TVector2 &position) const;

    TVector2 leftTop() const;
    void setLeftTop(float left, float top);
    void setLeftTop(const TVector2 &leftTop);

    TVector2 bottomRight() const;
    TVector2 rightBottom() const;
    void setBottomRight(float bottom, float right);
    void setBottomRight(const TVector2 &bottomRight);

    float bottom() const;
    float right() const;

    TVector2 size() const;
    void setSize(float width, float height);
    void move(float dx, float dy);
    void move(const TVector2 &dxy);

    TRect intersection(const TRect &rect) const;
    TRect unionWith(const TRect &rect) const;
    bool operator ==(const TRect &rect) const;

    std::string toString(const char *format = "%.2f,%.2f,%.2f,%.2f") const;
};

typedef std::vector<TRect> TRectList;

namespace Model {
    TRectList mirror(const TRectList &rectList, float x = 0.0);
    nlohmann::json toJson(const TRectList &rectList);
}

#endif // RECT_H
