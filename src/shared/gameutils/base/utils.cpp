#include "utils.h"

#include <algorithm>
#include <time.h>
#include <cstdarg> // vararg
#include <cstdio> // vsnprintf
#include <string.h>

std::string readText(const std::string &file)
{
    std::string ret;
    FILE *fp = fopen(file.c_str(), "rb");
    if(fp)
    {
        fseek(fp, 0, SEEK_END);
        int fileSize = ftell(fp);
        fseek(fp, 0, SEEK_SET);
        char *buf = new char[fileSize+sizeof(int)];
        int sizeRead = fread(buf, 1, fileSize, fp);
        fclose(fp);

        if(sizeRead == fileSize)
        {
            *(int*)(buf+fileSize) = 0;
            ret = buf;
        }
        delete buf;
    }
    return ret;
}

int getIndexFromStringArray(const char *array[], const std::string &str, int maxCount)
{
    std::string newStr(str);
    std::transform(newStr.begin(), newStr.end(), newStr.begin(), ::tolower);
    int i=0;
    while(true)
    {
        std::string tempStr = array[i];
        if(i>=maxCount || tempStr=="")
            break;

        std::transform(tempStr.begin(), tempStr.end(), tempStr.begin(), ::tolower);
        if(tempStr==newStr)
            return i;

        i++;
    }
    return -1;
}

std::string replace(const std::string &s, const std::string &rep, const std::string &to)
{
    std::string str(s);
    int repSize = rep.size();
    if(repSize < 1)
        return str;
    int npos = 0;
    while(true)
    {
        int i = str.find(rep, npos);
        if(i >= 0)
        {
            str.replace(i, repSize, to);
        } else {
            break;
        }
        npos = i+repSize;
    }
    return str;
}

time_t stringToTime(const std::string & time_string)
{
    struct tm tm1;
    time_t time1;
    sscanf(time_string.c_str(), "%d/%d/%d %d:%d:%d" ,
                &(tm1.tm_year),
                &(tm1.tm_mon),
                &(tm1.tm_mday),
                &(tm1.tm_hour),
                &(tm1.tm_min),
                &(tm1.tm_sec));

    tm1.tm_year -= 1900;
    tm1.tm_mon --;
    tm1.tm_isdst=-1;
    time1 = mktime(&tm1);

    return time1;
}

std::string timeToString(time_t t, const char *format)
{
    char buf[256] = {0};
    tm *tm1 = localtime(&t);
    sprintf(buf, format, tm1->tm_year+1900, tm1->tm_mon+1, tm1->tm_mday, tm1->tm_hour, tm1->tm_min, tm1->tm_sec);
    return std::string(buf);
}

std::string getCurrentTimeString(const char *format)
{
    time_t t = time(NULL);
    return timeToString(t, format);
}


std::string extractSuffix(const std::string &fileName)
{
    int l = fileName.size();
    int pos = fileName.find_last_of(".");
    if(pos < 0)
        return std::string();
    pos++;
    std::string ret = fileName.substr(pos, l-pos);
    // std::transform(ret.begin(), ret.end(), ret.begin(), ::towlower);
    return ret;
}

std::string extractFileName(const std::string &fileName)
{
    int pos = fileName.find_last_of("/");
    if(pos < 0)
    {
        pos = fileName.find_last_of("\\");
        if(pos < 0)
            return fileName;
    }
    pos++;
    std::string ret = fileName.substr(pos, fileName.size()-pos);
    return ret;
}

std::string extractBaseName(const std::string &fileName)
{
    int pos = fileName.find_last_of("/");
    if(pos < 0)
    {
        pos = fileName.find_last_of("\\");
        if(pos < 0)
            return fileName;
    }
    pos++;
    std::string ret = fileName.substr(pos, fileName.size()-pos);
    pos = ret.find_last_of(".");
    if(pos > 0)
        ret = ret.substr(0, pos);

    return ret;
}

std::string extractPath(const std::string &fileName)
{
    int pos = fileName.find_last_of("/");
    if(pos < 0)
    {
        pos = fileName.find_last_of("\\");
        if(pos < 0)
            return fileName;
    }
    pos++;
    std::string ret = fileName.substr(0, pos);
    return ret;
}

std::string format(const char *fmt, ...)
{
    va_list args;
    int size_buffer = 256, size_out;
    char *buffer;
    while (true)
    {
        buffer = new char[size_buffer];
        memset(buffer, 0, size_buffer);

        va_start(args, fmt);
        size_out = vsnprintf(buffer, size_buffer, fmt, args);
        va_end(args);

        // see http://perfec.to/vsnprintf/pasprintf.c
        // if size_out ...
        //      == -1             --> output was truncated
        //      == size_buffer    --> output was truncated
        //      == size_buffer-1  --> ambiguous, /may/ have been truncated
        //       > size_buffer    --> output was truncated, and size_out
        //                            bytes would have been written
        if(size_out == size_buffer || size_out == -1 || size_out == size_buffer-1)
            size_buffer *= 2;
        else if(size_out > size_buffer)
            size_buffer = size_out + 2; // to avoid the ambiguous case
        else
            break;

        delete[] buffer;
    }
    std::string message = buffer;
    delete[] buffer;
    return message;
}

template<typename T>
bool contains(std::vector<T> container, T item)
{
    for(T t : container)
    {
        if(t==item)
            return true;
    }
    return false;
}

float getRandValue(int min,int max)
{
    if(min == max)
        return min;
    double m1=(double)(rand()%101)/101;
    min++;
    double m2=(double)((rand()%(max-min+1))+min);
    m2=m2-1;
    return m1+m2;
}
