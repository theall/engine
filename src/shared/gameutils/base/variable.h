#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>
#include "utils.h"

enum ModelType
{
    MT_UNKNOWN,
    MT_GAME,
    MT_SCENE,
    MT_CHARACTER,
    MT_CHARACTER_COLLECTION,
    MT_COUNT
};
DEFINE_TYPE_TO_STRING(ModelType)

enum GameEvent
{
    GE_NONE,
    GE_KEYDOWN,
    GE_CHANGE_DIR,
    GE_IDLE,
    GE_IN_AIR,
    GE_LANDING,
    GE_FALLING,
    GE_COUNT
};
DEFINE_TYPE_TO_STRING(GameEvent)

enum CharacterGender
{
    CG_MALE,
    CG_FEMALE,
    CG_UNKNOWN,
    CG_COUNT
};
DEFINE_TYPE_TO_STRING(CharacterGender)

enum SpriteType
{
    ST_IMAGE,
    ST_BUTTON,
    ST_TEXT,
    ST_BLINK_TEXT,
    ST_CURSOR,
    ST_CHARACTER,
    ST_EFFECT,
    ST_DOOR,
    ST_TERRIAN,
    ST_GHOST,
    ST_COUNT
};
DEFINE_TYPE_TO_STRING(SpriteType)

enum SpriteState
{
    SS_NONE,
    SS_BORN,
    SS_ON_GROUND,
    SS_IN_AIR,
    SS_SPAWN,
    SS_DIED,
    SS_COUNT
};
DEFINE_TYPE_TO_STRING(SpriteState)

enum SpriteDirection
{
    SD_NONE,
    SD_EAST,
    SD_SOUTH,
    SD_WEST,
    SD_NORTH,
    SD_SOUTH_EAST,
    SD_SOUTH_WEST,
    SD_NORTH_WEST,
    SD_NORTH_EAST,
    SD_COUNT
};
DEFINE_TYPE_TO_STRING(SpriteDirection)

enum ActionMode
{
    AM_DEFAULT,
    AM_ONCE,
    AM_RECYCLE,
    AM_COUNT
};
DEFINE_TYPE_TO_STRING(ActionMode)

enum SoundSetModel
{
    SSM_ORDERED,
    SSM_RANDOM,
    SSM_COUNT
};
DEFINE_TYPE_TO_STRING(SoundSetModel)

enum GameOperation
{
    GO_NONE,
    GO_EXIT,
    GO_LEAVE_SCENE,
    GO_COUNT
};
DEFINE_TYPE_TO_STRING(GameOperation)

enum CameraMove
{
    CM_MOVE_NONE = 0x0,
    CM_MOVE_X = 0x1,
    CM_MOVE_Y = 0x2,
    CM_MOVE_Z = 0x4,
    CM_MOVE_XY = CM_MOVE_X|CM_MOVE_Y,
    CM_MOVE_ALL = CM_MOVE_XY|CM_MOVE_Z,
    CM_COUNT
};
DEFINE_TYPE_TO_STRING(CameraMove)

enum MoveSegmentType
{
    MST_NONE,
    MST_LINE,
    MST_RANDOM,
    MST_COUNT
};
DEFINE_TYPE_TO_STRING(MoveSegmentType)

enum InterruptType
{
    IT_DISABLE,
    IT_KEY_EVENT,
    IT_ALL,
    IT_COUNT
};
DEFINE_TYPE_TO_STRING(InterruptType)

enum VectorType
{
    VT_RELATIVE,
    VT_ABSOLUTE,
    VT_COUNT
};
DEFINE_TYPE_TO_STRING(VectorType)

#endif // VARIABLE_H
