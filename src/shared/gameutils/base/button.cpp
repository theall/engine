#include "button.h"

struct ButtonMap
{
    TButton::ButtonType buttonType;
    std::string shortName;
};

const char *g_codeString[] = {
    "",
    "↑",
    "↗",
    "→",
    "↘",
    "↓",
    "↙",
    "←",
    "↖",
    "A",
    "B",
    "C",
    "X",
    "Y",
    "Z",
    "L1",
    "L2",
    "L3",
    "R1",
    "R2",
    "R3",
    ""
};

const ButtonMap g_buttonMap[] =
{
    {TButton::DIR, "↑+→+↓+←"},
    {TButton::NO_UP, "→+↓+←"},
    {TButton::NO_RIGHT, "↑+↓+←"},
    {TButton::NO_DOWN, "↑+→+←"},
    {TButton::NO_LEFT, "↑+→+↓"},
    {TButton::UP_RIGHT, "↗"},
    {TButton::RIGHT_DOWN, "↘"},
    {TButton::DOWN_LEFT, "↙"},
    {TButton::LEFT_UP, "↖"},
    {TButton::UP, "↑"},
    {TButton::RIGHT, "→"},
    {TButton::DOWN, "↓"},
    {TButton::LEFT, "←"},
    {TButton::A, "A"},
    {TButton::B, "B"},
    {TButton::C, "C"},
    {TButton::X, "X"},
    {TButton::Y, "Y"},
    {TButton::Z, "Z"},
    {TButton::L1, "L1"},
    {TButton::L2, "L2"},
    {TButton::L3, "L3"},
    {TButton::R1, "R1"},
    {TButton::R2, "R2"},
    {TButton::R3, "R3"},
    {TButton::SELECT, "M"},
    {TButton::START, "T"}
};

TButton::TButton()
{

}

TButton::TButton(TButton::ButtonType buttonType)
{
    mButtonType = buttonType;
}

TButton::~TButton()
{

}

std::string TButton::getShortName()
{
    for(uint32_t i=0;i<sizeof(g_buttonMap)/sizeof(ButtonMap);i++)
    {
        const ButtonMap *k = &g_buttonMap[i];
        if(mButtonType==k->buttonType)
        {
            return k->shortName;
        }
    }
    return "";
}

std::string TButton::toString(int value)
{
    std::string ret;
    for(uint32_t i=0;i<sizeof(g_buttonMap)/sizeof(ButtonMap);i++)
    {
        const ButtonMap *k = &g_buttonMap[i];
        if((value&k->buttonType)==k->buttonType)
        {
            ret += "+";
            ret += k->shortName;
        }
    }
    if(ret.size() > 0)
        ret.erase(ret.begin());

    return ret;
}
