#ifndef BUTTON_H
#define BUTTON_H

#include <string>

class TButton
{
public:
    enum ButtonType
    {
        NONE = 0x0,
        UP = 0x1,
        RIGHT = 0x2,
        DOWN = 0x4,
        LEFT = 0x8,
        A = 0x10,
        B = 0x20,
        C = 0x40,
        X = 0x80,
        Y = 0x100,
        Z = 0x200,
        L1 = 0x400,
        L2 = 0x800,
        L3 = 0x1000,
        R1 = 0x2000,
        R2 = 0x4000,
        R3 = 0x8000,
        SELECT = 0x10000000,
        START = 0x20000000,

        UP_RIGHT = UP|RIGHT,
        RIGHT_DOWN = RIGHT|DOWN,
        DOWN_LEFT = DOWN|LEFT,
        LEFT_UP = LEFT|UP,

        DIR = UP|RIGHT|DOWN|LEFT,
        NO_UP = DIR^UP,
        NO_RIGHT = DIR^RIGHT,
        NO_DOWN = DIR^DOWN,
        NO_LEFT = DIR^LEFT,

        ALL = 0xffffffff
    };

    TButton();
    TButton(ButtonType buttonType);
    ~TButton();

    std::string getShortName();
    static std::string toString(int value);

private:
    ButtonType mButtonType;
};

#endif // BUTTON_H
