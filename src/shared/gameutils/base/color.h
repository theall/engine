#ifndef COLOR_H
#define COLOR_H

#include "jsoninterface.h"

typedef unsigned char uchar;
class TColor
{
public:
    uchar r,g,b,a;
    TColor();
    TColor(uchar _r, uchar _g, uchar _b, uchar _a=255);
    static TColor fromJson(nlohmann::json j);
    nlohmann::json toJson() const;
    std::string toString(const char *format = "%d %d %d") const;

    void operator =(const TColor &color);
};

#endif // COLOR_H
