#ifndef CHARACTERCOLLECTIONMODEL_H
#define CHARACTERCOLLECTIONMODEL_H

#include "../base/model.h"

namespace Model {

class TCharacterCollection : TModel
{
public:
    TCharacterCollection();
    TCharacterCollection(nlohmann::json j, void *context = nullptr);
    TCharacterCollection(void *fileData, int size);

    void loadFromJson(nlohmann::json j, void *context = nullptr);

private:

};

}

#endif // CHARACTERCOLLECTIONMODEL_H
