#include "rectarea.h"

static const char *K_RECT = "rect";

using namespace Model;

TRectArea::TRectArea() :
    TArea(TArea::Rect)
{

}

nlohmann::json TRectArea::toJson() const
{
    nlohmann::json j = TArea::toJson();
    j.emplace(K_RECT, mRect.toJson());
    return j;
}

void TRectArea::loadFromJson(nlohmann::json j, void *context)
{
    TArea::loadFromJson(j, context);

    mRect = TRect::fromJson(j);
}

TRect TRectArea::rect() const
{
    return mRect;
}

void TRectArea::setRect(const TRect &rect)
{
    mRect = rect;
}
