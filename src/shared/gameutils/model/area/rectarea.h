#ifndef MODEL_RECTAREA_H
#define MODEL_RECTAREA_H

#include "area.h"
#include "../../base/rect.h"

namespace Model {

class TRectArea : public TArea
{
public:
    TRectArea();

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context) override;

    TRect rect() const;
    void setRect(const TRect &rect);

private:
    TRect mRect;
};

}

#endif // MODEL_RECTAREA_H
