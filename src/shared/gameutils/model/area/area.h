#ifndef MODEL_AREA_H
#define MODEL_AREA_H

#include "../../base/jsoninterface.h"

namespace Model {

class TArea : public TJsonReader, TJsonWriter
{
public:
    enum Type
    {
        Rect,
        Circle
    };

    TArea(Type type);

    Type type() const;

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context) override;

private:
    Type mType;
};

typedef std::vector<TArea *> TAreaList;
}

#endif // MODEL_AREA_H
