#include "circlearea.h"

using namespace Model;

TCircleArea::TCircleArea() :
    TArea(TArea::Circle)
{

}

nlohmann::json TCircleArea::toJson() const
{
    return TArea::toJson();
}

void TCircleArea::loadFromJson(nlohmann::json j, void *context)
{
    TArea::loadFromJson(j, context);
}
