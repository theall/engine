#include "area.h"
#include "../../base/utils.h"

static const char *K_TYPE = "type";

using namespace Model;

static const char *g_areaTypeStr[] = {
    "Rect",
    "Circle"
};

std::string areaTypeToString(TArea::Type type)
{
    return g_areaTypeStr[type];
}

TArea::Type stringToAreaType(const std::string &str)
{
    int i = getIndexFromStringArray(g_areaTypeStr, str);
    if(i < 0)
        throw format("Invalid area type string: %s", str.c_str());
    return (TArea::Type)i;
}

TArea::TArea(Type type) :
    mType(type)
{

}

TArea::Type TArea::type() const
{
    return mType;
}

nlohmann::json TArea::toJson() const
{
    nlohmann::json j;
    j.emplace(K_TYPE, areaTypeToString(mType));
    return j;
}

void TArea::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    mType = stringToAreaType(j.value(K_TYPE, "rect"));
}
