#ifndef MODEL_CIRCLEAREA_H
#define MODEL_CIRCLEAREA_H

#include "area.h"
#include "../../base/rect.h"

namespace Model {

class TCircleArea : public TArea
{
public:
    TCircleArea();

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context) override;

private:
};

}

#endif // MODEL_CIRCLEAREA_H
