#include "charactercollection.h"

using namespace Model;

TCharacterCollection::TCharacterCollection() :
    TModel(MT_CHARACTER_COLLECTION)
{

}

TCharacterCollection::TCharacterCollection(nlohmann::json j, void *context):
    TModel(MT_CHARACTER_COLLECTION)
{
    loadFromJson(j, context);
}

TCharacterCollection::TCharacterCollection(void *fileData, int size) :
    TModel(MT_CHARACTER_COLLECTION)
{
    (void)fileData;
    (void)size;
}

void TCharacterCollection::loadFromJson(nlohmann::json j, void *context)
{
    (void)j;
    (void)context;
}
