#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include "../base/model.h"
#include "../base/vector.h"

namespace Model {

class TGame : public TModel
{
public:
    TGame();
    TGame(nlohmann::json j, void *context = nullptr);
    TGame(void *fileData, int size);
    ~TGame();

    // TModel interface
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    nlohmann::json toJson() const override;

    std::vector<std::string> fontList() const;
    void setFontList(const std::vector<std::string> &fontList);

    std::vector<std::string> scenePathList() const;
    void setScenePathList(const std::vector<std::string> &scenePathList);

    int entrySceneIndex() const;
    void setEntrySceneIndex(int entrySceneIndex);

    std::vector<std::string> characterPathList() const;
    void setCharacterPathList(const std::vector<std::string> &characterPathList);

    TVector2 resolution() const;
    void setResolution(const TVector2 &resolution);

private:
    int mEntrySceneIndex;
    TVector2 mResolution;
    std::vector<std::string> mScenePathList;
    std::vector<std::string> mFontList;
    std::vector<std::string> mCharacterPathList;
};

typedef std::vector<TGame*> TGameList;

}

#endif // GAMEMODEL_H
