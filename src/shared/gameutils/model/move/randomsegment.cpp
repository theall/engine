#include "randomsegment.h"

static const char *K_RECT = "rect";
static const char *K_RETURN_POINT = "return_point";
static const char *K_RETURN_VELOCITY = "return_velocity";

using namespace Model;

TRandomSegment::TRandomSegment() :
    TMoveSegment(MST_RANDOM)
  , mReturnVelocity(0.0)
{

}

TRandomSegment::TRandomSegment(nlohmann::json j, void *context) :
    TMoveSegment(MST_RANDOM)
  , mReturnVelocity(0.0)
{
    loadFromJson(j, context);
}

TRandomSegment::~TRandomSegment()
{

}

TRect TRandomSegment::randomRect() const
{
    return mRandomRect;
}

void TRandomSegment::setRandomRect(const TRect &randomRect)
{
    mRandomRect = randomRect;
}

TVector2 TRandomSegment::returnPoint() const
{
    return mReturnPoint;
}

void TRandomSegment::setReturnPoint(const TVector2 &returnPoint)
{
    mReturnPoint = returnPoint;
}

float TRandomSegment::returnVelocity() const
{
    return mReturnVelocity;
}

void TRandomSegment::setReturnVelocity(float returnVelocity)
{
    mReturnVelocity = returnVelocity;
}

nlohmann::json TRandomSegment::toJson() const
{
    nlohmann::json j;
    j.emplace(K_RECT, mRandomRect.toJson());
    j.emplace(K_RETURN_POINT, mReturnPoint.toJson());
    j.emplace(K_RETURN_VELOCITY, mReturnVelocity);
    return j;
}

void TRandomSegment::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    mRandomRect = TRect::fromJson(j[K_RECT]);
    mReturnPoint = TVector2::fromJson(j[K_RETURN_POINT]);
    mReturnVelocity = j.value(K_RETURN_VELOCITY, 0.0f);
}
