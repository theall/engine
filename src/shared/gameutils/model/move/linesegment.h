#ifndef MODEL_LINESEGMENT_H
#define MODEL_LINESEGMENT_H

#include "../../base/vector.h"
#include "movesegment.h"

namespace Model {

class TLineSegment : public TMoveSegment
{
public:
    TLineSegment();
    TLineSegment(nlohmann::json j, void *context = nullptr);
    ~TLineSegment();

    TVector2 vector() const;
    void setVector(const TVector2 &vector);

    TSpeedModel speedModel() const;
    void setSpeedModel(const TSpeedModel &speedModel);

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

private:
    TVector2 mVector;
    TSpeedModel mSpeedModel;
};

}

#endif // MODEL_LINESEGMENT_H
