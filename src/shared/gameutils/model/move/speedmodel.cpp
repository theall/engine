#include "speedmodel.h"
#include "../../base/utils.h"

static const char *K_POINT = "point";
static const char *K_VELOCITY = "velocity";

using namespace Model;

TSpeedModel::TSpeedModel() :
    mStart(0.0)
  , mEnd(1.0)
  , mVelocity(1.0)
{

}

TSpeedModel::TSpeedModel(nlohmann::json j, void *context)
{
    loadFromJson(j, context);
}

TSpeedModel::~TSpeedModel()
{

}

void TSpeedModel::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    mVelocity = j.value(K_VELOCITY, 1.0);
    if(j.contain(K_POINT))
    {
        TVector2 point = TVector2::fromJson(j[K_POINT]);
        mStart = point.x;
        mEnd = point.y;
        if(mStart < 0)
            mStart = 0;
        else if (mStart > 1)
            mStart = 1;
        if(mEnd < 0)
            mEnd = 0;
        else if (mEnd > 1)
            mEnd = 1;
    }
}

nlohmann::json TSpeedModel::toJson() const
{
    nlohmann::json j;
    j.emplace(K_POINT, TVector2(mStart,mEnd).toJson());
    j.emplace(K_VELOCITY, mVelocity);
    return j;
}

float TSpeedModel::start() const
{
    return mStart;
}

void TSpeedModel::setStart(float start)
{
    mStart = start;
}

float TSpeedModel::end() const
{
    return mEnd;
}

void TSpeedModel::setEnd(float end)
{
    mEnd = end;
}

float TSpeedModel::velocity() const
{
    return mVelocity;
}

void TSpeedModel::setVelocity(float velocity)
{
    mVelocity = velocity;
}
