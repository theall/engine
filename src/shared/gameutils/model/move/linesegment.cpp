#include "linesegment.h"

static const char *K_VECTOR = "vector";
static const char *K_SPEED_MODEL = "speed_model";

using namespace Model;

TLineSegment::TLineSegment() :
    TMoveSegment(MST_LINE)
{

}

TLineSegment::TLineSegment(nlohmann::json j, void *context) :
    TMoveSegment(MST_LINE)
{
    loadFromJson(j, context);
}

TLineSegment::~TLineSegment()
{

}

TVector2 TLineSegment::vector() const
{
    return mVector;
}

void TLineSegment::setVector(const TVector2 &vector)
{
    mVector = vector;
}

TSpeedModel TLineSegment::speedModel() const
{
    return mSpeedModel;
}

void TLineSegment::setSpeedModel(const TSpeedModel &speedModel)
{
    mSpeedModel = speedModel;
}

nlohmann::json TLineSegment::toJson() const
{
    nlohmann::json j = TMoveSegment::toJson();
    j.emplace(K_VECTOR, mVector.toJson());
    j[K_SPEED_MODEL] = mSpeedModel.toJson();
    return j;
}

void TLineSegment::loadFromJson(nlohmann::json j, void *context)
{
    TMoveSegment::loadFromJson(j, context);
    mVector = TVector2::fromJson(j[K_VECTOR]);
    if(j.contain(K_SPEED_MODEL))
        mSpeedModel.loadFromJson(j[K_SPEED_MODEL], context);
}
