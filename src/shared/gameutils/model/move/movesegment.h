#ifndef MODEL_MOVESEGMENT_H
#define MODEL_MOVESEGMENT_H

#include <vector>
#include "speedmodel.h"
#include "../../base/variable.h"

namespace Model {

class TMoveSegment : public TJsonReader, TJsonWriter
{
public:
    TMoveSegment(MoveSegmentType moveSegmentType = MST_NONE);
    ~TMoveSegment();

    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    nlohmann::json toJson() const override;

    MoveSegmentType moveSegmentType() const;
    void setMoveSegmentType(const MoveSegmentType &moveSegmentType);

private:
    MoveSegmentType mMoveSegmentType;
};

typedef std::vector<TMoveSegment*> TMoveSegmentList;
}

#endif // MODEL_MOVESEGMENT_H
