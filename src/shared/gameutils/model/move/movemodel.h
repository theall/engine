#ifndef MODEL_MOVEMODEL_H
#define MODEL_MOVEMODEL_H

#include "movesegment.h"

namespace Model {

class TMoveModel : public TJsonReader, TJsonWriter
{
public:
    TMoveModel();
    ~TMoveModel();

    TMoveSegmentList moveSegmentList() const;
    void setMoveSegmentList(const TMoveSegmentList &moveSegmentList);

    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    nlohmann::json toJson() const override;

    int loopCount() const;
    void setLoopCount(int loopCount);

private:
    int mLoopCount;
    TMoveSegmentList mMoveSegmentList;
};

}

#endif // MODEL_MOVEMODEL_H
