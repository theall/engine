#include "movesegment.h"

static const char *K_TYPE = "type";

using namespace Model;

TMoveSegment::TMoveSegment(MoveSegmentType moveSegmentType) :
    mMoveSegmentType(moveSegmentType)
{

}

TMoveSegment::~TMoveSegment()
{

}

void TMoveSegment::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    mMoveSegmentType = StringToMoveSegmentType(j.value(K_TYPE, "None"));
}

nlohmann::json TMoveSegment::toJson() const
{
    nlohmann::json j;
    j[K_TYPE] = MoveSegmentTypeToString(mMoveSegmentType);
    return j;
}

MoveSegmentType TMoveSegment::moveSegmentType() const
{
    return mMoveSegmentType;
}

void TMoveSegment::setMoveSegmentType(const MoveSegmentType &moveSegmentType)
{
    mMoveSegmentType = moveSegmentType;
}
