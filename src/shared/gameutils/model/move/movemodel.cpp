#include "movemodel.h"
#include "linesegment.h"
#include "randomsegment.h"

static const char *K_LOOP_COUNT = "loop_count";
static const char *K_SEGMENTS = "segments";
static const char *K_SEGMENT_TYPE = "type";

using namespace Model;

TMoveModel::TMoveModel() :
    mLoopCount(0)
{

}

TMoveModel::~TMoveModel()
{

}

TMoveSegmentList TMoveModel::moveSegmentList() const
{
    return mMoveSegmentList;
}

void TMoveModel::setMoveSegmentList(const TMoveSegmentList &moveSegmentList)
{
    mMoveSegmentList = moveSegmentList;
}

void TMoveModel::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    mLoopCount = j.value(K_LOOP_COUNT, 0);

    FREE_CONTAINER(mMoveSegmentList);
    for(nlohmann::json ja : j[K_SEGMENTS])
    {
        MoveSegmentType moveSegmentType = StringToMoveSegmentType(ja[K_SEGMENT_TYPE]);
        if(moveSegmentType == MST_LINE)
            mMoveSegmentList.emplace_back(new TLineSegment(ja));
        else if(moveSegmentType == MST_RANDOM)
            mMoveSegmentList.emplace_back(new TRandomSegment(ja));
    }
}

nlohmann::json TMoveModel::toJson() const
{
    nlohmann::json j;
    j.emplace(K_LOOP_COUNT, mLoopCount);

    nlohmann::json segmentArray = nlohmann::json::array();
    for(TMoveSegment *moveSegment : mMoveSegmentList)
    {
        segmentArray.emplace_back(moveSegment->toJson());
    }
    j.emplace(K_SEGMENTS, segmentArray);
    return j;
}

int TMoveModel::loopCount() const
{
    return mLoopCount;
}

void TMoveModel::setLoopCount(int loopCount)
{
    mLoopCount = loopCount;
}
