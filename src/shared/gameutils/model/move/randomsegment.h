#ifndef MODEL_RANDOMSEGMENT_H
#define MODEL_RANDOMSEGMENT_H

#include "../../base/rect.h"
#include "movesegment.h"

namespace Model {

class TRandomSegment : public TMoveSegment
{
public:
    TRandomSegment();
    TRandomSegment(nlohmann::json j, void *context = nullptr);
    ~TRandomSegment();

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

    TRect randomRect() const;
    void setRandomRect(const TRect &randomRect);

    TVector2 returnPoint() const;
    void setReturnPoint(const TVector2 &returnPoint);

    float returnVelocity() const;
    void setReturnVelocity(float returnVelocity);

private:
    TRect mRandomRect;
    TVector2 mReturnPoint;
    float mReturnVelocity;
};

}
#endif // MODEL_RANDOMSEGMENT_H
