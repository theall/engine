#ifndef MODEL_SPEEDMODEL_H
#define MODEL_SPEEDMODEL_H

#include "../../base/vector.h"

namespace Model {

class TSpeedModel : public TJsonReader, TJsonWriter
{
public:
    TSpeedModel();
    TSpeedModel(nlohmann::json j, void *context = nullptr);
    ~TSpeedModel();

    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    nlohmann::json toJson() const override;

    float start() const;
    void setStart(float start);

    float end() const;
    void setEnd(float end);

    float velocity() const;
    void setVelocity(float velocity);

private:
    float mStart;
    float mEnd;
    float mVelocity;
};

}

#endif // MODEL_SPEEDMODEL_H
