#include "character.h"

#include "../base/variable.h"

static const char *K_SCALE = "scale";
static const char *K_ICON = "icon";
static const char *K_GENDER = "gender";
static const char *K_AGE = "age";
static const char *K_WEIGHT = "weight";
static const char *K_HEIGHT = "height";
static const char *K_FORCE = "force";
static const char *K_DEFEND = "defend";
//static const char *K_SOURCE = "source";
static const char *K_ACTIONS = "actions";

using namespace Model;

TCharacter::TCharacter() :
    TModel(MT_CHARACTER)
  , mGender(CG_MALE)
{

}

TCharacter::TCharacter(nlohmann::json j, void *context) :
    TModel(MT_CHARACTER)
  , mGender(CG_MALE)
{
    loadFromJson(j, context);
}

TCharacter::~TCharacter()
{
    FREE_CONTAINER(mActionList);
}

TActionList TCharacter::getActionList() const
{
    return mActionList;
}

void TCharacter::setActionList(const TActionList &actionList)
{
    mActionList = actionList;
}

int TCharacter::getWeight() const
{
    return mWeight;
}

void TCharacter::setWeight(int weight)
{
    mWeight = weight;
}

int TCharacter::getForce() const
{
    return mForce;
}

void TCharacter::setForce(int force)
{
    mForce = force;
}

int TCharacter::getDefend() const
{
    return mDefend;
}

void TCharacter::setDefend(int defend)
{
    mDefend = defend;
}

std::string TCharacter::getAiCode() const
{
    return mAiCode;
}

void TCharacter::setAiCode(const std::string &aiCode)
{
    mAiCode = aiCode;
}

CharacterGender TCharacter::getGender() const
{
    return mGender;
}

void TCharacter::setGender(const CharacterGender &gender)
{
    mGender = gender;
}

std::string TCharacter::getIconPath() const
{
    return mIconPath;
}

void TCharacter::setIconPath(const std::string &iconPath)
{
    mIconPath = iconPath;
}

int TCharacter::getHeight() const
{
    return mHeight;
}

void TCharacter::setHeight(int height)
{
    mHeight = height;
}

float TCharacter::getScale() const
{
    return mScale;
}

void TCharacter::setScale(float scale)
{
    mScale = scale;
}

int TCharacter::getAge() const
{
    return mAge;
}

void TCharacter::setAge(int age)
{
    mAge = age;
}

void TCharacter::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;

    mAge = j.value(K_AGE, 0);
    mIconPath = j[K_ICON].get<std::string>();
    mScale = j.value(K_SCALE, 1.0f);
    mWeight = j.value(K_WEIGHT, 0);
    mHeight = j.value(K_HEIGHT, 0);
    mForce = j.value(K_FORCE, 0);
    mDefend = j.value(K_DEFEND, 0);
    mGender = StringToCharacterGender(j.value(K_GENDER, "Male"));

    // Action list
    FREE_CONTAINER(mActionList);
    for (nlohmann::json a : j[K_ACTIONS]) {
        mActionList.emplace_back(new TAction(a, this));
    }
}

nlohmann::json TCharacter::toJson() const
{
    nlohmann::json j = TModel::toJson();
    j.emplace(K_AGE, mAge);
    j.emplace(K_ICON, mIconPath);
    j.emplace(K_SCALE, mScale);
    j.emplace(K_WEIGHT, mWeight);
    j.emplace(K_HEIGHT, mHeight);
    j.emplace(K_FORCE, mForce);
    j.emplace(K_DEFEND, mDefend);
    j.emplace(K_GENDER, CharacterGenderToString(mGender));


    nlohmann::json actionList = nlohmann::json::array();
    for(TAction *action : mActionList)
    {
        actionList.emplace_back(action->toJson());
    }
    j.emplace(K_ACTIONS, actionList);

    return j;
}
