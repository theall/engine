#include "layer.h"
#include "layer/charactersprite.h"
#include "layer/imagesprite.h"
#include "layer/buttonsprite.h"
#include "layer/doorsprite.h"
#include "layer/terriansprite.h"
#include "layer/ghostsprite.h"

static const char *K_SPRITES = "sprites";
static const char *K_OFFSET = "offset";
static const char *K_TYPE = "type";
static const char *K_SPEED_FACTOR = "speed_factor";

using namespace Model;

TLayer::TLayer() :
    mSpeedFactor(TVector2(1.0, 1.0))
{

}

TLayer::TLayer(nlohmann::json j, void *context) :
    mSpeedFactor(TVector2(1.0, 1.0))
{
    loadFromJson(j, context);
}

TLayer::~TLayer()
{
    FREE_CONTAINER(mSpriteList);
}

void TLayer::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;

    if(j.contain(K_OFFSET))
        mOffset = TVector2::fromJson(j[K_OFFSET]);
    if(j.contain(K_SPEED_FACTOR))
        mSpeedFactor = TVector2::fromJson(j[K_SPEED_FACTOR]);

    FREE_CONTAINER(mSpriteList);
    for(nlohmann::json ja : j[K_SPRITES])
    {
        std::string spriteTypeName = ja[K_TYPE];
        SpriteType spriteType = StringToSpriteType(spriteTypeName);
        if(spriteType == ST_CHARACTER)
            mSpriteList.emplace_back(new TCharacterSprite(ja, context));
        else if(spriteType == ST_IMAGE)
            mSpriteList.emplace_back(new TImageSprite(ja, context));
        else if(spriteType == ST_BUTTON)
            mSpriteList.emplace_back(new TButtonSprite(ja, context));
        else if(spriteType == ST_DOOR)
            mSpriteList.emplace_back(new TDoorSprite(ja, context));
        else if(spriteType == ST_TERRIAN)
            mSpriteList.emplace_back(new TTerrianSprite(ja, context));
        else if(spriteType == ST_GHOST)
            mSpriteList.emplace_back(new TGhostSprite(ja, context));
        else
            throw format("Invalid sprite type \"%s\"", spriteTypeName.c_str());
    }
}

TVector2 TLayer::offset() const
{
    return mOffset;
}

void TLayer::setOffset(const TVector2 &offset)
{
    mOffset = offset;
}

TSpriteList TLayer::spriteList() const
{
    return mSpriteList;
}

void TLayer::setSpriteList(const TSpriteList &spriteList)
{
    mSpriteList = spriteList;
}

TVector2 TLayer::speedFactor() const
{
    return mSpeedFactor;
}

void TLayer::setSpeedFactor(const TVector2 &speedFactor)
{
    mSpeedFactor = speedFactor;
}

nlohmann::json TLayer::toJson() const
{
    nlohmann::json j = {};
    j.emplace(K_OFFSET, mOffset.toJson());
    j.emplace(K_SPEED_FACTOR, mSpeedFactor.toJson());
    nlohmann::json spriteList = nlohmann::json::array();
    for(TSprite *sprite : mSpriteList)
    {
        spriteList.emplace_back(sprite->toJson());
    }
    j.emplace(K_SPRITES, spriteList);

    return j;
}
