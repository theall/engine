#ifndef MODEL_BUTTONSPRITE_H
#define MODEL_BUTTONSPRITE_H

#include "imagesprite.h"
#include "textsprite.h"
#include "../../../base/rect.h"

namespace Model {

class TButtonSprite : public TSprite
{
public:
    TButtonSprite();
    TButtonSprite(nlohmann::json j, void *context = nullptr);
    ~TButtonSprite();

    TImageSprite normalImage() const;
    void setNormalImage(const TImageSprite &normalImage);

    TImageSprite disabledImage() const;
    void setDisabledImage(const TImageSprite &disabledImage);

    TImageSprite selectedImage() const;
    void setSelectedImage(const TImageSprite &selectedImage);

    TImageSprite clickedImage() const;
    void setClickedImage(const TImageSprite &clickedImage);

    std::string text() const;
    void setText(const std::string &text);

    TColor textColor() const;
    void setTextColor(const TColor &color);

    std::string name() const;
    void setName(const std::string &name);

    TVector2 size() const;
    void setSize(const TVector2 &size);

private:
    std::string mName;
    TVector2 mSize;
    TImageSprite mNormalImage;
    TImageSprite mDisabledImage;
    TImageSprite mSelectedImage;
    TImageSprite mClickedImage;
    TTextSprite mTextSprite;
    std::string mEnterSound;
    std::string mClickSound;

    // TJsonWriter interface
public:
    nlohmann::json toJson() const override;

    // TJsonReader interface
public:
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    std::string enterSound() const;
    void setEnterSound(const std::string &enterSound);
    std::string clickSound() const;
    void setClickSound(const std::string &clickSound);
    TTextSprite textSprite() const;
    void setTextSprite(const TTextSprite &textSprite);
};

}

#endif // MODEL_BUTTONSPRITE_H
