#ifndef MODEL_CHARACTERSPRITET_H
#define MODEL_CHARACTERSPRITET_H

#include "sprite.h"

namespace Model {

class TCharacterSprite : public TSprite
{
public:
    TCharacterSprite();
    TCharacterSprite(nlohmann::json j, void *context = nullptr);
    ~TCharacterSprite();

    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    nlohmann::json toJson() const override;

    std::string uuid() const;
    void setUuid(const std::string &uuid);

    SpriteDirection direction() const;
    void setDirection(const SpriteDirection &direction);

private:
    std::string mUuid;
    SpriteDirection mDirection;
};

typedef std::vector<TCharacterSprite*> TCharacterSpriteList;
}

#endif // MODEL_CHARACTERSPRITET_H
