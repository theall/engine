#include "buttonsprite.h"

static const char *K_NAME = "name";
static const char *K_TEXT = "text";
static const char *K_NORMAL = "normal";
static const char *K_DISABLED = "disabled";
static const char *K_SELECTED = "selected";
static const char *K_CLICKED = "clicked";
static const char *K_SIZE = "size";
static const char *K_ENTER_SOUND = "enter_sound";
static const char *K_CLICK_SOUND = "click_sound";

using namespace Model;

TButtonSprite::TButtonSprite() :
    TSprite(ST_BUTTON)
{

}

TButtonSprite::TButtonSprite(nlohmann::json j, void *context) :
    TSprite(ST_BUTTON)
{
    loadFromJson(j, context);
}

TButtonSprite::~TButtonSprite()
{

}

TImageSprite TButtonSprite::clickedImage() const
{
    return mClickedImage;
}

void TButtonSprite::setClickedImage(const TImageSprite &clickedImage)
{
    mClickedImage = clickedImage;
}

std::string TButtonSprite::name() const
{
    return mName;
}

void TButtonSprite::setName(const std::string &name)
{
    mName = name;
}

TVector2 TButtonSprite::size() const
{
    return mSize;
}

void TButtonSprite::setSize(const TVector2 &size)
{
    mSize = size;
}

TTextSprite TButtonSprite::textSprite() const
{
    return mTextSprite;
}

void TButtonSprite::setTextSprite(const TTextSprite &textSprite)
{
    mTextSprite = textSprite;
}

std::string TButtonSprite::clickSound() const
{
    return mClickSound;
}

void TButtonSprite::setClickSound(const std::string &clickSound)
{
    mClickSound = clickSound;
}

std::string TButtonSprite::enterSound() const
{
    return mEnterSound;
}

void TButtonSprite::setEnterSound(const std::string &enterSound)
{
    mEnterSound = enterSound;
}

std::string TButtonSprite::text() const
{
    return mTextSprite.text();
}

void TButtonSprite::setText(const std::string &text)
{
    mTextSprite.setText(text);
}

TColor TButtonSprite::textColor() const
{
    return mTextSprite.textColor();
}

void TButtonSprite::setTextColor(const TColor &color)
{
    mTextSprite.setTextColor(color);
}

TImageSprite TButtonSprite::selectedImage() const
{
    return mSelectedImage;
}

void TButtonSprite::setSelectedImage(const TImageSprite &selectedImage)
{
    mSelectedImage = selectedImage;
}

TImageSprite TButtonSprite::disabledImage() const
{
    return mDisabledImage;
}

void TButtonSprite::setDisabledImage(const TImageSprite &disabledImage)
{
    mDisabledImage = disabledImage;
}

TImageSprite TButtonSprite::normalImage() const
{
    return mNormalImage;
}

void TButtonSprite::setNormalImage(const TImageSprite &normalImage)
{
    mNormalImage = normalImage;
}

nlohmann::json TButtonSprite::toJson() const
{
    nlohmann::json j = TSprite::toJson();
    j.emplace(K_NAME, mName);
    j.emplace(K_TEXT, mTextSprite.toJson());
    j.emplace(K_ENTER_SOUND, mEnterSound);
    j.emplace(K_CLICK_SOUND, mClickSound);
    j.emplace(K_NORMAL, mNormalImage.toJson());
    j.emplace(K_DISABLED, mDisabledImage.toJson());
    j.emplace(K_SELECTED, mSelectedImage.toJson());
    j.emplace(K_CLICKED, mClickedImage.toJson());
    j.emplace(K_SIZE, mSize.toJson());
    return j;
}

void TButtonSprite::loadFromJson(nlohmann::json j, void *context)
{
    TSprite::loadFromJson(j, context);
    mName = j[K_NAME];
    mEnterSound = j[K_ENTER_SOUND];
    mClickSound = j[K_CLICK_SOUND];
    mSize = TVector2::fromJson(j[K_SIZE]);
    mNormalImage.loadFromJson(j[K_NORMAL]);
    mDisabledImage.loadFromJson(j[K_DISABLED]);
    mSelectedImage.loadFromJson(j[K_SELECTED]);
    mClickedImage.loadFromJson(j[K_CLICKED]);
    mTextSprite.loadFromJson(j[K_TEXT], this);
}
