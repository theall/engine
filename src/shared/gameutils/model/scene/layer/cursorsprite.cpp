#include "cursorsprite.h"

static const char *K_FILE = "file";

using namespace Model;

TCursorSprite::TCursorSprite() :
    TSprite(ST_CURSOR)
{

}

TCursorSprite::~TCursorSprite()
{

}

std::string TCursorSprite::file() const
{
    return mFile;
}

void TCursorSprite::setFile(const std::string &file)
{
    mFile = file;
}

TVector2 TCursorSprite::hotPos() const
{
    return mPos;
}

void TCursorSprite::setHotPos(const TVector2 &hotPos)
{
    mPos = hotPos;
}

nlohmann::json TCursorSprite::toJson() const
{
    nlohmann::json j = TSprite::toJson();
    j.emplace(K_FILE, mFile);
    return j;
}

void TCursorSprite::loadFromJson(nlohmann::json j, void *context)
{
    TSprite::loadFromJson(j, context);
    mFile = j.value(K_FILE, "");
}
