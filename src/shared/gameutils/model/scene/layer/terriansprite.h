#ifndef MODEL_TERRIANSPRITE_H
#define MODEL_TERRIANSPRITE_H

#include "sprite.h"
#include "../../../base/rect.h"

namespace Model {

class TTerrianSprite : public TSprite
{
public:
    TTerrianSprite();
    TTerrianSprite(nlohmann::json j, void *context = nullptr);
    ~TTerrianSprite();

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

    TRect rect() const;
    void setRect(const TRect &rect);

private:
    TRect mRect;
};

}

#endif // MODEL_TERRIANSPRITE_H
