#include "charactersprite.h"

static const char *K_UUID = "uuid";
static const char *K_DIRECTION = "direction";

using namespace Model;

TCharacterSprite::TCharacterSprite() :
    TSprite(ST_CHARACTER)
{

}

TCharacterSprite::TCharacterSprite(nlohmann::json j, void *context) :
    TSprite(ST_CHARACTER)
{
    loadFromJson(j, context);
}

TCharacterSprite::~TCharacterSprite()
{

}

std::string TCharacterSprite::uuid() const
{
    return mUuid;
}

void TCharacterSprite::setUuid(const std::string &uuid)
{
    mUuid = uuid;
}

SpriteDirection TCharacterSprite::direction() const
{
    return mDirection;
}

void TCharacterSprite::setDirection(const SpriteDirection &direction)
{
    mDirection = direction;
}

void TCharacterSprite::loadFromJson(nlohmann::json j, void *context)
{
    TSprite::loadFromJson(j, context);

    mUuid = j[K_UUID].get<std::string>();
    mDirection = StringToSpriteDirection(j.value(K_DIRECTION, "east"));
}

nlohmann::json TCharacterSprite::toJson() const
{
    nlohmann::json j = TSprite::toJson();
    j.emplace(K_UUID, mUuid);
    j.emplace(K_DIRECTION, SpriteDirectionToString(mDirection));
    return j;
}
