#include "sprite.h"

static const char *K_TYPE = "type";
static const char *K_POS = "pos";
static const char *K_MOVE_MODEL = "move_model";

using namespace Model;

TSprite::TSprite(SpriteType type) :
    mType(type)
{

}

TSprite::~TSprite()
{

}

SpriteType TSprite::type() const
{
    return mType;
}

void TSprite::setType(const SpriteType &type)
{
    mType = type;
}

TVector2 TSprite::position() const
{
    return mPos;
}

void TSprite::setPosition(const TVector2 &position)
{
    mPos = position;
}

TMoveModel TSprite::moveModel() const
{
    return mMoveModel;
}

void TSprite::setMoveModel(const TMoveModel &moveModel)
{
    mMoveModel = moveModel;
}

nlohmann::json TSprite::toJson() const
{
    nlohmann::json j;
    j.emplace(K_TYPE, SpriteTypeToString(mType));
    j.emplace(K_POS, mPos.toJson());
    j.emplace(K_MOVE_MODEL, mMoveModel.toJson());
    return j;
}

void TSprite::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    SpriteType spriteType = StringToSpriteType(j[K_TYPE]);
    if(spriteType != mType)
        throw format("Dismatch sprite type, need %s, actual sprite type is %s", SpriteTypeToString(mType).c_str(), SpriteTypeToString(spriteType).c_str());

    mPos = TVector2::fromJson(j[K_POS]);
    if(j.contain(K_MOVE_MODEL))
        mMoveModel.loadFromJson(j[K_MOVE_MODEL], context);
}
