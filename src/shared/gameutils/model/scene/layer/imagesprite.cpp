#include "imagesprite.h"

static const char *K_IMAGE_FILE = "image";

using namespace Model;

TImageSprite::TImageSprite() :
    TSprite(ST_IMAGE)
{

}

TImageSprite::TImageSprite(nlohmann::json j, void *context) :
    TSprite(ST_IMAGE)
{
    loadFromJson(j, context);
}

TImageSprite::~TImageSprite()
{

}

std::string TImageSprite::imagePath() const
{
    return mImagePath;
}

void TImageSprite::setImagePath(const std::string &imagePath)
{
    mImagePath = imagePath;
}

nlohmann::json TImageSprite::toJson() const
{
    nlohmann::json j = TSprite::toJson();
    j.emplace(K_IMAGE_FILE, mImagePath);
    return j;
}

void TImageSprite::loadFromJson(nlohmann::json j, void *context)
{
    TSprite::loadFromJson(j, context);

    mImagePath = j[K_IMAGE_FILE].get<std::string>();
}
