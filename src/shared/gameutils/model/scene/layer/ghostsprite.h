#ifndef MODEL_GHOSTSPRITE_H
#define MODEL_GHOSTSPRITE_H

#include "sprite.h"
#include "../../../base/rect.h"

namespace Model {

class TGhostSprite : public TSprite
{
public:
    TGhostSprite();
    TGhostSprite(nlohmann::json j, void *context = nullptr);
    ~TGhostSprite();

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

    TRect rect() const;
    void setRect(const TRect &rect);

private:
    TRect mRect;
};

}

#endif // MODEL_GHOSTSPRITE_H
