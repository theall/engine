#include "doorsprite.h"

static const char *K_ENTRY = "entry";
static const char *K_EXIT = "exit";

using namespace Model;

TDoorSprite::TDoorSprite() :
    TSprite(ST_DOOR)
{

}

TDoorSprite::TDoorSprite(nlohmann::json j, void *context) :
    TSprite(ST_DOOR)
{
    loadFromJson(j, context);
}

TDoorSprite::~TDoorSprite()
{

}

TRect TDoorSprite::entryRect() const
{
    return mEntryRect;
}

void TDoorSprite::setEntryRect(const TRect &entryRect)
{
    mEntryRect = entryRect;
}

TRect TDoorSprite::exitRect() const
{
    return mExitRect;
}

void TDoorSprite::setExitRect(const TRect &exitRect)
{
    mExitRect = exitRect;
}

nlohmann::json TDoorSprite::toJson() const
{
    nlohmann::json j;
    j.emplace(K_ENTRY, mEntryRect.toJson());
    j.emplace(K_EXIT, mExitRect.toJson());
    return j;
}

void TDoorSprite::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    mEntryRect = TRect::fromJson(j[K_ENTRY]);
    mExitRect = TRect::fromJson(j[K_EXIT]);
}
