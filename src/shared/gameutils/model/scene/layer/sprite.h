#ifndef MODEL_SPRITE_H
#define MODEL_SPRITE_H

#include "../../../base/variable.h"
#include "../../../base/vector.h"
#include "../../move/movemodel.h"

namespace Model {

class TSprite : public TJsonReader, TJsonWriter
{
public:
    TSprite(SpriteType type);
    ~TSprite();

    SpriteType type() const;
    void setType(const SpriteType &type);

    // TJsonWriter interface
    virtual nlohmann::json toJson() const;

    // TJsonReader interface
    virtual void loadFromJson(nlohmann::json j, void *context);
    TVector2 position() const;
    void setPosition(const TVector2 &position);

    TMoveModel moveModel() const;
    void setMoveModel(const TMoveModel &moveModel);

protected:
    TVector2 mPos;
    SpriteType mType;
    TMoveModel mMoveModel;
};

typedef std::vector<TSprite*> TSpriteList;
}

#endif // MODEL_SPRITE_H
