#ifndef MODEL_TEXTSPRITE_H
#define MODEL_TEXTSPRITE_H

#include "sprite.h"
#include "../../../base/color.h"

namespace Model {

class TTextSprite : public TSprite
{
public:
    TTextSprite();
    TTextSprite(nlohmann::json j, void *context = nullptr);
    ~TTextSprite();

private:
    std::string mText;
    TColor mTextColor;

    // TJsonWriter interface
public:
    nlohmann::json toJson() const override;

    // TJsonReader interface
public:
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    std::string text() const;
    void setText(const std::string &text);
    TColor textColor() const;
    void setTextColor(const TColor &textColor);
};

}

#endif // MODEL_TEXTSPRITE_H
