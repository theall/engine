#include "ghostsprite.h"

static const char *K_AREA = "area";

using namespace Model;

TGhostSprite::TGhostSprite() :
    TSprite(ST_TERRIAN)
{

}

TGhostSprite::TGhostSprite(nlohmann::json j, void *context) :
    TSprite(ST_TERRIAN)
{
    loadFromJson(j, context);
}

TGhostSprite::~TGhostSprite()
{

}

nlohmann::json TGhostSprite::toJson() const
{
    nlohmann::json j = TSprite::toJson();
    j.emplace(K_AREA, mRect.toJson());
    return j;
}

void TGhostSprite::loadFromJson(nlohmann::json j, void *context)
{
    TSprite::loadFromJson(j, context);

    mRect = TRect::fromJson(j[K_AREA]);
}

TRect TGhostSprite::rect() const
{
    return mRect;
}

void TGhostSprite::setRect(const TRect &rect)
{
    mRect = rect;
}
