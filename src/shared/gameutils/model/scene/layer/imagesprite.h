#ifndef MODEL_IMAGESPRITE_H
#define MODEL_IMAGESPRITE_H

#include "sprite.h"

namespace Model {

class TImageSprite : public TSprite
{
public:
    TImageSprite();
    TImageSprite(nlohmann::json j, void *context = nullptr);
    ~TImageSprite();

    std::string imagePath() const;
    void setImagePath(const std::string &imagePath);

private:
    std::string mImagePath;

    // TJsonWriter interface
public:
    nlohmann::json toJson() const override;

    // TJsonReader interface
public:
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
};

}

#endif // MODEL_IMAGESPRITE_H
