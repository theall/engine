#include "textsprite.h"

static const char *K_TEXT = "text";
static const char *K_COLOR = "color";

using namespace Model;

TTextSprite::TTextSprite() :
    TSprite(ST_TEXT)
{

}

TTextSprite::TTextSprite(nlohmann::json j, void *context) :
    TSprite(ST_TEXT)
{
    loadFromJson(j, context);
}

TTextSprite::~TTextSprite()
{

}

TColor TTextSprite::textColor() const
{
    return mTextColor;
}

void TTextSprite::setTextColor(const TColor &textColor)
{
    mTextColor = textColor;
}

std::string TTextSprite::text() const
{
    return mText;
}

void TTextSprite::setText(const std::string &text)
{
    mText = text;
}

nlohmann::json TTextSprite::toJson() const
{
    nlohmann::json j = TSprite::toJson();
    j.emplace(K_TEXT, mText);
    j.emplace(K_COLOR, mTextColor.toJson());
    return j;
}

void TTextSprite::loadFromJson(nlohmann::json j, void *context)
{
    TSprite::loadFromJson(j, context);
    mText = j[K_TEXT];
    mTextColor = TColor::fromJson(j[K_COLOR]);
}
