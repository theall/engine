#ifndef MODELCURSORSPRITE_H
#define MODELCURSORSPRITE_H

#include "sprite.h"

namespace Model {

class TCursorSprite : public TSprite
{
public:
    TCursorSprite();
    ~TCursorSprite();

    std::string file() const;
    void setFile(const std::string &file);

    TVector2 hotPos() const;
    void setHotPos(const TVector2 &hotPos);

private:
    std::string mFile;
    TVector2 mHotPos;

    // TJsonWriter interface
public:
    nlohmann::json toJson() const override;

    // TJsonReader interface
public:
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
};

}

#endif // MODELCURSORSPRITE_H
