#ifndef MODELBLINKTEXTSPRITE_H
#define MODELBLINKTEXTSPRITE_H

#include "textsprite.h"

namespace Model {

class TBlinkTextSprite : public TSprite
{
public:
    TBlinkTextSprite();
    TBlinkTextSprite(nlohmann::json j, void *context = nullptr);
    ~TBlinkTextSprite();

private:
    int mBlinkTimes;
    TTextSprite mTextSprite;

    // TJsonWriter interface
public:
    nlohmann::json toJson() const override;

    // TJsonReader interface
public:
    void loadFromJson(nlohmann::json j, void *context) override;
    int blinkTimes() const;
    void setBlinkTimes(int blinkTimes);
};

}

#endif // MODELBLINKTEXTSPRITE_H
