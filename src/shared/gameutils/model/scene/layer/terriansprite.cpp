#include "terriansprite.h"

static const char *K_SIZE = "size";

using namespace Model;

TTerrianSprite::TTerrianSprite() :
    TSprite(ST_TERRIAN)
{

}

TTerrianSprite::TTerrianSprite(nlohmann::json j, void *context) :
    TSprite(ST_TERRIAN)
{
    loadFromJson(j, context);
}

TTerrianSprite::~TTerrianSprite()
{

}

nlohmann::json TTerrianSprite::toJson() const
{
    nlohmann::json j = TSprite::toJson();
    j.emplace(K_SIZE, mRect.size().toJson());
    return j;
}

void TTerrianSprite::loadFromJson(nlohmann::json j, void *context)
{
    TSprite::loadFromJson(j, context);

    TVector2 size = TVector2::fromJson(j[K_SIZE]);
    mRect.setLeftTop(mPos);
    mRect.setSize(size.x, size.y);
}

TRect TTerrianSprite::rect() const
{
    return mRect;
}

void TTerrianSprite::setRect(const TRect &rect)
{
    mRect = rect;
}
