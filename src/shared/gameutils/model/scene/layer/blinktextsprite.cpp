#include "blinktextsprite.h"

using namespace Model;

TBlinkTextSprite::TBlinkTextSprite():
    TSprite(ST_BLINK_TEXT)
  , mBlinkTimes(1)
{

}

TBlinkTextSprite::TBlinkTextSprite(nlohmann::json j, void *context) :
    TSprite(ST_BLINK_TEXT)
  , mBlinkTimes(1)
{
    loadFromJson(j, context);
}

TBlinkTextSprite::~TBlinkTextSprite()
{

}

int TBlinkTextSprite::blinkTimes() const
{
    return mBlinkTimes;
}

void TBlinkTextSprite::setBlinkTimes(int blinkTimes)
{
    mBlinkTimes = blinkTimes;
}

nlohmann::json TBlinkTextSprite::toJson() const
{
    return nlohmann::json();
}

void TBlinkTextSprite::loadFromJson(nlohmann::json j, void *context)
{
    (void)j;
    (void)context;
}
