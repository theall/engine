#ifndef MODEL_DOORSPRITE_H
#define MODEL_DOORSPRITE_H

#include "../../../base/rect.h"
#include "sprite.h"

namespace Model {

class TDoorSprite : public TSprite
{
public:
    TDoorSprite();
    TDoorSprite(nlohmann::json j, void *context = nullptr);
    ~TDoorSprite();

    TRect entryRect() const;
    void setEntryRect(const TRect &entryRect);

    TRect exitRect() const;
    void setExitRect(const TRect &exitRect);

    // TJsonWriter interface
    virtual nlohmann::json toJson() const;

    // TJsonReader interface
    virtual void loadFromJson(nlohmann::json j, void *context);

private:
    TRect mEntryRect;
    TRect mExitRect;
};

}

#endif // MODEL_DOORSPRITE_H
