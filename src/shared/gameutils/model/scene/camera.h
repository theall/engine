#ifndef MODEL_CAMERA_H
#define MODEL_CAMERA_H

#include "../../base/rect.h"

namespace Model {

class TCamera : public TJsonReader, TJsonWriter
{
public:
    TCamera();
    ~TCamera();

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

    TRect rect() const;
    void setRect(const TRect &rect);

private:
    TRect mRect;
};

}

#endif // MODEL_CAMERA_H
