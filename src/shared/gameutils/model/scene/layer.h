#ifndef LAYERMODEL_H
#define LAYERMODEL_H

#include "../../base/vector.h"
#include "layer/sprite.h"

namespace Model {

class TLayer : public TJsonReader, TJsonWriter
{
public:
    TLayer();
    TLayer(nlohmann::json j, void *context = nullptr);
    ~TLayer();

    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    nlohmann::json toJson() const override;
    TVector2 offset() const;
    void setOffset(const TVector2 &offset);

    TSpriteList spriteList() const;
    void setSpriteList(const TSpriteList &spriteList);

    TVector2 speedFactor() const;
    void setSpeedFactor(const TVector2 &speedFactor);

private:
    TVector2 mOffset;
    TVector2 mSpeedFactor;
    TSpriteList mSpriteList;
};

typedef std::vector<TLayer*> TLayerList;
}

#endif // LAYERMODEL_H
