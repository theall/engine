#include "camera.h"

static const char *K_RECT = "rect";

using namespace Model;

TCamera::TCamera()
{

}

TCamera::~TCamera()
{

}

nlohmann::json TCamera::toJson() const
{
    nlohmann::json j;
    j.emplace(K_RECT, mRect.toJson());
    return j;
}

void TCamera::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    mRect = TRect::fromJson(j[K_RECT]);
}

TRect TCamera::rect() const
{
    return mRect;
}

void TCamera::setRect(const TRect &rect)
{
    mRect = rect;
}
