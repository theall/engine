#include "scene.h"

static const char *K_MAIN_LAYER = "main_layer";
static const char *K_LAYERS = "layers";
static const char *K_CURSOR = "cursor";
static const char *K_SOUND = "sound";
static const char *K_COLOR = "color";
static const char *K_CAMERA = "camera";
static const char *K_FPS = "fps";
static const char *K_GRAVITY = "gravity";

using namespace Model;

TScene::TScene() :
    TModel(MT_SCENE)
  , mFps(60)
  , mMainLayerIndex(0)
{

}

TScene::TScene(nlohmann::json j, void *context) :
    TModel(MT_SCENE)
  , mFps(60)
  , mMainLayerIndex(0)
{
    loadFromJson(j, context);
}

TScene::TScene(void *fileData, int size) :
    TModel(MT_SCENE)
  , mFps(60)
  , mMainLayerIndex(0)
{
    (void)fileData;
    (void)size;
}

TScene::~TScene()
{
    FREE_CONTAINER(mLayerList);
}

void TScene::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;

    mMainLayerIndex = j.value(K_MAIN_LAYER,0);
    mBackgroundColor = TColor::fromJson(j.value(K_COLOR, nlohmann::json()));
    mFps = j.value(K_FPS, 60);
    if(j.contain(K_CURSOR))
        mCursor.loadFromJson(j[K_CURSOR]);
    if(j.contain(K_SOUND))
        mSoundSet.loadFromJson(j[K_SOUND]);
    mCamera.loadFromJson(j[K_CAMERA]);
    if(j.contain(K_GRAVITY))
        mGravity = TVector2::fromJson(j[K_GRAVITY]);

    FREE_CONTAINER(mLayerList);
    for (nlohmann::json ja : j[K_LAYERS]) {
        mLayerList.emplace_back(new TLayer(ja));
    }
}

nlohmann::json TScene::toJson() const
{
    nlohmann::json j = nlohmann::json::array();
    j.emplace(K_MAIN_LAYER, mMainLayerIndex);
    j.emplace(K_COLOR, mBackgroundColor.toJson());
    j.emplace(K_CURSOR, mCursor.toJson());
    j.emplace(K_SOUND, mSoundSet.toJson());
    j.emplace(K_CAMERA, mCamera.toJson());
    j.emplace(K_FPS, mFps);
    j.emplace(K_GRAVITY, mGravity.toJson());
    nlohmann::json layerArray = nlohmann::json::array();
    for(TLayer *layer : mLayerList)
    {
        layerArray.emplace_back(layer->toJson());
    }
    j.emplace(K_LAYERS, layerArray);

    return j;
}

int TScene::mainLayerIndex() const
{
    return mMainLayerIndex;
}

TLayerList TScene::layerList() const
{
    return mLayerList;
}

void TScene::setMainLayerIndex(int mainLayerIndex)
{
    mMainLayerIndex = mainLayerIndex;
}

TCursorSprite TScene::cursor() const
{
    return mCursor;
}

void TScene::setCursor(const TCursorSprite &cursor)
{
    mCursor = cursor;
}

TColor TScene::backgroundColor() const
{
    return mBackgroundColor;
}

void TScene::setBackgroundColor(const TColor &backgroundColor)
{
    mBackgroundColor = backgroundColor;
}

const TSoundSet &TScene::soundSet() const
{
    return mSoundSet;
}

void TScene::setSoundSet(const TSoundSet &soundSet)
{
    mSoundSet = soundSet;
}

TCamera TScene::camera() const
{
    return mCamera;
}

void TScene::setCamera(const TCamera &camera)
{
    mCamera = camera;
}

int TScene::fps() const
{
    return mFps;
}

void TScene::setFps(int fps)
{
    mFps = fps;
}

TVector2 TScene::gravity() const
{
    return mGravity;
}

void TScene::setGravity(const TVector2 &gravity)
{
    mGravity = gravity;
}

void TScene::setLayerList(const TLayerList &layerList)
{
    mLayerList = layerList;
}
