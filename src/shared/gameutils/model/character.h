#ifndef TMODELCHARACTER_H
#define TMODELCHARACTER_H

#include <string>
#include <vector>

#include "character/action.h"
#include "../base/utils.h"
#include "../base/model.h"

namespace Model {

class TCharacter : public TModel
{
public:
    TCharacter();
    TCharacter(nlohmann::json j, void *context = nullptr);
    ~TCharacter();

    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    nlohmann::json toJson() const override;

    int getAge() const;
    void setAge(int age);

    float getScale() const;
    void setScale(float scale);

    int getHeight() const;
    void setHeight(int height);

    Model::TActionList getActionList() const;
    void setActionList(const Model::TActionList &actionList);

    int getWeight() const;
    void setWeight(int weight);

    int getForce() const;
    void setForce(int force);

    int getDefend() const;
    void setDefend(int defend);

    std::string getAiCode() const;
    void setAiCode(const std::string &aiCode);

    CharacterGender getGender() const;
    void setGender(const CharacterGender &gender);

    SpriteType getSpriteType() const;
    void setSpriteType(const SpriteType &spriteType);

    std::string getIconPath() const;
    void setIconPath(const std::string &iconPath);

    ModelType getModelType() const;
    void setModelType(const ModelType &modelType);

private:
    int mAge;
    int mWeight;
    int mForce;
    int mDefend;
    float mScale;
    int mHeight;
    std::string mAiCode;
    CharacterGender mGender;
    std::string mIconPath;

    TActionList mActionList;
};

typedef std::vector<TCharacter*> TCharacterList;

}
#endif // TMODELCHARACTER_H
