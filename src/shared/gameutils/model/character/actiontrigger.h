#ifndef TMODELACTIONTRIGGER_H
#define TMODELACTIONTRIGGER_H

#include <vector>

#include "../../base/jsoninterface.h"
#include "../../base/variable.h"

namespace Model {

class TActionTrigger : public TJsonReader, TJsonWriter
{
public:
    TActionTrigger();
    TActionTrigger(nlohmann::json j, void *context = nullptr);
    ~TActionTrigger();

    GameEvent event() const;
    void setEvent(const GameEvent &event);

    int context() const;
    void setContext(int context);

    std::string value() const;
    void setValue(const std::string &value);

private:
    GameEvent mEvent;
    int mContext;
    std::string mValue;

    // TJsonWriter interface
public:
    nlohmann::json toJson() const override;
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
};

typedef std::vector<TActionTrigger*> TActionTriggerList;
}
#endif // TMODELACTIONTRIGGER_H
