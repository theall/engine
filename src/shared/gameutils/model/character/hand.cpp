#include "hand.h"

static const char *K_HAND_DIRECTION = "direction";
static const char *K_HAND_ANGLE = "angle";
static const char *K_HAND_RECT = "rect";

using namespace Model;

THand::THand()
{

}

THand::THand(nlohmann::json j, void *context)
{
    loadFromJson(j, context);
}

THand::~THand()
{

}

void THand::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;

    mPalmDirection = (HandDirection)j[K_HAND_DIRECTION].get<int>();
    mFlatAngle = j[K_HAND_ANGLE].get<int>();
    mRect = TRect::fromJson(j[K_HAND_RECT]);
}

HandDirection THand::direction() const
{
    return mPalmDirection;
}

void THand::setDirection(const HandDirection &direction)
{
    mPalmDirection = direction;
}

float THand::angle() const
{
    return mFlatAngle;
}

void THand::setAngle(float angle)
{
    mFlatAngle = angle;
}

TRect THand::rect() const
{
    return mRect;
}

void THand::setRect(const TRect &rect)
{
    mRect = rect;
}

THand *THand::mirror(float x) const
{
    THand *hand = new THand;
    hand->setDirection(mPalmDirection);
    hand->setAngle(mFlatAngle);
    hand->setRect(mRect.mirror(x));

    return hand;
}

nlohmann::json THand::toJson() const
{
    nlohmann::json j = {};
    j.emplace(K_HAND_DIRECTION, mPalmDirection);
    j.emplace(K_HAND_ANGLE, mFlatAngle);
    j.emplace(K_HAND_RECT, mRect.toJson());
    return j;
}

THandList Model::mirror(const THandList &handList, float x)
{
    THandList newHandList;
    for(THand *hand : handList)
    {
        if(!hand)
            continue;

        newHandList.emplace_back(hand->mirror(x));
    }

    return newHandList;
}

nlohmann::json Model::toJson(const THandList &handList)
{
    nlohmann::json j = nlohmann::json::array();
    for(THand *hand : handList)
    {
        j.push_back(hand->toJson());
    }
    return j;
}
