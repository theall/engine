#ifndef TMODELHAND_H
#define TMODELHAND_H

#include <vector>

#include "../../base/rect.h"

enum HandDirection
{
    HD_X,
    HD_Y,
    HD_Z,
    HD_N_X,
    HD_N_Y,
    HD_N_Z,
    HD_COUNT
};

namespace Model {

class THand : public TJsonReader, TJsonWriter
{
public:
    THand();
    THand(nlohmann::json j, void *context = nullptr);
    ~THand();

    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

    HandDirection direction() const;
    void setDirection(const HandDirection &direction);

    float angle() const;
    void setAngle(float angle);

    TRect rect() const;
    void setRect(const TRect &rect);

    THand *mirror(float x = 0.0) const;

private:
    HandDirection mPalmDirection;
    float mFlatAngle;
    TRect mRect;

    // TJsonWriter interface
public:
    nlohmann::json toJson() const override;
};

typedef std::vector<THand*> THandList;

THandList mirror(const THandList &handList, float x = 0.0);
nlohmann::json toJson(const THandList &handList);

}

#endif // TMODELHAND_H
