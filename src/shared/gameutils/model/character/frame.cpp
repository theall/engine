#include "frame.h"

#include "../../base/variable.h"

// Frame key
static const char *K_IMAGE = "image";
static const char *K_COLLIDE_AREA = "collide_area";
static const char *K_UNDERTAKE_AREA = "undertake_area";
static const char *K_TERRIAN_AREA = "terrian_area";
static const char *K_ATTACK_AREA = "attack_area";
static const char *K_VECTOR_TABLE = "vector_table";
static const char *K_HAND = "hand";
static const char *K_FOOT = "foot";
static const char *K_VECTOR = "vector";
static const char *K_ANCHOR = "anchor";
static const char *K_ANTI_GRAVITY = "anti_gravity";
static const char *K_DURATION = "duration";
static const char *K_INTERRUPT_TYPE = "interrupt type";
static const char *K_SOUND = "sound";

using namespace Model;

TFrame::TFrame() :
    mAntiGravity(true)
  , mDuration(1)
  , mSoundSet(new TSoundSet)
  , mInterruptType(IT_DISABLE)
{

}

TFrame::TFrame(nlohmann::json jsonObject, void *context) :
    mAntiGravity(true)
  , mDuration(1)
  , mSoundSet(new TSoundSet)
  , mInterruptType(IT_DISABLE)
{
    loadFromJson(jsonObject, context);
}

TFrame::~TFrame()
{
    FREE_CONTAINER(mHandList);
    FREE_CONTAINER(mVectorTable);

    if(mSoundSet)
    {
        delete mSoundSet;
        mSoundSet = nullptr;
    }
}

void TFrame::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;

    mImagePath = j[K_IMAGE].get<std::string>();
    if(mImagePath.empty())
        throw std::string("Null image path.");

    // Collide rect
    mCollideRectList.clear();
    if(j.contain(K_COLLIDE_AREA))
    {
        for(nlohmann::json r : j[K_COLLIDE_AREA])
            mCollideRectList.emplace_back(TRect::fromJson(r));
    }

    // Attack rect
    mAttackRectList.clear();
    if(j.contain(K_ATTACK_AREA))
    {
        for(nlohmann::json r : j[K_ATTACK_AREA])
            mAttackRectList.emplace_back(TRect::fromJson(r));
    }

    // Undertake rect
    mUndertakeRectList.clear();
    if(j.contain(K_UNDERTAKE_AREA))
    {
        for(nlohmann::json r : j[K_UNDERTAKE_AREA])
            mUndertakeRectList.emplace_back(TRect::fromJson(r));
    }

    // Terrian rect
    mTerrianRectList.clear();
    if(j.contain(K_TERRIAN_AREA))
    {
        for(nlohmann::json r : j[K_TERRIAN_AREA])
            mTerrianRectList.emplace_back(TRect::fromJson(r));
    }

    // Vector table
    FREE_CONTAINER(mVectorTable);
    if(j.contain(K_VECTOR_TABLE))
    {
        for(nlohmann::json v : j[K_VECTOR_TABLE])
            mVectorTable.emplace_back(new TVectorItem(v));
    }

    // Hand rect
    mHandList.clear();
    if(j.contain(K_HAND))
    {
        for(nlohmann::json r : j[K_HAND])
            mHandList.emplace_back(new THand(r, this));
    }

    // Sound
    if(j.contain(K_SOUND))
        mSoundSet->loadFromJson(j[K_SOUND], this);
    if(j.contain(K_FOOT))
        mFootRect = TRect::fromJson(j[K_FOOT]);
    if(j.contain(K_VECTOR))
        mVector = TVector2::fromJson(j[K_VECTOR]);
    if(j.contain(K_ANCHOR))
        mAnchor = TVector2::fromJson(j[K_ANCHOR]);

    mAntiGravity = j.value(K_ANTI_GRAVITY, false);
    mDuration = j.value(K_DURATION, 1);
    mInterruptType = j.value(K_INTERRUPT_TYPE, IT_DISABLE);
    if(mDuration <= 0) {
        mDuration = 1;
        // throw format("Duration must not be negetive, current is %d", mDuration);
    }
}

nlohmann::json TFrame::toJson() const
{
    nlohmann::json j = {};
    j.emplace(K_IMAGE, mImagePath);
    j.emplace(K_DURATION, mDuration);
    j.emplace(K_VECTOR, mVector.toJson());
    j.emplace(K_ANCHOR, mAnchor.toJson());
    j.emplace(K_FOOT, mFootRect.toJson());
    j.emplace(K_ANTI_GRAVITY, mAntiGravity);
    if(mSoundSet)
        j.emplace(K_SOUND, mSoundSet->toJson());
    j.emplace(K_COLLIDE_AREA, Model::toJson(mCollideRectList));
    j.emplace(K_ATTACK_AREA, Model::toJson(mAttackRectList));
    j.emplace(K_UNDERTAKE_AREA, Model::toJson(mUndertakeRectList));
    j.emplace(K_TERRIAN_AREA, Model::toJson(mTerrianRectList));
    j.emplace(K_HAND, Model::toJson(mHandList));
    j.emplace(K_VECTOR_TABLE, Model::toJson(mVectorTable));
    j.emplace(K_INTERRUPT_TYPE, mInterruptType);
    return j;
}

TRectList TFrame::getUndertakeRectList() const
{
    return mUndertakeRectList;
}

void TFrame::setUndertakeRectList(const TRectList &undertakeRectList)
{
    mUndertakeRectList = undertakeRectList;
}

TRectList TFrame::getAttackRectList() const
{
    return mAttackRectList;
}

void TFrame::setAttackRectList(const TRectList &attackRectList)
{
    mAttackRectList = attackRectList;
}

TRectList TFrame::getCollideRectList() const
{
    return mCollideRectList;
}

void TFrame::setCollideRectList(const TRectList &collideRectList)
{
    mCollideRectList = collideRectList;
}

THandList TFrame::getHandList() const
{
    return mHandList;
}

void TFrame::setHandList(const THandList &handList)
{
    mHandList = handList;
}

TRectList TFrame::getTerrianRectList() const
{
    return mTerrianRectList;
}

void TFrame::setTerrianRectList(const TRectList &terrianRectList)
{
    mTerrianRectList = terrianRectList;
}

TRect TFrame::getFootRect() const
{
    return mFootRect;
}

void TFrame::setFootRect(const TRect &footRect)
{
    mFootRect = footRect;
}

std::string TFrame::getImagePath() const
{
    return mImagePath;
}

void TFrame::setImagePath(const std::string &imagePath)
{
    mImagePath = imagePath;
}

TVector2 TFrame::getVector() const
{
    return mVector;
}

void TFrame::setVector(const TVector2 &vector)
{
    mVector = vector;
}

bool TFrame::getAntiGravity() const
{
    return mAntiGravity;
}

void TFrame::setAntiGravity(bool antiGravity)
{
    mAntiGravity = antiGravity;
}

int TFrame::getDuration() const
{
    return mDuration;
}

void TFrame::setDuration(int duration)
{
    mDuration = duration;
}

TVector2 TFrame::getAnchor() const
{
    return mAnchor;
}

void TFrame::setAnchor(const TVector2 &anchor)
{
    mAnchor = anchor;
}

TSoundSet *TFrame::getSoundSet() const
{
    return mSoundSet;
}

void TFrame::setSoundSet(TSoundSet *soundSet)
{
    if(soundSet==mSoundSet)
        return;

    if(mSoundSet)
        delete mSoundSet;

    mSoundSet = soundSet;
}

InterruptType TFrame::getInterruptType() const
{
    return mInterruptType;
}

void TFrame::setInterruptType(const InterruptType &interruptType)
{
    mInterruptType = interruptType;
}

TVectorItemsList TFrame::getVectorItemsList() const
{
    return mVectorTable;
}

void TFrame::setVectorItemsList(const TVectorItemsList &vectorItemsList)
{
    mVectorTable = vectorItemsList;
}
