#include "action.h"
#include "../../base/utils.h"

#include <algorithm>

static const char *K_ACTION_NAME = "name";
static const char *K_ACTION_VECTOR = "vector";
static const char *K_ACTION_FRAMES = "frames";
static const char *K_ACTION_TRIGGERS = "triggers";

using namespace Model;

TAction::TAction()
{
}

TAction::TAction(nlohmann::json j, void *context)
{
    loadFromJson(j, context);
}

TAction::TAction(void *fileData, int size)
{
    (void)fileData;
    (void)size;
}

TAction::~TAction()
{
    FREE_CONTAINER(mFramesList);
    FREE_CONTAINER(mTriggerList);
}

void TAction::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    if(j.contain(K_ACTION_NAME))
        mName = j[K_ACTION_NAME];
    //mActionMode = StringToActionMode(j[K_ACTION_MODE].get<std::string>());
    if(j.contain(K_ACTION_VECTOR))
        mVector = TVector2::fromJson(j[K_ACTION_VECTOR]);

    FREE_CONTAINER(mFramesList);
    for (nlohmann::json ja : j[K_ACTION_FRAMES]) {
        mFramesList.emplace_back(new TFrame(ja));
    }

    // Trigger list
    FREE_CONTAINER(mTriggerList);
    for(nlohmann::json t : j[K_ACTION_TRIGGERS]) {
        mTriggerList.emplace_back(new TActionTrigger(t));
    }
}

nlohmann::json TAction::toJson() const
{
    nlohmann::json j = {};
    j.emplace(K_ACTION_NAME, mName);
    j.emplace(K_ACTION_VECTOR, mVector.toJson());

    nlohmann::json frameArray = nlohmann::json::array();
    for(TFrame *frame : mFramesList)
    {
        frameArray.push_back(frame->toJson());
    }
    j.emplace(K_ACTION_FRAMES, frameArray);

    nlohmann::json triggerArray = nlohmann::json::array();
    for(TActionTrigger *trigger : mTriggerList)
    {
        triggerArray.push_back(trigger->toJson());
    }
    j.emplace(K_ACTION_TRIGGERS, triggerArray);

    return j;
}

std::string TAction::getName() const
{
    return mName;
}

void TAction::setName(const std::string &name)
{
    mName = name;
}

TVector2 TAction::getVector() const
{
    return mVector;
}

void TAction::setVector(const TVector2 &vector)
{
    mVector = vector;
}

ActionMode TAction::getActionMode() const
{
    return mActionMode;
}

void TAction::setActionMode(const ActionMode &actionMode)
{
    mActionMode = actionMode;
}

TFramesList TAction::getFramesList() const
{
    return mFramesList;
}

TActionTriggerList TAction::getTriggerList() const
{
    return mTriggerList;
}

void TAction::setTriggerList(const TActionTriggerList &triggerList)
{
    FREE_CONTAINER(mTriggerList);

    mTriggerList = triggerList;
}

void TAction::setFramesList(const TFramesList &framesList)
{
    FREE_CONTAINER(mFramesList);

    mFramesList = framesList;
}
