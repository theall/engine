#ifndef TMODELFRAME_H
#define TMODELFRAME_H

#include <vector>

#include "../../base/vector.h"
#include "../../base/rect.h"
#include "../../base/datetime.h"
#include "../sound/soundset.h"
#include "hand.h"
#include "vectoritem.h"

namespace Model {

class TFrame : public TJsonReader, TJsonWriter
{
public:
    TFrame();
    TFrame(nlohmann::json jsonObject, void *context = nullptr);
    ~TFrame();

    TRectList getUndertakeRectList() const;
    void setUndertakeRectList(const TRectList &undertakeRectList);

    TRectList getAttackRectList() const;
    void setAttackRectList(const TRectList &attackRectList);

    TRectList getCollideRectList() const;
    void setCollideRectList(const TRectList &collideRectList);

    THandList getHandList() const;
    void setHandList(const THandList &handList);

    TRectList getTerrianRectList() const;
    void setTerrianRectList(const TRectList &terrianRectList);

    TRect getFootRect() const;
    void setFootRect(const TRect &footRect);

    std::string getImagePath() const;
    void setImagePath(const std::string &imagePath);

    TVector2 getVector() const;
    void setVector(const TVector2 &vector);

    bool getAntiGravity() const;
    void setAntiGravity(bool antiGravity);

    int getDuration() const;
    void setDuration(int duration);

    TVector2 getAnchor() const;
    void setAnchor(const TVector2 &anchor);

    // TJsonWriter interface
    nlohmann::json toJson() const override;
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

    TSoundSet *getSoundSet() const;
    void setSoundSet(TSoundSet *soundSet);

    InterruptType getInterruptType() const;
    void setInterruptType(const InterruptType &interruptType);

    TVectorItemsList getVectorItemsList() const;
    void setVectorItemsList(const TVectorItemsList &vectorItemsList);

private:
    std::string mImagePath;
    TRectList mUndertakeRectList;
    TRectList mAttackRectList;
    TRectList mCollideRectList;
    TRectList mTerrianRectList;
    THandList mHandList;
    TRect mFootRect;
    TVector2 mVector;
    TVectorItemsList mVectorTable;
    bool mAntiGravity;
    int mDuration;
    TVector2 mAnchor;
    TSoundSet *mSoundSet;
    InterruptType mInterruptType;
};

typedef std::vector<TFrame*> TFramesList;
}

#endif // TMODELFRAME_H
