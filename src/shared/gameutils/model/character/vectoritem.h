#ifndef VECTORITEMMODEL_H
#define VECTORITEMMODEL_H

#include "../../base/vector.h"
#include "../../base/variable.h"

namespace Model {

class TVectorItem : public TJsonReader, TJsonWriter
{
public:
    TVectorItem();
    TVectorItem(nlohmann::json jsonObject, void *context = nullptr);
    ~TVectorItem();

    // TJsonWriter interface
    nlohmann::json toJson() const override;
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

    VectorType vectorType() const;
    void setVectorType(const VectorType &vectorType);

    TVector2 vector() const;
    void setVector(const TVector2 &vector);

private:
    VectorType mVectorType;
    TVector2 mVector;
};

typedef std::vector<TVectorItem*> TVectorItemsList;
nlohmann::json toJson(const TVectorItemsList &vectorItemsList);
}

#endif // VECTORITEMMODEL_H
