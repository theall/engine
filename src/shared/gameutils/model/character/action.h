#ifndef TMODELACTION_H
#define TMODELACTION_H

#include <vector>

#include "frame.h"
#include "actiontrigger.h"

#include "../../base/utils.h"
#include "../../base/variable.h"

namespace Model {

class TAction : public TJsonReader, TJsonWriter
{
public:
    TAction();
    TAction(nlohmann::json j, void *context = nullptr);
    TAction(void *fileData, int size);
    ~TAction();

    std::string getName() const;
    void setName(const std::string &name);

    TVector2 getVector() const;
    void setVector(const TVector2 &vector);

    ActionMode getActionMode() const;
    void setActionMode(const ActionMode &actionMode);

    TFramesList getFramesList() const;
    void setFramesList(const TFramesList &framesList);

    TActionTriggerList getTriggerList() const;
    void setTriggerList(const TActionTriggerList &triggerList);

private:
    std::string mName;
    ActionMode mActionMode;
    TActionTriggerList mTriggerList;
    TFramesList mFramesList;
    TVector2 mVector;

    // TJsonWriter interface
public:
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    nlohmann::json toJson() const override;
};

typedef std::vector<TAction*> TActionList;

}
#endif // TMODELACTION_H
