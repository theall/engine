#include "actiontrigger.h"

static const char *K_ACTION_TRIGGER_EVENT = "event";
static const char *K_ACTION_TRIGGER_VALUE = "value";

using namespace Model;

TActionTrigger::TActionTrigger() :
    mEvent(GE_NONE)
{

}

TActionTrigger::TActionTrigger(nlohmann::json j, void *context) :
    mEvent(GE_NONE)
{
    loadFromJson(j, context);
}

TActionTrigger::~TActionTrigger()
{

}

void TActionTrigger::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;

    mEvent = StringToGameEvent(j[K_ACTION_TRIGGER_EVENT].get<std::string>());
    mValue = j[K_ACTION_TRIGGER_VALUE].get<std::string>();
    //mContext = j["context"].get<int>();
}

GameEvent TActionTrigger::event() const
{
    return mEvent;
}

void TActionTrigger::setEvent(const GameEvent &event)
{
    mEvent = event;
}

int TActionTrigger::context() const
{
    return mContext;
}

void TActionTrigger::setContext(int context)
{
    mContext = context;
}

std::string TActionTrigger::value() const
{
    return mValue;
}

void TActionTrigger::setValue(const std::string &value)
{
    mValue = value;
}

nlohmann::json TActionTrigger::toJson() const
{
    nlohmann::json j = {};
    j.emplace(K_ACTION_TRIGGER_EVENT, GameEventToString(mEvent));
    j.emplace(K_ACTION_TRIGGER_VALUE, mValue);

    return j;
}
