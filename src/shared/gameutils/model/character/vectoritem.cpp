#include "vectoritem.h"

static const char *K_VECTOR = "vector";
static const char *K_TYPE = "type";

using namespace Model;

TVectorItem::TVectorItem() :
    mVectorType(VT_RELATIVE)
{

}

TVectorItem::TVectorItem(nlohmann::json jsonObject, void *context) :
    mVectorType(VT_RELATIVE)
{
    loadFromJson(jsonObject, context);
}

TVectorItem::~TVectorItem()
{

}

nlohmann::json TVectorItem::toJson() const
{
    nlohmann::json j;
    j.emplace(K_TYPE, VectorTypeToString(mVectorType));
    j.emplace(K_VECTOR, mVector.toJson());
    return j;
}

void TVectorItem::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    mVectorType = StringToVectorType(j[K_TYPE]);
    mVector = TVector2::fromJson(j[K_VECTOR]);
}

VectorType TVectorItem::vectorType() const
{
    return mVectorType;
}

void TVectorItem::setVectorType(const VectorType &vectorType)
{
    mVectorType = vectorType;
}

TVector2 TVectorItem::vector() const
{
    return mVector;
}

void TVectorItem::setVector(const TVector2 &vector)
{
    mVector = vector;
}

nlohmann::json Model::toJson(const TVectorItemsList &vectorItemsList)
{
    nlohmann::json j = nlohmann::json::array();
    for(TVectorItem *vectorItem : vectorItemsList)
    {
        j.push_back(vectorItem->toJson());
    }
    return j;
}
