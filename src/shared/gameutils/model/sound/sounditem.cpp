#include "sounditem.h"

static const char *K_FILE = "file";
static const char *K_DELAY = "delay";
static const char *K_LOOP_COUNT = "loop_count";

using namespace Model;

TSoundItem::TSoundItem() :
    mLoopCount(0)
  , mDelay(0)
{

}

TSoundItem::TSoundItem(const TSoundItem &soundItem)
{
    mLoopCount = soundItem.loopCount();
    mDelay = soundItem.delay();
    mFileName = soundItem.fileName();
}

TSoundItem::TSoundItem(nlohmann::json j, void *context) :
    mLoopCount(0)
  , mDelay(0)
{
    loadFromJson(j, context);
}

TSoundItem::~TSoundItem()
{

}

std::string TSoundItem::fileName() const
{
    return mFileName;
}

void TSoundItem::setFileName(const std::string &fileName)
{
    mFileName = fileName;
}

int TSoundItem::delay() const
{
    return mDelay;
}

void TSoundItem::setDelay(int delay)
{
    mDelay = delay;
}

int TSoundItem::loopCount() const
{
    return mLoopCount;
}

void TSoundItem::setLoopCount(int loopCount)
{
    mLoopCount = loopCount;
}

nlohmann::json TSoundItem::toJson() const
{
    nlohmann::json j;
    j.emplace(K_FILE, mFileName);
    j.emplace(K_LOOP_COUNT, mLoopCount);
    j.emplace(K_DELAY, mDelay);
    return j;
}

void TSoundItem::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    mFileName = j.value(K_FILE, "");
    mLoopCount = j.value(K_LOOP_COUNT, 0);
    mDelay = j.value(K_DELAY, 0);
}

nlohmann::json Model::toJson(const TSoundItemList &soundItemList)
{
    nlohmann::json j = nlohmann::json::array();
    for(TSoundItem *soundItem : soundItemList)
        j.emplace_back(soundItem->toJson());

    return j;
}
