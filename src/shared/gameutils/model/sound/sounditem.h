#ifndef MODEL_SOUNDITEM_H
#define MODEL_SOUNDITEM_H

#include "../../base/jsoninterface.h"

namespace Model {

class TSoundItem : public TJsonReader, TJsonWriter
{
public:
    TSoundItem();
    TSoundItem(const TSoundItem &soundItem);
    TSoundItem(nlohmann::json j, void *context = nullptr);
    ~TSoundItem();

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

    std::string fileName() const;
    void setFileName(const std::string &fileName);

    int loopCount() const;
    void setLoopCount(int loopCount);
    int delay() const;
    void setDelay(int delay);

private:
    std::string mFileName;
    int mLoopCount;
    int mDelay;
};

typedef std::vector<TSoundItem*> TSoundItemList;
nlohmann::json toJson(const TSoundItemList &soundItemList);

}

#endif // MODEL_SOUNDITEM_H
