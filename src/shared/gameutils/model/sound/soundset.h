#ifndef SOUNDSET_H
#define SOUNDSET_H

#include "sounditem.h"
#include <gameutils/base/variable.h>

namespace Model {

class TSoundSet : public TJsonReader, TJsonWriter
{
public:
    TSoundSet();
    ~TSoundSet();

    // TJsonWriter interface
    nlohmann::json toJson() const override;

    // TJsonReader interface
    void loadFromJson(nlohmann::json j, void *context = nullptr) override;

    TSoundItemList soundItemList() const;
    void setSoundItemList(const TSoundItemList &soundItemList);

    SoundSetModel soundSetModel() const;
    void setSoundSetModel(const SoundSetModel &soundSetModel);

    bool autoPlay() const;
    void setAutoPlay(bool autoPlay);

private:
    SoundSetModel mSoundSetModel;
    bool mAutoPlay;
    TSoundItemList mSoundItemList;
};

}
#endif // SOUNDSET_H
