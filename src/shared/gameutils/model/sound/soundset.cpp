#include "soundset.h"

static const char *K_MODEL = "model";
static const char *K_AUTO_PLAY = "auto_play";
static const char *K_ITEMS = "items";

using namespace Model;

TSoundSet::TSoundSet() :
    mSoundSetModel(SSM_ORDERED)
  , mAutoPlay(false)
{

}

TSoundSet::~TSoundSet()
{
    FREE_CONTAINER(mSoundItemList);
}

nlohmann::json TSoundSet::toJson() const
{
    nlohmann::json j;
    j.emplace(K_MODEL, SoundSetModelToString(mSoundSetModel));
    j.emplace(K_ITEMS, Model::toJson(mSoundItemList));
    j.emplace(K_AUTO_PLAY, mAutoPlay);
    return j;
}

void TSoundSet::loadFromJson(nlohmann::json j, void *context)
{
    mSoundSetModel = StringToSoundSetModel(j.value(K_MODEL, "Ordered"));
    mAutoPlay = j.value(K_AUTO_PLAY, false);
    FREE_CONTAINER(mSoundItemList);
    for(nlohmann::json ja : j[K_ITEMS])
        mSoundItemList.emplace_back(new TSoundItem(ja, context));
}

TSoundItemList TSoundSet::soundItemList() const
{
    return mSoundItemList;
}

void TSoundSet::setSoundItemList(const TSoundItemList &soundItemList)
{
    mSoundItemList = soundItemList;
}

SoundSetModel TSoundSet::soundSetModel() const
{
    return mSoundSetModel;
}

void TSoundSet::setSoundSetModel(const SoundSetModel &soundSetModel)
{
    mSoundSetModel = soundSetModel;
}

bool TSoundSet::autoPlay() const
{
    return mAutoPlay;
}

void TSoundSet::setAutoPlay(bool autoPlay)
{
    mAutoPlay = autoPlay;
}
