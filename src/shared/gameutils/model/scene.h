#ifndef SCENE_H
#define SCENE_H

#include "../base/model.h"
#include "../base/color.h"
#include "scene/layer.h"
#include "scene/layer/cursorsprite.h"
#include "sound/soundset.h"
#include "scene/camera.h"

namespace Model {

class TScene : public TModel
{
public:
    TScene();
    TScene(nlohmann::json j, void *context = nullptr);
    TScene(void *fileData, int size);
    ~TScene();

    void loadFromJson(nlohmann::json j, void *context = nullptr) override;
    nlohmann::json toJson() const override;

    TLayerList layerList() const;
    void setLayerList(const TLayerList &layerList);

    int mainLayerIndex() const;
    void setMainLayerIndex(int mainLayerIndex);

    TCursorSprite cursor() const;
    void setCursor(const TCursorSprite &cursor);

    TColor backgroundColor() const;
    void setBackgroundColor(const TColor &backgroundColor);

    const TSoundSet &soundSet() const;
    void setSoundSet(const TSoundSet &soundSet);

    TCamera camera() const;
    void setCamera(const TCamera &camera);

    int fps() const;
    void setFps(int fps);

    TVector2 gravity() const;
    void setGravity(const TVector2 &gravity);

private:
    int mFps;
    int mMainLayerIndex;
    TCursorSprite mCursor;
    TColor mBackgroundColor;
    TSoundSet mSoundSet;
    TCamera mCamera;
    TLayerList mLayerList;
    TVector2 mGravity;
};

typedef std::vector<TScene*> TSceneList;
}

#endif // SCENE_H
