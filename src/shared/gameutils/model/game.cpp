#include "game.h"

static const char *K_TYPE = "type";
static const char *K_ENTRY = "entry";
static const char *K_SCENES = "scenes";
static const char *K_FONTS = "fonts";
static const char *K_CHARACTERS = "characters";
static const char *K_RESOLUTION = "resolution";

using namespace Model;

#define FILL_STRING_LIST(member,key) \
member.clear();\
for (nlohmann::json ja : j[key]) {\
    member.emplace_back(ja.get<std::string>());\
}

TGame::TGame() :
    TModel(MT_GAME)
  , mEntrySceneIndex(-1)
{

}

TGame::TGame(nlohmann::json j, void *context) :
    TModel(MT_GAME)
  , mEntrySceneIndex(-1)
{
    loadFromJson(j, context);
}

TGame::TGame(void *fileData, int size) :
    TModel(MT_GAME)
  , mEntrySceneIndex(-1)
{
    (void)fileData;
    (void)size;
}

TGame::~TGame()
{

}

int TGame::entrySceneIndex() const
{
    return mEntrySceneIndex;
}

void TGame::setEntrySceneIndex(int entrySceneIndex)
{
    mEntrySceneIndex = entrySceneIndex;
}

std::vector<std::string> TGame::characterPathList() const
{
    return mCharacterPathList;
}

void TGame::setCharacterPathList(const std::vector<std::string> &characterPathList)
{
    mCharacterPathList = characterPathList;
}

TVector2 TGame::resolution() const
{
    return mResolution;
}

void TGame::setResolution(const TVector2 &resolution)
{
    mResolution = resolution;
}

std::vector<std::string> TGame::scenePathList() const
{
    return mScenePathList;
}

void TGame::setScenePathList(const std::vector<std::string> &scenePathList)
{
    mScenePathList = scenePathList;
}

std::vector<std::string> TGame::fontList() const
{
    return mFontList;
}

void TGame::setFontList(const std::vector<std::string> &fontList)
{
    mFontList = fontList;
}

void TGame::loadFromJson(nlohmann::json j, void *context)
{
    (void)context;
    
    mModelType = StringToModelType(j[K_TYPE].get<std::string>());
    mEntrySceneIndex = j[K_ENTRY];
    mResolution = TVector2::fromJson(j[K_RESOLUTION]);
    mScenePathList.clear();
    for (nlohmann::json ja : j[K_SCENES]) {
        mScenePathList.emplace_back(ja.get<std::string>());
    }
    FILL_STRING_LIST(mScenePathList, K_SCENES);
    FILL_STRING_LIST(mFontList, K_FONTS);
    FILL_STRING_LIST(mCharacterPathList, K_CHARACTERS);
}

nlohmann::json TGame::toJson() const
{
    nlohmann::json j = {};
    j.emplace(K_TYPE, ModelTypeToString(mModelType));
    j.emplace(K_ENTRY, mEntrySceneIndex);
    j.emplace(K_RESOLUTION, mResolution.toJson());

    nlohmann::json scenePathList = nlohmann::json::array();
    for(std::string s : mScenePathList)
        scenePathList.emplace_back(s);
    j.emplace(K_SCENES, scenePathList);

    nlohmann::json fontList = nlohmann::json::array();
    for(std::string s : mFontList)
        fontList.emplace_back(s);
    j.emplace(K_FONTS, fontList);

    return j;
}
