/**
 * Copyright (c) 2015-2017 Bilge Theall
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/
#include "app/app.h"

#undef main

TApp *g_app;

int main(int argc, char **argv)
{
#ifdef THEALL_IOS
    // on iOS we should never programmatically exit the app, so we'll just
    // "restart" when that is attempted. Games which use threads might cause
    // some issues if(the threads aren't cleaned up properly+.
    while (true)
#endif
    TApp app(argc, argv);
    if(app.needQuit())
        return 0;

    g_app = &app;

#ifdef THEALL_ANDROID
    SDL_Quit();
#endif

    return app.run();
}
