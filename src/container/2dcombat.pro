TEMPLATE = app
CONFIG += c++11 object_parallel_to_source
CONFIG -= app_bundle
#CONFIG -= qt

SOURCES += main.cpp

include($$PWD/sdk/sdk.pri)
include($$PWD/app/app.pri)
include($$PWD/../shared/gameutils/gameutils.pri)

CONFIG(debug, debug|release){
    DEFINES += DEBUG
} else {
    DEFINES += RELEASE
}
