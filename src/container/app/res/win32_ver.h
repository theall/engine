#ifndef WIN32_VER_H
#define WIN32_VER_H

//#define BUILD_COUNT 0
#define BUILD_NUMBER "0"

#define K_COMPANYNAME      "CompanyName"
#define K_FILEDESCRIPTION  "FileDescription"
#define K_FILEVERSION      "FileVersion"
#define K_INTERNALNAME     "InternalName"
#define K_LEGALCOPYRIGHT   "LegalCopyright"
#define K_ORIGINALFILENAME "OriginalFilename"
#define K_PRODUCTNAME      "ProductName"
#define K_PRODUCTVERSION   "ProductVersion"
#define K_COMPILEPLATFORM  "CompilePlatform"
#define K_BUILDNUMBER      "BuildNumber"
#define K_DOMAIN           "Domain"
#define K_BUILD_TIME       "BuildTime"

#define PRODUCT_ICON           "2dcombat.ico"
#define FILE_VERSION           0,1,0,0
#define FILE_VERSION_STR       "0.1.0.0"
#define PRODUCT_VERSION        0,1,0,0
#define PRODUCT_VERSION_STR    "0.1.0.0"
#define COMPANY_NAME           "Bilge Theall"
#define DOMAIN_NAME            "Theall"
#define INTERNAL_NAME          "2dcombat.exe"
#define FILE_DESCRIPTION       "2DCombat Beta Edition."
#define LEGAL_COPYRIGHT        "Copyright (C) 2016-2017 Bilge Theall, All rights reserved."
#define ORIGINAL_FILE_NAME     "2dcombat.exe"
#define PRODUCT_NAME           "2dcombat"
#define ORGANIZATION_DOMAIN    "http://www.2dcombat.cn/"
#define COMPILE_PLATFORM       "MingW 2013, 32 bit"
#define BUILD_TIME             "2017-01-19 19:08:18"

#endif
