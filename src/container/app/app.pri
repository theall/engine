SOURCES += \
    $$PWD/app.cpp \
    $$PWD/controller/controller.cpp \
    $$PWD/controller/keyboardcontroller.cpp \
    $$PWD/game/character/frame.cpp \
    $$PWD/game/character/action.cpp \
    $$PWD/game/character/actiontrigger.cpp \
    $$PWD/game/character/character.cpp \
    $$PWD/game/scene/scene.cpp \
    $$PWD/game/scene/layer.cpp \
    $$PWD/game/game.cpp \
    $$PWD/game/scenemanager.cpp \
    $$PWD/game/scenestack.cpp \
    $$PWD/game/scene/layer/charactersprite.cpp \
    $$PWD/game/scene/layer/sprite.cpp \
    $$PWD/game/scene/layer/imagesprite.cpp \
    $$PWD/game/resourcecache.cpp \
    $$PWD/game/scene/layer/buttonsprite.cpp \
    $$PWD/game/scene/layer/textsprite.cpp \
    $$PWD/game/scene/layer/blinktextsprite.cpp \
    $$PWD/game/scene/queryscene.cpp \
    $$PWD/game/scene/messagescene.cpp \
    $$PWD/game/scene/layer/cursorsprite.cpp \
    $$PWD/game/gameevent.cpp \
    $$PWD/loader.cpp \
    $$PWD/game/sound/sounditem.cpp \
    $$PWD/game/sound/soundset.cpp \
    $$PWD/game/scene/camera.cpp \
    $$PWD/game/scene/camerablock.cpp \
    $$PWD/game/scene/layer/doorsprite.cpp \
    $$PWD/game/move/movemodel.cpp \
    $$PWD/game/move/speedmodel.cpp \
    $$PWD/game/move/movesegment.cpp \
    $$PWD/game/move/linesegment.cpp \
    $$PWD/game/move/randomsegment.cpp \
    $$PWD/game/base/area/area.cpp \
    $$PWD/game/base/area/areagroup.cpp \
    $$PWD/game/base/area/rectarea.cpp \
    $$PWD/game/scene/layer/ghostsprite.cpp \
    $$PWD/game/scene/layer/terriansprite.cpp \
    $$PWD/cmdparser.cpp

HEADERS  += \
    $$PWD/app.h \
    $$PWD/controller/controller.h \
    $$PWD/controller/keyboardcontroller.h \
    $$PWD/game/character/frame.h \
    $$PWD/game/character/action.h \
    $$PWD/game/character/actiontrigger.h \
    $$PWD/game/character/character.h \
    $$PWD/game/scene/scene.h \
    $$PWD/game/scene/layer.h \
    $$PWD/game/game.h \
    $$PWD/game/scenemanager.h \
    $$PWD/game/scenestack.h \
    $$PWD/game/scene/layer/charactersprite.h \
    $$PWD/game/scene/layer/sprite.h \
    $$PWD/game/scene/layer/imagesprite.h \
    $$PWD/game/resourcecache.h \
    $$PWD/game/scene/layer/buttonsprite.h \
    $$PWD/game/scene/layer/textsprite.h \
    $$PWD/game/scene/layer/blinktextsprite.h \
    $$PWD/game/scene/queryscene.h \
    $$PWD/game/scene/messagescene.h \
    $$PWD/game/scene/layer/cursorsprite.h \
    $$PWD/game/gameevent.h \
    $$PWD/game/base/runnable.h \
    $$PWD/loader.h \
    $$PWD/game/sound/sounditem.h \
    $$PWD/game/sound/soundset.h \
    $$PWD/game/scene/camera.h \
    $$PWD/game/scene/camerablock.h \
    $$PWD/game/scene/layer/doorsprite.h \
    $$PWD/game/move/movemodel.h \
    $$PWD/game/move/speedmodel.h \
    $$PWD/game/move/movesegment.h \
    $$PWD/game/move/linesegment.h \
    $$PWD/game/move/randomsegment.h \
    $$PWD/game/base/area/area.h \
    $$PWD/game/base/area/areagroup.h \
    $$PWD/game/base/area/rectarea.h \
    $$PWD/game/scene/layer/ghostsprite.h \
    $$PWD/game/scene/layer/terriansprite.h \
    $$PWD/cmdparser.h

win32: {
    RC_FILE = $$PWD/res/2dcombat.rc
}

unix: {

}
macx: {
}
