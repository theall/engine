#include "loader.h"

#include <gameutils/model/game.h>
#include <gameutils/model/scene.h>
#include <gameutils/model/character.h>

#include "game/game.h"
#include "game/scene/layer/charactersprite.h"

static const char *K_MODEL_TYPE = "type";

extern TFileSystem *fs;
ModelType g_runningModelType;

TLoader::TLoader()
{

}

TLoader::~TLoader()
{

}

TRunnable *TLoader::load(const std::string &fileName)
{
    TFileData *fileData = fs->newFileData(fileName);
    if(!fileData)
        return nullptr;

    mFilePath = fileName;

    json jsonObject = {};
    try {
        char *data = (char*)fileData->getData();
        jsonObject = json::parse(data, data+fileData->getSize());
        fileData->release();
    } catch(detail::exception e) {
        fileData->release();
        throw std::string(e.what());
    } catch(...) {
        fileData->release();
        throw std::string("Unknow error while parsing json data.");
    }

    try {
        std::string modelTypeStr = jsonObject[K_MODEL_TYPE].get<std::string>();
        g_runningModelType = StringToModelType(modelTypeStr);
        if(g_runningModelType == MT_GAME)
        {
            Model::TGame gameModel;
            gameModel.loadFromJson(jsonObject);
            return new TGame(gameModel);
        } else if(g_runningModelType == MT_SCENE) {
            Model::TScene sceneModel;
            sceneModel.loadFromJson(jsonObject);
            return new TScene(sceneModel);
        } else if(g_runningModelType == MT_CHARACTER) {
            Model::TCharacter characterModel;
            characterModel.loadFromJson(jsonObject);
            TCharacter *character = new TCharacter;
            character->loadFromModel(characterModel);
            return new TCharacterSprite(character);
        } else {
            throw TException("Unknow file model \"%s\"\n\n\tFile: %s", modelTypeStr.c_str(), fileName.c_str());
        }
    } catch(std::string s) {
        throw TException("Failed to load %s \n\n\t%s", fileName.c_str(), s.c_str());
    } catch(detail::exception e) {
        throw std::string(e.what());
    }
    return nullptr;
}
