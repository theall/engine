#ifndef LOADER_H
#define LOADER_H

#include <string>
#include "game/base/runnable.h"

class TLoader
{
public:
    TLoader();
    ~TLoader();

    TRunnable *load(const std::string &fileName);

private:
    std::string mFilePath;
};

#endif // LOADER_H
