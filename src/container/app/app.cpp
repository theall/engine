#include "app.h"
#include "loader.h"
#include "cmdparser.h"
#include "game/game.h"
#include "controller/keyboardcontroller.h"
#include "game/scene/layer/charactersprite.h"
#include "game/resourcecache.h"

TRunnable *g_runnable;
TGraphics *gfx = TGraphics::instance();
TAudio *ao = TAudio::instance();
TFileSystem *fs = TFileSystem::instance();
TKeyboard *kbd = TKeyboard::instance();
TWindow *wnd = TWindow::instance();
TKeyboardController *keyboardController = TKeyboardController::instance();
TMouse *mouse = TMouse::instance();
TTimer *timer = TTimer::instance();

TApp::TApp(int argc, char *argv[]) :
    TEngine(argc, argv)
  , mNeedQuit(false)
  , mShowFps(false)
  , mShowFullScreen(false)
  , mTimeOutFrames(0)
  , mStatusMessage(nullptr)
{
    TCmdParser cmdParser(argc, argv);
    if(cmdParser.needExit())
    {
        mNeedQuit = true;
        return;
    }

    // Initialize rand function
    srand((unsigned)time(0));

    mWindowName = wnd->getWindowTitle();
    TWindowSize windowSize = wnd->getSize();
    mWindowSize = TVector2(windowSize.width, windowSize.height);
    mWindowCenter = mWindowSize / 2;
    kbd->setKeyRepeat(true);

    setSourceFile(cmdParser.fileName());

#ifdef DEBUG
    mShowFps = true;
#endif

    TResourceCache *resourceCache = TResourceCache::instance();
    resourceCache->setCharacterPath(cmdParser.spritePath());
    resourceCache->setFontPath(cmdParser.fontPath());
}

TApp::~TApp()
{
    if(mStatusMessage)
    {
        delete mStatusMessage;
        mStatusMessage = nullptr;
    }
    TResourceCache::deleteInstance();
}

void TApp::update()
{
    if(!g_runnable)
        return;

    keyboardController->step();
    g_runnable->step();
    if(mTimeOutFrames > 0)
        mTimeOutFrames--;
}

void TApp::draw()
{
    if(g_runnable)
        g_runnable->render();

    TFont *defaultFont = gfx->getDefaultFont();
    TFont *currentFont = gfx->getFont();

    if(mShowFps) {
        gfx->push();
        gfx->setTranslation(-20, 10);
        std::string fpsStr = format("FPS %d", timer->getFPS());
        defaultFont->print(fpsStr, mWindowSize.x-defaultFont->getWidth(fpsStr), 0, Color(0,255,0,255));
        gfx->pop();
    }

#ifdef DEBUG
    gfx->push();
    gfx->setTranslation(10, 10);
    TVector2 mousePos((float)mouse->getX(), (float)mouse->getY());
    currentFont->print(mousePos.toString("%.0f.%.0f"), 0, 0, Color(0,0,255,255));
    if(TCharacterSprite *character = dynamic_cast<TCharacterSprite*>(g_runnable))
    {
        TAction *action = character->getCurrentAction();
        if(action)
        {
            gfx->print(action->getName(), 0, 15);
            if(action->getCurrentFrame())
                gfx->print("anchor: "+action->getCurrentFrame()->mAnchor.toString(), 0, 45);
        }
        gfx->print(character->getPos().toString(), 0, 30);
        std::string s = keyboardController->getInputCommandString();
        if(!s.empty())
            defaultFont->print(s, 0, 60, Color(0,0,255,255));
    } else if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
        gfx->print("camera: "+scene->camera()->rect().toString(), 0, 10);
        gfx->print("view: "+scene->camera()->viewRect().toString(), 0, 20);
    }

    gfx->pop();
#endif
    if(mTimeOutFrames > 0) {
        gfx->push();
        gfx->setTranslation(10, mWindowSize.y-mStatusMessage->getHeight());
        mStatusMessage->draw(0, -5);
        gfx->pop();
    }
}

void TApp::load()
{
    if(!mFileName.empty())
    {
        TLoader loader;
        g_runnable = loader.load(mFileName);
        if(!g_runnable) {
            throw TException("Load failed, %s", mFileName.c_str());
            return;
        }

        if(TGame *game = dynamic_cast<TGame*>(g_runnable))
        {
            std::string gameName = game->name();
            fs->setIdentity("capture/" + gameName);
        } else if(TCharacterSprite *characterSprite = dynamic_cast<TCharacterSprite*>(g_runnable)) {
            characterSprite->moveTo(mWindowCenter);
            characterSprite->setController(keyboardController);
        } else if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
            scene->onPushed();
            setFps(scene->fps());
        }
    }

#ifdef DEBUG
    TSprite::setDebugFlag(true);
#endif
}

bool TApp::onMessage(TMessage *msg)
{
    EventType et = (EventType)msg->value;

    switch (et) {
    case EVENT_KEYPRESSED:
        {
            TVariant v = msg->arguments[0];
            int key = v.toInt();
            if(key == TKeyboard::KEY_F11)
            {
                TImageData *imageData = gfx->newScreenshot();
                std::string currentTimeStr = getCurrentTimeString("%d_%d_%d_%d_%d_%d") + ".png";
                TFileData *fileData = imageData->encode(TAbstractImageData::ENCODED_PNG, currentTimeStr.c_str());
                fs->write(fileData);
                fileData->release();
                setDisplayMessage("Save to "+fs->getSaveFileFullName(currentTimeStr));
            } else if(key == TKeyboard::KEY_F1) {
                setFps(60);
                setSkipFrames(0);
                gfx->origin();
                if(TCharacterSprite *c = dynamic_cast<TCharacterSprite*>(g_runnable))
                {
                    c->moveTo(mWindowCenter);
                } else if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
                    scene->camera()->setRect(TRect(0, 0, mWindowSize.x, mWindowSize.y));
                }
            } else if(key == TKeyboard::KEY_1 || key == TKeyboard::KEY_2) {
                float scaleFactor = key==TKeyboard::KEY_1?1.1:0.9;
                gfx->scale(scaleFactor, scaleFactor, mWindowCenter.x, mWindowCenter.y);
                if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
                    scene->camera()->updateViewRect();
                }
            } else if(key == TKeyboard::KEY_UP) {
                if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
                    scene->moveCamera(0, -20);
                }
            } else if(key == TKeyboard::KEY_DOWN) {
                if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
                    scene->moveCamera(0, 20);
                }
            } else if(key == TKeyboard::KEY_LEFT) {
                if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
                    scene->moveCamera(-20, 0);
                }
            } else if(key == TKeyboard::KEY_RIGHT) {
                if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
                    scene->moveCamera(20, 0);
                }
            }  else if(key == TKeyboard::KEY_F2) {
                if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
                    scene->camera()->clear();
                }
            } else if(key == TKeyboard::KEY_F3) {
                if(TScene *scene = dynamic_cast<TScene*>(g_runnable)) {
                    scene->setQuake(180, 5);
                }
            } else if(key == TKeyboard::KEY_MINUS) {
                int f = fps();
                if(f > 10) {
                    if(f > 1)
                        f -= 10;
                    else
                        f = 1;
                } else {
                    f = 1;
                }
                setFps(f);
            } else if(key == TKeyboard::KEY_EQUALS) {
                setFps(fps()+10);
            } else if(key == TKeyboard::KEY_LEFTBRACKET) {
                setSkipFrames(skipFrames()-1);
                setDisplayMessage(TException("Skip: %d", skipFrames()).what());
            } else if(key == TKeyboard::KEY_RIGHTBRACKET) {
                setSkipFrames(skipFrames()+1);
                setDisplayMessage(TException("Skip: %d", skipFrames()).what());
            } else if(key == TKeyboard::KEY_F5) {
                mShowFullScreen = !mShowFullScreen;
                wnd->setFullscreen(mShowFullScreen);
            }
        }
        break;
#ifdef DEBUG
    case EVENT_FILEDROPPED:
        {
            TVariant v = msg->arguments[0];
            TDroppedFile *droppedFile = (TDroppedFile*)v.getUserData();
            if(droppedFile)
            {
                std::string newFile = droppedFile->getFilename();
                setDisplayMessage("Load " + newFile);
                if(g_runnable)
                {
                    delete g_runnable;
                    g_runnable = nullptr;
                }
                setSourceFile(newFile);
                load();
            }
        }
        break;
#endif
    case EVENT_QUIT:
        //wnd->showMessage("Info", "Really wanto exit?");
        return false;
        break;
    default:
        break;
    }
    return true;
}

void TApp::setDisplayMessage(const std::string &msg, int timeOut)
{
    mTimeOutFrames = timeOut;

    if(!mStatusMessage)
        mStatusMessage = new TText;
    mStatusMessage->set(msg, Color(255,255,255,255));
}

void TApp::setShowFps(bool showFps)
{
    mShowFps = showFps;
}

bool TApp::needQuit() const
{
    return mNeedQuit;
}

void TApp::setSourceFile(const std::string &file)
{
    int pos = file.find_last_of('/');
    if(pos < 0)
        pos = file.find_last_of('\\');
    if(pos >= 0)
    {
        mSourcePath = file.substr(0, pos);
        mFileName = file.substr(pos+1, file.size()-pos);
        fs->setSource(mSourcePath.c_str());
        std::string title = mWindowName + " - " + file;
        wnd->setWindowTitle(title);
    }
}
