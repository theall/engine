#include "cmdparser.h"

#include <stdio.h>
#include <string.h>
#include <getopt.h>

extern char *optarg;
extern int opterr;

struct option g_options[] = {
    {"sprite-path",required_argument,NULL,'s'},
    {"font-path",optional_argument,NULL,'f'},
    {0,0,0,0}
};

TCmdParser::TCmdParser(int argc, char *argv[]) :
    mNeedExit(false)
{
    int c;
    opterr = 0;

    while((c=getopt_long(argc,argv,"s:f:",g_options,NULL)) != -1)
    {
        switch(c)
        {
            case 's':
                mSpritePath = optarg;
                break;
            case 'f':
                mFontPath = optarg;
                break;
            case '?':
                printf("2dcombat <options> file\n");
                printf("Usage : \n"
                "--sprite-path[-s] : set sprite path\n"
                "--font-path[-f] : set font path\n"
                );
                mNeedExit = true;
            default:
                break;
        }
    }

    if(argc > optind)
        mFileName = argv[optind];
}

TCmdParser::~TCmdParser()
{

}

std::string TCmdParser::spritePath() const
{
    return mSpritePath;
}

std::string TCmdParser::fontPath() const
{
    return mFontPath;
}

std::string TCmdParser::fileName() const
{
    return mFileName;
}

bool TCmdParser::needExit() const
{
    return mNeedExit;
}
