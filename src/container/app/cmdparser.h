#ifndef CMDPARSER_H
#define CMDPARSER_H

#include <string>

class TCmdParser
{
public:
    TCmdParser(int argc, char *argv[]);
    ~TCmdParser();

    std::string spritePath() const;
    std::string fontPath() const;
    std::string fileName() const;

    bool needExit() const;

private:
    bool mNeedExit;
    std::string mSpritePath;
    std::string mFontPath;
    std::string mFileName;
};

#endif // CMDPARSER_H
