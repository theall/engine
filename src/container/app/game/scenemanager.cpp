#include "scenemanager.h"

TSceneManager::TSceneManager() :
    mCurrentSceneIndex(-1)
  , mSceneStack(new TSceneStack)
{

}

TSceneManager::~TSceneManager()
{
    delete mSceneStack;
}

int TSceneManager::currentSceneIndex() const
{
    return mCurrentSceneIndex;
}

void TSceneManager::setCurrentSceneIndex(int currentSceneIndex)
{
    mCurrentSceneIndex = currentSceneIndex;
}

TScene *TSceneManager::getCurrentScene() const
{
    if(mCurrentSceneIndex>=0 && mCurrentSceneIndex<(int)mSceneList.size())
        return mSceneList.at(mCurrentSceneIndex);

    return nullptr;
}

TSceneStack *TSceneManager::getSceneStack() const
{
    return mSceneStack;
}
