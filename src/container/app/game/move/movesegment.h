#ifndef MOVESEGMENT_H
#define MOVESEGMENT_H

#include <vector>
#include "speedmodel.h"
#include <gameutils/base/vector.h>

class TMoveSegment
{
public:
    TMoveSegment();
    ~TMoveSegment();

    virtual TVector2 getSpeed() = 0;
    virtual bool isEnd() const = 0;
    virtual void reset() = 0;

protected:
};

typedef std::vector<TMoveSegment *> TMoveSegmentList;

#endif // MOVESEGMENT_H
