#ifndef MOVEMODEL_H
#define MOVEMODEL_H

#include "movesegment.h"

namespace Model {
class TMoveModel;
}

class TMoveModel
{
public:
    TMoveModel();
    ~TMoveModel();

    void loadFromModel(const Model::TMoveModel &moveModel, void *context);

    void reset();
    TVector2 getNextSpeed();
    bool isValid() const;

private:
    int mLoopCount;
    int mRewindCount;
    int mCurrentSegment;
    bool mIsValid;
    TMoveSegmentList mMoveSegmentList;
};

#endif // MOVEMODEL_H
