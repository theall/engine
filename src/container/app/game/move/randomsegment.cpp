#include "randomsegment.h"
#include <gameutils/base/utils.h>
#include <gameutils/model/move/randomsegment.h>

TRandomSegment::TRandomSegment() :
    TMoveSegment()
  , mState(None)
{

}

TRandomSegment::TRandomSegment(const Model::TRandomSegment &randomSegment, void *context) :
    TMoveSegment()
  , mState(None)
{
    loadFromModel(randomSegment, context);
}

void TRandomSegment::loadFromModel(const Model::TRandomSegment &randomSegment, void *context)
{
    (void)context;
    TRect randomRect = randomSegment.randomRect();
    mStart = TVector2(randomRect.x, randomRect.right());
    mEnd = TVector2(randomRect.y, randomRect.bottom());

    float returnVelocity = randomSegment.returnVelocity();
    if(returnVelocity != 0.0) {
        mReturnSegment.setSpeedModelParameter(0.0, 1.0, returnVelocity);
        mReturnPoint = randomSegment.returnPoint();
    }
    mState = None;
}

TRandomSegment::~TRandomSegment()
{

}

TVector2 TRandomSegment::getSpeed()
{
    TVector2 ret;
    if(mState == None)
    {
        ret.x = getRandValue(mStart.x, mStart.y);
        ret.y = getRandValue(mEnd.x, mEnd.y);
        mState = Randomed;
        mReturnSegment.setVector(mReturnPoint - ret);
    } else if(mState == Randomed) {
        ret =  mReturnSegment.getSpeed();
        if(mReturnSegment.isEnd())
            mState = Returned;
    }
    return ret;
}

bool TRandomSegment::isEnd() const
{
    return mState==Returned;
}

void TRandomSegment::setRect(const TRect &rect)
{
    mStart = rect.leftTop();
    mEnd = rect.rightBottom();
    mState = None;
}

void TRandomSegment::setRect(const TVector2 &start, const TVector2 &end)
{
    mStart = start;
    mEnd = end;
    mState = None;
}

void TRandomSegment::setPoint(const TVector2 &point)
{
    TRect rect(point, TVector2(1.0, 1.0));
    setRect(rect);
}

void TRandomSegment::reset()
{
    mState = None;
}
