#ifndef RANDOMVECTOR_H
#define RANDOMVECTOR_H

#include "movesegment.h"
#include "linesegment.h"
#include <gameutils/base/rect.h>

namespace Model {
class TRandomSegment;
}
class TRandomSegment : public TMoveSegment
{
public:
    TRandomSegment();
    TRandomSegment(const Model::TRandomSegment &randomSegment, void *context);
    ~TRandomSegment();

    void loadFromModel(const Model::TRandomSegment &randomSegment, void *context);

    // TMoveSegment interface
    TVector2 getSpeed() override;
    bool isEnd() const override;

    void setRect(const TRect &rect);
    void setRect(const TVector2 &start, const TVector2 &end);
    void setPoint(const TVector2 &point);

    void reset() override;

private:
    enum State
    {
        None,
        Randomed,
        Returned
    };

    TVector2 mStart;
    TVector2 mEnd;
    State mState;
    TVector2 mReturnPoint;
    TLineSegment mReturnSegment;
};

#endif // RANDOMVECTOR_H
