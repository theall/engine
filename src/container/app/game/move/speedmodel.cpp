#include "speedmodel.h"

#include <gameutils/model/move/speedmodel.h>

TSpeedModel::TSpeedModel() :
    mVelocity(0.0)
  , mAccelerator1(0.0)
  , mAccelerator2(0.0)
  , mDistance1(0.0)
  , mDistance2(0.0)
  , mDistance(0.0)
  , mCurrentSpeed(0.0)
  , mCurrentDistance(0.0)
  , mFirstPoint(0.0)
  , mSecondPoint(1.0)
{

}

TSpeedModel::TSpeedModel(const Model::TSpeedModel &speedModel, void *context) :
    mVelocity(0.0)
  , mAccelerator1(0.0)
  , mAccelerator2(0.0)
  , mDistance1(0.0)
  , mDistance2(0.0)
  , mDistance(0.0)
  , mCurrentSpeed(0.0)
  , mCurrentDistance(0.0)
  , mFirstPoint(0.0)
  , mSecondPoint(1.0)
{
    loadFromModel(speedModel, context);
}

TSpeedModel::~TSpeedModel()
{

}

void TSpeedModel::loadFromModel(const Model::TSpeedModel &speedModel, void *context)
{
    (void)context;
    mFirstPoint = speedModel.start();
    mSecondPoint = speedModel.end();
    mVelocity = speedModel.velocity();
    if(mSecondPoint<=0 || mSecondPoint>1)
        mSecondPoint = 1.0;

    if(mFirstPoint<0 || mFirstPoint > mSecondPoint)
        mFirstPoint = 0;
}

float TSpeedModel::getNextSpeed()
{
    if(mCurrentDistance < mDistance1) {
        mCurrentSpeed += mAccelerator1;
    } else if(mCurrentDistance < mDistance2) {
        mCurrentSpeed = mVelocity;
    } else if(mCurrentDistance < mDistance) {
        mCurrentSpeed += mAccelerator2;
    } else {
        mCurrentSpeed = 0;
    }

    mCurrentDistance += mCurrentSpeed;
    if(mCurrentDistance > mDistance)
        mCurrentSpeed = mDistance - mCurrentDistance;
    return mCurrentSpeed;
}

void TSpeedModel::setDistance(float distance)
{
    if(mVelocity == 0)
    {
        mDistance1 = 0;
        mDistance2 = 0;
        mDistance = 0;
        mAccelerator1 = 0;
        mAccelerator2 = 0;
    } else {
        mDistance1 = mFirstPoint * distance;
        mDistance2 = mSecondPoint * distance;
        mDistance = distance;
        mAccelerator1 =  mDistance1 / mVelocity;
        mAccelerator2 = -(1 - mSecondPoint) * distance / mVelocity;
    }
    mCurrentDistance = 0;
}

bool TSpeedModel::isEnd() const
{
    return mCurrentDistance>=mDistance;
}

void TSpeedModel::reset()
{
    mCurrentDistance = 0;
}

void TSpeedModel::setParameter(float x1, float x2, float velocity)
{
    mFirstPoint = x1;
    mSecondPoint = x2;
    mVelocity = velocity;
}
