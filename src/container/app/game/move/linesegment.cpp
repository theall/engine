#include "linesegment.h"

#include <math.h>
#include <gameutils/model/move/linesegment.h>

TLineSegment::TLineSegment() :
    TMoveSegment()
  , mDistance(0)
  , mRadian(0)
{

}

TLineSegment::TLineSegment(const Model::TLineSegment &lineSegment, void *context) :
    TMoveSegment()
  , mDistance(0)
  , mRadian(0)
{
    loadFromModel(lineSegment, context);
}

TLineSegment::~TLineSegment()
{

}

void TLineSegment::loadFromModel(const Model::TLineSegment &lineSegment, void *context)
{
    mSpeedModel.loadFromModel(lineSegment.speedModel(), context);
    setVector(lineSegment.vector());
}

TVector2 TLineSegment::getSpeed()
{
    TVector2 ret;
    float speed = mSpeedModel.getNextSpeed();
    ret.x = speed * cos(mRadian);
    ret.y = speed * sin(mRadian);
    return ret;
}

void TLineSegment::setVector(const TVector2 &vector)
{
    mDistance = sqrt(vector.x*vector.x + vector.y*vector.y);
    mRadian = atan(vector.y / vector.x);
    if(vector.x < 0)
        mRadian += M_PI;
    mSpeedModel.setDistance(mDistance);
}

bool TLineSegment::isEnd() const
{
    return mSpeedModel.isEnd();
}

void TLineSegment::reset()
{
    mSpeedModel.reset();
}

void TLineSegment::setSpeedModelParameter(float x1, float x2, float velocity)
{
    mSpeedModel.setParameter(x1, x2, velocity);
}
