#include "movemodel.h"
#include "linesegment.h"
#include "randomsegment.h"

#include <gameutils/model/move/movemodel.h>

TMoveModel::TMoveModel() :
    mLoopCount(0)
  , mRewindCount(0)
  , mCurrentSegment(0)
  , mIsValid(false)
{

}

TMoveModel::~TMoveModel()
{

}

void TMoveModel::loadFromModel(const Model::TMoveModel &moveModel, void *context)
{
    (void)context;
    mLoopCount = moveModel.loopCount();
    Model::TMoveSegmentList moveSegmentList = moveModel.moveSegmentList();

    for(Model::TMoveSegment *moveSegmentModel : moveSegmentList)
    {
        MoveSegmentType moveSegmentType = moveSegmentModel->moveSegmentType();
        if(moveSegmentType == MST_LINE)
        {
            TLineSegment *lineSegment = new TLineSegment();
            lineSegment->loadFromModel(*(Model::TLineSegment *)moveSegmentModel, this);
            mMoveSegmentList.emplace_back(lineSegment);
        } else if(moveSegmentType == MST_RANDOM) {
            TRandomSegment *randomSegment = new TRandomSegment();
            randomSegment->loadFromModel(*(Model::TRandomSegment *)moveSegmentModel, this);
            mMoveSegmentList.emplace_back(randomSegment);
        }
    }
    mIsValid = mMoveSegmentList.size()>0;
}

void TMoveModel::reset()
{

}

TVector2 TMoveModel::getNextSpeed()
{
    if(mLoopCount>0 && mRewindCount>=mLoopCount)
        return TVector2();
    int segmentSize = mMoveSegmentList.size();
    if(segmentSize <= 0)
        return TVector2();

    if(mCurrentSegment<0 || mCurrentSegment>=segmentSize)
        mCurrentSegment = 0;

    TMoveSegment *moveSegment = mMoveSegmentList.at(mCurrentSegment);
    TVector2 speed;
    if(moveSegment->isEnd()) {
        moveSegment->reset();
        mCurrentSegment++;
        if(mCurrentSegment >= segmentSize)
        {
            mRewindCount++;
        }
    } else {
        speed = moveSegment->getSpeed();
    }
    return speed;
}

bool TMoveModel::isValid() const
{
    return mIsValid;
}
