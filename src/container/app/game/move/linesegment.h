#ifndef LINESEGMENT_H
#define LINESEGMENT_H

#include "movesegment.h"

namespace Model {
class TLineSegment;
}

class TLineSegment : public TMoveSegment
{
public:
    TLineSegment();
    TLineSegment(const Model::TLineSegment &lineSegment, void *context);
    ~TLineSegment();

    void loadFromModel(const Model::TLineSegment &lineSegment, void *context);

    // TMoveSegment interface
    TVector2 getSpeed() override;

    void setVector(const TVector2 &vector);

    // TMoveSegment interface
    bool isEnd() const override;

    void reset() override;
    void setSpeedModelParameter(float x1, float x2, float velocity);

private:
    float mDistance;
    float mRadian;
    TSpeedModel mSpeedModel;
};

#endif // LINESEGMENT_H
