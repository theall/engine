#ifndef SPEEDMODEL_H
#define SPEEDMODEL_H

namespace Model {
class TSpeedModel;
}
class TSpeedModel
{
public:
    TSpeedModel();
    TSpeedModel(const Model::TSpeedModel &speedModel, void *context);
    ~TSpeedModel();

    void loadFromModel(const Model::TSpeedModel &speedModel, void *context);

    virtual float getNextSpeed();
    void setDistance(float distance);
    bool isEnd() const;
    void reset();
    void setParameter(float x1, float x2, float velocity);

private:
    float mVelocity;
    float mAccelerator1;
    float mAccelerator2;
    float mDistance1;
    float mDistance2;
    float mDistance;
    float mCurrentSpeed;
    float mCurrentDistance;
    float mFirstPoint;
    float mSecondPoint;
};

#endif // SPEEDMODEL_H
