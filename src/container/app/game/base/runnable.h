#ifndef RUNNABLE_H
#define RUNNABLE_H


class TRunnable
{
public:
    virtual ~TRunnable() {}
    virtual void step() = 0;
    virtual void render() = 0;
};

#endif // RUNNABLE_H
