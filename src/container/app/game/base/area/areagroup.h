#ifndef AREAGROUP_H
#define AREAGROUP_H

#include "rectarea.h"

struct TCollideAreaPair
{
    TRectArea *area1;
    TRectArea *area2;
    TRect rect;
};
typedef std::vector<TCollideAreaPair *> TCollideAreaPairList;

class TAreaGroup
{
public:
    TAreaGroup();
    TAreaGroup(const TRectAreaList &areaList);
    TAreaGroup(const TAreaGroup &areaGroup);
    ~TAreaGroup();

    void clear();
    void addArea(const TRect &rect);
    TAreaGroup *offset(const TVector2 &position) const;
    TRectList getRectList(const TVector2 &position = TVector2()) const;
    TCollideAreaPairList getCollideAreaPairList(const TAreaGroup &areaGroup) const;

    TRectAreaList getAreaList() const;
    void setAreaList(const TRectAreaList &areaList);
    void setRectList(const TRectList &rectList, const TVector2 &anchor = TVector2());

    TRectAreaList mirror(const TVector2 &anchor, float width) const;
    void operator =(const TRectAreaList &areaList);
    void operator =(const TAreaGroup &areaGroup);

    bool isValid() const;

private:
    TRectAreaList mAreaList;
};

typedef std::vector<TAreaGroup *> TAreaGroupList;

#endif // AREAGROUP_H
