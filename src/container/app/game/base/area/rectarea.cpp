#include "rectarea.h"

TRectArea::TRectArea(const TRect &rect) :
    TArea(TArea::Box)
  , mRect(rect)
{

}

TRectArea::TRectArea(const TRectArea &rectArea) :
    TArea(TArea::Box)
  , mRect(rectArea.rect())
{

}

TRectArea::~TRectArea()
{

}

TArea *TRectArea::offset(const TVector2 &position)
{
    return new TRectArea(mRect.offset(position));
}

TRect TRectArea::collideWith(const TRectArea &rectArea) const
{
    return mRect.intersection(rectArea.rect());
}

TRect TRectArea::rect() const
{
    return mRect;
}

void TRectArea::setRect(const TRect &rect)
{
    mRect = rect;
}
