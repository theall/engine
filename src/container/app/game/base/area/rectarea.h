#ifndef RECTAREA_H
#define RECTAREA_H

#include "area.h"
#include <gameutils/base/rect.h>
#include <gameutils/base/vector.h>

class TRectArea : public TArea
{
public:
    TRectArea(const TRect &rect = TRect());
    TRectArea(const TRectArea &rectArea);
    ~TRectArea();

    TArea *offset(const TVector2 &position) override;
    TRect collideWith(const TRectArea &rectArea) const;

    TRect rect() const;
    void setRect(const TRect &rect);

private:
    TRect mRect;
};
typedef std::vector<TRectArea *> TRectAreaList;

#endif // RECTAREA_H
