#include "areagroup.h"

#include <gameutils/base/utils.h>

TAreaGroup::TAreaGroup()
{

}

TAreaGroup::TAreaGroup(const TRectAreaList &areaList) :
    mAreaList(areaList)
{

}

TAreaGroup::TAreaGroup(const TAreaGroup &areaGroup) :
    mAreaList(areaGroup.getAreaList())
{

}

TAreaGroup::~TAreaGroup()
{
    clear();
}

void TAreaGroup::clear()
{
    FREE_CONTAINER(mAreaList);
}

void TAreaGroup::addArea(const TRect &rect)
{
    mAreaList.emplace_back(new TRectArea(rect));
}

TAreaGroup *TAreaGroup::offset(const TVector2 &position) const
{
    TRectAreaList newAreaList;
    for(TRectArea *rectArea : mAreaList)
    {
        newAreaList.emplace_back(static_cast<TRectArea *>(rectArea->offset(position)));
    }

    return new TAreaGroup(newAreaList);
}

TRectList TAreaGroup::getRectList(const TVector2 &position) const
{
    TRectList newAreaList;
    for(TRectArea *rectArea : mAreaList)
    {
        newAreaList.emplace_back(rectArea->rect().offset(position));
    }

    return newAreaList;
}

TCollideAreaPairList TAreaGroup::getCollideAreaPairList(const TAreaGroup &areaGroup) const
{
    TCollideAreaPairList pairList;
    for(TRectArea *thisArea : mAreaList)
    {
        for(TRectArea *destArea : areaGroup.getAreaList())
        {
            TRect rect = thisArea->collideWith(*destArea);
            if(rect.isValid())
            {
                TCollideAreaPair *collideAreaPair = new TCollideAreaPair;
                collideAreaPair->area1 = thisArea;
                collideAreaPair->area2 = destArea;
                collideAreaPair->rect = rect;
                pairList.emplace_back(collideAreaPair);
            }
        }
    }
    return pairList;
}

TRectAreaList TAreaGroup::getAreaList() const
{
    return mAreaList;
}

void TAreaGroup::setAreaList(const TRectAreaList &areaList)
{
    clear();

    for(TRectArea *rectArea : areaList)
    {
        mAreaList.emplace_back(new TRectArea(*rectArea));
    }
}

void TAreaGroup::setRectList(const TRectList &rectList, const TVector2 &anchor)
{
    clear();
    for(TRect rect : rectList)
    {
        rect.offset(anchor);
        mAreaList.emplace_back(new TRectArea(rect));
    }
}

TRectAreaList TAreaGroup::mirror(const TVector2 &anchor, float width) const
{
    TRectAreaList rectAreaList;
    for(TRectArea *rectArea : mAreaList)
    {
        TRect rect = rectArea->rect();
        rect.offset(anchor);
        rect.mirror(width);
        rectAreaList.emplace_back(new TRectArea(rect));
    }
    return rectAreaList;
}

void TAreaGroup::operator =(const TRectAreaList &areaList)
{
    setAreaList(areaList);
}

void TAreaGroup::operator =(const TAreaGroup &areaGroup)
{
    *this = areaGroup.getAreaList();
}

bool TAreaGroup::isValid() const
{
    return !mAreaList.empty();
}
