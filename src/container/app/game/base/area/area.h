#ifndef AREA_H
#define AREA_H

#include <vector>
#include <gameutils/base/vector.h>

class TArea
{
public:
    enum Type
    {
        Box,
        Circle,
        Edge,
        Triangle
    };

    TArea(Type type);
    virtual ~TArea();

    virtual TArea *offset(const TVector2 &position) = 0;

    Type type() const;

private:
    Type mType;
};
typedef std::vector<TArea *> TAreaList;

#endif // AREA_H
