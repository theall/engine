#ifndef CAMERA_H
#define CAMERA_H

#include <vector>
#include <gameutils/base/rect.h>

#include "layer/sprite.h"

namespace Model {
class TCamera;
}

class TCamera : public TRunnable
{
public:
    TCamera();
    TCamera(const Model::TCamera &cameraModel, void *context = nullptr);
    virtual ~TCamera();

    void loadFromModel(const Model::TCamera &cameraModel, void *context = nullptr);

    TRect rect() const;
    void setRect(const TRect &rect);

    void addSprite(TSprite *sprite);
    void removeSprite(TSprite *sprite);

    // TRunnable interface
    void step() override;
    void render() override;

    bool contain(const TRect &rect, bool restrictCheck = false);
    bool contain(TSprite *sprite, bool restrictCheck = false);

    bool move(float dx, float dy);
    bool move(const TVector2 &d);

    TRect viewRect() const;
    void updateViewRect();

    void clear();

private:
    float mScale;
    TRect mRect;
    TRect mViewRect;
    SpriteDirection mDirection;
    std::vector<TSprite *> mSpriteList;
};

#endif // CAMERA_H
