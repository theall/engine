#include "imagesprite.h"
#include "../scene.h"
#include "../../resourcecache.h"

#include <gameutils/model/scene/layer/imagesprite.h>

TImageSprite::TImageSprite() :
    TSprite(ST_IMAGE)
  , mImage(nullptr)
{

}

TImageSprite::~TImageSprite()
{

}

TImageSprite::TImageSprite(const Model::TImageSprite &spriteModel, void *context) :
    TSprite(ST_IMAGE)
{
    loadFromModel(spriteModel, context);
}

void TImageSprite::loadFromModel(const Model::TImageSprite &spriteModel, void *context)
{
    (void)context;
    mImage = TResourceCache::instance()->loadImage(spriteModel.imagePath());
    mPos = spriteModel.position();
    if(mImage)
        mRect = TRect(mPos.x, mPos.y, mImage->getWidth(), mImage->getHeight());
}

int TImageSprite::getWidth() const
{
    return mImage?mImage->getWidth():0;
}

int TImageSprite::getHeight() const
{
    return mImage?mImage->getHeight():0;
}

TVector2 TImageSprite::getSize() const
{
    TVector2 size;
    if(mImage)
    {
        size.x = mImage->getWidth();
        size.y = mImage->getHeight();
    }
    return size;
}

TRect TImageSprite::getCurrentRect()
{
    return mRect;
}

void TImageSprite::step()
{
    TSprite::step();
}

void TImageSprite::render()
{
    if(!mImage)
        return;

    mImage->draw(mPos.x, mPos.y);
}
