#ifndef GHOSTSPRITE_H
#define GHOSTSPRITE_H

#include "sprite.h"
#include "../../base/area/areagroup.h"

namespace Model {
class TGhostSprite;
}

class TGhostSprite : public TSprite
{
public:
    TGhostSprite(const Model::TGhostSprite &spriteModel, void *context = nullptr);
    ~TGhostSprite();

    void loadFromModel(const Model::TGhostSprite &spriteModel, void *context = nullptr);
    TAreaGroup *getAreaGroup();
    void setAreaGroup(const TAreaGroup &areaGroup);

private:
    TRect mRect;
    TAreaGroup mAreaGroup;

    // TRunnable interface
public:
    void step() override;
    void render() override;

    // TSprite interface
public:
    TRect getCurrentRect() override;
};

#endif // GHOSTSPRITE_H
