#ifndef BUTTONSPRITE_H
#define BUTTONSPRITE_H

#include "imagesprite.h"
#include "textsprite.h"

namespace Model {
class TButtonSprite;
}

class TButtonSprite : public TSprite
{
public:
    TButtonSprite();
    TButtonSprite(const Model::TButtonSprite &buttonModel, void *context = nullptr);
    ~TButtonSprite();

    void loadFromModel(const Model::TButtonSprite &buttonModel, void *context = nullptr);

    std::string text() const;
    void setText(const std::string &text);

private:
    std::string mName;
    TRect mRect;
    TImageSprite mNormalImage;
    TImageSprite mDisabledImage;
    TImageSprite mSelectedImage;
    TImageSprite mClickedImage;
    TTextSprite mTextSprite;
    TSource *mEnterSound;
    TSource *mClickSound;
    bool mMouseIn;
    bool mMousePressed;
    bool mIsEnabled;
    int mPressedFrames;

    // TSprite interface
public:
    void step() override;
    void render() override;
    TRect getCurrentRect() override;
    bool isEnabled() const;
    void setEnabled(bool isEnabled);
    bool mouseIn() const;
    bool mousePressed() const;
};

#endif // BUTTONSPRITE_H
