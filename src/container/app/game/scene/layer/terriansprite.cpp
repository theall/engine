#include "terriansprite.h"

#include <sdk/engine.h>
#include <gameutils/model/scene/layer/terriansprite.h>

extern TGraphics *gfx;

TTerrianSprite::TTerrianSprite(const Model::TTerrianSprite &spriteModel, void *context) :
    TSprite(ST_TERRIAN)
{
    loadFromModel(spriteModel, context);
}

TTerrianSprite::~TTerrianSprite()
{

}

void TTerrianSprite::loadFromModel(const Model::TTerrianSprite &spriteModel, void *context)
{
    (void)context;
    mRect = spriteModel.rect();
    mAreaGroup.clear();
    mAreaGroup.addArea(mRect);
}

TAreaGroup *TTerrianSprite::getAreaGroup()
{
    return &mAreaGroup;
}

void TTerrianSprite::setAreaGroup(const TAreaGroup &areaGroup)
{
    mAreaGroup = areaGroup;
}

void TTerrianSprite::step()
{

}

void TTerrianSprite::render()
{
    if(mDebugEnable)
    {
        gfx->drawRect(mRect.x, mRect.y, mRect.w, mRect.h, Colorf(255,255,0,255));
    }
}

TRect TTerrianSprite::getCurrentRect()
{
    return mRect;
}
