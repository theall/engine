#include "blinktextsprite.h"

TBlinkTextSprite::TBlinkTextSprite() :
    TSprite(ST_BLINK_TEXT)
  , mFrameCount(0)
  , mInterval(0)
{

}

TBlinkTextSprite::~TBlinkTextSprite()
{

}

int TBlinkTextSprite::interval() const
{
    return mInterval;
}

void TBlinkTextSprite::setInterval(int frames)
{
    mInterval = frames;
}

void TBlinkTextSprite::step()
{
    mFrameCount++;
}

void TBlinkTextSprite::render()
{

}

TRect TBlinkTextSprite::getCurrentRect()
{
    return TRect();
}
