#ifndef IMAGESPRITE_H
#define IMAGESPRITE_H

#include "sprite.h"
#include <sdk/engine.h>

namespace Model {
class TImageSprite;
}

class TImageSprite : public TSprite
{
public:
    TImageSprite();
    TImageSprite(const Model::TImageSprite &spriteModel, void *context = nullptr);
    ~TImageSprite();

    void loadFromModel(const Model::TImageSprite &spriteModel, void *context = nullptr);

    int getWidth() const;
    int getHeight() const;
    TVector2 getSize() const;

    TRect getCurrentRect() override;

private:
    TRect mRect;
    TDrawableImage *mImage;

    // TSprite interface
public:
    void step() override;
    void render() override;
};

#endif // IMAGESPRITE_H
