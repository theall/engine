#include "buttonsprite.h"
#include "../../resourcecache.h"

#include <sdk/engine.h>
#include <gameutils/model/scene/layer/buttonsprite.h>

extern TMouse *mouse;
extern TGraphics *gfx;

static const TVector2 g_offset = TVector2(4,3);

TButtonSprite::TButtonSprite() :
    TSprite(ST_BUTTON)
  , mMouseIn(false)
  , mMousePressed(false)
  , mPressedFrames(0)
{

}

TButtonSprite::TButtonSprite(const Model::TButtonSprite &buttonModel, void *context) :
    TSprite(ST_BUTTON)
  , mMouseIn(false)
  , mMousePressed(false)
  , mPressedFrames(0)
{
    loadFromModel(buttonModel, context);
}

TButtonSprite::~TButtonSprite()
{

}

void TButtonSprite::loadFromModel(const Model::TButtonSprite &buttonModel, void *context)
{
    TVector2 offset = buttonModel.position();
    mRect = TRect(offset, buttonModel.size());
    mNormalImage.loadFromModel(buttonModel.normalImage(), context);
    mNormalImage.move(offset);
    mDisabledImage.loadFromModel(buttonModel.disabledImage(), context);
    mDisabledImage.move(offset);
    mSelectedImage.loadFromModel(buttonModel.selectedImage(), context);
    mSelectedImage.move(offset);
    mClickedImage.loadFromModel(buttonModel.clickedImage(), context);
    mClickedImage.move(offset+g_offset);

    mTextSprite.loadFromModel(buttonModel.textSprite());
    mEnterSound = TResourceCache::instance()->loadSound(buttonModel.enterSound());
    mClickSound = TResourceCache::instance()->loadSound(buttonModel.clickSound());

    setText(buttonModel.text());
}

std::string TButtonSprite::text() const
{
    return mTextSprite.text();
}

void TButtonSprite::setText(const std::string &text)
{
    mTextSprite.setText(text);
    mTextSprite.move(mRect.hcenter(mTextSprite.textWidth()), mRect.vcenter(mTextSprite.textHeight()));
}

bool TButtonSprite::mouseIn() const
{
    return mMouseIn;
}

bool TButtonSprite::mousePressed() const
{
    return mMousePressed;
}

bool TButtonSprite::isEnabled() const
{
    return mIsEnabled;
}

void TButtonSprite::setEnabled(bool isEnabled)
{
    mIsEnabled = isEnabled;
}

void TButtonSprite::step()
{
    double mouseX;
    double mouseY;
    mouse->getPosition(mouseX, mouseY);
    bool mouseIn = mRect.contain((float)mouseX, (float)mouseY);
    bool leftMmouseDown = mouse->isLeftButtonDown();
    if(mouseIn)
    {
        // Enter in
        if(!mMouseIn)
        {
            if(mEnterSound) {
                mEnterSound->restart();
            }
        }
        if(leftMmouseDown)
        {
            if(mMousePressed)
                mPressedFrames++;
            else {
                // Play sound
                if(mClickSound) {
                    mClickSound->restart();
                }
            }
        } else {
            mPressedFrames = 0;
            if(mMousePressed)
            {
                // Trigger event
            }
        }
        mMousePressed = leftMmouseDown;
    } else {
        if(mMouseIn)
        {
            // Mouse leave
        }
    }
    mMouseIn = mouseIn;

    TSprite::step();
}

void TButtonSprite::render()
{
    if(mIsEnabled)
    {
        if(mMouseIn)
        {
            if(mMousePressed)
                mClickedImage.render();
            else
                mSelectedImage.render();
        } else {
            mNormalImage.render();
        }
    } else {
        mDisabledImage.render();
    }

    TVector2 textPos;
    if(mMousePressed)
    {
        textPos = g_offset;
    } else if (mMouseIn) {
        //textPos = TVector2(-10, 0);
    }
    mTextSprite.arbitrarilyRender(textPos);

    if(mMouseIn)
    {
        gfx->drawRect(mRect.x, mRect.y, mRect.w, mRect.h);
    }
}

TRect TButtonSprite::getCurrentRect()
{
    return mRect;
}
