#include "textsprite.h"

#include <gameutils/model/scene/layer/textsprite.h>

Color toColor(const TColor &color)
{
    return Color(color.r, color.g, color.b, color.a);
}

Colorf toColorf(const TColor &color)
{
    return Colorf(color.r, color.g, color.b, color.a);
}

TTextSprite::TTextSprite() :
    TSprite(ST_TEXT)
  , mTextWidth(0)
  , mTextHeight(0)
{

}

TTextSprite::TTextSprite(const std::string &text, const TColor &color) :
    TSprite(ST_TEXT)
  , mTextColor(toColor(color))
  , mTextWidth(0)
  , mTextHeight(0)
{
    setText(text);
}

TTextSprite::TTextSprite(const Model::TTextSprite &textModel, void *context) :
    TSprite(ST_TEXT)
  , mTextWidth(0)
  , mTextHeight(0)
{
    loadFromModel(textModel, context);
}

TTextSprite::~TTextSprite()
{

}

void TTextSprite::loadFromModel(const Model::TTextSprite &textModel, void *context)
{
    (void)context;

    mStdText = textModel.text();
    mTextColor = toColor(textModel.textColor());

    prepareDrawableText();
}

std::string TTextSprite::text() const
{
    return mStdText;
}

void TTextSprite::setText(const std::string &text)
{
    mStdText = text;

    prepareDrawableText();
}

float TTextSprite::textWidth() const
{
    return mTextWidth;
}

float TTextSprite::textHeight() const
{
    return mTextHeight;
}

void TTextSprite::arbitrarilyRender(const TVector2 &offset)
{
    TVector2 newPos = mPos + offset;
    mText.draw(newPos.x, newPos.y);
}

void TTextSprite::arbitrarilyRender(float scale)
{
    mText.draw(mPos.x, mPos.y, 0, scale, scale);
}

void TTextSprite::prepareDrawableText()
{
    TFont::ColoredString coloredString;
    coloredString.str = mStdText;
    coloredString.color = mTextColor;
    mText.set(coloredString);

    mTextHeight = mText.getHeight();
    mTextWidth = mText.getWidth();
}

Color TTextSprite::textColor() const
{
    return mTextColor;
}

void TTextSprite::setTextColor(const Color &textColor)
{
    mTextColor = textColor;

    prepareDrawableText();
}

void TTextSprite::step()
{
    TSprite::step();
}

void TTextSprite::render()
{
    mText.draw(mPos.x, mPos.y);
}

TRect TTextSprite::getCurrentRect()
{
    return TRect(mPos, TVector2(mTextWidth, mTextHeight));
}
