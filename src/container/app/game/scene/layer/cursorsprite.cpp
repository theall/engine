#include "cursorsprite.h"

#include <gameutils/model/scene/layer/cursorsprite.h>

extern TMouse *mouse;

TCursorSprite::TCursorSprite() :
    mCursor(nullptr)
{

}

TCursorSprite::TCursorSprite(const Model::TCursorSprite &spriteModel, void *context) :
    mCursor(nullptr)
{
    loadFromModel(spriteModel, context);
}

TCursorSprite::~TCursorSprite()
{
    if(mCursor)
    {
        mCursor->release();
        mCursor = nullptr;
    }
}

void TCursorSprite::loadFromModel(const Model::TCursorSprite &spriteModel, void *context)
{
    (void)context;

    TVector2 hotPos = spriteModel.hotPos();
    std::string file = spriteModel.file();
    if(!file.empty())
        mCursor = mouse->newCursor(spriteModel.file(), (int)hotPos.x, (int)hotPos.y);
}

TCursor *TCursorSprite::cursor() const
{
    return mCursor;
}
