#include "charactersprite.h"
#include "../../game.h"
#include "../../../controller/controller.h"
#include "../../resourcecache.h"
#include "../../character/actiontrigger.h"
#include <gameutils/model/scene/layer/charactersprite.h>

extern TGraphics *gfx;

#define DRAW_AREA(rectList,r,g,b) \
    gfx->setColor(Colorf(r,g,b,255));\
    for(TRect rt : rectList)\
    {\
        if(rt.isValid())\
        {\
            gfx->drawRect(rt.x, rt.y, rt.w, rt.h);\
        }\
    }

#define FREE_MAP(x) for(auto it : x)\
{\
    delete it.first;\
    delete it.second;\
}\
x.clear();

TCharacterSprite::TCharacterSprite(TCharacter *character) :
    TSprite(ST_CHARACTER)
  , mDirection(SD_EAST)
  , mController(nullptr)
  , mCurrentAction(nullptr)
  , mLeftDefaultAction(nullptr)
  , mRightDefaultAction(nullptr)
  , mCurrentSoundSet(nullptr)
  , mNoneEventFrames(0)
  , mIdelFrames(300)
{
    loadFromCharacter(character);
}

TCharacterSprite::TCharacterSprite(const Model::TCharacterSprite &characterModel, void *context) :
    TSprite(ST_CHARACTER)
  , mController(nullptr)
  , mCurrentAction(nullptr)
  , mLeftDefaultAction(nullptr)
  , mRightDefaultAction(nullptr)
  , mCurrentSoundSet(nullptr)
  , mNoneEventFrames(0)
  , mIdelFrames(0)
{
    loadFromModel(characterModel, context);
}

TCharacterSprite::~TCharacterSprite()
{
    freeResource();
}

void TCharacterSprite::loadFromCharacter(TCharacter *character)
{
    if(!character)
        return;

    freeResource();
    TAction *defaultLeftAction = character->getLeftDefaultAction();
    for(TAction *action : character->getLeftActionList())
    {
        TAction *newAction = new TAction(*action);
        if(action == defaultLeftAction)
            mLeftDefaultAction = newAction;
        for(TActionTrigger *actionTrigger : action->getActionTriggerList()) {
            mLeftTriggerActionMap.insert(std::make_pair(new TActionTrigger(*actionTrigger), newAction));
        }
    }

    TAction *defaultRightAction = character->getRightDefaultAction();
    for(TAction *action : character->getRightActionList())
    {
        TAction *newAction = new TAction(*action);
        if(action == defaultRightAction)
            mRightDefaultAction = newAction;
        for(TActionTrigger *actionTrigger : action->getActionTriggerList()) {
            mRightTriggerActionMap.insert(std::make_pair(new TActionTrigger(*actionTrigger), newAction));
        }
    }
    setDirection(mDirection);
}

void TCharacterSprite::loadFromModel(const Model::TCharacterSprite &characterModel, void *context)
{
    (void)context;
    std::string characterId = characterModel.uuid();
    TCharacter *character = TResourceCache::instance()->loadCharacter(characterId);
    if(!character)
        throw TException("Character is not found with string: %s", characterId.c_str());

    loadFromCharacter(character);   
    setDirection(characterModel.direction());
    TSprite::loadFromModel(characterModel, context);
}

void TCharacterSprite::step()
{
    TAction *actionFind = nullptr;
    GameEvent gameEvent = GE_NONE;
    std::string commandString;
    std::vector<Command> commandList;
    if(mController)
    {
        mController->step();
        if(mController->hasKeyDown())
        {
            gameEvent = GE_KEYDOWN;
            commandString = mController->getInputCommandString();
            commandList = mController->getCommandList();
        }
    }
    if(mCurrentAction)
    {
        mCurrentAction->step();
        if(mCurrentAction->isEnd()) {
            actionFind = nullptr;
            if(mCurrentAction==mLeftDefaultAction || mCurrentAction==mRightDefaultAction)
                gameEvent = GE_IDLE;                
        } else {
            InterruptType it = mCurrentAction->getInterruptType();
            if(it==IT_DISABLE)
                actionFind = mCurrentAction;
            else if(it==IT_KEY_EVENT) {
                if(gameEvent==GE_NONE) {
                    actionFind = mCurrentAction;
                }
            }
        }
    }
    if(!actionFind)
    {
        for(auto actionTriggerIterator : mTriggerActionMap)
        {
            if(actionTriggerIterator.first->fired(gameEvent, commandString))
            {
                actionFind = actionTriggerIterator.second;
                break;
            }
        }
        if(!actionFind || actionFind==mLeftDefaultAction || actionFind==mRightDefaultAction)
        {
            if(commandList.size()>0)
            {
                Command &command = commandList.back();
                if((command.value&TButton::LEFT) && mDirection!=SD_WEST)
                {
                    mDirection = SD_WEST;
                    gameEvent = GE_CHANGE_DIR;
                    mTriggerActionMap = mLeftTriggerActionMap;
                } else if((command.value&TButton::RIGHT) && mDirection!=SD_EAST) {
                    mDirection = SD_EAST;
                    gameEvent = GE_CHANGE_DIR;
                    mTriggerActionMap = mRightTriggerActionMap;
                }
            }
            actionFind = mDirection==SD_WEST?mLeftDefaultAction:mRightDefaultAction;
        }
    }

    if(actionFind && mCurrentAction != actionFind)
    {
        clearSpeed();

        if(mController)
            mController->clear();
    }

    setCurrentAction(actionFind);

    if(mCurrentAction) {
        TFrame *currentFrame = mCurrentAction->getCurrentFrame();
        if(!mCurrentAction->isRecycled())
            mSpeed += currentFrame->currentVector();
        if(currentFrame && currentFrame->isFirst())
        {
            TSoundSet *newSoundSet = (TSoundSet *)currentFrame->getSoundSet();
            if(newSoundSet) {
                mCurrentSoundSet = newSoundSet;
                newSoundSet->shuttle();
            }
        }
    }
    if(mCurrentSoundSet)
        mCurrentSoundSet->step();

    TSprite::step();
}

TAction *TCharacterSprite::getCurrentAction() const
{
    return mCurrentAction;
}

void TCharacterSprite::setCurrentAction(TAction *action)
{
    if(mCurrentAction==action)
        return;

    mCurrentAction = action;
    if(action) {
        action->reset();
    }
}

TFrame *TCharacterSprite::getCurrentFrame() const
{
    if(!mCurrentAction)
        return nullptr;

    return mCurrentAction->getCurrentFrame();
}

void TCharacterSprite::freeResource()
{
    FREE_MAP(mLeftTriggerActionMap);
    FREE_MAP(mRightTriggerActionMap);
}

void TCharacterSprite::render()
{
    TFrame *frame = getCurrentFrame();
    if(!frame)
        return;

    TVector2 imagePos = mPos - frame->mAnchor;;
    if(frame->mImage)
    {
        frame->mImage->draw(imagePos.x, imagePos.y);
    }
    if(mCurrentSoundSet)
        mCurrentSoundSet->render();

    if(mDebugEnable)
    {
        Colorf currentColor = gfx->getColor();
        DRAW_AREA(frame->mCollideAreaGroup.getRectList(imagePos), 128, 128, 128);
        DRAW_AREA(frame->mAttackAreaGroup.getRectList(imagePos), 255, 0, 0);
        DRAW_AREA(frame->mUndertakeAreaGroup.getRectList(imagePos), 0, 255, 0);
        DRAW_AREA(frame->mTerrianAreaGroup.getRectList(imagePos), 0, 0, 255);
        gfx->setColor(currentColor);

        if(mCurrentAction)
        {
            char szBuf[200];
            itoa(mCurrentAction->getIndex(), szBuf, 10);
            gfx->print(std::string(szBuf), imagePos.x, imagePos.y);
        }
    }
}

TRect TCharacterSprite::getCurrentRect()
{
    TFrame *frame = getCurrentFrame();
    if(!frame || !frame->mImage)
        return TRect();

    return TRect(mPos.x - frame->mAnchor.x, mPos.y - frame->mAnchor.y, frame->mImage->getWidth(), frame->mImage->getHeight());
}

SpriteDirection TCharacterSprite::getDirection() const
{
    return mDirection;
}

void TCharacterSprite::setDirection(const SpriteDirection &spriteDirection)
{
    mDirection = spriteDirection;
    if(mDirection == SD_WEST)
        mTriggerActionMap = mLeftTriggerActionMap;
    else
        mTriggerActionMap = mRightTriggerActionMap;
}

TController *TCharacterSprite::getController() const
{
    return mController;
}

void TCharacterSprite::setController(TController *controller)
{
    mController = controller;
}

bool TCharacterSprite::CmpByValueLength::operator()(const TActionTrigger *t1, const TActionTrigger *t2) {
    return t1->value().size() >= t2->value().size();
}
