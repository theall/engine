#ifndef DOORSPRITE_H
#define DOORSPRITE_H

#include "sprite.h"
#include <sdk/engine.h>

namespace Model {
class TDoorSprite;
}

class TDoorSprite : public TSprite
{
public:
    TDoorSprite();
    TDoorSprite(const Model::TDoorSprite &spriteModel, void *context = nullptr);
    ~TDoorSprite();

    void loadFromModel(const Model::TDoorSprite &spriteModel, void *context = nullptr);

    TRect entryRect() const;
    TRect exitRect() const;

    // TRunnable interface
    void step() override;
    void render() override;

    // TSprite interface
    TRect getCurrentRect() override;

private:
    TRect mEntryRect;
    TRect mExitRect;
};

#endif // DOORSPRITE_H
