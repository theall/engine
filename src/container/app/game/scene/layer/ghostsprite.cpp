#include "ghostsprite.h"

#include <sdk/engine.h>
#include <gameutils/model/scene/layer/ghostsprite.h>

extern TGraphics *gfx;

TGhostSprite::TGhostSprite(const Model::TGhostSprite &spriteModel, void *context) :
    TSprite(ST_GHOST)
{
    loadFromModel(spriteModel, context);
}

TGhostSprite::~TGhostSprite()
{

}

void TGhostSprite::loadFromModel(const Model::TGhostSprite &spriteModel, void *context)
{
    (void)context;
    mRect = spriteModel.rect();
    mAreaGroup.clear();
    mAreaGroup.addArea(mRect);
}

TAreaGroup *TGhostSprite::getAreaGroup()
{
    return &mAreaGroup;
}

void TGhostSprite::setAreaGroup(const TAreaGroup &areaGroup)
{
    mAreaGroup = areaGroup;
}

void TGhostSprite::step()
{

}

void TGhostSprite::render()
{
    if(mDebugEnable)
    {
        gfx->drawRect(mRect.x, mRect.y, mRect.w, mRect.h, Colorf(255,0,0,255));
    }
}

TRect TGhostSprite::getCurrentRect()
{
    return mRect;
}
