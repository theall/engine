#ifndef CHARACTERSPRITET_H
#define CHARACTERSPRITET_H

#include <vector>
#include <gameutils/base/rect.h>

#include "sprite.h"
#include "../../character/character.h"

namespace Model {
class TCharacterSprite;
}

class TController;

class TCharacterSprite : public TSprite
{
public:
    TCharacterSprite(TCharacter *character);
    TCharacterSprite(const Model::TCharacterSprite &characterModel, void *context = nullptr);
    ~TCharacterSprite();

    void loadFromCharacter(TCharacter *character);
    void loadFromModel(const Model::TCharacterSprite &characterModel, void *context = nullptr);

    SpriteDirection getDirection() const;
    void setDirection(const SpriteDirection &spriteDirection);

    TController *getController() const;
    void setController(TController *controller);

    TAction *getCurrentAction() const;
    void setCurrentAction(TAction *action);

    void step() override;
    void render() override;
    TRect getCurrentRect() override;

    TFrame *getCurrentFrame() const;

private:
    SpriteDirection mDirection;
    TController *mController;
    TAction *mCurrentAction;
    TAction *mLeftDefaultAction;
    TAction *mRightDefaultAction;
    TSoundSet *mCurrentSoundSet;
    TSoundSetList mSoundSetList;
    int mNoneEventFrames;
    int mIdelFrames;

    struct CmpByValueLength {
        bool operator()(const TActionTrigger *t1, const TActionTrigger *t2);
    };
    std::map<TActionTrigger *, TAction *, CmpByValueLength> mTriggerActionMap;
    std::map<TActionTrigger *, TAction *, CmpByValueLength> mLeftTriggerActionMap;
    std::map<TActionTrigger *, TAction *, CmpByValueLength> mRightTriggerActionMap;

    void freeResource();
};

typedef std::vector<TCharacterSprite*> TCharacterSpriteList;

#endif // CHARACTERSPRITET_H
