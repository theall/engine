#ifndef SPRITE_H
#define SPRITE_H

#include <string>
#include <vector>
#include <gameutils/base/rect.h>
#include <gameutils/base/vector.h>
#include <gameutils/base/variable.h>

#include "../../base/runnable.h"
#include "../../move/movemodel.h"

namespace Model {
class TSprite;
}

class TSprite : public TRunnable
{
public:
    TSprite(const SpriteType &spriteType);
    virtual ~TSprite();

    void loadFromModel(const Model::TSprite &model, void *context = nullptr);

    SpriteType getSpriteType() const;

    void moveTo(const TVector2 &newPos);
    void moveTo(float newX, float newY);
    void move(const TVector2 &dPos);
    void move(float dx, float dy);

    TVector2 getPos() const;
    void setPos(const TVector2 &pos);
    void setPos(float x, float y);
    int getZValue() const;
    void setZValue(int value);

    TVector2 getSpeed() const;
    void setSpeed(const TVector2 &speed);
    void addSpeed(const TVector2 &speed);
    void clearSpeed();

    static void setDebugFlag(bool enable);

    virtual TRect getCurrentRect() = 0;

    virtual void step() override;
    virtual void render() override;

    GameEvent getEvent() const;
    void setEvent(const GameEvent &event);

    bool getInAir() const;
    void setInAir(bool inAir);

    static TVector2 getMaxSpeed();
    static void setMaxSpeed(const TVector2 &maxSpeed);

protected:
    SpriteType mSpriteType;
    int mZValue;
    TVector2 mPos;
    TVector2 mSpeed;
    TMoveModel mMoveModel;
    GameEvent mEvent;
    bool mInAir;
    static bool mDebugEnable;
    static TVector2 mMaxSpeed;
};

typedef std::vector<TSprite*> TSpriteList;

#endif // SPRITE_H
