#ifndef CURSORSPRITE_H
#define CURSORSPRITE_H

#include "sprite.h"
#include <sdk/engine.h>

namespace Model {
class TCursorSprite;
}

class TCursorSprite
{
public:
    TCursorSprite();
    TCursorSprite(const Model::TCursorSprite &spriteModel, void *context = nullptr);
    ~TCursorSprite();

    void loadFromModel(const Model::TCursorSprite &spriteModel, void *context = nullptr);

    TCursor *cursor() const;

private:
    TCursor *mCursor;
};

#endif // CURSORSPRITE_H
