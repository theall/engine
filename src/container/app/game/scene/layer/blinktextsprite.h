#ifndef BLINKTEXTSPRITE_H
#define BLINKTEXTSPRITE_H

#include "sprite.h"

class TBlinkTextSprite : public TSprite
{
public:
    TBlinkTextSprite();
    ~TBlinkTextSprite();

    int interval() const;
    void setInterval(int frames);

private:
    int mFrameCount;
    int mInterval;

    // TSprite interface
public:
    void step() override;
    void render() override;

    // TSprite interface
    TRect getCurrentRect() override;
};

#endif // BLINKTEXTSPRITE_H
