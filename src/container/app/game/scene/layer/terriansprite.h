#ifndef TERRIANSPRITE_H
#define TERRIANSPRITE_H

#include "sprite.h"
#include "../../base/area/areagroup.h"

namespace Model {
class TTerrianSprite;
}

class TTerrianSprite : public TSprite
{
public:
    TTerrianSprite(const Model::TTerrianSprite &spriteModel, void *context = nullptr);
    ~TTerrianSprite();

    void loadFromModel(const Model::TTerrianSprite &spriteModel, void *context = nullptr);
    TAreaGroup *getAreaGroup();
    void setAreaGroup(const TAreaGroup &areaGroup);

private:
    TRect mRect;
    TAreaGroup mAreaGroup;

    // TRunnable interface
public:
    void step() override;
    void render() override;

    // TSprite interface
public:
    TRect getCurrentRect() override;
};

#endif // TERRIANSPRITE_H
