#ifndef TEXTSPRITE_H
#define TEXTSPRITE_H

#include "sprite.h"
#include <sdk/engine.h>
#include <gameutils/base/color.h>

namespace Model {
class TTextSprite;
}

Color toColor(const TColor &color);
Colorf toColorf(const TColor &color);

class TTextSprite : public TSprite
{
public:
    TTextSprite();
    TTextSprite(const std::string &text, const TColor &color = TColor());
    TTextSprite(const Model::TTextSprite &textModel, void *context = nullptr);
    ~TTextSprite();

    void loadFromModel(const Model::TTextSprite &textModel, void *context = nullptr);

    std::string text() const;
    void setText(const std::string &text);

    Color textColor() const;
    void setTextColor(const Color &textColor);

    float textWidth() const;
    float textHeight() const;

    void arbitrarilyRender(const TVector2 &offset);
    void arbitrarilyRender(float scale);

private:
    TText mText;
    std::string mStdText;
    Color mTextColor;

    float mTextWidth;
    float mTextHeight;

    void prepareDrawableText();

    // TSprite floaterface
public:
    void step() override;
    void render() override;
    TRect getCurrentRect() override;
};

#endif // TEXTSPRITE_H
