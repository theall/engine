#include "sprite.h"
#include <algorithm>
#include <gameutils/model/scene/layer/sprite.h>

bool TSprite::mDebugEnable = false;
TVector2 TSprite::mMaxSpeed = TVector2(99, 99);

TSprite::TSprite(const SpriteType &spriteType) :
    mSpriteType(spriteType)
  , mZValue(0)
  , mInAir(false)
{

}

TSprite::~TSprite()
{

}

void TSprite::loadFromModel(const Model::TSprite &model, void *context)
{
    (void)context;
    mPos = model.position();
    mMoveModel.loadFromModel(model.moveModel(), context);
}

SpriteType TSprite::getSpriteType() const
{
    return mSpriteType;
}

void TSprite::setDebugFlag(bool enable)
{
    mDebugEnable = enable;
}

void TSprite::step()
{
    if(mMoveModel.isValid())
        mSpeed = mMoveModel.getNextSpeed();
    mPos += mSpeed;
}

void TSprite::render()
{

}

GameEvent TSprite::getEvent() const
{
    return mEvent;
}

void TSprite::setEvent(const GameEvent &event)
{
    mEvent = event;
}

bool TSprite::getInAir() const
{
    return mInAir;
}

void TSprite::setInAir(bool inAir)
{
    if(mInAir == inAir)
        return;

    mInAir = inAir;
    if(!mInAir)
    {
        mSpeed.x = 0;
        mSpeed.y = 0;
    }
}

TVector2 TSprite::getMaxSpeed()
{
    return mMaxSpeed;
}

void TSprite::setMaxSpeed(const TVector2 &maxSpeed)
{
    mMaxSpeed.x = fabs(maxSpeed.x);
    mMaxSpeed.y = fabs(maxSpeed.y);
}

int TSprite::getZValue() const
{
    return mZValue;
}

void TSprite::setZValue(int value)
{
    mZValue = value;
}

void TSprite::moveTo(const TVector2 &newPos)
{
    mPos = newPos;
}

void TSprite::moveTo(float newX, float newY)
{
    mPos.x = newX;
    mPos.y = newY;
}

void TSprite::move(const TVector2 &dPos)
{
    mPos += dPos;
}

void TSprite::move(float dx, float dy)
{
    mPos.x += dx;
    mPos.y += dy;
}

TVector2 TSprite::getSpeed() const
{
    return mSpeed;
}

void TSprite::setSpeed(const TVector2 &speed)
{
    mSpeed = speed;
}

void TSprite::addSpeed(const TVector2 &speed)
{
    mSpeed += speed;

    if(fabs(mSpeed.x) > mMaxSpeed.x)
        mSpeed.x = mSpeed.x>0?mMaxSpeed.x:-mMaxSpeed.x;
    if(fabs(mSpeed.y) > mMaxSpeed.y)
        mSpeed.y = mSpeed.y>0?mMaxSpeed.y:-mMaxSpeed.y;
}

void TSprite::clearSpeed()
{
    mSpeed.x = 0;
    mSpeed.y = 0;
}

TVector2 TSprite::getPos() const
{
    return mPos;
}

void TSprite::setPos(const TVector2 &pos)
{
    mPos = pos;
}

void TSprite::setPos(float x, float y)
{
    mPos.x = x;
    mPos.y = y;
}
