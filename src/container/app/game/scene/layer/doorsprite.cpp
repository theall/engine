#include "doorsprite.h"

#include <gameutils/model/scene/layer/doorsprite.h>

extern TGraphics *gfx;
TDoorSprite::TDoorSprite() :
    TSprite(ST_IMAGE)
{

}

TDoorSprite::TDoorSprite(const Model::TDoorSprite &spriteModel, void *context) :
    TSprite(ST_IMAGE)
{
    loadFromModel(spriteModel, context);
}

TDoorSprite::~TDoorSprite()
{

}

void TDoorSprite::loadFromModel(const Model::TDoorSprite &spriteModel, void *context)
{
    (void)context;
    mEntryRect = spriteModel.entryRect();
    mExitRect = spriteModel.exitRect();
}

TRect TDoorSprite::entryRect() const
{
    return mEntryRect;
}

TRect TDoorSprite::exitRect() const
{
    return mExitRect;
}

void TDoorSprite::step()
{

}

void TDoorSprite::render()
{
    gfx->drawRect(mEntryRect.x, mEntryRect.y, mEntryRect.w, mEntryRect.h);
    gfx->drawRect(mExitRect.x, mExitRect.y, mExitRect.w, mExitRect.h);
}

TRect TDoorSprite::getCurrentRect()
{
    return mEntryRect;
}
