#ifndef LAYER_H
#define LAYER_H

#include "layer/sprite.h"

namespace Model {
class TLayer;
}

class TCamera;

class TLayer : public TRunnable
{
public:
    TLayer();
    TLayer(const Model::TLayer &layer, void *context = nullptr);
    virtual ~TLayer();

    void loadFromModel(const Model::TLayer &layer, void *context = nullptr);
    void step() override;
    void render() override;

    void addSprite(TSprite *sprite);
    TSprite *getSprite(int index) const;

    TVector2 getSpeedFactor() const;
    void setSpeedFactor(const TVector2 &speedFactor);

    TCamera *getCamera() const;
    void setCamera(TCamera *camera);

    bool getEnableScale() const;
    void setEnableScale(bool enableScale);

    TVector2 getGravity() const;
    void setGravity(const TVector2 &gravity);

private:
    TVector2 mOffset;
    TVector2 mSpeedFactor;
    TVector2 mGravity;
    TSpriteList mSpriteList;
    TCamera *mCamera;
    bool mEnableScale;
};

typedef std::vector<TLayer*> TLayerList;

#endif // LAYER_H
