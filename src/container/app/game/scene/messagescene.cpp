#include "messagescene.h"
#include "layer/textsprite.h"
#include <sdk/engine.h>

extern TWindow *wnd;
TMessageScene::TMessageScene(const std::string &msg, int timeOut) :
    TScene(TScene::Overlay)
  , mTextSprite(new TTextSprite(msg))
{
    setTimeOutFrames(timeOut);
    mLayerList[0]->addSprite(mTextSprite);

    TWindowSize windowSize = wnd->getSize();
    mTextSprite->setPos(10, windowSize.height-mTextSprite->textHeight()-5);
    mTextSprite->setTextColor(Color(255,255,255,255));
}

TMessageScene::~TMessageScene()
{
    //delete mTextSprite;
}

void TMessageScene::setMessage(const std::string &msg, int timeOut)
{
    setTimeOutFrames(timeOut);
    mTextSprite->setText(msg);
}

void TMessageScene::render()
{
    if(!isTimeOut())
        TScene::render();
}
