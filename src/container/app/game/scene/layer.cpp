#include "layer.h"
#include "camera.h"
#include "layer/charactersprite.h"
#include "layer/imagesprite.h"
#include "layer/buttonsprite.h"
#include "layer/cursorsprite.h"
#include "layer/doorsprite.h"
#include "layer/terriansprite.h"
#include "layer/ghostsprite.h"
#include "../../controller/keyboardcontroller.h"

#include <gameutils/model/scene/layer.h>

extern TGraphics *gfx;

typedef std::pair<TAreaGroup *, TSprite *> AreaGroupPair;
typedef std::vector<AreaGroupPair> AreaGroupPairList;

#define FREE_AREAGROUPPAIRLIST(x) \
    for(auto a : x) \
        delete a.first; \
    x.clear()

bool orderObjectInZindex(TSprite *a, TSprite *b)
{
    return a->getZValue() < b->getZValue();
}

TLayer::TLayer() :
    mCamera(nullptr)
  , mEnableScale(true)
{

}

TLayer::TLayer(const Model::TLayer &layer, void *context) :
    mCamera(nullptr)
  , mEnableScale(true)
{
    loadFromModel(layer, context);
}

TLayer::~TLayer()
{
    //FREE_CONTAINER(mSpriteList);
}

void TLayer::loadFromModel(const Model::TLayer &layer, void *context)
{
    mOffset = layer.offset();
    mSpeedFactor = layer.speedFactor();
    bool controllerSeted = false;
    for(Model::TSprite *spriteModel : layer.spriteList())
    {
        SpriteType spriteType = spriteModel->type();
        TSprite *sprite = nullptr;
        if(spriteType == ST_CHARACTER) {
            TCharacterSprite *characterSprite = new TCharacterSprite(*(Model::TCharacterSprite*)spriteModel, context);
            if(!controllerSeted)
            {
                characterSprite->setController(TKeyboardController::instance());
                TKeyboardController::instance()->setPuppet(characterSprite);
                controllerSeted = true;
            }
            sprite = characterSprite;
        } else if(spriteType == ST_IMAGE) {
            sprite = new TImageSprite(*(Model::TImageSprite*)spriteModel, context);
        } else if(spriteType == ST_BUTTON) {
            sprite = new TButtonSprite(*(Model::TButtonSprite*)spriteModel, context);
        } else if(spriteType == ST_DOOR) {
            sprite = new TDoorSprite(*(Model::TDoorSprite*)spriteModel, context);
        } else if(spriteType == ST_TERRIAN) {
            sprite = new TTerrianSprite(*(Model::TTerrianSprite*)spriteModel, context);
        } else if(spriteType == ST_GHOST) {
            sprite = new TGhostSprite(*(Model::TGhostSprite*)spriteModel, context);
        } else {
            throw TException("Un-supported sprite type \"%s\"", SpriteTypeToString(spriteType).c_str());
        }
        if(sprite) {
            sprite->move(mOffset);
            mSpriteList.emplace_back(sprite);
        }
    }
}

void TLayer::step()
{
    // Compute collision
    AreaGroupPairList attackAreaGroupPairList;
    AreaGroupPairList undertakeAreaGroupPairList;
    AreaGroupPairList collideAreaGroupPairList;
    AreaGroupPairList terrianAreaGroupPairList;
    AreaGroupPairList characterTerrianAreaGroupPairList;
    for(TSprite *sprite : mSpriteList) {
        sprite->step();
        if(TCharacterSprite *characterSprite = dynamic_cast<TCharacterSprite *>(sprite))
        {
            TFrame *frame = characterSprite->getCurrentFrame();
            if(frame)
            {
                TVector2 spritePos = characterSprite->getPos() - frame->mAnchor;
                if(frame->mAttackAreaGroup.isValid()) {
                    attackAreaGroupPairList.emplace_back(std::make_pair(frame->mAttackAreaGroup.offset(spritePos), sprite));
                } else if(frame->mUndertakeAreaGroup.isValid()) {
                    undertakeAreaGroupPairList.emplace_back(std::make_pair(frame->mUndertakeAreaGroup.offset(spritePos), sprite));
                } else if(frame->mCollideAreaGroup.isValid()) {
                    collideAreaGroupPairList.emplace_back(std::make_pair(frame->mCollideAreaGroup.offset(spritePos), sprite));
                } else if(frame->mTerrianAreaGroup.isValid()) {
                    AreaGroupPair p = std::make_pair(frame->mTerrianAreaGroup.offset(spritePos), sprite);
                    terrianAreaGroupPairList.emplace_back(p);
                    characterTerrianAreaGroupPairList.emplace_back(p);
                }
            }
        } else if (TTerrianSprite *terrianSprite = dynamic_cast<TTerrianSprite *>(sprite)) {
            terrianAreaGroupPairList.emplace_back(std::make_pair(terrianSprite->getAreaGroup(), sprite));
        } else if (TGhostSprite *ghostSprite = dynamic_cast<TGhostSprite *>(sprite)) {
            attackAreaGroupPairList.emplace_back(std::make_pair(ghostSprite->getAreaGroup(), sprite));
        }
    }


    // Attack-undertack area
//    for(TAreaGroup *attackAreaGroup : attackAreaGroupPairList)
//    {
//        for(TAreaGroup *undertakeAreaGroup : undertakeAreaGroupPairList)
//        {
//            TCollideAreaPairList collidePairList = attackAreaGroup->getCollideAreaPairList(undertakeAreaGroup);

//            FREE_CONTAINER(collidePairList);
//        }
//    }

    // Collide-terrian area
    for(AreaGroupPair collideAreaGroupPair : collideAreaGroupPairList)
    {
        TAreaGroup *collideAreaGroup = collideAreaGroupPair.first;
        TSprite *sprite = collideAreaGroupPair.second;
        TVector2 characterSpeed = sprite->getSpeed();
        for(AreaGroupPair terrianAreaGroupPair : terrianAreaGroupPairList)
        {
            TCollideAreaPairList collidePairList = collideAreaGroup->getCollideAreaPairList(*terrianAreaGroupPair.first);
            if(!collidePairList.empty())
            {
                TCollideAreaPair *collideAreaPair = collidePairList.at(0);
                if(collideAreaPair->rect.w>=1 && collideAreaPair->rect.h>=1)
                {
                    TVector2 fixPos;
                    TRect rect1 = collideAreaPair->area1->rect();
                    TRect rect2 = collideAreaPair->area2->rect();
                    if(collideAreaPair->rect.w != rect1.w) {
                        if(rect1.hcenter() <= rect2.hcenter())
                            fixPos.x = -collideAreaPair->rect.w;
                        else
                            fixPos.x = collideAreaPair->rect.w;
                    } else if(collideAreaPair->rect.h != rect1.h) {
                        if(rect1.vcenter() <= rect2.vcenter())
                            fixPos.y = -collideAreaPair->rect.h;
                        else if(characterSpeed.y < 0)
                            fixPos.y = collideAreaPair->rect.h;
                    }
                    if(!fixPos.isNull())
                        sprite->move(fixPos);
    //                else
    //                    characterSprite->setEvent(GE_NONE);
                }

                FREE_CONTAINER(collidePairList);
            } else {
                sprite->setInAir(true);

                if(TCharacterSprite *characterSprite = dynamic_cast<TCharacterSprite *>(sprite))
                {
                    if(TFrame *currentFrame = characterSprite->getCurrentFrame())
                        if(!currentFrame->mAntiGravity)
                            sprite->addSpeed(mGravity);
                } else {
                        sprite->addSpeed(mGravity);
                }
            }
        }
    }

    FREE_AREAGROUPPAIRLIST(attackAreaGroupPairList);
    FREE_AREAGROUPPAIRLIST(undertakeAreaGroupPairList);
    FREE_AREAGROUPPAIRLIST(collideAreaGroupPairList);
    FREE_AREAGROUPPAIRLIST(characterTerrianAreaGroupPairList);
}

void TLayer::render()
{
    if(!mCamera)
        return;

    TRect rect = mCamera->rect();
    gfx->push();
    gfx->translate(-rect.x, -rect.y);
    for(TSprite *o : mSpriteList)
    {
        if(mCamera->contain(o))
            o->render();
    }
#ifdef DEBUG
    TRect viewRect = mCamera->viewRect();
    gfx->drawRect(viewRect.x+10, viewRect.y+10, viewRect.w-20, viewRect.h-20, Colorf(255,0,0,255));
#endif
    gfx->pop();
}

void TLayer::addSprite(TSprite *sprite)
{
    mSpriteList.emplace_back(sprite);
}

TSprite *TLayer::getSprite(int index) const
{
    if(index>=0 && index<(int)mSpriteList.size())
        return mSpriteList[index];
    return nullptr;
}

TVector2 TLayer::getSpeedFactor() const
{
    return mSpeedFactor;
}

void TLayer::setSpeedFactor(const TVector2 &speedFactor)
{
    mSpeedFactor = speedFactor;
}

TCamera *TLayer::getCamera() const
{
    return mCamera;
}

void TLayer::setCamera(TCamera *camera)
{
    mCamera = camera;
}

bool TLayer::getEnableScale() const
{
    return mEnableScale;
}

void TLayer::setEnableScale(bool enableScale)
{
    mEnableScale = enableScale;
}

TVector2 TLayer::getGravity() const
{
    return mGravity;
}

void TLayer::setGravity(const TVector2 &gravity)
{
    mGravity = gravity;
}
