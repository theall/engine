#ifndef MESSAGESCENE_H
#define MESSAGESCENE_H

#include "scene.h"

class TTextSprite;

class TMessageScene : public TScene
{
public:
    TMessageScene(const std::string &msg = std::string(), int timeOut = 180);
    ~TMessageScene();

    void setMessage(const std::string &msg, int timeOut = 180);

    void render() override;

private:
    TTextSprite *mTextSprite;
};

#endif // MESSAGESCENE_H
