#include "queryscene.h"

#include <sdk/engine.h>

extern TWindow *wnd;

TQueryScene::TQueryScene() :
    TScene()
{

}

TQueryScene::TQueryScene(const std::string &msg) :
    TScene()
{
    mTextSprite.setText(msg);

    TVector2 imageSize = mBackgroundSprite.getSize();
    TWindowSize windowSize = wnd->getSize();
    TRect rect(0, 0, windowSize.width, windowSize.height);
    mBackgroundSprite.setPos(rect.hcenter(imageSize.x), rect.vcenter(imageSize.y));
}

TQueryScene::~TQueryScene()
{

}
