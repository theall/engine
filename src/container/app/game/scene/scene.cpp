#include "scene.h"
#include "layer/textsprite.h"
#include "../resourcecache.h"
#include "../../controller/keyboardcontroller.h"

#include <gameutils/model/scene.h>

extern TFileSystem *fs;
extern TGraphics *gfx;
extern TMouse *mouse;

TScene::TScene(Type type) :
    mMonopolize(false)
  , mType(type)
  , mTimeOutFrames(-1)
  , mQuakeTime(0)
  , mQuakeAmplitude(1)
{
    mLayerList.emplace_back(new TLayer());
}

TScene::TScene(const std::string &file, void *context) :
    mMonopolize(false)
  , mType(Monopolize)
  , mTimeOutFrames(-1)
  , mQuakeTime(0)
  , mQuakeAmplitude(1)
{
    (void)context;

    TFileData *fileData = fs->newFileData(file);
    if(!fileData)
        return;

    std::string currentSource = fs->getSource();
    std::string fileFullName = currentSource + "/" + file;
    std::string sceneRoot = extractPath(fileFullName);
    Model::TScene sceneModel;
    fs->setSource(sceneRoot);
    try {
        sceneModel.loadFromData(fileData->getData(), fileData->getSize());
        fileData->release();
    } catch(std::string s) {
        fileData->release();
        throw format("Failed to load %s, \n\t%s", fileFullName.c_str(), s.c_str());
    }

    loadFromModel(sceneModel, context);
    fs->setSource(currentSource);
}

TScene::TScene(const Model::TScene &sceneModel, void *context) :
    mMonopolize(false)
  , mType(Monopolize)
  , mTimeOutFrames(-1)
  , mQuakeTime(0)
  , mQuakeAmplitude(1)
{
    loadFromModel(sceneModel, context);
}

TScene::~TScene()
{
    //FREE_CONTAINER(mLayerList);
    onPoped();
}

void TScene::loadFromModel(const Model::TScene &sceneModel, void *context)
{
    mMainLayerIndex = sceneModel.mainLayerIndex();
    mCursorSprite.loadFromModel(sceneModel.cursor(), this);
    mSoundSet.loadFromModel(sceneModel.soundSet(), this);
    mCamera.loadFromModel(sceneModel.camera());
    mFps = sceneModel.fps();
    mGravity = sceneModel.gravity();

    FREE_CONTAINER(mLayerList);
    for(Model::TLayer *layerModel : sceneModel.layerList())
    {
        TLayer *layer = new TLayer(*layerModel, context);
        layer->setGravity(mGravity);
        layer->setCamera(&mCamera);
        mLayerList.emplace_back(layer);
    }

    TCursor *cursor = mCursorSprite.cursor();
    if(cursor)
    {
        mouse->setCursor(cursor);
    }
    mBackgroundColor = toColorf(sceneModel.backgroundColor());
    TSprite *sprite = static_cast<TSprite *>(TKeyboardController::instance()->getPuppet());
    if(sprite)
        mCamera.addSprite(sprite);
}

void TScene::render()
{
    if(mType == Monopolize)
        gfx->clear(mBackgroundColor);

    if(mQuakeTime>0 && mQuakeAmplitude>0) {
        gfx->push();
        float rx = getRandValue(-mQuakeAmplitude, mQuakeAmplitude);
        float ry = getRandValue(-mQuakeAmplitude, mQuakeAmplitude);
        gfx->translate(rx, ry);
    }

    for(TLayer *layer : mLayerList)
        layer->render();

    if(mQuakeTime>0 && mQuakeAmplitude>0) {
        gfx->pop();
    }
    mSoundSet.render();

#ifdef DEBUG
    TRect cameraRect = mCamera.rect();
    gfx->drawRect(0, 0, cameraRect.w, cameraRect.h);
#endif
}

bool TScene::monopolize() const
{
    return mMonopolize;
}

void TScene::setMonopolize(bool monopolize)
{
    mMonopolize = monopolize;
}

int TScene::timeOutFrames() const
{
    return mTimeOutFrames;
}

void TScene::setTimeOutFrames(int timeOutFrames)
{
    mTimeOutFrames = timeOutFrames;
}

bool TScene::isTimeOut() const
{
    return mTimeOutFrames==0;
}

Colorf TScene::backgroundColor() const
{
    return mBackgroundColor;
}

TScene::Type TScene::type() const
{
    return mType;
}

void TScene::setType(const Type &type)
{
    mType = type;
}

void TScene::prepareDestroy()
{
    TSource *currentSource = mSoundSet.currentSource();
    if(currentSource)
        currentSource->stop();
}

void TScene::onPushed()
{
    mSoundSet.shuttle();
}

void TScene::onPoped()
{
    mSoundSet.stop();
}

void TScene::moveCamera(float dx, float dy)
{
    mCamera.move(dx, dy);
}

void TScene::moveCamera(const TVector2 &d)
{
    mCamera.move(d);
}

void TScene::setQuake(int quakeTime, int quakeAmplitude)
{
    if(quakeTime<=0 || quakeAmplitude<=0)
        return;

    mQuakeTime = quakeTime;
    mQuakeAmplitude = quakeAmplitude;
}

int TScene::fps() const
{
    return mFps;
}

void TScene::setFps(int fps)
{
    mFps = fps;
}

TCamera *TScene::camera()
{
    return &mCamera;
}

void TScene::step()
{
    if(mTimeOutFrames > 0)
        mTimeOutFrames--;

    if(mQuakeTime > 0)
        mQuakeTime--;

    for(TLayer *layer : mLayerList)
        layer->step();

    mCamera.step();
    mSoundSet.step();
}
