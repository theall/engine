#ifndef QUERYSCENE_H
#define QUERYSCENE_H

#include "scene.h"
#include "layer/textsprite.h"
#include "layer/imagesprite.h"
#include "layer/buttonsprite.h"

class TQueryScene : public TScene
{
public:
    TQueryScene();
    TQueryScene(const std::string &msg);
    ~TQueryScene();

private:
    TImageSprite mBackgroundSprite;
    TTextSprite mTextSprite;
    TButtonSprite mButtonOkSprite;
    TButtonSprite mButtonCancelSprite;
};

#endif // QUERYSCENE_H
