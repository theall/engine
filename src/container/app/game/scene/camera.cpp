#include "camera.h"

#include <sdk/engine.h>
#include <gameutils/model/scene/camera.h>

extern TGraphics *gfx;
extern TWindow *wnd;
TCamera::TCamera() :
    mScale(1.0)
  , mDirection(SD_EAST)
{

}

TCamera::TCamera(const Model::TCamera &cameraModel, void *context) :
    mScale(1.0)
  , mDirection(SD_EAST)
{
    loadFromModel(cameraModel, context);
}

TCamera::~TCamera()
{

}

void TCamera::loadFromModel(const Model::TCamera &cameraModel, void *context)
{
    (void)context;
    setRect(cameraModel.rect());
}

TRect TCamera::rect() const
{
    return mRect;
}

void TCamera::setRect(const TRect &rect)
{
    mRect = rect;
    updateViewRect();
}

void TCamera::addSprite(TSprite *sprite)
{
    if(sprite) {
        for(TSprite *s : mSpriteList)
        {
            if(s == sprite)
                return;
        }
        mSpriteList.emplace_back(sprite);
    }
}

void TCamera::removeSprite(TSprite *sprite)
{
    if(sprite) {
        int index = -1;
        bool got = false;
        for(TSprite *s : mSpriteList)
        {
            index++;
            if(s == sprite)
            {
                got = true;
                break;
            }
        }
        if(got)
            mSpriteList.erase(mSpriteList.begin()+index);
    }
}

void TCamera::step()
{
    float minX = 3.402823466e+38F;
    float minY = 3.402823466e+38F;
    float maxX = 1.175494351e-38F;
    float maxY = 1.175494351e-38F;
    for(TSprite *sprite : mSpriteList)
    {
        TVector2 pos = sprite->getPos();
        if(pos.x < minX)
            minX = pos.x;
        if(pos.x > maxX)
            maxX = pos.x;
        if(pos.y < minY)
            minY = pos.y;
        if(pos.y > maxY)
            maxY = pos.y;
    }
    if(!mSpriteList.empty())
    {
        TSprite *mainSprite = mSpriteList.at(0);
        TVector2 mainSpritePos = mainSprite->getPos();
        mRect.x = mainSpritePos.x - mRect.w/2;
        mRect.y = mainSpritePos.y - mRect.h/2;
        updateViewRect();
    }
}

void TCamera::render()
{

}

bool TCamera::contain(const TRect &rect, bool restrictCheck)
{
    TRect interRect = mViewRect.intersection(rect);
    if(restrictCheck)
        return interRect==mViewRect || interRect==rect;
    return !interRect.isEmpty();
}

bool TCamera::contain(TSprite *sprite, bool restrictCheck)
{
    if(!sprite)
        return false;
    return contain(sprite->getCurrentRect(), restrictCheck);
}

bool TCamera::move(float dx, float dy)
{
    mRect.x += dx;
    mRect.y += dy;
    updateViewRect();
    return true;
}

bool TCamera::move(const TVector2 &d)
{
    mRect.x += d.x;
    mRect.y += d.y;
    updateViewRect();
    return true;
}

TRect TCamera::viewRect() const
{
    return mViewRect;
}

void TCamera::updateViewRect()
{
    if(!gfx || !wnd)
        return;

    TWindowSize windowSize = wnd->getSize();
    TMatrix4 m = gfx->getMatrix().getReverseMatrix();
    float x1 = 0;
    float x2 = 0;
    float y1 = 0;
    float y2 = 0;
    m.getXY(0, 0, x1, y1);
    m.getXY(windowSize.width, windowSize.height, x2, y2);
    mViewRect.w = x2 - x1;
    mViewRect.h = y2 - y1;
    mViewRect.x = mRect.hcenter(mViewRect.w);
    mViewRect.y = mRect.vcenter(mViewRect.h);
}

void TCamera::clear()
{
    mSpriteList.clear();
}
