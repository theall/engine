#ifndef SCENE_H
#define SCENE_H

#include "layer.h"
#include "camera.h"
#include "../sound/soundset.h"
#include "layer/cursorsprite.h"

#include <sdk/engine.h>

class TCursorSprite;

namespace Model {
class TScene;
}

class TScene : public TRunnable
{
public:
    enum Type
    {
        Overlay,
        Monopolize
    };

    TScene(Type type = Monopolize);
    TScene(const std::string &file, void *context = nullptr);
    TScene(const Model::TScene &sceneModel, void *context = nullptr);
    virtual ~TScene();

    void loadFromModel(const Model::TScene &sceneModel, void *context = nullptr);

    virtual void step() override;
    virtual void render() override;

    bool monopolize() const;
    void setMonopolize(bool monopolize);

    int timeOutFrames() const;
    void setTimeOutFrames(int timeOutFrames);
    bool isTimeOut() const;

    Colorf backgroundColor() const;

    TScene::Type type() const;
    void setType(const Type &type);

    void prepareDestroy();

    void onPushed();
    void onPoped();

    void moveCamera(float dx, float dy);
    void moveCamera(const TVector2 &d);

    void setQuake(int quakeTime, int quakeAmplitude = 1);

    int fps() const;
    void setFps(int fps);

    TCamera *camera();

protected:
    bool mMonopolize;
    Type mType;
    int mTimeOutFrames;
    int mMainLayerIndex;
    Colorf mBackgroundColor;
    TSoundSet mSoundSet;
    TCursorSprite mCursorSprite;
    TLayerList mLayerList;
    TCamera mCamera;
    int mQuakeTime;
    int mQuakeAmplitude;
    int mFps;
    TVector2 mGravity;
};

typedef std::vector<TScene*> TSceneList;

#endif // SCENE_H
