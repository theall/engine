#ifndef CAMERABLOCK_H
#define CAMERABLOCK_H


class TCameraBlock
{
public:
    enum Direction
    {
        Horizontal,
        Vertical
    };

    TCameraBlock();
    ~TCameraBlock();

    Direction direction() const;

private:
    Direction mDirection;
};

#endif // CAMERABLOCK_H
