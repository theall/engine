#include "game.h"
#include "resourcecache.h"
#include "scene/messagescene.h"

#include <gameutils/model/game.h>

extern TGraphics *gfx;
extern TFileSystem *fs;
extern TWindow *wnd;

TGame::TGame() :
    mSceneManager(new TSceneManager)
  , mDefaultFont(nullptr)
  , mMessageScene(nullptr)
{

}

TGame::TGame(const Model::TGame &gameModel) :
    mSceneManager(new TSceneManager)
  , mDefaultFont(nullptr)
  , mMessageScene(nullptr)
{
    loadFromModel(gameModel);
}

TGame::~TGame()
{
    FREE_CONTAINER(mSceneList);

    if(mMessageScene)
    {
        delete mMessageScene;
        mMessageScene = nullptr;
    }
}

bool TGame::loadFromFile(const std::string &file)
{
    (void)file;
    return false;
}

bool TGame::loadFromModel(const Model::TGame &gameModel)
{
    mResolution = gameModel.resolution();

    TResourceCache *resourceCache = TResourceCache::instance();

    FREE_CONTAINER(mFontList);
    for(std::string file : gameModel.fontList())
    {
        TFont *font = resourceCache->loadFont(file);
        if(font)
            mFontList.emplace_back(font);
    }
    if(mFontList.size() > 0)
    {
        mDefaultFont = mFontList.at(0);
        mDefaultFont->setPixelSize(22);
        gfx->setFont(mDefaultFont);
    }

    for(std::string file : gameModel.characterPathList())
    {
        resourceCache->loadCharacter(file);
    }

    FREE_CONTAINER(mSceneList);
    for(std::string file : gameModel.scenePathList())
    {
        TScene *scene = new TScene(file, this);
        mSceneList.emplace_back(scene);
    }

    TScene *entryScene = mSceneList.at(gameModel.entrySceneIndex());
    if(!entryScene)
        throw TException("No entry scene.");

    mSceneStack.push(entryScene);
    if(!mMessageScene)
        mMessageScene = new TMessageScene(" ");
    return true;
}

std::string TGame::name() const
{
    return mName;
}

void TGame::step()
{
    mSceneStack.step();
    mMessageScene->step();
}

void TGame::render()
{
    mSceneStack.render();
    mMessageScene->render();
}

void TGame::reset()
{
#ifdef DEBUG
//    game.clearSpeed();
//    game.moveTo(400, 300);
#endif
}

void TGame::showMsg(const std::string &msg, int timeOut)
{
    if(!mMessageScene)
        throw TException("Message scene not created yet.");

    mMessageScene->setMessage(msg, timeOut);
}

void TGame::showQueryDialog(const std::string &msg)
{
    (void)msg;
}
