#ifndef GAMEEVENT_H
#define GAMEEVENT_H

enum GameEvent
{
    GE_SCENE_GOTO,
    GE_SCENE_RETURN,
};

class TGameEvent
{
public:
    TGameEvent();
};

#endif // GAMEEVENT_H
