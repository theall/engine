#include "sounditem.h"
#include "../resourcecache.h"


#include <sdk/engine.h>

TSoundItem::TSoundItem() :
    mSource(nullptr)
  , mLoopCount(0)
  , mDelay(0)
{
    reset();
}

TSoundItem::TSoundItem(const TSoundItem &soundItem)
{
    reset();
    mSource = soundItem.source();
    mLoopCount = soundItem.loopCount();
    mDelay = soundItem.delay();
}

TSoundItem::TSoundItem(const Model::TSoundItem &soundItemModel, void *context) :
    mSource(nullptr)
  , mLoopCount(0)
  , mDelay(0)
{
    reset();
    loadFromModel(soundItemModel, context);
}

TSoundItem::~TSoundItem()
{

}

void TSoundItem::loadFromModel(const Model::TSoundItem &soundItemModel, void *context)
{
    (void)context;

    if(mSource)
        mSource->release();

    mSource = TResourceCache::instance()->loadSound(soundItemModel.fileName());
    mLoopCount = soundItemModel.loopCount();
    mDelay = soundItemModel.delay();
}

void TSoundItem::step()
{
    if(!mSource)
        return;

    mFinishedTriggered = false;
    if(mStepFrames==mDelay) {
        // Delay ok
        mNeedPlay = true;
    } else if(mStepFrames>mDelay && mSource->isStopped()) {
        if(mLoopCount<0 || mRewindCount<mLoopCount)
        {
            mNeedPlay = true;
            mRewindCount++;
        } else if (mRewindCount==mLoopCount) {
            mFinishedTriggered = true;
        }
    }
    mStepFrames++;
}

void TSoundItem::render()
{
    if(mNeedPlay) {
        mNeedPlay = false;
        mSource->play();
    }
}

void TSoundItem::reset()
{
    mRewindCount = 0;
    mStepFrames = 0;
    mFinishedTriggered = false;
    mNeedPlay = false;
}

TSource *TSoundItem::source() const
{
    return mSource;
}

int TSoundItem::loopCount() const
{
    return mLoopCount;
}

void TSoundItem::setLoopCount(int loopCount)
{
    mLoopCount = loopCount;
}

int TSoundItem::delay() const
{
    return mDelay;
}

void TSoundItem::setDelay(int delay)
{
    mDelay = delay;
}

bool TSoundItem::finishedTriggered() const
{
    return mFinishedTriggered;
}

bool TSoundItem::isValid() const
{
    return mSource!=nullptr;
}

void TSoundItem::stop()
{
    if(mSource)
        mSource->stop();
}
