#include "soundset.h"

#include <iostream>
#include <sdk/common/exception.h>
#include <gameutils/model/sound/soundset.h>

TSoundSet::TSoundSet() :
    mModel(SSM_ORDERED)
  , mCurrentItemIndex(-1)
  , mAutoPlay(false)
  , mCurrentSoundItem(nullptr)
  , mCurrentSource(nullptr)
{

}

TSoundSet::TSoundSet(const Model::TSoundSet &soundSetModel, void *context) :
    mModel(SSM_ORDERED)
  , mCurrentItemIndex(-1)
  , mAutoPlay(false)
  , mCurrentSoundItem(nullptr)
  , mCurrentSource(nullptr)
{
    loadFromModel(soundSetModel, context);
}

TSoundSet::~TSoundSet()
{
    FREE_CONTAINER(mSoundItemList);
}

void TSoundSet::loadFromModel(const Model::TSoundSet &soundSetModel, void *context)
{
    mModel = soundSetModel.soundSetModel();
    mAutoPlay = soundSetModel.autoPlay();
    FREE_CONTAINER(mSoundItemList);
    for(Model::TSoundItem *soundItemModel : soundSetModel.soundItemList())
    {
        TSoundItem *soundItem = new TSoundItem(*soundItemModel, context);
        if(soundItem->isValid())
            mSoundItemList.emplace_back(soundItem);
        else
            delete soundItem;
    }
    mCurrentItemIndex = -1;
}

void TSoundSet::step()
{
    if(mCurrentSoundItem)
    {
        // Play end?
        if(mCurrentSoundItem->finishedTriggered())
        {
            // Reset to enable play next time
            mCurrentSoundItem->reset();

            // Only work in auto play mode
            mCurrentSoundItem = mAutoPlay?getNextSoundItem():nullptr;
            if(!mCurrentSoundItem)
                return;
        }
        mCurrentSoundItem->step();
    }
}

void TSoundSet::render()
{
    if(mCurrentSoundItem)
        mCurrentSoundItem->render();
}

TSource *TSoundSet::currentSource() const
{
    return mCurrentSource;
}

void TSoundSet::operator =(const TSoundSet &soundSet)
{
    mModel = soundSet.model();
    mCurrentItemIndex = soundSet.currentItemIndex();
    mCurrentSource = soundSet.currentSource();
    mAutoPlay = soundSet.getAutoPlay();

    FREE_CONTAINER(mSoundItemList);
    for(TSoundItem *soundItem : soundSet.soundItemList())
    {
        mSoundItemList.emplace_back(new TSoundItem(*soundItem));
    }
}

void TSoundSet::reset()
{
    for(TSoundItem *soundItem : mSoundItemList)
    {
        soundItem->reset();
    }
}

bool TSoundSet::isValid() const
{
    return mSoundItemList.size()>0;
}

bool TSoundSet::getAutoPlay() const
{
    return mAutoPlay;
}

void TSoundSet::shuttle()
{
    if(mCurrentSoundItem) {
        mCurrentSoundItem->reset();
        mCurrentSoundItem->stop();
    }
    mCurrentSoundItem = getNextSoundItem();
}

void TSoundSet::stop()
{
    if(mCurrentSoundItem)
        mCurrentSoundItem->stop();
}

TSoundItem *TSoundSet::getNextSoundItem()
{
    int soundItemCount = mSoundItemList.size();
    if(soundItemCount <= 0)
        return nullptr;
    TSoundItem *soundItem = nullptr;
    if(mModel == SSM_ORDERED)
    {
        mCurrentItemIndex++;
        if(mCurrentItemIndex >= soundItemCount)
            mCurrentItemIndex = 0;
    } else if(mModel == SSM_RANDOM) {
        mCurrentItemIndex = rand()%soundItemCount;
    }
    soundItem = mSoundItemList.at(mCurrentItemIndex);

    return soundItem;
}

SoundSetModel TSoundSet::model() const
{
    return mModel;
}

int TSoundSet::currentItemIndex() const
{
    return mCurrentItemIndex;
}

TSoundItemList TSoundSet::soundItemList() const
{
    return mSoundItemList;
}
