#ifndef SOUNDITEM_H
#define SOUNDITEM_H

#include <vector>
#include "../base/runnable.h"

class TSource;

namespace Model {
class TSoundItem;
}
class TSoundItem : public TRunnable
{
public:
    TSoundItem();
    TSoundItem(const TSoundItem &soundItem);
    TSoundItem(const Model::TSoundItem &soundItemModel, void *context = nullptr);
    virtual ~TSoundItem();

    void loadFromModel(const Model::TSoundItem &soundItemModel, void *context = nullptr);

    void step() override;
    void render() override;

    void reset();

    TSource *source() const;

    int loopCount() const;
    void setLoopCount(int loopCount);

    int delay() const;
    void setDelay(int delay);

    bool finishedTriggered() const;
    bool isValid() const;

    void stop();

private:
    TSource *mSource;
    int mLoopCount;
    int mDelay;
    int mStepFrames;
    int mRewindCount;
    bool mFinishedTriggered;
    bool mNeedPlay;
};

typedef std::vector<TSoundItem*> TSoundItemList;

#endif // SOUNDITEM_H
