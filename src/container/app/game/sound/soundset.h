#ifndef MODEL_SOUNDSET_H
#define MODEL_SOUNDSET_H

#include "sounditem.h"
#include <gameutils/base/variable.h>

namespace Model {
class TSoundSet;
}

class TSoundSet : public TRunnable
{
public:
    TSoundSet();
    TSoundSet(const Model::TSoundSet &model, void *context = nullptr);
    virtual ~TSoundSet();

    void loadFromModel(const Model::TSoundSet &model, void *context = nullptr);

    void step() override;
    void render() override;

    TSource *currentSource() const;
    SoundSetModel model() const;
    int currentItemIndex() const;
    TSoundItemList soundItemList() const;

    void operator =(const TSoundSet &soundSet);
    void reset();
    bool isValid() const;

    bool getAutoPlay() const;
    void shuttle();
    void stop();

private:
    SoundSetModel mModel;
    int mCurrentItemIndex;
    bool mAutoPlay;
    TSoundItemList mSoundItemList;
    TSoundItem *mCurrentSoundItem;
    TSource *mCurrentSource;

    TSoundItem *getNextSoundItem();
};

typedef std::vector<TSoundSet*> TSoundSetList;
#endif // MODEL_SOUNDSET_H
