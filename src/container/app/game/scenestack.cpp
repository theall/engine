#include "scenestack.h"
#include "../app.h"
#include <sdk/engine.h>

extern TAudio *ao;
extern TGraphics *gfx;
extern TApp *g_app;

TSceneStack::TSceneStack()
{

}

TSceneStack::~TSceneStack()
{

}

void TSceneStack::push(TScene *scene)
{
    if(!scene)
        throw format("Try to push null scene into stack.");

    mSceneList.emplace_back(scene);
    scene->onPushed();
    update();
}

void TSceneStack::pop()
{
    TScene *lastScene = getLastScene();
    if(lastScene)
    {
        mSceneList.pop_back();
        lastScene->onPoped();
        delete lastScene;
    }
    update();
}

void TSceneStack::clear()
{
    mSceneList.clear();
    update();
}

bool TSceneStack::isEmpty() const
{
    return mSceneList.empty();
}

bool TSceneStack::isMonopolize() const
{
    int sceneCount = mSceneList.size();
    return sceneCount==1 || (sceneCount>0&&mSceneList[0]->monopolize());
}

void TSceneStack::step()
{
    // Remove time out scenes
//    TSceneList newSceneList;
//    for(TScene *scene : mSceneList)
//    {
//        if(!scene->isTimeOut())
//            newSceneList.emplace_back(scene);
//        else
//        {
//            delete scene;
//        }
//    }
//    mSceneList = newSceneList;

    int sceneCount = mSceneList.size();
    if(sceneCount <= 0)
        return;

    TScene *lastScene = mSceneList.at(sceneCount-1);
    if(lastScene->monopolize())
    {
        lastScene->step();
    } else {
        for(TScene *scene : mSceneList)
            scene->step();
    }
}

void TSceneStack::render()
{
    for(TScene *scene : mSceneList)
        scene->render();
}

void TSceneStack::update()
{
    Colorf color(0,0,0,255);
    TScene *lastScene = getLastScene();
    if(lastScene) {
        color = lastScene->backgroundColor();
        g_app->setFps(lastScene->fps());
    }
    gfx->reset();
    gfx->setBackgroundColor(color);
    gfx->clear(color);
    gfx->origin();
}

TScene *TSceneStack::getLastScene() const
{
    int sceneCount = mSceneList.size();
    return sceneCount>0?mSceneList.at(sceneCount-1):nullptr;
}
