#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "scenestack.h"

class TSceneManager
{
public:
    TSceneManager();
    ~TSceneManager();

    int currentSceneIndex() const;
    void setCurrentSceneIndex(int currentSceneIndex);

    TScene *getCurrentScene() const;
    TSceneStack *getSceneStack() const;

private:
    int mCurrentSceneIndex;
    TSceneList mSceneList;
    TSceneStack *mSceneStack;
};

#endif // SCENEMANAGER_H
