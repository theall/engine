#ifndef RESOURCECACHE_H
#define RESOURCECACHE_H

#include "character/character.h"

class TResourceCache
{
    DECLARE_INSTANCE(TResourceCache)

public:
    TResourceCache();
    ~TResourceCache();

    TCharacter *loadCharacter(const std::string &fileName);
    TFont *loadFont(const std::string &fileName);
    TSource *loadSound(const std::string &fileName);
    TDrawableImage *loadImage(const std::string &fileName);

    std::string characterPath() const;
    void setCharacterPath(const std::string &characterPath);

    std::string soundPath() const;
    void setSoundPath(const std::string &soundPath);

    std::string fontPath() const;
    void setFontPath(const std::string &fontPath);

private:
    std::string mCharacterPath;
    std::string mSoundPath;
    std::string mFontPath;

    std::map<std::string, TCharacter*> mCharacterPathMap;
    std::map<std::string, TCharacter*> mCharacterUuidMap;
    std::map<std::string, TDrawableImage*> mImageMap;
    std::map<std::string, TFont*> mFontMap;
    std::map<std::string, TSource*> mSoundMap;

    TFont *findFont(const std::string &fileName);
    TSource *findSound(const std::string &fileName);
    TDrawableImage *findImage(const std::string &fileName);
    TCharacter *findCharacterByFilePath(const std::string &file);
    TCharacter *findCharacterByUuid(const std::string &uuid);
};

#endif // RESOURCECACHE_H
