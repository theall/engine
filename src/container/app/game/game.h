#ifndef GAME_H
#define GAME_H

#include <string>
#include <map>

#include "scenemanager.h"

namespace Model {
class TGame;
}
class TMessageScene;

class TGame : public TRunnable
{
public:
    TGame();
    TGame(const Model::TGame &gameModel);
    virtual ~TGame();

    bool loadFromFile(const std::string &file);
    bool loadFromModel(const Model::TGame &gameModel);

    std::string name() const;
    void step() override;
    void render() override;

    void reset();
    void showMsg(const std::string &msg, int timeOut = 180);
    void showQueryDialog(const std::string &msg);

private:
    std::string mFilePath;
    std::string mName;
    TVector2 mResolution;
    std::vector<TFont*> mFontList;
    std::vector<TScene*> mSceneList;
    TSceneManager *mSceneManager;
    TSceneStack mSceneStack;
    TFont *mDefaultFont;
    TMessageScene *mMessageScene;
};

#endif // GAME_H
