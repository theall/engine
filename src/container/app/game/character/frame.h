#ifndef SPRITE_FRAME_H
#define SPRITE_FRAME_H

#include <vector>
#include <sdk/engine.h>
#include <gameutils/base/rect.h>
#include <gameutils/base/vector.h>
#include <gameutils/model/character/hand.h>
#include <gameutils/model/character/frame.h>

#include "../base/area/areagroup.h"
#include "../sound/soundset.h"

class TFrame
{
public:
    TDrawableImage *mImage;
    TAreaGroup mAttackAreaGroup;
    TAreaGroup mUndertakeAreaGroup;
    TAreaGroup mCollideAreaGroup;
    TAreaGroup mTerrianAreaGroup;
    TSoundSet mSoundSet;
    Model::THandList mHandList;
    TRect mFootRect;
    TVector2 mAnchor;
    TVector2 mVector;
    bool mAntiGravity;
    int mDuration;
    bool mIsKeyFrame;
    InterruptType mInterruptType;
    std::vector<TVector2> mVectorTable;

    TFrame();
    TFrame(Model::TFrame *frame);
    TFrame(const TFrame &frame);
    ~TFrame();

    TFrame *generateMirrorFrame() const;
    const TSoundSet *getSoundSet() const;

    bool isEnd() const;
    void reset();
    void step();
    TVector2 currentVector() const;
    bool isFirst() const;

private:
    int mIndex;
    bool mIsEnd;
};

typedef std::vector<TFrame*> TFramesList;

#endif // SPRITE_FRAME_H
