#include "actiontrigger.h"

TActionTrigger::TActionTrigger()
{

}

TActionTrigger::TActionTrigger(const TActionTrigger &actionTrigger)
{
    mEvent = actionTrigger.event();
    mValue = actionTrigger.value();
    mContext = actionTrigger.context();
}

TActionTrigger::TActionTrigger(Model::TActionTrigger *actionTrigger)
{
    if(!actionTrigger)
        return;

    mEvent = actionTrigger->event();
    mValue = actionTrigger->value();
    mContext = actionTrigger->context();
}

TActionTrigger::~TActionTrigger()
{

}

bool TActionTrigger::fired(const GameEvent &ge, const std::string &command)
{
    if(ge==GE_NONE)
    {
        if(mEvent==ge && command.empty())
            return true;
    } else if(ge==GE_KEYDOWN) {
        if(!mValue.empty())
        {
            unsigned int npos = command.rfind(mValue);
            if(npos==std::string::npos)
                return false;
            else {
                unsigned int n = command.size() - npos - mValue.size();
                if(n==0 || (command.back()=='~' && n==1))
                    return true;
            }
        }
    } else if(ge==mEvent) {
        return true;
    }
    return false;
}

GameEvent TActionTrigger::event() const
{
    return mEvent;
}

void TActionTrigger::setEvent(const GameEvent &event)
{
    mEvent = event;
}

std::string TActionTrigger::value() const
{
    return mValue;
}

void TActionTrigger::setValue(const std::string &value)
{
    mValue = value;
}

TActionTrigger *TActionTrigger::generateMirrorActionTrigger() const
{
    TActionTrigger *actionTrigger = new TActionTrigger;
    std::string newValue = mValue;
    newValue = replace(newValue, "→", "←");
    newValue = replace(newValue, "↗", "↖");
    newValue = replace(newValue, "↘", "↙");

    actionTrigger->setEvent(mEvent);
    actionTrigger->setValue(newValue);
    return actionTrigger;
}

int TActionTrigger::context() const
{
    return mContext;
}

void TActionTrigger::setContext(int context)
{
    mContext = context;
}

TActionTriggerManager::TActionTriggerManager()
{
    
}

TActionTriggerManager::~TActionTriggerManager()
{

}

bool TActionTriggerManager::fired(const GameEvent &ge, const std::string &command)
{
    for(TActionTrigger *t : mTriggerList)
    {
        if(t->fired(ge, command))
            return true;
    }
    return false;
}

TActionTriggerList TActionTriggerManager::triggerList() const
{
    return mTriggerList;
}

void TActionTriggerManager::setTriggerList(const TActionTriggerList &triggerList)
{
    if(mTriggerList.size() > 0)
    {
        FREE_CONTAINER(mTriggerList);
    }
    mTriggerList = triggerList;
}

void TActionTriggerManager::operator =(const TActionTriggerManager &actionTriggerManager)
{
    mTriggerList = actionTriggerManager.triggerList();
}
