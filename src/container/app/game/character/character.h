#ifndef SPRITE_TCHARACTER_H
#define SPRITE_TCHARACTER_H

#include <string>
#include <vector>
#include <gameutils/model/character.h>

#include "frame.h"
#include "action.h"

class TCharacter
{
public:
    TCharacter();
    TCharacter(const std::string &file);
    TCharacter(const TCharacter &character);
    TCharacter(const Model::TCharacter &character, void *context = nullptr);
    ~TCharacter();

    void load(const std::string &file);
    void loadFromModel(const Model::TCharacter &characterModel, void *context = nullptr);

    int getAge() const;
    int getWeight() const;
    int getForce() const;
    int getDefend() const;
    float getScale() const;
    std::string getComment() const;
    int getHeight() const;
    std::string getCreatTime() const;
    std::string getUpdateTime() const;
    std::string getEmail() const;
    std::string getName() const;
    std::string getAiCode() const;
    std::string getVersion() const;
    std::string getAuthor() const;
    CharacterGender getGender() const;
    std::string getUuid() const;

    TActionList getLeftActionList() const;
    TActionList getRightActionList() const;
    TAction *getLeftDefaultAction() const;
    TAction *getRightDefaultAction() const;

private:
    int mAge;
    int mWeight;
    int mForce;
    int mDefend;
    float mScale;
    int mHeight;
    std::string mUuid;
    std::string mComment;
    std::string mCreatTime;
    std::string mUpdateTime;
    std::string mEmail;
    std::string mName;
    std::string mAiCode;
    std::string mVersion;
    std::string mAuthor;
    CharacterGender mGender;
    TActionList mLeftActionList;
    TActionList mRightActionList;
    TAction *mLeftDefaultAction;
    TAction *mRightDefaultAction;
};

typedef std::vector<TCharacter*> TCharacterList;
#endif // SPRITE_TCHARACTER_H
