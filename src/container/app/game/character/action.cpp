#include "action.h"

#include <algorithm>

TAction::TAction() :
    mIndex(0)
  , mActionMode(AM_DEFAULT)
  , mRecircleCount(0)
{

}

TAction::TAction(const TAction &action) :
    mIndex(0)
  , mActionMode(AM_DEFAULT)
  , mRecircleCount(0)
{
    mName = action.getName();
    mActionMode = action.getActionMode();
    mVector = action.getVector();
    for(TFrame *frame : action.getFramesList())
    {
        mFramesList.emplace_back(new TFrame(*frame));
    }
}

TAction::TAction(Model::TAction *action) :
    mIndex(0)
  , mActionMode(AM_DEFAULT)
  , mRecircleCount(0)
{
    if(!action)
        return;

    mName = action->getName();
    mActionMode = action->getActionMode();
    mVector = action->getVector();

    TFramesList frameList;
    for(Model::TFrame *frame : action->getFramesList())
    {
        TFrame *newFrame = new TFrame(frame);
        frameList.emplace_back(newFrame);
    }
    setFramesList(frameList);

    TActionTriggerList triggerList;
    for(Model::TActionTrigger *actionTrigger : action->getTriggerList())
    {
        triggerList.emplace_back(new TActionTrigger(actionTrigger));
    }
    setActionTriggerList(triggerList);
}

TAction::~TAction()
{
    FREE_CONTAINER(mFramesList);
}

void TAction::reset()
{
    mIndex = 0;
    mRecircleCount = 0;
}

void TAction::step()
{
    TFrame *currentFrame = getCurrentFrame();
    if(currentFrame)
    {
        currentFrame->step();
        if(currentFrame->isEnd()) {
            currentFrame->reset();
            mIndex++;
            if(mIndex >= (int)mFramesList.size())
            {
                mIndex = 0;
                mRecircleCount++;
            }
        }
    }
}

bool TAction::accept(const GameEvent &ge, const std::string &command)
{
    for(TActionTrigger *t : mActionTriggerList)
    {
        if(t->fired(ge, command))
            return true;
    }
    return false;
}

TFrame *TAction::getCurrentFrame() const
{
    if(mIndex>-1 && mIndex<(int)mFramesList.size())
        return mFramesList.at(mIndex);

    return nullptr;
}

bool TAction::isRecycled() const
{
    return mRecircleCount>0;
}

std::string TAction::getName() const
{
    return mName;
}

TVector2 TAction::getVector() const
{
    if(mRecircleCount < 1)
        return mVector;

    return TVector2();
}

ActionMode TAction::getActionMode() const
{
    return mActionMode;
}

bool TAction::isEnd() const
{
    return mIndex==0 && mRecircleCount>0;
}

TAction *TAction::generateMirrorAction() const
{
    TAction *newAction = new TAction;
    newAction->setName(mName);

    TFramesList frameList;
    for(TFrame *frame : mFramesList)
    {
        frameList.emplace_back(frame->generateMirrorFrame());
    }
    newAction->setFramesList(frameList);

    TActionTriggerList triggerList;
    for(TActionTrigger *actionTrigger : mActionTriggerList)
    {
        triggerList.emplace_back(actionTrigger->generateMirrorActionTrigger());
    }
    newAction->setActionTriggerList(triggerList);

    return newAction;
}

void TAction::setName(const std::string &name)
{
    mName = name;
}

bool TAction::isValid() const
{
    return !mFramesList.empty();
}

TFramesList TAction::getFramesList() const
{
    return mFramesList;
}

void TAction::setFramesList(const TFramesList &framesList)
{
    FREE_CONTAINER(mFramesList);
    for(TFrame *frame : framesList)
    {
        if(!frame)
            continue;

        mFramesList.emplace_back(frame);
    }
}

TActionTriggerList TAction::getActionTriggerList() const
{
    return mActionTriggerList;
}

void TAction::setActionTriggerList(const TActionTriggerList &actionTriggerList)
{
    FREE_CONTAINER(mActionTriggerList);
    mActionTriggerList = actionTriggerList;
}

InterruptType TAction::getInterruptType() const
{
    TFrame *currentFrame = getCurrentFrame();
    if(currentFrame)
        return currentFrame->mInterruptType;
    return IT_COUNT;
}

int TAction::getIndex() const
{
    return mIndex;
}

void TAction::setIndex(int index)
{
    mIndex = index;
}
