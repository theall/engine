#ifndef SPRITE_ACTION_H
#define SPRITE_ACTION_H

#include <vector>
#include <gameutils/base/vector.h>
#include <gameutils/model/character/action.h>

#include "frame.h"
#include "actiontrigger.h"

class TAction
{
public:
    TAction();
    TAction(const TAction &action);
    TAction(Model::TAction *action);
    ~TAction();

    void reset();
    void step();

    bool accept(const GameEvent &ge, const std::string &command = std::string());

    TFrame *getCurrentFrame() const;
    bool isRecycled() const;

    std::string getName() const;
    TVector2 getVector() const;

    ActionMode getActionMode() const;

    bool isEnd() const;

    TAction *generateMirrorAction() const;

    void setName(const std::string &name);

    bool isValid() const;
    TFramesList getFramesList() const;
    void setFramesList(const TFramesList &framesList);

    TActionTriggerList getActionTriggerList() const;
    void setActionTriggerList(const TActionTriggerList &actionTriggerList);

    InterruptType getInterruptType() const;

    int getIndex() const;
    void setIndex(int index);

private:
    int mIndex;
    std::string mName;
    ActionMode mActionMode;
    TActionTriggerList mActionTriggerList;
    TFramesList mFramesList;
    TVector2 mVector;
    int mRecircleCount;
};

typedef std::vector<TAction*> TActionList;
#endif // SPRITE_ACTION_H
