#include "character.h"

extern TFileSystem *fs;

TCharacter::TCharacter() :
    mLeftDefaultAction(NULL)
  , mRightDefaultAction(NULL)
{

}

TCharacter::TCharacter(const std::string &file) :
    mLeftDefaultAction(NULL)
  , mRightDefaultAction(NULL)
{
    load(file);
}

TCharacter::TCharacter(const TCharacter &character)
{
    mAge = character.getAge();
    mWeight = character.getWeight();
    mForce = character.getForce();
    mDefend = character.getDefend();
    mScale = character.getScale();
    mHeight = character.getHeight();
    mUuid = character.getUuid();
    mComment = character.getComment();
    mCreatTime = character.getCreatTime();
    mUpdateTime = character.getUpdateTime();
    mEmail = character.getEmail();
    mName = character.getName();
    mAiCode = character.getAiCode();
    mVersion = character.getVersion();
    mAuthor = character.getAuthor();
    mGender = character.getGender();
    mLeftActionList = character.getLeftActionList();
    mRightActionList = character.getRightActionList();
}

TCharacter::TCharacter(const Model::TCharacter &character, void *context) :
    mLeftDefaultAction(NULL)
  , mRightDefaultAction(NULL)
{
    loadFromModel(character, context);
}

TCharacter::~TCharacter()
{
    FREE_CONTAINER(mLeftActionList);
    FREE_CONTAINER(mRightActionList);
}

void TCharacter::load(const std::string &file)
{
    std::string currentSourceBackup = fs->getSource();
    std::string fileName = extractFileName(file);
    if(fileName != file)
    {
        std::string fileFullName = currentSourceBackup + "/" + file;
        std::string characterRoot = extractPath(fileFullName);
        fs->setSource(characterRoot);
    }
    TFileData *fileData = fs->newFileData(fileName);
    Model::TCharacter characterModel;
    try {
        characterModel.loadFromData(fileData->getData(), fileData->getSize());
        fileData->release();
    } catch(std::string s) {
        fileData->release();
        throw format("Failed to load %s, \n\t%s", file.c_str(), s.c_str());
    }

    loadFromModel(characterModel);
    fs->setSource(currentSourceBackup);
}

int TCharacter::getAge() const
{
    return mAge;
}

int TCharacter::getWeight() const
{
    return mWeight;
}

int TCharacter::getForce() const
{
    return mForce;
}

int TCharacter::getDefend() const
{
    return mDefend;
}

float TCharacter::getScale() const
{
    return mScale;
}

std::string TCharacter::getComment() const
{
    return mComment;
}

int TCharacter::getHeight() const
{
    return mHeight;
}

std::string TCharacter::getCreatTime() const
{
    return mCreatTime;
}

std::string TCharacter::getUpdateTime() const
{
    return mUpdateTime;
}

std::string TCharacter::getEmail() const
{
    return mEmail;
}

std::string TCharacter::getName() const
{
    return mName;
}

std::string TCharacter::getAiCode() const
{
    return mAiCode;
}

std::string TCharacter::getVersion() const
{
    return mVersion;
}

std::string TCharacter::getAuthor() const
{
    return mAuthor;
}

CharacterGender TCharacter::getGender() const
{
    return mGender;
}

std::string TCharacter::getUuid() const
{
    return mUuid;
}

void TCharacter::loadFromModel(const Model::TCharacter &characterModel, void *context)
{
    (void)context;

    mAge = characterModel.getAge();
    mWeight = characterModel.getWeight();
    mForce = characterModel.getForce();
    mDefend = characterModel.getDefend();
    mScale = characterModel.getScale();
    mComment = characterModel.getComment();
    mHeight = characterModel.getHeight();
    mCreatTime = characterModel.getCreatTime();
    mUpdateTime = characterModel.getUpdateTime();
    mEmail = characterModel.getEmail();
    mName = characterModel.getName();
    mAiCode = characterModel.getAiCode();
    mVersion = characterModel.getVersion();
    mAuthor = characterModel.getAuthor();
    mGender = characterModel.getGender();
    mUuid = characterModel.getUuid();

    // Actions
    FREE_CONTAINER(mLeftActionList);
    FREE_CONTAINER(mRightActionList);
    for(Model::TAction *actionModel : characterModel.getActionList())
    {
        TAction *action = new TAction(actionModel);
        if(action->isValid())
            mRightActionList.emplace_back(action);
    }
    for(TAction *action : mRightActionList)
    {
        if(action->accept(GE_NONE))
        {
            mRightDefaultAction = action;
            break;
        }
    }
    std::sort(mRightActionList.begin(), mRightActionList.end(), [=](TAction *a1, TAction *a2){
        return a1->getName().size() < a2->getName().size();
    });

    for(TAction *action : mRightActionList)
    {
        mLeftActionList.emplace_back(action->generateMirrorAction());
    }
    for(TAction *action : mLeftActionList)
    {
        if(action->accept(GE_NONE))
        {
            mLeftDefaultAction = action;
            break;
        }
    }
}

TActionList TCharacter::getLeftActionList() const
{
    return mLeftActionList;
}

TActionList TCharacter::getRightActionList() const
{
    return mRightActionList;
}

TAction *TCharacter::getLeftDefaultAction() const
{
    return mLeftDefaultAction;
}

TAction *TCharacter::getRightDefaultAction() const
{
    return mRightDefaultAction;
}
