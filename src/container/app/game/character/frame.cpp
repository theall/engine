#include "frame.h"

TFrame::TFrame() :
    mImage(nullptr)
  , mAntiGravity(false)
  , mDuration(1)
  , mIsKeyFrame(true)
  , mInterruptType(IT_DISABLE)
  , mIndex(0)
  , mIsEnd(false)
{

}

TFrame::TFrame(Model::TFrame *frame) :
    mImage(nullptr)
  , mAntiGravity(false)
  , mDuration(1)
  , mIsKeyFrame(true)
  , mInterruptType(IT_DISABLE)
  , mIndex(0)
  , mIsEnd(false)
{
    if(!frame)
        return;

    mHandList = frame->getHandList();
    mFootRect = frame->getFootRect();
    mAnchor = frame->getAnchor();
    mImage = TGraphics::instance()->newImage(frame->getImagePath());
    mVector = frame->getVector();
    mAntiGravity = frame->getAntiGravity();
    mDuration = frame->getDuration();
    mSoundSet.loadFromModel(*frame->getSoundSet(), this);
    mInterruptType = frame->getInterruptType();
    mVectorTable.clear();
    for(Model::TVectorItem *vectorItem : frame->getVectorItemsList())
    {
        mVectorTable.emplace_back(vectorItem->vector());
    }
    mUndertakeAreaGroup.setRectList(frame->getUndertakeRectList(), mAnchor);
    mAttackAreaGroup.setRectList(frame->getAttackRectList(), mAnchor);
    mCollideAreaGroup.setRectList(frame->getCollideRectList(), mAnchor);
    mTerrianAreaGroup.setRectList(frame->getTerrianRectList(), mAnchor);
}

TFrame::TFrame(const TFrame &frame) :
    mImage(nullptr)
  , mAntiGravity(false)
  , mDuration(1)
  , mIsKeyFrame(true)
  , mInterruptType(IT_DISABLE)
  , mIndex(0)
  , mIsEnd(false)
{
    mImage = frame.mImage;
    mAttackAreaGroup = frame.mAttackAreaGroup;
    mUndertakeAreaGroup = frame.mUndertakeAreaGroup;
    mCollideAreaGroup = frame.mCollideAreaGroup;
    mTerrianAreaGroup = frame.mTerrianAreaGroup;
    mSoundSet = frame.mSoundSet;
    mHandList = frame.mHandList;
    mFootRect = frame.mFootRect;
    mAnchor = frame.mAnchor;
    mVector = frame.mVector;
    mAntiGravity = frame.mAntiGravity;
    mDuration = frame.mDuration;
    mIsKeyFrame = frame.mIsKeyFrame;
    mInterruptType = frame.mInterruptType;
    mVectorTable = frame.mVectorTable;
}

TFrame::~TFrame()
{

}

TFrame *TFrame::generateMirrorFrame() const
{
    if(!mImage)
        return NULL;

    TFrame *newFrame = new TFrame;
    if(!newFrame)
        throw TException("No enough memeory to create new frame.");

    float imageHalfWidth = (float)mImage->getWidth() / 2;
    newFrame->mImage = mImage->generateMirrorImage();
    newFrame->mHandList = Model::mirror(mHandList, imageHalfWidth);
    newFrame->mFootRect = mFootRect.mirror(imageHalfWidth);
    newFrame->mAnchor = mAnchor.mirror(imageHalfWidth);
    for(TVector2 vector : mVectorTable)
    {
        newFrame->mVectorTable.emplace_back(vector.mirror());
    }
    newFrame->mVector = mVector.mirror();
    newFrame->mAntiGravity = mAntiGravity;
    newFrame->mDuration = mDuration;
    newFrame->mSoundSet = mSoundSet;
    newFrame->mInterruptType = mInterruptType;
    newFrame->mAttackAreaGroup = mAttackAreaGroup.mirror(mAnchor, imageHalfWidth);
    newFrame->mUndertakeAreaGroup = mUndertakeAreaGroup.mirror(mAnchor, imageHalfWidth);
    newFrame->mCollideAreaGroup = mCollideAreaGroup.mirror(mAnchor, imageHalfWidth);
    newFrame->mTerrianAreaGroup = mTerrianAreaGroup.mirror(mAnchor, imageHalfWidth);
    return newFrame;
}

const TSoundSet *TFrame::getSoundSet() const
{
    return mSoundSet.isValid()?&mSoundSet:nullptr;
}

bool TFrame::isEnd() const
{
    return mIsEnd;
}

void TFrame::reset()
{
    mIndex = 0;
    mIsEnd = false;
}

void TFrame::step()
{
    mIndex++;
    if(mIndex >= mDuration) {
        mIsEnd = true;
    }
}

TVector2 TFrame::currentVector() const
{
    if(mIndex>=0 && mIndex<(int)mVectorTable.size())
        return mVectorTable.at(mIndex);
    return TVector2();
}

bool TFrame::isFirst() const
{
    return mIndex==0;
}

