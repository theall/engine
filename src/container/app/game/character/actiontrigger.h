#ifndef SPRITE_TACTIONTRIGGER_H
#define SPRITE_TACTIONTRIGGER_H

#include <vector>
#include <string>
#include <gameutils/base/variable.h>
#include <gameutils/model/character/actiontrigger.h>

class TActionTrigger
{
public:
    TActionTrigger();
    TActionTrigger(const TActionTrigger &actionTrigger);
    TActionTrigger(Model::TActionTrigger *actionTrigger);
    ~TActionTrigger();

    bool fired(const GameEvent &ge, const std::string &command);

    GameEvent event() const;
    void setEvent(const GameEvent &event);
    std::string value() const;
    void setValue(const std::string &value);
    std::string name() const;

    TActionTrigger *generateMirrorActionTrigger() const;

    int context() const;
    void setContext(int context);

private:
    GameEvent mEvent;
    int mContext;
    std::string mValue;
};

typedef std::vector<TActionTrigger*> TActionTriggerList;

class TActionTriggerManager
{
public:
    TActionTriggerManager();
    ~TActionTriggerManager();

    bool fired(const GameEvent &ge, const std::string &command);

    TActionTriggerList triggerList() const;
    void setTriggerList(const TActionTriggerList &triggerList);

    void operator =(const TActionTriggerManager &actionTriggerManager);
private:
    TActionTriggerList mTriggerList;
};

#endif // SPRITE_TACTIONTRIGGER_H
