#ifndef SCENESTACK_H
#define SCENESTACK_H

#include "scene/scene.h"

class TSceneStack
{
public:
    TSceneStack();
    ~TSceneStack();

    void push(TScene *scene);
    void pop();
    void clear();
    bool isEmpty() const;
    bool isMonopolize() const;

    void step();
    void render();

private:
    TSceneList mSceneList;

    void update();
    TScene *getLastScene() const;
};

#endif // SCENESTACK_H
