#include "resourcecache.h"

extern TFileSystem *fs;
extern TGraphics *gfx;
extern TAudio *ao;

#define FREE_MAP_CONTAINER(x) \
    for(auto a : x) \
        delete a.second; \
    x.clear()

bool isAbsolutePath(const std::string &fileName)
{
    bool isAbsolutePath = false;
#ifdef _WIN32
    isAbsolutePath = fileName.size()>1&&fileName[1]==':';
#elif Unix
    isAbsolutePath = fileName.size()>0&&fileName[0]=='/';
#endif
    return isAbsolutePath;
}

std::string getAbsolutePath(const std::string &fileName)
{
    if(isAbsolutePath(fileName))
        return fileName;

    std::string path = fs->getSource();
    if(!fileName.empty() && fileName[0] != '/')
        path.push_back('/');

    return path + fileName;
}

TResourceCache *TResourceCache::mInstance = nullptr;
TResourceCache::TResourceCache()
{

}

TResourceCache::~TResourceCache()
{
    FREE_MAP_CONTAINER(mCharacterPathMap);
    // Graphics do not need to free
//    FREE_MAP_CONTAINER(mImageMap);
//    FREE_MAP_CONTAINER(mFontMap);
//    FREE_MAP_CONTAINER(mSoundMap);
}

TCharacter *TResourceCache::findCharacterByFilePath(const std::string &fileName)
{
    std::string absPath = getAbsolutePath(fileName);
    std::map<std::string, TCharacter*>::iterator it = mCharacterPathMap.find(absPath);
    if(it == mCharacterPathMap.end())
        return nullptr;

    return it->second;
}

TCharacter *TResourceCache::findCharacterByUuid(const std::string &uuid)
{
    std::map<std::string, TCharacter*>::iterator it = mCharacterUuidMap.find(uuid);
    if(it == mCharacterUuidMap.end())
        return nullptr;

    return it->second;
}

TDrawableImage *TResourceCache::findImage(const std::string &fileName)
{
    std::string absPath = getAbsolutePath(fileName);
    std::map<std::string, TDrawableImage*>::iterator it = mImageMap.find(absPath);
    if(it == mImageMap.end())
        return nullptr;

    return it->second;
}

TCharacter *TResourceCache::loadCharacter(const std::string &fileName)
{
    if(fileName.empty())
        return nullptr;

    TCharacter *character = findCharacterByFilePath(fileName);
    if(!character)
        character = findCharacterByUuid(fileName);

    if(!character)
    {
        std::string sourceBackup = fs->getSource();
        if(!fs->exists(fileName.c_str()) && !mCharacterPath.empty())
        {
            fs->setSource(mCharacterPath);
        }
        character = new TCharacter(fileName);
        fs->setSource(sourceBackup);
        mCharacterPathMap.insert(std::make_pair(getAbsolutePath(fileName), character));
        mCharacterUuidMap.insert(std::make_pair(character->getUuid(), character));
    }
    return character;
}

TDrawableImage *TResourceCache::loadImage(const std::string &fileName)
{
    if(fileName.empty())
        return nullptr;

    TDrawableImage *image = findImage(fileName);
    if(!image)
    {
        image = gfx->newImage(fileName);
        mImageMap.insert(std::make_pair(getAbsolutePath(fileName), image));
    }
    return image;
}

TFont *TResourceCache::findFont(const std::string &fileName)
{
    std::string absPath = getAbsolutePath(fileName);
    std::map<std::string, TFont*>::iterator it = mFontMap.find(absPath);
    if(it == mFontMap.end())
        return nullptr;

    return it->second;
}

TFont *TResourceCache::loadFont(const std::string &fileName)
{
    if(fileName.empty())
        return nullptr;

    TFont *font = findFont(fileName);
    if(!font)
    {
        font = gfx->newFont(fileName);
        mFontMap.insert(std::make_pair(getAbsolutePath(fileName), font));
    }
    return font;
}

TSource *TResourceCache::findSound(const std::string &fileName)
{
    std::string absPath = getAbsolutePath(fileName);
    std::map<std::string, TSource*>::iterator it = mSoundMap.find(absPath);
    if(it == mSoundMap.end())
        return nullptr;

    return it->second;
}

TSource *TResourceCache::loadSound(const std::string &fileName)
{
    if(fileName.empty())
        return nullptr;

    TSource *source = findSound(fileName);
    if(!source)
    {
        source = ao->newSource(fileName);
        mSoundMap.insert(std::make_pair(getAbsolutePath(fileName), source));
    }
    return source;
}

std::string TResourceCache::characterPath() const
{
    return mCharacterPath;
}

void TResourceCache::setCharacterPath(const std::string &characterPath)
{
    mCharacterPath = characterPath;
}

std::string TResourceCache::soundPath() const
{
    return mSoundPath;
}

void TResourceCache::setSoundPath(const std::string &soundPath)
{
    mSoundPath = soundPath;
}

std::string TResourceCache::fontPath() const
{
    return mFontPath;
}

void TResourceCache::setFontPath(const std::string &fontPath)
{
    mFontPath = fontPath;
}
