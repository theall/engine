#ifndef KEYBOARDCONTROLLER_H
#define KEYBOARDCONTROLLER_H

#include "controller.h"

class TKeyboardController : public TController
{
    DECLARE_INSTANCE(TKeyboardController)

public:
    TKeyboardController();
    ~TKeyboardController();

private:
    TKeyboard *mKeyboard;

    // TController interface
private:
    int getButtonState() override;
};

#endif // KEYBOARDCONTROLLER_H
