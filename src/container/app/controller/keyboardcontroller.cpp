#include "keyboardcontroller.h"

struct KEY_MAP
{
    TKeyboard::Key keyboardKey;
    TButton::ButtonType button;
};

KEY_MAP g_keyMap[] =
{
    {TKeyboard::KEY_W, TButton::UP},
    {TKeyboard::KEY_D, TButton::RIGHT},
    {TKeyboard::KEY_S, TButton::DOWN},
    {TKeyboard::KEY_A, TButton::LEFT},
    {TKeyboard::KEY_J, TButton::A},
    {TKeyboard::KEY_K, TButton::B},
    {TKeyboard::KEY_L, TButton::C},
    {TKeyboard::KEY_U, TButton::X},
    {TKeyboard::KEY_I, TButton::Y},
    {TKeyboard::KEY_O, TButton::Z}
};

TKeyboardController *TKeyboardController::mInstance = NULL;
TKeyboardController::TKeyboardController() :
    TController()
{
    mKeyboard = TKeyboard::instance();
}

TKeyboardController::~TKeyboardController()
{

}

int TKeyboardController::getButtonState()
{
    int button = TButton::NONE;
    for(uint32 i=0;i<sizeof(g_keyMap)/sizeof(KEY_MAP);i++)
    {
        KEY_MAP *k = &g_keyMap[i];
        if(mKeyboard->isDown(k->keyboardKey))
        {
            button |= k->button;
        }
    }
    return button;
}
