#include "controller.h"

TController::TController() :
    mCurrentFrames(0)
  , mCheckInterval(5)
  , mStickyFrames(30)
  , mTimeOutFrames(30)
  , mHasKeyDown(false)
  , mLastButtonState(0)
  , mPuppet(nullptr)
{

}

TController::~TController()
{

}

void TController::step()
{
    Command inputCommand;
    int currentButtonState = getButtonState();
    inputCommand.value = currentButtonState;
    mLastButtonState = currentButtonState;
    if(mCommandList.empty())
    {
        mCommandList.emplace_back(inputCommand);
    }
    for(Command &cmd : mCommandList)
    {
        cmd.increase();
    }
    Command &lastCommand = mCommandList.back();
    // Check last command is time out
    if(lastCommand.value==TButton::NONE)
    {
        mHasKeyDown = false;
        if(lastCommand.frames >= mTimeOutFrames)
            mCommandList.clear();
    } else {
        mHasKeyDown = true;
    }

    if(inputCommand.value == lastCommand.value) {
        lastCommand.increaseRepeat();
    } else {
        if(lastCommand.value==TButton::NONE)
            mCommandList.pop_back();
        if(lastCommand.frames >= 5)
            mCommandList.emplace_back(inputCommand);
    }

    mInputCommand.clear();
    for(Command &cmd : mCommandList)
    {
        if(cmd.value==0 || cmd.repeat<1)
            continue;

        std::string commandStr = TButton::toString(cmd.value);
        if(cmd.repeat >= mStickyFrames)
            commandStr.push_back('~');

        //commandStr.push_back(',');
        mInputCommand += commandStr;
    }
//    if(!mInputCommand.empty()) {
//        mInputCommand.pop_back();
//    }
}

std::string TController::getInputCommandString() const
{
    return mInputCommand;
}

void TController::clear()
{
    mCommandList.clear();
    mInputCommand.clear();
}

std::vector<Command> TController::getCommandList() const
{
    return mCommandList;
}

int TController::getCheckInterval() const
{
    return mCheckInterval;
}

void TController::setCheckInterval(int checkInterval)
{
    mCheckInterval = checkInterval;
}

void *TController::getPuppet() const
{
    return mPuppet;
}

void TController::setPuppet(void *puppet)
{
    mPuppet = puppet;
}

void TController::setStickyFrames(int stickyFrames)
{
    mStickyFrames = stickyFrames;
}

void TController::setTimeOutFrames(int timeOutFrames)
{
    mTimeOutFrames = timeOutFrames;
}

int TController::stickyFrames() const
{
    return mStickyFrames;
}

int TController::timeOutFrames() const
{
    return mTimeOutFrames;
}

bool TController::hasKeyDown() const
{
    return mHasKeyDown;
}
