#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <sdk/engine.h>
#include <gameutils/base/button.h>

struct Command
{
public:
    int value;
    int frames;
    int repeat;
    Command() {
        value = TButton::NONE;
        frames = 0;
        repeat = 0;
    }
    void operator =(const Command &cmd)
    {
        value = cmd.value;
        frames = cmd.frames;
        repeat = cmd.repeat;
    }
    bool operator !=(const Command &cmd)
    {
        return value!=cmd.value;
    }
    bool operator ==(const Command &cmd)
    {
        return value==cmd.value;
    }
    bool isNull()
    {
        return value==0;
    }
    bool isTimeOut(int count = 60)
    {
        return frames>=count;
    }
    void increase()
    {
        frames++;
    }
    bool isSticky(int count = 60)
    {
        return repeat>=count;
    }
    void increaseRepeat()
    {
        repeat++;
    }
};

class TController
{
public:
    TController();
    virtual ~TController();

    virtual int getButtonState() = 0;

    void step();

    std::string getInputCommandString() const;

    int stickyFrames() const;
    void setStickyFrames(int stickyFrames);

    int timeOutFrames() const;
    void setTimeOutFrames(int timeOutFrames);

    bool hasKeyDown() const;
    void clear();

    std::vector<Command> getCommandList() const;

    int getCheckInterval() const;
    void setCheckInterval(int checkInterval);

    void *getPuppet() const;
    void setPuppet(void *puppet);

private:
    int mCurrentFrames;
    int mCheckInterval;
    int mStickyFrames;
    int mTimeOutFrames;
    bool mHasKeyDown;
    int mLastButtonState;
    std::string mInputCommand;
    std::vector<Command> mCommandList;
    void *mPuppet;
};

#endif // CONTROLLER_H
