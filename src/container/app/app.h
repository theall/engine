#ifndef TAPP_H
#define TAPP_H

#include <sdk/engine.h>
#include <gameutils/base/vector.h>

#include "game/scene/layer/textsprite.h"

class TApp : public TEngine
{
public:
    TApp(int argc, char *argv[]);
    ~TApp();

    // TEngine interface
    void update();
    void draw();
    void load();
    bool onMessage(TMessage *msg);
    void setDisplayMessage(const std::string &msg, int timeOut = 180);
    void setShowFps(bool showFps);

    bool needQuit() const;

private:
    bool mNeedQuit;
    bool mShowFps;
    bool mShowFullScreen;
    std::string mFileName;
    std::string mSourcePath;
    int mTimeOutFrames;
    TText *mStatusMessage;
    TVector2 mWindowSize;
    TVector2 mWindowCenter;
    std::string mWindowName;

    void setSourceFile(const std::string &file);
};
#endif // TAPP_H
