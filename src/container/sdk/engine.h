#ifndef TENGINE_H
#define TENGINE_H

#include "modules/window/window.h"
#include "modules/audio/audio.h"
#include "modules/event/event.h"
#include "modules/filesystem/filesystem.h"
#include "modules/timer/timer.h"
#include "modules/math/math.h"
#include "modules/graphics/graphics.h"
#include "modules/mouse/mouse.h"
#include "modules/joystick/joystickmanager.h"
#include "modules/network/network.h"

class TEngine
{
public:
    TEngine(int argc, char *argv[]);
    ~TEngine();

    int run();

    virtual void update() = 0;
    virtual void draw() = 0;
    virtual void load() = 0;
    virtual bool onMessage(TMessage *msg) = 0;

    int fps() const;
    void setFps(int fps);

    void setWindowResolution(int width, int height);

    int skipFrames() const;
    void setSkipFrames(int skipFrames);

private:
    std::string mError;
    int mFps;
    int mSkipFrames;
    int mWidth;
    int mHeight;
    float mSleepMicroSecs;
    void boot();
    bool init();
    void handleError(const std::string &msg);
};

#endif // TENGINE_H
