#ifndef STD_PCH
#define STD_PCH

#include <exception>
#include <cstdarg> // vararg
#include <cstdio> // vsnprintf
#include <cstring> // strncpy
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
// C++
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>

// C
#include <cmath>
#include <cstdio>

#include <climits> // for CHAR_BIT
#include <cstdlib> // for rand() and RAND_MAX

#include <unordered_map>
#include <atomic>
// STD
#include <bitset>
#include <stdint.h>

#endif // STD_PCH

