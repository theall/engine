/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_VARIANT_H
#define THEALL_VARIANT_H

#include "object.h"
#include "int.h"
#include "types.h"

class TVariant
{
public:

    enum VariantType
	{
		UNKNOWN = 0,
		BOOLEAN,
		NUMBER,
		STRING,
		SMALLSTRING,
		LUSERDATA,
		FUSERDATA,
		NIL,
		TABLE
	};

    TVariant();
    TVariant(bool boolean);
    TVariant(double number);
    TVariant(const char *string, size_t len);
    TVariant(void *userdata);
    TVariant(Type udatatype, void *userdata);
    TVariant(std::vector<std::pair<TVariant, TVariant>> *table);
    TVariant(const TVariant &v);
    TVariant(TVariant &&v);
    ~TVariant();

    TVariant &operator = (const TVariant &v);

    VariantType getType() const { return type; }

    bool toBool() const;
    int toInt() const;
    double toDouble() const;
    void *getUserData() const;

private:

	class SharedString : public TObject
	{
	public:

		SharedString(const char *string, size_t len)
			: len(len)
		{
			str = new char[len+1];
			memcpy(str, string, len);
		}
		virtual ~SharedString() { delete[] str; }

		char *str;
		size_t len;
	};

	class SharedTable : public TObject
	{
	public:

        SharedTable(std::vector<std::pair<TVariant, TVariant>> *table)
			: table(table)
		{
		}

		virtual ~SharedTable() { delete table; }

        std::vector<std::pair<TVariant, TVariant>> *table;
	};

	static const int MAX_SMALL_STRING_LENGTH = 15;

    VariantType type;
    Type mTypeId;

	union Data
	{
		bool boolean;
		double number;
		SharedString *string;
		void *userdata;
		SharedTable *table;
		struct
		{
			char str[MAX_SMALL_STRING_LENGTH];
			uint8 len;
		} smallstring;
	} data;

}; // Variant

#endif // THEALL_VARIANT_H
