/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_CONFIG_H
#define THEALL_CONFIG_H

// Platform stuff.
#if defined(WIN32) || defined(_WIN32)
#	define THEALL_WINDOWS 1
	// If _USING_V110_SDK71_ is defined it means we are using the xp toolset.
#	if defined(_MSC_VER) && (_MSC_VER >= 1700) && !_USING_V110_SDK71_
#	include <winapifamily.h>
#		if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP) && !WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP)
#			define THEALL_WINDOWS_UWP 1
#			define THEALL_NO_MODPLUG 1
#			define THEALL_NOMPG123 1
#		endif
#	endif
#endif
#if defined(linux) || defined(__linux) || defined(__linux__)
#	define THEALL_LINUX 1
#endif
#if defined(__ANDROID__)
#	define THEALL_ANDROID 1
#endif
#if defined(__APPLE__)
#	include <TargetConditionals.h>
#	if TARGET_OS_IPHONE
#		define THEALL_IOS 1
#	elif TARGET_OS_MAC
#		define THEALL_MACOSX 1
#	endif
#endif
#if defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__)
// I know it's not linux, but it seems most "linux-only" code is bsd-compatible
#	define THEALL_LINUX 1
#endif

// Endianness.
#if defined(__ppc__) || defined(__ppc) || defined(__powerpc__) || defined(__powerpc)
#	define THEALL_BIG_ENDIAN 1
#else
#	define THEALL_LITTLE_ENDIAN 1
#endif

// Warnings.
#ifndef _CRT_SECURE_NO_WARNINGS
#	define _CRT_SECURE_NO_WARNINGS
#endif

// Preferably, and ironically, this macro should go unused.
#ifndef THEALL_UNUSED
#	define THEALL_UNUSED(x) (void)sizeof(x)
#endif

#ifndef THEALL_BUILD
#	define THEALL_BUILD
#	define THEALL_BUILD_STANDALONE
#	define THEALL_BUILD_EXE
//#	define THEALL_BUILD_DLL
#endif

// DLL-stuff.
#ifdef THEALL_WINDOWS
#	define THEALL_EXPORT __declspec(dllexport)
#else
#	define THEALL_EXPORT
#endif

#if defined(THEALL_WINDOWS)
#ifndef THEALL_WINDOWS_UWP
#	define THEALL_LEGENDARY_CONSOLE_IO_HACK
#endif // THEALL_WINDOWS_UWP
#ifndef NOMINMAX
#	define NOMINMAX
#endif
#endif

#if defined(THEALL_MACOSX) || defined(THEALL_IOS)
#	define THEALL_LEGENDARY_APP_ARGV_HACK
#endif

#if defined(THEALL_ANDROID) || defined(THEALL_IOS)
#	define THEALL_LEGENDARY_ACCELEROMETER_AS_JOYSTICK_HACK
#endif

// Autotools config.h
#ifdef HAVE_CONFIG_H
#	include <../config.h>
#	undef VERSION
#	ifdef WORDS_BIGENDIAN
#		undef THEALL_LITTLE_ENDIAN
#		define THEALL_BIG_ENDIAN 1
#	else
#		undef THEALL_BIG_ENDIAN
#		define THEALL_LITTLE_ENDIAN 1
#	endif
#else
#	define THEALL_ENABLE_AUDIO
#	define THEALL_ENABLE_AUDIO_NULL
#	define THEALL_ENABLE_AUDIO_OPENAL
#	define THEALL_ENABLE_BOX2D
#	define THEALL_ENABLE_DDSPARSE
#	define THEALL_ENABLE_ENET
#	define THEALL_ENABLE_EVENT
#	define THEALL_ENABLE_EVENT_SDL
#	define THEALL_ENABLE_FILESYSTEM
#	define THEALL_ENABLE_FILESYSTEM_PHYSFS
#	define THEALL_ENABLE_FONT
#	define THEALL_ENABLE_FONT_FREETYPE
#	define THEALL_ENABLE_GRAPHICS
#	define THEALL_ENABLE_GRAPHICS_OPENGL
#	define THEALL_ENABLE_IMAGE
#	define THEALL_ENABLE_IMAGE_MAGPIE
#	define THEALL_ENABLE_JOYSTICK
#	define THEALL_ENABLE_JOYSTICK_SDL
#	define THEALL_ENABLE_KEYBOARD
#	define THEALL_ENABLE_KEYBOARD_SDL
#	define THEALL_ENABLE_LOVE
#	define THEALL_ENABLE_LUASOCKET
#	define THEALL_ENABLE_LUAUTF8
#	define THEALL_ENABLE_MATH
#	define THEALL_ENABLE_MOUSE
#	define THEALL_ENABLE_MOUSE_SDL
#	define THEALL_ENABLE_NOISE1234
#	define THEALL_ENABLE_PHYSICS
#	define THEALL_ENABLE_PHYSICS_BOX2D
#	define THEALL_ENABLE_SOUND
#	define THEALL_ENABLE_SOUND_LULLABY
#	define THEALL_ENABLE_SYSTEM
#	define THEALL_ENABLE_SYSTEM_SDL
#	define THEALL_ENABLE_THREAD
#	define THEALL_ENABLE_THREAD_SDL
#	define THEALL_ENABLE_TIMER
#	define THEALL_ENABLE_TIMER_SDL
#	define THEALL_ENABLE_TOUCH
#	define THEALL_ENABLE_TOUCH_SDL
#	define THEALL_ENABLE_UTF8
#	define THEALL_ENABLE_VIDEO
#	define THEALL_ENABLE_VIDEO_THEORA
#	define THEALL_ENABLE_WINDOW
#	define THEALL_ENABLE_WINDOW_SDL
#	define THEALL_ENABLE_WUFF
#endif

// Check we have a sane configuration
#if !defined(THEALL_WINDOWS) && !defined(THEALL_LINUX) && !defined(THEALL_IOS) && !defined(THEALL_MACOSX) && !defined(THEALL_ANDROID)
#	error Could not detect target platform
#endif
#if !defined(THEALL_LITTLE_ENDIAN) && !defined(THEALL_BIG_ENDIAN)
#	error Could not detect endianness
#endif

#endif // THEALL_CONFIG_H
