/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "variant.h"
#include "stringmap.h"

TVariant::TVariant()
	: type(NIL)
{
}

TVariant::TVariant(bool boolean)
	: type(BOOLEAN)
{
	data.boolean = boolean;
}

TVariant::TVariant(double number)
	: type(NUMBER)
{
	data.number = number;
}

TVariant::TVariant(const char *string, size_t len)
{
	if(len <= MAX_SMALL_STRING_LENGTH)
	{
		type = SMALLSTRING;
		memcpy(data.smallstring.str, string, len);
		data.smallstring.len = (uint8) len;
	}
	else
	{
		type = STRING;
		data.string = new SharedString(string, len);
	}
}

TVariant::TVariant(void *userdata)
	: type(LUSERDATA)
{
	data.userdata = userdata;
}

TVariant::TVariant(Type udatatype, void *userdata)
	: type(FUSERDATA)
    , mTypeId(udatatype)
{
//	if(udatatype != INVALID_ID)
//	{
//		Proxy *p = (Proxy *) userdata;
//		data.userdata = p->object;
//		p->object->retain();
//	}
//	else
		data.userdata = userdata;
}

// Variant gets ownership of the vector.
TVariant::TVariant(std::vector<std::pair<TVariant, TVariant>> *table)
	: type(TABLE)
{
	data.table = new SharedTable(table);
}

TVariant::TVariant(const TVariant &v)
	: type(v.type)
    , mTypeId(v.mTypeId)
	, data(v.data)
{
	if(type == STRING)
		data.string->retain();
	else if(type == FUSERDATA)
		((TObject *) data.userdata)->retain();
	else if(type == TABLE)
		data.table->retain();
}

TVariant::TVariant(TVariant &&v)
	: type(std::move(v.type))
    , mTypeId(std::move(v.mTypeId))
	, data(std::move(v.data))
{
	v.type = NIL;
}

TVariant::~TVariant()
{
	switch (type)
	{
	case STRING:
		data.string->release();
		break;
	case FUSERDATA:
		((TObject *) data.userdata)->release();
		break;
	case TABLE:
		data.table->release();
		break;
	default:
		break;
	}
}

TVariant &TVariant::operator = (const TVariant &v)
{
	if(v.type == STRING)
		v.data.string->retain();
	else if(v.type == FUSERDATA)
		((TObject *) v.data.userdata)->retain();
	else if(v.type == TABLE)
		v.data.table->retain();

	if(type == STRING)
		data.string->release();
	else if(type == FUSERDATA)
		((TObject *) v.data.userdata)->release();
	else if(type == TABLE)
		data.table->release();

	type = v.type;
	data = v.data;
    mTypeId = v.mTypeId;

    return *this;
}

bool TVariant::toBool() const
{
    return data.boolean;
}

int TVariant::toInt() const
{
    return (int)data.number;
}

double TVariant::toDouble() const
{
    return data.number;
}

void *TVariant::getUserData() const
{
    return data.userdata;
}
