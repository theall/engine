/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_VECTOR_H
#define THEALL_VECTOR_H

#include "matrix.h"

/**
 * 2D Vector class.
 *
 * @author Anders Ruud
 * @date 2006-05-13
 **/
class TVector
{
public:

	// The components.
	float x, y;

	/**
	 * Creates a new (1,1) Vector.
	 **/
    TVector();

	/**
	 * Creates a new Vector.
	 * @param x The x position/dimension.
	 * @param y The y position/dimension.
	 **/
    TVector(float x, float y);

	/**
	 * Gets the length of the Vector.
	 * @return The length of the Vector.
	 *
	 * This method requires sqrtf() and should be used
	 * carefully.
	 **/
	float getLength() const;

	/**
	 * Normalizes the Vector.
	 * @param length Desired length of the vector.
	 * @return The old length of the Vector.
	 **/
	float normalize(float length = 1.0);

	/**
	 * Gets a vector perpendicular to the Vector.
	 * To get the true (normalized) normal, use v.getNormal(1.0f / v.getLength())
	 * @return A normal to the Vector.
	 **/
    TVector getNormal() const;

	/**
	 * Gets a vector perpendicular to the Vector.
	 * To get the true (normalized) normal, use v.getNormal(1.0f / v.getLength())
	 * @param scale factor to apply.
	 * @return A normal to the Vector.
	 **/
    TVector getNormal(float scale) const;

	/**
	 * Adds a Vector to this Vector.
	 * @param v The Vector we want to add to this Vector.
	 * @return The resulting Vector.
	 **/
    TVector operator + (const TVector &v) const;

	/**
	 * Substracts a Vector to this Vector.
	 * @param v The Vector we want to subtract to this Vector.
	 * @return The resulting Vector.
	 **/
    TVector operator - (const TVector &v) const;

	/**
	 * Resizes a Vector by a scalar.
	 * @param s The scalar with which to resize the Vector.
	 * @return The resulting Vector.
	 **/
    TVector operator * (float s) const;

	/**
	 * Resizes a Vector by a scalar.
	 * @param s The scalar with which to resize the Vector.
	 * @return The resulting Vector.
	 **/
    TVector operator / (float s) const;

	/**
	 * Reverses the Vector.
	 * @return The reversed Vector.
	 **/
    TVector operator - () const;

	/**
	 * Adds a Vector to this Vector, and also saves changes in the first Vector.
	 * @param v The Vector we want to add to this Vector.
	 **/
    void operator += (const TVector &v);

	/**
	 * Subtracts a Vector to this Vector, and also saves changes in the first Vector.
	 * @param v The Vector we want to subtract to this Vector.
	 **/
    void operator -= (const TVector &v);

	/**
	 * Resizes the Vector, and also saves changes in the first Vector.
	 * @param s The scalar by which we want to resize the Vector.
	 **/
	void operator *= (float s);

	/**
	 * Resizes the Vector, and also saves changes in the first Vector.
	 * @param s The scalar by which we want to resize the Vector.
	 **/
	void operator /= (float s);

	/**
	 * Calculates the dot product of two Vectors.
	 * @return The dot product of the two Vectors.
	 **/
    float operator * (const TVector &v) const;

	/**
	 * Calculates the cross product of two Vectors.
	 * @return The cross product of the two Vectors.
	 **/
    float operator ^ (const TVector &v) const;

    bool operator == (const TVector &v) const;

    bool operator < (const TVector &v) const;
	/**
	 * Gets the x value of the Vector.
	 * @return The x value of the Vector.
	 **/
	float getX() const;

	/**
	 * Gets the x value of the Vector.
	 * @return The x value of the Vector.
	 **/
	float getY() const;

	/**
	 * Sets the x value of the Vector.
	 * @param x The x value of the Vector.
	 **/
    void setX(float _x);

	/**
	 * Sets the x value of the Vector.
	 * @param y The x value of the Vector.
	 **/
    void setY(float _y);

};

inline float TVector::getLength() const
{
	return sqrtf(x*x + y*y);
}

inline TVector TVector::getNormal() const
{
    return TVector(-y, x);
}

inline TVector TVector::getNormal(float scale) const
{
    return TVector(-y * scale, x * scale);
}

inline float TVector::normalize(float length)
{

	float length_current = getLength();

	if(length_current > 0)
		(*this) *= length / length_current;

	return length_current;
}

/**
 * Inline methods must have body in header.
 **/

inline TVector::TVector()
	: x(0.0f)
	, y(0.0f)
{
}

inline TVector::TVector(float x, float y)
	: x(x)
	, y(y)
{
}

inline TVector TVector::operator + (const TVector &v) const
{
    return TVector(x + v.x, y + v.y);
}

inline TVector TVector::operator - (const TVector &v) const
{
    return TVector(x - v.getX(), y - v.getY());
}

inline TVector TVector::operator * (float s) const
{
    return TVector(x*s, y*s);
}

inline TVector TVector::operator / (float s) const
{
    return TVector(x/s, y/s);
}

inline TVector TVector::operator - () const
{
    return TVector(-x, -y);
}

inline void TVector::operator += (const TVector &v)
{
	x += v.getX();
	y += v.getY();
}

inline void TVector::operator -= (const TVector &v)
{
	x -= v.getX();
	y -= v.getY();
}

inline void TVector::operator *= (float s)
{
	x *= s;
	y *= s;
}

inline void TVector::operator /= (float s)
{
	x /= s;
	y /= s;
}

inline float TVector::operator * (const TVector &v) const
{
	return x * v.getX() + y * v.getY();
}

inline float TVector::operator ^ (const TVector &v) const
{
	return x * v.getY() - y * v.getX();
}

inline bool TVector::operator == (const TVector &v) const
{
	return getLength() == v.getLength();
}

inline bool TVector::operator < (const TVector &v) const
{
	return getLength() < v.getLength();
}

/**
 * Accessor methods
 **/

inline float TVector::getX() const
{
	return x;
}

inline float TVector::getY() const
{
	return y;
}

inline void TVector::setX(float _x)
{
    x = _x;
}

inline void TVector::setY(float _y)
{
    y = _y;
}

#endif// THEALL_VECTOR_H
