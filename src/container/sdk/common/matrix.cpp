/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "matrix.h"

#include "utils/std_pch.h"

float getA(float arcs[16], int n)
{
    if(n==1)
    {
        return arcs[0];
    }
    float ans = 0;
    float temp[16]={0.0};
    int i, j, k;
    for(i=0;i<n;i++)
    {
        for(j=0;j<n-1;j++)
        {
            for(k=0;k<n-1;k++)
            {
                temp[j*4+k] = arcs[(j+1)*4+(k>=i?k+1:k)];
            }
        }
        float t = getA(temp, n-1);
        if(i%2==0)
        {
            ans += arcs[i]*t;
        }
        else
        {
            ans -= arcs[i]*t;
        }
    }
    return ans;
}

void getAStart(float arcs[16], int n, float ans[16])
{
    if(n==1)
    {
        ans[0] = 1;
        return;
    }
    int i, j, k, t;
    float temp[16];
    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++)
        {
            for(k=0;k<n-1;k++)
            {
                for(t=0;t<n-1;t++)
                {
                    temp[k*4+t] = arcs[(k>=i?k+1:k)*4+(t>=j?t+1:t)];
                }
            }

            int index = j*4 + i;
            ans[index] = getA(temp, n-1);
            if((i+j)%2 == 1)
            {
                ans[index] = -ans[index];
            }
        }
    }
}

bool getMatrixInverse(float src[16], float des[16])
{
    float flag=getA(src, 4);
    float t[16];
    if(flag==0)
    {
        return false;
    }
    else
    {
        getAStart(src, 4, t);
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                des[i*4+j]=t[i*4+j]/flag;
            }
        }
    }
    return true;
}

// | e0 e4 e8  e12 |
// | e1 e5 e9  e13 |
// | e2 e6 e10 e14 |
// | e3 e7 e11 e15 |

TMatrix4::TMatrix4()
{
	setIdentity();
}

TMatrix4::TMatrix4(float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky)
{
	setTransformation(x, y, angle, sx, sy, ox, oy, kx, ky);
}

TMatrix4::~TMatrix4()
{
}

//                 | e0 e4 e8  e12 |
//                 | e1 e5 e9  e13 |
//                 | e2 e6 e10 e14 |
//                 | e3 e7 e11 e15 |
// | e0 e4 e8  e12 |
// | e1 e5 e9  e13 |
// | e2 e6 e10 e14 |
// | e3 e7 e11 e15 |

TMatrix4 TMatrix4::operator * (const TMatrix4 &m) const
{
	TMatrix4 t;

	t.e[0] = (e[0]*m.e[0]) + (e[4]*m.e[1]) + (e[8]*m.e[2]) + (e[12]*m.e[3]);
	t.e[4] = (e[0]*m.e[4]) + (e[4]*m.e[5]) + (e[8]*m.e[6]) + (e[12]*m.e[7]);
	t.e[8] = (e[0]*m.e[8]) + (e[4]*m.e[9]) + (e[8]*m.e[10]) + (e[12]*m.e[11]);
	t.e[12] = (e[0]*m.e[12]) + (e[4]*m.e[13]) + (e[8]*m.e[14]) + (e[12]*m.e[15]);

	t.e[1] = (e[1]*m.e[0]) + (e[5]*m.e[1]) + (e[9]*m.e[2]) + (e[13]*m.e[3]);
	t.e[5] = (e[1]*m.e[4]) + (e[5]*m.e[5]) + (e[9]*m.e[6]) + (e[13]*m.e[7]);
	t.e[9] = (e[1]*m.e[8]) + (e[5]*m.e[9]) + (e[9]*m.e[10]) + (e[13]*m.e[11]);
	t.e[13] = (e[1]*m.e[12]) + (e[5]*m.e[13]) + (e[9]*m.e[14]) + (e[13]*m.e[15]);

	t.e[2] = (e[2]*m.e[0]) + (e[6]*m.e[1]) + (e[10]*m.e[2]) + (e[14]*m.e[3]);
	t.e[6] = (e[2]*m.e[4]) + (e[6]*m.e[5]) + (e[10]*m.e[6]) + (e[14]*m.e[7]);
	t.e[10] = (e[2]*m.e[8]) + (e[6]*m.e[9]) + (e[10]*m.e[10]) + (e[14]*m.e[11]);
	t.e[14] = (e[2]*m.e[12]) + (e[6]*m.e[13]) + (e[10]*m.e[14]) + (e[14]*m.e[15]);

	t.e[3] = (e[3]*m.e[0]) + (e[7]*m.e[1]) + (e[11]*m.e[2]) + (e[15]*m.e[3]);
	t.e[7] = (e[3]*m.e[4]) + (e[7]*m.e[5]) + (e[11]*m.e[6]) + (e[15]*m.e[7]);
	t.e[11] = (e[3]*m.e[8]) + (e[7]*m.e[9]) + (e[11]*m.e[10]) + (e[15]*m.e[11]);
	t.e[15] = (e[3]*m.e[12]) + (e[7]*m.e[13]) + (e[11]*m.e[14]) + (e[15]*m.e[15]);

	return t;
}

void TMatrix4::operator *= (const TMatrix4 &m)
{
	TMatrix4 t = (*this) * m;
    memcpy(e, t.e, sizeof(float)*16);
}

const float *TMatrix4::getElements() const
{
	return e;
}

void TMatrix4::setIdentity()
{
	memset(e, 0, sizeof(float)*16);
	e[0] = e[5] = e[10] = e[15] = 1;
}

void TMatrix4::setTranslation(float x, float y)
{
	setIdentity();
	e[12] = x;
	e[13] = y;
}

void TMatrix4::setRotation(float rad)
{
	setIdentity();
	float c = cosf(rad), s = sinf(rad);
	e[0] = c;
	e[4] = -s;
	e[1] = s;
	e[5] = c;
}

void TMatrix4::setScale(float sx, float sy)
{
	setIdentity();
	e[0] = sx;
	e[5] = sy;
}

void TMatrix4::setShear(float kx, float ky)
{
	setIdentity();
	e[1] = ky;
	e[4] = kx;
}

void TMatrix4::setTransformation(float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky)
{
	memset(e, 0, sizeof(float)*16); // zero out matrix
	float c = cosf(angle), s = sinf(angle);
	// matrix multiplication carried out on paper:
	// |1     x| |c -s    | |sx       | | 1 ky    | |1     -ox|
	// |  1   y| |s  c    | |   sy    | |kx  1    | |  1   -oy|
	// |    1  | |     1  | |      1  | |      1  | |    1    |
	// |      1| |       1| |        1| |        1| |       1 |
	//   move      rotate      scale       skew       origin
	e[10] = e[15] = 1.0f;
	e[0]  = c * sx - ky * s * sy; // = a
	e[1]  = s * sx + ky * c * sy; // = b
	e[4]  = kx * c * sx - s * sy; // = c
	e[5]  = kx * s * sx + c * sy; // = d
	e[12] = x - ox * e[0] - oy * e[4];
	e[13] = y - ox * e[1] - oy * e[5];
}

void TMatrix4::translate(float x, float y)
{
	TMatrix4 t;
	t.setTranslation(x, y);
    operator *=(t);
}

void TMatrix4::rotate(float rad)
{
	TMatrix4 t;
	t.setRotation(rad);
    operator *=(t);
}

void TMatrix4::scale(float sx, float sy)
{
	TMatrix4 t;
	t.setScale(sx, sy);
    operator *=(t);
}

void TMatrix4::shear(float kx, float ky)
{
	TMatrix4 t;
	t.setShear(kx,ky);
    operator *=(t);
}

TMatrix4 TMatrix4::ortho(float left, float right, float bottom, float top)
{
	TMatrix4 m;

	m.e[0] = 2.0f / (right - left);
	m.e[5] = 2.0f / (top - bottom);
	m.e[10] = -1.0;

	m.e[12] = -(right + left) / (right - left);
	m.e[13] = -(top + bottom) / (top - bottom);

    return m;
}

TMatrix4 TMatrix4::getReverseMatrix()
{
    TMatrix4 t;
    bool ret = getMatrixInverse(e, (float*)t.getElements());
    if(!ret)
        t.setIdentity();
    return t;
}

void TMatrix4::getXY(float x, float y, float &rx, float &ry)
{
    float rz = 0;
    getXYZ(x, y, 0, rx, ry, rz);
}

void TMatrix4::getXYZ(float x, float y, float z, float &rx, float &ry, float &rz)
{
    rx = x*e[0] + y*e[4] + z*e[8] + e[12];
    ry = x*e[1] + y*e[5] + z*e[9] + e[13];
    rz = x*e[2] + y*e[6] + z*e[10] + e[14];
}

/**
 * | e0 e3 e6 |
 * | e1 e4 e7 |
 * | e2 e5 e8 |
 **/
Matrix3::Matrix3()
{
	setIdentity();
}

Matrix3::Matrix3(const TMatrix4 &mat4)
{
	const float *mat4elems = mat4.getElements();

	// Column 0.
	e[0] = mat4elems[0];
	e[1] = mat4elems[1];
	e[2] = mat4elems[2];

	// Column 1.
	e[3] = mat4elems[4];
	e[4] = mat4elems[5];
	e[5] = mat4elems[6];

	// Column 2.
	e[6] = mat4elems[8];
	e[7] = mat4elems[9];
	e[8] = mat4elems[10];
}

Matrix3::Matrix3(float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky)
{
	setTransformation(x, y, angle, sx, sy, ox, oy, kx, ky);
}

Matrix3::~Matrix3()
{
}

void Matrix3::setIdentity()
{
	memset(e, 0, sizeof(float) * 9);
	e[8] = e[4] = e[0] = 1.0f;
}

Matrix3 Matrix3::operator * (const Matrix3 &m) const
{
	Matrix3 t;

	t.e[0] = (e[0]*m.e[0]) + (e[3]*m.e[1]) + (e[6]*m.e[2]);
	t.e[3] = (e[0]*m.e[3]) + (e[3]*m.e[4]) + (e[6]*m.e[5]);
	t.e[6] = (e[0]*m.e[6]) + (e[3]*m.e[7]) + (e[6]*m.e[8]);

	t.e[1] = (e[1]*m.e[0]) + (e[4]*m.e[1]) + (e[7]*m.e[2]);
	t.e[4] = (e[1]*m.e[3]) + (e[4]*m.e[4]) + (e[7]*m.e[5]);
	t.e[7] = (e[1]*m.e[6]) + (e[4]*m.e[7]) + (e[7]*m.e[8]);

	t.e[2] = (e[2]*m.e[0]) + (e[5]*m.e[1]) + (e[8]*m.e[2]);
	t.e[5] = (e[2]*m.e[3]) + (e[5]*m.e[4]) + (e[8]*m.e[5]);
	t.e[8] = (e[2]*m.e[6]) + (e[5]*m.e[7]) + (e[8]*m.e[8]);

	return t;
}

void Matrix3::operator *= (const Matrix3 &m)
{
	Matrix3 t = (*this) * m;
	memcpy(e, t.e, sizeof(float) * 9);
}

const float *Matrix3::getElements() const
{
	return e;
}

Matrix3 Matrix3::transposedInverse() const
{
	// e0 e3 e6
	// e1 e4 e7
	// e2 e5 e8

	float det = e[0] * (e[4]*e[8] - e[7]*e[5])
	          - e[1] * (e[3]*e[8] - e[5]*e[6])
	          + e[2] * (e[3]*e[7] - e[4]*e[6]);

	float invdet = 1.0f / det;

	Matrix3 m;

	m.e[0] =  invdet * (e[4]*e[8] - e[7]*e[5]);
	m.e[3] = -invdet * (e[1]*e[8] - e[2]*e[7]);
	m.e[6] =  invdet * (e[1]*e[5] - e[2]*e[4]);
	m.e[1] = -invdet * (e[3]*e[8] - e[5]*e[6]);
	m.e[4] =  invdet * (e[0]*e[8] - e[2]*e[6]);
	m.e[7] = -invdet * (e[0]*e[5] - e[3]*e[2]);
	m.e[2] =  invdet * (e[3]*e[7] - e[6]*e[4]);
	m.e[5] = -invdet * (e[0]*e[7] - e[6]*e[1]);
	m.e[8] =  invdet * (e[0]*e[4] - e[3]*e[1]);

	return m;
}

void Matrix3::setTransformation(float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky)
{
	float c = cosf(angle), s = sinf(angle);
	// matrix multiplication carried out on paper:
	// |1    x| |c -s  | |sx     | | 1 ky  | |1   -ox|
	// |  1  y| |s  c  | |   sy  | |kx  1  | |  1 -oy|
	// |     1| |     1| |      1| |      1| |     1 |
	//   move    rotate    scale     skew      origin
	e[0] = c * sx - ky * s * sy; // = a
	e[1] = s * sx + ky * c * sy; // = b
	e[3] = kx * c * sx - s * sy; // = c
	e[4] = kx * s * sx + c * sy; // = d
	e[6] = x - ox * e[0] - oy * e[3];
	e[7] = y - ox * e[1] - oy * e[4];

	e[2] = e[5] = 0.0f;
	e[8] = 1.0f;
}

