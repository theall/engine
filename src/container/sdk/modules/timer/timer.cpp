/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "timer.h"

#include <SDL2/SDL.h>

#include "common/config.h"
#include "common/int.h"

#if defined(THEALL_WINDOWS)
#include <windows.h>
#elif defined(THEALL_MACOSX) || defined(THEALL_IOS)
#include <mach/mach_time.h>
#include <sys/time.h>
#elif defined(THEALL_LINUX)
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#endif

#if defined(THEALL_LINUX)
static inline double getTimeOfDay()
{
    timeval t;
    gettimeofday(&t, NULL);
    return (double) t.tv_sec + (double) t.tv_usec / 1000000.0;
}
#endif

TTimer *TTimer::mInstance=NULL;

TTimer::TTimer()
    : mCurrTime(0)
    , mPrevFpsUpdate(0)
    , mFps(0)
    , mAverageDelta(0)
    , mFpsUpdateFrequency(1)
    , mFrames(0)
    , mDt(0)
{
    mPrevFpsUpdate = mCurrTime = getTime();
}

TTimer::~TTimer()
{

}

void TTimer::step()
{
    // Frames rendered
    mFrames++;

    // "Current" time is previous time by now.
    mPrevTime = mCurrTime;

    // Get time from system.
    mCurrTime = getTime();

    // Convert to number of seconds.
    mDt = mCurrTime - mPrevTime;

    double timeSinceLast = mCurrTime - mPrevFpsUpdate;
    // Update FPS?
    if(timeSinceLast > mFpsUpdateFrequency)
    {
        mFps = int((mFrames/timeSinceLast) + 0.5);
        mAverageDelta = timeSinceLast/mFrames;
        mPrevFpsUpdate = mCurrTime;
        mFrames = 0;
    }
}

void TTimer::sleep(unsigned int microSeconds) const
{
    SDL_Delay(microSeconds);
}

double TTimer::getDelta() const
{
    return mDt;
}

int TTimer::getFPS() const
{
    return mFps;
}

double TTimer::getAverageDelta() const
{
    return mAverageDelta;
}

double TTimer::getTimerPeriod()
{
#if defined(THEALL_MACOSX) || defined(THEALL_IOS)
    mach_timebase_info_data_t info;
    mach_timebase_info(&info);
    return (double) info.numer / (double) info.denom / 1000000000.0;
#elif defined(THEALL_WINDOWS)
    LARGE_INTEGER temp;
    if(QueryPerformanceFrequency(&temp) != 0 && temp.QuadPart != 0)
        return 1.0 / (double) temp.QuadPart;
#endif
    return 0;
}

double TTimer::getTime()
{
    // The timer period (reciprocal of the frequency.)
    static const double timerPeriod = getTimerPeriod();

#if defined(THEALL_LINUX)
    double mt;
    // Check for POSIX timers and monotonic clocks. If not supported, use the gettimeofday fallback.
#if _POSIX_TIMERS > 0 && defined(_POSIX_MONOTONIC_CLOCK) \
&& (defined(CLOCK_MONOTONIC_RAW) || defined(CLOCK_MONOTONIC))
    timespec t;
#ifdef CLOCK_MONOTONIC_RAW
    clockid_t clk_id = CLOCK_MONOTONIC_RAW;
#else
    clockid_t clk_id = CLOCK_MONOTONIC;
#endif
    if(clock_gettime(clk_id, &t) == 0)
        mt = (double) t.tv_sec + (double) t.tv_nsec / 1000000000.0;
    else
#endif
        mt = getTimeOfDay();
    return mt;
#elif defined(THEALL_MACOSX) || defined(THEALL_IOS)
    return (double) mach_absolute_time() * timerPeriod;
#elif defined(THEALL_WINDOWS)
    LARGE_INTEGER microTime;
    QueryPerformanceCounter(&microTime);
    return (double) microTime.QuadPart * timerPeriod;
#endif
}
