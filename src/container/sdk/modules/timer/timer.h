/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_TIMER_SDL_TIMER_H
#define THEALL_TIMER_SDL_TIMER_H

#include "common/object.h"

class TTimer : public TObject
{
    DECLARE_INSTANCE(TTimer)

public:

    TTimer();
    ~TTimer();

    /**
     * Measures the time between this call and the previous call,
     * and updates internal values accordinly.
     **/
    void step();

    /**
     * Tries to sleep for the specified amount of time. The precision is
     * usually 1ms.
     * @param seconds The number of seconds to sleep for.
     **/
    void sleep(unsigned int microSeconds) const;

    /**
     * Gets the time between the last two frames, assuming step is called
     * each frame.
     **/
    double getDelta() const;

    /**
     * Gets the average FPS over the last second. Beucase the value is only updated
     * once per second, it does not look erratic when displayed on screen.
     * @return The "current" FPS.
     **/
    int getFPS() const;

    /**
     * Gets the average delta time (seconds per frame) over the last second.
     **/
    double getAverageDelta() const;

    /**
     * Gets the amount of time passed since an unspecified time. Useful for
     * profiling code or measuring intervals. The time is microsecond-precise,
     * and increases monotonically.
     * @return The time (in seconds)
     **/
    static double getTime();

private:

    // Frame delta vars.
    double mCurrTime;
    double mPrevTime;
    double mPrevFpsUpdate;

    // Updated with a certain frequency.
    int mFps;
    double mAverageDelta;

    // The frequency by which to update the FPS.
    double mFpsUpdateFrequency;

    // Frames since last FPS update.
    int mFrames;

    // The current timestep.
    double mDt;

    // Returns the timer period on some platforms.
    static double getTimerPeriod();
}; // Timer

#endif // THEALL_TIMER_SDL_TIMER_H
