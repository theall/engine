/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "video.h"

#include "shader.h"




TVideo::TVideo(TVideoStream *stream)
    : mStream(stream)
    , mFilter(TTexture::getDefaultFilter())
{
    mFilter.mipmap = TTexture::FILTER_NONE;

	stream->fillBackBuffer();

	for (int i = 0; i < 4; i++)
        mVertices[i].r = mVertices[i].g = mVertices[i].b = mVertices[i].a = 255;

	// Vertices are ordered for use with triangle strips:
	// 0----2
	// |  / |
	// | /  |
	// 1----3
    mVertices[0].x = 0.0f;
    mVertices[0].y = 0.0f;
    mVertices[1].x = 0.0f;
    mVertices[1].y = (float) stream->getHeight();
    mVertices[2].x = (float) stream->getWidth();
    mVertices[2].y = 0.0f;
    mVertices[3].x = (float) stream->getWidth();
    mVertices[3].y = (float) stream->getHeight();

    mVertices[0].s = 0.0f;
    mVertices[0].t = 0.0f;
    mVertices[1].s = 0.0f;
    mVertices[1].t = 1.0f;
    mVertices[2].s = 1.0f;
    mVertices[2].t = 0.0f;
    mVertices[3].s = 1.0f;
    mVertices[3].t = 1.0f;

	loadVolatile();
}

TVideo::~TVideo()
{
    unloadVolatile();
}

void TVideo::play()
{
    getStream()->play();
}

void TVideo::pause()
{
    getStream()->pause();
}

void TVideo::seek(int offset)
{
    getStream()->seek(offset);
}

void TVideo::rewind()
{
    getStream()->rewind();
}

void TVideo::tell()
{
    getStream()->tell();
}

bool TVideo::isPlaying()
{
    return getStream()->isPlaying();
}

bool TVideo::loadVolatile()
{
    glGenTextures(3, &mTextures[0]);

	// Create the textures using the initial frame data.
    auto frame = (const TVideoStream::Frame*) mStream->getFrontBuffer();

	int widths[3]  = {frame->yw, frame->cw, frame->cw};
	int heights[3] = {frame->yh, frame->ch, frame->ch};

	const unsigned char *data[3] = {frame->yplane, frame->cbplane, frame->crplane};

	TTexture::Wrap wrap; // Clamp wrap mode.

	for (int i = 0; i < 3; i++)
	{
        gl.bindTexture(mTextures[i]);

        gl.setTextureFilter(mFilter);
		gl.setTextureWrap(wrap);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, widths[i], heights[i], 0,
		             GL_LUMINANCE, GL_UNSIGNED_BYTE, data[i]);
	}

	return true;
}

void TVideo::unloadVolatile()
{
	for (int i = 0; i < 3; i++)
	{
        gl.deleteTexture(mTextures[i]);
        mTextures[i] = 0;
	}
}

TVideoStream *TVideo::getStream()
{
    return mStream;
}

void TVideo::draw(float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky)
{
	update();

	TShader *shader = TShader::current;
	bool defaultShader = (shader == TShader::defaultShader);
	if(defaultShader)
	{
		// If we're still using the default shader, substitute the video version
		TShader::defaultVideoShader->attach();
		shader = TShader::defaultVideoShader;
	}

    shader->setVideoTextures(mTextures[0], mTextures[1], mTextures[2]);

	TOpenGL::TempTransform transform(gl);
	transform.get() *= TMatrix4(x, y, angle, sx, sy, ox, oy, kx, ky);

	gl.useVertexAttribArrays(ATTRIBFLAG_POS | ATTRIBFLAG_TEXCOORD);

    glVertexAttribPointer(ATTRIB_POS, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), &mVertices[0].x);
    glVertexAttribPointer(ATTRIB_TEXCOORD, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), &mVertices[0].s);

	gl.prepareDraw();
	gl.drawArrays(GL_TRIANGLE_STRIP, 0, 4);

	// If we were using the default shader, reattach it
	if(defaultShader)
		TShader::defaultShader->attach();
}

void TVideo::update()
{
    bool bufferschanged = mStream->swapBuffers();
    mStream->fillBackBuffer();

	if(bufferschanged)
	{
        auto frame = (const TVideoStream::Frame*) mStream->getFrontBuffer();

		int widths[3]  = {frame->yw, frame->cw, frame->cw};
		int heights[3] = {frame->yh, frame->ch, frame->ch};

		const unsigned char *data[3] = {frame->yplane, frame->cbplane, frame->crplane};

		for (int i = 0; i < 3; i++)
		{
            gl.bindTexture(mTextures[i]);
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, widths[i], heights[i],
			                GL_LUMINANCE, GL_UNSIGNED_BYTE, data[i]);
		}
	}
}

TSource *TVideo::getSource()
{
    return mSource;
}

void TVideo::setSource(TSource *source)
{
    mSource = source;
    //getStream()->setSync(source);
}

int TVideo::getWidth() const
{
    return mStream->getWidth();
}

int TVideo::getHeight() const
{
    return mStream->getHeight();
}

void TVideo::setFilter(const TTexture::Filter &f)
{
	if(!TTexture::validateFilter(f, false))
		throw TException("Invalid texture filter.");

    mFilter = f;

	for (int i = 0; i < 3; i++)
	{
        gl.bindTexture(mTextures[i]);
        gl.setTextureFilter(mFilter);
	}
}

const TTexture::Filter &TVideo::getFilter() const
{
    return mFilter;
}

