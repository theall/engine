/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "image.h"

#include "graphics.h"
#include "common/int.h"

// STD
#include <algorithm> // for min/max

#ifdef THEALL_ANDROID
// log2 is not declared in the math.h shipped with the Android NDK
#include <cmath>
inline double log2(double n)
{ 
	// log(n)/log(2) is log2.  
	return std::log(n) / std::log(2);
}
#endif

int TDrawableImage::imageCount = 0;

float TDrawableImage::mMaxMipmapSharpness = 0.0f;

TTexture::FilterMode TDrawableImage::mDefaultMipmapFilter = TTexture::FILTER_LINEAR;
float TDrawableImage::mDefaultMipmapSharpness = 0.0f;

static int getMipmapCount(int basewidth, int baseheight)
{
	return (int) log2(std::max(basewidth, baseheight)) + 1;
}

template <typename T>
static bool verifyMipmapLevels(const std::vector<T> &miplevels)
{
	int numlevels = (int) miplevels.size();

	if(numlevels == 1)
		return false;

	int width  = miplevels[0]->getWidth();
	int height = miplevels[0]->getHeight();

	int expectedlevels = getMipmapCount(width, height);

	// All mip levels must be present when not using auto-generated mipmaps.
	if(numlevels != expectedlevels)
		throw TException("Image does not have all required mipmap levels (expected %d, got %d)", expectedlevels, numlevels);

	// Verify the size of each mip level.
	for (int i = 1; i < numlevels; i++)
	{
		width  = std::max(width / 2, 1);
		height = std::max(height / 2, 1);

		if(miplevels[i]->getWidth() != width)
			throw TException("Width of image mipmap level %d is incorrect (expected %d, got %d)", i+1, width, miplevels[i]->getWidth());
		if(miplevels[i]->getHeight() != height)
			throw TException("Height of image mipmap level %d is incorrect (expected %d, got %d)", i+1, height, miplevels[i]->getHeight());
	}

	return true;
}

TDrawableImage::TDrawableImage(const std::vector<TImageData *> &imagedata, const Flags &flags)
	: texture(0)
    , mMipmapSharpness(mDefaultMipmapSharpness)
    , mIsCompressed(false)
    , mFlags(flags)
    , mIsRGB(false)
    , mUsingDefaultTexture(false)
    , mTextureMemorySize(0)
{
	if(imagedata.empty())
        throw TException("Image data is empty.");

	mWidth = imagedata[0]->getWidth();
	mHeight = imagedata[0]->getHeight();

	if(verifyMipmapLevels(imagedata))
        mFlags.mipmaps = true;

	for (const auto &id : imagedata)
        mData.push_back(id);

	preload();
	loadVolatile();

	++imageCount;
}

TDrawableImage::TDrawableImage(const std::vector<TCompressedImageData *> &compresseddata, const Flags &flags)
	: texture(0)
    , mMipmapSharpness(mDefaultMipmapSharpness)
    , mIsCompressed(true)
    , mFlags(flags)
    , mIsRGB(false)
    , mUsingDefaultTexture(false)
    , mTextureMemorySize(0)
{
	mWidth = compresseddata[0]->getWidth(0);
	mHeight = compresseddata[0]->getHeight(0);

	if(verifyMipmapLevels(compresseddata))
        mFlags.mipmaps = true;
	else if(flags.mipmaps && getMipmapCount(mWidth, mHeight) != compresseddata[0]->getMipmapCount())
	{
		if(compresseddata[0]->getMipmapCount() == 1)
            mFlags.mipmaps = false;
		else
		{
			throw TException("Image cannot have mipmaps: compressed image data does not have all required mipmap levels (expected %d, got %d)",
			                      getMipmapCount(mWidth, mHeight),
			                      compresseddata[0]->getMipmapCount());
		}
	}

	for (const auto &cd : compresseddata)
	{
        mCData.push_back(cd);
        if(cd->getFormat() != mCData[0]->getFormat())
			throw TException("All image mipmap levels must have the same format.");
	}

	preload();
	loadVolatile();

	++imageCount;
}

TDrawableImage::~TDrawableImage()
{
	unloadVolatile();
	--imageCount;
}

void TDrawableImage::preload()
{
	for (int i = 0; i < 4; i++)
		mVertices[i].r = mVertices[i].g = mVertices[i].b = mVertices[i].a = 255;

	// Vertices are ordered for use with triangle strips:
	// 0----2
	// |  / |
	// | /  |
	// 1----3
	mVertices[0].x = 0.0f;
	mVertices[0].y = 0.0f;
	mVertices[1].x = 0.0f;
	mVertices[1].y = (float) mHeight;
	mVertices[2].x = (float) mWidth;
	mVertices[2].y = 0.0f;
	mVertices[3].x = (float) mWidth;
	mVertices[3].y = (float) mHeight;

	mVertices[0].s = 0.0f;
	mVertices[0].t = 0.0f;
	mVertices[1].s = 0.0f;
	mVertices[1].t = 1.0f;
	mVertices[2].s = 1.0f;
	mVertices[2].t = 0.0f;
	mVertices[3].s = 1.0f;
	mVertices[3].t = 1.0f;

    if(mFlags.mipmaps) {
        mFilter.mipmap = mDefaultMipmapFilter;
    }

    if(!TAbstractGraphics::isGammaCorrect())
        mFlags.linear = false;

    if(TAbstractGraphics::isGammaCorrect() && !mFlags.linear)
        mIsRGB = true;
	else
        mIsRGB = false;
}

void TDrawableImage::generateMipmaps()
{
	// The GL_GENERATE_MIPMAP texparameter is set in loadVolatile if we don't
	// have support for glGenerateMipmap.
    if(mFlags.mipmaps && !isCompressed() &&
		(GLAD_ES_VERSION_2_0 || GLAD_VERSION_3_0 || GLAD_ARB_framebuffer_object))
	{
		if(gl.bugs.generateMipmapsRequiresTexture2DEnable)
			glEnable(GL_TEXTURE_2D);

		glGenerateMipmap(GL_TEXTURE_2D);
	}
}

void TDrawableImage::loadDefaultTexture()
{
    mUsingDefaultTexture = true;

	gl.bindTexture(texture);
	setFilter(mFilter);

	// A nice friendly checkerboard to signify invalid textures...
	GLubyte px[] = {0xFF,0xFF,0xFF,0xFF, 0xFF,0xA0,0xA0,0xFF,
	                0xFF,0xA0,0xA0,0xFF, 0xFF,0xFF,0xFF,0xFF};

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, px);
}

void TDrawableImage::loadFromCompressedData()
{
    GLenum iformat = getCompressedFormat(mCData[0]->getFormat(), mIsRGB);

    if(TAbstractGraphics::isGammaCorrect() && !mIsRGB) {
        mFlags.linear = true;
    }

	int count = 1;

    if(mFlags.mipmaps && mCData.size() > 1)
        count = (int) mCData.size();
    else if(mFlags.mipmaps) {
        count = mCData[0]->getMipmapCount();
    }

	for (int i = 0; i < count; i++)
	{
        // Compressed image mipmaps can come from separate TCompressedImageData
		// objects, or all from a single object.
        auto cd = mCData.size() > 1 ? mCData[i].get() : mCData[0].get();
        int datamip = mCData.size() > 1 ? 0 : i;

		glCompressedTexImage2D(GL_TEXTURE_2D, i, iformat, cd->getWidth(datamip),
		                       cd->getHeight(datamip), 0,
		                       (GLsizei) cd->getSize(datamip), cd->getData(datamip));
	}
}

void TDrawableImage::loadFromImageData()
{
    GLenum iformat = mIsRGB ? GL_SRGB8_ALPHA8 : GL_RGBA8;
	GLenum format  = GL_RGBA;

	// in GLES2, the internalformat and format params of TexImage have to match.
	if(GLAD_ES_VERSION_2_0 && !GLAD_ES_VERSION_3_0)
	{
        format  = mIsRGB ? GL_SRGB_ALPHA : GL_RGBA;
		iformat = format;
	}

    int mipcount = mFlags.mipmaps ? (int) mData.size() : 1;

	for (int i = 0; i < mipcount; i++)
	{
        TImageData *id = mData[i].get();
		TLock lock(id->getMutex());

		glTexImage2D(GL_TEXTURE_2D, i, iformat, id->getWidth(), id->getHeight(),
		             0, format, GL_UNSIGNED_BYTE, id->getData());
	}

    if(mData.size() <= 1)
		generateMipmaps();
}

bool TDrawableImage::loadVolatile()
{
    TOpenGL::TempDebugGroup debuggroup("Image load");

    if(isCompressed() && !hasCompressedTextureSupport(mCData[0]->getFormat(), mIsRGB))
	{
		const char *str;
        if(TCompressedImageData::getConstant(mCData[0]->getFormat(), str))
		{
			throw TException("Cannot create image: "
                                  "%s%s compressed images are not supported on this system.", mIsRGB ? "sRGB " : "", str);
		}
		else
			throw TException("cannot create image: format is not supported on this system.");
	}
	else if(!isCompressed())
	{
        if(mIsRGB && !hasSRGBSupport())
			throw TException("sRGB images are not supported on this system.");

		// GL_EXT_sRGB doesn't support glGenerateMipmap for sRGB textures.
        if(mIsRGB && (GLAD_ES_VERSION_2_0 && GLAD_EXT_sRGB && !GLAD_ES_VERSION_3_0)
            && mData.size() <= 1)
		{
            mFlags.mipmaps = false;
			mFilter.mipmap = FILTER_NONE;
		}
	}

	// NPOT textures don't support mipmapping without full NPOT support.
	if((GLAD_ES_VERSION_2_0 && !(GLAD_ES_VERSION_3_0 || GLAD_OES_texture_npot))
		&& (mWidth != nextP2(mWidth) || mHeight != nextP2(mHeight)))
	{
        mFlags.mipmaps = false;
		mFilter.mipmap = FILTER_NONE;
	}

    if(mMaxMipmapSharpness == 0.0f && GLAD_VERSION_1_4) {
        glGetFloatv(GL_MAX_TEXTURE_LOD_BIAS, &mMaxMipmapSharpness);
    }

	glGenTextures(1, &texture);
	gl.bindTexture(texture);

	setFilter(mFilter);
	setWrap(mWrap);
    setMipmapSharpness(mMipmapSharpness);

	// Use a default texture if the size is too big for the system.
	if(mWidth > gl.getMaxTextureSize() || mHeight > gl.getMaxTextureSize())
	{
		loadDefaultTexture();
		return true;
	}

    if(!mFlags.mipmaps && (GLAD_ES_VERSION_3_0 || GLAD_VERSION_1_0))
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

    if(mFlags.mipmaps && !isCompressed() && mData.size() <= 1 &&
		!(GLAD_ES_VERSION_2_0 || GLAD_VERSION_3_0 || GLAD_ARB_framebuffer_object))
	{
		// Auto-generate mipmaps every time the texture is modified, if
		// glGenerateMipmap isn't supported.
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	}

	while (glGetError() != GL_NO_ERROR); // Clear errors.

	try
	{
		if(isCompressed())
			loadFromCompressedData();
		else
			loadFromImageData();

		GLenum glerr = glGetError();
		if(glerr != GL_NO_ERROR)
            throw TException("Cannot create image (OpenGL error: %s)", TOpenGL::errorString(glerr));
	}
	catch (TException &)
	{
		gl.deleteTexture(texture);
		texture = 0;
		throw;
	}

    size_t prevmemsize = mTextureMemorySize;

	if(isCompressed())
        mTextureMemorySize = mCData[0]->getSize();
	else
        mTextureMemorySize = mData[0]->getSize();

    if(mFlags.mipmaps)
        mTextureMemorySize *= 1.33334;

    gl.updateTextureMemorySize(prevmemsize, mTextureMemorySize);

    mUsingDefaultTexture = false;
	return true;
}

void TDrawableImage::unloadVolatile()
{
	if(texture == 0)
		return;

	gl.deleteTexture(texture);
	texture = 0;

    gl.updateTextureMemorySize(mTextureMemorySize, 0);
    mTextureMemorySize = 0;
}

void TDrawableImage::draw(float x, float y)
{
    TMatrix4 t(x, y, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0);

    drawv(t, mVertices);
}

bool TDrawableImage::refresh(int xoffset, int yoffset, int w, int h)
{
	// No effect if the texture hasn't been created yet.
    if(texture == 0 || mUsingDefaultTexture)
		return false;

	if(xoffset < 0 || yoffset < 0 || w <= 0 || h <= 0 ||
		(xoffset + w) > mWidth || (yoffset + h) > mHeight)
	{
		throw TException("Invalid rectangle dimensions.");
	}

    TOpenGL::TempDebugGroup debuggroup("Image refresh");

	gl.bindTexture(texture);

	if(isCompressed())
	{
		loadFromCompressedData();
		return true;
	}

	GLenum format = GL_RGBA;

	// In ES2, the format parameter of TexSubImage must match the internal
	// format of the texture.
    if(mIsRGB && (GLAD_ES_VERSION_2_0 && !GLAD_ES_VERSION_3_0))
		format = GL_SRGB_ALPHA;

    int mipcount = mFlags.mipmaps ? (int) mData.size() : 1;

	// Reupload the sub-rectangle of each mip level (if we have custom mipmaps.)
	for (int i = 0; i < mipcount; i++)
	{
        const TPixel *pdata = (const TPixel *) mData[i]->getData();
        pdata += yoffset * mData[i]->getWidth() + xoffset;

        TLock lock(mData[i]->getMutex());
		glTexSubImage2D(GL_TEXTURE_2D, i, xoffset, yoffset, w, h, format,
						GL_UNSIGNED_BYTE, pdata);

		xoffset /= 2;
		yoffset /= 2;
		w = std::max(w / 2, 1);
		h = std::max(h / 2, 1);
	}

    if(mData.size() <= 1)
		generateMipmaps();

	return true;
}

void TDrawableImage::drawv(const TMatrix4 &t, const Vertex *v)
{
    TOpenGL::TempDebugGroup debuggroup("Image draw");

    TOpenGL::TempTransform transform(gl);
	transform.get() *= t;

	gl.bindTexture(texture);

	gl.useVertexAttribArrays(ATTRIBFLAG_POS | ATTRIBFLAG_TEXCOORD);

	glVertexAttribPointer(ATTRIB_POS, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), &v[0].x);
	glVertexAttribPointer(ATTRIB_TEXCOORD, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), &v[0].s);

	gl.prepareDraw();
	gl.drawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void TDrawableImage::draw(float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky)
{
	TMatrix4 t(x, y, angle, sx, sy, ox, oy, kx, ky);

	drawv(t, mVertices);
}

void TDrawableImage::drawq(TQuad *quad, float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky)
{
	TMatrix4 t(x, y, angle, sx, sy, ox, oy, kx, ky);

	drawv(t, quad->getVertices());
}

const void *TDrawableImage::getHandle() const
{
	return &texture;
}

const std::vector<StrongRef<TImageData>> &TDrawableImage::getImageData() const
{
    return mData;
}

const std::vector<StrongRef<TCompressedImageData>> &TDrawableImage::getCompressedData() const
{
    return mCData;
}

void TDrawableImage::setFilter(const TTexture::Filter &f)
{
    if(!validateFilter(f, mFlags.mipmaps))
	{
        if(f.mipmap != FILTER_NONE && !mFlags.mipmaps)
			throw TException("Non-mipmapped image cannot have mipmap filtering.");
		else
			throw TException("Invalid texture filter.");
	}

	mFilter = f;

	// We don't want filtering or (attempted) mipmaps on the default texture.
    if(mUsingDefaultTexture)
	{
		mFilter.mipmap = FILTER_NONE;
		mFilter.min = mFilter.mag = FILTER_NEAREST;
	}

	gl.bindTexture(texture);
	gl.setTextureFilter(mFilter);
}

bool TDrawableImage::setWrap(const TTexture::Wrap &w)
{
	bool success = true;
	mWrap = w;

	if((GLAD_ES_VERSION_2_0 && !(GLAD_ES_VERSION_3_0 || GLAD_OES_texture_npot))
		&& (mWidth != nextP2(mWidth) || mHeight != nextP2(mHeight)))
	{
		if(mWrap.s != WRAP_CLAMP || mWrap.t != WRAP_CLAMP)
			success = false;

		// If we only have limited NPOT support then the wrap mode must be CLAMP.
		mWrap.s = mWrap.t = WRAP_CLAMP;
	}

	if(!gl.isClampZeroTextureWrapSupported())
	{
		if(mWrap.s == WRAP_CLAMP_ZERO)
			mWrap.s = WRAP_CLAMP;
		if(mWrap.t == WRAP_CLAMP_ZERO)
			mWrap.t = WRAP_CLAMP;
	}

	gl.bindTexture(texture);
	gl.setTextureWrap(mWrap);

	return success;
}

void TDrawableImage::setMipmapSharpness(float sharpness)
{
	// OpenGL ES doesn't support LOD bias via glTexParameter.
	if(!GLAD_VERSION_1_4)
		return;

	// LOD bias has the range (-maxbias, maxbias)
    mMipmapSharpness = std::min(std::max(sharpness, -mMaxMipmapSharpness + 0.01f), mMaxMipmapSharpness - 0.01f);

	gl.bindTexture(texture);

	// negative bias is sharper
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -mMipmapSharpness);
}

float TDrawableImage::getMipmapSharpness() const
{
    return mMipmapSharpness;
}

const TDrawableImage::Flags &TDrawableImage::getFlags() const
{
    return mFlags;
}

void TDrawableImage::setDefaultMipmapSharpness(float sharpness)
{
    mDefaultMipmapSharpness = sharpness;
}

float TDrawableImage::getDefaultMipmapSharpness()
{
    return mDefaultMipmapSharpness;
}

void TDrawableImage::setDefaultMipmapFilter(TTexture::FilterMode f)
{
    mDefaultMipmapFilter = f;
}

TTexture::FilterMode TDrawableImage::getDefaultMipmapFilter()
{
    return mDefaultMipmapFilter;
}

bool TDrawableImage::isCompressed() const
{
    return mIsCompressed;
}

GLenum TDrawableImage::getCompressedFormat(TCompressedImageData::Format cformat, bool &isSRGB) const
{
	switch (cformat)
	{
    case TCompressedImageData::FORMAT_DXT1:
		return isSRGB ? GL_COMPRESSED_SRGB_S3TC_DXT1_EXT : GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
    case TCompressedImageData::FORMAT_DXT3:
		return isSRGB ? GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT : GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
    case TCompressedImageData::FORMAT_DXT5:
		return isSRGB ? GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT : GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
    case TCompressedImageData::FORMAT_BC4:
		isSRGB = false;
		return GL_COMPRESSED_RED_RGTC1;
    case TCompressedImageData::FORMAT_BC4s:
		isSRGB = false;
		return GL_COMPRESSED_SIGNED_RED_RGTC1;
    case TCompressedImageData::FORMAT_BC5:
		isSRGB = false;
		return GL_COMPRESSED_RG_RGTC2;
    case TCompressedImageData::FORMAT_BC5s:
		isSRGB = false;
		return GL_COMPRESSED_SIGNED_RG_RGTC2;
    case TCompressedImageData::FORMAT_BC6H:
		isSRGB = false;
		return GL_COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT;
    case TCompressedImageData::FORMAT_BC6Hs:
		isSRGB = false;
		return GL_COMPRESSED_RGB_BPTC_SIGNED_FLOAT;
    case TCompressedImageData::FORMAT_BC7:
		return isSRGB ? GL_COMPRESSED_SRGB_ALPHA_BPTC_UNORM : GL_COMPRESSED_RGBA_BPTC_UNORM;
    case TCompressedImageData::FORMAT_PVR1_RGB2:
		return isSRGB ? GL_COMPRESSED_SRGB_PVRTC_2BPPV1_EXT : GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
    case TCompressedImageData::FORMAT_PVR1_RGB4:
		return isSRGB ? GL_COMPRESSED_SRGB_PVRTC_4BPPV1_EXT : GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
    case TCompressedImageData::FORMAT_PVR1_RGBA2:
		return isSRGB ? GL_COMPRESSED_SRGB_ALPHA_PVRTC_2BPPV1_EXT : GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
    case TCompressedImageData::FORMAT_PVR1_RGBA4:
		return isSRGB ? GL_COMPRESSED_SRGB_ALPHA_PVRTC_4BPPV1_EXT : GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
    case TCompressedImageData::FORMAT_ETC1:
		// The ETC2 format can load ETC1 textures.
		if(GLAD_ES_VERSION_3_0 || GLAD_VERSION_4_3 || GLAD_ARB_ES3_compatibility)
			return isSRGB ? GL_COMPRESSED_SRGB8_ETC2 : GL_COMPRESSED_RGB8_ETC2;
		else
		{
			isSRGB = false;
			return GL_ETC1_RGB8_OES;
		}
    case TCompressedImageData::FORMAT_ETC2_RGB:
		return isSRGB ? GL_COMPRESSED_SRGB8_ETC2 : GL_COMPRESSED_RGB8_ETC2;
    case TCompressedImageData::FORMAT_ETC2_RGBA:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC : GL_COMPRESSED_RGBA8_ETC2_EAC;
    case TCompressedImageData::FORMAT_ETC2_RGBA1:
		return isSRGB ? GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 : GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2;
    case TCompressedImageData::FORMAT_EAC_R:
		isSRGB = false;
		return GL_COMPRESSED_R11_EAC;
    case TCompressedImageData::FORMAT_EAC_Rs:
		isSRGB = false;
		return GL_COMPRESSED_SIGNED_R11_EAC;
    case TCompressedImageData::FORMAT_EAC_RG:
		isSRGB = false;
		return GL_COMPRESSED_RG11_EAC;
    case TCompressedImageData::FORMAT_EAC_RGs:
		isSRGB = false;
		return GL_COMPRESSED_SIGNED_RG11_EAC;
    case TCompressedImageData::FORMAT_ASTC_4x4:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR : GL_COMPRESSED_RGBA_ASTC_4x4_KHR;
    case TCompressedImageData::FORMAT_ASTC_5x4:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR : GL_COMPRESSED_RGBA_ASTC_5x4_KHR;
    case TCompressedImageData::FORMAT_ASTC_5x5:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR : GL_COMPRESSED_RGBA_ASTC_5x5_KHR;
    case TCompressedImageData::FORMAT_ASTC_6x5:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR : GL_COMPRESSED_RGBA_ASTC_6x5_KHR;
    case TCompressedImageData::FORMAT_ASTC_6x6:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR : GL_COMPRESSED_RGBA_ASTC_6x6_KHR;
    case TCompressedImageData::FORMAT_ASTC_8x5:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR : GL_COMPRESSED_RGBA_ASTC_8x5_KHR;
    case TCompressedImageData::FORMAT_ASTC_8x6:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR : GL_COMPRESSED_RGBA_ASTC_8x6_KHR;
    case TCompressedImageData::FORMAT_ASTC_8x8:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR : GL_COMPRESSED_RGBA_ASTC_8x8_KHR;
    case TCompressedImageData::FORMAT_ASTC_10x5:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR : GL_COMPRESSED_RGBA_ASTC_10x5_KHR;
    case TCompressedImageData::FORMAT_ASTC_10x6:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR : GL_COMPRESSED_RGBA_ASTC_10x6_KHR;
    case TCompressedImageData::FORMAT_ASTC_10x8:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR : GL_COMPRESSED_RGBA_ASTC_10x8_KHR;
    case TCompressedImageData::FORMAT_ASTC_10x10:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR : GL_COMPRESSED_RGBA_ASTC_10x10_KHR;
    case TCompressedImageData::FORMAT_ASTC_12x10:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR : GL_COMPRESSED_RGBA_ASTC_12x10_KHR;
    case TCompressedImageData::FORMAT_ASTC_12x12:
		return isSRGB ? GL_COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR : GL_COMPRESSED_RGBA_ASTC_12x12_KHR;
	default:
		return isSRGB ? GL_SRGB8_ALPHA8 : GL_RGBA8;
	}
}

bool TDrawableImage::hasAnisotropicFilteringSupport()
{
	return GLAD_EXT_texture_filter_anisotropic != GL_FALSE;
}

bool TDrawableImage::hasCompressedTextureSupport(TCompressedImageData::Format format, bool sRGB)
{
	switch (format)
	{
    case TCompressedImageData::FORMAT_DXT1:
		return GLAD_EXT_texture_compression_s3tc || GLAD_EXT_texture_compression_dxt1;
    case TCompressedImageData::FORMAT_DXT3:
		return GLAD_EXT_texture_compression_s3tc || GLAD_ANGLE_texture_compression_dxt3;
    case TCompressedImageData::FORMAT_DXT5:
		return GLAD_EXT_texture_compression_s3tc || GLAD_ANGLE_texture_compression_dxt5;
    case TCompressedImageData::FORMAT_BC4:
    case TCompressedImageData::FORMAT_BC4s:
    case TCompressedImageData::FORMAT_BC5:
    case TCompressedImageData::FORMAT_BC5s:
		return (GLAD_VERSION_3_0 || GLAD_ARB_texture_compression_rgtc || GLAD_EXT_texture_compression_rgtc);
    case TCompressedImageData::FORMAT_BC6H:
    case TCompressedImageData::FORMAT_BC6Hs:
    case TCompressedImageData::FORMAT_BC7:
		return GLAD_VERSION_4_2 || GLAD_ARB_texture_compression_bptc;
    case TCompressedImageData::FORMAT_PVR1_RGB2:
    case TCompressedImageData::FORMAT_PVR1_RGB4:
    case TCompressedImageData::FORMAT_PVR1_RGBA2:
    case TCompressedImageData::FORMAT_PVR1_RGBA4:
		return sRGB ? GLAD_EXT_pvrtc_sRGB : GLAD_IMG_texture_compression_pvrtc;
    case TCompressedImageData::FORMAT_ETC1:
		// ETC2 support guarantees ETC1 support as well.
		return GLAD_ES_VERSION_3_0 || GLAD_VERSION_4_3 || GLAD_ARB_ES3_compatibility || GLAD_OES_compressed_ETC1_RGB8_texture;
    case TCompressedImageData::FORMAT_ETC2_RGB:
    case TCompressedImageData::FORMAT_ETC2_RGBA:
    case TCompressedImageData::FORMAT_ETC2_RGBA1:
    case TCompressedImageData::FORMAT_EAC_R:
    case TCompressedImageData::FORMAT_EAC_Rs:
    case TCompressedImageData::FORMAT_EAC_RG:
    case TCompressedImageData::FORMAT_EAC_RGs:
		return GLAD_ES_VERSION_3_0 || GLAD_VERSION_4_3 || GLAD_ARB_ES3_compatibility;
    case TCompressedImageData::FORMAT_ASTC_4x4:
    case TCompressedImageData::FORMAT_ASTC_5x4:
    case TCompressedImageData::FORMAT_ASTC_5x5:
    case TCompressedImageData::FORMAT_ASTC_6x5:
    case TCompressedImageData::FORMAT_ASTC_6x6:
    case TCompressedImageData::FORMAT_ASTC_8x5:
    case TCompressedImageData::FORMAT_ASTC_8x6:
    case TCompressedImageData::FORMAT_ASTC_8x8:
    case TCompressedImageData::FORMAT_ASTC_10x5:
    case TCompressedImageData::FORMAT_ASTC_10x6:
    case TCompressedImageData::FORMAT_ASTC_10x8:
    case TCompressedImageData::FORMAT_ASTC_10x10:
    case TCompressedImageData::FORMAT_ASTC_12x10:
    case TCompressedImageData::FORMAT_ASTC_12x12:
		return GLAD_ES_VERSION_3_2 || GLAD_KHR_texture_compression_astc_ldr;
	default:
		return false;
	}
}

bool TDrawableImage::hasSRGBSupport()
{
	return GLAD_ES_VERSION_3_0 || GLAD_EXT_sRGB || GLAD_VERSION_2_1 || GLAD_EXT_texture_sRGB;
}

bool TDrawableImage::getConstant(const char *in, FlagType &out)
{
    return mFlagNames.find(in, out);
}

bool TDrawableImage::getConstant(FlagType in, const char *&out)
{
    return mFlagNames.find(in, out);
}

TDrawableImage *TDrawableImage::generateMirrorImage() const
{
    std::vector<TImageData *> newDataVector;
    for(auto imageData : mData)
    {
        TImageData *newData = new TImageData(*imageData);
        newData->flipHorizontal();
        newDataVector.emplace_back(newData);
    }

    return new TDrawableImage(newDataVector, mFlags);
}

TStringMap<TDrawableImage::FlagType, TDrawableImage::FLAG_TYPE_MAX_ENUM>::Entry TDrawableImage::mFlagNameEntries[] =
{
	{"mipmaps", FLAG_TYPE_MIPMAPS},
	{"linear", FLAG_TYPE_LINEAR},
};

TStringMap<TDrawableImage::FlagType, TDrawableImage::FLAG_TYPE_MAX_ENUM> TDrawableImage::mFlagNames(TDrawableImage::mFlagNameEntries, sizeof(TDrawableImage::mFlagNameEntries));

