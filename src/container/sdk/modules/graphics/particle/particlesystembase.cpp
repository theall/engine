/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "common/config.h"
#include "particlesystembase.h"

#include "common/math.h"
#include "modules/math/randomgenerator.h"

// STD
#include <algorithm>
#include <cmath>
#include <cstdlib>

TRandomGenerator rng;

float calculate_variation(float inner, float outer, float var)
{
	float low = inner - (outer/2.0f)*var;
	float high = inner + (outer/2.0f)*var;
	float r = (float) rng.random();
	return low*(1-r)+high*r;
}

TParticleSystemBase::TParticleSystemBase(TTexture *texture, uint32 size)
    : mPMem(nullptr)
    , mPFree(nullptr)
    , mPHead(nullptr)
    , mPTail(nullptr)
    , mTexture(texture)
    , mActive(true)
    , mInsertMode(INSERT_MODE_TOP)
    , mMaxParticles(0)
    , mActiveParticles(0)
    , mEmissionRate(0)
    , mEmitCounter(0)
    , mAreaSpreadDistribution(DISTRIBUTION_NONE)
    , mLifeTime(-1)
    , mLife(0)
    , mParticleLifeMin(0)
    , mParticleLifeMax(0)
    , mDirection(0)
    , mSpread(0)
    , mSpeedMin(0)
    , mSpeedMax(0)
    , mLinearAccelerationMin(0, 0)
    , mLinearAccelerationMax(0, 0)
    , mRadialAccelerationMin(0)
    , mRadialAccelerationMax(0)
    , mTangentialAccelerationMin(0)
    , mTangentialAccelerationMax(0)
    , mLinearDampingMin(0.0f)
    , mLinearDampingMax(0.0f)
    , mSizeVariation(0)
    , mRotationMin(0)
    , mRotationMax(0)
    , mSpinStart(0)
    , mSpinEnd(0)
    , mSpinVariation(0)
    , mOffset(float(texture->getWidth())*0.5f, float(texture->getHeight())*0.5f)
    , mDefaultOffset(true)
    , mRelativeRotation(false)
{
	if(size == 0 || size > MAX_PARTICLES)
		throw TException("Invalid ParticleSystem size.");

    mSizes.push_back(1.0f);
    mColors.push_back(Colorf(1.0f, 1.0f, 1.0f, 1.0f));
	setBufferSize(size);
}

TParticleSystemBase::TParticleSystemBase(const TParticleSystemBase &p)
    : mPMem(nullptr)
    , mPFree(nullptr)
    , mPHead(nullptr)
    , mPTail(nullptr)
    , mTexture(p.mTexture)
    , mActive(p.mActive)
    , mInsertMode(p.mInsertMode)
    , mMaxParticles(p.mMaxParticles)
    , mActiveParticles(0)
    , mEmissionRate(p.mEmissionRate)
    , mEmitCounter(0.0f)
    , mPosition(p.mPosition)
    , mPrevPosition(p.mPrevPosition)
    , mAreaSpreadDistribution(p.mAreaSpreadDistribution)
    , mAreaSpread(p.mAreaSpread)
    , mLifeTime(p.mLifeTime)
    , mLife(p.mLifeTime) // Initialize with the maximum life time.
    , mParticleLifeMin(p.mParticleLifeMin)
    , mParticleLifeMax(p.mParticleLifeMax)
    , mDirection(p.mDirection)
    , mSpread(p.mSpread)
    , mSpeedMin(p.mSpeedMin)
    , mSpeedMax(p.mSpeedMax)
    , mLinearAccelerationMin(p.mLinearAccelerationMin)
    , mLinearAccelerationMax(p.mLinearAccelerationMax)
    , mRadialAccelerationMin(p.mRadialAccelerationMin)
    , mRadialAccelerationMax(p.mRadialAccelerationMax)
    , mTangentialAccelerationMin(p.mTangentialAccelerationMin)
    , mTangentialAccelerationMax(p.mTangentialAccelerationMax)
    , mLinearDampingMin(p.mLinearDampingMin)
    , mLinearDampingMax(p.mLinearDampingMax)
    , mSizes(p.mSizes)
    , mSizeVariation(p.mSizeVariation)
    , mRotationMin(p.mRotationMin)
    , mRotationMax(p.mRotationMax)
    , mSpinStart(p.mSpinStart)
    , mSpinEnd(p.mSpinEnd)
    , mSpinVariation(p.mSpinVariation)
    , mOffset(p.mOffset)
    , mDefaultOffset(p.mDefaultOffset)
    , mColors(p.mColors)
    , mQuads(p.mQuads)
    , mRelativeRotation(p.mRelativeRotation)
{
    setBufferSize(mMaxParticles);
}

TParticleSystemBase::~TParticleSystemBase()
{
	deleteBuffers();
}

void TParticleSystemBase::resetOffset()
{
    if(mQuads.empty())
        mOffset = TVector(float(mTexture->getWidth())*0.5f, float(mTexture->getHeight())*0.5f);
	else
	{
        TQuad::Viewport v = mQuads[0]->getViewport();
        mOffset = TVector(v.x*0.5f, v.y*0.5f);
	}
}

void TParticleSystemBase::createBuffers(size_t size)
{
	try
	{
        mPFree = mPMem = new Particle[size];
        mMaxParticles = (uint32) size;
	}
	catch (std::bad_alloc &)
	{
		deleteBuffers();
		throw TException("Out of memory");
	}
}

void TParticleSystemBase::deleteBuffers()
{
	// Clean up for great gracefulness!
    delete[] mPMem;

    mPMem = nullptr;
    mMaxParticles = 0;
    mActiveParticles = 0;
}

void TParticleSystemBase::setBufferSize(uint32 size)
{
	if(size == 0 || size > MAX_PARTICLES)
		throw TException("Invalid buffer size");
	deleteBuffers();
	createBuffers(size);
	reset();
}

uint32 TParticleSystemBase::getBufferSize() const
{
    return mMaxParticles;
}

void TParticleSystemBase::addParticle(float t)
{
	if(isFull())
		return;

	// Gets a free particle and updates the allocation pointer.
    Particle *p = mPFree++;
	initParticle(p, t);

    switch (mInsertMode)
	{
	default:
	case INSERT_MODE_TOP:
		insertTop(p);
		break;
	case INSERT_MODE_BOTTOM:
		insertBottom(p);
		break;
	case INSERT_MODE_RANDOM:
		insertRandom(p);
		break;
	}

    mActiveParticles++;
}

void TParticleSystemBase::initParticle(Particle *p, float t)
{
	float min,max;

	// Linearly interpolate between the previous and current emitter position.
    TVector pos = mPrevPosition + (mPosition - mPrevPosition) * t;

    min = mParticleLifeMin;
    max = mParticleLifeMax;
	if(min == max)
		p->life = min;
	else
		p->life = (float) rng.random(min, max);
	p->lifetime = p->life;

	p->position = pos;

    switch (mAreaSpreadDistribution)
	{
	case DISTRIBUTION_UNIFORM:
        p->position.x += (float) rng.random(-mAreaSpread.getX(), mAreaSpread.getX());
        p->position.y += (float) rng.random(-mAreaSpread.getY(), mAreaSpread.getY());
		break;
	case DISTRIBUTION_NORMAL:
        p->position.x += (float) rng.randomNormal(mAreaSpread.getX());
        p->position.y += (float) rng.randomNormal(mAreaSpread.getY());
		break;
	case DISTRIBUTION_NONE:
	default:
		break;
	}

	p->origin = pos;

    min = mSpeedMin;
    max = mSpeedMax;
	float speed = (float) rng.random(min, max);

    min = mDirection - mSpread/2.0f;
    max = mDirection + mSpread/2.0f;
	float dir = (float) rng.random(min, max);

	p->velocity = TVector(cosf(dir), sinf(dir)) * speed;

    p->linearAcceleration.x = (float) rng.random(mLinearAccelerationMin.x, mLinearAccelerationMax.x);
    p->linearAcceleration.y = (float) rng.random(mLinearAccelerationMin.y, mLinearAccelerationMax.y);

    min = mRadialAccelerationMin;
    max = mRadialAccelerationMax;
	p->radialAcceleration = (float) rng.random(min, max);

    min = mTangentialAccelerationMin;
    max = mTangentialAccelerationMax;
	p->tangentialAcceleration = (float) rng.random(min, max);

    min = mLinearDampingMin;
    max = mLinearDampingMax;
	p->linearDamping = (float) rng.random(min, max);

    p->sizeOffset       = (float) rng.random(mSizeVariation); // time offset for size change
    p->sizeIntervalSize = (1.0f - (float) rng.random(mSizeVariation)) - p->sizeOffset;
    p->size = mSizes[(size_t)(p->sizeOffset - .5f) * (mSizes.size() - 1)];

    min = mRotationMin;
    max = mRotationMax;
    p->spinStart = calculate_variation(mSpinStart, mSpinEnd, mSpinVariation);
    p->spinEnd = calculate_variation(mSpinEnd, mSpinStart, mSpinVariation);
	p->rotation = (float) rng.random(min, max);

	p->angle = p->rotation;
    if(mRelativeRotation)
		p->angle += atan2f(p->velocity.y, p->velocity.x);

    p->color = mColors[0];

	p->quadIndex = 0;
}

void TParticleSystemBase::insertTop(Particle *p)
{
    if(mPHead == nullptr)
	{
        mPHead = p;
		p->prev = nullptr;
	}
	else
	{
        mPTail->next = p;
        p->prev = mPTail;
	}
	p->next = nullptr;
    mPTail = p;
}

void TParticleSystemBase::insertBottom(Particle *p)
{
    if(mPTail == nullptr)
	{
        mPTail = p;
		p->next = nullptr;
	}
	else
	{
        mPHead->prev = p;
        p->next = mPHead;
	}
	p->prev = nullptr;
    mPHead = p;
}

void TParticleSystemBase::insertRandom(Particle *p)
{
	// Nonuniform, but 64-bit is so large nobody will notice. Hopefully.
    uint64 pos = rng.rand() % ((int64) mActiveParticles + 1);

	// Special case where the particle gets inserted before the head.
    if(pos == mActiveParticles)
	{
        Particle *pA = mPHead;
		if(pA)
			pA->prev = p;
		p->prev = nullptr;
		p->next = pA;
        mPHead = p;
		return;
	}

	// Inserts the particle after the randomly selected particle.
    Particle *pA = mPMem + pos;
	Particle *pB = pA->next;
	pA->next = p;
	if(pB)
		pB->prev = p;
	else
        mPTail = p;
	p->prev = pA;
	p->next = pB;
}

TParticleSystemBase::Particle *TParticleSystemBase::removeParticle(Particle *p)
{
	// The linked list is updated in this function and old pointers may be
	// invalidated. The returned pointer will inform the caller of the new
	// pointer to the next particle.
	Particle *pNext = nullptr;

	// Removes the particle from the linked list.
	if(p->prev)
		p->prev->next = p->next;
	else
        mPHead = p->next;

	if(p->next)
	{
		p->next->prev = p->prev;
		pNext = p->next;
	}
	else
        mPTail = p->prev;

	// The (in memory) last particle can now be moved into the free slot.
	// It will skip the moving if it happens to be the removed particle.
    mPFree--;
    if(p != mPFree)
	{
        *p = *mPFree;
        if(pNext == mPFree)
			pNext = p;

		if(p->prev)
			p->prev->next = p;
		else
            mPHead = p;

		if(p->next)
			p->next->prev = p;
		else
            mPTail = p;
	}

    mActiveParticles--;
	return pNext;
}

void TParticleSystemBase::setTexture(TTexture *tex)
{
    mTexture.set(tex);

    if(mDefaultOffset)
		resetOffset();
}

TTexture *TParticleSystemBase::getTexture() const
{
    return mTexture.get();
}

void TParticleSystemBase::setInsertMode(InsertMode mode)
{
    mInsertMode = mode;
}

TParticleSystemBase::InsertMode TParticleSystemBase::getInsertMode() const
{
    return mInsertMode;
}

void TParticleSystemBase::setEmissionRate(float rate)
{
	if(rate < 0.0f)
		throw TException("Invalid emission rate");
    mEmissionRate = rate;

	// Prevent an explosion when dramatically increasing the rate
    mEmitCounter = std::min(mEmitCounter, 1.0f/rate);
}

float TParticleSystemBase::getEmissionRate() const
{
    return mEmissionRate;
}

void TParticleSystemBase::setEmitterLifetime(float life)
{
    mLife = mLifeTime = life;
}

float TParticleSystemBase::getEmitterLifetime() const
{
    return mLifeTime;
}

void TParticleSystemBase::setParticleLifetime(float min, float max)
{
    mParticleLifeMin = min;
	if(max == 0)
        mParticleLifeMax = min;
	else
        mParticleLifeMax = max;
}

void TParticleSystemBase::getParticleLifetime(float &min, float &max) const
{
    min = mParticleLifeMin;
    max = mParticleLifeMax;
}

void TParticleSystemBase::setPosition(float x, float y)
{
    mPosition = TVector(x, y);
    mPrevPosition = mPosition;
}

const TVector &TParticleSystemBase::getPosition() const
{
    return mPosition;
}

void TParticleSystemBase::moveTo(float x, float y)
{
    mPosition = TVector(x, y);
}

void TParticleSystemBase::setAreaSpread(AreaSpreadDistribution distribution, float x, float y)
{
    mAreaSpread = TVector(x, y);
    mAreaSpreadDistribution = distribution;
}

TParticleSystemBase::AreaSpreadDistribution TParticleSystemBase::getAreaSpreadDistribution() const
{
    return mAreaSpreadDistribution;
}

const TVector &TParticleSystemBase::getAreaSpreadParameters() const
{
    return mAreaSpread;
}

void TParticleSystemBase::setDirection(float direction)
{
    mDirection = direction;
}

float TParticleSystemBase::getDirection() const
{
    return mDirection;
}

void TParticleSystemBase::setSpread(float spread)
{
    mSpread = spread;
}

float TParticleSystemBase::getSpread() const
{
    return mSpread;
}

void TParticleSystemBase::setSpeed(float speed)
{
    mSpeedMin = mSpeedMax = speed;
}

void TParticleSystemBase::setSpeed(float min, float max)
{
    mSpeedMin = min;
    mSpeedMax = max;
}

void TParticleSystemBase::getSpeed(float &min, float &max) const
{
    min = mSpeedMin;
    max = mSpeedMax;
}

void TParticleSystemBase::setLinearAcceleration(float x, float y)
{
    mLinearAccelerationMin.x = mLinearAccelerationMax.x = x;
    mLinearAccelerationMin.y = mLinearAccelerationMax.y = y;
}

void TParticleSystemBase::setLinearAcceleration(float xmin, float ymin, float xmax, float ymax)
{
    mLinearAccelerationMin = TVector(xmin, ymin);
    mLinearAccelerationMax = TVector(xmax, ymax);
}

void TParticleSystemBase::getLinearAcceleration(TVector &min, TVector &max) const
{
    min = mLinearAccelerationMin;
    max = mLinearAccelerationMax;
}

void TParticleSystemBase::setRadialAcceleration(float acceleration)
{
    mRadialAccelerationMin = mRadialAccelerationMax = acceleration;
}

void TParticleSystemBase::setRadialAcceleration(float min, float max)
{
    mRadialAccelerationMin = min;
    mRadialAccelerationMax = max;
}

void TParticleSystemBase::getRadialAcceleration(float &min, float &max) const
{
    min = mRadialAccelerationMin;
    max = mRadialAccelerationMax;
}

void TParticleSystemBase::setTangentialAcceleration(float acceleration)
{
    mTangentialAccelerationMin = mTangentialAccelerationMax = acceleration;
}

void TParticleSystemBase::setTangentialAcceleration(float min, float max)
{
    mTangentialAccelerationMin = min;
    mTangentialAccelerationMax = max;
}

void TParticleSystemBase::getTangentialAcceleration(float &min, float &max) const
{
    min = mTangentialAccelerationMin;
    max = mTangentialAccelerationMax;
}

void TParticleSystemBase::setLinearDamping(float min, float max)
{
    mLinearDampingMin = min;
    mLinearDampingMax = max;
}

void TParticleSystemBase::getLinearDamping(float &min, float &max) const
{
    min = mLinearDampingMin;
    max = mLinearDampingMax;
}

void TParticleSystemBase::setSize(float size)
{
    mSizes.resize(1);
    mSizes[0] = size;
}

void TParticleSystemBase::setSizes(const std::vector<float> &newSizes)
{
    mSizes = newSizes;
}

const std::vector<float> &TParticleSystemBase::getSizes() const
{
    return mSizes;
}

void TParticleSystemBase::setSizeVariation(float variation)
{
    mSizeVariation = variation;
}

float TParticleSystemBase::getSizeVariation() const
{
    return mSizeVariation;
}

void TParticleSystemBase::setRotation(float rotation)
{
    mRotationMin = mRotationMax = rotation;
}

void TParticleSystemBase::setRotation(float min, float max)
{
    mRotationMin = min;
    mRotationMax = max;
}

void TParticleSystemBase::getRotation(float &min, float &max) const
{
    min = mRotationMin;
    max = mRotationMax;
}

void TParticleSystemBase::setSpin(float spin)
{
    mSpinStart = spin;
    mSpinEnd = spin;
}

void TParticleSystemBase::setSpin(float start, float end)
{
    mSpinStart = start;
    mSpinEnd = end;
}

void TParticleSystemBase::getSpin(float &start, float &end) const
{
    start = mSpinStart;
    end = mSpinEnd;
}

void TParticleSystemBase::setSpinVariation(float variation)
{
    mSpinVariation = variation;
}

float TParticleSystemBase::getSpinVariation() const
{
    return mSpinVariation;
}

void TParticleSystemBase::setOffset(float x, float y)
{
    mOffset = TVector(x, y);
    mDefaultOffset = false;
}

TVector TParticleSystemBase::getOffset() const
{
    return mOffset;
}

void TParticleSystemBase::setColor(const std::vector<Colorf> &newColors)
{
    mColors = newColors;

    for (Colorf &c : mColors)
	{
		// We want to store the colors as [0, 1], rather than [0, 255].
		c.r /= 255.0f;
		c.g /= 255.0f;
		c.b /= 255.0f;
		c.a /= 255.0f;
	}
}

std::vector<Colorf> TParticleSystemBase::getColor() const
{
	// The particle system stores colors in the range of [0, 1]...
    std::vector<Colorf> ncolors(mColors);

	for (Colorf &c : ncolors)
	{
		c.r *= 255.0f;
		c.g *= 255.0f;
		c.b *= 255.0f;
		c.a *= 255.0f;
	}

	return ncolors;
}

void TParticleSystemBase::setQuads(const std::vector<TQuad *> &newQuads)
{
	std::vector<StrongRef<TQuad>> quadlist;
	quadlist.reserve(newQuads.size());

	for (TQuad *q : newQuads)
		quadlist.push_back(q);

    mQuads = quadlist;

    if(mDefaultOffset)
		resetOffset();
}

void TParticleSystemBase::setQuads()
{
    mQuads.clear();
}

std::vector<TQuad *> TParticleSystemBase::getQuads() const
{
	std::vector<TQuad *> quadlist;
    quadlist.reserve(mQuads.size());

    for (const StrongRef<TQuad> &q : mQuads)
		quadlist.push_back(q.get());

	return quadlist;
}

void TParticleSystemBase::setRelativeRotation(bool enable)
{
    mRelativeRotation = enable;
}

bool TParticleSystemBase::hasRelativeRotation() const
{
    return mRelativeRotation;
}

uint32 TParticleSystemBase::getCount() const
{
    return mActiveParticles;
}

void TParticleSystemBase::start()
{
    mActive = true;
}

void TParticleSystemBase::stop()
{
    mActive = false;
    mLife = mLifeTime;
    mEmitCounter = 0;
}

void TParticleSystemBase::pause()
{
    mActive = false;
}

void TParticleSystemBase::reset()
{
    if(mPMem == nullptr)
		return;

    mPFree = mPMem;
    mPHead = nullptr;
    mPTail = nullptr;
    mActiveParticles = 0;
    mLife = mLifeTime;
    mEmitCounter = 0;
}

void TParticleSystemBase::emit_(uint32 num)
{
    if(!mActive)
		return;

    num = std::min(num, mMaxParticles - mActiveParticles);

	while (num--)
		addParticle(1.0f);
}

bool TParticleSystemBase::isActive() const
{
    return mActive;
}

bool TParticleSystemBase::isPaused() const
{
    return !mActive && mLife < mLifeTime;
}

bool TParticleSystemBase::isStopped() const
{
    return !mActive && mLife >= mLifeTime;
}

bool TParticleSystemBase::isEmpty() const
{
    return mActiveParticles == 0;
}

bool TParticleSystemBase::isFull() const
{
    return mActiveParticles == mMaxParticles;
}

void TParticleSystemBase::update(float dt)
{
    if(mPMem == nullptr || dt == 0.0f)
		return;

	// Traverse all particles and update.
    Particle *p = mPHead;

	while (p)
	{
		// Decrease lifespan.
		p->life -= dt;

		if(p->life <= 0)
			p = removeParticle(p);
		else
		{
			// Temp variables.
			TVector radial, tangential;
			TVector ppos = p->position;

			// Get vector from particle center to particle.
			radial = ppos - p->origin;
			radial.normalize();
			tangential = radial;

			// Resize radial acceleration.
			radial *= p->radialAcceleration;

			// Calculate tangential acceleration.
			{
				float a = tangential.getX();
				tangential.setX(-tangential.getY());
				tangential.setY(a);
			}

			// Resize tangential.
			tangential *= p->tangentialAcceleration;

			// Update velocity.
			p->velocity += (radial + tangential + p->linearAcceleration) * dt;

			// Apply damping.
			p->velocity *= 1.0f / (1.0f + p->linearDamping * dt);

			// Modify position.
			ppos += p->velocity * dt;

			p->position = ppos;

			const float t = 1.0f - p->life / p->lifetime;

			// Rotate.
			p->rotation += (p->spinStart * (1.0f - t) + p->spinEnd * t) * dt;

			p->angle = p->rotation;

            if(mRelativeRotation)
				p->angle += atan2f(p->velocity.y, p->velocity.x);

			// Change size according to given intervals:
			// i = 0       1       2      3          n-1
			//     |-------|-------|------|--- ... ---|
			// t = 0    1/(n-1)        3/(n-1)        1
			//
			// `s' is the interpolation variable scaled to the current
			// interval width, e.g. if n = 5 and t = 0.3, then the current
			// indices are 1,2 and s = 0.3 - 0.25 = 0.05
			float s = p->sizeOffset + t * p->sizeIntervalSize; // size variation
            s *= (float)(mSizes.size() - 1); // 0 <= s < sizes.size()
			size_t i = (size_t)s;
            size_t k = (i == mSizes.size() - 1) ? i : i + 1; // boundary check (prevents failing on t = 1.0f)
			s -= (float)i; // transpose s to be in interval [0:1]: i <= s < i + 1 ~> 0 <= s < 1
            p->size = mSizes[i] * (1.0f - s) + mSizes[k] * s;

			// Update color according to given intervals (as above)
            s = t * (float)(mColors.size() - 1);
			i = (size_t)s;
            k = (i == mColors.size() - 1) ? i : i + 1;
			s -= (float)i;                            // 0 <= s <= 1
            p->color = mColors[i] * (1.0f - s) + mColors[k] * s;

			// Update the quad index.
            k = mQuads.size();
			if(k > 0)
			{
				s = t * (float) k; // [0:numquads-1] (clamped below)
				i = (s > 0.0f) ? (size_t) s : 0;
				p->quadIndex = (int) ((i < k) ? i : k - 1);
			}

			// Next particle.
			p = p->next;
		}
	}

	// Make some more particles.
    if(mActive)
	{
        float rate = 1.0f / mEmissionRate; // the amount of time between each particle emit
        mEmitCounter += dt;
        float total = mEmitCounter - rate;
        while (mEmitCounter > rate)
		{
            addParticle(1.0f - (mEmitCounter - rate) / total);
            mEmitCounter -= rate;
		}

        mLife -= dt;
        if(mLifeTime != -1 && mLife < 0)
			stop();
	}

    mPrevPosition = mPosition;
}

bool TParticleSystemBase::getConstant(const char *in, AreaSpreadDistribution &out)
{
	return distributions.find(in, out);
}

bool TParticleSystemBase::getConstant(AreaSpreadDistribution in, const char *&out)
{
	return distributions.find(in, out);
}

bool TParticleSystemBase::getConstant(const char *in, InsertMode &out)
{
	return insertModes.find(in, out);
}

bool TParticleSystemBase::getConstant(InsertMode in, const char *&out)
{
	return insertModes.find(in, out);
}

TStringMap<TParticleSystemBase::AreaSpreadDistribution, TParticleSystemBase::DISTRIBUTION_MAX_ENUM>::Entry TParticleSystemBase::distributionsEntries[] =
{
	{ "none",    DISTRIBUTION_NONE },
	{ "uniform", DISTRIBUTION_UNIFORM },
	{ "normal",  DISTRIBUTION_NORMAL },
};

TStringMap<TParticleSystemBase::AreaSpreadDistribution, TParticleSystemBase::DISTRIBUTION_MAX_ENUM> TParticleSystemBase::distributions(TParticleSystemBase::distributionsEntries, sizeof(TParticleSystemBase::distributionsEntries));

TStringMap<TParticleSystemBase::InsertMode, TParticleSystemBase::INSERT_MODE_MAX_ENUM>::Entry TParticleSystemBase::insertModesEntries[] =
{
	{ "top",    INSERT_MODE_TOP },
	{ "bottom", INSERT_MODE_BOTTOM },
	{ "random", INSERT_MODE_RANDOM },
};

TStringMap<TParticleSystemBase::InsertMode, TParticleSystemBase::INSERT_MODE_MAX_ENUM> TParticleSystemBase::insertModes(TParticleSystemBase::insertModesEntries, sizeof(TParticleSystemBase::insertModesEntries));


