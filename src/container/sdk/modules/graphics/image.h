/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_GRAPHICS_OPENGL_IMAGE_H
#define THEALL_GRAPHICS_OPENGL_IMAGE_H

#include "common/config.h"
#include "common/matrix.h"
#include "common/vector.h"
#include "common/stringmap.h"
#include "common/math.h"

#include "modules/image/imagedata.h"
#include "modules/image/compressedimagedata.h"

#include "texture/texture.h"
#include "base/volatile.h"

// OpenGL
#include "opengl.h"

/**
 * A drawable image based on OpenGL-textures. This class takes TImageData
 * objects and create textures on the GPU for fast drawing.
 *
 * @author Anders Ruud
 **/
class TDrawableImage : public TTexture, public TVolatile
{
public:

	enum FlagType
	{
		FLAG_TYPE_MIPMAPS,
		FLAG_TYPE_LINEAR,
		FLAG_TYPE_MAX_ENUM
	};

	struct Flags
	{
		bool mipmaps = false;
		bool linear = false;
	};

	/**
	 * Creates a new Image. Not that anything is ready to use
	 * before load is called.
	 *
	 * @param data The data from which to load the image. Each element in the
	 * array is a mipmap level. If more than the base level is present, all
	 * mip levels must be present.
	 **/
    TDrawableImage(const std::vector<TImageData *> &data, const Flags &flags);

	/**
	 * Creates a new Image with compressed image data.
	 *
	 * @param cdata The compressed data from which to load the image.
	 **/
    TDrawableImage(const std::vector<TCompressedImageData *> &data, const Flags &flags);

    virtual ~TDrawableImage();

	// Implements Volatile.
	bool loadVolatile();
	void unloadVolatile();

	/**
     * @copydoc TDrawable::draw()
	 **/
    void draw(float x, float y);
	void draw(float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky);

	/**
	 * @copydoc Texture::drawq()
	 **/
    void drawq(TQuad *quad, float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky);

	virtual const void *getHandle() const;

    const std::vector<StrongRef<TImageData>> &getImageData() const;
    const std::vector<StrongRef<TCompressedImageData>> &getCompressedData() const;

    virtual void setFilter(const TTexture::Filter &f);
    virtual bool setWrap(const TTexture::Wrap &w);

	void setMipmapSharpness(float sharpness);
	float getMipmapSharpness() const;

	/**
     * Whether this Image is using a compressed texture (via TCompressedImageData).
	 **/
	bool isCompressed() const;

	/**
     * Re-uploads the TImageData or TCompressedImageData associated with this Image to
	 * the GPU.
	 **/
	bool refresh(int xoffset, int yoffset, int w, int h);

	const Flags &getFlags() const;

	static void setDefaultMipmapSharpness(float sharpness);
	static float getDefaultMipmapSharpness();
	static void setDefaultMipmapFilter(FilterMode f);
	static FilterMode getDefaultMipmapFilter();

	static bool hasAnisotropicFilteringSupport();
    static bool hasCompressedTextureSupport(TCompressedImageData::Format format, bool isRGB);
	static bool hasSRGBSupport();

	static bool getConstant(const char *in, FlagType &out);
	static bool getConstant(FlagType in, const char *&out);

	static int imageCount;

    /**
     * Generate mirrored image.
     **/
    TDrawableImage *generateMirrorImage() const;

private:

	void drawv(const TMatrix4 &t, const Vertex *v);

	void preload();

	void generateMipmaps();
	void loadDefaultTexture();
	void loadFromCompressedData();
	void loadFromImageData();

    GLenum getCompressedFormat(TCompressedImageData::Format cformat, bool &isSRGB) const;

    // The TImageData from which the texture is created. May be empty if
	// Compressed image data was used to create the texture.
	// Each element in the array is a mipmap level.
    std::vector<StrongRef<TImageData>> mData;

	// Or the Compressed Image Data from which the texture is created. May be
    // empty if raw TImageData was used to create the texture.
    std::vector<StrongRef<TCompressedImageData>> mCData;

	// OpenGL texture identifier.
	GLuint texture;

	// Mipmap texture LOD bias (sharpness) value.
    float mMipmapSharpness;

	// Whether this Image is using a compressed texture.
    bool mIsCompressed;

	// The flags used to initialize this Image.
    Flags mFlags;

    bool mIsRGB;

	// True if the image wasn't able to be properly created and it had to fall
	// back to a default texture.
    bool mUsingDefaultTexture;

    size_t mTextureMemorySize;

    static float mMaxMipmapSharpness;

    static FilterMode mDefaultMipmapFilter;
    static float mDefaultMipmapSharpness;

    static TStringMap<FlagType, FLAG_TYPE_MAX_ENUM>::Entry mFlagNameEntries[];
    static TStringMap<FlagType, FLAG_TYPE_MAX_ENUM> mFlagNames;

}; // Image

#endif // THEALL_GRAPHICS_OPENGL_IMAGE_H
