/**
 * Copyright (c) 2006-2016 THEALL Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/
#include <string>

const char *SYNTAX = "\
    #ifndef GL_ES\n\
    #define lowp\n\
    #define mediump\n\
    #define highp\n\
    #endif\n\
    #define number float\n\
    #define Image sampler2D\n\
    #define extern uniform\n\
    #define Texel texture2D\n\
    #pragma optionNV(strict on)\n";

//Uniforms shared by the vertex and pixel shader stages.
const char *UNIFORMS = "\
    #ifdef GL_ES\n\
    // According to the GLSL ES 1.0 spec, uniform precision must match between stages,\n\
    // but we can't guarantee that highp is always supported in fragment shaders...\n\
    // We *really* don't want to use mediump for these in vertex shaders though.\n\
    #if defined(VERTEX) || defined(GL_FRAGMENT_PRECISION_HIGH)\n\
    #define THEALL_UNIFORM_PRECISION highp\n\
    #else\n\
    #define THEALL_UNIFORM_PRECISION mediump\n\
    #endif\n\
    uniform THEALL_UNIFORM_PRECISION mat4 TransformMatrix;\n\
    uniform THEALL_UNIFORM_PRECISION mat4 ProjectionMatrix;\n\
    uniform THEALL_UNIFORM_PRECISION mat4 TransformProjectionMatrix;\n\
    uniform THEALL_UNIFORM_PRECISION mat3 NormalMatrix;\n\
    #else\n\
    #define TransformMatrix gl_ModelViewMatrix\n\
    #define ProjectionMatrix gl_ProjectionMatrix\n\
    #define TransformProjectionMatrix gl_ModelViewProjectionMatrix\n\
    #define NormalMatrix gl_NormalMatrix\n\
    #endif\n\
    uniform mediump vec4 theall_ScreenSize;\n";

const char *FUNCTIONS = "\
    float gammaToLinearPrecise(float c) {\n\
        return c <= 0.04045 ? c * 0.077399380804954 : pow((c + 0.055) * 0.9478672985782, 2.4);\n\
    }\n\
    vec3 gammaToLinearPrecise(vec3 c) {\n\
        bvec3 leq = lessThanEqual(c, vec3(0.04045));\n\
        c.r = leq.r ? c.r * 0.077399380804954 : pow((c.r + 0.055) * 0.9478672985782, 2.4);\n\
        c.g = leq.g ? c.g * 0.077399380804954 : pow((c.g + 0.055) * 0.9478672985782, 2.4);\n\
        c.b = leq.b ? c.b * 0.077399380804954 : pow((c.b + 0.055) * 0.9478672985782, 2.4);\n\
        return c;\n\
    }\n\
    vec4 gammaToLinearPrecise(vec4 c) { return vec4(gammaToLinearPrecise(c.rgb), c.a); }\n\
    float linearToGammaPrecise(float c) {\n\
        return c < 0.0031308 ? c * 12.92 : 1.055 * pow(c, 1.0 / 2.4) - 0.055;\n\
    }\n\
    vec3 linearToGammaPrecise(vec3 c) {\n\
        bvec3 lt = lessThanEqual(c, vec3(0.0031308));\n\
        c.r = lt.r ? c.r * 12.92 : 1.055 * pow(c.r, 1.0 / 2.4) - 0.055;\n\
        c.g = lt.g ? c.g * 12.92 : 1.055 * pow(c.g, 1.0 / 2.4) - 0.055;\n\
        c.b = lt.b ? c.b * 12.92 : 1.055 * pow(c.b, 1.0 / 2.4) - 0.055;\n\
        return c;\n\
    }\n\
    vec4 linearToGammaPrecise(vec4 c) { return vec4(linearToGammaPrecise(c.rgb), c.a); }\n\
    \n\
    // pow(x, 2.2) isn't an amazing approximation, but at least it's efficient...\n\
    mediump float gammaToLinearFast(mediump float c) { return pow(max(c, 0.0), 2.2); }\n\
    mediump vec3 gammaToLinearFast(mediump vec3 c) { return pow(max(c, vec3(0.0)), vec3(2.2)); }\n\
    mediump vec4 gammaToLinearFast(mediump vec4 c) { return vec4(gammaToLinearFast(c.rgb), c.a); }\n\
    mediump float linearToGammaFast(mediump float c) { return pow(max(c, 0.0), 1.0 / 2.2); }\n\
    mediump vec3 linearToGammaFast(mediump vec3 c) { return pow(max(c, vec3(0.0)), vec3(1.0 / 2.2)); }\n\
    mediump vec4 linearToGammaFast(mediump vec4 c) { return vec4(linearToGammaFast(c.rgb), c.a); }\n\
    \n\
    #ifdef THEALL_PRECISE_GAMMA\n\
    #define gammaToLinear gammaToLinearPrecise\n\
    #define linearToGamma linearToGammaPrecise\n\
    #else\n\
    #define gammaToLinear gammaToLinearFast\n\
    #define linearToGamma linearToGammaFast\n\
    #endif\n\
    \n\
    #ifdef THEALL_GAMMA_CORRECT\n\
    #define gammaCorrectColor gammaToLinear\n\
    #define unGammaCorrectColor linearToGamma\n\
    #define gammaCorrectColorPrecise gammaToLinearPrecise\n\
    #define unGammaCorrectColorPrecise linearToGammaPrecise\n\
    #define gammaCorrectColorFast gammaToLinearFast\n\
    #define unGammaCorrectColorFast linearToGammaFast\n\
    #else\n\
    #define gammaCorrectColor\n\
    #define unGammaCorrectColor\n\
    #define gammaCorrectColorPrecise\n\
    #define unGammaCorrectColorPrecise\n\
    #define gammaCorrectColorFast\n\
    #define unGammaCorrectColorFast\n\
    #endif\n";


namespace GLSL_VERTEX {
const char *HEADER = "\
    #define VERTEX\n\
    #define THEALL_PRECISE_GAMMA\n\
    \n\
    attribute vec4 VertexPosition;\n\
    attribute vec4 VertexTexCoord;\n\
    attribute vec4 VertexColor;\n\
    attribute vec4 ConstantColor;\n\
    \n\
    varying vec4 VaryingTexCoord;\n\
    varying vec4 VaryingColor;\n\
    \n\
    #ifdef GL_ES\n\
    uniform mediump float theall_PointSize;\n\
    #endif\n";

const char *FUNCTIONS = "";

const char *FOOTER = "\
    void main() {\n\
        VaryingTexCoord = VertexTexCoord;\n\
        VaryingColor = gammaCorrectColor(VertexColor) * ConstantColor;\n\
    #ifdef GL_ES\n\
        gl_PointSize = theall_PointSize;\n\
    #endif\n\
        gl_Position = position(TransformProjectionMatrix, VertexPosition);\n\
    }\n";

const char *FOOTER_MULTI_CANVAS = FOOTER;
}

namespace GLSL_PIXEL {

const char *HEADER = "\
    #define PIXEL\n\
    \n\
    #ifdef GL_ES\n\
    precision mediump float;\n\
    #endif\n\
    \n\
    varying mediump vec4 VaryingTexCoord;\n\
    varying mediump vec4 VaryingColor;\n\
    \n\
    #define theall_Canvases gl_FragData\n\
    \n\
    uniform sampler2D _tex0_;\n";

const char *FUNCTIONS = "\
    uniform sampler2D theall_VideoYChannel;\n\
    uniform sampler2D theall_VideoCbChannel;\n\
    uniform sampler2D theall_VideoCrChannel;\n\
    \n\
    vec4 VideoTexel(vec2 texcoords)\n\
    {\n\
        vec3 yuv;\n\
        yuv[0] = Texel(theall_VideoYChannel, texcoords).r;\n\
        yuv[1] = Texel(theall_VideoCbChannel, texcoords).r;\n\
        yuv[2] = Texel(theall_VideoCrChannel, texcoords).r;\n\
        yuv += vec3(-0.0627451017, -0.501960814, -0.501960814);\n\
    \n\
        vec4 color;\n\
        color.r = dot(yuv, vec3(1.164,  0.000,  1.596));\n\
        color.g = dot(yuv, vec3(1.164, -0.391, -0.813));\n\
        color.b = dot(yuv, vec3(1.164,  2.018,  0.000));\n\
        color.a = 1.0;\n\
    \n\
        return gammaCorrectColor(color);\n\
    }\n";

const char *FOOTER = "\
    void main() {\n\
        // fix crashing issue in OSX when _tex0_ is unused within effect()\n\
        float dummy = Texel(_tex0_, vec2(.5)).r;\n\
    \n\
        // See Shader::checkSetScreenParams in Shader.cpp.\n\
        vec2 pixelcoord = vec2(gl_FragCoord.x, (gl_FragCoord.y * theall_ScreenSize.z) + theall_ScreenSize.w);\n\
        vec4 textureColor = Texel(_tex0_, VaryingTexCoord.st);\n\
        if(textureColor==vec4(0.0,0.0,0.0,1.0))\n\
             return;\n\
        gl_FragColor = textureColor * VaryingColor;\n\
    }\n";

const char *FOOTER_MULTI_CANVAS = "\
    void main() {\n\
        // fix crashing issue in OSX when _tex0_ is unused within effect()\n\
        float dummy = Texel(_tex0_, vec2(.5)).r;\n\
    \n\
        // See Shader::checkSetScreenParams in Shader.cpp.\n\
        vec2 pixelcoord = vec2(gl_FragCoord.x, (gl_FragCoord.y * theall_ScreenSize.z) + theall_ScreenSize.w);\n\
    \n\
        effects(VaryingColor, _tex0_, VaryingTexCoord.st, pixelcoord);\n\
    }\n";
}

namespace DEFAULT_CODE {

const char *VERTEX = "vec4 position(mat4 transform_proj, vec4 vertpos) {\
        return transform_proj * vertpos;\
    }";

const char *PIXEL = "vec4 effect(mediump vec4 vcolor, Image tex, vec2 texcoord, vec2 pixcoord) {\
        return Texel(tex, texcoord) * vcolor;\
    }";

const char *VIDEO = "vec4 effect(mediump vec4 vcolor, Image tex, vec2 texcoord, vec2 pixcoord) {\
        return VideoTexel(texcoord) * vcolor;\
    }";
}

std::string createShaderStageCode(bool stage_vertex, std::string code, bool isglsles, bool gammacorrect, bool multicanvas=false)
{
    std::string ret = "#version ";
    if(isglsles)
        ret += "100\n";
    else
        ret += "120\n";

    ret += SYNTAX;
    if(gammacorrect)
        ret += "#define THEALL_GAMMA_CORRECT 1\n";

    if(stage_vertex) {
        ret += GLSL_VERTEX::HEADER;
    } else {
        ret += GLSL_PIXEL::HEADER;
    }

    ret += UNIFORMS;
    ret += FUNCTIONS;

    if(stage_vertex)
        ret += GLSL_VERTEX::FUNCTIONS;
    else
        ret += GLSL_PIXEL::FUNCTIONS;

    if(isglsles)
        ret += "#line 1\n";
    else
        ret += "#line 0\n";

    ret += code;
    if(multicanvas)
    {
        if(stage_vertex)
            ret += GLSL_VERTEX::FOOTER_MULTI_CANVAS;
        else
            ret += GLSL_PIXEL::FOOTER_MULTI_CANVAS;
    } else {
        if(stage_vertex)
            ret += GLSL_VERTEX::FOOTER;
        else
            ret += GLSL_PIXEL::FOOTER;
    }
    return ret;
}
