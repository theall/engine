/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "polyline.h"

// OpenGL
#include "opengl.h"

// C++
#include <algorithm>

// treat adjacent segments with angles between their directions <5 degree as straight
static const float LINES_PARALLEL_EPS = 0.05f;

void TPolyline::render(const float *coords, size_t count, size_t size_hint, float halfwidth, float pixel_size, bool drawOverdraw)
{
	static std::vector<TVector> anchors;
	anchors.clear();
	anchors.reserve(size_hint);

	static std::vector<TVector> normals;
	normals.clear();
	normals.reserve(size_hint);

	// prepare vertex arrays
    if(drawOverdraw)
		halfwidth -= pixel_size * 0.3f;

	// compute sleeve
	bool is_looping = (coords[0] == coords[count - 2]) && (coords[1] == coords[count - 1]);
	TVector s;
	if(!is_looping) // virtual starting point at second point mirrored on first point
		s = TVector(coords[2] - coords[0], coords[3] - coords[1]);
	else // virtual starting point at last vertex
		s = TVector(coords[0] - coords[count - 4], coords[1] - coords[count - 3]);

	float len_s = s.getLength();
	TVector ns = s.getNormal(halfwidth / len_s);

	TVector q, r(coords[0], coords[1]);
	for (size_t i = 0; i + 3 < count; i += 2)
	{
		q = r;
		r = TVector(coords[i + 2], coords[i + 3]);
		renderEdge(anchors, normals, s, len_s, ns, q, r, halfwidth);
	}

	q = r;
	r = is_looping ? TVector(coords[2], coords[3]) : r + s;
	renderEdge(anchors, normals, s, len_s, ns, q, r, halfwidth);

	mVertexCount = normals.size();

	size_t extra_vertices = 0;

    if(drawOverdraw)
	{
		calc_overdraw_vertex_count(is_looping);

		// When drawing overdraw lines using triangle strips, we want to add an
		// extra degenerate triangle in between the core line and the overdraw
		// line in order to break up the strip into two. This will let us draw
		// everything in one draw call.
		if(mDrawMode == GL_TRIANGLE_STRIP)
			extra_vertices = 2;
	}

	// Use a single linear array for both the regular and overdraw vertices.
	mVertices = new TVector[mVertexCount + extra_vertices + mOverdrawVertexCount];

	for (size_t i = 0; i < mVertexCount; ++i)
		mVertices[i] = anchors[i] + normals[i];

    if(drawOverdraw)
	{
		mOverDraw = mVertices + mVertexCount + extra_vertices;
		mOverdrawVertexStart = mVertexCount + extra_vertices;
		render_overdraw(normals, pixel_size, is_looping);
	}

	// Add the degenerate triangle strip.
	if(extra_vertices)
	{
		mVertices[mVertexCount + 0] = mVertices[mVertexCount - 1];
		mVertices[mVertexCount + 1] = mVertices[mOverdrawVertexStart];
	}
}

void TNoneJoinPolyline::renderEdge(std::vector<TVector> &anchors, std::vector<TVector> &normals,
                                TVector &s, float &len_s, TVector &ns,
                                const TVector &q, const TVector &r, float hw)
{
	anchors.push_back(q);
	anchors.push_back(q);
	normals.push_back(ns);
	normals.push_back(-ns);

	s     = (r - q);
	len_s = s.getLength();
	ns    = s.getNormal(hw / len_s);

	anchors.push_back(q);
	anchors.push_back(q);
	normals.push_back(-ns);
	normals.push_back(ns);
}


/** Calculate line boundary points.
 *
 * Sketch:
 *
 *              u1
 * -------------+---...___
 *              |         ```'''--  ---
 * p- - - - - - q- - . _ _           | w/2
 *              |          ` ' ' r   +
 * -------------+---...___           | w/2
 *              u2         ```'''-- ---
 *
 * u1 and u2 depend on four things:
 *   - the half line width w/2
 *   - the previous line vertex p
 *   - the current line vertex q
 *   - the next line vertex r
 *
 * u1/u2 are the intersection points of the parallel lines to p-q and q-r,
 * i.e. the point where
 *
 *    (q + w/2 * ns) + lambda * (q - p) = (q + w/2 * nt) + mu * (r - q)   (u1)
 *    (q - w/2 * ns) + lambda * (q - p) = (q - w/2 * nt) + mu * (r - q)   (u2)
 *
 * with nt,nt being the normals on the segments s = p-q and t = q-r,
 *
 *    ns = perp(s) / |s|
 *    nt = perp(t) / |t|.
 *
 * Using the linear equation system (similar for u2)
 *
 *         q + w/2 * ns + lambda * s - (q + w/2 * nt + mu * t) = 0                 (u1)
 *    <=>  q-q + lambda * s - mu * t                          = (nt - ns) * w/2
 *    <=>  lambda * s   - mu * t                              = (nt - ns) * w/2
 *
 * the intersection points can be efficiently calculated using Cramer's rule.
 */
void MiterJoinPolyline::renderEdge(std::vector<TVector> &anchors, std::vector<TVector> &normals,
                                   TVector &s, float &len_s, TVector &ns,
                                   const TVector &q, const TVector &r, float hw)
{
	TVector t    = (r - q);
	float len_t = t.getLength();
	TVector nt   = t.getNormal(hw / len_t);

	anchors.push_back(q);
	anchors.push_back(q);

	float det = s ^ t;
	if(fabs(det) / (len_s * len_t) < LINES_PARALLEL_EPS && s * t > 0)
	{
		// lines parallel, compute as u1 = q + ns * w/2, u2 = q - ns * w/2
		normals.push_back(ns);
		normals.push_back(-ns);
	}
	else
	{
		// cramers rule
		float lambda = ((nt - ns) ^ t) / det;
		TVector d = ns + s * lambda;
		normals.push_back(d);
		normals.push_back(-d);
	}

	s     = t;
	ns    = nt;
	len_s = len_t;
}

/** Calculate line boundary points.
 *
 * Sketch:
 *
 *     uh1___uh2
 *      .'   '.
 *    .'   q   '.
 *  .'   '   '   '.
 *.'   '  .'.  '   '.
 *   '  .' ul'.  '
 * p  .'       '.  r
 *
 *
 * ul can be found as above, uh1 and uh2 are much simpler:
 *
 * uh1 = q + ns * w/2, uh2 = q + nt * w/2
 */
void BevelJoinPolyline::renderEdge(std::vector<TVector> &anchors, std::vector<TVector> &normals,
                                   TVector &s, float &len_s, TVector &ns,
                                   const TVector &q, const TVector &r, float hw)
{
	TVector t    = (r - q);
	float len_t = t.getLength();

	float det = s ^ t;
	if(fabs(det) / (len_s * len_t) < LINES_PARALLEL_EPS && s * t > 0)
	{
		// lines parallel, compute as u1 = q + ns * w/2, u2 = q - ns * w/2
		TVector n = t.getNormal(hw / len_t);
		anchors.push_back(q);
		anchors.push_back(q);
		normals.push_back(n);
		normals.push_back(-n);
		s     = t;
		len_s = len_t;
		return; // early out
	}

	// cramers rule
	TVector nt= t.getNormal(hw / len_t);
	float lambda = ((nt - ns) ^ t) / det;
	TVector d = ns + s * lambda;

	anchors.push_back(q);
	anchors.push_back(q);
	anchors.push_back(q);
	anchors.push_back(q);
	if(det > 0) // 'left' turn -> intersection on the top
	{
		normals.push_back(d);
		normals.push_back(-ns);
		normals.push_back(d);
		normals.push_back(-nt);
	}
	else
	{
		normals.push_back(ns);
		normals.push_back(-d);
		normals.push_back(nt);
		normals.push_back(-d);
	}
	s     = t;
	len_s = len_t;
	ns    = nt;
}

void TPolyline::calc_overdraw_vertex_count(bool is_looping)
{
	mOverdrawVertexCount = 2 * mVertexCount + (is_looping ? 0 : 2);
}

void TPolyline::render_overdraw(const std::vector<TVector> &normals, float pixel_size, bool is_looping)
{
	// upper segment
	for (size_t i = 0; i + 1 < mVertexCount; i += 2)
	{
		mOverDraw[i]   = mVertices[i];
		mOverDraw[i+1] = mVertices[i] + normals[i] * (pixel_size / normals[i].getLength());
	}
	// lower segment
	for (size_t i = 0; i + 1 < mVertexCount; i += 2)
	{
		size_t k = mVertexCount - i - 1;
		mOverDraw[mVertexCount + i]   = mVertices[k];
		mOverDraw[mVertexCount + i+1] = mVertices[k] + normals[k] * (pixel_size / normals[i].getLength());
	}

	// if not looping, the outer overdraw vertices need to be displaced
	// to cover the line endings, i.e.:
	// +- - - - //- - +         +- - - - - //- - - +
	// +-------//-----+         : +-------//-----+ :
	// | core // line |   -->   : | core // line | :
	// +-----//-------+         : +-----//-------+ :
	// +- - //- - - - +         +- - - //- - - - - +
	if(!is_looping)
	{
		// left edge
		TVector spacer = (mOverDraw[1] - mOverDraw[3]);
		spacer.normalize(pixel_size);
		mOverDraw[1] += spacer;
		mOverDraw[mOverdrawVertexCount - 3] += spacer;

		// right edge
		spacer = (mOverDraw[mVertexCount-1] - mOverDraw[mVertexCount-3]);
		spacer.normalize(pixel_size);
		mOverDraw[mVertexCount-1] += spacer;
		mOverDraw[mVertexCount+1] += spacer;

		// we need to draw two more triangles to close the
		// overdraw at the line start.
		mOverDraw[mOverdrawVertexCount-2] = mOverDraw[0];
		mOverDraw[mOverdrawVertexCount-1] = mOverDraw[1];
	}
}

void TNoneJoinPolyline::calc_overdraw_vertex_count(bool /*is_looping*/)
{
	mOverdrawVertexCount = 4 * (mVertexCount-2); // less than ideal
}

void TNoneJoinPolyline::render_overdraw(const std::vector<TVector> &/*normals*/, float pixel_size, bool /*is_looping*/)
{
	for (size_t i = 2; i + 3 < mVertexCount; i += 4)
	{
		TVector s = mVertices[i] - mVertices[i+3];
		TVector t = mVertices[i] - mVertices[i+1];
		s.normalize(pixel_size);
		t.normalize(pixel_size);

		const size_t k = 4 * (i - 2);
		mOverDraw[k  ] = mVertices[i];
		mOverDraw[k+1] = mVertices[i]   + s + t;
		mOverDraw[k+2] = mVertices[i+1] + s - t;
		mOverDraw[k+3] = mVertices[i+1];

		mOverDraw[k+4] = mVertices[i+1];
		mOverDraw[k+5] = mVertices[i+1] + s - t;
		mOverDraw[k+6] = mVertices[i+2] - s - t;
		mOverDraw[k+7] = mVertices[i+2];

		mOverDraw[k+8]  = mVertices[i+2];
		mOverDraw[k+9]  = mVertices[i+2] - s - t;
		mOverDraw[k+10] = mVertices[i+3] - s + t;
		mOverDraw[k+11] = mVertices[i+3];

		mOverDraw[k+12] = mVertices[i+3];
		mOverDraw[k+13] = mVertices[i+3] - s + t;
		mOverDraw[k+14] = mVertices[i]   + s + t;
		mOverDraw[k+15] = mVertices[i];
	}
}

TPolyline::~TPolyline()
{
	if(mVertices)
		delete[] mVertices;
}

void TPolyline::draw()
{
    TOpenGL::TempDebugGroup debuggroup("Line draw");

	GLushort *indices = nullptr;
	Color *colors = nullptr;

	size_t total_vertex_count = mVertexCount;
	if(mOverDraw)
		total_vertex_count = mOverdrawVertexStart + mOverdrawVertexCount;

	// TODO: We should probably be using a reusable index buffer.
	if(mUseQuadIndices)
	{
		size_t numindices = (total_vertex_count / 4) * 6;

		try
		{
			indices = new GLushort[numindices];
		}
		catch (std::bad_alloc &)
		{
			throw TException("Out of memory.");
		}

		// Fill the index array to make 2 triangles from each quad.
		// NOTE: The triangle vertex ordering here is important!
		for (size_t i = 0; i < numindices / 6; i++)
		{
			// First triangle.
			indices[i * 6 + 0] = GLushort(i * 4 + 0);
			indices[i * 6 + 1] = GLushort(i * 4 + 1);
			indices[i * 6 + 2] = GLushort(i * 4 + 2);

			// Second triangle.
			indices[i * 6 + 3] = GLushort(i * 4 + 0);
			indices[i * 6 + 4] = GLushort(i * 4 + 2);
			indices[i * 6 + 5] = GLushort(i * 4 + 3);
		}
	}

	gl.prepareDraw();

	gl.bindTexture(gl.getDefaultTexture());

	uint32 enabledattribs = ATTRIBFLAG_POS;

	if(mOverDraw)
	{
		// Prepare per-vertex colors. Set the core to white, and the overdraw
		// line's colors to white on one side and transparent on the other.
		colors = new Color[total_vertex_count];
		memset(colors, 255, mOverdrawVertexStart * sizeof(Color));
		fill_color_array(colors + mOverdrawVertexStart);

		glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, colors);

		enabledattribs |= ATTRIBFLAG_COLOR;
	}

	gl.useVertexAttribArrays(enabledattribs);

	glVertexAttribPointer(ATTRIB_POS, 2, GL_FLOAT, GL_FALSE, 0, mVertices);

	// Draw the core line and the overdraw in a single draw call. We can do this
	// because the vertex array contains both the core line and the overdraw
	// vertices.
	if(mUseQuadIndices)
		gl.drawElements(mDrawMode, (int) (total_vertex_count / 4) * 6, GL_UNSIGNED_SHORT, indices);
	else
		gl.drawArrays(mDrawMode, 0, (int) total_vertex_count);

	if(mOverDraw)
		delete[] colors;

	if(indices)
		delete[] indices;
}

void TPolyline::fill_color_array(Color *colors)
{
	for (size_t i = 0; i < mOverdrawVertexCount; ++i)
	{
		// avoids branching. equiv to if(i%2 == 1) colors[i].a = 0;
		colors[i] = {255, 255, 255, GLubyte(255 * ((i+1) % 2))};
	}
}

void TNoneJoinPolyline::fill_color_array(Color *colors)
{
	for (size_t i = 0; i < mOverdrawVertexCount; ++i)
	{
		// if(i % 4 == 1 || i % 4 == 2) colors[i].a = 0
		colors[i] = {255, 255, 255, GLubyte(255 * ((i+1) % 4 < 2))};
	}
}

