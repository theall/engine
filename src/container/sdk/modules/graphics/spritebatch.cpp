/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "common/config.h"
#include "spritebatch.h"

// OpenGL
#include "opengl.h"

#include "glbuffer.h"
#include "texture/texture.h"

// C++
#include <algorithm>

// C
#include <stddef.h>




TSpriteBatch::TSpriteBatch(TTexture *texture, int size, TMesh::Usage usage)
    : mTexture(texture)
    , mSize(size)
    , mNext(0)
    , mColor(0)
    , mArrayBuf(nullptr)
    , mQuadIndices(size)
{
	if(size <= 0)
		throw TException("Invalid SpriteBatch size.");

	GLenum gl_usage = TMesh::getGLBufferUsage(usage);
	size_t vertex_size = sizeof(Vertex) * 4 * size;

    mArrayBuf = new TGLBuffer(vertex_size, nullptr, GL_ARRAY_BUFFER, gl_usage, TGLBuffer::MAP_EXPLICIT_RANGE_MODIFY);
}

TSpriteBatch::~TSpriteBatch()
{
    delete mColor;
    delete mArrayBuf;
}

int TSpriteBatch::add(float x, float y, float a, float sx, float sy, float ox, float oy, float kx, float ky, int index /*= -1*/)
{
	// Only do this if there's a free slot.
    if((index == -1 && mNext >= mSize) || index < -1 || index >= mSize)
		return -1;

	Matrix3 t(x, y, a, sx, sy, ox, oy, kx, ky);

    addv(mTexture->getVertices(), t, (index == -1) ? mNext : index);

	// Increment counter.
	if(index == -1)
        return mNext++;

	return index;
}

int TSpriteBatch::addq(TQuad *quad, float x, float y, float a, float sx, float sy, float ox, float oy, float kx, float ky, int index /*= -1*/)
{
	// Only do this if there's a free slot.
    if((index == -1 && mNext >= mSize) || index < -1 || index >= mNext)
		return -1;

	Matrix3 t(x, y, a, sx, sy, ox, oy, kx, ky);

    addv(quad->getVertices(), t, (index == -1) ? mNext : index);

	// Increment counter.
	if(index == -1)
        return mNext++;

	return index;
}

void TSpriteBatch::clear()
{
	// Reset the position of the next index.
    mNext = 0;
}

void TSpriteBatch::flush()
{
    TGLBuffer::Bind bind(*mArrayBuf);
    mArrayBuf->unmap();
}

void TSpriteBatch::setTexture(TTexture *newtexture)
{
    mTexture.set(newtexture);
}

TTexture *TSpriteBatch::getTexture() const
{
    return mTexture.get();
}

void TSpriteBatch::setColor(const Color &color)
{
    if(!mColor)
        mColor = new Color(color);
	else
        *(mColor) = color;
}

void TSpriteBatch::setColor()
{
    delete mColor;
    mColor = nullptr;
}

const Color *TSpriteBatch::getColor() const
{
    return mColor;
}

int TSpriteBatch::getCount() const
{
    return mNext;
}

void TSpriteBatch::setBufferSize(int newsize)
{
	if(newsize <= 0)
		throw TException("Invalid SpriteBatch size.");

    if(newsize == mSize)
		return;

	// Map the old GLBuffer to get a pointer to its data.
	void *old_data = nullptr;
	{
        TGLBuffer::Bind bind(*mArrayBuf);
        old_data = mArrayBuf->map();
	}

	size_t vertex_size = sizeof(Vertex) * 4 * newsize;
	TGLBuffer *new_array_buf = nullptr;

	try
	{
        new_array_buf = new TGLBuffer(vertex_size, nullptr, mArrayBuf->getTarget(), mArrayBuf->getUsage(), mArrayBuf->getMapFlags());

		// Copy as much of the old data into the new GLBuffer as can fit.
		TGLBuffer::Bind bind(*new_array_buf);
		void *new_data = new_array_buf->map();
        memcpy(new_data, old_data, sizeof(Vertex) * 4 * std::min(newsize, mSize));

        mQuadIndices = QuadIndices(newsize);
	}
	catch (TException &)
	{
		delete new_array_buf;
		throw;
	}

	// We don't need to unmap the old GLBuffer since we're deleting it.
    delete mArrayBuf;

    mArrayBuf = new_array_buf;
    mSize = newsize;

    mNext = std::min(mNext, newsize);
}

int TSpriteBatch::getBufferSize() const
{
    return mSize;
}

void TSpriteBatch::attachAttribute(const std::string &name, TMesh *mesh)
{
	AttachedAttribute oldattrib;
	AttachedAttribute newattrib;

	if(mesh->getVertexCount() < (size_t) getBufferSize() * 4)
		throw TException("Mesh has too few vertices to be attached to this SpriteBatch (at least %d vertices are required)", getBufferSize()*4);

    auto it = mAttachedAttributes.find(name);
    if(it != mAttachedAttributes.end())
		oldattrib = it->second;

	newattrib.index = mesh->getAttributeIndex(name);

	if(newattrib.index < 0)
		throw TException("The specified mesh does not have a vertex attribute named '%s'", name.c_str());

	newattrib.mesh = mesh;

    mAttachedAttributes[name] = newattrib;
}

void TSpriteBatch::draw(float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky)
{
	const size_t pos_offset   = offsetof(Vertex, x);
	const size_t texel_offset = offsetof(Vertex, s);
	const size_t color_offset = offsetof(Vertex, r);

    if(mNext == 0)
		return;

	TOpenGL::TempDebugGroup debuggroup("SpriteBatch draw");

	TOpenGL::TempTransform transform(gl);
	transform.get() *= TMatrix4(x, y, angle, sx, sy, ox, oy, kx, ky);

    gl.bindTexture(*(GLuint *) mTexture->getHandle());

	uint32 enabledattribs = ATTRIBFLAG_POS | ATTRIBFLAG_TEXCOORD;

	{
		// Scope this bind so it doesn't interfere with the
		// Mesh::bindAttributeToShaderInput calls below.
        TGLBuffer::Bind array_bind(*mArrayBuf);

		// Make sure the VBO isn't mapped when we draw (sends data to GPU if needed.)
        mArrayBuf->unmap();

		// Apply per-sprite color, if a color is set.
        if(mColor)
		{
			enabledattribs |= ATTRIBFLAG_COLOR;
            glVertexAttribPointer(ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), mArrayBuf->getPointer(color_offset));
		}

        glVertexAttribPointer(ATTRIB_POS, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), mArrayBuf->getPointer(pos_offset));
        glVertexAttribPointer(ATTRIB_TEXCOORD, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), mArrayBuf->getPointer(texel_offset));
	}

    for (const auto &it : mAttachedAttributes)
	{
		TMesh *mesh = it.second.mesh.get();

		// We have to do this check here as well because setBufferSize can be
		// called after attachAttribute.
		if(mesh->getVertexCount() < (size_t) getBufferSize() * 4)
			throw TException("Mesh with attribute '%s' attached to this SpriteBatch has too few vertices", it.first.c_str());

		int location = mesh->bindAttributeToShaderInput(it.second.index, it.first);

		if(location >= 0)
			enabledattribs |= 1u << (uint32) location;
	}

	gl.useVertexAttribArrays(enabledattribs);

	gl.prepareDraw();

    TGLBuffer::Bind element_bind(*mQuadIndices.getBuffer());
    gl.drawElements(GL_TRIANGLES, (GLsizei) mQuadIndices.getIndexCount(mNext), mQuadIndices.getType(), mQuadIndices.getPointer(0));
}

void TSpriteBatch::addv(const Vertex *v, const Matrix3 &m, int index)
{
	// Needed for colors.
	Vertex sprite[4] = {v[0], v[1], v[2], v[3]};
	const size_t sprite_size = 4 * sizeof(Vertex); // bytecount

	m.transform(sprite, sprite, 4);

    if(mColor)
        setColorv(sprite, *mColor);

    TGLBuffer::Bind bind(*mArrayBuf);

	// Always keep the VBO mapped when adding data for now (it'll be unmapped
	// on draw.)
    mArrayBuf->map();

    mArrayBuf->fill(index * sprite_size, sprite_size, sprite);
}

void TSpriteBatch::setColorv(Vertex *v, const Color &color)
{
	for (size_t i = 0; i < 4; ++i)
	{
		v[i].r = color.r;
		v[i].g = color.g;
		v[i].b = color.b;
		v[i].a = color.a;
	}
}

