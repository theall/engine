/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_GRAPHICS_OPENGL_TEXT_H
#define THEALL_GRAPHICS_OPENGL_TEXT_H

#include "common/config.h"
#include "base/drawable.h"
#include "font.h"
#include "glbuffer.h"

class TText : public TDrawable
{
public:
    TText();
    TText(TFont *font, const std::vector<TFont::ColoredString> &text = {});
    virtual ~TText();

    void set(const std::string &text, const Color &color = Color());
    void set(const TFont::ColoredString &text);
	void set(const std::vector<TFont::ColoredString> &text);
	void set(const std::vector<TFont::ColoredString> &text, float wrap, TFont::AlignMode align);
	void set();

	int add(const std::vector<TFont::ColoredString> &text, float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky);
	int addf(const std::vector<TFont::ColoredString> &text, float wrap, TFont::AlignMode align, float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky);
	void clear();

	// Implements TDrawable.
    virtual void draw(float x, float y, float angle=0.0, float sx=1.0, float sy=1.0, float ox=0.0, float oy=0.0, float kx=0.0, float ky=0.0);

	void setFont(TFont *f);
	TFont *getFont() const;

	/**
	 * Gets the width of the currently set text.
	 **/
	int getWidth(int index = 0) const;

	/**
	 * Gets the height of the currently set text.
	 **/
	int getHeight(int index = 0) const;

private:

	struct TextData
	{
		TFont::ColoredCodepoints codepoints;
		float wrap;
		TFont::AlignMode align;
		TFont::TextInfo text_info;
		bool use_matrix;
		bool append_vertices;
		Matrix3 matrix;
	};

	void uploadVertices(const std::vector<TFont::GlyphVertex> &vertices, size_t vertoffset);
	void regenerateVertices();
	void addTextData(const TextData &s);

	StrongRef<TFont> font;
	TGLBuffer *vbo;

	std::vector<TFont::DrawCommand> draw_commands;

	std::vector<TextData> text_data;

	size_t vert_offset;

	// Used so we know when the font's texture cache is invalidated.
	uint32 texture_cache_id;

}; // Text

#endif // THEALL_GRAPHICS_OPENGL_TEXT_H
