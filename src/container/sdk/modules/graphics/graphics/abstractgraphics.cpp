/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "abstractgraphics.h"
#include "modules/math/math.h"

static bool gammaCorrect = false;

void TAbstractGraphics::setGammaCorrect(bool gammacorrect)
{
	gammaCorrect = gammacorrect;
}

bool TAbstractGraphics::isGammaCorrect()
{
	return gammaCorrect;
}

void TAbstractGraphics::gammaCorrectColor(Colorf &c)
{
	if(isGammaCorrect())
	{
        c.r = TMath::instance()->gammaToLinear(c.r);
        c.g = TMath::instance()->gammaToLinear(c.g);
        c.b = TMath::instance()->gammaToLinear(c.b);
	}
}

void TAbstractGraphics::unGammaCorrectColor(Colorf &c)
{
	if(isGammaCorrect())
	{
        c.r = TMath::instance()->linearToGamma(c.r);
        c.g = TMath::instance()->linearToGamma(c.g);
        c.b = TMath::instance()->linearToGamma(c.b);
	}
}

TAbstractGraphics::~TAbstractGraphics()
{
}

bool TAbstractGraphics::getConstant(const char *in, DrawMode &out)
{
	return drawModes.find(in, out);
}

bool TAbstractGraphics::getConstant(DrawMode in, const char *&out)
{
	return drawModes.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, ArcMode &out)
{
	return arcModes.find(in, out);
}

bool TAbstractGraphics::getConstant(ArcMode in, const char *&out)
{
	return arcModes.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, BlendMode &out)
{
	return blendModes.find(in, out);
}

bool TAbstractGraphics::getConstant(BlendMode in, const char *&out)
{
	return blendModes.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, BlendAlpha &out)
{
	return blendAlphaModes.find(in, out);
}

bool TAbstractGraphics::getConstant(BlendAlpha in, const char *&out)
{
	return blendAlphaModes.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, LineStyle &out)
{
	return lineStyles.find(in, out);
}

bool TAbstractGraphics::getConstant(LineStyle in, const char *&out)
{
	return lineStyles.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, LineJoin &out)
{
	return lineJoins.find(in, out);
}

bool TAbstractGraphics::getConstant(LineJoin in, const char *&out)
{
	return lineJoins.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, StencilAction &out)
{
	return stencilActions.find(in, out);
}

bool TAbstractGraphics::getConstant(StencilAction in, const char *&out)
{
	return stencilActions.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, CompareMode &out)
{
	return compareModes.find(in, out);
}

bool TAbstractGraphics::getConstant(CompareMode in, const char *&out)
{
	return compareModes.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, Feature &out)
{
	return features.find(in, out);
}

bool TAbstractGraphics::getConstant(Feature in, const char *&out)
{
	return features.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, SystemLimit &out)
{
	return systemLimits.find(in, out);
}

bool TAbstractGraphics::getConstant(SystemLimit in, const char *&out)
{
	return systemLimits.find(in, out);
}

bool TAbstractGraphics::getConstant(const char *in, StackType &out)
{
	return stackTypes.find(in, out);
}

bool TAbstractGraphics::getConstant(StackType in, const char *&out)
{
	return stackTypes.find(in, out);
}

TStringMap<TAbstractGraphics::DrawMode, TAbstractGraphics::DRAW_MAX_ENUM>::Entry TAbstractGraphics::drawModeEntries[] =
{
	{ "line", DRAW_LINE },
	{ "fill", DRAW_FILL },
};

TStringMap<TAbstractGraphics::DrawMode, TAbstractGraphics::DRAW_MAX_ENUM> TAbstractGraphics::drawModes(TAbstractGraphics::drawModeEntries, sizeof(TAbstractGraphics::drawModeEntries));

TStringMap<TAbstractGraphics::ArcMode, TAbstractGraphics::ARC_MAX_ENUM>::Entry TAbstractGraphics::arcModeEntries[] =
{
	{ "open",   ARC_OPEN   },
	{ "closed", ARC_CLOSED },
	{ "pie",    ARC_PIE    },
};

TStringMap<TAbstractGraphics::ArcMode, TAbstractGraphics::ARC_MAX_ENUM> TAbstractGraphics::arcModes(TAbstractGraphics::arcModeEntries, sizeof(TAbstractGraphics::arcModeEntries));

TStringMap<TAbstractGraphics::BlendMode, TAbstractGraphics::BLEND_MAX_ENUM>::Entry TAbstractGraphics::blendModeEntries[] =
{
	{ "alpha",    BLEND_ALPHA    },
	{ "add",      BLEND_ADD      },
	{ "subtract", BLEND_SUBTRACT },
	{ "multiply", BLEND_MULTIPLY },
	{ "lighten",  BLEND_LIGHTEN  },
	{ "darken",   BLEND_DARKEN   },
	{ "screen",   BLEND_SCREEN   },
	{ "replace",  BLEND_REPLACE  },
};

TStringMap<TAbstractGraphics::BlendMode, TAbstractGraphics::BLEND_MAX_ENUM> TAbstractGraphics::blendModes(TAbstractGraphics::blendModeEntries, sizeof(TAbstractGraphics::blendModeEntries));

TStringMap<TAbstractGraphics::BlendAlpha, TAbstractGraphics::BLENDALPHA_MAX_ENUM>::Entry TAbstractGraphics::blendAlphaEntries[] =
{
	{ "alphamultiply", BLENDALPHA_MULTIPLY      },
	{ "premultiplied", BLENDALPHA_PREMULTIPLIED },
};

TStringMap<TAbstractGraphics::BlendAlpha, TAbstractGraphics::BLENDALPHA_MAX_ENUM> TAbstractGraphics::blendAlphaModes(TAbstractGraphics::blendAlphaEntries, sizeof(TAbstractGraphics::blendAlphaEntries));

TStringMap<TAbstractGraphics::LineStyle, TAbstractGraphics::LINE_MAX_ENUM>::Entry TAbstractGraphics::lineStyleEntries[] =
{
	{ "smooth", LINE_SMOOTH },
	{ "rough",  LINE_ROUGH  }
};

TStringMap<TAbstractGraphics::LineStyle, TAbstractGraphics::LINE_MAX_ENUM> TAbstractGraphics::lineStyles(TAbstractGraphics::lineStyleEntries, sizeof(TAbstractGraphics::lineStyleEntries));

TStringMap<TAbstractGraphics::LineJoin, TAbstractGraphics::LINE_JOIN_MAX_ENUM>::Entry TAbstractGraphics::lineJoinEntries[] =
{
	{ "none",  LINE_JOIN_NONE  },
	{ "miter", LINE_JOIN_MITER },
	{ "bevel", LINE_JOIN_BEVEL }
};

TStringMap<TAbstractGraphics::LineJoin, TAbstractGraphics::LINE_JOIN_MAX_ENUM> TAbstractGraphics::lineJoins(TAbstractGraphics::lineJoinEntries, sizeof(TAbstractGraphics::lineJoinEntries));

TStringMap<TAbstractGraphics::StencilAction, TAbstractGraphics::STENCIL_MAX_ENUM>::Entry TAbstractGraphics::stencilActionEntries[] =
{
	{ "replace", STENCIL_REPLACE },
	{ "increment", STENCIL_INCREMENT },
	{ "decrement", STENCIL_DECREMENT },
	{ "incrementwrap", STENCIL_INCREMENT_WRAP },
	{ "decrementwrap", STENCIL_DECREMENT_WRAP },
	{ "invert", STENCIL_INVERT },
};

TStringMap<TAbstractGraphics::StencilAction, TAbstractGraphics::STENCIL_MAX_ENUM> TAbstractGraphics::stencilActions(TAbstractGraphics::stencilActionEntries, sizeof(TAbstractGraphics::stencilActionEntries));

TStringMap<TAbstractGraphics::CompareMode, TAbstractGraphics::COMPARE_MAX_ENUM>::Entry TAbstractGraphics::compareModeEntries[] =
{
	{ "less",     COMPARE_LESS     },
	{ "lequal",   COMPARE_LEQUAL   },
	{ "equal",    COMPARE_EQUAL    },
	{ "gequal",   COMPARE_GEQUAL   },
	{ "greater",  COMPARE_GREATER  },
	{ "notequal", COMPARE_NOTEQUAL },
	{ "always",   COMPARE_ALWAYS   },
};

TStringMap<TAbstractGraphics::CompareMode, TAbstractGraphics::COMPARE_MAX_ENUM> TAbstractGraphics::compareModes(TAbstractGraphics::compareModeEntries, sizeof(TAbstractGraphics::compareModeEntries));

TStringMap<TAbstractGraphics::Feature, TAbstractGraphics::FEATURE_MAX_ENUM>::Entry TAbstractGraphics::featureEntries[] =
{
	{ "multicanvasformats", FEATURE_MULTI_CANVAS_FORMATS },
	{ "clampzero", FEATURE_CLAMP_ZERO },
	{ "lighten", FEATURE_LIGHTEN },
};

TStringMap<TAbstractGraphics::Feature, TAbstractGraphics::FEATURE_MAX_ENUM> TAbstractGraphics::features(TAbstractGraphics::featureEntries, sizeof(TAbstractGraphics::featureEntries));

TStringMap<TAbstractGraphics::SystemLimit, TAbstractGraphics::LIMIT_MAX_ENUM>::Entry TAbstractGraphics::systemLimitEntries[] =
{
	{ "pointsize",   LIMIT_POINT_SIZE   },
	{ "texturesize", LIMIT_TEXTURE_SIZE },
	{ "multicanvas", LIMIT_MULTI_CANVAS },
	{ "canvasmsaa",  LIMIT_CANVAS_MSAA  },
};

TStringMap<TAbstractGraphics::SystemLimit, TAbstractGraphics::LIMIT_MAX_ENUM> TAbstractGraphics::systemLimits(TAbstractGraphics::systemLimitEntries, sizeof(TAbstractGraphics::systemLimitEntries));

TStringMap<TAbstractGraphics::StackType, TAbstractGraphics::STACK_MAX_ENUM>::Entry TAbstractGraphics::stackTypeEntries[] =
{
	{ "all", STACK_ALL },
	{ "transform", STACK_TRANSFORM },
};

TStringMap<TAbstractGraphics::StackType, TAbstractGraphics::STACK_MAX_ENUM> TAbstractGraphics::stackTypes(TAbstractGraphics::stackTypeEntries, sizeof(TAbstractGraphics::stackTypeEntries));
