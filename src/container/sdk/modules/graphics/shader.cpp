/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "common/config.h"

#include "shader.h"
#include "canvas.h"

// C++
#include <algorithm>
#include <limits>

#include "glsl.hpp"

namespace
{
	// temporarily attaches a shader program (for setting uniforms, etc)
	// reattaches the originally active program when destroyed
	struct TemporaryAttacher
	{
		TemporaryAttacher(TShader *shader)
		: curShader(shader)
		, prevShader(TShader::current)
		{
			curShader->attach(true);
		}

		~TemporaryAttacher()
		{
			if(prevShader != nullptr)
				prevShader->attach();
			else
				curShader->detach();
		}

		TShader *curShader;
		TShader *prevShader;
	};
} // anonymous namespace


TShader *TShader::current = nullptr;
TShader *TShader::defaultShader = nullptr;
TShader *TShader::defaultVideoShader = nullptr;

TShader::ShaderSource TShader::defaultCode[TAbstractGraphics::RENDERER_MAX_ENUM][2] =
{
    createShaderStageCode(true, DEFAULT_CODE::VERTEX, false, false),
    createShaderStageCode(false, DEFAULT_CODE::PIXEL, false, false),

    createShaderStageCode(true, DEFAULT_CODE::VERTEX, false, true),
    createShaderStageCode(false, DEFAULT_CODE::PIXEL, false, true),

    createShaderStageCode(true, DEFAULT_CODE::VERTEX, true, false),
    createShaderStageCode(false, DEFAULT_CODE::PIXEL, true, false),

    createShaderStageCode(true, DEFAULT_CODE::VERTEX, true, true),
    createShaderStageCode(false, DEFAULT_CODE::PIXEL, true, true)
};

TShader::ShaderSource TShader::defaultVideoCode[TAbstractGraphics::RENDERER_MAX_ENUM][2] =
{
    createShaderStageCode(true, DEFAULT_CODE::VERTEX, false, false),
    createShaderStageCode(false, DEFAULT_CODE::VIDEO, false, false),

    createShaderStageCode(true, DEFAULT_CODE::VERTEX, false, true),
    createShaderStageCode(false, DEFAULT_CODE::VIDEO, false, true),

    createShaderStageCode(true, DEFAULT_CODE::VERTEX, true, false),
    createShaderStageCode(false, DEFAULT_CODE::VIDEO, true, false),

    createShaderStageCode(true, DEFAULT_CODE::VERTEX, true, true),
    createShaderStageCode(false, DEFAULT_CODE::VIDEO, true, true)
};

std::vector<int> TShader::textureCounters;

TShader::TShader(const ShaderSource &source)
    : mShaderSource(source)
    , mProgram(0)
    , mBuiltinUniforms()
    , mBuiltinAttributes()
    , mLastCanvas((TCanvas *) -1)
    , mLastViewport()
    , mLastPointSize(0.0f)
    , mVideoTextureUnits()
{
	if(source.vertex.empty() && source.pixel.empty())
		throw TException("Cannot create shader: no source code!");

	// initialize global texture id counters if needed
	if((int) textureCounters.size() < gl.getMaxTextureUnits() - 1)
		textureCounters.resize(gl.getMaxTextureUnits() - 1, 0);

	// load shader source and create program object
	loadVolatile();
}

TShader::~TShader()
{
	if(current == this)
		detach();

    for (const auto &retainable : mBoundRetainables)
		retainable.second->release();

    mBoundRetainables.clear();

	unloadVolatile();
}

GLuint TShader::compileCode(ShaderStage stage, const std::string &code)
{
	GLenum glstage;
	const char *typestr;

	if(!stageNames.find(stage, typestr))
		typestr = "";

	switch (stage)
	{
	case STAGE_VERTEX:
		glstage = GL_VERTEX_SHADER;
		break;
	case STAGE_PIXEL:
		glstage = GL_FRAGMENT_SHADER;
		break;
	default:
		throw TException("Cannot create shader object: unknown shader type.");
		break;
	}

	GLuint shaderid = glCreateShader(glstage);

	if(shaderid == 0)
	{
		if(glGetError() == GL_INVALID_ENUM)
			throw TException("Cannot create %s shader object: %s shaders not supported.", typestr, typestr);
		else
			throw TException("Cannot create %s shader object.", typestr);
	}

	const char *src = code.c_str();
	GLint srclen = (GLint) code.length();
	glShaderSource(shaderid, 1, (const GLchar **)&src, &srclen);

	glCompileShader(shaderid);

	GLint infologlen;
	glGetShaderiv(shaderid, GL_INFO_LOG_LENGTH, &infologlen);

	// Get any warnings the shader compiler may have produced.
	if(infologlen > 0)
	{
		GLchar *infolog = new GLchar[infologlen];
		glGetShaderInfoLog(shaderid, infologlen, nullptr, infolog);

		// Save any warnings for later querying.
        mShaderWarnings[stage] = infolog;

		delete[] infolog;
	}

	GLint status;
	glGetShaderiv(shaderid, GL_COMPILE_STATUS, &status);

	if(status == GL_FALSE)
	{
		glDeleteShader(shaderid);
		throw TException("Cannot compile %s shader code:\n%s",
                              typestr, mShaderWarnings[stage].c_str());
	}

	return shaderid;
}

void TShader::mapActiveUniforms()
{
	// Built-in uniform locations default to -1 (nonexistant.)
	for (int i = 0; i < int(BUILTIN_MAX_ENUM); i++)
        mBuiltinUniforms[i] = -1;

    mUniforms.clear();

	GLint activeprogram = 0;
	glGetIntegerv(GL_CURRENT_PROGRAM, &activeprogram);

    gl.useProgram(mProgram);

	GLint numuniforms;
    glGetProgramiv(mProgram, GL_ACTIVE_UNIFORMS, &numuniforms);

	GLchar cname[256];
	const GLint bufsize = (GLint) (sizeof(cname) / sizeof(GLchar));

	for (int i = 0; i < numuniforms; i++)
	{
		GLsizei namelen = 0;
		Uniform u;

        glGetActiveUniform(mProgram, (GLuint) i, bufsize, &namelen, &u.count, &u.type, cname);

		u.name = std::string(cname, (size_t) namelen);
        u.location = glGetUniformLocation(mProgram, u.name.c_str());
		u.baseType = getUniformBaseType(u.type);

		// Initialize all samplers to 0. Both GLSL and GLSL ES are supposed to
		// do this themselves, but some Android devices (galaxy tab 3 and 4)
		// don't seem to do it...
		if(u.baseType == UNIFORM_SAMPLER)
			glUniform1i(u.location, 0);

		// glGetActiveUniform appends "[0]" to the end of array uniform names...
		if(u.name.length() > 3)
		{
			size_t findpos = u.name.find("[0]");
			if(findpos != std::string::npos && findpos == u.name.length() - 3)
				u.name.erase(u.name.length() - 3);
		}

		// If this is a built-in (LOVE-created) uniform, store the location.
		BuiltinUniform builtin;
		if(builtinNames.find(u.name.c_str(), builtin))
            mBuiltinUniforms[int(builtin)] = u.location;

		if(u.location != -1)
            mUniforms[u.name] = u;
	}

	gl.useProgram(activeprogram);
}

bool TShader::loadVolatile()
{
    TOpenGL::TempDebugGroup debuggroup("Shader load");

    // Recreating the shader program will invalidate uniforms that rely on these.
    mLastCanvas = (TCanvas *) -1;
    mLastViewport = TOpenGL::Viewport();

    mLastPointSize = -1.0f;

	// Invalidate the cached matrices by setting some elements to NaN.
	float nan = std::numeric_limits<float>::quiet_NaN();
    mLastProjectionMatrix.setTranslation(nan, nan);
    mLastTransformMatrix.setTranslation(nan, nan);

	for (int i = 0; i < 3; i++)
        mVideoTextureUnits[i] = 0;

	// zero out active texture list
    mActiveTexUnits.clear();
    mActiveTexUnits.insert(mActiveTexUnits.begin(), gl.getMaxTextureUnits() - 1, 0);

	std::vector<GLuint> shaderids;

    bool gammacorrect = TAbstractGraphics::isGammaCorrect();
    const ShaderSource *defaults = &defaultCode[TAbstractGraphics::RENDERER_OPENGL][gammacorrect ? 1 : 0];
	if(GLAD_ES_VERSION_2_0)
        defaults = &defaultCode[TAbstractGraphics::RENDERER_OPENGLES][gammacorrect ? 1 : 0];

	// The shader program must have both vertex and pixel shader stages.
    const std::string &vertexcode = mShaderSource.vertex.empty() ? defaults->vertex : mShaderSource.vertex;
    const std::string &pixelcode = mShaderSource.pixel.empty() ? defaults->pixel : mShaderSource.pixel;

	try
	{
		shaderids.push_back(compileCode(STAGE_VERTEX, vertexcode));
		shaderids.push_back(compileCode(STAGE_PIXEL, pixelcode));
	}
	catch (TException &)
	{
		for (GLuint id : shaderids)
			glDeleteShader(id);
		throw;
	}

    mProgram = glCreateProgram();

    if(mProgram == 0)
	{
		for (GLuint id : shaderids)
			glDeleteShader(id);
		throw TException("Cannot create shader program object.");
	}

	for (GLuint id : shaderids)
        glAttachShader(mProgram, id);

	// Bind generic vertex attribute indices to names in the shader.
	for (int i = 0; i < int(ATTRIB_MAX_ENUM); i++)
	{
		const char *name = nullptr;
		if(attribNames.find((VertexAttribID) i, name))
            glBindAttribLocation(mProgram, i, (const GLchar *) name);
	}

    glLinkProgram(mProgram);

	// Flag shaders for auto-deletion when the program object is deleted.
	for (GLuint id : shaderids)
		glDeleteShader(id);

	GLint status;
    glGetProgramiv(mProgram, GL_LINK_STATUS, &status);

	if(status == GL_FALSE)
	{
		std::string warnings = getProgramWarnings();
        glDeleteProgram(mProgram);
        mProgram = 0;
		throw TException("Cannot link shader program object:\n%s", warnings.c_str());
	}

	// Get all active uniform variables in this shader from OpenGL.
	mapActiveUniforms();

	for (int i = 0; i < int(ATTRIB_MAX_ENUM); i++)
	{
		const char *name = nullptr;
		if(attribNames.find(VertexAttribID(i), name))
            mBuiltinAttributes[i] = glGetAttribLocation(mProgram, name);
		else
            mBuiltinAttributes[i] = -1;
	}

	if(current == this)
	{
		// make sure glUseProgram gets called.
		current = nullptr;
		attach();
		checkSetBuiltinUniforms();
	}

	return true;
}

void TShader::unloadVolatile()
{
	if(current == this)
		gl.useProgram(0);

    if(mProgram != 0)
	{
        glDeleteProgram(mProgram);
        mProgram = 0;
	}

	// decrement global texture id counters for texture units which had textures bound from this shader
    for (size_t i = 0; i < mActiveTexUnits.size(); ++i)
	{
        if(mActiveTexUnits[i] > 0)
			textureCounters[i] = std::max(textureCounters[i] - 1, 0);
	}

	// active texture list is probably invalid, clear it
    mActiveTexUnits.clear();
    mActiveTexUnits.resize(gl.getMaxTextureUnits() - 1, 0);

    mAttributes.clear();

	// same with uniform location list
    mUniforms.clear();

	// And the locations of any built-in uniform variables.
	for (int i = 0; i < int(BUILTIN_MAX_ENUM); i++)
        mBuiltinUniforms[i] = -1;

    mShaderWarnings.clear();
}

std::string TShader::getProgramWarnings() const
{
	GLint strsize, nullpos;
    glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &strsize);

	if(strsize == 0)
		return "";

	char *tempstr = new char[strsize];
	// be extra sure that the error string will be 0-terminated
	memset(tempstr, '\0', strsize);
    glGetProgramInfoLog(mProgram, strsize, &nullpos, tempstr);
	tempstr[nullpos] = '\0';

	std::string warnings(tempstr);
	delete[] tempstr;

	return warnings;
}

std::string TShader::getWarnings() const
{
	std::string warnings;
	const char *stagestr;

	// Get the individual shader stage warnings
    for (const auto &warning : mShaderWarnings)
	{
		if(stageNames.find(warning.first, stagestr))
			warnings += std::string(stagestr) + std::string(" shader:\n") + warning.second;
	}

	warnings += getProgramWarnings();

	return warnings;
}

void TShader::attach(bool temporary)
{
	if(current != this)
	{
        gl.useProgram(mProgram);
		current = this;
        // retain/release happens in TAbstractGraphics::setShader.
	}

	if(!temporary)
	{
		// make sure all sent textures are properly bound to their respective texture units
		// note: list potentially contains texture ids of deleted/invalid textures!
        for (size_t i = 0; i < mActiveTexUnits.size(); ++i)
		{
            if(mActiveTexUnits[i] > 0)
                gl.bindTextureToUnit(mActiveTexUnits[i], i + 1, false);
		}

		// We always want to use texture unit 0 for everyhing else.
		gl.setTextureUnit(0);
	}
}

void TShader::detach()
{
	if(defaultShader)
	{
		if(current != defaultShader)
			defaultShader->attach();

		return;
	}

	if(current != nullptr)
		gl.useProgram(0);

	current = nullptr;
}

const TShader::Uniform &TShader::getUniform(const std::string &name) const
{
    std::map<std::string, Uniform>::const_iterator it = mUniforms.find(name);

    if(it == mUniforms.end())
		throw TException("Variable '%s' does not exist.\n"
		                      "A common error is to define but not use the variable.", name.c_str());

	return it->second;
}

void TShader::checkSetUniformError(const Uniform &u, int size, int count, UniformType sendtype) const
{
    if(!mProgram)
		throw TException("No active shader program.");

	int realsize = getUniformTypeSize(u.type);

	if(size != realsize)
		throw TException("Value size of %d does not match variable size of %d.", size, realsize);

	if((u.count == 1 && count > 1) || count < 0)
		throw TException("Invalid number of values (expected %d, got %d).", u.count, count);

	if(u.baseType == UNIFORM_SAMPLER && sendtype != u.baseType)
		throw TException("Cannot send a value of this type to an Image variable.");

	if((sendtype == UNIFORM_FLOAT && u.baseType == UNIFORM_INT) || (sendtype == UNIFORM_INT && u.baseType == UNIFORM_FLOAT))
		throw TException("Cannot convert between float and int.");
}

void TShader::sendInt(const std::string &name, int size, const GLint *vec, int count)
{
	TemporaryAttacher attacher(this);

	const Uniform &u = getUniform(name);
	checkSetUniformError(u, size, count, UNIFORM_INT);

	switch (size)
	{
	case 4:
		glUniform4iv(u.location, count, vec);
		break;
	case 3:
		glUniform3iv(u.location, count, vec);
		break;
	case 2:
		glUniform2iv(u.location, count, vec);
		break;
	case 1:
	default:
		glUniform1iv(u.location, count, vec);
		break;
	}
}

void TShader::sendFloat(const std::string &name, int size, const GLfloat *vec, int count)
{
	TemporaryAttacher attacher(this);

	const Uniform &u = getUniform(name);
	checkSetUniformError(u, size, count, UNIFORM_FLOAT);

	switch (size)
	{
	case 4:
		glUniform4fv(u.location, count, vec);
		break;
	case 3:
		glUniform3fv(u.location, count, vec);
		break;
	case 2:
		glUniform2fv(u.location, count, vec);
		break;
	case 1:
	default:
		glUniform1fv(u.location, count, vec);
		break;
	}
}

void TShader::sendMatrix(const std::string &name, int size, const GLfloat *m, int count)
{
	TemporaryAttacher attacher(this);

	if(size < 2 || size > 4)
	{
		throw TException("Invalid matrix size: %dx%d "
							  "(can only set 2x2, 3x3 or 4x4 matrices.)", size,size);
	}

	const Uniform &u = getUniform(name);
	checkSetUniformError(u, size, count, UNIFORM_FLOAT);

	switch (size)
	{
	case 4:
		glUniformMatrix4fv(u.location, count, GL_FALSE, m);
		break;
	case 3:
		glUniformMatrix3fv(u.location, count, GL_FALSE, m);
		break;
	case 2:
	default:
		glUniformMatrix2fv(u.location, count, GL_FALSE, m);
		break;
	}
}

void TShader::sendTexture(const std::string &name, TTexture *texture)
{
	GLuint gltex = *(GLuint *) texture->getHandle();

	TemporaryAttacher attacher(this);

	int texunit = getTextureUnit(name);

	const Uniform &u = getUniform(name);
	checkSetUniformError(u, 1, 1, UNIFORM_SAMPLER);

	// bind texture to assigned texture unit and send uniform to shader program
	gl.bindTextureToUnit(gltex, texunit, true);

	glUniform1i(u.location, texunit);

	// increment global shader texture id counter for this texture unit, if we haven't already
    if(mActiveTexUnits[texunit-1] == 0)
		++textureCounters[texunit-1];

	// store texture id so it can be re-bound to the proper texture unit later
    mActiveTexUnits[texunit-1] = gltex;

	retainObject(name, texture);
}

void TShader::retainObject(const std::string &name, TObject *object)
{
	object->retain();

    auto it = mBoundRetainables.find(name);
    if(it != mBoundRetainables.end())
		it->second->release();

    mBoundRetainables[name] = object;
}

int TShader::getTextureUnit(const std::string &name)
{
    auto it = mTexUnitPool.find(name);

    if(it != mTexUnitPool.end())
		return it->second;

	int texunit = 1;

	// prefer texture units which are unused by all other shaders
	auto freeunit_it = std::find(textureCounters.begin(), textureCounters.end(), 0);

	if(freeunit_it != textureCounters.end())
	{
		// we don't want to use unit 0
		texunit = (int) std::distance(textureCounters.begin(), freeunit_it) + 1;
	}
	else
	{
		// no completely unused texture units exist, try to use next free slot in our own list
        auto nextunit_it = std::find(mActiveTexUnits.begin(), mActiveTexUnits.end(), 0);

        if(nextunit_it == mActiveTexUnits.end())
			throw TException("No more texture units available for shader.");

		// we don't want to use unit 0
        texunit = (int) std::distance(mActiveTexUnits.begin(), nextunit_it) + 1;
	}

    mTexUnitPool[name] = texunit;
	return texunit;
}

TShader::UniformType TShader::getExternVariable(const std::string &name, int &components, int &count)
{
    auto it = mUniforms.find(name);

    if(it == mUniforms.end())
	{
		components = 0;
		count = 0;
		return UNIFORM_UNKNOWN;
	}

	components = getUniformTypeSize(it->second.type);
	count = (int) it->second.count;

	return it->second.baseType;
}

GLint TShader::getAttribLocation(const std::string &name)
{
    auto it = mAttributes.find(name);
    if(it != mAttributes.end())
		return it->second;

    GLint location = glGetAttribLocation(mProgram, name.c_str());

    mAttributes[name] = location;
	return location;
}

bool TShader::hasVertexAttrib(VertexAttribID attrib) const
{
    return mBuiltinAttributes[int(attrib)] != -1;
}

void TShader::setVideoTextures(GLuint ytexture, GLuint cbtexture, GLuint crtexture)
{
	TemporaryAttacher attacher(this);

	// Set up the texture units that will be used by the shader to sample from
	// the textures, if they haven't been set up yet.
    if(mVideoTextureUnits[0] == 0)
	{
		const GLint locs[3] = {
            mBuiltinUniforms[BUILTIN_VIDEO_Y_CHANNEL],
            mBuiltinUniforms[BUILTIN_VIDEO_CB_CHANNEL],
            mBuiltinUniforms[BUILTIN_VIDEO_CR_CHANNEL]
		};

		const char *names[3] = {nullptr, nullptr, nullptr};
		builtinNames.find(BUILTIN_VIDEO_Y_CHANNEL,  names[0]);
		builtinNames.find(BUILTIN_VIDEO_CB_CHANNEL, names[1]);
		builtinNames.find(BUILTIN_VIDEO_CR_CHANNEL, names[2]);

		for (int i = 0; i < 3; i++)
		{
			if(locs[i] >= 0 && names[i] != nullptr)
			{
                mVideoTextureUnits[i] = getTextureUnit(names[i]);

				// Increment global shader texture id counter for this texture
				// unit, if we haven't already.
                if(mActiveTexUnits[mVideoTextureUnits[i] - 1] == 0)
                    ++textureCounters[mVideoTextureUnits[i] - 1];

                glUniform1i(locs[i], mVideoTextureUnits[i]);
			}
		}
	}

	const GLuint textures[3] = {ytexture, cbtexture, crtexture};

	// Bind the textures to their respective texture units.
	for (int i = 0; i < 3; i++)
	{
        if(mVideoTextureUnits[i] != 0)
		{
			// Store texture id so it can be re-bound later.
            mActiveTexUnits[mVideoTextureUnits[i] - 1] = textures[i];
            gl.bindTextureToUnit(textures[i], mVideoTextureUnits[i], false);
		}
	}

	gl.setTextureUnit(0);
}

void TShader::checkSetScreenParams()
{
    TOpenGL::Viewport view = gl.getViewport();

    if(view == mLastViewport && mLastCanvas == TCanvas::current)
		return;

	// In the shader, we do pixcoord.y = gl_FragCoord.y * params.z + params.w.
	// This lets us flip pixcoord.y when needed, to be consistent (drawing with
	// no Canvas active makes the y-values for pixel coordinates flipped.)
	GLfloat params[] = {
		(GLfloat) view.w, (GLfloat) view.h,
		0.0f, 0.0f,
	};

	if(TCanvas::current != nullptr)
	{
		// No flipping: pixcoord.y = gl_FragCoord.y * 1.0 + 0.0.
		params[2] = 1.0f;
		params[3] = 0.0f;
	}
	else
	{
		// gl_FragCoord.y is flipped when drawing to the screen, so we un-flip:
		// pixcoord.y = gl_FragCoord.y * -1.0 + height.
		params[2] = -1.0f;
		params[3] = (GLfloat) view.h;
	}

    GLint location = mBuiltinUniforms[BUILTIN_SCREEN_SIZE];

	if(location >= 0)
	{
		TemporaryAttacher attacher(this);
		glUniform4fv(location, 1, params);
	}

    mLastCanvas = TCanvas::current;
    mLastViewport = view;
}

void TShader::checkSetPointSize(float size)
{
    if(size == mLastPointSize)
		return;

    GLint location = mBuiltinUniforms[BUILTIN_POINT_SIZE];

	if(location >= 0)
	{
		TemporaryAttacher attacher(this);
		glUniform1f(location, size);
	}

    mLastPointSize = size;
}

void TShader::checkSetBuiltinUniforms()
{
	checkSetScreenParams();

	// We use a more efficient method for sending transformation matrices to
	// the GPU on desktop GL.
	if(GLAD_ES_VERSION_2_0)
	{
		checkSetPointSize(gl.getPointSize());

		const TMatrix4 &curxform = gl.matrices.transform.back();
		const TMatrix4 &curproj = gl.matrices.projection.back();

		TemporaryAttacher attacher(this);

		bool tpmatrixneedsupdate = false;

		// Only upload the matrices if they've changed.
        if(memcmp(curxform.getElements(), mLastTransformMatrix.getElements(), sizeof(float) * 16) != 0)
		{
            GLint location = mBuiltinUniforms[BUILTIN_TRANSFORM_MATRIX];
			if(location >= 0)
				glUniformMatrix4fv(location, 1, GL_FALSE, curxform.getElements());

			// Also upload the re-calculated normal matrix, if possible. The
			// normal matrix is the transpose of the inverse of the rotation
			// portion (top-left 3x3) of the transform matrix.
            location = mBuiltinUniforms[BUILTIN_NORMAL_MATRIX];
			if(location >= 0)
			{
				Matrix3 normalmatrix = Matrix3(curxform).transposedInverse();
				glUniformMatrix3fv(location, 1, GL_FALSE, normalmatrix.getElements());
			}

			tpmatrixneedsupdate = true;
            mLastTransformMatrix = curxform;
		}

        if(memcmp(curproj.getElements(), mLastProjectionMatrix.getElements(), sizeof(float) * 16) != 0)
		{
            GLint location = mBuiltinUniforms[BUILTIN_PROJECTION_MATRIX];
			if(location >= 0)
				glUniformMatrix4fv(location, 1, GL_FALSE, curproj.getElements());

			tpmatrixneedsupdate = true;
            mLastProjectionMatrix = curproj;
		}

		if(tpmatrixneedsupdate)
		{
            GLint location = mBuiltinUniforms[BUILTIN_TRANSFORM_PROJECTION_MATRIX];
			if(location >= 0)
			{
				TMatrix4 tp_matrix(curproj * curxform);
				glUniformMatrix4fv(location, 1, GL_FALSE, tp_matrix.getElements());
			}
		}
	}
}

const std::map<std::string, TObject *> &TShader::getBoundRetainables() const
{
    return mBoundRetainables;
}

std::string TShader::getGLSLVersion()
{
	const char *tmp = (const char *) glGetString(GL_SHADING_LANGUAGE_VERSION);

	if(tmp == nullptr)
		return "0.0";

	// the version string always begins with a version number of the format
	//   major_number.minor_number
	// or
	//   major_number.minor_number.release_number
	// we can keep release_number, since it does not affect the check below.
	std::string versionstring(tmp);
	size_t minorendpos = versionstring.find(' ');
	return versionstring.substr(0, minorendpos);
}

bool TShader::isSupported()
{
	return GLAD_ES_VERSION_2_0 || (getGLSLVersion() >= "1.2");
}

int TShader::getUniformTypeSize(GLenum type) const
{
	switch (type)
	{
	case GL_INT:
	case GL_FLOAT:
	case GL_BOOL:
	case GL_SAMPLER_1D:
	case GL_SAMPLER_2D:
	case GL_SAMPLER_3D:
		return 1;
	case GL_INT_VEC2:
	case GL_FLOAT_VEC2:
	case GL_FLOAT_MAT2:
	case GL_BOOL_VEC2:
		return 2;
	case GL_INT_VEC3:
	case GL_FLOAT_VEC3:
	case GL_FLOAT_MAT3:
	case GL_BOOL_VEC3:
		return 3;
	case GL_INT_VEC4:
	case GL_FLOAT_VEC4:
	case GL_FLOAT_MAT4:
	case GL_BOOL_VEC4:
		return 4;
	default:
		return 1;
	}
}

TShader::UniformType TShader::getUniformBaseType(GLenum type) const
{
	switch (type)
	{
	case GL_INT:
	case GL_INT_VEC2:
	case GL_INT_VEC3:
	case GL_INT_VEC4:
		return UNIFORM_INT;
	case GL_FLOAT:
	case GL_FLOAT_VEC2:
	case GL_FLOAT_VEC3:
	case GL_FLOAT_VEC4:
	case GL_FLOAT_MAT2:
	case GL_FLOAT_MAT3:
	case GL_FLOAT_MAT4:
	case GL_FLOAT_MAT2x3:
	case GL_FLOAT_MAT2x4:
	case GL_FLOAT_MAT3x2:
	case GL_FLOAT_MAT3x4:
	case GL_FLOAT_MAT4x2:
	case GL_FLOAT_MAT4x3:
		return UNIFORM_FLOAT;
	case GL_BOOL:
	case GL_BOOL_VEC2:
	case GL_BOOL_VEC3:
	case GL_BOOL_VEC4:
		return UNIFORM_BOOL;
	case GL_SAMPLER_1D:
	case GL_SAMPLER_1D_SHADOW:
	case GL_SAMPLER_1D_ARRAY:
	case GL_SAMPLER_1D_ARRAY_SHADOW:
	case GL_SAMPLER_2D:
	case GL_SAMPLER_2D_MULTISAMPLE:
	case GL_SAMPLER_2D_MULTISAMPLE_ARRAY:
	case GL_SAMPLER_2D_RECT:
	case GL_SAMPLER_2D_RECT_SHADOW:
	case GL_SAMPLER_2D_SHADOW:
	case GL_SAMPLER_2D_ARRAY:
	case GL_SAMPLER_2D_ARRAY_SHADOW:
	case GL_SAMPLER_3D:
	case GL_SAMPLER_CUBE:
	case GL_SAMPLER_CUBE_SHADOW:
	case GL_SAMPLER_CUBE_MAP_ARRAY:
	case GL_SAMPLER_CUBE_MAP_ARRAY_SHADOW:
		return UNIFORM_SAMPLER;
	default:
		return UNIFORM_UNKNOWN;
	}
}

bool TShader::getConstant(const char *in, UniformType &out)
{
	return uniformTypes.find(in, out);
}

bool TShader::getConstant(UniformType in, const char *&out)
{
	return uniformTypes.find(in, out);
}

bool TShader::getConstant(const char *in, VertexAttribID &out)
{
	return attribNames.find(in, out);
}

bool TShader::getConstant(VertexAttribID in, const char *&out)
{
	return attribNames.find(in, out);
}

TStringMap<TShader::ShaderStage, TShader::STAGE_MAX_ENUM>::Entry TShader::stageNameEntries[] =
{
	{"vertex", TShader::STAGE_VERTEX},
	{"pixel", TShader::STAGE_PIXEL},
};

TStringMap<TShader::ShaderStage, TShader::STAGE_MAX_ENUM> TShader::stageNames(TShader::stageNameEntries, sizeof(TShader::stageNameEntries));

TStringMap<TShader::UniformType, TShader::UNIFORM_MAX_ENUM>::Entry TShader::uniformTypeEntries[] =
{
	{"float", TShader::UNIFORM_FLOAT},
	{"int", TShader::UNIFORM_INT},
	{"bool", TShader::UNIFORM_BOOL},
	{"image", TShader::UNIFORM_SAMPLER},
	{"unknown", TShader::UNIFORM_UNKNOWN},
};

TStringMap<TShader::UniformType, TShader::UNIFORM_MAX_ENUM> TShader::uniformTypes(TShader::uniformTypeEntries, sizeof(TShader::uniformTypeEntries));

TStringMap<VertexAttribID, ATTRIB_MAX_ENUM>::Entry TShader::attribNameEntries[] =
{
	{"VertexPosition", ATTRIB_POS},
	{"VertexTexCoord", ATTRIB_TEXCOORD},
	{"VertexColor", ATTRIB_COLOR},
	{"ConstantColor", ATTRIB_CONSTANTCOLOR},
};

TStringMap<VertexAttribID, ATTRIB_MAX_ENUM> TShader::attribNames(TShader::attribNameEntries, sizeof(TShader::attribNameEntries));

TStringMap<TShader::BuiltinUniform, TShader::BUILTIN_MAX_ENUM>::Entry TShader::builtinNameEntries[] =
{
	{"TransformMatrix", TShader::BUILTIN_TRANSFORM_MATRIX},
	{"ProjectionMatrix", TShader::BUILTIN_PROJECTION_MATRIX},
	{"TransformProjectionMatrix", TShader::BUILTIN_TRANSFORM_PROJECTION_MATRIX},
	{"NormalMatrix", TShader::BUILTIN_NORMAL_MATRIX},
    {"theall_PointSize", TShader::BUILTIN_POINT_SIZE},
    {"theall_ScreenSize", TShader::BUILTIN_SCREEN_SIZE},
    {"theall_VideoYChannel", TShader::BUILTIN_VIDEO_Y_CHANNEL},
    {"theall_VideoCbChannel", TShader::BUILTIN_VIDEO_CB_CHANNEL},
    {"theall_VideoCrChannel", TShader::BUILTIN_VIDEO_CR_CHANNEL},
};

TStringMap<TShader::BuiltinUniform, TShader::BUILTIN_MAX_ENUM> TShader::builtinNames(TShader::builtinNameEntries, sizeof(TShader::builtinNameEntries));

