/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "mesh.h"
#include "common/matrix.h"
#include "common/exception.h"
#include "shader.h"

// C++
#include <algorithm>
#include <limits>




static const char *getBuiltinAttribName(VertexAttribID attribid)
{
	const char *name = "";
	TShader::getConstant(attribid, name);
	return name;
}

static_assert(offsetof(Vertex, x) == sizeof(float) * 0, "Incorrect position offset in Vertex struct");
static_assert(offsetof(Vertex, s) == sizeof(float) * 2, "Incorrect texture coordinate offset in Vertex struct");
static_assert(offsetof(Vertex, r) == sizeof(float) * 4, "Incorrect color offset in Vertex struct");

static std::vector<TMesh::AttribFormat> getDefaultVertexFormat()
{
	// Corresponds to the Vertex struct.
	std::vector<TMesh::AttribFormat> vertexformat = {
		{getBuiltinAttribName(ATTRIB_POS),      TMesh::DATA_FLOAT, 2},
		{getBuiltinAttribName(ATTRIB_TEXCOORD), TMesh::DATA_FLOAT, 2},
		{getBuiltinAttribName(ATTRIB_COLOR),    TMesh::DATA_BYTE,  4},
	};

	return vertexformat;
}

TMesh::TMesh(const std::vector<AttribFormat> &vertexformat, const void *data, size_t datasize, DrawMode drawmode, Usage usage)
    : mVertexFormat(vertexformat)
    , mVbo(nullptr)
    , mVertexCount(0)
    , mVertexStride(0)
    , mIbo(nullptr)
    , mUseIndexBuffer(false)
    , mElementCount(0)
    , mElementDataType(0)
    , mDrawMode(drawmode)
    , mRangeMin(-1)
    , mRangeMax(-1)
{
	setupAttachedAttributes();
	calculateAttributeSizes();

    mVertexCount = datasize / mVertexStride;
    mElementDataType = getGLDataTypeFromMax(mVertexCount);

    if(mVertexCount == 0)
		throw TException("Data size is too small for specified vertex attribute formats.");

    mVbo = new TGLBuffer(datasize, data, GL_ARRAY_BUFFER, getGLBufferUsage(usage), TGLBuffer::MAP_EXPLICIT_RANGE_MODIFY);

    mVertexScratchBuffer = new char[mVertexStride];
}

TMesh::TMesh(const std::vector<AttribFormat> &vertexformat, int vertexcount, DrawMode drawmode, Usage usage)
    : mVertexFormat(vertexformat)
    , mVbo(nullptr)
    , mVertexCount((size_t) vertexcount)
    , mVertexStride(0)
    , mIbo(nullptr)
    , mUseIndexBuffer(false)
    , mElementCount(0)
    , mElementDataType(getGLDataTypeFromMax(vertexcount))
    , mDrawMode(drawmode)
    , mRangeMin(-1)
    , mRangeMax(-1)
{
	if(vertexcount <= 0)
		throw TException("Invalid number of vertices (%d).", vertexcount);

	setupAttachedAttributes();
	calculateAttributeSizes();

    size_t buffersize = mVertexCount * mVertexStride;

    mVbo = new TGLBuffer(buffersize, nullptr, GL_ARRAY_BUFFER, getGLBufferUsage(usage), TGLBuffer::MAP_EXPLICIT_RANGE_MODIFY);

	// Initialize the buffer's contents to 0.
    TGLBuffer::Bind bind(*mVbo);
    memset(mVbo->map(), 0, buffersize);
    mVbo->setMappedRangeModified(0, mVbo->getSize());
    mVbo->unmap();

    mVertexScratchBuffer = new char[mVertexStride];
}

TMesh::TMesh(const std::vector<Vertex> &vertices, DrawMode drawmode, Usage usage)
	: TMesh(getDefaultVertexFormat(), &vertices[0], vertices.size() * sizeof(Vertex), drawmode, usage)
{
}

TMesh::TMesh(int vertexcount, DrawMode drawmode, Usage usage)
	: TMesh(getDefaultVertexFormat(), vertexcount, drawmode, usage)
{
}

TMesh::~TMesh()
{
    delete mVbo;
    delete mIbo;
    delete mVertexScratchBuffer;

    for (const auto &attrib : mAttachedAttributes)
	{
		if(attrib.second.mesh != this)
			attrib.second.mesh->release();
	}
}

void TMesh::setupAttachedAttributes()
{
    for (size_t i = 0; i < mVertexFormat.size(); i++)
	{
        const std::string &name = mVertexFormat[i].name;

        if(mAttachedAttributes.find(name) != mAttachedAttributes.end())
			throw TException("Duplicate vertex attribute name: %s", name.c_str());

        mAttachedAttributes[name] = {this, (int) i, true};
	}
}

void TMesh::calculateAttributeSizes()
{
	size_t stride = 0;

    for (const AttribFormat &format : mVertexFormat)
	{
		// Hardware really doesn't like attributes that aren't 32 bit-aligned.
		if(format.type == DATA_BYTE && format.components != 4)
			throw TException("byte vertex attributes must have 4 components.");

		if(format.components <= 0 || format.components > 4)
			throw TException("Vertex attributes must have between 1 and 4 components.");

		// Total size in bytes of each attribute in a single vertex.
        mAttributeSizes.push_back(getAttribFormatSize(format));
        stride += mAttributeSizes.back();
	}

    mVertexStride = stride;
}

size_t TMesh::getAttributeOffset(size_t attribindex) const
{
	size_t offset = 0;

	for (size_t i = 0; i < attribindex; i++)
        offset += mAttributeSizes[i];

	return offset;
}

void TMesh::setVertex(size_t vertindex, const void *data, size_t datasize)
{
    if(vertindex >= mVertexCount)
		throw TException("Invalid vertex index: %ld", vertindex + 1);

    size_t offset = vertindex * mVertexStride;
    size_t size = std::min(datasize, mVertexStride);

    TGLBuffer::Bind bind(*mVbo);
    uint8 *bufferdata = (uint8 *) mVbo->map();

	memcpy(bufferdata + offset, data, size);

    mVbo->setMappedRangeModified(offset, size);
}

size_t TMesh::getVertex(size_t vertindex, void *data, size_t datasize)
{
    if(vertindex >= mVertexCount)
		throw TException("Invalid vertex index: %ld", vertindex + 1);

    size_t offset = vertindex * mVertexStride;
    size_t size = std::min(datasize, mVertexStride);

	// We're relying on vbo->map() returning read/write data... ew.
    TGLBuffer::Bind bind(*mVbo);
    const uint8 *bufferdata = (const uint8 *) mVbo->map();

	memcpy(data, bufferdata + offset, size);

	return size;
}

void *TMesh::getVertexScratchBuffer()
{
    return mVertexScratchBuffer;
}

void TMesh::setVertexAttribute(size_t vertindex, int attribindex, const void *data, size_t datasize)
{
    if(vertindex >= mVertexCount)
		throw TException("Invalid vertex index: %ld", vertindex + 1);

    if(attribindex >= (int) mVertexFormat.size())
		throw TException("Invalid vertex attribute index: %d", attribindex + 1);

    size_t offset = vertindex * mVertexStride + getAttributeOffset(attribindex);
    size_t size = std::min(datasize, mAttributeSizes[attribindex]);

    TGLBuffer::Bind bind(*mVbo);
    uint8 *bufferdata = (uint8 *) mVbo->map();

	memcpy(bufferdata + offset, data, size);

    mVbo->setMappedRangeModified(offset, size);
}

size_t TMesh::getVertexAttribute(size_t vertindex, int attribindex, void *data, size_t datasize)
{
    if(vertindex >= mVertexCount)
		throw TException("Invalid vertex index: %ld", vertindex + 1);

    if(attribindex >= (int) mVertexFormat.size())
		throw TException("Invalid vertex attribute index: %d", attribindex + 1);

    size_t offset = vertindex * mVertexStride + getAttributeOffset(attribindex);
    size_t size = std::min(datasize, mAttributeSizes[attribindex]);

	// We're relying on vbo->map() returning read/write data... ew.
    TGLBuffer::Bind bind(*mVbo);
    const uint8 *bufferdata = (const uint8 *) mVbo->map();

	memcpy(data, bufferdata + offset, size);

	return size;
}

size_t TMesh::getVertexCount() const
{
    return mVertexCount;
}

size_t TMesh::getVertexStride() const
{
    return mVertexStride;
}

const std::vector<TMesh::AttribFormat> &TMesh::getVertexFormat() const
{
    return mVertexFormat;
}

TMesh::DataType TMesh::getAttributeInfo(int attribindex, int &components) const
{
    if(attribindex < 0 || attribindex >= (int) mVertexFormat.size())
		throw TException("Invalid vertex attribute index: %d", attribindex + 1);

    DataType type = mVertexFormat[attribindex].type;
    components = mVertexFormat[attribindex].components;

	return type;
}

int TMesh::getAttributeIndex(const std::string &name) const
{
    for (int i = 0; i < (int) mVertexFormat.size(); i++)
	{
        if(mVertexFormat[i].name == name)
			return i;
	}

	return -1;
}

void TMesh::setAttributeEnabled(const std::string &name, bool enable)
{
    auto it = mAttachedAttributes.find(name);

    if(it == mAttachedAttributes.end())
		throw TException("Mesh does not have an attached vertex attribute named '%s'", name.c_str());

	it->second.enabled = enable;
}

bool TMesh::isAttributeEnabled(const std::string &name) const
{
    const auto it = mAttachedAttributes.find(name);

    if(it == mAttachedAttributes.end())
		throw TException("Mesh does not have an attached vertex attribute named '%s'", name.c_str());

	return it->second.enabled;
}

void TMesh::attachAttribute(const std::string &name, TMesh *mesh)
{
	if(mesh != this)
	{
        for (const auto &it : mesh->mAttachedAttributes)
		{
			// If the supplied Mesh has attached attributes of its own, then we
			// prevent it from being attached to avoid reference cycles.
			if(it.second.mesh != mesh)
				throw TException("Cannot attach a Mesh which has attached Meshes of its own.");
		}
	}

	AttachedAttribute oldattrib;
	AttachedAttribute newattrib;

    auto it = mAttachedAttributes.find(name);
    if(it != mAttachedAttributes.end())
		oldattrib = it->second;

	newattrib.mesh = mesh;
	newattrib.enabled = oldattrib.mesh ? oldattrib.enabled : true;
	newattrib.index = mesh->getAttributeIndex(name);

	if(newattrib.index < 0)
		throw TException("The specified mesh does not have a vertex attribute named '%s'", name.c_str());

	if(newattrib.mesh != this)
		newattrib.mesh->retain();

    mAttachedAttributes[name] = newattrib;

	if(oldattrib.mesh && oldattrib.mesh != this)
		oldattrib.mesh->release();
}

void *TMesh::mapVertexData()
{
    TGLBuffer::Bind bind(*mVbo);
    return mVbo->map();
}

void TMesh::unmapVertexData(size_t modifiedoffset, size_t modifiedsize)
{
    TGLBuffer::Bind bind(*mVbo);
    mVbo->setMappedRangeModified(modifiedoffset, modifiedsize);
    mVbo->unmap();
}

void TMesh::flush()
{
	{
        TGLBuffer::Bind vbobind(*mVbo);
        mVbo->unmap();
	}

    if(mIbo != nullptr)
	{
        TGLBuffer::Bind ibobind(*mIbo);
        mIbo->unmap();
	}
}

/**
 * Copies index data from a vector to a mapped index buffer.
 **/
template <typename T>
static void copyToIndexBuffer(const std::vector<uint32> &indices, TGLBuffer::Mapper &buffermap, size_t maxval)
{
	T *elems = (T *) buffermap.get();

	for (size_t i = 0; i < indices.size(); i++)
	{
		if(indices[i] >= maxval)
			throw TException("Invalid vertex map value: %d", indices[i] + 1);

		elems[i] = (T) indices[i];
	}
}

void TMesh::setVertexMap(const std::vector<uint32> &map)
{
	size_t maxval = getVertexCount();

	GLenum datatype = getGLDataTypeFromMax(maxval);

	// Calculate the size in bytes of the index buffer data.
	size_t size = map.size() * getGLDataTypeSize(datatype);

    if(mIbo && size > mIbo->getSize())
	{
        delete mIbo;
        mIbo = nullptr;
	}

    if(!mIbo && size > 0)
        mIbo = new TGLBuffer(size, nullptr, GL_ELEMENT_ARRAY_BUFFER, mVbo->getUsage());

    mUseIndexBuffer = true;
    mElementCount = map.size();

    if(!mIbo || mElementCount == 0)
		return;

    TGLBuffer::Bind ibobind(*mIbo);
    TGLBuffer::Mapper ibomap(*mIbo);

	// Fill the buffer with the index values from the vector.
	switch (datatype)
	{
	case GL_UNSIGNED_SHORT:
		copyToIndexBuffer<uint16>(map, ibomap, maxval);
		break;
	case GL_UNSIGNED_INT:
	default:
		copyToIndexBuffer<uint32>(map, ibomap, maxval);
		break;
	}

    mElementDataType = datatype;
}

void TMesh::setVertexMap()
{
    mUseIndexBuffer = false;
}

/**
 * Copies index data from a mapped buffer to a vector.
 **/
template <typename T>
static void copyFromIndexBuffer(void *buffer, size_t count, std::vector<uint32> &indices)
{
	T *elems = (T *) buffer;
	for (size_t i = 0; i < count; i++)
		indices.push_back((uint32) elems[i]);
}

bool TMesh::getVertexMap(std::vector<uint32> &map) const
{
    if(!mUseIndexBuffer)
		return false;

	map.clear();
    map.reserve(mElementCount);

    if(!mIbo || mElementCount == 0)
		return true;

    TGLBuffer::Bind ibobind(*mIbo);

	// We unmap the buffer in Mesh::draw, Mesh::setVertexMap, and Mesh::flush.
    void *buffer = mIbo->map();

	// Fill the vector from the buffer.
    switch (mElementDataType)
	{
	case GL_UNSIGNED_SHORT:
        copyFromIndexBuffer<uint16>(buffer, mElementCount, map);
		break;
	case GL_UNSIGNED_INT:
	default:
        copyFromIndexBuffer<uint32>(buffer, mElementCount, map);
		break;
	}

	return true;
}

size_t TMesh::getVertexMapCount() const
{
    return mElementCount;
}

void TMesh::setTexture(TTexture *tex)
{
    mTexture.set(tex);
}

void TMesh::setTexture()
{
    mTexture.set(nullptr);
}

TTexture *TMesh::getTexture() const
{
    return mTexture.get();
}

void TMesh::setDrawMode(DrawMode mode)
{
    mDrawMode = mode;
}

TMesh::DrawMode TMesh::getDrawMode() const
{
    return mDrawMode;
}

void TMesh::setDrawRange(int min, int max)
{
	if(min < 0 || max < 0 || min > max)
		throw TException("Invalid draw range.");

    mRangeMin = min;
    mRangeMax = max;
}

void TMesh::setDrawRange()
{
    mRangeMin = mRangeMax = -1;
}

void TMesh::getDrawRange(int &min, int &max) const
{
    min = mRangeMin;
    max = mRangeMax;
}

int TMesh::bindAttributeToShaderInput(int attributeindex, const std::string &inputname)
{
    const AttribFormat &format = mVertexFormat[attributeindex];

	GLint attriblocation = -1;

	// If the attribute is one of the LOVE-defined ones, use the constant
	// attribute index for it, otherwise query the index from the shader.
	VertexAttribID builtinattrib;
	if(TShader::getConstant(inputname.c_str(), builtinattrib))
		attriblocation = (GLint) builtinattrib;
	else if(TShader::current)
		attriblocation = TShader::current->getAttribLocation(inputname);

	// The active shader might not use this vertex attribute name.
	if(attriblocation < 0)
		return attriblocation;

	// Needed for unmap and glVertexAttribPointer.
    TGLBuffer::Bind vbobind(*mVbo);

	// Make sure the buffer isn't mapped (sends data to GPU if needed.)
    mVbo->unmap();

    const void *gloffset = mVbo->getPointer(getAttributeOffset(attributeindex));
	GLenum datatype = getGLDataType(format.type);
	GLboolean normalized = (datatype == GL_UNSIGNED_BYTE);

    glVertexAttribPointer(attriblocation, format.components, datatype, normalized, mVertexStride, gloffset);

	return attriblocation;
}

void TMesh::draw(float x, float y, float angle, float sx, float sy, float ox, float oy, float kx, float ky)
{
	TOpenGL::TempDebugGroup debuggroup("Mesh draw");

	uint32 enabledattribs = 0;

    for (const auto &attrib : mAttachedAttributes)
	{
		if(!attrib.second.enabled)
			continue;

		TMesh *mesh = attrib.second.mesh;
		int location = mesh->bindAttributeToShaderInput(attrib.second.index, attrib.first);

		if(location >= 0)
			enabledattribs |= 1u << (uint32) location;
	}

	// Not supported on all platforms or GL versions, I believe.
	if(!(enabledattribs & ATTRIBFLAG_POS))
		throw TException("Mesh must have an enabled VertexPosition attribute to be drawn.");

	gl.useVertexAttribArrays(enabledattribs);

    if(mTexture.get())
        gl.bindTexture(*(GLuint *) mTexture->getHandle());
	else
		gl.bindTexture(gl.getDefaultTexture());

	TMatrix4 m(x, y, angle, sx, sy, ox, oy, kx, ky);

	TOpenGL::TempTransform transform(gl);
	transform.get() *= m;

	gl.prepareDraw();

    if(mUseIndexBuffer && mIbo && mElementCount > 0)
	{
		// Use the custom vertex map (index buffer) to draw the vertices.
        TGLBuffer::Bind ibo_bind(*mIbo);

		// Make sure the index buffer isn't mapped (sends data to GPU if needed.)
        mIbo->unmap();

        int max = (int) mElementCount - 1;
        if(mRangeMax >= 0)
            max = std::min(mRangeMax, max);

		int min = 0;
        if(mRangeMin >= 0)
            min = std::min(mRangeMin, max);

        GLenum type = mElementDataType;
        const void *indices = mIbo->getPointer(min * getGLDataTypeSize(type));

        gl.drawElements(getGLDrawMode(mDrawMode), max - min + 1, type, indices);
	}
	else
	{
        int max = (int) mVertexCount - 1;
        if(mRangeMax >= 0)
            max = std::min(mRangeMax, max);

		int min = 0;
        if(mRangeMin >= 0)
            min = std::min(mRangeMin, max);

		// Normal non-indexed drawing (no custom vertex map.)
        gl.drawArrays(getGLDrawMode(mDrawMode), min, max - min + 1);
	}
}

size_t TMesh::getAttribFormatSize(const AttribFormat &format)
{
	switch (format.type)
	{
	case DATA_BYTE:
		return format.components * sizeof(uint8);
	case DATA_FLOAT:
		return format.components * sizeof(float);
	default:
		return 0;
	}
}

GLenum TMesh::getGLDrawMode(DrawMode mode)
{
	switch (mode)
	{
	case DRAWMODE_FAN:
		return GL_TRIANGLE_FAN;
	case DRAWMODE_STRIP:
		return GL_TRIANGLE_STRIP;
	case DRAWMODE_TRIANGLES:
	default:
		return GL_TRIANGLES;
	case DRAWMODE_POINTS:
		return GL_POINTS;
	}
}

GLenum TMesh::getGLDataType(DataType type)
{
	switch (type)
	{
	case DATA_BYTE:
		return GL_UNSIGNED_BYTE;
	case DATA_FLOAT:
		return GL_FLOAT;
	default:
		return 0;
	}
}

GLenum TMesh::getGLDataTypeFromMax(size_t maxvalue)
{
	if(maxvalue > THEALL_UINT16_MAX)
		return GL_UNSIGNED_INT;
	else
		return GL_UNSIGNED_SHORT;
}

size_t TMesh::getGLDataTypeSize(GLenum datatype)
{
	switch (datatype)
	{
	case GL_UNSIGNED_BYTE:
		return sizeof(uint8);
	case GL_UNSIGNED_SHORT:
		return sizeof(uint16);
	case GL_UNSIGNED_INT:
		return sizeof(uint32);
	default:
		return 0;
	}
}

GLenum TMesh::getGLBufferUsage(Usage usage)
{
	switch (usage)
	{
	case USAGE_STREAM:
		return GL_STREAM_DRAW;
	case USAGE_DYNAMIC:
		return GL_DYNAMIC_DRAW;
	case USAGE_STATIC:
		return GL_STATIC_DRAW;
	default:
		return 0;
	}
}

bool TMesh::getConstant(const char *in, Usage &out)
{
	return usages.find(in, out);
}

bool TMesh::getConstant(Usage in, const char *&out)
{
	return usages.find(in, out);
}

bool TMesh::getConstant(const char *in, TMesh::DrawMode &out)
{
	return drawModes.find(in, out);
}

bool TMesh::getConstant(TMesh::DrawMode in, const char *&out)
{
	return drawModes.find(in, out);
}

bool TMesh::getConstant(const char *in, DataType &out)
{
	return dataTypes.find(in, out);
}

bool TMesh::getConstant(DataType in, const char *&out)
{
	return dataTypes.find(in, out);
}

TStringMap<TMesh::Usage, TMesh::USAGE_MAX_ENUM>::Entry TMesh::usageEntries[] =
{
	{"stream", USAGE_STREAM},
	{"dynamic", USAGE_DYNAMIC},
	{"static", USAGE_STATIC},
};

TStringMap<TMesh::Usage, TMesh::USAGE_MAX_ENUM> TMesh::usages(TMesh::usageEntries, sizeof(TMesh::usageEntries));

TStringMap<TMesh::DrawMode, TMesh::DRAWMODE_MAX_ENUM>::Entry TMesh::drawModeEntries[] =
{
	{"fan", DRAWMODE_FAN},
	{"strip", DRAWMODE_STRIP},
	{"triangles", DRAWMODE_TRIANGLES},
	{"points", DRAWMODE_POINTS},
};

TStringMap<TMesh::DrawMode, TMesh::DRAWMODE_MAX_ENUM> TMesh::drawModes(TMesh::drawModeEntries, sizeof(TMesh::drawModeEntries));

TStringMap<TMesh::DataType, TMesh::DATA_MAX_ENUM>::Entry TMesh::dataTypeEntries[] =
{
	{"byte", DATA_BYTE},
	{"float", DATA_FLOAT},
};

TStringMap<TMesh::DataType, TMesh::DATA_MAX_ENUM> TMesh::dataTypes(TMesh::dataTypeEntries, sizeof(TMesh::dataTypeEntries));

