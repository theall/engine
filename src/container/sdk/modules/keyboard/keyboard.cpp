/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "keyboard.h"
#include "../window/window.h"

TKeyboard *TKeyboard::mInstance=NULL;

TStringMap<TKeyboard::Key, TKeyboard::KEY_MAX_ENUM>::Entry TKeyboard::mKeyEntries[] =
{
    {"unknown", TKeyboard::KEY_UNKNOWN},

    {"return", TKeyboard::KEY_RETURN},
    {"escape", TKeyboard::KEY_ESCAPE},
    {"backspace", TKeyboard::KEY_BACKSPACE},
    {"tab", TKeyboard::KEY_TAB},
    {"space", TKeyboard::KEY_SPACE},
    {"!", TKeyboard::KEY_EXCLAIM},
    {"\"", TKeyboard::KEY_QUOTEDBL},
    {"#", TKeyboard::KEY_HASH},
    {"%", TKeyboard::KEY_PERCENT},
    {"$", TKeyboard::KEY_DOLLAR},
    {"&", TKeyboard::KEY_AMPERSAND},
    {"'", TKeyboard::KEY_QUOTE},
    {"(", TKeyboard::KEY_LEFTPAREN},
    {")", TKeyboard::KEY_RIGHTPAREN},
    {"*", TKeyboard::KEY_ASTERISK},
    {"+", TKeyboard::KEY_PLUS},
    {",", TKeyboard::KEY_COMMA},
    {"-", TKeyboard::KEY_MINUS},
    {".", TKeyboard::KEY_PERIOD},
    {"/", TKeyboard::KEY_SLASH},
    {"0", TKeyboard::KEY_0},
    {"1", TKeyboard::KEY_1},
    {"2", TKeyboard::KEY_2},
    {"3", TKeyboard::KEY_3},
    {"4", TKeyboard::KEY_4},
    {"5", TKeyboard::KEY_5},
    {"6", TKeyboard::KEY_6},
    {"7", TKeyboard::KEY_7},
    {"8", TKeyboard::KEY_8},
    {"9", TKeyboard::KEY_9},
    {":", TKeyboard::KEY_COLON},
    {";", TKeyboard::KEY_SEMICOLON},
    {"<", TKeyboard::KEY_LESS},
    {"=", TKeyboard::KEY_EQUALS},
    {">", TKeyboard::KEY_GREATER},
    {"?", TKeyboard::KEY_QUESTION},
    {"@", TKeyboard::KEY_AT},

    {"[", TKeyboard::KEY_LEFTBRACKET},
    {"\\", TKeyboard::KEY_BACKSLASH},
    {"]", TKeyboard::KEY_RIGHTBRACKET},
    {"^", TKeyboard::KEY_CARET},
    {"_", TKeyboard::KEY_UNDERSCORE},
    {"`", TKeyboard::KEY_BACKQUOTE},
    {"a", TKeyboard::KEY_A},
    {"b", TKeyboard::KEY_B},
    {"c", TKeyboard::KEY_C},
    {"d", TKeyboard::KEY_D},
    {"e", TKeyboard::KEY_E},
    {"f", TKeyboard::KEY_F},
    {"g", TKeyboard::KEY_G},
    {"h", TKeyboard::KEY_H},
    {"i", TKeyboard::KEY_I},
    {"j", TKeyboard::KEY_J},
    {"k", TKeyboard::KEY_K},
    {"l", TKeyboard::KEY_L},
    {"m", TKeyboard::KEY_M},
    {"n", TKeyboard::KEY_N},
    {"o", TKeyboard::KEY_O},
    {"p", TKeyboard::KEY_P},
    {"q", TKeyboard::KEY_Q},
    {"r", TKeyboard::KEY_R},
    {"s", TKeyboard::KEY_S},
    {"t", TKeyboard::KEY_T},
    {"u", TKeyboard::KEY_U},
    {"v", TKeyboard::KEY_V},
    {"w", TKeyboard::KEY_W},
    {"x", TKeyboard::KEY_X},
    {"y", TKeyboard::KEY_Y},
    {"z", TKeyboard::KEY_Z},

    {"capslock", TKeyboard::KEY_CAPSLOCK},

    {"f1", TKeyboard::KEY_F1},
    {"f2", TKeyboard::KEY_F2},
    {"f3", TKeyboard::KEY_F3},
    {"f4", TKeyboard::KEY_F4},
    {"f5", TKeyboard::KEY_F5},
    {"f6", TKeyboard::KEY_F6},
    {"f7", TKeyboard::KEY_F7},
    {"f8", TKeyboard::KEY_F8},
    {"f9", TKeyboard::KEY_F9},
    {"f10", TKeyboard::KEY_F10},
    {"f11", TKeyboard::KEY_F11},
    {"f12", TKeyboard::KEY_F12},

    {"printscreen", TKeyboard::KEY_PRINTSCREEN},
    {"scrolllock", TKeyboard::KEY_SCROLLLOCK},
    {"pause", TKeyboard::KEY_PAUSE},
    {"insert", TKeyboard::KEY_INSERT},
    {"home", TKeyboard::KEY_HOME},
    {"pageup", TKeyboard::KEY_PAGEUP},
    {"delete", TKeyboard::KEY_DELETE},
    {"end", TKeyboard::KEY_END},
    {"pagedown", TKeyboard::KEY_PAGEDOWN},
    {"right", TKeyboard::KEY_RIGHT},
    {"left", TKeyboard::KEY_LEFT},
    {"down", TKeyboard::KEY_DOWN},
    {"up", TKeyboard::KEY_UP},

    {"numlock", TKeyboard::KEY_NUMLOCKCLEAR},
    {"kp/", TKeyboard::KEY_KP_DIVIDE},
    {"kp*", TKeyboard::KEY_KP_MULTIPLY},
    {"kp-", TKeyboard::KEY_KP_MINUS},
    {"kp+", TKeyboard::KEY_KP_PLUS},
    {"kpenter", TKeyboard::KEY_KP_ENTER},
    {"kp0", TKeyboard::KEY_KP_0},
    {"kp1", TKeyboard::KEY_KP_1},
    {"kp2", TKeyboard::KEY_KP_2},
    {"kp3", TKeyboard::KEY_KP_3},
    {"kp4", TKeyboard::KEY_KP_4},
    {"kp5", TKeyboard::KEY_KP_5},
    {"kp6", TKeyboard::KEY_KP_6},
    {"kp7", TKeyboard::KEY_KP_7},
    {"kp8", TKeyboard::KEY_KP_8},
    {"kp9", TKeyboard::KEY_KP_9},
    {"kp.", TKeyboard::KEY_KP_PERIOD},
    {"kp,", TKeyboard::KEY_KP_COMMA},
    {"kp=", TKeyboard::KEY_KP_EQUALS},

    {"application", TKeyboard::KEY_APPLICATION},
    {"power", TKeyboard::KEY_POWER},
    {"f13", TKeyboard::KEY_F13},
    {"f14", TKeyboard::KEY_F14},
    {"f15", TKeyboard::KEY_F15},
    {"f16", TKeyboard::KEY_F16},
    {"f17", TKeyboard::KEY_F17},
    {"f18", TKeyboard::KEY_F18},
    {"f19", TKeyboard::KEY_F19},
    {"f20", TKeyboard::KEY_F20},
    {"f21", TKeyboard::KEY_F21},
    {"f22", TKeyboard::KEY_F22},
    {"f23", TKeyboard::KEY_F23},
    {"f24", TKeyboard::KEY_F24},
    {"execute", TKeyboard::KEY_EXECUTE_},
    {"help", TKeyboard::KEY_HELP},
    {"menu", TKeyboard::KEY_MENU},
    {"select", TKeyboard::KEY_SELECT},
    {"stop", TKeyboard::KEY_STOP},
    {"again", TKeyboard::KEY_AGAIN},
    {"undo", TKeyboard::KEY_UNDO},
    {"cut", TKeyboard::KEY_CUT},
    {"copy", TKeyboard::KEY_COPY},
    {"paste", TKeyboard::KEY_PASTE},
    {"find", TKeyboard::KEY_FIND},
    {"mute", TKeyboard::KEY_MUTE},
    {"volumeup", TKeyboard::KEY_VOLUMEUP},
    {"volumedown", TKeyboard::KEY_VOLUMEDOWN},

    {"alterase", TKeyboard::KEY_ALTERASE},
    {"sysreq", TKeyboard::KEY_SYSREQ},
    {"cancel", TKeyboard::KEY_CANCEL},
    {"clear", TKeyboard::KEY_CLEAR},
    {"prior", TKeyboard::KEY_PRIOR},
    {"return2", TKeyboard::KEY_RETURN2},
    {"separator", TKeyboard::KEY_SEPARATOR},
    {"out", TKeyboard::KEY_OUT},
    {"oper", TKeyboard::KEY_OPER},
    {"clearagain", TKeyboard::KEY_CLEARAGAIN},

    {"thsousandsseparator", TKeyboard::KEY_THOUSANDSSEPARATOR},
    {"decimalseparator", TKeyboard::KEY_DECIMALSEPARATOR},
    {"currencyunit", TKeyboard::KEY_CURRENCYUNIT},
    {"currencysubunit", TKeyboard::KEY_CURRENCYSUBUNIT},

    {"lctrl", TKeyboard::KEY_LCTRL},
    {"lshift", TKeyboard::KEY_LSHIFT},
    {"lalt", TKeyboard::KEY_LALT},
    {"lgui", TKeyboard::KEY_LGUI},
    {"rctrl", TKeyboard::KEY_RCTRL},
    {"rshift", TKeyboard::KEY_RSHIFT},
    {"ralt", TKeyboard::KEY_RALT},
    {"rgui", TKeyboard::KEY_RGUI},

    {"mode", TKeyboard::KEY_MODE},

    {"audionext", TKeyboard::KEY_AUDIONEXT},
    {"audioprev", TKeyboard::KEY_AUDIOPREV},
    {"audiostop", TKeyboard::KEY_AUDIOSTOP},
    {"audioplay", TKeyboard::KEY_AUDIOPLAY},
    {"audiomute", TKeyboard::KEY_AUDIOMUTE},
    {"mediaselect", TKeyboard::KEY_MEDIASELECT},
    {"www", TKeyboard::KEY_WWW},
    {"mail", TKeyboard::KEY_MAIL},
    {"calculator", TKeyboard::KEY_CALCULATOR},
    {"computer", TKeyboard::KEY_COMPUTER},
    {"appsearch", TKeyboard::KEY_APP_SEARCH},
    {"apphome", TKeyboard::KEY_APP_HOME},
    {"appback", TKeyboard::KEY_APP_BACK},
    {"appforward", TKeyboard::KEY_APP_FORWARD},
    {"appstop", TKeyboard::KEY_APP_STOP},
    {"apprefresh", TKeyboard::KEY_APP_REFRESH},
    {"appbookmarks", TKeyboard::KEY_APP_BOOKMARKS},

    {"brightnessdown", TKeyboard::KEY_BRIGHTNESSDOWN},
    {"brightnessup", TKeyboard::KEY_BRIGHTNESSUP},
    {"displayswitch", TKeyboard::KEY_DISPLAYSWITCH},
    {"kbdillumtoggle", TKeyboard::KEY_KBDILLUMTOGGLE},
    {"kbdillumdown", TKeyboard::KEY_KBDILLUMDOWN},
    {"kbdillumup", TKeyboard::KEY_KBDILLUMUP},
    {"eject", TKeyboard::KEY_EJECT},
    {"sleep", TKeyboard::KEY_SLEEP},
};

TStringMap<TKeyboard::Key, TKeyboard::KEY_MAX_ENUM> TKeyboard::mKeys(TKeyboard::mKeyEntries, sizeof(TKeyboard::mKeyEntries));

TStringMap<TKeyboard::Scancode, TKeyboard::SCANCODE_MAX_ENUM>::Entry TKeyboard::mScancodeStrMapEntries[] =
{
    {"unknown", TKeyboard::SCANCODE_UNKNOWN},

    {"a", TKeyboard::SCANCODE_A},
    {"b", TKeyboard::SCANCODE_B},
    {"c", TKeyboard::SCANCODE_C},
    {"d", TKeyboard::SCANCODE_D},
    {"e", TKeyboard::SCANCODE_E},
    {"f", TKeyboard::SCANCODE_F},
    {"g", TKeyboard::SCANCODE_G},
    {"h", TKeyboard::SCANCODE_H},
    {"i", TKeyboard::SCANCODE_I},
    {"j", TKeyboard::SCANCODE_J},
    {"k", TKeyboard::SCANCODE_K},
    {"l", TKeyboard::SCANCODE_L},
    {"m", TKeyboard::SCANCODE_M},
    {"n", TKeyboard::SCANCODE_N},
    {"o", TKeyboard::SCANCODE_O},
    {"p", TKeyboard::SCANCODE_P},
    {"q", TKeyboard::SCANCODE_Q},
    {"r", TKeyboard::SCANCODE_R},
    {"s", TKeyboard::SCANCODE_S},
    {"t", TKeyboard::SCANCODE_T},
    {"u", TKeyboard::SCANCODE_U},
    {"v", TKeyboard::SCANCODE_V},
    {"w", TKeyboard::SCANCODE_W},
    {"x", TKeyboard::SCANCODE_X},
    {"y", TKeyboard::SCANCODE_Y},
    {"z", TKeyboard::SCANCODE_Z},

    {"1", TKeyboard::SCANCODE_1},
    {"2", TKeyboard::SCANCODE_2},
    {"3", TKeyboard::SCANCODE_3},
    {"4", TKeyboard::SCANCODE_4},
    {"5", TKeyboard::SCANCODE_5},
    {"6", TKeyboard::SCANCODE_6},
    {"7", TKeyboard::SCANCODE_7},
    {"8", TKeyboard::SCANCODE_8},
    {"9", TKeyboard::SCANCODE_9},
    {"0", TKeyboard::SCANCODE_0},

    {"return", TKeyboard::SCANCODE_RETURN},
    {"escape", TKeyboard::SCANCODE_ESCAPE},
    {"backspace", TKeyboard::SCANCODE_BACKSPACE},
    {"tab", TKeyboard::SCANCODE_TAB},
    {"space", TKeyboard::SCANCODE_SPACE},

    {"-", TKeyboard::SCANCODE_MINUS},
    {"=", TKeyboard::SCANCODE_EQUALS},
    {"[", TKeyboard::SCANCODE_LEFTBRACKET},
    {"]", TKeyboard::SCANCODE_RIGHTBRACKET},
    {"\\", TKeyboard::SCANCODE_BACKSLASH},
    {"nonus#", TKeyboard::SCANCODE_NONUSHASH},
    {";", TKeyboard::SCANCODE_SEMICOLON},
    {"'", TKeyboard::SCANCODE_APOSTROPHE},
    {"`", TKeyboard::SCANCODE_GRAVE},
    {",", TKeyboard::SCANCODE_COMMA},
    {".", TKeyboard::SCANCODE_PERIOD},
    {"/", TKeyboard::SCANCODE_SLASH},

    {"capslock", TKeyboard::SCANCODE_CAPSLOCK},

    {"f1", TKeyboard::SCANCODE_F1},
    {"f2", TKeyboard::SCANCODE_F2},
    {"f3", TKeyboard::SCANCODE_F3},
    {"f4", TKeyboard::SCANCODE_F4},
    {"f5", TKeyboard::SCANCODE_F5},
    {"f6", TKeyboard::SCANCODE_F6},
    {"f7", TKeyboard::SCANCODE_F7},
    {"f8", TKeyboard::SCANCODE_F8},
    {"f9", TKeyboard::SCANCODE_F9},
    {"f10", TKeyboard::SCANCODE_F10},
    {"f11", TKeyboard::SCANCODE_F11},
    {"f12", TKeyboard::SCANCODE_F12},

    {"printscreen", TKeyboard::SCANCODE_PRINTSCREEN},
    {"scrolllock", TKeyboard::SCANCODE_SCROLLLOCK},
    {"pause", TKeyboard::SCANCODE_PAUSE},
    {"insert", TKeyboard::SCANCODE_INSERT},
    {"home", TKeyboard::SCANCODE_HOME},
    {"pageup", TKeyboard::SCANCODE_PAGEUP},
    {"delete", TKeyboard::SCANCODE_DELETE},
    {"end", TKeyboard::SCANCODE_END},
    {"pagedown", TKeyboard::SCANCODE_PAGEDOWN},
    {"right", TKeyboard::SCANCODE_RIGHT},
    {"left", TKeyboard::SCANCODE_LEFT},
    {"down", TKeyboard::SCANCODE_DOWN},
    {"up", TKeyboard::SCANCODE_UP},

    {"numlock", TKeyboard::SCANCODE_NUMLOCKCLEAR},
    {"kp/", TKeyboard::SCANCODE_KP_DIVIDE},
    {"kp*", TKeyboard::SCANCODE_KP_MULTIPLY},
    {"kp-", TKeyboard::SCANCODE_KP_MINUS},
    {"kp+", TKeyboard::SCANCODE_KP_PLUS},
    {"kpenter", TKeyboard::SCANCODE_KP_ENTER},
    {"kp1", TKeyboard::SCANCODE_KP_1},
    {"kp2", TKeyboard::SCANCODE_KP_2},
    {"kp3", TKeyboard::SCANCODE_KP_3},
    {"kp4", TKeyboard::SCANCODE_KP_4},
    {"kp5", TKeyboard::SCANCODE_KP_5},
    {"kp6", TKeyboard::SCANCODE_KP_6},
    {"kp7", TKeyboard::SCANCODE_KP_7},
    {"kp8", TKeyboard::SCANCODE_KP_8},
    {"kp9", TKeyboard::SCANCODE_KP_9},
    {"kp0", TKeyboard::SCANCODE_KP_0},
    {"kp.", TKeyboard::SCANCODE_KP_PERIOD},

    {"nonusbackslash", TKeyboard::SCANCODE_NONUSBACKSLASH},
    {"application", TKeyboard::SCANCODE_APPLICATION},
    {"power", TKeyboard::SCANCODE_POWER},
    {"kp=", TKeyboard::SCANCODE_KP_EQUALS},
    {"f13", TKeyboard::SCANCODE_F13},
    {"f14", TKeyboard::SCANCODE_F14},
    {"f15", TKeyboard::SCANCODE_F15},
    {"f16", TKeyboard::SCANCODE_F16},
    {"f17", TKeyboard::SCANCODE_F17},
    {"f18", TKeyboard::SCANCODE_F18},
    {"f19", TKeyboard::SCANCODE_F19},
    {"f20", TKeyboard::SCANCODE_F20},
    {"f21", TKeyboard::SCANCODE_F21},
    {"f22", TKeyboard::SCANCODE_F22},
    {"f23", TKeyboard::SCANCODE_F23},
    {"f24", TKeyboard::SCANCODE_F24},
    {"execute", TKeyboard::SCANCODE_EXECUTE},
    {"help", TKeyboard::SCANCODE_HELP},
    {"menu", TKeyboard::SCANCODE_MENU},
    {"select", TKeyboard::SCANCODE_SELECT},
    {"stop", TKeyboard::SCANCODE_STOP},
    {"again", TKeyboard::SCANCODE_AGAIN},
    {"undo", TKeyboard::SCANCODE_UNDO},
    {"cut", TKeyboard::SCANCODE_CUT},
    {"copy", TKeyboard::SCANCODE_COPY},
    {"paste", TKeyboard::SCANCODE_PASTE},
    {"find", TKeyboard::SCANCODE_FIND},
    {"mute", TKeyboard::SCANCODE_MUTE},
    {"volumeup", TKeyboard::SCANCODE_VOLUMEUP},
    {"volumedown", TKeyboard::SCANCODE_VOLUMEDOWN},
    {"kp,", TKeyboard::SCANCODE_KP_COMMA},
    {"kp=400", TKeyboard::SCANCODE_KP_EQUALSAS400},

    {"international1", TKeyboard::SCANCODE_INTERNATIONAL1},
    {"international2", TKeyboard::SCANCODE_INTERNATIONAL2},
    {"international3", TKeyboard::SCANCODE_INTERNATIONAL3},
    {"international4", TKeyboard::SCANCODE_INTERNATIONAL4},
    {"international5", TKeyboard::SCANCODE_INTERNATIONAL5},
    {"international6", TKeyboard::SCANCODE_INTERNATIONAL6},
    {"international7", TKeyboard::SCANCODE_INTERNATIONAL7},
    {"international8", TKeyboard::SCANCODE_INTERNATIONAL8},
    {"international9", TKeyboard::SCANCODE_INTERNATIONAL9},
    {"lang1", TKeyboard::SCANCODE_LANG1},
    {"lang2", TKeyboard::SCANCODE_LANG2},
    {"lang3", TKeyboard::SCANCODE_LANG3},
    {"lang4", TKeyboard::SCANCODE_LANG4},
    {"lang5", TKeyboard::SCANCODE_LANG5},
    {"lang6", TKeyboard::SCANCODE_LANG6},
    {"lang7", TKeyboard::SCANCODE_LANG7},
    {"lang8", TKeyboard::SCANCODE_LANG8},
    {"lang9", TKeyboard::SCANCODE_LANG9},

    {"alterase", TKeyboard::SCANCODE_ALTERASE},
    {"sysreq", TKeyboard::SCANCODE_SYSREQ},
    {"cancel", TKeyboard::SCANCODE_CANCEL},
    {"clear", TKeyboard::SCANCODE_CLEAR},
    {"prior", TKeyboard::SCANCODE_PRIOR},
    {"return2", TKeyboard::SCANCODE_RETURN2},
    {"separator", TKeyboard::SCANCODE_SEPARATOR},
    {"out", TKeyboard::SCANCODE_OUT},
    {"oper", TKeyboard::SCANCODE_OPER},
    {"clearagain", TKeyboard::SCANCODE_CLEARAGAIN},
    {"crsel", TKeyboard::SCANCODE_CRSEL},
    {"exsel", TKeyboard::SCANCODE_EXSEL},

    {"kp00", TKeyboard::SCANCODE_KP_00},
    {"kp000", TKeyboard::SCANCODE_KP_000},
    {"thsousandsseparator", TKeyboard::SCANCODE_THOUSANDSSEPARATOR},
    {"decimalseparator", TKeyboard::SCANCODE_DECIMALSEPARATOR},
    {"currencyunit", TKeyboard::SCANCODE_CURRENCYUNIT},
    {"currencysubunit", TKeyboard::SCANCODE_CURRENCYSUBUNIT},
    {"kp(", TKeyboard::SCANCODE_KP_LEFTPAREN},
    {"kp)", TKeyboard::SCANCODE_KP_RIGHTPAREN},
    {"kp{", TKeyboard::SCANCODE_KP_LEFTBRACE},
    {"kp}", TKeyboard::SCANCODE_KP_RIGHTBRACE},
    {"kptab", TKeyboard::SCANCODE_KP_TAB},
    {"kpbackspace", TKeyboard::SCANCODE_KP_BACKSPACE},
    {"kpa", TKeyboard::SCANCODE_KP_A},
    {"kpb", TKeyboard::SCANCODE_KP_B},
    {"kpc", TKeyboard::SCANCODE_KP_C},
    {"kpd", TKeyboard::SCANCODE_KP_D},
    {"kpe", TKeyboard::SCANCODE_KP_E},
    {"kpf", TKeyboard::SCANCODE_KP_F},
    {"kpxor", TKeyboard::SCANCODE_KP_XOR},
    {"kpower", TKeyboard::SCANCODE_KP_POWER},
    {"kp%", TKeyboard::SCANCODE_KP_PERCENT},
    {"kp<", TKeyboard::SCANCODE_KP_LESS},
    {"kp>", TKeyboard::SCANCODE_KP_GREATER},
    {"kp&", TKeyboard::SCANCODE_KP_AMPERSAND},
    {"kp&&", TKeyboard::SCANCODE_KP_DBLAMPERSAND},
    {"kp|", TKeyboard::SCANCODE_KP_VERTICALBAR},
    {"kp||", TKeyboard::SCANCODE_KP_DBLVERTICALBAR},
    {"kp:", TKeyboard::SCANCODE_KP_COLON},
    {"kp#", TKeyboard::SCANCODE_KP_HASH},
    {"kp ", TKeyboard::SCANCODE_KP_SPACE},
    {"kp@", TKeyboard::SCANCODE_KP_AT},
    {"kp!", TKeyboard::SCANCODE_KP_EXCLAM},
    {"kpmemstore", TKeyboard::SCANCODE_KP_MEMSTORE},
    {"kpmemrecall", TKeyboard::SCANCODE_KP_MEMRECALL},
    {"kpmemclear", TKeyboard::SCANCODE_KP_MEMCLEAR},
    {"kpmem+", TKeyboard::SCANCODE_KP_MEMADD},
    {"kpmem-", TKeyboard::SCANCODE_KP_MEMSUBTRACT},
    {"kpmem*", TKeyboard::SCANCODE_KP_MEMMULTIPLY},
    {"kpmem/", TKeyboard::SCANCODE_KP_MEMDIVIDE},
    {"kp+-", TKeyboard::SCANCODE_KP_PLUSMINUS},
    {"kpclear", TKeyboard::SCANCODE_KP_CLEAR},
    {"kpclearentry", TKeyboard::SCANCODE_KP_CLEARENTRY},
    {"kpbinary", TKeyboard::SCANCODE_KP_BINARY},
    {"kpoctal", TKeyboard::SCANCODE_KP_OCTAL},
    {"kpdecimal", TKeyboard::SCANCODE_KP_DECIMAL},
    {"kphex", TKeyboard::SCANCODE_KP_HEXADECIMAL},

    {"lctrl", TKeyboard::SCANCODE_LCTRL},
    {"lshift", TKeyboard::SCANCODE_LSHIFT},
    {"lalt", TKeyboard::SCANCODE_LALT},
    {"lgui", TKeyboard::SCANCODE_LGUI},
    {"rctrl", TKeyboard::SCANCODE_RCTRL},
    {"rshift", TKeyboard::SCANCODE_RSHIFT},
    {"ralt", TKeyboard::SCANCODE_RALT},
    {"rgui", TKeyboard::SCANCODE_RGUI},

    {"mode", TKeyboard::SCANCODE_MODE},

    {"audionext", TKeyboard::SCANCODE_AUDIONEXT},
    {"audioprev", TKeyboard::SCANCODE_AUDIOPREV},
    {"audiostop", TKeyboard::SCANCODE_AUDIOSTOP},
    {"audioplay", TKeyboard::SCANCODE_AUDIOPLAY},
    {"audiomute", TKeyboard::SCANCODE_AUDIOMUTE},
    {"mediaselect", TKeyboard::SCANCODE_MEDIASELECT},
    {"www", TKeyboard::SCANCODE_WWW},
    {"mail", TKeyboard::SCANCODE_MAIL},
    {"calculator", TKeyboard::SCANCODE_CALCULATOR},
    {"computer", TKeyboard::SCANCODE_COMPUTER},
    {"acsearch", TKeyboard::SCANCODE_AC_SEARCH},
    {"achome", TKeyboard::SCANCODE_AC_HOME},
    {"acback", TKeyboard::SCANCODE_AC_BACK},
    {"acforward", TKeyboard::SCANCODE_AC_FORWARD},
    {"acstop", TKeyboard::SCANCODE_AC_STOP},
    {"acrefresh", TKeyboard::SCANCODE_AC_REFRESH},
    {"acbookmarks", TKeyboard::SCANCODE_AC_BOOKMARKS},

    {"brightnessdown", TKeyboard::SCANCODE_BRIGHTNESSDOWN},
    {"brightnessup", TKeyboard::SCANCODE_BRIGHTNESSUP},
    {"displayswitch", TKeyboard::SCANCODE_DISPLAYSWITCH},
    {"kbdillumtoggle", TKeyboard::SCANCODE_KBDILLUMTOGGLE},
    {"kbdillumdown", TKeyboard::SCANCODE_KBDILLUMDOWN},
    {"kbdillumup", TKeyboard::SCANCODE_KBDILLUMUP},
    {"eject", TKeyboard::SCANCODE_EJECT},
    {"sleep", TKeyboard::SCANCODE_SLEEP},

    {"app1", TKeyboard::SCANCODE_APP1},
    {"app2", TKeyboard::SCANCODE_APP2},
};

bool TKeyboard::getConstant(const char *in, Key &out)
{
    return mKeys.find(in, out);
}

bool TKeyboard::getConstant(Key in, const char *&out)
{
    return mKeys.find(in, out);
}

bool TKeyboard::getConstant(const char *in, Scancode &out)
{
    return mScancodesStrMap.find(in, out);
}

bool TKeyboard::getConstant(Scancode in, const char *&out)
{
    return mScancodesStrMap.find(in, out);
}

TStringMap<TKeyboard::Scancode, TKeyboard::SCANCODE_MAX_ENUM> TKeyboard::mScancodesStrMap(TKeyboard::mScancodeStrMapEntries, sizeof(TKeyboard::mScancodeStrMapEntries));

TKeyboard::TKeyboard()
    : mKeyRepeat(false)
{
}

const char *TKeyboard::getName() const
{
    return "theall.keyboard";
}

void TKeyboard::setKeyRepeat(bool enable)
{
    mKeyRepeat = enable;
}

bool TKeyboard::hasKeyRepeat() const
{
    return mKeyRepeat;
}

bool TKeyboard::isDown(const std::vector<Key> &keylist) const
{
	const Uint8 *state = SDL_GetKeyboardState(nullptr);

	for (Key key : keylist)
	{
        SDL_Scancode scancode = SDL_GetScancodeFromKey(mKeyMap[key]);

		if(state[scancode])
			return true;
	}

    return false;
}

bool TKeyboard::isDown(const Key &key) const
{
    const Uint8 *state = SDL_GetKeyboardState(nullptr);
    SDL_Scancode scancode = SDL_GetScancodeFromKey(mKeyMap[key]);
    return state[scancode];
}

bool TKeyboard::isScancodeDown(const std::vector<Scancode> &scancodelist) const
{
	const Uint8 *state = SDL_GetKeyboardState(nullptr);

	for (Scancode scancode : scancodelist)
	{
		SDL_Scancode sdlcode = SDL_SCANCODE_UNKNOWN;

        if(mScancodesEnumMap.find(scancode, sdlcode) && state[sdlcode])
			return true;
	}

	return false;
}

TKeyboard::Key TKeyboard::getKeyFromScancode(Scancode scancode) const
{
	SDL_Scancode sdlscancode = SDL_SCANCODE_UNKNOWN;
    mScancodesEnumMap.find(scancode, sdlscancode);

	SDL_Keycode sdlkey = SDL_GetKeyFromScancode(sdlscancode);

	for (int i = 0; i < KEY_MAX_ENUM; i++)
	{
        if(mKeyMap[i] == sdlkey)
			return (Key) i;
	}

	return KEY_UNKNOWN;
}

TKeyboard::Scancode TKeyboard::getScancodeFromKey(Key key) const
{
	Scancode scancode = SCANCODE_UNKNOWN;

	if(key != KEY_MAX_ENUM)
	{
        SDL_Keycode sdlkey = mKeyMap[key];

		SDL_Scancode sdlscancode = SDL_GetScancodeFromKey(sdlkey);
        mScancodesEnumMap.find(sdlscancode, scancode);
	}

	return scancode;
}

void TKeyboard::setTextInput(bool enable)
{
	if(enable)
		SDL_StartTextInput();
	else
		SDL_StopTextInput();
}

void TKeyboard::setTextInput(bool enable, double x, double y, double w, double h)
{
	// SDL_SetTextInputRect expects coordinates in window-space but setTextInput
	// takes pixels, so we should convert.

    TWindow *window = TWindow::instance();
    if(window)
    {
        window->pixelToWindowCoords(&x, &y);
        window->pixelToWindowCoords(&w, &h);
    }

	SDL_Rect rect = {(int) x, (int) y, (int) w, (int) h};
	SDL_SetTextInputRect(&rect);

	setTextInput(enable);
}

bool TKeyboard::hasTextInput() const
{
	return SDL_IsTextInputActive() != SDL_FALSE;
}

bool TKeyboard::hasScreenKeyboard() const
{
	return SDL_HasScreenKeyboardSupport() != SDL_FALSE;
}

bool TKeyboard::getConstant(Scancode in, SDL_Scancode &out)
{
    return mScancodesEnumMap.find(in, out);
}

bool TKeyboard::getConstant(SDL_Scancode in, Scancode &out)
{
    return mScancodesEnumMap.find(in, out);
}

const SDL_Keycode *TKeyboard::createKeyMap()
{
	// Array must be static so its lifetime continues once the function returns.
    static SDL_Keycode k[TKeyboard::KEY_MAX_ENUM] = {SDLK_UNKNOWN};

    k[TKeyboard::KEY_UNKNOWN] = SDLK_UNKNOWN;

    k[TKeyboard::KEY_RETURN] = SDLK_RETURN;
    k[TKeyboard::KEY_ESCAPE] = SDLK_ESCAPE;
    k[TKeyboard::KEY_BACKSPACE] = SDLK_BACKSPACE;
    k[TKeyboard::KEY_TAB] = SDLK_TAB;
    k[TKeyboard::KEY_SPACE] = SDLK_SPACE;
    k[TKeyboard::KEY_EXCLAIM] = SDLK_EXCLAIM;
    k[TKeyboard::KEY_QUOTEDBL] = SDLK_QUOTEDBL;
    k[TKeyboard::KEY_HASH] = SDLK_HASH;
    k[TKeyboard::KEY_PERCENT] = SDLK_PERCENT;
    k[TKeyboard::KEY_DOLLAR] = SDLK_DOLLAR;
    k[TKeyboard::KEY_AMPERSAND] = SDLK_AMPERSAND;
    k[TKeyboard::KEY_QUOTE] = SDLK_QUOTE;
    k[TKeyboard::KEY_LEFTPAREN] = SDLK_LEFTPAREN;
    k[TKeyboard::KEY_RIGHTPAREN] = SDLK_RIGHTPAREN;
    k[TKeyboard::KEY_ASTERISK] = SDLK_ASTERISK;
    k[TKeyboard::KEY_PLUS] = SDLK_PLUS;
    k[TKeyboard::KEY_COMMA] = SDLK_COMMA;
    k[TKeyboard::KEY_MINUS] = SDLK_MINUS;
    k[TKeyboard::KEY_PERIOD] = SDLK_PERIOD;
    k[TKeyboard::KEY_SLASH] = SDLK_SLASH;
    k[TKeyboard::KEY_0] = SDLK_0;
    k[TKeyboard::KEY_1] = SDLK_1;
    k[TKeyboard::KEY_2] = SDLK_2;
    k[TKeyboard::KEY_3] = SDLK_3;
    k[TKeyboard::KEY_4] = SDLK_4;
    k[TKeyboard::KEY_5] = SDLK_5;
    k[TKeyboard::KEY_6] = SDLK_6;
    k[TKeyboard::KEY_7] = SDLK_7;
    k[TKeyboard::KEY_8] = SDLK_8;
    k[TKeyboard::KEY_9] = SDLK_9;
    k[TKeyboard::KEY_COLON] = SDLK_COLON;
    k[TKeyboard::KEY_SEMICOLON] = SDLK_SEMICOLON;
    k[TKeyboard::KEY_LESS] = SDLK_LESS;
    k[TKeyboard::KEY_EQUALS] = SDLK_EQUALS;
    k[TKeyboard::KEY_GREATER] = SDLK_GREATER;
    k[TKeyboard::KEY_QUESTION] = SDLK_QUESTION;
    k[TKeyboard::KEY_AT] = SDLK_AT;

    k[TKeyboard::KEY_LEFTBRACKET] = SDLK_LEFTBRACKET;
    k[TKeyboard::KEY_BACKSLASH] = SDLK_BACKSLASH;
    k[TKeyboard::KEY_RIGHTBRACKET] = SDLK_RIGHTBRACKET;
    k[TKeyboard::KEY_CARET] = SDLK_CARET;
    k[TKeyboard::KEY_UNDERSCORE] = SDLK_UNDERSCORE;
    k[TKeyboard::KEY_BACKQUOTE] = SDLK_BACKQUOTE;
    k[TKeyboard::KEY_A] = SDLK_a;
    k[TKeyboard::KEY_B] = SDLK_b;
    k[TKeyboard::KEY_C] = SDLK_c;
    k[TKeyboard::KEY_D] = SDLK_d;
    k[TKeyboard::KEY_E] = SDLK_e;
    k[TKeyboard::KEY_F] = SDLK_f;
    k[TKeyboard::KEY_G] = SDLK_g;
    k[TKeyboard::KEY_H] = SDLK_h;
    k[TKeyboard::KEY_I] = SDLK_i;
    k[TKeyboard::KEY_J] = SDLK_j;
    k[TKeyboard::KEY_K] = SDLK_k;
    k[TKeyboard::KEY_L] = SDLK_l;
    k[TKeyboard::KEY_M] = SDLK_m;
    k[TKeyboard::KEY_N] = SDLK_n;
    k[TKeyboard::KEY_O] = SDLK_o;
    k[TKeyboard::KEY_P] = SDLK_p;
    k[TKeyboard::KEY_Q] = SDLK_q;
    k[TKeyboard::KEY_R] = SDLK_r;
    k[TKeyboard::KEY_S] = SDLK_s;
    k[TKeyboard::KEY_T] = SDLK_t;
    k[TKeyboard::KEY_U] = SDLK_u;
    k[TKeyboard::KEY_V] = SDLK_v;
    k[TKeyboard::KEY_W] = SDLK_w;
    k[TKeyboard::KEY_X] = SDLK_x;
    k[TKeyboard::KEY_Y] = SDLK_y;
    k[TKeyboard::KEY_Z] = SDLK_z;

    k[TKeyboard::KEY_CAPSLOCK] = SDLK_CAPSLOCK;

    k[TKeyboard::KEY_F1] = SDLK_F1;
    k[TKeyboard::KEY_F2] = SDLK_F2;
    k[TKeyboard::KEY_F3] = SDLK_F3;
    k[TKeyboard::KEY_F4] = SDLK_F4;
    k[TKeyboard::KEY_F5] = SDLK_F5;
    k[TKeyboard::KEY_F6] = SDLK_F6;
    k[TKeyboard::KEY_F7] = SDLK_F7;
    k[TKeyboard::KEY_F8] = SDLK_F8;
    k[TKeyboard::KEY_F9] = SDLK_F9;
    k[TKeyboard::KEY_F10] = SDLK_F10;
    k[TKeyboard::KEY_F11] = SDLK_F11;
    k[TKeyboard::KEY_F12] = SDLK_F12;

    k[TKeyboard::KEY_PRINTSCREEN] = SDLK_PRINTSCREEN;
    k[TKeyboard::KEY_SCROLLLOCK] = SDLK_SCROLLLOCK;
    k[TKeyboard::KEY_PAUSE] = SDLK_PAUSE;
    k[TKeyboard::KEY_INSERT] = SDLK_INSERT;
    k[TKeyboard::KEY_HOME] = SDLK_HOME;
    k[TKeyboard::KEY_PAGEUP] = SDLK_PAGEUP;
    k[TKeyboard::KEY_DELETE] = SDLK_DELETE;
    k[TKeyboard::KEY_END] = SDLK_END;
    k[TKeyboard::KEY_PAGEDOWN] = SDLK_PAGEDOWN;
    k[TKeyboard::KEY_RIGHT] = SDLK_RIGHT;
    k[TKeyboard::KEY_LEFT] = SDLK_LEFT;
    k[TKeyboard::KEY_DOWN] = SDLK_DOWN;
    k[TKeyboard::KEY_UP] = SDLK_UP;

    k[TKeyboard::KEY_NUMLOCKCLEAR] = SDLK_NUMLOCKCLEAR;
    k[TKeyboard::KEY_KP_DIVIDE] = SDLK_KP_DIVIDE;
    k[TKeyboard::KEY_KP_MULTIPLY] = SDLK_KP_MULTIPLY;
    k[TKeyboard::KEY_KP_MINUS] = SDLK_KP_MINUS;
    k[TKeyboard::KEY_KP_PLUS] = SDLK_KP_PLUS;
    k[TKeyboard::KEY_KP_ENTER] = SDLK_KP_ENTER;
    k[TKeyboard::KEY_KP_0] = SDLK_KP_0;
    k[TKeyboard::KEY_KP_1] = SDLK_KP_1;
    k[TKeyboard::KEY_KP_2] = SDLK_KP_2;
    k[TKeyboard::KEY_KP_3] = SDLK_KP_3;
    k[TKeyboard::KEY_KP_4] = SDLK_KP_4;
    k[TKeyboard::KEY_KP_5] = SDLK_KP_5;
    k[TKeyboard::KEY_KP_6] = SDLK_KP_6;
    k[TKeyboard::KEY_KP_7] = SDLK_KP_7;
    k[TKeyboard::KEY_KP_8] = SDLK_KP_8;
    k[TKeyboard::KEY_KP_9] = SDLK_KP_9;
    k[TKeyboard::KEY_KP_PERIOD] = SDLK_KP_PERIOD;
    k[TKeyboard::KEY_KP_COMMA] = SDLK_KP_COMMA;
    k[TKeyboard::KEY_KP_EQUALS] = SDLK_KP_EQUALS;

    k[TKeyboard::KEY_APPLICATION] = SDLK_APPLICATION;
    k[TKeyboard::KEY_POWER] = SDLK_POWER;
    k[TKeyboard::KEY_F13] = SDLK_F13;
    k[TKeyboard::KEY_F14] = SDLK_F14;
    k[TKeyboard::KEY_F15] = SDLK_F15;
    k[TKeyboard::KEY_F16] = SDLK_F16;
    k[TKeyboard::KEY_F17] = SDLK_F17;
    k[TKeyboard::KEY_F18] = SDLK_F18;
    k[TKeyboard::KEY_F19] = SDLK_F19;
    k[TKeyboard::KEY_F20] = SDLK_F20;
    k[TKeyboard::KEY_F21] = SDLK_F21;
    k[TKeyboard::KEY_F22] = SDLK_F22;
    k[TKeyboard::KEY_F23] = SDLK_F23;
    k[TKeyboard::KEY_F24] = SDLK_F24;
    k[TKeyboard::KEY_EXECUTE_] = SDLK_EXECUTE;
    k[TKeyboard::KEY_HELP] = SDLK_HELP;
    k[TKeyboard::KEY_MENU] = SDLK_MENU;
    k[TKeyboard::KEY_SELECT] = SDLK_SELECT;
    k[TKeyboard::KEY_STOP] = SDLK_STOP;
    k[TKeyboard::KEY_AGAIN] = SDLK_AGAIN;
    k[TKeyboard::KEY_UNDO] = SDLK_UNDO;
    k[TKeyboard::KEY_CUT] = SDLK_CUT;
    k[TKeyboard::KEY_COPY] = SDLK_COPY;
    k[TKeyboard::KEY_PASTE] = SDLK_PASTE;
    k[TKeyboard::KEY_FIND] = SDLK_FIND;
    k[TKeyboard::KEY_MUTE] = SDLK_MUTE;
    k[TKeyboard::KEY_VOLUMEUP] = SDLK_VOLUMEUP;
    k[TKeyboard::KEY_VOLUMEDOWN] = SDLK_VOLUMEDOWN;

    k[TKeyboard::KEY_ALTERASE] = SDLK_ALTERASE;
    k[TKeyboard::KEY_SYSREQ] = SDLK_SYSREQ;
    k[TKeyboard::KEY_CANCEL] = SDLK_CANCEL;
    k[TKeyboard::KEY_CLEAR] = SDLK_CLEAR;
    k[TKeyboard::KEY_PRIOR] = SDLK_PRIOR;
    k[TKeyboard::KEY_RETURN2] = SDLK_RETURN2;
    k[TKeyboard::KEY_SEPARATOR] = SDLK_SEPARATOR;
    k[TKeyboard::KEY_OUT] = SDLK_OUT;
    k[TKeyboard::KEY_OPER] = SDLK_OPER;
    k[TKeyboard::KEY_CLEARAGAIN] = SDLK_CLEARAGAIN;

    k[TKeyboard::KEY_THOUSANDSSEPARATOR] = SDLK_THOUSANDSSEPARATOR;
    k[TKeyboard::KEY_DECIMALSEPARATOR] = SDLK_DECIMALSEPARATOR;
    k[TKeyboard::KEY_CURRENCYUNIT] = SDLK_CURRENCYUNIT;
    k[TKeyboard::KEY_CURRENCYSUBUNIT] = SDLK_CURRENCYSUBUNIT;

    k[TKeyboard::KEY_LCTRL] = SDLK_LCTRL;
    k[TKeyboard::KEY_LSHIFT] = SDLK_LSHIFT;
    k[TKeyboard::KEY_LALT] = SDLK_LALT;
    k[TKeyboard::KEY_LGUI] = SDLK_LGUI;
    k[TKeyboard::KEY_RCTRL] = SDLK_RCTRL;
    k[TKeyboard::KEY_RSHIFT] = SDLK_RSHIFT;
    k[TKeyboard::KEY_RALT] = SDLK_RALT;
    k[TKeyboard::KEY_RGUI] = SDLK_RGUI;

    k[TKeyboard::KEY_MODE] = SDLK_MODE;

    k[TKeyboard::KEY_AUDIONEXT] = SDLK_AUDIONEXT;
    k[TKeyboard::KEY_AUDIOPREV] = SDLK_AUDIOPREV;
    k[TKeyboard::KEY_AUDIOSTOP] = SDLK_AUDIOSTOP;
    k[TKeyboard::KEY_AUDIOPLAY] = SDLK_AUDIOPLAY;
    k[TKeyboard::KEY_AUDIOMUTE] = SDLK_AUDIOMUTE;
    k[TKeyboard::KEY_MEDIASELECT] = SDLK_MEDIASELECT;
    k[TKeyboard::KEY_WWW] = SDLK_WWW;
    k[TKeyboard::KEY_MAIL] = SDLK_MAIL;
    k[TKeyboard::KEY_CALCULATOR] = SDLK_CALCULATOR;
    k[TKeyboard::KEY_COMPUTER] = SDLK_COMPUTER;
    k[TKeyboard::KEY_APP_SEARCH] = SDLK_AC_SEARCH;
    k[TKeyboard::KEY_APP_HOME] = SDLK_AC_HOME;
    k[TKeyboard::KEY_APP_BACK] = SDLK_AC_BACK;
    k[TKeyboard::KEY_APP_FORWARD] = SDLK_AC_FORWARD;
    k[TKeyboard::KEY_APP_STOP] = SDLK_AC_STOP;
    k[TKeyboard::KEY_APP_REFRESH] = SDLK_AC_REFRESH;
    k[TKeyboard::KEY_APP_BOOKMARKS] = SDLK_AC_BOOKMARKS;

    k[TKeyboard::KEY_BRIGHTNESSDOWN] = SDLK_BRIGHTNESSDOWN;
    k[TKeyboard::KEY_BRIGHTNESSUP] = SDLK_BRIGHTNESSUP;
    k[TKeyboard::KEY_DISPLAYSWITCH] = SDLK_DISPLAYSWITCH;
    k[TKeyboard::KEY_KBDILLUMTOGGLE] = SDLK_KBDILLUMTOGGLE;
    k[TKeyboard::KEY_KBDILLUMDOWN] = SDLK_KBDILLUMDOWN;
    k[TKeyboard::KEY_KBDILLUMUP] = SDLK_KBDILLUMUP;
    k[TKeyboard::KEY_EJECT] = SDLK_EJECT;
    k[TKeyboard::KEY_SLEEP] = SDLK_SLEEP;

	return k;
}

const SDL_Keycode *TKeyboard::mKeyMap = TKeyboard::createKeyMap();

TEnumMap<TKeyboard::Scancode, SDL_Scancode, SDL_NUM_SCANCODES>::Entry TKeyboard::mScancodeEnumMapEntries[] =
{
	{SCANCODE_UNKNOWN, SDL_SCANCODE_UNKNOWN},

	{SCANCODE_A, SDL_SCANCODE_A},
	{SCANCODE_B, SDL_SCANCODE_B},
	{SCANCODE_C, SDL_SCANCODE_C},
	{SCANCODE_D, SDL_SCANCODE_D},
	{SCANCODE_E, SDL_SCANCODE_E},
	{SCANCODE_F, SDL_SCANCODE_F},
	{SCANCODE_G, SDL_SCANCODE_G},
	{SCANCODE_H, SDL_SCANCODE_H},
	{SCANCODE_I, SDL_SCANCODE_I},
	{SCANCODE_J, SDL_SCANCODE_J},
	{SCANCODE_K, SDL_SCANCODE_K},
	{SCANCODE_L, SDL_SCANCODE_L},
	{SCANCODE_M, SDL_SCANCODE_M},
	{SCANCODE_N, SDL_SCANCODE_N},
	{SCANCODE_O, SDL_SCANCODE_O},
	{SCANCODE_P, SDL_SCANCODE_P},
	{SCANCODE_Q, SDL_SCANCODE_Q},
	{SCANCODE_R, SDL_SCANCODE_R},
	{SCANCODE_S, SDL_SCANCODE_S},
	{SCANCODE_T, SDL_SCANCODE_T},
	{SCANCODE_U, SDL_SCANCODE_U},
	{SCANCODE_V, SDL_SCANCODE_V},
	{SCANCODE_W, SDL_SCANCODE_W},
	{SCANCODE_X, SDL_SCANCODE_X},
	{SCANCODE_Y, SDL_SCANCODE_Y},
	{SCANCODE_Z, SDL_SCANCODE_Z},

	{SCANCODE_1, SDL_SCANCODE_1},
	{SCANCODE_2, SDL_SCANCODE_2},
	{SCANCODE_3, SDL_SCANCODE_3},
	{SCANCODE_4, SDL_SCANCODE_4},
	{SCANCODE_5, SDL_SCANCODE_5},
	{SCANCODE_6, SDL_SCANCODE_6},
	{SCANCODE_7, SDL_SCANCODE_7},
	{SCANCODE_8, SDL_SCANCODE_8},
	{SCANCODE_9, SDL_SCANCODE_9},
	{SCANCODE_0, SDL_SCANCODE_0},

	{SCANCODE_RETURN, SDL_SCANCODE_RETURN},
	{SCANCODE_ESCAPE, SDL_SCANCODE_ESCAPE},
	{SCANCODE_BACKSPACE, SDL_SCANCODE_BACKSPACE},
	{SCANCODE_TAB, SDL_SCANCODE_TAB},
	{SCANCODE_SPACE, SDL_SCANCODE_SPACE},

	{SCANCODE_MINUS, SDL_SCANCODE_MINUS},
	{SCANCODE_EQUALS, SDL_SCANCODE_EQUALS},
	{SCANCODE_LEFTBRACKET, SDL_SCANCODE_LEFTBRACKET},
	{SCANCODE_RIGHTBRACKET, SDL_SCANCODE_RIGHTBRACKET},
	{SCANCODE_BACKSLASH, SDL_SCANCODE_BACKSLASH},
	{SCANCODE_NONUSHASH, SDL_SCANCODE_NONUSHASH},
	{SCANCODE_SEMICOLON, SDL_SCANCODE_SEMICOLON},
	{SCANCODE_APOSTROPHE, SDL_SCANCODE_APOSTROPHE},
	{SCANCODE_GRAVE, SDL_SCANCODE_GRAVE},
	{SCANCODE_COMMA, SDL_SCANCODE_COMMA},
	{SCANCODE_PERIOD, SDL_SCANCODE_PERIOD},
	{SCANCODE_SLASH, SDL_SCANCODE_SLASH},

	{SCANCODE_CAPSLOCK, SDL_SCANCODE_CAPSLOCK},

	{SCANCODE_F1, SDL_SCANCODE_F1},
	{SCANCODE_F2, SDL_SCANCODE_F2},
	{SCANCODE_F3, SDL_SCANCODE_F3},
	{SCANCODE_F4, SDL_SCANCODE_F4},
	{SCANCODE_F5, SDL_SCANCODE_F5},
	{SCANCODE_F6, SDL_SCANCODE_F6},
	{SCANCODE_F7, SDL_SCANCODE_F7},
	{SCANCODE_F8, SDL_SCANCODE_F8},
	{SCANCODE_F9, SDL_SCANCODE_F9},
	{SCANCODE_F10, SDL_SCANCODE_F10},
	{SCANCODE_F11, SDL_SCANCODE_F11},
	{SCANCODE_F12, SDL_SCANCODE_F12},

	{SCANCODE_PRINTSCREEN, SDL_SCANCODE_PRINTSCREEN},
	{SCANCODE_SCROLLLOCK, SDL_SCANCODE_SCROLLLOCK},
	{SCANCODE_PAUSE, SDL_SCANCODE_PAUSE},
	{SCANCODE_INSERT, SDL_SCANCODE_INSERT},
	{SCANCODE_HOME, SDL_SCANCODE_HOME},
	{SCANCODE_PAGEUP, SDL_SCANCODE_PAGEUP},
	{SCANCODE_DELETE, SDL_SCANCODE_DELETE},
	{SCANCODE_END, SDL_SCANCODE_END},
	{SCANCODE_PAGEDOWN, SDL_SCANCODE_PAGEDOWN},
	{SCANCODE_RIGHT, SDL_SCANCODE_RIGHT},
	{SCANCODE_LEFT, SDL_SCANCODE_LEFT},
	{SCANCODE_DOWN, SDL_SCANCODE_DOWN},
	{SCANCODE_UP, SDL_SCANCODE_UP},

	{SCANCODE_NUMLOCKCLEAR, SDL_SCANCODE_NUMLOCKCLEAR},
	{SCANCODE_KP_DIVIDE, SDL_SCANCODE_KP_DIVIDE},
	{SCANCODE_KP_MULTIPLY, SDL_SCANCODE_KP_MULTIPLY},
	{SCANCODE_KP_MINUS, SDL_SCANCODE_KP_MINUS},
	{SCANCODE_KP_PLUS, SDL_SCANCODE_KP_PLUS},
	{SCANCODE_KP_ENTER, SDL_SCANCODE_KP_ENTER},
	{SCANCODE_KP_1, SDL_SCANCODE_KP_1},
	{SCANCODE_KP_2, SDL_SCANCODE_KP_2},
	{SCANCODE_KP_3, SDL_SCANCODE_KP_3},
	{SCANCODE_KP_4, SDL_SCANCODE_KP_4},
	{SCANCODE_KP_5, SDL_SCANCODE_KP_5},
	{SCANCODE_KP_6, SDL_SCANCODE_KP_6},
	{SCANCODE_KP_7, SDL_SCANCODE_KP_7},
	{SCANCODE_KP_8, SDL_SCANCODE_KP_8},
	{SCANCODE_KP_9, SDL_SCANCODE_KP_9},
	{SCANCODE_KP_0, SDL_SCANCODE_KP_0},
	{SCANCODE_KP_PERIOD, SDL_SCANCODE_KP_PERIOD},

	{SCANCODE_NONUSBACKSLASH, SDL_SCANCODE_NONUSBACKSLASH},
	{SCANCODE_APPLICATION, SDL_SCANCODE_APPLICATION},
	{SCANCODE_POWER, SDL_SCANCODE_POWER},
	{SCANCODE_KP_EQUALS, SDL_SCANCODE_KP_EQUALS},
	{SCANCODE_F13, SDL_SCANCODE_F13},
	{SCANCODE_F14, SDL_SCANCODE_F14},
	{SCANCODE_F15, SDL_SCANCODE_F15},
	{SCANCODE_F16, SDL_SCANCODE_F16},
	{SCANCODE_F17, SDL_SCANCODE_F17},
	{SCANCODE_F18, SDL_SCANCODE_F18},
	{SCANCODE_F19, SDL_SCANCODE_F19},
	{SCANCODE_F20, SDL_SCANCODE_F20},
	{SCANCODE_F21, SDL_SCANCODE_F21},
	{SCANCODE_F22, SDL_SCANCODE_F22},
	{SCANCODE_F23, SDL_SCANCODE_F23},
	{SCANCODE_F24, SDL_SCANCODE_F24},
	{SCANCODE_EXECUTE, SDL_SCANCODE_EXECUTE},
	{SCANCODE_HELP, SDL_SCANCODE_HELP},
	{SCANCODE_MENU, SDL_SCANCODE_MENU},
	{SCANCODE_SELECT, SDL_SCANCODE_SELECT},
	{SCANCODE_STOP, SDL_SCANCODE_STOP},
	{SCANCODE_AGAIN, SDL_SCANCODE_AGAIN},
	{SCANCODE_UNDO, SDL_SCANCODE_UNDO},
	{SCANCODE_CUT, SDL_SCANCODE_CUT},
	{SCANCODE_COPY, SDL_SCANCODE_COPY},
	{SCANCODE_PASTE, SDL_SCANCODE_PASTE},
	{SCANCODE_FIND, SDL_SCANCODE_FIND},
	{SCANCODE_MUTE, SDL_SCANCODE_MUTE},
	{SCANCODE_VOLUMEUP, SDL_SCANCODE_VOLUMEUP},
	{SCANCODE_VOLUMEDOWN, SDL_SCANCODE_VOLUMEDOWN},
	{SCANCODE_KP_COMMA, SDL_SCANCODE_KP_COMMA},
	{SCANCODE_KP_EQUALSAS400, SDL_SCANCODE_KP_EQUALSAS400},

	{SCANCODE_INTERNATIONAL1, SDL_SCANCODE_INTERNATIONAL1},
	{SCANCODE_INTERNATIONAL2, SDL_SCANCODE_INTERNATIONAL2},
	{SCANCODE_INTERNATIONAL3, SDL_SCANCODE_INTERNATIONAL3},
	{SCANCODE_INTERNATIONAL4, SDL_SCANCODE_INTERNATIONAL4},
	{SCANCODE_INTERNATIONAL5, SDL_SCANCODE_INTERNATIONAL5},
	{SCANCODE_INTERNATIONAL6, SDL_SCANCODE_INTERNATIONAL6},
	{SCANCODE_INTERNATIONAL7, SDL_SCANCODE_INTERNATIONAL7},
	{SCANCODE_INTERNATIONAL8, SDL_SCANCODE_INTERNATIONAL8},
	{SCANCODE_INTERNATIONAL9, SDL_SCANCODE_INTERNATIONAL9},
	{SCANCODE_LANG1, SDL_SCANCODE_LANG1},
	{SCANCODE_LANG2, SDL_SCANCODE_LANG2},
	{SCANCODE_LANG3, SDL_SCANCODE_LANG3},
	{SCANCODE_LANG4, SDL_SCANCODE_LANG4},
	{SCANCODE_LANG5, SDL_SCANCODE_LANG5},
	{SCANCODE_LANG6, SDL_SCANCODE_LANG6},
	{SCANCODE_LANG7, SDL_SCANCODE_LANG7},
	{SCANCODE_LANG8, SDL_SCANCODE_LANG8},
	{SCANCODE_LANG9, SDL_SCANCODE_LANG9},

	{SCANCODE_ALTERASE, SDL_SCANCODE_ALTERASE},
	{SCANCODE_SYSREQ, SDL_SCANCODE_SYSREQ},
	{SCANCODE_CANCEL, SDL_SCANCODE_CANCEL},
	{SCANCODE_CLEAR, SDL_SCANCODE_CLEAR},
	{SCANCODE_PRIOR, SDL_SCANCODE_PRIOR},
	{SCANCODE_RETURN2, SDL_SCANCODE_RETURN2},
	{SCANCODE_SEPARATOR, SDL_SCANCODE_SEPARATOR},
	{SCANCODE_OUT, SDL_SCANCODE_OUT},
	{SCANCODE_OPER, SDL_SCANCODE_OPER},
	{SCANCODE_CLEARAGAIN, SDL_SCANCODE_CLEARAGAIN},
	{SCANCODE_CRSEL, SDL_SCANCODE_CRSEL},
	{SCANCODE_EXSEL, SDL_SCANCODE_EXSEL},

	{SCANCODE_KP_00, SDL_SCANCODE_KP_00},
	{SCANCODE_KP_000, SDL_SCANCODE_KP_000},
	{SCANCODE_THOUSANDSSEPARATOR, SDL_SCANCODE_THOUSANDSSEPARATOR},
	{SCANCODE_DECIMALSEPARATOR, SDL_SCANCODE_DECIMALSEPARATOR},
	{SCANCODE_CURRENCYUNIT, SDL_SCANCODE_CURRENCYUNIT},
	{SCANCODE_CURRENCYSUBUNIT, SDL_SCANCODE_CURRENCYSUBUNIT},
	{SCANCODE_KP_LEFTPAREN, SDL_SCANCODE_KP_LEFTPAREN},
	{SCANCODE_KP_RIGHTPAREN, SDL_SCANCODE_KP_RIGHTPAREN},
	{SCANCODE_KP_LEFTBRACE, SDL_SCANCODE_KP_LEFTBRACE},
	{SCANCODE_KP_RIGHTBRACE, SDL_SCANCODE_KP_RIGHTBRACE},
	{SCANCODE_KP_TAB, SDL_SCANCODE_KP_TAB},
	{SCANCODE_KP_BACKSPACE, SDL_SCANCODE_KP_BACKSPACE},
	{SCANCODE_KP_A, SDL_SCANCODE_KP_A},
	{SCANCODE_KP_B, SDL_SCANCODE_KP_B},
	{SCANCODE_KP_C, SDL_SCANCODE_KP_C},
	{SCANCODE_KP_D, SDL_SCANCODE_KP_D},
	{SCANCODE_KP_E, SDL_SCANCODE_KP_E},
	{SCANCODE_KP_F, SDL_SCANCODE_KP_F},
	{SCANCODE_KP_XOR, SDL_SCANCODE_KP_XOR},
	{SCANCODE_KP_POWER, SDL_SCANCODE_KP_POWER},
	{SCANCODE_KP_PERCENT, SDL_SCANCODE_KP_PERCENT},
	{SCANCODE_KP_LESS, SDL_SCANCODE_KP_LESS},
	{SCANCODE_KP_GREATER, SDL_SCANCODE_KP_GREATER},
	{SCANCODE_KP_AMPERSAND, SDL_SCANCODE_KP_AMPERSAND},
	{SCANCODE_KP_DBLAMPERSAND, SDL_SCANCODE_KP_DBLAMPERSAND},
	{SCANCODE_KP_VERTICALBAR, SDL_SCANCODE_KP_VERTICALBAR},
	{SCANCODE_KP_DBLVERTICALBAR, SDL_SCANCODE_KP_DBLVERTICALBAR},
	{SCANCODE_KP_COLON, SDL_SCANCODE_KP_COLON},
	{SCANCODE_KP_HASH, SDL_SCANCODE_KP_HASH},
	{SCANCODE_KP_SPACE, SDL_SCANCODE_KP_SPACE},
	{SCANCODE_KP_AT, SDL_SCANCODE_KP_AT},
	{SCANCODE_KP_EXCLAM, SDL_SCANCODE_KP_EXCLAM},
	{SCANCODE_KP_MEMSTORE, SDL_SCANCODE_KP_MEMSTORE},
	{SCANCODE_KP_MEMRECALL, SDL_SCANCODE_KP_MEMRECALL},
	{SCANCODE_KP_MEMCLEAR, SDL_SCANCODE_KP_MEMCLEAR},
	{SCANCODE_KP_MEMADD, SDL_SCANCODE_KP_MEMADD},
	{SCANCODE_KP_MEMSUBTRACT, SDL_SCANCODE_KP_MEMSUBTRACT},
	{SCANCODE_KP_MEMMULTIPLY, SDL_SCANCODE_KP_MEMMULTIPLY},
	{SCANCODE_KP_MEMDIVIDE, SDL_SCANCODE_KP_MEMDIVIDE},
	{SCANCODE_KP_PLUSMINUS, SDL_SCANCODE_KP_PLUSMINUS},
	{SCANCODE_KP_CLEAR, SDL_SCANCODE_KP_CLEAR},
	{SCANCODE_KP_CLEARENTRY, SDL_SCANCODE_KP_CLEARENTRY},
	{SCANCODE_KP_BINARY, SDL_SCANCODE_KP_BINARY},
	{SCANCODE_KP_OCTAL, SDL_SCANCODE_KP_OCTAL},
	{SCANCODE_KP_DECIMAL, SDL_SCANCODE_KP_DECIMAL},
	{SCANCODE_KP_HEXADECIMAL, SDL_SCANCODE_KP_HEXADECIMAL},

	{SCANCODE_LCTRL, SDL_SCANCODE_LCTRL},
	{SCANCODE_LSHIFT, SDL_SCANCODE_LSHIFT},
	{SCANCODE_LALT, SDL_SCANCODE_LALT},
	{SCANCODE_LGUI, SDL_SCANCODE_LGUI},
	{SCANCODE_RCTRL, SDL_SCANCODE_RCTRL},
	{SCANCODE_RSHIFT, SDL_SCANCODE_RSHIFT},
	{SCANCODE_RALT, SDL_SCANCODE_RALT},
	{SCANCODE_RGUI, SDL_SCANCODE_RGUI},

	{SCANCODE_MODE, SDL_SCANCODE_MODE},

	{SCANCODE_AUDIONEXT, SDL_SCANCODE_AUDIONEXT},
	{SCANCODE_AUDIOPREV, SDL_SCANCODE_AUDIOPREV},
	{SCANCODE_AUDIOSTOP, SDL_SCANCODE_AUDIOSTOP},
	{SCANCODE_AUDIOPLAY, SDL_SCANCODE_AUDIOPLAY},
	{SCANCODE_AUDIOMUTE, SDL_SCANCODE_AUDIOMUTE},
	{SCANCODE_MEDIASELECT, SDL_SCANCODE_MEDIASELECT},
	{SCANCODE_WWW, SDL_SCANCODE_WWW},
	{SCANCODE_MAIL, SDL_SCANCODE_MAIL},
	{SCANCODE_CALCULATOR, SDL_SCANCODE_CALCULATOR},
	{SCANCODE_COMPUTER, SDL_SCANCODE_COMPUTER},
	{SCANCODE_AC_SEARCH, SDL_SCANCODE_AC_SEARCH},
	{SCANCODE_AC_HOME, SDL_SCANCODE_AC_HOME},
	{SCANCODE_AC_BACK, SDL_SCANCODE_AC_BACK},
	{SCANCODE_AC_FORWARD, SDL_SCANCODE_AC_FORWARD},
	{SCANCODE_AC_STOP, SDL_SCANCODE_AC_STOP},
	{SCANCODE_AC_REFRESH, SDL_SCANCODE_AC_REFRESH},
	{SCANCODE_AC_BOOKMARKS, SDL_SCANCODE_AC_BOOKMARKS},

	{SCANCODE_BRIGHTNESSDOWN, SDL_SCANCODE_BRIGHTNESSDOWN},
	{SCANCODE_BRIGHTNESSUP, SDL_SCANCODE_BRIGHTNESSUP},
	{SCANCODE_DISPLAYSWITCH, SDL_SCANCODE_DISPLAYSWITCH},
	{SCANCODE_KBDILLUMTOGGLE, SDL_SCANCODE_KBDILLUMTOGGLE},
	{SCANCODE_KBDILLUMDOWN, SDL_SCANCODE_KBDILLUMDOWN},
	{SCANCODE_KBDILLUMUP, SDL_SCANCODE_KBDILLUMUP},
	{SCANCODE_EJECT, SDL_SCANCODE_EJECT},
	{SCANCODE_SLEEP, SDL_SCANCODE_SLEEP},

	{SCANCODE_APP1, SDL_SCANCODE_APP1},
	{SCANCODE_APP2, SDL_SCANCODE_APP2},
};

TEnumMap<TKeyboard::Scancode, SDL_Scancode, SDL_NUM_SCANCODES> TKeyboard::mScancodesEnumMap(TKeyboard::mScancodeEnumMapEntries, sizeof(TKeyboard::mScancodeEnumMapEntries));
