#ifndef TMESSAGE_H
#define TMESSAGE_H

#include "common/object.h"
#include "common/variant.h"

#include <string>

class TMessage : public TObject
{
public:
    int value;
    std::vector<TVariant> arguments;

    TMessage(const int &value, const std::vector<TVariant> &vargs);
    ~TMessage();

    int getValue();
    std::vector<TVariant> getArgs();
}; // Message

#endif // TMESSAGE_H
