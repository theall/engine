/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef EVENT_H
#define EVENT_H

#include "../keyboard/keyboard.h"

#include <map>
#include <queue>

#include "utils/pch.h"

#include "common/object.h"

#include "modules/thread/thread.h"

#include "message.h"

enum EventType
{
    EVENT_NONE = 0,
    EVENT_KEYPRESSED,
    EVENT_KEYRELEASED,
    EVENT_TEXTINPUT,
    EVENT_TEXTEDITED,
    EVENT_MOUSEMOVED,
    EVENT_WHEELMOVED,
    EVENT_DIRECTORYDROPPED,
    EVENT_FILEDROPPED,
    EVENT_QUIT,
    EVENT_LOWMEMORY,
    EVENT_JOYSTICKAXIS,
    EVENT_JOYSTICKHAT,
    EVENT_GAMEPADAXIS,
    EVENT_JOYSTICKADDED,
    EVENT_JOYSTICKREMOVED,
    EVENT_FOCUS,
    EVENT_MOUSEFOCUS,
    EVENT_VISIBLE,
    EVENT_RESIZE,
    EVENT_MOUSEPRESSED,
    EVENT_MOUSERELEASED,
    EVENT_TOUCHPRESSED,
    EVENT_TOUCHRELEASED,
    EVENT_TOUCHMOVED,
    EVENT_JOYSTICKPRESSED,
    EVENT_JOYSTICKRELEASED,
    EVENT_GAMEPADPRESSED,
    EVENT_GAMEPADRELEASED
};

class TEvent : public TObject
{
    DECLARE_INSTANCE(TEvent)

public:
    TEvent();
    ~TEvent();

    void push(TMessage *msg);
    bool poll(TMessage *&msg);

	/**
	 * Pumps the event queue. This function gathers all the pending input information
	 * from devices and places it on the event queue. Normally not needed if you poll
	 * for events.
	 **/
	void pump();

	/**
	 * Waits for the next event (indefinitely). Useful for creating games where
	 * the screen and game state only needs updating when the user interacts with
	 * the window.
	 **/
    TMessage *wait();

	/**
	 * Clears the event queue.
	 */
	void clear();

private:
    TMutexRef mMutex;
    std::queue<TMessage *> mQueue;

    TMessage *convert(const SDL_Event &e) const;
    TMessage *convertJoystickEvent(const SDL_Event &e) const;
    TMessage *convertWindowEvent(const SDL_Event &e) const;

    static std::map<SDL_Keycode, TKeyboard::Key> createKeyMap();
    static std::map<SDL_Keycode, TKeyboard::Key> mKeys;

}; // Event

#endif // EVENT_H
