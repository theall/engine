#include "message.h"


TMessage::TMessage(const int &value, const std::vector<TVariant> &vargs)
    : value(value)
    , arguments(vargs)
{
}

TMessage::~TMessage()
{
}

int TMessage::getValue()
{
    return value;
}

std::vector<TVariant> TMessage::getArgs()
{
    return arguments;
}
