/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "event.h"

#include "common/config.h"
#include "common/exception.h"

#include "../filesystem/droppedfile.h"
#include "../filesystem/filesystem.h"
#include "../keyboard/Keyboard.h"
#include "../joystick/joystickmanager.h"
#include "../graphics/graphics.h"
#include "../window/window.h"
#include "../audio/audio.h"
#include "../touch/touch.h"

// SDL reports mouse coordinates in the window coordinate system in OS X, but
// we want them in pixel coordinates (may be different with high-DPI enabled.)
static void windowToPixelCoords(double *x, double *y)
{
    TWindow::instance()->windowToPixelCoords(x, y);
}

#ifndef THEALL_MACOSX
static void normalizedToPixelCoords(double *x, double *y)
{
    int w = 1, h = 1;

    TWindow::instance()->getPixelDimensions(w, h);

    if(x)
        *x = ((*x) * (double) w);
    if(y)
        *y = ((*y) * (double) h);
}
#endif

// SDL's event watch callbacks trigger when the event is actually posted inside
// SDL, unlike with SDL_PollEvents. This is useful for some events which require
// handling inside the function which triggered them on some backends.
static int SDLCALL watchAppEvents(void * /*udata*/, SDL_Event *event)
{
    switch (event->type)
    {
    // On iOS, calling any OpenGL ES function after the function which triggers
    // SDL_APP_DIDENTERBACKGROUND is called will kill the app, so we handle it
    // with an event watch callback, which will be called inside that function.
    case SDL_APP_DIDENTERBACKGROUND:
    case SDL_APP_WILLENTERFOREGROUND:
        TGraphics::instance()->setActive(event->type == SDL_APP_WILLENTERFOREGROUND);
        break;
    default:
        break;
    }

    return 1;
}

TEvent *TEvent::mInstance=NULL;

TEvent::TEvent()
{
    if(SDL_InitSubSystem(SDL_INIT_EVENTS) < 0)
        throw TException("Could not initialize SDL events subsystem (%s)", SDL_GetError());

    SDL_AddEventWatch(watchAppEvents, this);
}

TEvent::~TEvent()
{
    SDL_DelEventWatch(watchAppEvents, this);
    SDL_QuitSubSystem(SDL_INIT_EVENTS);
}

void TEvent::push(TMessage *msg)
{
    TLock lock(mMutex);
    msg->retain();
    mQueue.push(msg);
}

bool TEvent::poll(TMessage *&msg)
{
    TLock lock(mMutex);
    if(mQueue.empty())
        return false;
    msg = mQueue.front();
    mQueue.pop();
    return true;
}

void TEvent::pump()
{
    SDL_Event e;

    while (SDL_PollEvent(&e))
    {
        TMessage *msg = convert(e);
        if(msg)
        {
            push(msg);
            msg->release();
        }
    }
}

TMessage *TEvent::wait()
{
    SDL_Event e;

    if(SDL_WaitEvent(&e) != 1)
        return nullptr;

    return convert(e);
}

void TEvent::clear()
{
    SDL_Event e;

    while (SDL_PollEvent(&e))
    {
        // Do nothing with 'e' ...
    }

    TLock lock(mMutex);
    while (!mQueue.empty())
    {
        // std::queue::pop will remove the first (front) element.
        mQueue.front()->release();
        mQueue.pop();
    }
}

TMessage *TEvent::convert(const SDL_Event &e) const
{
    TMessage *msg = nullptr;

    std::vector<TVariant> vargs;
    vargs.reserve(4);

    //Filesystem *filesystem = nullptr;

    TKeyboard::Key key = TKeyboard::KEY_UNKNOWN;
    TKeyboard::Scancode scancode = TKeyboard::SCANCODE_UNKNOWN;

    EventType event;
    const char *text1;
    const char *text2;
    std::map<SDL_Keycode, TKeyboard::Key>::const_iterator keyit;

#ifndef THEALL_MACOSX
    TTouch::TouchInfo touchinfo;
#endif

#ifdef THEALL_LINUX
    static bool touchNormalizationBug = false;
#endif

    switch (e.type)
    {
    case SDL_KEYDOWN:
        if(e.key.repeat)
        {
            if(!TKeyboard::instance()->hasKeyRepeat())
                break;
        }

        keyit = mKeys.find(e.key.keysym.sym);
        if(keyit != mKeys.end())
            key = keyit->second;

//        if(!TKeyboard::getConstant(key, text1))
//            text1 = "unknown";

        TKeyboard::getConstant(e.key.keysym.scancode, scancode);
//        if(!TKeyboard::getConstant(scancode, text2))
//            text2 = "unknown";

        vargs.emplace_back((double)key);
        vargs.emplace_back((double)scancode);
        vargs.emplace_back(e.key.repeat != 0);
        msg = new TMessage(EVENT_KEYPRESSED, vargs);
        break;
    case SDL_KEYUP:
        keyit = mKeys.find(e.key.keysym.sym);
        if(keyit != mKeys.end())
            key = keyit->second;

//        if(!TKeyboard::getConstant(key, text1))
//            text1 = "unknown";

        TKeyboard::getConstant(e.key.keysym.scancode, scancode);
//        if(!TKeyboard::getConstant(scancode, text2))
//            text2 = "unknown";

        vargs.emplace_back(text1);
        vargs.emplace_back(text2);
        msg = new TMessage(EVENT_KEYRELEASED, vargs);
        break;
    case SDL_TEXTINPUT:
        vargs.emplace_back(e.text.text);
        msg = new TMessage(EVENT_TEXTINPUT, vargs);
        break;
    case SDL_TEXTEDITING:
        vargs.emplace_back(e.edit.text);
        vargs.emplace_back((double)e.edit.start);
        vargs.emplace_back((double)e.edit.length);
        msg = new TMessage(EVENT_TEXTEDITED, vargs);
        break;
    case SDL_MOUSEMOTION:
        {
            double x = (double) e.motion.x;
            double y = (double) e.motion.y;
            double xrel = (double) e.motion.xrel;
            double yrel = (double) e.motion.yrel;
            windowToPixelCoords(&x, &y);
            windowToPixelCoords(&xrel, &yrel);
            vargs.emplace_back(x);
            vargs.emplace_back(y);
            vargs.emplace_back(xrel);
            vargs.emplace_back(yrel);
            vargs.emplace_back(e.motion.which == SDL_TOUCH_MOUSEID);
            msg = new TMessage(EVENT_MOUSEMOVED, vargs);
        }
        break;
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
        {
            // SDL uses button index 3 for the right mouse button, but we use
            // index 2.
            int button = e.button.button;
            switch (button)
            {
            case SDL_BUTTON_RIGHT:
                button = 2;
                break;
            case SDL_BUTTON_MIDDLE:
                button = 3;
                break;
            }

            double x = (double) e.button.x;
            double y = (double) e.button.y;
            windowToPixelCoords(&x, &y);
            vargs.emplace_back(x);
            vargs.emplace_back(y);
            vargs.emplace_back((double) button);
            vargs.emplace_back(e.button.which == SDL_TOUCH_MOUSEID);
            msg = new TMessage((e.type == SDL_MOUSEBUTTONDOWN) ?
                              EVENT_MOUSEPRESSED : EVENT_MOUSERELEASED,
                              vargs);
        }
        break;
    case SDL_MOUSEWHEEL:
        vargs.emplace_back((double) e.wheel.x);
        vargs.emplace_back((double) e.wheel.y);
        msg = new TMessage(EVENT_WHEELMOVED, vargs);
        break;
    case SDL_FINGERDOWN:
    case SDL_FINGERUP:
    case SDL_FINGERMOTION:
        // Touch events are disabled in OS X because we only actually want touch
        // screen events, but most touch devices in OS X aren't touch screens
        // (and SDL doesn't differentiate.) Non-screen touch devices like Mac
        // trackpads won't give touch coords in the window's coordinate-space.
#ifndef THEALL_MACOSX
        touchinfo.id = (int64) e.tfinger.fingerId;
        touchinfo.x = e.tfinger.x;
        touchinfo.y = e.tfinger.y;
        touchinfo.dx = e.tfinger.dx;
        touchinfo.dy = e.tfinger.dy;
        touchinfo.pressure = e.tfinger.pressure;
#ifdef THEALL_LINUX
        // FIXME: hacky workaround for SDL not normalizing touch coordinates in
        // its X11 backend: https://bugzilla.libsdl.org/show_bug.cgi?id=2307
        if(touchNormalizationBug || fabs(touchinfo.x) >= 1.5 || fabs(touchinfo.y) >= 1.5 || fabs(touchinfo.dx) >= 1.5 || fabs(touchinfo.dy) >= 1.5)
        {
            touchNormalizationBug = true;
            windowToPixelCoords(&touchinfo.x, &touchinfo.y);
            windowToPixelCoords(&touchinfo.dx, &touchinfo.dy);
        }
        else
#endif
        {
            // SDL's coords are normalized to [0, 1], but we want them in pixels.
            normalizedToPixelCoords(&touchinfo.x, &touchinfo.y);
            normalizedToPixelCoords(&touchinfo.dx, &touchinfo.dy);
        }

        // We need to update the love.touch.sdl internal state from here.
        TTouch::instance()->onEvent(e.type, touchinfo);

        // This is a bit hackish and we lose the higher 32 bits of the id on
        // 32-bit systems, but SDL only ever gives id's that at most use as many
        // bits as can fit in a pointer (for now.)
        // We use lightuserdata instead of a lua_Number (double) because doubles
        // can't represent all possible id values on 64-bit systems.
        vargs.emplace_back((void *) (intptr_t) touchinfo.id);
        vargs.emplace_back(touchinfo.x);
        vargs.emplace_back(touchinfo.y);
        vargs.emplace_back(touchinfo.dx);
        vargs.emplace_back(touchinfo.dy);
        vargs.emplace_back(touchinfo.pressure);

        if(e.type == SDL_FINGERDOWN)
            event = EVENT_TOUCHPRESSED;
        else if(e.type == SDL_FINGERUP)
            event = EVENT_TOUCHRELEASED;
        else
            event = EVENT_TOUCHMOVED;
        msg = new TMessage(event, vargs);
#endif
        break;
    case SDL_JOYBUTTONDOWN:
    case SDL_JOYBUTTONUP:
    case SDL_JOYAXISMOTION:
    case SDL_JOYBALLMOTION:
    case SDL_JOYHATMOTION:
    case SDL_JOYDEVICEADDED:
    case SDL_JOYDEVICEREMOVED:
    case SDL_CONTROLLERBUTTONDOWN:
    case SDL_CONTROLLERBUTTONUP:
    case SDL_CONTROLLERAXISMOTION:
        msg = convertJoystickEvent(e);
        break;
    case SDL_WINDOWEVENT:
        msg = convertWindowEvent(e);
        break;
    case SDL_DROPFILE:
        {
            TFileSystem *filesystem = TFileSystem::instance();
            // Allow mounting any dropped path, so zips or dirs can be mounted.
            filesystem->allowMountingForPath(e.drop.file);

            if(filesystem->isRealDirectory(e.drop.file))
            {
                vargs.emplace_back(e.drop.file);
                msg = new TMessage(EVENT_DIRECTORYDROPPED, vargs);
            }
            else
            {
                vargs.emplace_back(new TDroppedFile(e.drop.file));
                msg = new TMessage(EVENT_FILEDROPPED, vargs);
            }
            SDL_free(e.drop.file);
        }
        break;
    case SDL_QUIT:
    case SDL_APP_TERMINATING:
        msg = new TMessage(EVENT_QUIT, std::vector<TVariant>());
        break;
    case SDL_APP_LOWMEMORY:
        msg = new TMessage(EVENT_LOWMEMORY, std::vector<TVariant>());
        break;
    default:
        break;
    }

    return msg;
}

TMessage *TEvent::convertJoystickEvent(const SDL_Event &e) const
{
    TJoystickManager *joystickManager = TJoystickManager::instance();
    if(!joystickManager)
        return nullptr;

    TMessage *msg = nullptr;

    std::vector<TVariant> vargs;
    vargs.reserve(4);

    TJoystick::Hat hat;
    TJoystick::GamepadButton padbutton;
    TJoystick::GamepadAxis padaxis;
    const char *txt;

    switch (e.type)
    {
    case SDL_JOYBUTTONDOWN:
    case SDL_JOYBUTTONUP:
        {
            TJoystick *joystick = joystickManager->getJoystickFromID(e.jbutton.which);
            if(!joystick)
                break;

            vargs.emplace_back(joystick);
            vargs.emplace_back((double)e.jbutton.button+1);
            msg = new TMessage((e.type == SDL_JOYBUTTONDOWN) ?
                              EVENT_JOYSTICKPRESSED : EVENT_JOYSTICKRELEASED,
                              vargs);
            break;
        }
    case SDL_JOYAXISMOTION:
        {
            TJoystick *joystick = joystickManager->getJoystickFromID(e.jaxis.which);
            if(!joystick)
                break;

            vargs.emplace_back(joystick);
            vargs.emplace_back((double)e.jaxis.axis+1);
            float value = TJoystick::clampval(e.jaxis.value / 32768.0f);
            vargs.emplace_back((double)value);
            msg = new TMessage(EVENT_JOYSTICKAXIS, vargs);
        }
        break;
    case SDL_JOYHATMOTION:
        {
            if(!TJoystick::getConstant(e.jhat.value, hat) || !TAbstractJoystick::getConstant(hat, txt))
                break;

            TJoystick *joystick = joystickManager->getJoystickFromID(e.jhat.which);
            if(!joystick)
                break;

            vargs.emplace_back(joystick);
            vargs.emplace_back((double)(e.jhat.hat+1));
            vargs.emplace_back(txt);
            msg = new TMessage(EVENT_JOYSTICKHAT, vargs);
        }
        break;
    case SDL_CONTROLLERBUTTONDOWN:
    case SDL_CONTROLLERBUTTONUP:
        {
            if(!TJoystick::getConstant((SDL_GameControllerButton) e.cbutton.button, padbutton))
                break;

            if(!TAbstractJoystick::getConstant(padbutton, txt))
                break;

            TJoystick *joystick = joystickManager->getJoystickFromID(e.cbutton.which);
            if(!joystick)
                break;

            vargs.emplace_back(joystick);
            vargs.emplace_back(txt);
            msg = new TMessage(e.type == SDL_CONTROLLERBUTTONDOWN ?
                              EVENT_GAMEPADPRESSED : EVENT_GAMEPADRELEASED, vargs);
        }
        break;
    case SDL_CONTROLLERAXISMOTION:
        if(TJoystick::getConstant((SDL_GameControllerAxis) e.caxis.axis, padaxis))
        {
            if(!TAbstractJoystick::getConstant(padaxis, txt))
                break;

            TJoystick *joystick = joystickManager->getJoystickFromID(e.caxis.which);
            if(!joystick)
                break;

            vargs.emplace_back(joystick);

            vargs.emplace_back(txt);
            float value = TJoystick::clampval(e.caxis.value / 32768.0f);
            vargs.emplace_back((double) value);
            msg = new TMessage(EVENT_GAMEPADAXIS, vargs);
        }
        break;
    case SDL_JOYDEVICEADDED:
        {
            // jdevice.which is the joystick device index.
            TJoystick *joystick = joystickManager->addJoystick(e.jdevice.which);
            if(joystick)
            {
                vargs.emplace_back(joystick);
                msg = new TMessage(EVENT_JOYSTICKADDED, vargs);
            }
        }
        break;
    case SDL_JOYDEVICEREMOVED:
        {
            // jdevice.which is the joystick instance ID now.
            TJoystick *joystick = joystickManager->getJoystickFromID(e.jdevice.which);
            if(joystick)
            {
                joystickManager->removeJoystick((TJoystick *) joystick);
                vargs.emplace_back(joystick);
                msg = new TMessage(EVENT_JOYSTICKREMOVED, vargs);
            }
        }
        break;
    default:
        break;
    }

    return msg;
}

TMessage *TEvent::convertWindowEvent(const SDL_Event &e) const
{
    TMessage *msg = nullptr;

    std::vector<TVariant> vargs;
    vargs.reserve(4);

    //window::TWindow *win = nullptr;

    if(e.type != SDL_WINDOWEVENT)
        return nullptr;

    switch (e.window.event)
    {
    case SDL_WINDOWEVENT_FOCUS_GAINED:
    case SDL_WINDOWEVENT_FOCUS_LOST:
        vargs.emplace_back(e.window.event == SDL_WINDOWEVENT_FOCUS_GAINED);
        msg = new TMessage(EVENT_FOCUS, vargs);
        break;
    case SDL_WINDOWEVENT_ENTER:
    case SDL_WINDOWEVENT_LEAVE:
        vargs.emplace_back(e.window.event == SDL_WINDOWEVENT_ENTER);
        msg = new TMessage(EVENT_MOUSEFOCUS, vargs);
        break;
    case SDL_WINDOWEVENT_SHOWN:
    case SDL_WINDOWEVENT_HIDDEN:
        vargs.emplace_back(e.window.event == SDL_WINDOWEVENT_SHOWN);
        msg = new TMessage(EVENT_VISIBLE, vargs);
        break;
    case SDL_WINDOWEVENT_RESIZED:
        {
            int px_w = e.window.data1;
            int px_h = e.window.data2;

            SDL_Window *sdlwin = SDL_GetWindowFromID(e.window.windowID);
            if(sdlwin)
                SDL_GL_GetDrawableSize(sdlwin, &px_w, &px_h);

            vargs.emplace_back((double) px_w);
            vargs.emplace_back((double) px_h);
            vargs.emplace_back((double) e.window.data1);
            vargs.emplace_back((double) e.window.data2);
            msg = new TMessage(EVENT_RESIZE, vargs);
        }
        break;
    case SDL_WINDOWEVENT_SIZE_CHANGED:
        TWindow::instance()->onSizeChanged(e.window.data1, e.window.data2);
        break;
    case SDL_WINDOWEVENT_MINIMIZED:
    case SDL_WINDOWEVENT_RESTORED:
#ifdef THEALL_ANDROID
    {
        TAudio *audio = TAudio::instance();
        if(audio)
        {
            if(e.window.event == SDL_WINDOWEVENT_MINIMIZED)
                audio->pause();
            else if(e.window.event == SDL_WINDOWEVENT_RESTORED)
                audio->resume();
        }
    }
#endif
        break;
    }

    return msg;
}

std::map<SDL_Keycode, TKeyboard::Key> TEvent::createKeyMap()
{
    std::map<SDL_Keycode, TKeyboard::Key> k;

    k[SDLK_UNKNOWN] = TKeyboard::KEY_UNKNOWN;

    k[SDLK_RETURN] = TKeyboard::KEY_RETURN;
    k[SDLK_ESCAPE] = TKeyboard::KEY_ESCAPE;
    k[SDLK_BACKSPACE] = TKeyboard::KEY_BACKSPACE;
    k[SDLK_TAB] = TKeyboard::KEY_TAB;
    k[SDLK_SPACE] = TKeyboard::KEY_SPACE;
    k[SDLK_EXCLAIM] = TKeyboard::KEY_EXCLAIM;
    k[SDLK_QUOTEDBL] = TKeyboard::KEY_QUOTEDBL;
    k[SDLK_HASH] = TKeyboard::KEY_HASH;
    k[SDLK_PERCENT] = TKeyboard::KEY_PERCENT;
    k[SDLK_DOLLAR] = TKeyboard::KEY_DOLLAR;
    k[SDLK_AMPERSAND] = TKeyboard::KEY_AMPERSAND;
    k[SDLK_QUOTE] = TKeyboard::KEY_QUOTE;
    k[SDLK_LEFTPAREN] = TKeyboard::KEY_LEFTPAREN;
    k[SDLK_RIGHTPAREN] = TKeyboard::KEY_RIGHTPAREN;
    k[SDLK_ASTERISK] = TKeyboard::KEY_ASTERISK;
    k[SDLK_PLUS] = TKeyboard::KEY_PLUS;
    k[SDLK_COMMA] = TKeyboard::KEY_COMMA;
    k[SDLK_MINUS] = TKeyboard::KEY_MINUS;
    k[SDLK_PERIOD] = TKeyboard::KEY_PERIOD;
    k[SDLK_SLASH] = TKeyboard::KEY_SLASH;
    k[SDLK_0] = TKeyboard::KEY_0;
    k[SDLK_1] = TKeyboard::KEY_1;
    k[SDLK_2] = TKeyboard::KEY_2;
    k[SDLK_3] = TKeyboard::KEY_3;
    k[SDLK_4] = TKeyboard::KEY_4;
    k[SDLK_5] = TKeyboard::KEY_5;
    k[SDLK_6] = TKeyboard::KEY_6;
    k[SDLK_7] = TKeyboard::KEY_7;
    k[SDLK_8] = TKeyboard::KEY_8;
    k[SDLK_9] = TKeyboard::KEY_9;
    k[SDLK_COLON] = TKeyboard::KEY_COLON;
    k[SDLK_SEMICOLON] = TKeyboard::KEY_SEMICOLON;
    k[SDLK_LESS] = TKeyboard::KEY_LESS;
    k[SDLK_EQUALS] = TKeyboard::KEY_EQUALS;
    k[SDLK_GREATER] = TKeyboard::KEY_GREATER;
    k[SDLK_QUESTION] = TKeyboard::KEY_QUESTION;
    k[SDLK_AT] = TKeyboard::KEY_AT;

    k[SDLK_LEFTBRACKET] = TKeyboard::KEY_LEFTBRACKET;
    k[SDLK_BACKSLASH] = TKeyboard::KEY_BACKSLASH;
    k[SDLK_RIGHTBRACKET] = TKeyboard::KEY_RIGHTBRACKET;
    k[SDLK_CARET] = TKeyboard::KEY_CARET;
    k[SDLK_UNDERSCORE] = TKeyboard::KEY_UNDERSCORE;
    k[SDLK_BACKQUOTE] = TKeyboard::KEY_BACKQUOTE;
    k[SDLK_a] = TKeyboard::KEY_A;
    k[SDLK_b] = TKeyboard::KEY_B;
    k[SDLK_c] = TKeyboard::KEY_C;
    k[SDLK_d] = TKeyboard::KEY_D;
    k[SDLK_e] = TKeyboard::KEY_E;
    k[SDLK_f] = TKeyboard::KEY_F;
    k[SDLK_g] = TKeyboard::KEY_G;
    k[SDLK_h] = TKeyboard::KEY_H;
    k[SDLK_i] = TKeyboard::KEY_I;
    k[SDLK_j] = TKeyboard::KEY_J;
    k[SDLK_k] = TKeyboard::KEY_K;
    k[SDLK_l] = TKeyboard::KEY_L;
    k[SDLK_m] = TKeyboard::KEY_M;
    k[SDLK_n] = TKeyboard::KEY_N;
    k[SDLK_o] = TKeyboard::KEY_O;
    k[SDLK_p] = TKeyboard::KEY_P;
    k[SDLK_q] = TKeyboard::KEY_Q;
    k[SDLK_r] = TKeyboard::KEY_R;
    k[SDLK_s] = TKeyboard::KEY_S;
    k[SDLK_t] = TKeyboard::KEY_T;
    k[SDLK_u] = TKeyboard::KEY_U;
    k[SDLK_v] = TKeyboard::KEY_V;
    k[SDLK_w] = TKeyboard::KEY_W;
    k[SDLK_x] = TKeyboard::KEY_X;
    k[SDLK_y] = TKeyboard::KEY_Y;
    k[SDLK_z] = TKeyboard::KEY_Z;

    k[SDLK_CAPSLOCK] = TKeyboard::KEY_CAPSLOCK;

    k[SDLK_F1] = TKeyboard::KEY_F1;
    k[SDLK_F2] = TKeyboard::KEY_F2;
    k[SDLK_F3] = TKeyboard::KEY_F3;
    k[SDLK_F4] = TKeyboard::KEY_F4;
    k[SDLK_F5] = TKeyboard::KEY_F5;
    k[SDLK_F6] = TKeyboard::KEY_F6;
    k[SDLK_F7] = TKeyboard::KEY_F7;
    k[SDLK_F8] = TKeyboard::KEY_F8;
    k[SDLK_F9] = TKeyboard::KEY_F9;
    k[SDLK_F10] = TKeyboard::KEY_F10;
    k[SDLK_F11] = TKeyboard::KEY_F11;
    k[SDLK_F12] = TKeyboard::KEY_F12;

    k[SDLK_PRINTSCREEN] = TKeyboard::KEY_PRINTSCREEN;
    k[SDLK_SCROLLLOCK] = TKeyboard::KEY_SCROLLLOCK;
    k[SDLK_PAUSE] = TKeyboard::KEY_PAUSE;
    k[SDLK_INSERT] = TKeyboard::KEY_INSERT;
    k[SDLK_HOME] = TKeyboard::KEY_HOME;
    k[SDLK_PAGEUP] = TKeyboard::KEY_PAGEUP;
    k[SDLK_DELETE] = TKeyboard::KEY_DELETE;
    k[SDLK_END] = TKeyboard::KEY_END;
    k[SDLK_PAGEDOWN] = TKeyboard::KEY_PAGEDOWN;
    k[SDLK_RIGHT] = TKeyboard::KEY_RIGHT;
    k[SDLK_LEFT] = TKeyboard::KEY_LEFT;
    k[SDLK_DOWN] = TKeyboard::KEY_DOWN;
    k[SDLK_UP] = TKeyboard::KEY_UP;

    k[SDLK_NUMLOCKCLEAR] = TKeyboard::KEY_NUMLOCKCLEAR;
    k[SDLK_KP_DIVIDE] = TKeyboard::KEY_KP_DIVIDE;
    k[SDLK_KP_MULTIPLY] = TKeyboard::KEY_KP_MULTIPLY;
    k[SDLK_KP_MINUS] = TKeyboard::KEY_KP_MINUS;
    k[SDLK_KP_PLUS] = TKeyboard::KEY_KP_PLUS;
    k[SDLK_KP_ENTER] = TKeyboard::KEY_KP_ENTER;
    k[SDLK_KP_0] = TKeyboard::KEY_KP_0;
    k[SDLK_KP_1] = TKeyboard::KEY_KP_1;
    k[SDLK_KP_2] = TKeyboard::KEY_KP_2;
    k[SDLK_KP_3] = TKeyboard::KEY_KP_3;
    k[SDLK_KP_4] = TKeyboard::KEY_KP_4;
    k[SDLK_KP_5] = TKeyboard::KEY_KP_5;
    k[SDLK_KP_6] = TKeyboard::KEY_KP_6;
    k[SDLK_KP_7] = TKeyboard::KEY_KP_7;
    k[SDLK_KP_8] = TKeyboard::KEY_KP_8;
    k[SDLK_KP_9] = TKeyboard::KEY_KP_9;
    k[SDLK_KP_PERIOD] = TKeyboard::KEY_KP_PERIOD;
    k[SDLK_KP_COMMA] = TKeyboard::KEY_KP_COMMA;
    k[SDLK_KP_EQUALS] = TKeyboard::KEY_KP_EQUALS;

    k[SDLK_APPLICATION] = TKeyboard::KEY_APPLICATION;
    k[SDLK_POWER] = TKeyboard::KEY_POWER;
    k[SDLK_F13] = TKeyboard::KEY_F13;
    k[SDLK_F14] = TKeyboard::KEY_F14;
    k[SDLK_F15] = TKeyboard::KEY_F15;
    k[SDLK_F16] = TKeyboard::KEY_F16;
    k[SDLK_F17] = TKeyboard::KEY_F17;
    k[SDLK_F18] = TKeyboard::KEY_F18;
    k[SDLK_F19] = TKeyboard::KEY_F19;
    k[SDLK_F20] = TKeyboard::KEY_F20;
    k[SDLK_F21] = TKeyboard::KEY_F21;
    k[SDLK_F22] = TKeyboard::KEY_F22;
    k[SDLK_F23] = TKeyboard::KEY_F23;
    k[SDLK_F24] = TKeyboard::KEY_F24;
    k[SDLK_EXECUTE] = TKeyboard::KEY_EXECUTE_;
    k[SDLK_HELP] = TKeyboard::KEY_HELP;
    k[SDLK_MENU] = TKeyboard::KEY_MENU;
    k[SDLK_SELECT] = TKeyboard::KEY_SELECT;
    k[SDLK_STOP] = TKeyboard::KEY_STOP;
    k[SDLK_AGAIN] = TKeyboard::KEY_AGAIN;
    k[SDLK_UNDO] = TKeyboard::KEY_UNDO;
    k[SDLK_CUT] = TKeyboard::KEY_CUT;
    k[SDLK_COPY] = TKeyboard::KEY_COPY;
    k[SDLK_PASTE] = TKeyboard::KEY_PASTE;
    k[SDLK_FIND] = TKeyboard::KEY_FIND;
    k[SDLK_MUTE] = TKeyboard::KEY_MUTE;
    k[SDLK_VOLUMEUP] = TKeyboard::KEY_VOLUMEUP;
    k[SDLK_VOLUMEDOWN] = TKeyboard::KEY_VOLUMEDOWN;

    k[SDLK_ALTERASE] = TKeyboard::KEY_ALTERASE;
    k[SDLK_SYSREQ] = TKeyboard::KEY_SYSREQ;
    k[SDLK_CANCEL] = TKeyboard::KEY_CANCEL;
    k[SDLK_CLEAR] = TKeyboard::KEY_CLEAR;
    k[SDLK_PRIOR] = TKeyboard::KEY_PRIOR;
    k[SDLK_RETURN2] = TKeyboard::KEY_RETURN2;
    k[SDLK_SEPARATOR] = TKeyboard::KEY_SEPARATOR;
    k[SDLK_OUT] = TKeyboard::KEY_OUT;
    k[SDLK_OPER] = TKeyboard::KEY_OPER;
    k[SDLK_CLEARAGAIN] = TKeyboard::KEY_CLEARAGAIN;

    k[SDLK_THOUSANDSSEPARATOR] = TKeyboard::KEY_THOUSANDSSEPARATOR;
    k[SDLK_DECIMALSEPARATOR] = TKeyboard::KEY_DECIMALSEPARATOR;
    k[SDLK_CURRENCYUNIT] = TKeyboard::KEY_CURRENCYUNIT;
    k[SDLK_CURRENCYSUBUNIT] = TKeyboard::KEY_CURRENCYSUBUNIT;

    k[SDLK_LCTRL] = TKeyboard::KEY_LCTRL;
    k[SDLK_LSHIFT] = TKeyboard::KEY_LSHIFT;
    k[SDLK_LALT] = TKeyboard::KEY_LALT;
    k[SDLK_LGUI] = TKeyboard::KEY_LGUI;
    k[SDLK_RCTRL] = TKeyboard::KEY_RCTRL;
    k[SDLK_RSHIFT] = TKeyboard::KEY_RSHIFT;
    k[SDLK_RALT] = TKeyboard::KEY_RALT;
    k[SDLK_RGUI] = TKeyboard::KEY_RGUI;

    k[SDLK_MODE] = TKeyboard::KEY_MODE;

    k[SDLK_AUDIONEXT] = TKeyboard::KEY_AUDIONEXT;
    k[SDLK_AUDIOPREV] = TKeyboard::KEY_AUDIOPREV;
    k[SDLK_AUDIOSTOP] = TKeyboard::KEY_AUDIOSTOP;
    k[SDLK_AUDIOPLAY] = TKeyboard::KEY_AUDIOPLAY;
    k[SDLK_AUDIOMUTE] = TKeyboard::KEY_AUDIOMUTE;
    k[SDLK_MEDIASELECT] = TKeyboard::KEY_MEDIASELECT;
    k[SDLK_WWW] = TKeyboard::KEY_WWW;
    k[SDLK_MAIL] = TKeyboard::KEY_MAIL;
    k[SDLK_CALCULATOR] = TKeyboard::KEY_CALCULATOR;
    k[SDLK_COMPUTER] = TKeyboard::KEY_COMPUTER;
    k[SDLK_AC_SEARCH] = TKeyboard::KEY_APP_SEARCH;
    k[SDLK_AC_HOME] = TKeyboard::KEY_APP_HOME;
    k[SDLK_AC_BACK] = TKeyboard::KEY_APP_BACK;
    k[SDLK_AC_FORWARD] = TKeyboard::KEY_APP_FORWARD;
    k[SDLK_AC_STOP] = TKeyboard::KEY_APP_STOP;
    k[SDLK_AC_REFRESH] = TKeyboard::KEY_APP_REFRESH;
    k[SDLK_AC_BOOKMARKS] = TKeyboard::KEY_APP_BOOKMARKS;

    k[SDLK_BRIGHTNESSDOWN] = TKeyboard::KEY_BRIGHTNESSDOWN;
    k[SDLK_BRIGHTNESSUP] = TKeyboard::KEY_BRIGHTNESSUP;
    k[SDLK_DISPLAYSWITCH] = TKeyboard::KEY_DISPLAYSWITCH;
    k[SDLK_KBDILLUMTOGGLE] = TKeyboard::KEY_KBDILLUMTOGGLE;
    k[SDLK_KBDILLUMDOWN] = TKeyboard::KEY_KBDILLUMDOWN;
    k[SDLK_KBDILLUMUP] = TKeyboard::KEY_KBDILLUMUP;
    k[SDLK_EJECT] = TKeyboard::KEY_EJECT;
    k[SDLK_SLEEP] = TKeyboard::KEY_SLEEP;

#ifdef THEALL_ANDROID
    k[SDLK_AC_BACK] = TKeyboard::KEY_ESCAPE;
#endif

    return k;
}

std::map<SDL_Keycode, TKeyboard::Key> TEvent::mKeys = TEvent::createKeyMap();
