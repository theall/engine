/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_THREAD_THREAD_H
#define THEALL_THREAD_THREAD_H

#include "common/config.h"

#include "utils/std_pch.h"

class TAbstractThread
{
public:

    virtual ~TAbstractThread() {}
	virtual bool start() = 0;
	virtual void wait() = 0;
	virtual bool isRunning() = 0;

}; // Thread

class TAbstractMutex
{
public:
    virtual ~TAbstractMutex() {}

    virtual void lock() = 0;
    virtual void unlock() = 0;
};

class TAbstractConditional
{
public:
    virtual ~TAbstractConditional() {}

    virtual void signal() = 0;
    virtual void broadcast() = 0;
    virtual bool wait(TAbstractMutex *mutex, int timeout=-1) = 0;
};

//class TLock
//{
//public:
//    TLock(TAbstractMutex *m);
//    TLock(TAbstractMutex &m);
//    ~TLock();

//private:
//    TAbstractMutex *mMutex;
//};

//class TEmptyLock
//{
//public:
//    TEmptyLock();
//    ~TEmptyLock();

//    void setLock(TAbstractMutex *m);
//    void setLock(TAbstractMutex &m);

//private:
//    TAbstractMutex *mMutex;
//};

//class Threadable
//{
//public:
//	Threadable();
//	virtual ~Threadable();

//	virtual void threadFunction() = 0;

//	bool start();
//	void wait();
//	bool isRunning() const;
//	const char *getThreadName() const;

//protected:

//    TAbstractThread *mOwner;
//	std::string threadName;

//};

class TMutexRef
{
public:
    TMutexRef();
    ~TMutexRef();

    operator TAbstractMutex*() const;
    TAbstractMutex *operator->() const;

private:
    TAbstractMutex *mMutex;
};

class TConditionalRef
{
public:
    TConditionalRef();
    ~TConditionalRef();

    operator TAbstractConditional*() const;
    TAbstractConditional *operator->() const;

private:
    TAbstractConditional *mConditional;
};

#endif // THEALL_THREAD_THREAD_H
