/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "abstractthread.h"

//TLock::TLock(TAbstractMutex *m)
//    : mMutex(m)
//{
//    mMutex->lock();
//}

//TLock::TLock(TAbstractMutex &m)
//    : mMutex(&m)
//{
//    mMutex->lock();
//}

//TLock::~TLock()
//{
//    mMutex->unlock();
//}

//TEmptyLock::TEmptyLock()
//    : mMutex(nullptr)
//{
//}

//TEmptyLock::~TEmptyLock()
//{
//    if(mMutex)
//        mMutex->unlock();
//}

//void TEmptyLock::setLock(TAbstractMutex *m)
//{
//    if(m)
//        m->lock();

//    if(mMutex)
//        mMutex->unlock();

//    mMutex = m;
//}

//void TEmptyLock::setLock(TAbstractMutex &m)
//{
//    m.lock();

//    if(mMutex)
//        mMutex->unlock();

//    mMutex = &m;
//}

//Threadable::Threadable()
//{
//    mOwner = nullptr;//newThread(this);
//}

//Threadable::~Threadable()
//{
//    delete mOwner;
//}

//bool Threadable::start()
//{
//    return mOwner->start();
//}

//void Threadable::wait()
//{
//    mOwner->wait();
//}

//bool Threadable::isRunning() const
//{
//    return mOwner->isRunning();
//}

//const char *Threadable::getThreadName() const
//{
//    return threadName.empty() ? nullptr : threadName.c_str();
//}

//TMutexRef::TMutexRef()
//    : mMutex(nullptr/*newMutex()*/)
//{
//}

//TMutexRef::~TMutexRef()
//{
//    delete mMutex;
//}

//TMutexRef::operator TAbstractMutex*() const
//{
//    return mMutex;
//}

//TAbstractMutex *TMutexRef::operator->() const
//{
//    return mMutex;
//}

//TConditionalRef::TConditionalRef()
//    : mConditional(nullptr/*newConditional()*/)
//{
//}

//TConditionalRef::~TConditionalRef()
//{
//    delete mConditional;
//}

//TConditionalRef::operator TAbstractConditional*() const
//{
//    return mConditional;
//}

//TAbstractConditional *TConditionalRef::operator->() const
//{
//    return mConditional;
//}
