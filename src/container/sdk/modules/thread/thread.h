/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_THREAD_SDL_THREAD_H
#define THEALL_THREAD_SDL_THREAD_H

#include <string>

// SDL
#include <SDL2/SDL_thread.h>

class TMutex
{
public:

    TMutex();
    ~TMutex();

	void lock();
	void unlock();

private:

	SDL_mutex *mutex;
    TMutex(const TMutex&/* mutex*/) {}

    friend class TConditional;

}; // Mutex

class TConditional
{
public:

    TConditional();
    ~TConditional();

	void signal();
	void broadcast();
    bool wait(TMutex *mutex, int timeout=-1);

private:
	SDL_cond *cond;
}; // Conditional

class TLock
{
public:
    TLock(TMutex *m);
    TLock(TMutex &m);
    ~TLock();

private:
    TMutex *mMutex;
};

class TEmptyLock
{
public:
    TEmptyLock();
    ~TEmptyLock();

    void setLock(TMutex *m);
    void setLock(TMutex &m);

private:
    TMutex *mMutex;
};

class TThread;
class Threadable
{
public:
    Threadable();
    virtual ~Threadable();

    virtual void threadFunction() = 0;

    bool start();
    void wait();
    bool isRunning() const;
    const char *getThreadName() const;

protected:

    TThread *mOwner;
    std::string threadName;

};

class TMutexRef
{
public:
    TMutexRef();
    ~TMutexRef();

    operator TMutex*() const;
    TMutex *operator->() const;

private:
    TMutex *mMutex;
};

class TConditionalRef
{
public:
    TConditionalRef();
    ~TConditionalRef();

    operator TConditional*() const;
    TConditional *operator->() const;

private:
    TConditional *mConditional;
};

class TThread
{
public:

    TThread(Threadable *t);
    ~TThread();
    bool start();
    void wait();
    bool isRunning();

private:
    Threadable *t;
    bool running;
    SDL_Thread *thread;
    TMutex mutex;

    static int thread_runner(void *data);

}; // Thread

TMutex *newMutex();
TConditional *newConditional();
TThread *newThread(Threadable *t);

#endif // THEALL_THREAD_SDL_THREAD_H
