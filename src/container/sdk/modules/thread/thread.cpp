/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "thread.h"

/**
 * Implementations of the functions declared in src/modules/threads.h.
 **/

TMutex *newMutex()
{
    return new TMutex();
}

TConditional *newConditional()
{
    return new TConditional();
}

TThread *newThread(Threadable *t)
{
    return new TThread(t);
}

TThread::TThread(Threadable *t)
	: t(t)
	, running(false)
	, thread(0)
{
}

TThread::~TThread()
{
	if(!running) // Clean up handle
		wait();
	/*
	if(running)
		wait();
	FIXME: Needed for proper thread cleanup
	*/
}

bool TThread::start()
{
    TLock l(mutex);
	if(running)
		return false;
	if(thread) // Clean old handle up
		SDL_WaitThread(thread, 0);
	thread = SDL_CreateThread(thread_runner, t->getThreadName(), this);
	running = (thread != 0);
	return running;
}

void TThread::wait()
{
	{
        TLock l(mutex);
		if(!thread)
			return;
	}
	SDL_WaitThread(thread, 0);
    TLock l(mutex);
	running = false;
	thread = 0;
}

bool TThread::isRunning()
{
    TLock l(mutex);
	return running;
}

int TThread::thread_runner(void *data)
{
    TThread *self = (TThread *) data; // some compilers don't like 'this'
	self->t->threadFunction();
    TLock l(self->mutex);
	self->running = false;
	return 0;
}


TMutex::TMutex()
{
    mutex = SDL_CreateMutex();
}

TMutex::~TMutex()
{
    SDL_DestroyMutex(mutex);
}

void TMutex::lock()
{
    SDL_LockMutex(mutex);
}

void TMutex::unlock()
{
    SDL_UnlockMutex(mutex);
}

TConditional::TConditional()
{
    cond = SDL_CreateCond();
}

TConditional::~TConditional()
{
    SDL_DestroyCond(cond);
}

void TConditional::signal()
{
    SDL_CondSignal(cond);
}

void TConditional::broadcast()
{
    SDL_CondBroadcast(cond);
}

bool TConditional::wait(TMutex *_mutex, int timeout)
{
    // Yes, I realise this can be dangerous,
    // however, you're asking for it if you're
    // mixing thread implementations.
    TMutex *mutex = (TMutex *) _mutex;
    if(timeout < 0)
        return !SDL_CondWait(cond, mutex->mutex);
    else
        return (SDL_CondWaitTimeout(cond, mutex->mutex, timeout) == 0);
}

TLock::TLock(TMutex *m)
    : mMutex(m)
{
    mMutex->lock();
}

TLock::TLock(TMutex &m)
    : mMutex(&m)
{
    mMutex->lock();
}

TLock::~TLock()
{
    mMutex->unlock();
}

TEmptyLock::TEmptyLock()
    : mMutex(nullptr)
{
}

TEmptyLock::~TEmptyLock()
{
    if(mMutex)
        mMutex->unlock();
}

void TEmptyLock::setLock(TMutex *m)
{
    if(m)
        m->lock();

    if(mMutex)
        mMutex->unlock();

    mMutex = m;
}

void TEmptyLock::setLock(TMutex &m)
{
    m.lock();

    if(mMutex)
        mMutex->unlock();

    mMutex = &m;
}

Threadable::Threadable()
{
    mOwner = newThread(this);
}

Threadable::~Threadable()
{
    delete mOwner;
}

bool Threadable::start()
{
    return mOwner->start();
}

void Threadable::wait()
{
    mOwner->wait();
}

bool Threadable::isRunning() const
{
    return mOwner->isRunning();
}

const char *Threadable::getThreadName() const
{
    return threadName.empty() ? nullptr : threadName.c_str();
}

TMutexRef::TMutexRef()
    : mMutex(newMutex())
{
}

TMutexRef::~TMutexRef()
{
    delete mMutex;
}

TMutexRef::operator TMutex*() const
{
    return mMutex;
}

TMutex *TMutexRef::operator->() const
{
    return mMutex;
}

TConditionalRef::TConditionalRef()
    : mConditional(nullptr/*newConditional()*/)
{
}

TConditionalRef::~TConditionalRef()
{
    delete mConditional;
}

TConditionalRef::operator TConditional*() const
{
    return mConditional;
}

TConditional *TConditionalRef::operator->() const
{
    return mConditional;
}
