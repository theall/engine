/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "file.h"

// STD
#include <cstring>

#include "filesystem.h"

//extern bool hack_setupWriteDirectory();
bool hack_setupWriteDirectory()
{
    return TFileSystem::instance()->setupWriteDirectory();
}

TFile::TFile(const std::string &filename)
    : TAbstractFile()
    , mFilename(filename)
    , mFile(nullptr)
    , mMode(MODE_CLOSED)
    , mBufferMode(BUFFER_NONE)
    , mBufferSize(0)
{
}

TFile::~TFile()
{
    if(mMode != MODE_CLOSED)
		close();
}

bool TFile::open(Mode mode)
{
	if(mode == MODE_CLOSED)
		return true;

    if(!PHYSFS_isInit()) {
		throw TException("PhysFS is not initialized.");
    }

	// File must exist if read mode.
    if((mode == MODE_READ) && !PHYSFS_exists(mFilename.c_str())) {
        throw TException("Could not open file %s. Does not exist.", mFilename.c_str());
    }

	// Check whether the write directory is set.
    if((mode == MODE_APPEND || mode == MODE_WRITE) && (PHYSFS_getWriteDir() == 0) && !hack_setupWriteDirectory())
		throw TException("Could not set write directory.");

	// File already open?
    if(mFile != nullptr)
		return false;

	PHYSFS_getLastError(); // Clear the error buffer.
	PHYSFS_File *handle = nullptr;

	switch (mode)
	{
	case MODE_READ:
        handle = PHYSFS_openRead(mFilename.c_str());
		break;
	case MODE_APPEND:
        handle = PHYSFS_openAppend(mFilename.c_str());
		break;
	case MODE_WRITE:
        handle = PHYSFS_openWrite(mFilename.c_str());
		break;
	default:
		break;
	}

	if(handle == nullptr)
	{
		const char *err = PHYSFS_getLastError();
		if(err == nullptr)
			err = "unknown error";
        throw TException("Could not open file %s (%s)", mFilename.c_str(), err);
	}

    mFile = handle;

    mMode = mode;

    if(mFile != nullptr && !setBuffer(mBufferMode, mBufferSize))
	{
		// Revert to buffer defaults if we don't successfully set the buffer.
        mBufferMode = BUFFER_NONE;
        mBufferSize = 0;
	}

    return (mFile != nullptr);
}

bool TFile::close()
{
    if(mFile == nullptr || !PHYSFS_close(mFile))
		return false;

    mMode = MODE_CLOSED;
    mFile = nullptr;

	return true;
}

bool TFile::isOpen() const
{
    return mMode != MODE_CLOSED && mFile != nullptr;
}

int64 TFile::getSize()
{
	// If the file is closed, open it to
	// check the size.
    if(mFile == nullptr)
	{
		open(MODE_READ);
        int64 size = (int64) PHYSFS_fileLength(mFile);
		close();
		return size;
	}

    return (int64) PHYSFS_fileLength(mFile);
}

int64 TFile::read(void *dst, int64 size)
{
    if(!mFile || mMode != MODE_READ)
		throw TException("File is not opened for reading.");

    int64 max = (int64)PHYSFS_fileLength(mFile);
	size = (size == ALL) ? max : size;
	size = (size > max) ? max : size;
	// Sadly, we'll have to clamp to 32 bits here
	size = (size > THEALL_UINT32_MAX) ? THEALL_UINT32_MAX : size;

	if(size < 0)
		throw TException("Invalid read size.");

#ifdef THEALL_USE_PHYSFS_2_1
    int64 read = PHYSFS_readBytes(mFile, dst, (PHYSFS_uint64) size);
#else
    int64 read = (int64)PHYSFS_read(mFile, dst, 1, (PHYSFS_uint32) size);
#endif

	return read;
}

bool TFile::write(const void *data, int64 size)
{
    if(!mFile || (mMode != MODE_WRITE && mMode != MODE_APPEND))
		throw TException("File is not opened for writing.");

	// Another clamp, for the time being.
	size = (size > THEALL_UINT32_MAX) ? THEALL_UINT32_MAX : size;

	if(size < 0)
		throw TException("Invalid write size.");

	// Try to write.
#ifdef THEALL_USE_PHYSFS_2_1
    int64 written = PHYSFS_writeBytes(mFile, data, (PHYSFS_uint64) size);
#else
    int64 written = (int64) PHYSFS_write(mFile, data, 1, (PHYSFS_uint32) size);
#endif

	// Check that correct amount of data was written.
	if(written != size)
		return false;

	// Manually flush the buffer in BUFFER_LINE mode if we find a newline.
    if(mBufferMode == BUFFER_LINE && mBufferSize > size)
	{
		if(memchr(data, '\n', (size_t) size) != NULL)
			flush();
	}

	return true;
}

bool TFile::flush()
{
    if(!mFile || (mMode != MODE_WRITE && mMode != MODE_APPEND))
		throw TException("File is not opened for writing.");

    return PHYSFS_flush(mFile) != 0;
}

#ifdef THEALL_WINDOWS
// MSVC doesn't like the 'this' keyword
// well, we'll use 'that'.
// It zigs, we zag.
inline bool test_eof(TFile *that, PHYSFS_File *)
{
	int64 pos = that->tell();
	int64 size = that->getSize();
	return pos == -1 || size == -1 || pos >= size;
}
#else
inline bool test_eof(File *, PHYSFS_File *file)
{
	return PHYSFS_eof(file);
}
#endif

bool TFile::isEOF()
{
    return mFile == nullptr || test_eof(this, mFile);
}

int64 TFile::tell()
{
    if(mFile == nullptr)
		return -1;

    return (int64) PHYSFS_tell(mFile);
}

bool TFile::seek(uint64 pos)
{
    return mFile != nullptr && PHYSFS_seek(mFile, (PHYSFS_uint64) pos) != 0;
}

bool TFile::setBuffer(BufferMode bufmode, int64 size)
{
	if(size < 0)
		return false;

	// If the file isn't open, we'll make sure the buffer values are set in
	// File::open.
	if(!isOpen())
	{
        mBufferMode = bufmode;
        mBufferSize = size;
		return true;
	}

	int ret = 1;

	switch (bufmode)
	{
	case BUFFER_NONE:
	default:
        ret = PHYSFS_setBuffer(mFile, 0);
		size = 0;
		break;
	case BUFFER_LINE:
	case BUFFER_FULL:
        ret = PHYSFS_setBuffer(mFile, size);
		break;
	}

	if(ret == 0)
		return false;

    mBufferMode = bufmode;
    mBufferSize = size;

	return true;
}

TFile::BufferMode TFile::getBuffer(int64 &size) const
{
    size = mBufferSize;
    return mBufferMode;
}

const std::string &TFile::getFilename() const
{
    return mFilename;
}

TFile::Mode TFile::getMode() const
{
    return mMode;
}



