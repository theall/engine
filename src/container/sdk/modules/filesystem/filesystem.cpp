/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include <iostream>
#include <sstream>
#include <algorithm>

#include "common/utf8.h"
#include "common/b64.h"

#include "filesystem.h"

// PhysFS
#ifdef THEALL_APPLE_USE_FRAMEWORKS
#include <physfs/physfs.h>
#else
#include <physfs.h>
#endif

// For great CWD. (Current Working Directory)
// Using this instead of boost::filesystem which totally
// cramped our style.
#ifdef THEALL_WINDOWS
#	include <windows.h>
#	include <direct.h>
#else
#	include <sys/param.h>
#	include <unistd.h>
#endif

#ifdef THEALL_IOS
#	include "common/ios.h"
#endif

#include <string>

#ifdef THEALL_ANDROID
#include <SDL2/SDL.h>
#include "common/android.h"
#endif

namespace
{
    std::string adjustDir(const std::string &dir)
    {
        std::string ret = dir;
        int l = ret.size() - 1;
        if(l <= 0)
            return dir;
        if(ret[l]!='/' && ret[l]!='\\')
            ret.push_back('/');

        return ret;
    }

	size_t getDriveDelim(const std::string &input)
	{
		for (size_t i = 0; i < input.size(); ++i)
			if(input[i] == '/' || input[i] == '\\')
				return i;
		// Something's horribly wrong
		return 0;
	}

	std::string getDriveRoot(const std::string &input)
	{
		return input.substr(0, getDriveDelim(input)+1);
	}

	std::string skipDriveRoot(const std::string &input)
	{
		return input.substr(getDriveDelim(input)+1);
	}

	std::string normalize(const std::string &input)
	{
		std::stringstream out;
		bool seenSep = false, isSep = false;
		for (size_t i = 0; i < input.size(); ++i)
		{
			isSep = (input[i] == THEALL_PATH_SEPARATOR[0]);
			if(!isSep || !seenSep)
				out << input[i];
			seenSep = isSep;
		}

		return out.str();
	}

}

TFileSystem *TFileSystem::mInstance=NULL;

TFileSystem::TFileSystem()
    : mFused(false)
    , mFusedSet(false)
{
    mRequirePath = {"?.lua", "?/init.lua"};
}

TFileSystem::~TFileSystem()
{
	if(PHYSFS_isInit())
		PHYSFS_deinit();
}

const char *TFileSystem::getName() const
{
	return "theall.filesystem.physfs";
}

void TFileSystem::init(const char *arg0)
{
	if(!PHYSFS_init(arg0))
		throw TException("%s", PHYSFS_getLastError());

	// Enable symlinks by default. Also fixes an issue in PhysFS 2.1-alpha.
	setSymlinksEnabled(true);
}

void TFileSystem::setFused(bool fused)
{
    if(mFusedSet)
		return;
    mFused = fused;
    mFusedSet = true;
}

bool TFileSystem::isFused() const
{
    if(!mFusedSet)
		return false;
    return mFused;
}

bool TFileSystem::setIdentity(const char *ident, bool appendToPath)
{
	if(!PHYSFS_isInit())
		return false;

    std::string old_save_path = mSaveFullPath;

	// Store the save directory.
    mSaveIdentity = std::string(ident);

	// Generate the relative path to the game save folder.
    mSavePathRelative = std::string(THEALL_APPDATA_PREFIX THEALL_APPDATA_FOLDER THEALL_PATH_SEPARATOR) + mSaveIdentity;

	// Generate the full path to the game save folder.
    mSaveFullPath = std::string(getCurrentDirectory()) + std::string(THEALL_PATH_SEPARATOR);
    if(mFused)
        mSaveFullPath += std::string(THEALL_APPDATA_PREFIX) + mSaveIdentity;
	else
        mSaveFullPath += mSavePathRelative;

    mSaveFullPath = normalize(mSaveFullPath);

#ifdef THEALL_ANDROID
	if(save_identity == "")
		save_identity = "unnamed";

	std::string storage_path;
	if(isAndroidSaveExternal())
		storage_path = SDL_AndroidGetExternalStoragePath();
	else
		storage_path = SDL_AndroidGetInternalStoragePath();

	std::string save_directory = storage_path + "/save";

	save_path_full = storage_path + std::string("/save/") + save_identity;

	if(!android::directoryExists(save_path_full.c_str()) &&
			!android::mkdir(save_path_full.c_str()))
		SDL_Log("Error: Could not create save directory %s!", save_path_full.c_str());
#endif

	// We now have something like:
	// save_identity: game
	// save_path_relative: ./LOVE/game
	// save_path_full: C:\Documents and Settings\user\Application Data/LOVE/game

	// We don't want old read-only save paths to accumulate when we set a new
	// identity.
	if(!old_save_path.empty())
	{
#ifdef THEALL_USE_PHYSFS_2_1
		PHYSFS_unmount(old_save_path.c_str());
#else
		PHYSFS_removeFromSearchPath(old_save_path.c_str());
#endif
	}

	// Try to add the save directory to the search path.
	// (No error on fail, it means that the path doesn't exist).
    PHYSFS_mount(mSaveFullPath.c_str(), nullptr, appendToPath);

	// HACK: This forces setupWriteDirectory to be called the next time a file
	// is opened for writing - otherwise it won't be called at all if it was
	// already called at least once before.
	PHYSFS_setWriteDir(nullptr);

    return true;
}

bool TFileSystem::setIdentity(const std::string &ident, bool appendToPath)
{
    return setIdentity(ident.c_str(), appendToPath);
}

const char *TFileSystem::getIdentity() const
{
    return mSaveIdentity.c_str();
}

bool TFileSystem::setSource(const std::string &source)
{
    return setSource(source.c_str());
}

bool TFileSystem::setSource(const char *source)
{
	if(!PHYSFS_isInit())
		return false;

    // Check whether directory is already set.
    if(!mGameSource.empty())
    {
        if(!PHYSFS_unmount(mGameSource.c_str()))
            return false;
    }

	std::string new_search_path = source;

#ifdef THEALL_ANDROID
	if(!android::createStorageDirectories())
		SDL_Log("Error creating storage directories!");

	char* game_archive_ptr = NULL;
	size_t game_archive_size = 0;
	bool archive_loaded = false;

	// try to load the game that was sent to LÖVE via a Intent
	archive_loaded = android::loadGameArchiveToMemory(android::getSelectedGameFile(), &game_archive_ptr, &game_archive_size);

	if(!archive_loaded)
	{
		// try to load the game in the assets/ folder
		archive_loaded = android::loadGameArchiveToMemory("game.love", &game_archive_ptr, &game_archive_size);
	}

	if(archive_loaded)
	{
		if(!PHYSFS_mountMemory(game_archive_ptr, game_archive_size, android::freeGameArchiveMemory, "archive.zip", "/", 0))
		{
			SDL_Log("Mounting of in-memory game archive failed!");
			android::freeGameArchiveMemory(game_archive_ptr);
			return false;
		}
	}
	else
	{
		// try to load the game in the directory that was sent to LÖVE via an
		// Intent ...
		std::string game_path = std::string(android::getSelectedGameFile());

		if(game_path == "")
		{
			// ... or fall back to the game at /sdcard/lovegame
			game_path = "/sdcard/lovegame/";
		}

		SDL_RWops *sdcard_main = SDL_RWFromFile(std::string(game_path + "main.lua").c_str(), "rb");

		if(sdcard_main)
		{
			new_search_path = game_path;
			sdcard_main->close(sdcard_main);

			if(!PHYSFS_mount(new_search_path.c_str(), nullptr, 1))
			{
				SDL_Log("mounting of %s failed", new_search_path.c_str());
				return false;
			}
		}
		else
		{
			// Neither assets/game.love or /sdcard/lovegame was mounted
			// sucessfully, therefore simply fail.
			return false;
		}
	}
#else
	// Add the directory.
	if(!PHYSFS_mount(new_search_path.c_str(), nullptr, 1))
		return false;
#endif

	// Save the game source.
    mGameSource = new_search_path;

	return true;
}

std::string TFileSystem::getSource() const
{
    return mGameSource;
}

bool TFileSystem::setupWriteDirectory()
{
	if(!PHYSFS_isInit())
		return false;

	// These must all be set.
    if(mSaveIdentity.empty() || mSaveFullPath.empty() || mSavePathRelative.empty())
		return false;

	// We need to make sure the write directory is created. To do that, we also
	// need to make sure all its parent directories are also created.
    std::string temp_writedir = getDriveRoot(mSaveFullPath);
    std::string temp_createdir = skipDriveRoot(mSaveFullPath);

	// On some sandboxed platforms, physfs will break when its write directory
	// is the root of the drive and it tries to create a folder (even if the
	// folder's path is in a writable location.) If the user's home folder is
	// in the save path, we'll try starting from there instead.
    if(mSaveFullPath.find(getUserDirectory()) == 0)
	{
		temp_writedir = getUserDirectory();
        temp_createdir = mSaveFullPath.substr(getUserDirectory().length());

		// Strip leading '/' characters from the path we want to create.
		size_t startpos = temp_createdir.find_first_not_of('/');
		if(startpos != std::string::npos)
			temp_createdir = temp_createdir.substr(startpos);
	}

	// Set either '/' or the user's home as a writable directory.
	// (We must create the save folder before mounting it).
	if(!PHYSFS_setWriteDir(temp_writedir.c_str()))
		return false;

	// Create the save folder. (We're now "at" either '/' or the user's home).
	if(!createDirectory(temp_createdir.c_str()))
	{
		// Clear the write directory in case of error.
		PHYSFS_setWriteDir(nullptr);
		return false;
	}

	// Set the final write directory.
    if(!PHYSFS_setWriteDir(mSaveFullPath.c_str()))
		return false;

	// Add the directory. (Will not be readded if already present).
    if(!PHYSFS_mount(mSaveFullPath.c_str(), nullptr, 0))
	{
		PHYSFS_setWriteDir(nullptr); // Clear the write directory in case of error.
		return false;
	}

	return true;
}

bool TFileSystem::mount(const char *archive, const char *mountpoint, bool appendToPath)
{
	if(!PHYSFS_isInit() || !archive)
		return false;

	std::string realPath;
	std::string sourceBase = getSourceBaseDirectory();

	// Check whether the given archive path is in the list of allowed full paths.
    auto it = std::find(mAllowedMountPaths.begin(), mAllowedMountPaths.end(), archive);

    if(it != mAllowedMountPaths.end())
		realPath = *it;
	else if(isFused() && sourceBase.compare(archive) == 0)
	{
		// Special case: if the game is fused and the archive is the source's
		// base directory, mount it even though it's outside of the save dir.
		realPath = sourceBase;
	}
	else
	{
		// Not allowed for safety reasons.
		if(strlen(archive) == 0 || strstr(archive, "..") || strcmp(archive, "/") == 0)
			return false;

		const char *realDir = PHYSFS_getRealDir(archive);
		if(!realDir)
			return false;

		realPath = realDir;

		// Always disallow mounting of files inside the game source, since it
		// won't work anyway if the game source is a zipped .love file.
        if(realPath.find(mGameSource) == 0)
			return false;

		realPath += THEALL_PATH_SEPARATOR;
		realPath += archive;
	}

	if(realPath.length() == 0)
		return false;

	return PHYSFS_mount(realPath.c_str(), mountpoint, appendToPath) != 0;
}

bool TFileSystem::unmount(const char *archive)
{
	if(!PHYSFS_isInit() || !archive)
		return false;

	std::string realPath;
	std::string sourceBase = getSourceBaseDirectory();

	// Check whether the given archive path is in the list of allowed full paths.
    auto it = std::find(mAllowedMountPaths.begin(), mAllowedMountPaths.end(), archive);

    if(it != mAllowedMountPaths.end())
		realPath = *it;
	else if(isFused() && sourceBase.compare(archive) == 0)
	{
		// Special case: if the game is fused and the archive is the source's
		// base directory, unmount it even though it's outside of the save dir.
		realPath = sourceBase;
	}
	else
	{
		// Not allowed for safety reasons.
		if(strlen(archive) == 0 || strstr(archive, "..") || strcmp(archive, "/") == 0)
			return false;

		const char *realDir = PHYSFS_getRealDir(archive);
		if(!realDir)
			return false;

		realPath = realDir;
		realPath += THEALL_PATH_SEPARATOR;
		realPath += archive;
	}

	const char *mountPoint = PHYSFS_getMountPoint(realPath.c_str());
	if(!mountPoint)
		return false;

#ifdef THEALL_USE_PHYSFS_2_1
	return PHYSFS_unmount(realPath.c_str()) != 0;
#else
	return PHYSFS_removeFromSearchPath(realPath.c_str()) != 0;
#endif
}

TFile *TFileSystem::newFile(const char *filename) const
{
    return new TFile(filename);
}

TFileData *TFileSystem::newFileData(void *data, unsigned int size, const char *filename) const
{
    TFileData *fd = new TFileData(size, std::string(filename));

	// Copy the data into FileData.
	memcpy(fd->getData(), data, size);

	return fd;
}

TFileData *TFileSystem::newFileData(const char *b64, const char *filename) const
{
	int size = (int) strlen(b64);
	int outsize = 0;
	char *dst = b64_decode(b64, size, outsize);
    TFileData *fd = new TFileData(outsize, std::string(filename));

	// Copy the data into FileData.
	memcpy(fd->getData(), dst, outsize);
	delete [] dst;

    return fd;
}

TFileData *TFileSystem::newFileData(const std::string &filename) const
{
    TFile file(filename);
    return file.read();
}

const char *TFileSystem::getWorkingDirectory()
{
    if(mCwd.empty())
	{
#ifdef THEALL_WINDOWS

		WCHAR w_cwd[THEALL_MAX_PATH];
		_wgetcwd(w_cwd, THEALL_MAX_PATH);
        mCwd = to_utf8(w_cwd);
        replace_char(mCwd, '\\', '/');
#else
		char *cwd_char = new char[THEALL_MAX_PATH];

		if(getcwd(cwd_char, THEALL_MAX_PATH))
			cwd = cwd_char; // if getcwd fails, cwd_char (and thus cwd) will still be empty

		delete [] cwd_char;
#endif
	}

    return mCwd.c_str();
}

std::string TFileSystem::getUserDirectory()
{
#ifdef THEALL_IOS
	// PHYSFS_getUserDir doesn't give exactly the path we want on iOS.
	static std::string userDir = normalize(ios::getHomeDirectory());
#else
    static std::string userDir = normalize(PHYSFS_getBaseDir());
#endif

	return userDir;
}

std::string TFileSystem::getAppdataDirectory()
{
    if(mAppData.empty())
	{
#ifdef THEALL_WINDOWS_UWP
		appdata = getUserDirectory();
#elif defined(THEALL_WINDOWS)
		wchar_t *w_appdata = _wgetenv(L"APPDATA");
        mAppData = to_utf8(w_appdata);
        replace_char(mAppData, '\\', '/');
#elif defined(THEALL_MACOSX)
		std::string udir = getUserDirectory();
		udir.append("/Library/Application Support");
		appdata = normalize(udir);
#elif defined(THEALL_IOS)
		appdata = normalize(ios::getAppdataDirectory());
#elif defined(THEALL_LINUX)
		char *xdgdatahome = getenv("XDG_DATA_HOME");
		if(!xdgdatahome)
			appdata = normalize(std::string(getUserDirectory()) + "/.local/share/");
		else
			appdata = xdgdatahome;
#else
		appdata = getUserDirectory();
#endif
	}
    return mAppData;
}


std::string TFileSystem::getSaveDirectory()
{
    return mSaveFullPath;
}

std::string TFileSystem::getSaveFileFullName(const std::string &fuleName)
{
    return adjustDir(mSaveFullPath) + fuleName;
}

std::string TFileSystem::getSourceBaseDirectory() const
{
    size_t source_len = mGameSource.length();

	if(source_len == 0)
		return "";

	// FIXME: This doesn't take into account parent and current directory
	// symbols (i.e. '..' and '.')
#ifdef THEALL_WINDOWS
	// In windows, delimiters can be either '/' or '\'.
    size_t base_end_pos = mGameSource.find_last_of("/\\", source_len - 2);
#else
	size_t base_end_pos = game_source.find_last_of('/', source_len - 2);
#endif

	if(base_end_pos == std::string::npos)
		return "";

	// If the source is in the unix root (aka '/'), we want to keep the '/'.
	if(base_end_pos == 0)
		base_end_pos = 1;

    return mGameSource.substr(0, base_end_pos);
}

std::string TFileSystem::getRealDirectory(const char *filename) const
{
	if(!PHYSFS_isInit())
		throw TException("PhysFS is not initialized.");

	const char *dir = PHYSFS_getRealDir(filename);

	if(dir == nullptr)
        throw TException("File does not exist.");

	return std::string(dir);
}

bool TFileSystem::exists(const char *path) const
{
	if(!PHYSFS_isInit())
		return false;

	return PHYSFS_exists(path) != 0;
}

bool TFileSystem::isDirectory(const char *dir) const
{
	if(!PHYSFS_isInit())
		return false;

#ifdef THEALL_USE_PHYSFS_2_1
    PHYSFS_Stat stat;
	if(PHYSFS_stat(dir, &stat))
		return stat.filetype == PHYSFS_FILETYPE_DIRECTORY;
	else
		return false;
#else
	return PHYSFS_isDirectory(dir) != 0;
#endif
}

bool TFileSystem::isFile(const char *file) const
{
	if(!PHYSFS_isInit())
		return false;

	return PHYSFS_exists(file) && !isDirectory(file);
}

bool TFileSystem::isSymlink(const char *filename) const
{
	if(!PHYSFS_isInit())
		return false;

#ifdef THEALL_USE_PHYSFS_2_1
    PHYSFS_Stat stat;
	if(PHYSFS_stat(filename, &stat))
		return stat.filetype == PHYSFS_FILETYPE_SYMLINK;
	else
		return false;
#else
	return PHYSFS_isSymbolicLink(filename) != 0;
#endif
}

bool TFileSystem::createDirectory(const char *dir)
{
	if(!PHYSFS_isInit())
		return false;

	if(PHYSFS_getWriteDir() == 0 && !setupWriteDirectory())
		return false;

    if(!PHYSFS_mkdir(dir)) {
        throw TException("Failed to make dir: %s", PHYSFS_getLastError());
		return false;
    }
	return true;
}

bool TFileSystem::remove(const char *file)
{
	if(!PHYSFS_isInit())
		return false;

	if(PHYSFS_getWriteDir() == 0 && !setupWriteDirectory())
		return false;

	if(!PHYSFS_delete(file))
		return false;

	return true;
}

TFileData *TFileSystem::read(const char *filename, int64 size) const
{
    TFile file(filename);

    file.open(TAbstractFile::MODE_READ);

    // close() is called in the TFile destructor.
	return file.read(size);
}

void TFileSystem::write(const char *filename, const void *data, int64 size) const
{
    TFile file(filename);

    file.open(TAbstractFile::MODE_WRITE);

    // close() is called in the TFile destructor.
	if(!file.write(data, size))
        throw TException("Data could not be written.");
}

void TFileSystem::write(TFileData *fileData) const
{
    if(!fileData)
        throw TException("Invalid file data.");

    write(fileData->getFilename().c_str(), fileData->getData(), fileData->getSize());
}

void TFileSystem::append(const char *filename, const void *data, int64 size) const
{
    TFile file(filename);

    file.open(TAbstractFile::MODE_APPEND);

    // close() is called in the TFile destructor.
	if(!file.write(data, size))
		throw TException("Data could not be written.");
}

void TFileSystem::getDirectoryItems(const char *dir, std::vector<std::string> &items)
{
	if(!PHYSFS_isInit())
		return;

	char **rc = PHYSFS_enumerateFiles(dir);

	if(rc == nullptr)
		return;

	for (char **i = rc; *i != 0; i++)
		items.push_back(*i);

	PHYSFS_freeList(rc);
}

int64 TFileSystem::getLastModified(const char *filename) const
{
	PHYSFS_sint64 time = -1;

	if(!PHYSFS_isInit())
		return -1;

#ifdef THEALL_USE_PHYSFS_2_1
    PHYSFS_Stat stat;
	if(PHYSFS_stat(filename, &stat))
		time = stat.modtime;
#else
	time = PHYSFS_getLastModTime(filename);
#endif

	if(time == -1)
		throw TException("Could not determine file modification date.");

	return time;
}

int64 TFileSystem::getSize(const char *filename) const
{
    TFile file(filename);
	int64 size = file.getSize();
	return size;
}

void TFileSystem::setSymlinksEnabled(bool enable)
{
	if(!PHYSFS_isInit())
		return;

	if(!enable)
	{
        PHYSFS_Version version;
		PHYSFS_getLinkedVersion(&version);

		// FIXME: This is a workaround for a bug in PHYSFS_enumerateFiles in
		// PhysFS 2.1-alpha.
		if(version.major == 2 && version.minor == 1)
			return;
	}

	PHYSFS_permitSymbolicLinks(enable ? 1 : 0);
}

bool TFileSystem::areSymlinksEnabled() const
{
	if(!PHYSFS_isInit())
		return false;

	return PHYSFS_symbolicLinksPermitted() != 0;
}

std::vector<std::string> &TFileSystem::getRequirePath()
{
    return mRequirePath;
}

void TFileSystem::allowMountingForPath(const std::string &path)
{
    if(std::find(mAllowedMountPaths.begin(), mAllowedMountPaths.end(), path) == mAllowedMountPaths.end())
        mAllowedMountPaths.push_back(path);
}

std::string TFileSystem::getSaveFullPath() const
{
    return mSaveFullPath;
}

void TFileSystem::setSaveFullPath(const std::string &saveFullPath)
{
    mSaveFullPath = saveFullPath;
    PHYSFS_setWriteDir(mSaveFullPath.c_str());
}

std::string TFileSystem::getCurrentDirectory() const
{
    char buf[MAX_PATH];
    memset(buf, 0, MAX_PATH);
    getcwd(buf, MAX_PATH);
    std::string ret(buf);
    replace_char(ret, '\\', '/');
    return ret;
}



