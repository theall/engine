/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_FILESYSTEM_FILE_DATA_H
#define THEALL_FILESYSTEM_FILE_DATA_H

#include <string>
#include "common/data.h"
#include "common/stringmap.h"
#include "common/int.h"

class TFileData : public TData
{
public:

	enum TDecoder
	{
		FILE,
		BASE64,
		DECODE_MAX_ENUM
	}; // TDecoder

    TFileData(uint64 mSize, const std::string &filename);

    virtual ~TFileData();

	// Implements Data.
	void *getData() const;
	size_t getSize() const;

	const std::string &getFilename() const;
	const std::string &getExtension() const;

	static bool getConstant(const char *in, TDecoder &out);
	static bool getConstant(TDecoder in, const char *&out);

private:

	// The actual data.
    char *mData;

	// Size of the data.
    uint64 mSize;

	// The filename used for error purposes.
    std::string mFilename;

	// The extension (without dot). Used to identify file type.
    std::string mExtension;

    static TStringMap<TDecoder, DECODE_MAX_ENUM>::Entry mDecoderEntries[];
    static TStringMap<TDecoder, DECODE_MAX_ENUM> mDecoders;

}; // FileData

#endif // THEALL_FILESYSTEM_FILE_DATA_H
