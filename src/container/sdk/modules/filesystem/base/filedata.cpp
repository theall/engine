/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "filedata.h"

// C++
#include <iostream>
#include <limits>

TFileData::TFileData(uint64 size, const std::string &filename)
    : mData(nullptr)
    , mSize((size_t) size)
    , mFilename(filename)
{
	try
	{
        mData = new char[(size_t) size];
	}
	catch (std::bad_alloc &)
	{
		throw TException("Out of memory.");
	}

	if(filename.rfind('.') != std::string::npos)
        mExtension = filename.substr(filename.rfind('.')+1);
}

TFileData::~TFileData()
{
    delete [] mData;
}

void *TFileData::getData() const
{
    return (void *)mData;
}

size_t TFileData::getSize() const
{
	size_t sizemax = std::numeric_limits<size_t>::max();
    return mSize > sizemax ? sizemax : (size_t) mSize;
}

const std::string &TFileData::getFilename() const
{
    return mFilename;
}

const std::string &TFileData::getExtension() const
{
    return mExtension;
}

bool TFileData::getConstant(const char *in, TDecoder &out)
{
    return mDecoders.find(in, out);
}

bool TFileData::getConstant(TDecoder in, const char *&out)
{
    return mDecoders.find(in, out);
}

TStringMap<TFileData::TDecoder, TFileData::DECODE_MAX_ENUM>::Entry TFileData::mDecoderEntries[] =
{
    {"file", TFileData::FILE},
    {"base64", TFileData::BASE64},
};

TStringMap<TFileData::TDecoder, TFileData::DECODE_MAX_ENUM> TFileData::mDecoders(TFileData::mDecoderEntries, sizeof(TFileData::mDecoderEntries));
