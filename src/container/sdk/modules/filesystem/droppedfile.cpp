/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "droppedfile.h"
#include "common/utf8.h"

// Assume POSIX or Visual Studio.
#include <sys/types.h>
#include <sys/stat.h>

#ifdef THEALL_WINDOWS
#include <wchar.h>
#else
#include <unistd.h> // POSIX.
#endif

TDroppedFile::TDroppedFile(const std::string &filename)
    : mFilename(filename)
    , mFile(nullptr)
    , mMode(MODE_CLOSED)
    , mBufferMode(BUFFER_NONE)
    , mBufferSize(0)
{
}

TDroppedFile::~TDroppedFile()
{
    if(mMode != MODE_CLOSED)
		close();
}

bool TDroppedFile::open(Mode newmode)
{
	if(newmode == MODE_CLOSED)
		return true;

	// File already open?
    if(mFile != nullptr)
		return false;

#ifdef THEALL_WINDOWS
	// make sure non-ASCII filenames work.
	std::wstring modestr = to_widestr(getModeString(newmode));
    std::wstring wfilename = to_widestr(mFilename);

    mFile = _wfopen(wfilename.c_str(), modestr.c_str());
#else
	file = fopen(filename.c_str(), getModeString(newmode));
#endif

    if(newmode == MODE_READ && mFile == nullptr)
        throw TException("Could not open file %s. Does not exist.", mFilename.c_str());

    mMode = newmode;

    if(mFile != nullptr && !setBuffer(mBufferMode, mBufferSize))
	{
		// Revert to buffer defaults if we don't successfully set the buffer.
        mBufferMode = BUFFER_NONE;
        mBufferSize = 0;
	}

    return mFile != nullptr;
}

bool TDroppedFile::close()
{
    if(mFile == nullptr || fclose(mFile) != 0)
		return false;

    mMode = MODE_CLOSED;
    mFile = nullptr;

	return true;
}

bool TDroppedFile::isOpen() const
{
    return mMode != MODE_CLOSED && mFile != nullptr;
}

int64 TDroppedFile::getSize()
{
#ifdef THEALL_WINDOWS

	// make sure non-ASCII filenames work.
    std::wstring wfilename = to_widestr(mFilename);

	struct _stat buf;
	if(_wstat(wfilename.c_str(), &buf) != 0)
		return -1;

	return (int64) buf.st_size;

#else

	// Assume POSIX support...
	struct stat buf;
	if(stat(filename.c_str(), &buf) != 0)
		return -1;

	return (int64) buf.st_size;

#endif
}

int64 TDroppedFile::read(void *dst, int64 size)
{
    if(!mFile || mMode != MODE_READ)
		throw TException("File is not opened for reading.");

	if(size < 0)
		throw TException("Invalid read size.");

    size_t read = fread(dst, 1, (size_t) size, mFile);

	return (int64) read;
}

bool TDroppedFile::write(const void *data, int64 size)
{
    if(!mFile || (mMode != MODE_WRITE && mMode != MODE_APPEND))
		throw TException("File is not opened for writing.");

	if(size < 0)
		throw TException("Invalid write size.");

    int64 written = (int64) fwrite(data, 1, (size_t) size, mFile);

	return written == size;
}

bool TDroppedFile::flush()
{
    if(!mFile || (mMode != MODE_WRITE && mMode != MODE_APPEND))
		throw TException("File is not opened for writing.");

    return fflush(mFile) == 0;
}

bool TDroppedFile::isEOF()
{
    return mFile == nullptr || feof(mFile) != 0;
}

int64 TDroppedFile::tell()
{
    if(mFile == nullptr)
		return -1;

    return (int64) ftell(mFile);
}

bool TDroppedFile::seek(uint64 pos)
{
    return mFile != nullptr && fseek(mFile, (long) pos, SEEK_SET) == 0;
}

bool TDroppedFile::setBuffer(BufferMode bufmode, int64 size)
{
	if(size < 0)
		return false;

	if(bufmode == BUFFER_NONE)
		size = 0;

	// If the file isn't open, we'll make sure the buffer values are set in
	// DroppedFile::open.
	if(!isOpen())
	{
        mBufferMode = bufmode;
        mBufferSize = size;
		return true;
	}

	int vbufmode;
	switch (bufmode)
	{
    case TAbstractFile::BUFFER_NONE:
	default:
		vbufmode = _IONBF;
		break;
    case TAbstractFile::BUFFER_LINE:
		vbufmode = _IOLBF;
		break;
    case TAbstractFile::BUFFER_FULL:
		vbufmode = _IOFBF;
		break;
	}

    if(setvbuf(mFile, nullptr, vbufmode, (size_t) size) != 0)
		return false;

    mBufferMode = bufmode;
    mBufferSize = size;

	return true;
}

TAbstractFile::BufferMode TDroppedFile::getBuffer(int64 &size) const
{
    size = mBufferSize;
    return mBufferMode;
}

const std::string &TDroppedFile::getFilename() const
{
    return mFilename;
}

TAbstractFile::Mode TDroppedFile::getMode() const
{
    return mMode;
}

const char *TDroppedFile::getModeString(Mode mode)
{
	switch (mode)
	{
    case TAbstractFile::MODE_CLOSED:
	default:
		return "c";
    case TAbstractFile::MODE_READ:
		return "rb";
    case TAbstractFile::MODE_WRITE:
		return "wb";
    case TAbstractFile::MODE_APPEND:
		return "ab";
	}
}


