/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "imagedata.h"

TImageData::TImageData(const TImageData &imageData) :
    mDecodeHandler(nullptr)
{
    for (TFormatHandler *handler : mFormatHandlers)
        handler->retain();

    mWidth = imageData.getWidth();
    mHeight = imageData.getHeight();
    create(mWidth, mHeight, imageData.getData());
}

TImageData::TImageData(std::list<TFormatHandler *> formats, TFileData *data)
	: mFormatHandlers(formats)
	, mDecodeHandler(nullptr)
{
	for (TFormatHandler *handler : mFormatHandlers)
		handler->retain();

	decode(data);
}

TImageData::TImageData(std::list<TFormatHandler *> formats, int width, int height)
	: mFormatHandlers(formats)
	, mDecodeHandler(nullptr)
{
	for (TFormatHandler *handler : mFormatHandlers)
		handler->retain();

    mWidth = width;
    mHeight = height;

	create(width, height);

	// Set to black/transparency.
    memset(mData, 0, width*height*sizeof(TPixel));
}

TImageData::TImageData(std::list<TFormatHandler *> formats, int width, int height, void *data, bool own)
	: mFormatHandlers(formats)
	, mDecodeHandler(nullptr)
{
	for (TFormatHandler *handler : mFormatHandlers)
		handler->retain();

    mWidth = width;
    mHeight = height;

	if(own)
        mData = (unsigned char *) data;
	else
		create(width, height, data);
}

TImageData::~TImageData()
{
	if(mDecodeHandler)
        mDecodeHandler->free(mData);
	else
        delete[] mData;

	for (TFormatHandler *handler : mFormatHandlers)
		handler->release();
}

void TImageData::create(int width, int height, void *data)
{
	try
	{
        mData = new unsigned char[width*height*sizeof(TPixel)];
	}
	catch(std::bad_alloc &)
	{
		throw TException("Out of memory");
	}

	if(data)
        memcpy(mData, data, width*height*sizeof(TPixel));

	mDecodeHandler = nullptr;
}

void TImageData::decode(TFileData *data)
{
	TFormatHandler *decoder = nullptr;
	TFormatHandler::DecodedImage decodedimage;

	for (TFormatHandler *handler : mFormatHandlers)
	{
		if(handler->canDecode(data))
		{
			decoder = handler;
			break;
		}
	}

	if(decoder)
		decodedimage = decoder->decode(data);

	if(decodedimage.data == nullptr)
	{
		const std::string &name = data->getFilename();
		throw TException("Could not decode file '%s' to TImageData: unsupported file format", name.c_str());
	}

	// The decoder *must* output a 32 bits-per-pixel image.
    if(decodedimage.size != decodedimage.width*decodedimage.height*sizeof(TPixel))
	{
		decoder->free(decodedimage.data);
		throw TException("Could not convert image!");
	}

	// Clean up any old data.
	if(mDecodeHandler)
        mDecodeHandler->free(mData);
	else
        delete[] mData;

    mWidth = decodedimage.width;
    mHeight = decodedimage.height;
    mData = decodedimage.data;

	mDecodeHandler = decoder;
}

TFileData *TImageData::encode(EncodedFormat format, const char *filename)
{
	TFormatHandler *encoder = nullptr;
	TFormatHandler::EncodedImage encodedimage;
	TFormatHandler::DecodedImage rawimage;

    rawimage.width = mWidth;
    rawimage.height = mHeight;
    rawimage.size = mWidth*mHeight*sizeof(TPixel);
    rawimage.data = mData;

	for (TFormatHandler *handler : mFormatHandlers)
	{
		if(handler->canEncode(format))
		{
			encoder = handler;
			break;
		}
	}

	if(encoder != nullptr)
	{
        TLock lock(mMutex);
		encodedimage = encoder->encode(rawimage, format);
	}

	if(encoder == nullptr || encodedimage.data == nullptr)
	{
		const char *fname = "unknown";
		getConstant(format, fname);
		throw TException("No suitable image encoder for %s format.", fname);
	}

	TFileData *filedata = nullptr;

	try
	{
		filedata = new TFileData(encodedimage.size, filename);
	}
	catch (TException &)
	{
		encoder->free(encodedimage.data);
		throw;
	}

	memcpy(filedata->getData(), encodedimage.data, encodedimage.size);
	encoder->free(encodedimage.data);

    return filedata;
}
