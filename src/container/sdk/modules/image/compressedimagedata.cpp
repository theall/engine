/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "compressedimagedata.h"

#include "handler/compressedformathandler.h"

TCompressedImageData::TCompressedImageData(std::list<TCompressedFormatHandler *> formats, TFileData *filedata) :
    TCompressedImageDataBase()
{
    TCompressedFormatHandler *parser = nullptr;

    for (TCompressedFormatHandler *handler : formats)
	{
		if(handler->canParse(filedata))
		{
			parser = handler;
			break;
		}
	}

	if(parser == nullptr)
		throw TException("Could not parse compressed data: Unknown format.");

    mData = parser->parse(filedata, mDataImages, mDataSize, mFormat, mIsRGB);

    if(mData == nullptr)
		throw TException("Could not parse compressed data.");

    if(mFormat == FORMAT_UNKNOWN)
	{
        delete[] mData;
		throw TException("Could not parse compressed data: Unknown format.");
	}

    if(mDataImages.size() == 0 || mDataSize == 0)
	{
        delete[] mData;
		throw TException("Could not parse compressed data: No valid data?");
	}
}

TCompressedImageData::~TCompressedImageData()
{
    delete[] mData;
}



