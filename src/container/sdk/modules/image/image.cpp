/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "common/config.h"

#include "image.h"

#include "imagedata.h"
#include "compressedimagedata.h"

#include "handler/pnghandler.h"
#include "handler/stbhandler.h"
#include "handler/ddshandler.h"
#include "handler/pvrhandler.h"
#include "handler/ktxhandler.h"
#include "handler/pkmhandler.h"
#include "handler/astchandler.h"

TImage *TImage::mInstance=NULL;

TImage::TImage()
{
    mFormatHandlers = {
		new PNGHandler,
		new STBHandler,
	};

    mCompressedFormatHandlers = {
		new DDSHandler,
		new PVRHandler,
		new KTXHandler,
		new PKMHandler,
		new ASTCHandler,
	};
}

TImage::~TImage()
{
	// TImageData objects reference the FormatHandlers in our list, so we should
	// release them instead of deleting them completely here.
    for (TFormatHandler *handler : mFormatHandlers)
		handler->release();

    for (TCompressedFormatHandler *handler : mCompressedFormatHandlers)
		handler->release();
}

const char *TImage::getName() const
{
	return "theall.image.magpie";
}

TImageData *TImage::newImageData(TFileData *data)
{
    return new TImageData(mFormatHandlers, data);
}

TImageData *TImage::newImageData(int width, int height)
{
    return new TImageData(mFormatHandlers, width, height);
}

TImageData *TImage::newImageData(int width, int height, void *data, bool own)
{
    return new TImageData(mFormatHandlers, width, height, data, own);
}

TCompressedImageData *TImage::newCompressedData(TFileData *data)
{
    return new TCompressedImageData(mCompressedFormatHandlers, data);
}

bool TImage::isCompressed(TFileData *data)
{
    for (TCompressedFormatHandler *handler : mCompressedFormatHandlers)
	{
		if(handler->canParse(data))
			return true;
	}

	return false;
}



