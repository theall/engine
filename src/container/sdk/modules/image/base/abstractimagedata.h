/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_IMAGE_IMAGE_DATA_H
#define THEALL_IMAGE_IMAGE_DATA_H

#include "common/data.h"
#include "modules/filesystem/base/filedata.h"
#include "modules/thread/thread.h"

// Pixel format structure.
struct TPixel
{
	// Red, green, blue, alpha.
	unsigned char r, g, b, a;
};

/**
 * Represents raw pixel data.
 **/
class TAbstractImageData : public TData
{
public:

	enum EncodedFormat
	{
		ENCODED_TGA,
		ENCODED_PNG,
		ENCODED_MAX_ENUM
	};

    TAbstractImageData();
    virtual ~TAbstractImageData();

	/**
     * Paste part of one TAbstractImageData onto another. The subregion defined by the top-left
     * corner (sx, sy) and the size (sw,sh) will be pasted to (dx,dy) in this TAbstractImageData.
	 * @param dx The destination x-coordinate.
	 * @param dy The destination y-coordinate.
	 * @param sx The source x-coordinate.
	 * @param sy The source y-coordinate.
	 * @param sw The source width.
	 * @param sh The source height.
	 **/
    void paste(TAbstractImageData *src, int dx, int dy, int sx, int sy, int sw, int sh);

    /**
     * Flip current imagedata in horizontal direction
     **/
    void flipHorizontal();

	/**
     * Checks whether a position is inside this TAbstractImageData. Useful for checking bounds.
	 * @param x The position along the x-axis.
	 * @param y The position along the y-axis.
	 **/
	bool inside(int x, int y) const;

	/**
     * Gets the width of this TAbstractImageData.
     * @return The width of this TAbstractImageData.
	 **/
	int getWidth() const;

	/**
     * Gets the height of this TAbstractImageData.
     * @return The height of this TAbstractImageData.
	 **/
	int getHeight() const;

	/**
	 * Sets the pixel at location (x,y).
	 * @param x The location along the x-axis.
	 * @param y The location along the y-axis.
	 * @param p The color to use for the given location.
	 **/
    void setPixel(int x, int y, TPixel p);

	/**
	 * Sets the pixel at location (x,y).
	 * Not thread-safe, and doesn't verify the coordinates!
	 **/
    void setPixelUnsafe(int x, int y, TPixel p);

	/**
	 * Gets the pixel at location (x,y).
	 * @param x The location along the x-axis.
	 * @param y The location along the y-axis.
	 * @return The color for the given location.
	 **/
    TPixel getPixel(int x, int y) const;

	/**
	 * Gets the pixel at location (x,y).
	 * Not thread-safe, and doesn't verify the coordinates!
	 **/
    TPixel getPixelUnsafe(int x, int y) const;

	/**
	 * Encodes raw pixel data into a given format.
	 * @param f The file to save the encoded image data to.
	 * @param format The format of the encoded data.
	 **/
	virtual TFileData *encode(EncodedFormat format, const char *filename) = 0;

    TMutex *getMutex() const;

	// Implements Data.
	virtual void *getData() const;
	virtual size_t getSize() const;

	static bool getConstant(const char *in, EncodedFormat &out);
	static bool getConstant(EncodedFormat in, const char *&out);

protected:

	// The width of the image data.
    int mWidth;

	// The height of the image data.
    int mHeight;

	// The actual data.
    unsigned char *mData;

	// We need to be thread-safe
	// so we lock when we're accessing our
	// data
    TMutexRef mMutex;

private:

    static TStringMap<EncodedFormat, ENCODED_MAX_ENUM>::Entry encodedFormatEntries[];
    static TStringMap<EncodedFormat, ENCODED_MAX_ENUM> encodedFormats;

}; // TAbstractImageData

#endif // THEALL_IMAGE_IMAGE_DATA_H
