/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "abstractimagedata.h"

TAbstractImageData::TAbstractImageData()
	: mData(nullptr)
{
}

TAbstractImageData::~TAbstractImageData()
{
}

size_t TAbstractImageData::getSize() const
{
    return size_t(getWidth()*getHeight())*sizeof(TPixel);
}

void *TAbstractImageData::getData() const
{
	return mData;
}

bool TAbstractImageData::inside(int x, int y) const
{
	return x >= 0 && x < getWidth() && y >= 0 && y < getHeight();
}

int TAbstractImageData::getWidth() const
{
	return mWidth;
}

int TAbstractImageData::getHeight() const
{
	return mHeight;
}

void TAbstractImageData::setPixel(int x, int y, TPixel c)
{
	if(!inside(x, y))
		throw TException("Attempt to set out-of-range pixel!");

    TLock lock(mMutex);

    TPixel *pixels = (TPixel *) getData();
	pixels[y*mWidth+x] = c;
}

void TAbstractImageData::setPixelUnsafe(int x, int y, TPixel c)
{
    TPixel *pixels = (TPixel *) getData();
	pixels[y*mWidth+x] = c;
}

TPixel TAbstractImageData::getPixel(int x, int y) const
{
	if(!inside(x, y))
		throw TException("Attempt to get out-of-range pixel!");

    TLock lock(mMutex);

    const TPixel *pixels = (const TPixel *) getData();
	return pixels[y*mWidth+x];
}

TPixel TAbstractImageData::getPixelUnsafe(int x, int y) const
{
    const TPixel *pixels = (const TPixel *) getData();
	return pixels[y*mWidth+x];
}

void TAbstractImageData::paste(TAbstractImageData *src, int dx, int dy, int sx, int sy, int sw, int sh)
{
    TLock lock2(src->mMutex);
    TLock lock1(mMutex);

    TPixel *s = (TPixel *)src->getData();
    TPixel *d = (TPixel *)getData();

	// Check bounds; if the data ends up completely out of bounds, get out early.
	if(sx >= src->getWidth() || sx + sw < 0 || sy >= src->getHeight() || sy + sh < 0
			|| dx >= getWidth() || dx + sw < 0 || dy >= getHeight() || dy + sh < 0)
		return;

	// Normalize values to the inside of both images.
	if(dx < 0)
	{
		sw += dx;
		sx -= dx;
		dx = 0;
	}
	if(dy < 0)
	{
		sh += dy;
		sy -= dy;
		dy = 0;
	}
	if(sx < 0)
	{
		sw += sx;
		dx -= sx;
		sx = 0;
	}
	if(sy < 0)
	{
		sh += sy;
		dy -= sy;
		sy = 0;
	}

	if(dx + sw > getWidth())
		sw = getWidth() - dx;

	if(dy + sh > getHeight())
		sh = getHeight() - dy;

	if(sx + sw > src->getWidth())
		sw = src->getWidth() - sx;

	if(sy + sh > src->getHeight())
		sh = src->getHeight() - sy;

	// If the dimensions match up, copy the entire memory stream in one go
	if(sw == getWidth() && getWidth() == src->getWidth()
		&& sh == getHeight() && getHeight() == src->getHeight())
	{
        memcpy(d, s, sizeof(TPixel) * sw * sh);
	}
	else if(sw > 0)
	{
		// Otherwise, copy each row individually.
		for (int i = 0; i < sh; i++)
            memcpy(d + dx + (i + dy) * getWidth(), s + sx + (i + sy) * src->getWidth(), sizeof(TPixel) * sw);
    }
}

void TAbstractImageData::flipHorizontal()
{
    TLock lock(mMutex);
    TPixel *d = (TPixel *)getData();
    if(!d || mWidth<1 || mHeight<1)
        return;

    int halfWidth = mWidth / 2;
    for(int y=0;y<mHeight;y++)
    {
        for(int x=0;x<halfWidth;x++)
        {
            TPixel *from = d + y * mWidth + x;
            TPixel *to = d + (y+1) * mWidth - x - 1;
            TPixel tmp = *from;
            *from = *to;
            *to = tmp;
        }
    }
}

TMutex *TAbstractImageData::getMutex() const
{
	return mMutex;
}

bool TAbstractImageData::getConstant(const char *in, EncodedFormat &out)
{
	return encodedFormats.find(in, out);
}

bool TAbstractImageData::getConstant(EncodedFormat in, const char *&out)
{
	return encodedFormats.find(in, out);
}

TStringMap<TAbstractImageData::EncodedFormat, TAbstractImageData::ENCODED_MAX_ENUM>::Entry TAbstractImageData::encodedFormatEntries[] =
{
	{"tga", ENCODED_TGA},
	{"png", ENCODED_PNG},
};

TStringMap<TAbstractImageData::EncodedFormat, TAbstractImageData::ENCODED_MAX_ENUM> TAbstractImageData::encodedFormats(TAbstractImageData::encodedFormatEntries, sizeof(TAbstractImageData::encodedFormatEntries));
