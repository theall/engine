/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

// STL
#include <iostream>

#include "videostream.h"

TVideoStream::TVideoStream(TFile *file)
    : mFile(file)
    , mHeaderParsed(false)
    , mStreamInited(false)
    , mVideoSerial(0)
    , mDecoder(nullptr)
    , mFrameReady(false)
    , mLastFrame(0)
    , mNextFrame(0)
    , mEos(false)
    , mLagCounter(0)
{
    ogg_sync_init(&mSync);
    th_info_init(&mVideoInfo);

    mFrontBuffer = new Frame();
    mBackBuffer = new Frame();

	try
	{
		parseHeader();
	}
	catch (TException &ex)
	{
        delete mBackBuffer;
        delete mFrontBuffer;
        th_info_clear(&mVideoInfo);
        ogg_sync_clear(&mSync);
		throw ex;
	}

	mFrameSync.set(new DeltaSync(), Acquire::NORETAIN);
}

TVideoStream::~TVideoStream()
{
    if(mDecoder)
        th_decode_free(mDecoder);

    th_info_clear(&mVideoInfo);
    if(mHeaderParsed)
        ogg_stream_clear(&mStream);

    ogg_sync_clear(&mSync);

    delete mFrontBuffer;
    delete mBackBuffer;
}

int TVideoStream::getWidth() const
{
    if(mHeaderParsed)
        return mVideoInfo.pic_width;
	else
		return 0;
}

int TVideoStream::getHeight() const
{
    if(mHeaderParsed)
        return mVideoInfo.pic_height;
	else
		return 0;
}

const std::string &TVideoStream::getFilename() const
{
    return mFile->getFilename();
}

void TVideoStream::setSync(FrameSync *frameSync)
{
    TLock l(mBufferMutex);
    mFrameSync = frameSync;
}

const void *TVideoStream::getFrontBuffer() const
{
    return mFrontBuffer;
}

size_t TVideoStream::getSize() const
{
	return sizeof(Frame);
}

bool TVideoStream::isPlaying() const
{
    return mFrameSync->isPlaying() && !mEos;
}

void TVideoStream::readPage()
{
	char *syncBuffer = nullptr;
    while (ogg_sync_pageout(&mSync, &mPage) != 1)
	{
        if(syncBuffer && !mHeaderParsed && ogg_stream_check(&mStream))
			throw TException("Invalid stream");

        syncBuffer = ogg_sync_buffer(&mSync, 8192);
        size_t read = mFile->read(syncBuffer, 8192);
        ogg_sync_wrote(&mSync, read);
	}
}

bool TVideoStream::readPacket(bool mustSucceed)
{
    if(!mStreamInited)
	{
		readPage();
        mVideoSerial = ogg_page_serialno(&mPage);
        ogg_stream_init(&mStream, mVideoSerial);
        mStreamInited = true;
        ogg_stream_pagein(&mStream, &mPage);
	}

    while (ogg_stream_packetout(&mStream, &mPacket) != 1)
	{
		do
		{
			// We need to read another page, but there is none, we're at the end
            if(ogg_page_eos(&mPage) && !mustSucceed)
                return mEos = true;

			readPage();
        } while (ogg_page_serialno(&mPage) != mVideoSerial);

        ogg_stream_pagein(&mStream, &mPage);
	}

	return false;
}

template<typename T>
inline void scaleFormat(th_pixel_fmt fmt, T &x, T &y)
{
	switch(fmt)
	{
	case TH_PF_420:
		y /= 2;
	case TH_PF_422:
		x /= 2;
		break;
	default:
		break;
	}
}

void TVideoStream::parseHeader()
{
    if(mHeaderParsed)
		return;

	th_comment comment;
	th_setup_info *setupInfo = nullptr;
	th_comment_init(&comment);
	int ret;

	do
	{
		readPacket();
        ret = th_decode_headerin(&mVideoInfo, &comment, &setupInfo, &mPacket);
		if(ret == TH_ENOTFORMAT)
		{
            ogg_stream_clear(&mStream);
            mStreamInited = false;
		}
    } while(ret < 0 && !ogg_page_eos(&mPage));

	if(ret < 0)
	{
		th_comment_clear(&comment);
		throw TException("Could not find header");
	}

	while (ret > 0)
	{
		readPacket();
        ret = th_decode_headerin(&mVideoInfo, &comment, &setupInfo, &mPacket);
	}

	th_comment_clear(&comment);

    mDecoder = th_decode_alloc(&mVideoInfo, setupInfo);
	th_setup_free(setupInfo);

    Frame *buffers[2] = {mBackBuffer, mFrontBuffer};

    mYPlaneXOffset = mCPlaneXOffset = mVideoInfo.pic_x;
    mYPlaneYOffset = mCPlaneYOffset = mVideoInfo.pic_y;

    scaleFormat(mVideoInfo.pixel_fmt, mCPlaneXOffset, mCPlaneYOffset);

	for (int i = 0; i < 2; i++)
	{
        buffers[i]->cw = buffers[i]->yw = mVideoInfo.pic_width;
        buffers[i]->ch = buffers[i]->yh = mVideoInfo.pic_height;

        scaleFormat(mVideoInfo.pixel_fmt, buffers[i]->cw, buffers[i]->ch);

		buffers[i]->yplane = new unsigned char[buffers[i]->yw * buffers[i]->yh];
		buffers[i]->cbplane = new unsigned char[buffers[i]->cw * buffers[i]->ch];
		buffers[i]->crplane = new unsigned char[buffers[i]->cw * buffers[i]->ch];

		memset(buffers[i]->yplane, 16, buffers[i]->yw * buffers[i]->yh);
		memset(buffers[i]->cbplane, 128, buffers[i]->cw * buffers[i]->ch);
		memset(buffers[i]->crplane, 128, buffers[i]->cw * buffers[i]->ch);
	}

    mHeaderParsed = true;
    th_decode_packetin(mDecoder, &mPacket, nullptr);
}

// Arbitrary seeking isn't supported yet, but rewinding is
void TVideoStream::rewind()
{
	// Seek our data stream back to the start
    mFile->seek(0);

	// Break our sync, and discard the rest of the page
    ogg_sync_reset(&mSync);
    ogg_sync_pageseek(&mSync, &mPage);

	// Read our first page/packet from the stream again
	readPacket(true);

	// Now tell theora we're at frame 1 (not 0!)
	int64 granPos = 1;
    th_decode_ctl(mDecoder, TH_DECCTL_SET_GRANPOS, &granPos, sizeof(granPos));

	// Force a redraw, since this will always be less than the sync's position
    mLastFrame = mNextFrame = -1;
    mEos = false;
}

void TVideoStream::seekDecoder(double target)
{
	const double rewindThreshold = 0.01;
    //const double seekThreshold = 0.0001;

	if(target < rewindThreshold)
	{
		rewind();
		return;
	}

	double low = 0;
    double high = mFile->getSize();

	while (high-low > rewindThreshold)
	{
		// Determine our next binary search position
		double pos = (high+low)/2;
        mFile->seek(pos);

		// Break sync
        ogg_sync_reset(&mSync);
        ogg_sync_pageseek(&mSync, &mPage);

		// Read a page
		readPacket(false);
        if(mEos)
		{
			// EOS, so we're definitely past our target (or the target is past
			// the end)
			high = pos;
            mEos = false;

			// And a workaround for single-page files:
			if(high < rewindThreshold)
				rewind();
			else
				continue;
		}

		// Now search all packets in this page
		int result = -1;
        for (int i = 0; i < ogg_page_packets(&mPage); ++i)
		{
			if(i > 0)
				readPacket(true);

			// Determine if this is the right place
            double curTime = th_granule_time(mDecoder, mPacket.granulepos);
            double nextTime = th_granule_time(mDecoder, mPacket.granulepos+1);

			if(curTime == -1)
				continue; // Invalid granule position (magic?)
			else if(curTime <= target && nextTime > target)
			{
				// the current frame should be displaying right now
				result = 0;
				break;
			}
			else if(curTime > target)
			{
				// No need to check the other packets, they're all past
				// this one
				result = 1;
				break;
			}
		}

		// The sign of result determines the direction
		if(result == 0)
			break;
		else if(result < 0)
			low = pos;
		else
			high = pos;
	}

	// Now update theora and our decoder on this new position of ours
    mLastFrame = mNextFrame = -1;
    mEos = false;
    th_decode_ctl(mDecoder, TH_DECCTL_SET_GRANPOS, &mPacket.granulepos, sizeof(mPacket.granulepos));
}

void TVideoStream::threadedFillBackBuffer(double dt)
{
	// Synchronize
	mFrameSync->update(dt);
	double position = mFrameSync->getPosition();

	// Seeking backwards
    if(position < mLastFrame)
		seekDecoder(position);

	// If we're at the end of the stream, or if we're displaying the right frame
	// stop here
    if(mEos || position < mNextFrame)
		return;

	th_ycbcr_buffer bufferinfo;
    th_decode_ycbcr_out(mDecoder, bufferinfo);

	ogg_int64_t granulePosition;
	do
	{
		if(readPacket())
			return;
    } while (th_decode_packetin(mDecoder, &mPacket, &granulePosition) != 0);
    mLastFrame = mNextFrame;
    mNextFrame = th_granule_time(mDecoder, granulePosition);

	{
		// Don't swap whilst we're writing to the backbuffer
        TLock l(mBufferMutex);
        mFrameReady = false;
	}

    for (int y = 0; y < mBackBuffer->yh; ++y)
	{
        memcpy(mBackBuffer->yplane+mBackBuffer->yw*y,
				bufferinfo[0].data+
                    bufferinfo[0].stride*(y+mYPlaneYOffset)+mYPlaneXOffset,
                mBackBuffer->yw);
	}

    for (int y = 0; y < mBackBuffer->ch; ++y)
	{
        memcpy(mBackBuffer->cbplane+mBackBuffer->cw*y,
				bufferinfo[1].data+
                    bufferinfo[1].stride*(y+mCPlaneYOffset)+mCPlaneXOffset,
                mBackBuffer->cw);
	}

    for (int y = 0; y < mBackBuffer->ch; ++y)
	{
        memcpy(mBackBuffer->crplane+mBackBuffer->cw*y,
				bufferinfo[2].data+
                    bufferinfo[2].stride*(y+mCPlaneYOffset)+mCPlaneXOffset,
                mBackBuffer->cw);
	}

	// Seeking forwards:
	// If we're still not on the right frame, either we're lagging or we're seeking
	// After 5 frames, go for a seek. This is not ideal.. but what is
    if(position > mNextFrame)
	{
        if(++mLagCounter > 5)
			seek(position);
	}
	else
        mLagCounter = 0;

    TLock l(mBufferMutex);
    mFrameReady = true;
}

void TVideoStream::fillBackBuffer()
{
	// Done in worker thread
}

bool TVideoStream::swapBuffers()
{
    if(mEos)
		return false;

    TLock l(mBufferMutex);
    if(!mFrameReady)
		return false;
    mFrameReady = false;

    Frame *temp = mFrontBuffer;
    mFrontBuffer = mBackBuffer;
    mBackBuffer = temp;

	return true;
}
