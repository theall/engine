/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_VIDEO_THEORA_VIDEOSTREAM_H
#define THEALL_VIDEO_THEORA_VIDEOSTREAM_H

#include "base/abstractvideostream.h"


#include "common/int.h"
#include "modules/filesystem/file.h"
#include "modules/thread/thread.h"

// OGG/Theora
#include <ogg/ogg.h>
#include <theora/codec.h>
#include <theora/theoradec.h>

class TVideoStream : public TAbstractVideoStream
{
public:
    TVideoStream(TFile *file);
	~TVideoStream();

	const void *getFrontBuffer() const;
	size_t getSize() const;
	void fillBackBuffer();
	bool swapBuffers();

	int getWidth() const;
	int getHeight() const;
	const std::string &getFilename() const;
	void setSync(FrameSync *mFrameSync);

	bool isPlaying() const;
    void rewind();
	void threadedFillBackBuffer(double dt);

private:
    StrongRef<TFile> mFile;

    bool mHeaderParsed;
    bool mStreamInited;
    int mVideoSerial;
    ogg_sync_state mSync;
    ogg_stream_state mStream;
    ogg_page mPage;
    ogg_packet mPacket;

    th_info mVideoInfo;
    th_dec_ctx *mDecoder;

    Frame *mFrontBuffer;
    Frame *mBackBuffer;
    unsigned int mYPlaneXOffset;
    unsigned int mCPlaneXOffset;
    unsigned int mYPlaneYOffset;
    unsigned int mCPlaneYOffset;

    TMutexRef mBufferMutex;
    bool mFrameReady;

    double mLastFrame;
    double mNextFrame;
    bool mEos;
    unsigned int mLagCounter;

	void readPage();
	bool readPacket(bool mustSucceed = false); // true if eos
	void parseHeader();
	void seekDecoder(double target);
}; // TVideoStream

#endif // THEALL_VIDEO_THEORA_VIDEOSTREAM_H
