/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "abstractvideostream.h"

void TAbstractVideoStream::setSync(TAbstractVideoStream::FrameSync *frameSync)
{
    mFrameSync = frameSync;
}

TAbstractVideoStream::FrameSync *TAbstractVideoStream::getSync() const
{
    return mFrameSync;
}

void TAbstractVideoStream::play()
{
    mFrameSync->play();
}

void TAbstractVideoStream::pause()
{
    mFrameSync->pause();
}

void TAbstractVideoStream::seek(double offset)
{
    mFrameSync->seek(offset);
}

double TAbstractVideoStream::tell() const
{
    return mFrameSync->tell();
}

bool TAbstractVideoStream::isPlaying() const
{
    return mFrameSync->isPlaying();
}

TAbstractVideoStream::Frame::Frame()
	: yplane(nullptr)
	, cbplane(nullptr)
	, crplane(nullptr)
{
}

TAbstractVideoStream::Frame::~Frame()
{
	delete[] yplane;
	delete[] cbplane;
	delete[] crplane;
}

void TAbstractVideoStream::FrameSync::copyState(const TAbstractVideoStream::FrameSync *other)
{
	seek(other->tell());
	if(other->isPlaying())
		play();
	else
		pause();
}

double TAbstractVideoStream::FrameSync::tell() const
{
	return getPosition();
}

TAbstractVideoStream::DeltaSync::DeltaSync()
	: playing(false)
	, position(0)
	, speed(1)
{
}

TAbstractVideoStream::DeltaSync::~DeltaSync()
{
}

double TAbstractVideoStream::DeltaSync::getPosition() const
{
	return position;
}

void TAbstractVideoStream::DeltaSync::update(double dt)
{
    TLock l(mutex);
	if(playing)
		position += dt*speed;
}

void TAbstractVideoStream::DeltaSync::play()
{
	playing = true;
}

void TAbstractVideoStream::DeltaSync::pause()
{
	playing = false;
}

void TAbstractVideoStream::DeltaSync::seek(double time)
{
    TLock l(mutex);
	position = time;
}

bool TAbstractVideoStream::DeltaSync::isPlaying() const
{
	return playing;
}

TAbstractVideoStream::SourceSync::SourceSync(TSource *source)
	: source(source)
{
}

double TAbstractVideoStream::SourceSync::getPosition() const
{
    return source->tell(TSource::UNIT_SECONDS);
}

void TAbstractVideoStream::SourceSync::play()
{
	source->play();
}

void TAbstractVideoStream::SourceSync::pause()
{
	source->pause();
}

void TAbstractVideoStream::SourceSync::seek(double time)
{
    source->seek(time, TSource::UNIT_SECONDS);
}

bool TAbstractVideoStream::SourceSync::isPlaying() const
{
	return !source->isStopped() && !source->isPaused();
}
