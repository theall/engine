/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_VIDEO_VIDEOSTREAM_H
#define THEALL_VIDEO_VIDEOSTREAM_H

#include "common/stream.h"

#include "modules/audio/source.h"
#include "modules/thread/thread.h"

class TAbstractVideoStream : public TStream
{
public:
    virtual ~TAbstractVideoStream() {}

	virtual int getWidth() const = 0;
	virtual int getHeight() const = 0;
	virtual const std::string &getFilename() const = 0;

	// Playback api
	virtual void play();
	virtual void pause();
	virtual void seek(double offset);
	virtual double tell() const;
	virtual bool isPlaying() const;

	class FrameSync;
	class DeltaSync;

	// The stream now owns the sync, do not reuse or free
    virtual void setSync(FrameSync *mFrameSync);
	virtual FrameSync *getSync() const;

	// Data structures
	struct Frame
	{
		Frame();
		~Frame();

		int yw, yh;
		unsigned char *yplane;

		int cw, ch;
		unsigned char *cbplane;
		unsigned char *crplane;
	};

	class FrameSync : public TObject
	{
	public:
		virtual double getPosition() const = 0;
		virtual void update(double /*dt*/) {}
		virtual ~FrameSync() {}

		void copyState(const FrameSync *other);

		// Playback api
		virtual void play() = 0;
		virtual void pause() = 0;
		virtual void seek(double offset) = 0;
		virtual double tell() const;
		virtual bool isPlaying() const = 0;
	};

	class DeltaSync : public FrameSync
	{
	public:
		DeltaSync();
		~DeltaSync();

		virtual double getPosition() const override;
		virtual void update(double dt) override;

		virtual void play() override;
		virtual void pause() override;
		virtual void seek(double time) override;
		virtual bool isPlaying() const override;

	private:
		bool playing;
		double position;
		double speed;
		TMutexRef mutex;
	};

	class SourceSync : public FrameSync
	{
	public:
        SourceSync(TSource *source);

		virtual double getPosition() const override;
		virtual void play() override;
		virtual void pause() override;
		virtual void seek(double time) override;
		virtual bool isPlaying() const override;

	private:
        StrongRef<TSource> source;
	};

protected:
    StrongRef<FrameSync> mFrameSync;
};




#endif // THEALL_VIDEO_VIDEOSTREAM_H
