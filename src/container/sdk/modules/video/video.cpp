/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

// STL
#include <vector>

#include "video.h"

#include "modules/timer/timer.h"

#include <SDL2/SDL.h>

TVideoPlay::TVideoPlay()
{
    workerThread = new TWorker();
	workerThread->start();
}

TVideoPlay::~TVideoPlay()
{
    if(workerThread)
    {
        delete workerThread;
        workerThread = NULL;
    }

}

TVideoStream *TVideoPlay::newVideoStream(TFile *file)
{
    TVideoStream *stream = new TVideoStream(file);
	workerThread->addStream(stream);
	return stream;
}

const char *TVideoPlay::getName() const
{
	return "theall.video.theora";
}

TWorker::TWorker()
	: stopping(false)
{
	threadName = "VideoWorker";
}

TWorker::~TWorker()
{
	stop();
}

void TWorker::addStream(TVideoStream *stream)
{
    TLock l(mutex);
	streams.push_back(stream);
	cond->broadcast();
}

void TWorker::stop()
{
	{
        TLock l(mutex);
		stopping = true;
		cond->broadcast();
	}

    mOwner->wait();
}

void TWorker::threadFunction()
{
    double lastFrame = TTimer::getTime();

	while (true)
	{
        SDL_Delay(2);

        TLock l(mutex);

		while (!stopping && streams.empty())
		{
			cond->wait(mutex);
            lastFrame = TTimer::getTime();
		}

		if(stopping)
			return;

        double curFrame = TTimer::getTime();
		double dt = curFrame-lastFrame;
		lastFrame = curFrame;

		for (auto it = streams.begin(); it != streams.end(); ++it)
		{
            TVideoStream *stream = *it;
			if(stream->getReferenceCount() == 1)
			{
				// We're the only ones left
				streams.erase(it);
				break;
			}

			stream->threadedFillBackBuffer(dt);
		}
	}
}
