/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_AUDIO_OPENAL_SOURCE_H
#define THEALL_AUDIO_OPENAL_SOURCE_H

#include "modules/sound/decoder.h"
#include "modules/sound/sounddata.h"

#include "common/config.h"
#include "common/object.h"

#include "base/abstractsource.h"

// OpenAL
#ifdef THEALL_APPLE_USE_FRAMEWORKS
#ifdef THEALL_IOS
#include <OpenAL/alc.h>
#include <OpenAL/al.h>
#else
#include <OpenAL-Soft/alc.h>
#include <OpenAL-Soft/al.h>
#endif
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif

class TAudio;
class Pool;

// Basically just a reference-counted non-streaming OpenAL buffer object.
class TStaticDataBuffer : public TObject
{
public:

    TStaticDataBuffer(ALenum format, const ALvoid *data, ALsizei size, ALsizei freq);
    virtual ~TStaticDataBuffer();

	inline ALuint getBuffer() const
	{
		return buffer;
	}

	inline ALsizei getSize() const
	{
		return size;
	}

private:

	ALuint buffer;
	ALsizei size;

}; // StaticDataBuffer

class TSource : public TAbstractSource
{
public:
    TSource(Pool *mPool, TSoundData *soundData);
    TSource(Pool *mPool, TDecoder *decoder);
    TSource(const TSource &s);
    virtual ~TSource();

    virtual TAbstractSource *clone();
	virtual bool play();
    virtual bool restart();
	virtual void stop();
	virtual void pause();
	virtual void resume();
	virtual void rewind();
	virtual bool isStopped() const;
	virtual bool isPaused() const;
	virtual bool isFinished() const;
	virtual bool update();
    virtual void setPitch(float pitch);
	virtual float getPitch() const;
    virtual void setVolume(float volume);
	virtual float getVolume() const;
	virtual void seekAtomic(float offset, void *unit);
	virtual void seek(float offset, Unit unit);
	virtual float tellAtomic(void *unit) const;
	virtual float tell(Unit unit);
	virtual double getDurationAtomic(void *unit);
	virtual double getDuration(Unit unit);
	virtual void setPosition(float *v);
	virtual void getPosition(float *v) const;
	virtual void setVelocity(float *v);
	virtual void getVelocity(float *v) const;
	virtual void setDirection(float *v);
	virtual void getDirection(float *v) const;
	virtual void setCone(float innerAngle, float outerAngle, float outerVolume);
	virtual void getCone(float &innerAngle, float &outerAngle, float &outerVolume) const;
	virtual void setRelative(bool enable);
	virtual bool isRelative() const;
    void setLooping(bool looping);
	bool isLooping() const;
    virtual void setMinVolume(float volume);
	virtual float getMinVolume() const;
    virtual void setMaxVolume(float volume);
	virtual float getMaxVolume() const;
	virtual void setReferenceDistance(float distance);
	virtual float getReferenceDistance() const;
	virtual void setRolloffFactor(float factor);
	virtual float getRolloffFactor() const;
	virtual void setMaxDistance(float distance);
	virtual float getMaxDistance() const;
	virtual int getChannels() const;

	bool playAtomic();
	void stopAtomic();
	void pauseAtomic();
	void resumeAtomic();
	void rewindAtomic();

private:

	void reset();

	void setFloatv(float *dst, const float *src) const;

	/**
	 * Gets the OpenAL format identifier based on number of
	 * channels and bits.
	 * @param channels Either 1 (mono) or 2 (stereo).
	 * @param bitDepth Either 8-bit samples, or 16-bit samples.
	 * @return One of AL_FORMAT_*, or 0 if unsupported format.
	 **/
    ALenum getFormat(int mChannels, int bitDepth) const;

    int streamAtomic(ALuint buffer, TAbstractDecoder *d);

    Pool *mPool;
    ALuint mSource;
    bool mIsValid;

	static const unsigned int MAX_BUFFERS = 8;
    ALuint mStreamBuffers[MAX_BUFFERS];

    StrongRef<TStaticDataBuffer> mStaticBuffer;

    float mPitch;
    float mVolume;
    float mPosition[3];
    float mVelocity[3];
    float mDirection[3];
    bool mRelative;
    bool mLooping;
    bool mPaused;
    float mMinVolume;
    float mMaxVolume;
    float mReferenceDistance;
    float mRolloffFactor;
    float mMaxDistance;

	struct Cone
	{
		int innerAngle = 360; // degrees
		int outerAngle = 360; // degrees
		float outerVolume = 0.0f;
	} cone;

    float mOffsetSamples;
    float mOffsetSeconds;

    int mSampleRate;
    int mChannels;
    int mBitDepth;

    StrongRef<TAbstractDecoder> decoder;

	unsigned int toLoop;

}; // Source

#endif // THEALL_AUDIO_OPENAL_SOURCE_H
