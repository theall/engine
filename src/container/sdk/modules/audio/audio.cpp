/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "audio.h"

#include "modules/sound/decoder.h"
#include "modules/filesystem/filesystem.h"
#include "modules/sound/sound.h"

#include <cstdlib>
#include <SDL2/SDL.h>

TAudio::TPoolThread::TPoolThread(Pool *pool)
	: pool(pool)
	, finish(false)
{
	threadName = "AudioPool";
}

TAudio::TPoolThread::~TPoolThread()
{
}

void TAudio::TPoolThread::threadFunction()
{
	while (true)
	{
		{
            TLock lock(mutex);
            if(finish)
			{
				return;
			}
		}

		pool->update();
        SDL_Delay(1);
	}
}

TAudio *TAudio::mInstance=NULL;
void TAudio::TPoolThread::setFinish()
{
    TLock lock(mutex);
	finish = true;
}

TAudio::TAudio()
	: device(nullptr)
	, capture(nullptr)
	, context(nullptr)
	, pool(nullptr)
    , mPoolThread(nullptr)
    , mDistanceModel(DISTANCE_INVERSE_CLAMPED)
{
	// Passing null for default device.
	device = alcOpenDevice(nullptr);

    if(device == nullptr)
        throw TException("Could not open device.");

	context = alcCreateContext(device, nullptr);

    if(context == nullptr)
        throw TException("Could not create context.");

    if(!alcMakeContextCurrent(context) || alcGetError(device) != ALC_NO_ERROR)
        throw TException("Could not make context current.");

	/*std::string captureName(alcGetString(NULL, ALC_CAPTURE_DEFAULT_DEVICE_SPECIFIER));
	const ALCchar * devices = alcGetString(NULL, ALC_CAPTURE_DEVICE_SPECIFIER);
	while (*devices)
	{
		std::string device(devices);
		devices += device.size() + 1;
        if(device.find("Mic") != std::string::npos || device.find("mic") != std::string::npos)
		{
			captureName = device;
		}
	}

	capture = alcCaptureOpenDevice(captureName.c_str(), 8000, AL_FORMAT_MONO16, 262144); // about 32 seconds

    if(!capture)
	{
		// We're not going to prevent LOVE from running without a microphone, but we should warn, at least
		std::cerr << "Warning, couldn't open capture device! No audio input!" << std::endl;
	}*/

	// pool must be allocated after AL context.
	try
	{
		pool = new Pool();
	}
    catch (TException &)
	{
		alcMakeContextCurrent(nullptr);
		alcDestroyContext(context);
        //if(capture) alcCaptureCloseDevice(capture);
		alcCloseDevice(device);
		throw;
	}

    mPoolThread = new TPoolThread(pool);
    mPoolThread->start();
}

TAudio::~TAudio()
{
    mPoolThread->setFinish();
    mPoolThread->wait();

    delete mPoolThread;
	delete pool;

	alcMakeContextCurrent(nullptr);
	alcDestroyContext(context);
    //if(capture) alcCaptureCloseDevice(capture);
	alcCloseDevice(device);
}


const char *TAudio::getName() const
{
    return "theall.audio.openal";
}

TSource *TAudio::newSource(TDecoder *decoder)
{
    return new TSource(pool, decoder);
}

TSource *TAudio::newSource(TSoundData *soundData)
{
    return new TSource(pool, soundData);
}

TSource *TAudio::newSource(std::string filePath)
{
    TFileData *fileData = TFileSystem::instance()->newFileData(filePath);
    TDecoder *decoder = TSound::instance()->newDecoder(fileData, TDecoder::DEFAULT_BUFFER_SIZE);
    fileData->release();
    if(!decoder)
        throw TException("No decoder for file %s", filePath.c_str());

    return new TSource(pool, decoder);
}

int TAudio::getSourceCount() const
{
	return pool->getSourceCount();
}

int TAudio::getMaxSources() const
{
	return pool->getMaxSources();
}

bool TAudio::play(TAbstractSource *source)
{
	return source->play();
}

void TAudio::stop(TAbstractSource *source)
{
	source->stop();
}

void TAudio::stop()
{
	pool->stop();
}

void TAudio::pause(TAbstractSource *source)
{
	source->pause();
}

void TAudio::pause()
{
	pool->pause();
#ifdef THEALL_ANDROID
	alcDevicePauseSOFT(device);
#endif
}

void TAudio::resume(TAbstractSource *source)
{
	source->resume();
}

void TAudio::resume()
{
#ifdef THEALL_ANDROID
	alcDeviceResumeSOFT(device);
#endif
	pool->resume();
}

void TAudio::rewind(TAbstractSource *source)
{
	source->rewind();
}

void TAudio::rewind()
{
	pool->rewind();
}

void TAudio::setVolume(float volume)
{
	alListenerf(AL_GAIN, volume);
}

float TAudio::getVolume() const
{
	ALfloat volume;
	alGetListenerf(AL_GAIN, &volume);
	return volume;
}

void TAudio::getPosition(float *v) const
{
	alGetListenerfv(AL_POSITION, v);
}

void TAudio::setPosition(float *v)
{
	alListenerfv(AL_POSITION, v);
}

void TAudio::getOrientation(float *v) const
{
	alGetListenerfv(AL_ORIENTATION, v);
}

void TAudio::setOrientation(float *v)
{
	alListenerfv(AL_ORIENTATION, v);
}

void TAudio::getVelocity(float *v) const
{
	alGetListenerfv(AL_VELOCITY, v);
}

void TAudio::setVelocity(float *v)
{
	alListenerfv(AL_VELOCITY, v);
}

void TAudio::setDopplerScale(float scale)
{
    if(scale >= 0.0f)
		alDopplerFactor(scale);
}

float TAudio::getDopplerScale() const
{
	return alGetFloat(AL_DOPPLER_FACTOR);
}

void TAudio::record()
{
    if(!canRecord()) return;
	alcCaptureStart(capture);
}

TSoundData *TAudio::getRecordedData()
{
    if(!canRecord())
		return NULL;
	int samplerate = 8000;
	ALCint samples;
	alcGetIntegerv(capture, ALC_CAPTURE_SAMPLES, 4, &samples);
	void *data = malloc(samples * (2/sizeof(char)));
	alcCaptureSamples(capture, data, samples);
    TSoundData *sd = new TSoundData(data, samples, samplerate, 16, 1);
	free(data);
	return sd;
}

TSoundData *TAudio::stopRecording(bool returnData)
{
    if(!canRecord())
		return NULL;
    TSoundData *sd = NULL;
    if(returnData)
	{
		sd = getRecordedData();
	}
	alcCaptureStop(capture);
	return sd;
}

bool TAudio::canRecord()
{
	return (capture != NULL);
}

TAudio::DistanceModel TAudio::getDistanceModel() const
{
    return mDistanceModel;
}

void TAudio::setDistanceModel(DistanceModel distanceModel)
{
    mDistanceModel = distanceModel;

	switch (distanceModel)
	{
	case DISTANCE_NONE:
		alDistanceModel(AL_NONE);
		break;

	case DISTANCE_INVERSE:
		alDistanceModel(AL_INVERSE_DISTANCE);
		break;

	case DISTANCE_INVERSE_CLAMPED:
		alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);
		break;

	case DISTANCE_LINEAR:
		alDistanceModel(AL_LINEAR_DISTANCE);
		break;

	case DISTANCE_LINEAR_CLAMPED:
		alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);
		break;

	case DISTANCE_EXPONENT:
		alDistanceModel(AL_EXPONENT_DISTANCE);
		break;

	case DISTANCE_EXPONENT_CLAMPED:
		alDistanceModel(AL_EXPONENT_DISTANCE_CLAMPED);
		break;

	default:
		break;
	}
}
