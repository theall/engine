/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "source.h"
#include "pool.h"
#include "common/math.h"

// STD
#include <iostream>
#include <float.h>
#include <algorithm>

#ifdef THEALL_IOS
// OpenAL on iOS barfs if the max distance is +inf.
static const float MAX_ATTENUATION_DISTANCE = 1000000.0f;
#else
static const float MAX_ATTENUATION_DISTANCE = FLT_MAX;
#endif

class InvalidFormatException : public TException
{
public:

	InvalidFormatException(int channels, int bitdepth)
		: TException("%d-channel Sources with %d bits per sample are not supported.", channels, bitdepth)
	{
	}

};

class SpatialSupportException : public TException
{
public:

	SpatialSupportException()
		: TException("This spatial audio functionality is only available for mono Sources. \
Ensure the Source is not multi-channel before calling this function.")
	{
	}

};

TStaticDataBuffer::TStaticDataBuffer(ALenum format, const ALvoid *data, ALsizei size, ALsizei freq)
	: size(size)
{
	alGenBuffers(1, &buffer);
	alBufferData(buffer, format, data, size, freq);
}

TStaticDataBuffer::~TStaticDataBuffer()
{
	alDeleteBuffers(1, &buffer);
}

TSource::TSource(Pool *pool, TSoundData *soundData)
    : TAbstractSource(TSource::TYPE_STATIC)
    , mPool(pool)
    , mIsValid(false)
    , mStaticBuffer(nullptr)
    , mPitch(1.0f)
    , mVolume(1.0f)
    , mRelative(false)
    , mLooping(false)
    , mPaused(false)
    , mMinVolume(0.0f)
    , mMaxVolume(1.0f)
    , mReferenceDistance(1.0f)
    , mRolloffFactor(1.0f)
    , mMaxDistance(MAX_ATTENUATION_DISTANCE)
	, cone()
    , mOffsetSamples(0)
    , mOffsetSeconds(0)
    , mSampleRate(soundData->getSampleRate())
    , mChannels(soundData->getChannels())
    , mBitDepth(soundData->getBitDepth())
	, decoder(nullptr)
	, toLoop(0)
{
	ALenum fmt = getFormat(soundData->getChannels(), soundData->getBitDepth());
	if(fmt == 0)
		throw InvalidFormatException(soundData->getChannels(), soundData->getBitDepth());

    mStaticBuffer.set(new TStaticDataBuffer(fmt, soundData->getData(), (ALsizei) soundData->getSize(), mSampleRate), Acquire::NORETAIN);

	float z[3] = {0, 0, 0};

    setFloatv(mPosition, z);
    setFloatv(mVelocity, z);
    setFloatv(mDirection, z);
}

TSource::TSource(Pool *pool, TDecoder *decoder)
    : TAbstractSource(TSource::TYPE_STREAM)
    , mPool(pool)
    , mIsValid(false)
    , mStaticBuffer(nullptr)
    , mPitch(1.0f)
    , mVolume(1.0f)
    , mRelative(false)
    , mLooping(false)
    , mPaused(false)
    , mMinVolume(0.0f)
    , mMaxVolume(1.0f)
    , mReferenceDistance(1.0f)
    , mRolloffFactor(1.0f)
    , mMaxDistance(MAX_ATTENUATION_DISTANCE)
	, cone()
    , mOffsetSamples(0)
    , mOffsetSeconds(0)
    , mSampleRate(decoder->getSampleRate())
    , mChannels(decoder->getChannels())
    , mBitDepth(decoder->getBitDepth())
	, decoder(decoder)
	, toLoop(0)
{
	if(getFormat(decoder->getChannels(), decoder->getBitDepth()) == 0)
		throw InvalidFormatException(decoder->getChannels(), decoder->getBitDepth());

    alGenBuffers(MAX_BUFFERS, mStreamBuffers);

	float z[3] = {0, 0, 0};

    setFloatv(mPosition, z);
    setFloatv(mVelocity, z);
    setFloatv(mDirection, z);
}

TSource::TSource(const TSource &s)
    : TAbstractSource(s.type)
    , mPool(s.mPool)
    , mIsValid(false)
    , mStaticBuffer(s.mStaticBuffer)
    , mPitch(s.mPitch)
    , mVolume(s.mVolume)
    , mRelative(s.mRelative)
    , mLooping(s.mLooping)
    , mPaused(false)
    , mMinVolume(s.mMinVolume)
    , mMaxVolume(s.mMaxVolume)
    , mReferenceDistance(s.mReferenceDistance)
    , mRolloffFactor(s.mRolloffFactor)
    , mMaxDistance(s.mMaxDistance)
	, cone(s.cone)
    , mOffsetSamples(0)
    , mOffsetSeconds(0)
    , mSampleRate(s.mSampleRate)
    , mChannels(s.mChannels)
    , mBitDepth(s.mBitDepth)
	, decoder(nullptr)
	, toLoop(0)
{
	if(type == TYPE_STREAM)
	{
		if(s.decoder.get())
			decoder.set(s.decoder->clone(), Acquire::NORETAIN);

        alGenBuffers(MAX_BUFFERS, mStreamBuffers);
	}

    setFloatv(mPosition, s.mPosition);
    setFloatv(mVelocity, s.mVelocity);
    setFloatv(mDirection, s.mDirection);
}

TSource::~TSource()
{
    if(mIsValid)
        mPool->stop(this);

	if(type == TYPE_STREAM)
        alDeleteBuffers(MAX_BUFFERS, mStreamBuffers);
}

TAbstractSource *TSource::clone()
{
    return new TSource(*this);
}

bool TSource::play()
{
    if(mIsValid && mPaused)
	{
        mPool->resume(this);
		return true;
	}

    mIsValid = mPool->play(this, mSource);
    return mIsValid;
}

bool TSource::restart()
{
    stop();
    return play();
}

void TSource::stop()
{
	if(!isStopped())
	{
        mPool->stop(this);
        mPool->softRewind(this);
	}
}

void TSource::pause()
{
    mPool->pause(this);
}

void TSource::resume()
{
    mPool->resume(this);
}

void TSource::rewind()
{
    mPool->rewind(this);
}

bool TSource::isStopped() const
{
    if(mIsValid)
	{
		ALenum state;
        alGetSourcei(mSource, AL_SOURCE_STATE, &state);
		return (state == AL_STOPPED);
	}

	return true;
}

bool TSource::isPaused() const
{
    if(mIsValid)
	{
		ALenum state;
        alGetSourcei(mSource, AL_SOURCE_STATE, &state);
		return (state == AL_PAUSED);
	}

	return false;
}

bool TSource::isFinished() const
{
	return type == TYPE_STATIC ? isStopped() : (isStopped() && !isLooping() && decoder->isFinished());
}

bool TSource::update()
{
    if(!mIsValid)
		return false;

	if(type == TYPE_STATIC)
	{
		// Looping mode could have changed.
        alSourcei(mSource, AL_LOOPING, isLooping() ? AL_TRUE : AL_FALSE);
		return !isStopped();
	}
	else if(type == TYPE_STREAM && (isLooping() || !isFinished()))
	{
		// Number of processed buffers.
		ALint processed = 0;

        alGetSourcei(mSource, AL_BUFFERS_PROCESSED, &processed);

		while (processed--)
		{
			ALuint buffer;

			float curOffsetSamples, curOffsetSecs;

            alGetSourcef(mSource, AL_SAMPLE_OFFSET, &curOffsetSamples);

			int freq = decoder->getSampleRate();
			curOffsetSecs = curOffsetSamples / freq;

			// Get a free buffer.
            alSourceUnqueueBuffers(mSource, 1, &buffer);

			float newOffsetSamples, newOffsetSecs;

            alGetSourcef(mSource, AL_SAMPLE_OFFSET, &newOffsetSamples);
			newOffsetSecs = newOffsetSamples / freq;

            mOffsetSamples += (curOffsetSamples - newOffsetSamples);
            mOffsetSeconds += (curOffsetSecs - newOffsetSecs);

			// FIXME: We should put freed buffers into a list that we later
			// consume here, so we can keep track of all free buffers even if we
			// tried to stream data to one but the decoder didn't have data for it.
			if(streamAtomic(buffer, decoder.get()) > 0)
                alSourceQueueBuffers(mSource, 1, &buffer);
		}

		return true;
	}

	return false;
}

void TSource::setPitch(float pitch)
{
    if(mIsValid)
        alSourcef(mSource, AL_PITCH, pitch);

    mPitch = pitch;
}

float TSource::getPitch() const
{
    if(mIsValid)
	{
		ALfloat f;
        alGetSourcef(mSource, AL_PITCH, &f);
		return f;
	}

	// In case the Source isn't playing.
    return mPitch;
}

void TSource::setVolume(float volume)
{
    if(mIsValid)
        alSourcef(mSource, AL_GAIN, volume);

    mVolume = volume;
}

float TSource::getVolume() const
{
    if(mIsValid)
	{
		ALfloat f;
        alGetSourcef(mSource, AL_GAIN, &f);
		return f;
	}

	// In case the Source isn't playing.
    return mVolume;
}

void TSource::seekAtomic(float offset, void *unit)
{
    if(mIsValid)
	{
        switch (*((TSource::Unit *) unit))
		{
        case TSource::UNIT_SAMPLES:
			if(type == TYPE_STREAM)
			{
                mOffsetSamples = offset;
				offset /= decoder->getSampleRate();
                mOffsetSeconds = offset;
				decoder->seek(offset);
			}
			else
                alSourcef(mSource, AL_SAMPLE_OFFSET, offset);
			break;
        case TSource::UNIT_SECONDS:
		default:
			if(type == TYPE_STREAM)
			{
                mOffsetSeconds = offset;
				decoder->seek(offset);
                mOffsetSamples = offset * decoder->getSampleRate();
			}
			else
                alSourcef(mSource, AL_SEC_OFFSET, offset);
			break;
		}
		if(type == TYPE_STREAM)
		{
            bool waspaused = mPaused;
			// Because we still have old data
			// from before the seek in the buffers
			// let's empty them.
			stopAtomic();
			playAtomic();
			if(waspaused)
				pauseAtomic();
		}
	}
}

void TSource::seek(float offset, TSource::Unit unit)
{
    return mPool->seek(this, offset, &unit);
}

float TSource::tellAtomic(void *unit) const
{
    if(mIsValid)
	{
		float offset;
        switch (*((TSource::Unit *) unit))
		{
        case TSource::UNIT_SAMPLES:
            alGetSourcef(mSource, AL_SAMPLE_OFFSET, &offset);
            if(type == TYPE_STREAM) offset += mOffsetSamples;
			break;
        case TSource::UNIT_SECONDS:
		default:
			{
                alGetSourcef(mSource, AL_SAMPLE_OFFSET, &offset);
                offset /= mSampleRate;
                if(type == TYPE_STREAM) offset += mOffsetSeconds;
			}
			break;
		}
		return offset;
	}
	return 0.0f;
}

float TSource::tell(TSource::Unit unit)
{
    return mPool->tell(this, &unit);
}

double TSource::getDurationAtomic(void *vunit)
{
	Unit unit = *(Unit *) vunit;

	if(type == TYPE_STREAM)
	{
		double seconds = decoder->getDuration();

		if(unit == UNIT_SECONDS)
			return seconds;
		else
			return seconds * decoder->getSampleRate();
	}
	else
	{
        ALsizei size = mStaticBuffer->getSize();
        ALsizei samples = (size / mChannels) / (mBitDepth / 8);

		if(unit == UNIT_SAMPLES)
			return (double) samples;
		else
            return (double) samples / (double) mSampleRate;
	}
}

double TSource::getDuration(Unit unit)
{
    return mPool->getDuration(this, &unit);
}

void TSource::setPosition(float *v)
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
        alSourcefv(mSource, AL_POSITION, v);

    setFloatv(mPosition, v);
}

void TSource::getPosition(float *v) const
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
        alGetSourcefv(mSource, AL_POSITION, v);
	else
        setFloatv(v, mPosition);
}

void TSource::setVelocity(float *v)
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
        alSourcefv(mSource, AL_VELOCITY, v);

    setFloatv(mVelocity, v);
}

void TSource::getVelocity(float *v) const
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
        alGetSourcefv(mSource, AL_VELOCITY, v);
	else
        setFloatv(v, mVelocity);
}

void TSource::setDirection(float *v)
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
        alSourcefv(mSource, AL_DIRECTION, v);
	else
        setFloatv(mDirection, v);
}

void TSource::getDirection(float *v) const
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
        alGetSourcefv(mSource, AL_DIRECTION, v);
	else
        setFloatv(v, mDirection);
}

void TSource::setCone(float innerAngle, float outerAngle, float outerVolume)
{
    if(mChannels > 1)
		throw SpatialSupportException();

	cone.innerAngle  = (int) THEALL_TODEG(innerAngle);
	cone.outerAngle  = (int) THEALL_TODEG(outerAngle);
	cone.outerVolume = outerVolume;

    if(mIsValid)
	{
        alSourcei(mSource, AL_CONE_INNER_ANGLE, cone.innerAngle);
        alSourcei(mSource, AL_CONE_OUTER_ANGLE, cone.outerAngle);
        alSourcef(mSource, AL_CONE_OUTER_GAIN, cone.outerVolume);
	}
}

void TSource::getCone(float &innerAngle, float &outerAngle, float &outerVolume) const
{
    if(mChannels > 1)
		throw SpatialSupportException();

	innerAngle  = THEALL_TORAD(cone.innerAngle);
	outerAngle  = THEALL_TORAD(cone.outerAngle);
	outerVolume = cone.outerVolume;
}

void TSource::setRelative(bool enable)
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
        alSourcei(mSource, AL_SOURCE_RELATIVE, enable ? AL_TRUE : AL_FALSE);

    mRelative = enable;
}

bool TSource::isRelative() const
{
    if(mChannels > 1)
		throw SpatialSupportException();

    return mRelative;
}

void TSource::setLooping(bool enable)
{
    if(mIsValid && type == TYPE_STATIC)
        alSourcei(mSource, AL_LOOPING, enable ? AL_TRUE : AL_FALSE);

    mLooping = enable;
}

bool TSource::isLooping() const
{
    return mLooping;
}

bool TSource::playAtomic()
{
	// This Source may now be associated with an OpenAL source that still has
	// the properties of another love Source. Let's reset it to the settings
	// of the new one.
	reset();

	if(type == TYPE_STATIC)
	{
        alSourcei(mSource, AL_BUFFER, mStaticBuffer->getBuffer());
	}
	else if(type == TYPE_STREAM)
	{
		int usedBuffers = 0;

		for (unsigned int i = 0; i < MAX_BUFFERS; i++)
		{
            if(streamAtomic(mStreamBuffers[i], decoder.get()) == 0)
				break;

			++usedBuffers;

			if(decoder->isFinished())
				break;
		}

		if(usedBuffers > 0)
            alSourceQueueBuffers(mSource, usedBuffers, mStreamBuffers);
	}

	// Clear errors.
	alGetError();

    alSourcePlay(mSource);

	// alSourcePlay may fail if the system has reached its limit of simultaneous
	// playing sources.
	bool success = alGetError() == AL_NO_ERROR;

    mIsValid = true; //if it fails it will be set to false again
	//but this prevents a horrible, horrible bug

	return success;
}

void TSource::stopAtomic()
{
    if(mIsValid)
	{
		if(type == TYPE_STATIC)
            alSourceStop(mSource);
		else if(type == TYPE_STREAM)
		{
            alSourceStop(mSource);
			int queued = 0;
            alGetSourcei(mSource, AL_BUFFERS_QUEUED, &queued);

			while (queued--)
			{
				ALuint buffer;
                alSourceUnqueueBuffers(mSource, 1, &buffer);
			}
		}

        alSourcei(mSource, AL_BUFFER, AL_NONE);
	}

	toLoop = 0;
    mIsValid = false;
}

void TSource::pauseAtomic()
{
    if(mIsValid)
	{
        alSourcePause(mSource);
        mPaused = true;
	}
}

void TSource::resumeAtomic()
{
    if(mIsValid && mPaused)
	{
        alSourcePlay(mSource);
        mPaused = false;
	}
}

void TSource::rewindAtomic()
{
    if(mIsValid && type == TYPE_STATIC)
	{
        alSourceRewind(mSource);
        if(!mPaused)
            alSourcePlay(mSource);
	}
    else if(mIsValid && type == TYPE_STREAM)
	{
        bool waspaused = mPaused;
		decoder->rewind();
		// Because we still have old data
		// from before the seek in the buffers
		// let's empty them.
		stopAtomic();
		playAtomic();
		if(waspaused)
			pauseAtomic();
        mOffsetSamples = 0;
        mOffsetSeconds = 0;
	}
	else if(type == TYPE_STREAM)
	{
		decoder->rewind();
        mOffsetSamples = 0;
        mOffsetSeconds = 0;
	}
}

void TSource::reset()
{
    alSourcei(mSource, AL_BUFFER, 0);
    alSourcefv(mSource, AL_POSITION, mPosition);
    alSourcefv(mSource, AL_VELOCITY, mVelocity);
    alSourcefv(mSource, AL_DIRECTION, mDirection);
    alSourcef(mSource, AL_PITCH, mPitch);
    alSourcef(mSource, AL_GAIN, mVolume);
    alSourcef(mSource, AL_MIN_GAIN, mMinVolume);
    alSourcef(mSource, AL_MAX_GAIN, mMaxVolume);
    alSourcef(mSource, AL_REFERENCE_DISTANCE, mReferenceDistance);
    alSourcef(mSource, AL_ROLLOFF_FACTOR, mRolloffFactor);
    alSourcef(mSource, AL_MAX_DISTANCE, mMaxDistance);
    alSourcei(mSource, AL_LOOPING, (type == TYPE_STATIC) && isLooping() ? AL_TRUE : AL_FALSE);
    alSourcei(mSource, AL_SOURCE_RELATIVE, mRelative ? AL_TRUE : AL_FALSE);
    alSourcei(mSource, AL_CONE_INNER_ANGLE, cone.innerAngle);
    alSourcei(mSource, AL_CONE_OUTER_ANGLE, cone.outerAngle);
    alSourcef(mSource, AL_CONE_OUTER_GAIN, cone.outerVolume);
}

void TSource::setFloatv(float *dst, const float *src) const
{
	dst[0] = src[0];
	dst[1] = src[1];
	dst[2] = src[2];
}

ALenum TSource::getFormat(int channels, int bitDepth) const
{
	if(channels == 1 && bitDepth == 8)
		return AL_FORMAT_MONO8;
	else if(channels == 1 && bitDepth == 16)
		return AL_FORMAT_MONO16;
	else if(channels == 2 && bitDepth == 8)
		return AL_FORMAT_STEREO8;
	else if(channels == 2 && bitDepth == 16)
		return AL_FORMAT_STEREO16;

#ifdef AL_EXT_MCFORMATS
	if(alIsExtensionPresent("AL_EXT_MCFORMATS"))
	{
		if(channels == 6 && bitDepth == 8)
			return AL_FORMAT_51CHN8;
		else if(channels == 6 && bitDepth == 16)
			return AL_FORMAT_51CHN16;
		else if(channels == 8 && bitDepth == 8)
			return AL_FORMAT_71CHN8;
		else if(channels == 8 && bitDepth == 16)
			return AL_FORMAT_71CHN16;
	}
#endif

	return 0;
}

int TSource::streamAtomic(ALuint buffer, TAbstractDecoder *d)
{
	// Get more sound data.
	int decoded = std::max(d->decode(), 0);

	// OpenAL implementations are allowed to ignore 0-size alBufferData calls.
	if(decoded > 0)
	{
		int fmt = getFormat(d->getChannels(), d->getBitDepth());

		if(fmt != 0)
			alBufferData(buffer, fmt, d->getBuffer(), decoded, d->getSampleRate());
		else
			decoded = 0;
	}

	if(decoder->isFinished() && isLooping())
	{
		int queued, processed;
        alGetSourcei(mSource, AL_BUFFERS_QUEUED, &queued);
        alGetSourcei(mSource, AL_BUFFERS_PROCESSED, &processed);
		if(queued > processed)
			toLoop = queued-processed;
		else
			toLoop = MAX_BUFFERS-processed;
		d->rewind();
	}

	if(toLoop > 0)
	{
		if(--toLoop == 0)
		{
            mOffsetSamples = 0;
            mOffsetSeconds = 0;
		}
	}

	return decoded;
}

void TSource::setMinVolume(float volume)
{
    if(mIsValid)
        alSourcef(mSource, AL_MIN_GAIN, volume);

    mMinVolume = volume;
}

float TSource::getMinVolume() const
{
    if(mIsValid)
	{
		ALfloat f;
        alGetSourcef(mSource, AL_MIN_GAIN, &f);
		return f;
	}

	// In case the Source isn't playing.
    return mMinVolume;
}

void TSource::setMaxVolume(float volume)
{
    if(mIsValid)
        alSourcef(mSource, AL_MAX_GAIN, volume);

    mMaxVolume = volume;
}

float TSource::getMaxVolume() const
{
    if(mIsValid)
	{
		ALfloat f;
        alGetSourcef(mSource, AL_MAX_GAIN, &f);
		return f;
	}

	// In case the Source isn't playing.
    return mMaxVolume;
}

void TSource::setReferenceDistance(float distance)
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
        alSourcef(mSource, AL_REFERENCE_DISTANCE, distance);

    mReferenceDistance = distance;
}

float TSource::getReferenceDistance() const
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
	{
		ALfloat f;
        alGetSourcef(mSource, AL_REFERENCE_DISTANCE, &f);
		return f;
	}

	// In case the Source isn't playing.
    return mReferenceDistance;
}

void TSource::setRolloffFactor(float factor)
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
        alSourcef(mSource, AL_ROLLOFF_FACTOR, factor);

    mRolloffFactor = factor;
}

float TSource::getRolloffFactor() const
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
	{
		ALfloat f;
        alGetSourcef(mSource, AL_ROLLOFF_FACTOR, &f);
		return f;
	}

	// In case the Source isn't playing.
    return mRolloffFactor;
}

void TSource::setMaxDistance(float distance)
{
    if(mChannels > 1)
		throw SpatialSupportException();

	distance = std::min(distance, MAX_ATTENUATION_DISTANCE);

    if(mIsValid)
        alSourcef(mSource, AL_MAX_DISTANCE, distance);

    mMaxDistance = distance;
}

float TSource::getMaxDistance() const
{
    if(mChannels > 1)
		throw SpatialSupportException();

    if(mIsValid)
	{
		ALfloat f;
        alGetSourcef(mSource, AL_MAX_DISTANCE, &f);
		return f;
	}

	// In case the Source isn't playing.
    return mMaxDistance;
}

int TSource::getChannels() const
{
    return mChannels;
}
