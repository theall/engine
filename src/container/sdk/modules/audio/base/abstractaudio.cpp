/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered Audio versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any Audio distribution.
 **/

#include "abstractaudio.h"

TStringMap<TAbstractAudio::DistanceModel, TAbstractAudio::DISTANCE_MAX_ENUM>::Entry TAbstractAudio::distanceModelEntries[] =
{
    {"none", TAbstractAudio::DISTANCE_NONE},
    {"inverse", TAbstractAudio::DISTANCE_INVERSE},
    {"inverseclamped", TAbstractAudio::DISTANCE_INVERSE_CLAMPED},
    {"linear", TAbstractAudio::DISTANCE_LINEAR},
    {"linearclamped", TAbstractAudio::DISTANCE_LINEAR_CLAMPED},
    {"exponent", TAbstractAudio::DISTANCE_EXPONENT},
    {"exponentclamped", TAbstractAudio::DISTANCE_EXPONENT_CLAMPED}
};

TStringMap<TAbstractAudio::DistanceModel, TAbstractAudio::DISTANCE_MAX_ENUM> TAbstractAudio::distanceModels(TAbstractAudio::distanceModelEntries, sizeof(TAbstractAudio::distanceModelEntries));

bool TAbstractAudio::getConstant(const char *in, DistanceModel &out)
{
	return distanceModels.find(in, out);
}

bool TAbstractAudio::getConstant(DistanceModel in, const char  *&out)
{
	return distanceModels.find(in, out);
}
