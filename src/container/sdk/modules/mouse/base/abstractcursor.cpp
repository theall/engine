/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "abstractcursor.h"

TAbstractCursor::~TAbstractCursor()
{
}

bool TAbstractCursor::getConstant(const char *in, SystemCursor &out)
{
	return systemCursors.find(in, out);
}

bool TAbstractCursor::getConstant(SystemCursor in, const char *&out)
{
	return systemCursors.find(in, out);
}

bool TAbstractCursor::getConstant(const char *in, CursorType &out)
{
	return types.find(in, out);
}

bool TAbstractCursor::getConstant(CursorType in, const char *&out)
{
	return types.find(in, out);
}

TStringMap<TAbstractCursor::SystemCursor, TAbstractCursor::CURSOR_MAX_ENUM>::Entry TAbstractCursor::systemCursorEntries[] =
{
    {"arrow", TAbstractCursor::CURSOR_ARROW},
    {"ibeam", TAbstractCursor::CURSOR_IBEAM},
    {"wait", TAbstractCursor::CURSOR_WAIT},
    {"crosshair", TAbstractCursor::CURSOR_CROSSHAIR},
    {"waitarrow", TAbstractCursor::CURSOR_WAITARROW},
    {"sizenwse", TAbstractCursor::CURSOR_SIZENWSE},
    {"sizenesw", TAbstractCursor::CURSOR_SIZENESW},
    {"sizewe", TAbstractCursor::CURSOR_SIZEWE},
    {"sizens", TAbstractCursor::CURSOR_SIZENS},
    {"sizeall", TAbstractCursor::CURSOR_SIZEALL},
    {"no", TAbstractCursor::CURSOR_NO},
    {"hand", TAbstractCursor::CURSOR_HAND},
};

TStringMap<TAbstractCursor::SystemCursor, TAbstractCursor::CURSOR_MAX_ENUM> TAbstractCursor::systemCursors(TAbstractCursor::systemCursorEntries, sizeof(TAbstractCursor::systemCursorEntries));

TStringMap<TAbstractCursor::CursorType, TAbstractCursor::CURSORTYPE_MAX_ENUM>::Entry TAbstractCursor::typeEntries[] =
{
    {"system", TAbstractCursor::CURSORTYPE_SYSTEM},
    {"image", TAbstractCursor::CURSORTYPE_IMAGE},
};

TStringMap<TAbstractCursor::CursorType, TAbstractCursor::CURSORTYPE_MAX_ENUM> TAbstractCursor::types(TAbstractCursor::typeEntries, sizeof(TAbstractCursor::typeEntries));
