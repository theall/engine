/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_MOUSE_MOUSE_H
#define THEALL_MOUSE_MOUSE_H

#include "common/object.h"

#include "abstractcursor.h"

#include "modules/image/imagedata.h"

// C++
#include <vector>

class TMouseBase : public TObject
{
public:
    virtual ~TMouseBase() {}

    virtual TAbstractCursor *newCursor(TImageData *data, int hotx, int hoty) = 0;
    virtual TAbstractCursor *getSystemCursor(TAbstractCursor::SystemCursor cursortype) = 0;

    virtual void setCursor(TAbstractCursor *cursor) = 0;
	virtual void setCursor() = 0;

    virtual TAbstractCursor *getCursor() const = 0;

	virtual bool hasCursor() const = 0;

	virtual double getX() const = 0;
	virtual double getY() const = 0;
	virtual void getPosition(double &x, double &y) const = 0;
	virtual void setX(double x) = 0;
	virtual void setY(double y) = 0;
	virtual void setPosition(double x, double y) = 0;
	virtual void setVisible(bool visible) = 0;
	virtual bool isDown(const std::vector<int> &buttons) const = 0;
	virtual bool isVisible() const = 0;
	virtual void setGrabbed(bool grab) = 0;
	virtual bool isGrabbed() const = 0;
	virtual bool setRelativeMode(bool relative) = 0;
	virtual bool getRelativeMode() const = 0;

}; // Mouse




#endif // THEALL_MOUSE_MOUSE_H
