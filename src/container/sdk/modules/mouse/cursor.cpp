/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "cursor.h"
#include "common/config.h"

TCursor::TCursor(TImageData *data, int hotx, int hoty)
	: cursor(nullptr)
	, type(CURSORTYPE_IMAGE)
	, systemType(CURSOR_MAX_ENUM)
{
	Uint32 rmask, gmask, bmask, amask;
#ifdef THEALL_BIG_ENDIAN
	rmask = 0xFF000000;
	gmask = 0x00FF0000;
	bmask = 0x0000FF00;
	amask = 0x000000FF;
#else
	rmask = 0x000000FF;
	gmask = 0x0000FF00;
	bmask = 0x00FF0000;
	amask = 0xFF000000;
#endif

	int w = data->getWidth();
	int h = data->getHeight();
	int pitch = w * 4;

	SDL_Surface *surface = SDL_CreateRGBSurfaceFrom(data->getData(), w, h, 32, pitch, rmask, gmask, bmask, amask);
	if(!surface)
		throw TException("Cannot create cursor: out of memory!");

	cursor = SDL_CreateColorCursor(surface, hotx, hoty);
	SDL_FreeSurface(surface);

	if(!cursor)
		throw TException("Cannot create cursor: %s", SDL_GetError());
}

TCursor::TCursor(TCursor::SystemCursor cursortype)
	: cursor(nullptr)
	, type(CURSORTYPE_SYSTEM)
	, systemType(cursortype)
{
	SDL_SystemCursor sdlcursortype;

	if(systemCursors.find(cursortype, sdlcursortype))
		cursor = SDL_CreateSystemCursor(sdlcursortype);
	else
		throw TException("Cannot create system cursor: invalid type.");

	if(!cursor)
		throw TException("Cannot create system cursor: %s", SDL_GetError());
}

TCursor::~TCursor()
{
	if(cursor)
		SDL_FreeCursor(cursor);
}

void *TCursor::getHandle() const
{
	return cursor;
}

TCursor::CursorType TCursor::getType() const
{
	return type;
}

TCursor::SystemCursor TCursor::getSystemType() const
{
	return systemType;
}

TEnumMap<TCursor::SystemCursor, SDL_SystemCursor, TCursor::CURSOR_MAX_ENUM>::Entry TCursor::systemCursorEntries[] =
{
    {TCursor::CURSOR_ARROW, SDL_SYSTEM_CURSOR_ARROW},
    {TCursor::CURSOR_IBEAM, SDL_SYSTEM_CURSOR_IBEAM},
    {TCursor::CURSOR_WAIT, SDL_SYSTEM_CURSOR_WAIT},
    {TCursor::CURSOR_CROSSHAIR, SDL_SYSTEM_CURSOR_CROSSHAIR},
    {TCursor::CURSOR_WAITARROW, SDL_SYSTEM_CURSOR_WAITARROW},
    {TCursor::CURSOR_SIZENWSE, SDL_SYSTEM_CURSOR_SIZENWSE},
    {TCursor::CURSOR_SIZENESW, SDL_SYSTEM_CURSOR_SIZENESW},
    {TCursor::CURSOR_SIZEWE, SDL_SYSTEM_CURSOR_SIZEWE},
    {TCursor::CURSOR_SIZENS, SDL_SYSTEM_CURSOR_SIZENS},
    {TCursor::CURSOR_SIZEALL, SDL_SYSTEM_CURSOR_SIZEALL},
    {TCursor::CURSOR_NO, SDL_SYSTEM_CURSOR_NO},
    {TCursor::CURSOR_HAND, SDL_SYSTEM_CURSOR_HAND},
};

TEnumMap<TCursor::SystemCursor, SDL_SystemCursor, TCursor::CURSOR_MAX_ENUM> TCursor::systemCursors(TCursor::systemCursorEntries, sizeof(TCursor::systemCursorEntries));
