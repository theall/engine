/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/


#include "mouse.h"
#include "modules/window/window.h"
#include "modules/filesystem/filesystem.h"
#include "modules/image/image.h"

// SDL
#include <SDL2/SDL_mouse.h>


// SDL reports mouse coordinates in the window coordinate system in OS X, but
// we want them in pixel coordinates (may be different with high-DPI enabled.)
static void windowToPixelCoords(double *x, double *y)
{
    TWindow::instance()->windowToPixelCoords(x, y);
}

// And vice versa for setting mouse coordinates.
static void pixelToWindowCoords(double *x, double *y)
{
    TWindow::instance()->pixelToWindowCoords(x, y);
}

TMouse *TMouse::mInstance=NULL;

TMouse::TMouse()
	: curCursor(nullptr)
{
}

TMouse::~TMouse()
{
	if(curCursor.get())
		setCursor();

	for (auto &c : systemCursors)
		c.second->release();
}

TCursor *TMouse::newCursor(TImageData *data, int hotx, int hoty)
{
    return new TCursor(data, hotx, hoty);
}

TCursor *TMouse::newCursor(const std::string &file, int hotx, int hoty)
{
    TFileData *fileData = TFileSystem::instance()->newFileData(file);
    TImageData *imageData = TImage::instance()->newImageData(fileData);
    fileData->release();
    return new TCursor(imageData, hotx, hoty);
}

TCursor *TMouse::getSystemCursor(TCursor::SystemCursor cursortype)
{
	TCursor *cursor = nullptr;
	auto it = systemCursors.find(cursortype);

	if(it != systemCursors.end())
		cursor = it->second;
	else
	{
		cursor = new TCursor(cursortype);
		systemCursors[cursortype] = cursor;
	}

	return cursor;
}

void TMouse::setCursor(TCursor *cursor)
{
    if(!cursor)
        return;

	curCursor.set(cursor);
    SDL_SetCursor((SDL_Cursor *) cursor->getHandle());
}

void TMouse::setCursor(const std::string &file, int hotx, int hoty)
{
    setCursor(newCursor(file, hotx, hoty));
}

void TMouse::setCursor()
{
	curCursor.set(nullptr);
	SDL_SetCursor(SDL_GetDefaultCursor());
}

TCursor *TMouse::getCursor() const
{
	return curCursor.get();
}


bool TMouse::hasCursor() const
{
	return SDL_GetDefaultCursor() != nullptr;
}

double TMouse::getX() const
{
	int x;
	SDL_GetMouseState(&x, nullptr);

	double dx = (double) x;
	windowToPixelCoords(&dx, nullptr);

	return dx;
}

double TMouse::getY() const
{
	int y;
	SDL_GetMouseState(nullptr, &y);

	double dy = (double) y;
	windowToPixelCoords(nullptr, &dy);

	return dy;
}

void TMouse::getPosition(double &x, double &y) const
{
	int mx, my;
	SDL_GetMouseState(&mx, &my);

	x = (double) mx;
	y = (double) my;
	windowToPixelCoords(&x, &y);
}

void TMouse::setPosition(double x, double y)
{
    SDL_Window *handle = (SDL_Window *) TWindow::instance()->getHandle();
    pixelToWindowCoords(&x, &y);
    SDL_WarpMouseInWindow(handle, (int) x, (int) y);

    // SDL_WarpMouse doesn't directly update SDL's internal mouse state in Linux
    // and Windows, so we call SDL_PumpEvents now to make sure the next
    // getPosition call always returns the updated state.
    SDL_PumpEvents();
}

void TMouse::setX(double x)
{
	setPosition(x, getY());
}

void TMouse::setY(double y)
{
	setPosition(getX(), y);
}

void TMouse::setVisible(bool visible)
{
	SDL_ShowCursor(visible ? SDL_ENABLE : SDL_DISABLE);
}

bool TMouse::isDown(const std::vector<Button> &buttons) const
{
	Uint32 buttonstate = SDL_GetMouseState(nullptr, nullptr);

    for (Button button : buttons)
	{
		if(button <= 0)
			continue;

		// We use button index 2 to represent the right mouse button, but SDL
		// uses 2 to represent the middle mouse button.
		switch (button)
		{
        case Right:
            button = (Button)SDL_BUTTON_RIGHT;
			break;
        case Middle:
            button = (Button)SDL_BUTTON_MIDDLE;
			break;
        case Left:
        case X1:
        case X2:
        default:
            break;
		}

        if(buttonstate & SDL_BUTTON((int)button))
			return true;
	}

    return false;
}

bool TMouse::isLeftButtonDown() const
{
    return SDL_GetMouseState(nullptr, nullptr) & SDL_BUTTON(SDL_BUTTON_LEFT);
}

bool TMouse::isVisible() const
{
	return SDL_ShowCursor(SDL_QUERY) == SDL_ENABLE;
}

void TMouse::setGrabbed(bool grab)
{
    TWindow::instance()->setMouseGrab(grab);
}

bool TMouse::isGrabbed() const
{
    return TWindow::instance()->isMouseGrabbed();
}

bool TMouse::setRelativeMode(bool relative)
{
	return SDL_SetRelativeMouseMode(relative ? SDL_TRUE : SDL_FALSE) == 0;
}

bool TMouse::getRelativeMode() const
{
	return SDL_GetRelativeMouseMode() != SDL_FALSE;
}
