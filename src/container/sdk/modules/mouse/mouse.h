/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_MOUSE_SDL_MOUSE_H
#define THEALL_MOUSE_SDL_MOUSE_H


#include "modules/image/imagedata.h"

#include "common/object.h"

#include "cursor.h"

// C++
#include <map>


class TMouse : public TObject
{
    DECLARE_INSTANCE(TMouse)

public:
    enum Button
    {
        Left = 1,
        Right,
        Middle,
        X1,
        X2
    };

    TMouse();
    virtual ~TMouse();

    TCursor *newCursor(TImageData *data, int hotx, int hoty);
    TCursor *newCursor(const std::string &file, int hotx = 0, int hoty = 0);
    TCursor *getSystemCursor(TCursor::SystemCursor cursortype);

    void setCursor(TCursor *cursor);
    void setCursor(const std::string &file, int hotx = 0, int hoty = 0);
    void setCursor();

    TCursor *getCursor() const;

    bool hasCursor() const;

    double getX() const;
    double getY() const;
    void getPosition(double &x, double &y) const;
    void setX(double x);
    void setY(double y);
    void setPosition(double x, double y);
    void setVisible(bool visible);
    bool isDown(const std::vector<Button> &buttons) const;
    bool isLeftButtonDown() const;

    bool isVisible() const;
    void setGrabbed(bool grab);
    bool isGrabbed() const;
    bool setRelativeMode(bool relative);
    bool getRelativeMode() const;

private:

    StrongRef<TCursor> curCursor;

	std::map<TCursor::SystemCursor, TCursor *> systemCursors;

}; // Mouse





#endif // THEALL_MOUSE_SDL_MOUSE_H
