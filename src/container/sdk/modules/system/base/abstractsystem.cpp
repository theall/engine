/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/


#include "common/config.h"

#include "abstractsystem.h"

#if defined(THEALL_MACOSX)
#include <CoreServices/CoreServices.h>
#elif defined(THEALL_IOS)
#include "common/ios.h"
#elif defined(THEALL_LINUX) || defined(THEALL_ANDROID)
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>
#elif defined(THEALL_WINDOWS)
#include "common/utf8.h"
#include <shlobj.h>
#include <shellapi.h>
#ifndef __MINGW32__
#pragma comment(lib, "shell32.lib")
#endif
#endif
#if defined(THEALL_ANDROID)
#include "common/android.h"
#elif defined(THEALL_LINUX)
#include <spawn.h>
#endif

#if defined(THEALL_LINUX)
static void sigchld_handler(int sig)
{
	// Because waitpid can set errno, we need to save it.
	auto old = errno;

	// Reap whilst there are children waiting to be reaped.
	while (waitpid(-1, nullptr, WNOHANG) > 0)
		;

	errno = old;
}
#endif


TAbstractSystem::TAbstractSystem()
{
#if defined(THEALL_LINUX)
	// Enable automatic cleanup of zombie processes
	// NOTE: We're using our own handler, instead of SA_NOCLDWAIT because the
	// latter breaks wait, and thus os.execute.
	// NOTE: This isn't perfect, due to multithreading our SIGCHLD can happen
	// on a different thread than the one calling wait(), thus causing a race.
	struct sigaction act = {0};
	sigemptyset(&act.sa_mask);
	act.sa_handler = sigchld_handler;
	act.sa_flags = SA_RESTART;
	sigaction(SIGCHLD, &act, nullptr);
#endif
}

std::string TAbstractSystem::getOS() const
{
#if defined(THEALL_MACOSX)
	return "OS X";
#elif defined(THEALL_IOS)
	return "iOS";
#elif defined(THEALL_WINDOWS_UWP)
	return "UWP";
#elif defined(THEALL_WINDOWS)
	return "Windows";
#elif defined(THEALL_ANDROID)
	return "Android";
#elif defined(THEALL_LINUX)
	return "Linux";
#else
	return "Unknown";
#endif
}

extern "C"
{
	extern char **environ; // The environment, always available
}

bool TAbstractSystem::openURL(const std::string &url) const
{

#if defined(THEALL_MACOSX)

	bool success = false;
	CFURLRef cfurl = CFURLCreateWithBytes(nullptr,
	                                      (const UInt8 *) url.c_str(),
	                                      url.length(),
	                                      kCFStringEncodingUTF8,
	                                      nullptr);

	success = LSOpenCFURLRef(cfurl, nullptr) == noErr;
	CFRelease(cfurl);
	return success;

#elif defined(THEALL_IOS)

	return ios::openURL(url);

#elif defined(THEALL_ANDROID)

	return android::openURL(url);

#elif defined(THEALL_LINUX)

	pid_t pid;
	const char *argv[] = {"xdg-open", url.c_str(), nullptr};

	// Note: at the moment this process inherits our file descriptors.
	// Note: the below const_cast is really ugly as well.
	if(posix_spawnp(&pid, "xdg-open", nullptr, nullptr, const_cast<char **>(argv), environ) != 0)
		return false;

	// Check if xdg-open already completed (or failed.)
	int status = 0;
	if(waitpid(pid, &status, WNOHANG) > 0)
		return (status == 0);
	else
		// We can't tell what actually happens without waiting for
		// the process to finish, which could take forever (literally).
		return true;

#elif defined(THEALL_WINDOWS)

	// Unicode-aware WinAPI functions don't accept UTF-8, so we need to convert.
	std::wstring wurl = to_widestr(url);

	HINSTANCE result = 0;

#if defined(THEALL_WINDOWS_UWP)
	
	Platform::String^ urlString = ref new Platform::String(wurl.c_str());
	auto uwpUri = ref new Windows::Foundation::Uri(urlString);
	Windows::System::Launcher::LaunchUriAsync(uwpUri);

#else

	result = ShellExecuteW(nullptr,
		L"open",
		wurl.c_str(),
		nullptr,
		nullptr,
		SW_SHOW);

#endif

	return (int) result > 32;

#endif
}

void TAbstractSystem::vibrate(double seconds) const
{
#ifdef THEALL_ANDROID
	android::vibrate(seconds);
#elif defined(THEALL_IOS)
	ios::vibrate();
#else
	THEALL_UNUSED(seconds);
#endif
}

bool TAbstractSystem::getConstant(const char *in, TAbstractSystem::PowerState &out)
{
	return powerStates.find(in, out);
}

bool TAbstractSystem::getConstant(TAbstractSystem::PowerState in, const char *&out)
{
	return powerStates.find(in, out);
}

TStringMap<TAbstractSystem::PowerState, TAbstractSystem::POWER_MAX_ENUM>::Entry TAbstractSystem::powerEntries[] =
{
    {"unknown", TAbstractSystem::POWER_UNKNOWN},
    {"battery", TAbstractSystem::POWER_BATTERY},
    {"nobattery", TAbstractSystem::POWER_NO_BATTERY},
    {"charging", TAbstractSystem::POWER_CHARGING},
    {"charged", TAbstractSystem::POWER_CHARGED},
};

TStringMap<TAbstractSystem::PowerState, TAbstractSystem::POWER_MAX_ENUM> TAbstractSystem::powerStates(TAbstractSystem::powerEntries, sizeof(TAbstractSystem::powerEntries));



