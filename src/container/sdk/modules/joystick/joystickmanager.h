/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_JOYSTICK_SDL_JOYSTICK_MODULE_H
#define THEALL_JOYSTICK_SDL_JOYSTICK_MODULE_H

#include "base/abstractjoystickmanager.h"

#include "joystick.h"

// C++
#include <string>
#include <vector>
#include <list>
#include <map>

class TJoystickManager : public TAbstractJoystickManager
{
    DECLARE_INSTANCE(TJoystickManager)

public:

    TJoystickManager();
    virtual ~TJoystickManager();

	// Implements Module.
	const char *getName() const;

    // Implements TJoystickManager.
    TJoystick *addJoystick(int deviceindex);
    void removeJoystick(TJoystick *joystick);
    TJoystick *getJoystickFromID(int instanceid);
    TJoystick *getJoystick(int joyindex);
    int getIndex(const TJoystick *joystick);
	int getJoystickCount() const;

    bool setGamepadMapping(const std::string &guid, TJoystick::GamepadInput gpinput, TJoystick::JoystickInput joyinput);
    TJoystick::JoystickInput getGamepadMapping(const std::string &guid, TJoystick::GamepadInput gpinput);
	void loadGamepadMappings(const std::string &mappings);
	std::string saveGamepadMappings();

private:

    std::string stringFromGamepadInput(TJoystick::GamepadInput gpinput) const;
    TJoystick::JoystickInput JoystickInputFromString(const std::string &str) const;
	void removeBindFromMapString(std::string &mapstr, const std::string &joybindstr) const;

	void checkGamepads(const std::string &guid) const;

	// SDL2's GUIDs identify *classes* of devices, instead of unique devices.
	std::string getDeviceGUID(int deviceindex) const;

	// Lists of currently connected Joysticks.
    std::vector<TJoystick *> activeSticks;

	// Persistent list of all Joysticks which have been connected at some point.
    std::list<TJoystick *> joysticks;

	// Persistent map indicating GUIDs for Gamepads which have been connected or
	// modified at some point.
	std::map<std::string, bool> recentGamepadGUIDs;
}; // TJoystickManager


#endif // THEALL_JOYSTICK_SDL_JOYSTICK_MODULE_H
