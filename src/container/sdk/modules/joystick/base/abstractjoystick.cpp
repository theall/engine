/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "abstractjoystick.h"

// STL
#include <cmath>



float TAbstractJoystick::clampval(float x)
{
	if(fabsf(x) < 0.01)
		return 0.0f;

	if(x < -0.99f) return -1.0f;
	if(x > 0.99f) return 1.0f;

	return x;
}

bool TAbstractJoystick::getConstant(const char *in, TAbstractJoystick::Hat &out)
{
	return hats.find(in, out);
}

bool TAbstractJoystick::getConstant(TAbstractJoystick::Hat in, const char *&out)
{
	return hats.find(in, out);
}

bool TAbstractJoystick::getConstant(const char *in, TAbstractJoystick::GamepadAxis &out)
{
	return gpAxes.find(in, out);
}

bool TAbstractJoystick::getConstant(TAbstractJoystick::GamepadAxis in, const char *&out)
{
	return gpAxes.find(in, out);
}

bool TAbstractJoystick::getConstant(const char *in, TAbstractJoystick::GamepadButton &out)
{
	return gpButtons.find(in, out);
}

bool TAbstractJoystick::getConstant(TAbstractJoystick::GamepadButton in, const char *&out)
{
	return gpButtons.find(in, out);
}

bool TAbstractJoystick::getConstant(const char *in, TAbstractJoystick::InputType &out)
{
	return inputTypes.find(in, out);
}

bool TAbstractJoystick::getConstant(TAbstractJoystick::InputType in, const char *&out)
{
	return inputTypes.find(in, out);
}

TStringMap<TAbstractJoystick::Hat, TAbstractJoystick::HAT_MAX_ENUM>::Entry TAbstractJoystick::hatEntries[] =
{
    {"c", TAbstractJoystick::HAT_CENTERED},
    {"u", TAbstractJoystick::HAT_UP},
    {"r", TAbstractJoystick::HAT_RIGHT},
    {"d", TAbstractJoystick::HAT_DOWN},
    {"l", TAbstractJoystick::HAT_LEFT},
    {"ru", TAbstractJoystick::HAT_RIGHTUP},
    {"rd", TAbstractJoystick::HAT_RIGHTDOWN},
    {"lu", TAbstractJoystick::HAT_LEFTUP},
    {"ld", TAbstractJoystick::HAT_LEFTDOWN},
};

TStringMap<TAbstractJoystick::Hat, TAbstractJoystick::HAT_MAX_ENUM> TAbstractJoystick::hats(TAbstractJoystick::hatEntries, sizeof(TAbstractJoystick::hatEntries));

TStringMap<TAbstractJoystick::GamepadAxis, TAbstractJoystick::GAMEPAD_AXIS_MAX_ENUM>::Entry TAbstractJoystick::gpAxisEntries[] =
{
	{"leftx", GAMEPAD_AXIS_LEFTX},
	{"lefty", GAMEPAD_AXIS_LEFTY},
	{"rightx", GAMEPAD_AXIS_RIGHTX},
	{"righty", GAMEPAD_AXIS_RIGHTY},
	{"triggerleft", GAMEPAD_AXIS_TRIGGERLEFT},
	{"triggerright", GAMEPAD_AXIS_TRIGGERRIGHT},
};

TStringMap<TAbstractJoystick::GamepadAxis, TAbstractJoystick::GAMEPAD_AXIS_MAX_ENUM> TAbstractJoystick::gpAxes(TAbstractJoystick::gpAxisEntries, sizeof(TAbstractJoystick::gpAxisEntries));

TStringMap<TAbstractJoystick::GamepadButton, TAbstractJoystick::GAMEPAD_BUTTON_MAX_ENUM>::Entry TAbstractJoystick::gpButtonEntries[] =
{
	{"a", GAMEPAD_BUTTON_A},
	{"b", GAMEPAD_BUTTON_B},
	{"x", GAMEPAD_BUTTON_X},
	{"y", GAMEPAD_BUTTON_Y},
	{"back", GAMEPAD_BUTTON_BACK},
	{"guide", GAMEPAD_BUTTON_GUIDE},
	{"start", GAMEPAD_BUTTON_START},
	{"leftstick", GAMEPAD_BUTTON_LEFTSTICK},
	{"rightstick", GAMEPAD_BUTTON_RIGHTSTICK},
	{"leftshoulder", GAMEPAD_BUTTON_LEFTSHOULDER},
	{"rightshoulder", GAMEPAD_BUTTON_RIGHTSHOULDER},
	{"dpup", GAMEPAD_BUTTON_DPAD_UP},
	{"dpdown", GAMEPAD_BUTTON_DPAD_DOWN},
	{"dpleft", GAMEPAD_BUTTON_DPAD_LEFT},
	{"dpright", GAMEPAD_BUTTON_DPAD_RIGHT},
};

TStringMap<TAbstractJoystick::GamepadButton, TAbstractJoystick::GAMEPAD_BUTTON_MAX_ENUM> TAbstractJoystick::gpButtons(TAbstractJoystick::gpButtonEntries, sizeof(TAbstractJoystick::gpButtonEntries));

TStringMap<TAbstractJoystick::InputType, TAbstractJoystick::INPUT_TYPE_MAX_ENUM>::Entry TAbstractJoystick::inputTypeEntries[] =
{
    {"axis", TAbstractJoystick::INPUT_TYPE_AXIS},
    {"button", TAbstractJoystick::INPUT_TYPE_BUTTON},
    {"hat", TAbstractJoystick::INPUT_TYPE_HAT},
};

TStringMap<TAbstractJoystick::InputType, TAbstractJoystick::INPUT_TYPE_MAX_ENUM> TAbstractJoystick::inputTypes(TAbstractJoystick::inputTypeEntries, sizeof(TAbstractJoystick::inputTypeEntries));
