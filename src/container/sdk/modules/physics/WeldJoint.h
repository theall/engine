/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_PHYSICS_BOX2D_WELD_JOINT_H
#define THEALL_PHYSICS_BOX2D_WELD_JOINT_H

// Module
#include "joint.h"


/**
 * A WeldJoint essentially glues two bodies together.
 **/
class TWeldJoint : public TJoint
{
public:

	/**
	 * Creates a new WeldJoint connecting body1 and body2.
	 **/
	TWeldJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected);

	TWeldJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected, float referenceAngle);

	virtual ~TWeldJoint();

	/**
	 * Sets the response speed.
	 **/
	void setFrequency(float hz);

	/**
	 * Gets the response speed.
	 **/
	float getFrequency() const;

	/**
	 * Sets the damping ratio.
	 * 0 = no damping, 1 = critical damping.
	 **/
	void setDampingRatio(float d);

	/**
	 * Gets the damping ratio.
	 * 0 = no damping, 1 = critical damping.
	 **/
	float getDampingRatio() const;

	/**
	 * Gets the reference angle.
	 **/
	float getReferenceAngle() const;

private:

	// The Box2D weld joint object.
	b2WeldJoint *joint;

	void init(b2WeldJointDef &def, TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected);
};





#endif // THEALL_PHYSICS_BOX2D_WELD_JOINT_H
