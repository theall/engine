/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "shape.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"

#include "common/Memoizer.h"

// STD
#include <bitset>


TShape::TShape()
	: shape(NULL)
	, own(false)
{
}

TShape::TShape(b2Shape *shape, bool own)
	: shape(shape)
	, own(own)
{
	if (own)
		TMemoizer::add(shape, this);
}

TShape::~TShape()
{
	if (shape && own)
	{
		TMemoizer::remove(shape);
		delete shape;
	}
	shape = 0;
}

TShape::Type TShape::getType() const
{
	switch (shape->GetType())
	{
	case b2Shape::e_circle:
		return SHAPE_CIRCLE;
	case b2Shape::e_polygon:
		return SHAPE_POLYGON;
	case b2Shape::e_edge:
		return SHAPE_EDGE;
	case b2Shape::e_chain:
		return SHAPE_CHAIN;
	default:
		return SHAPE_INVALID;
	}
}

float TShape::getRadius() const
{
    return TPhysics::scaleUp(shape->m_radius);
}

int TShape::getChildCount() const
{
	return shape->GetChildCount();
}

bool TShape::testPoint(float x, float y, float r, float px, float py) const
{
	b2Vec2 point(px, py);
    b2Transform transform(TPhysics::scaleDown(b2Vec2(x, y)), b2Rot(r));
    return shape->TestPoint(transform, TPhysics::scaleDown(point));
}

bool TShape::rayCast(b2RayCastOutput* output, const b2Vec2 &p1, const b2Vec2 &p2, float maxFraction, const b2Vec2& position, const b2Rot &rotation, int childIndex) const
{
	b2RayCastInput input;
    input.p1 = p1;
    input.p2 = p2;
	input.maxFraction = maxFraction;
    b2Transform transform(TPhysics::scaleDown(position), rotation);
    if (!shape->RayCast(output, input, transform, childIndex))
        return false; // No hit.
    return true;
}

b2AABB TShape::computeAABB(const b2Vec2 &position, const b2Rot &rotation, int childIndex) const
{
    b2Transform transform(position, rotation);
	b2AABB box;
	shape->ComputeAABB(&box, transform, childIndex);
    return TPhysics::scaleUp(box);
}

b2MassData TShape::computeMass(float density) const
{
	b2MassData data;
	shape->ComputeMass(&data, density);
    data.center = TPhysics::scaleUp(data.center);
//	lua_pushnumber(L, center.x);
//	lua_pushnumber(L, center.y);
//	lua_pushnumber(L, data.mass);
//	lua_pushnumber(L, Physics::scaleUp(Physics::scaleUp(data.I)));
    return data;
}




