/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "joint.h"

// STD
#include <bitset>


#include "common/Memoizer.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"

TJoint::TJoint(TBody *body1) :
    world(body1->world)
  , body1(body1)
  , body2(nullptr)
{

}

TJoint::TJoint(TBody *body1, TBody *body2) :
    world(body1->world)
  , body1(body1)
  , body2(body2)
{

}

TJoint::~TJoint()
{

}

TJoint::Type TJoint::getType() const
{
	switch (joint->GetType())
	{
	case e_revoluteJoint:
		return JOINT_REVOLUTE;
	case e_prismaticJoint:
		return JOINT_PRISMATIC;
	case e_distanceJoint:
		return JOINT_DISTANCE;
	case e_pulleyJoint:
		return JOINT_PULLEY;
	case e_mouseJoint:
		return JOINT_MOUSE;
	case e_gearJoint:
		return JOINT_GEAR;
	case e_frictionJoint:
		return JOINT_FRICTION;
	case e_weldJoint:
		return JOINT_WELD;
	case e_wheelJoint:
		return JOINT_WHEEL;
	case e_ropeJoint:
		return JOINT_ROPE;
	case e_motorJoint:
		return JOINT_MOTOR;
	default:
		return JOINT_INVALID;
	}
}

TBody *TJoint::getBodyA() const
{
	b2Body *b2body = joint->GetBodyA();
	if (b2body == nullptr)
		return nullptr;

	TBody *body = (TBody *) TMemoizer::find(b2body);
	if (body == nullptr)
		throw TException("A body has escaped Memoizer!");

	return body;
}

TBody *TJoint::getBodyB() const
{
	b2Body *b2body = joint->GetBodyB();
	if (b2body == nullptr)
		return nullptr;

	TBody *body = (TBody *) TMemoizer::find(b2body);
	if (body == nullptr)
		throw TException("A body has escaped Memoizer!");

	return body;
}

bool TJoint::isValid() const
{
	return joint != 0;
}

void TJoint::getAnchors(b2Vec2 &anchorA, b2Vec2 &anchorB)
{
    anchorA = TPhysics::scaleUp(joint->GetAnchorA());
    anchorB = TPhysics::scaleUp(joint->GetAnchorB());
}

b2Vec2 TJoint::getReactionForce(float dt)
{
    return  TPhysics::scaleUp(joint->GetReactionForce(dt));
}

float TJoint::getReactionTorque(float dt)
{
    return TPhysics::scaleUp(TPhysics::scaleUp(joint->GetReactionTorque(dt)));
}

b2Joint *TJoint::createJoint(b2JointDef *def)
{
    if(!def)
        return nullptr;

    def->userData = getUserData();
	joint = world->world->CreateJoint(def);
	TMemoizer::add(joint, this);
	// Box2D joint has a reference to this love Joint.
	this->retain();
	return joint;
}

void TJoint::destroyJoint(bool implicit)
{
	if (world->world->IsLocked())
	{
		// Called during time step. Save reference for destruction afterwards.
		this->retain();
		world->destructJoints.push_back(this);
		return;
	}

	if (!implicit && joint != 0)
		world->world->DestroyJoint(joint);
	TMemoizer::remove(joint);
	joint = NULL;
	// Release the reference of the Box2D joint.
    this->release();
}

void TJoint::setUserData(void *userData)
{
    if(joint)
        joint->SetUserData(userData);
}

void *TJoint::getUserData() const
{
    return joint?joint->GetUserData():nullptr;
}

bool TJoint::isActive() const
{
	return joint->IsActive();
}

bool TJoint::getCollideConnected() const
{
	return joint->GetCollideConnected();
}
