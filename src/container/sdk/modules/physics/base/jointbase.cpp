/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "jointbase.h"

TJointBase::~TJointBase()
{
}

bool TJointBase::getConstant(const char *in, Type &out)
{
	return types.find(in, out);
}

bool TJointBase::getConstant(Type in, const char  *&out)
{
	return types.find(in, out);
}

TStringMap<TJointBase::Type, TJointBase::JOINT_MAX_ENUM>::Entry TJointBase::typeEntries[] =
{
    {"distance", TJointBase::JOINT_DISTANCE},
    {"revolute", TJointBase::JOINT_REVOLUTE},
    {"prismatic", TJointBase::JOINT_PRISMATIC},
    {"mouse", TJointBase::JOINT_MOUSE},
    {"pulley", TJointBase::JOINT_PULLEY},
    {"gear", TJointBase::JOINT_GEAR},
    {"friction", TJointBase::JOINT_FRICTION},
    {"weld", TJointBase::JOINT_WELD},
    {"wheel", TJointBase::JOINT_WHEEL},
    {"rope", TJointBase::JOINT_ROPE},
    {"motor", TJointBase::JOINT_MOTOR},
};

TStringMap<TJointBase::Type, TJointBase::JOINT_MAX_ENUM> TJointBase::types(TJointBase::typeEntries, sizeof(TJointBase::typeEntries));
