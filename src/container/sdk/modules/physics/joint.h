/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_PHYSICS_BOX2D_JOINT_H
#define THEALL_PHYSICS_BOX2D_JOINT_H

#include "common/object.h"

// Box2D
#include "Box2D/Box2D.h"


// Forward declarations.
class TBody;
class TWorld;

/**
 * A Joint acts as positioning constraints on Bodies.
 * A Joint can be used to prevent Bodies from going to
 * far apart, or coming too close together.
 **/
class TJoint : public TObject
{
public:
    enum Type
    {
        JOINT_INVALID,
        JOINT_DISTANCE,
        JOINT_REVOLUTE,
        JOINT_PRISMATIC,
        JOINT_MOUSE,
        JOINT_PULLEY,
        JOINT_GEAR,
        JOINT_FRICTION,
        JOINT_WELD,
        JOINT_WHEEL,
        JOINT_ROPE,
        JOINT_MOTOR,
        JOINT_MAX_ENUM
    };

    friend class TGearJoint;

    /**
     * This constructor will connect one end of the joint to body1,
     * and the other one to the default ground body.
     *
     * This constructor is mainly used by TMouseJoint.
     **/
    TJoint(TBody *body1);

    /**
     * Create a joint between body1 and body2.
     **/
    TJoint(TBody *body1, TBody *body2);

    virtual ~TJoint();

    /**
     * Returns true if the joint is active in a Box2D world.
     **/
    bool isValid() const;

    /**
     * Gets the type of joint.
     **/
    Type getType() const;

    virtual TBody *getBodyA() const;
    virtual TBody *getBodyB() const;

    /**
     * Gets the anchor positions of the Joint in world
     * coordinates. This is useful for debugdrawing the joint.
     **/
    void getAnchors(b2Vec2 &anchorA, b2Vec2 &anchorB);

    /**
     * Gets the reaction force on body2 at the joint anchor.
     **/
    b2Vec2 getReactionForce(float dt);

    /**
     * Gets the reaction torque on body2.
     **/
    float getReactionTorque(float dt);

    bool isActive() const;

    bool getCollideConnected() const;

    /**
     * Joints require pointers to a Box2D joint objects at
     * different polymorphic levels, which is why these function
     * were created.
     **/

    /**
     * Destroys the joint. This function was created just to
     * get some cinsistency.
     **/
    void destroyJoint(bool implicit = false);

    void setUserData(void *userData);
    void *getUserData() const;

protected:

    /**
     * Creates a Joint, and ensures that the parent class
     * gets a copy of the pointer.
     **/
    b2Joint *createJoint(b2JointDef *def);

    TWorld *world;

private:
    // A Joint must be destroyed *before* the bodies it acts upon,
    // and the world they reside in. We therefore need refs
    // parents and associations to prevent wrong destruction order.
    TBody *body1, * body2;

    // The Box2D joint object.
    b2Joint *joint;

};

#endif // THEALL_PHYSICS_BOX2D_JOINT_H
