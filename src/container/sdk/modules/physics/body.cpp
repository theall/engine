/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "body.h"

#include "common/math.h"
#include "common/Memoizer.h"

#include "shape.h"
#include "fixture.h"
#include "world.h"
#include "physics.h"

// Needed for luax_pushjoint.
//#include "wrap_joint.h"


TBody::TBody(TWorld *world, b2Vec2 p, TBody::Type type) :
    world(world)
{
	b2BodyDef def;
    def.position = TPhysics::scaleDown(p);
    def.userData = (void *) nullptr;
	body = world->world->CreateBody(&def);
	// Box2D body holds a reference to the love Body.
	this->retain();
	this->setType(type);
    TMemoizer::add(body, this);
}

TBody::TBody(b2Body *b) : body(b)
{
    world = (TWorld *) TMemoizer::find(b->GetWorld());
	// Box2D body holds a reference to the love Body.
	this->retain();
    TMemoizer::add(body, this);
}

TBody::~TBody()
{
}

float TBody::getX()
{
    return TPhysics::scaleUp(body->GetPosition().x);
}

float TBody::getY()
{
    return TPhysics::scaleUp(body->GetPosition().y);
}

void TBody::getPosition(float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetPosition());
	x_o = v.x;
	y_o = v.y;
}

void TBody::getLinearVelocity(float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetLinearVelocity());
	x_o = v.x;
	y_o = v.y;
}

float TBody::getAngle()
{
	return body->GetAngle();
}

void TBody::getWorldCenter(float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetWorldCenter());
	x_o = v.x;
	y_o = v.y;
}

void TBody::getLocalCenter(float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetLocalCenter());
	x_o = v.x;
	y_o = v.y;
}

float TBody::getAngularVelocity() const
{
	return body->GetAngularVelocity();
}

float TBody::getMass() const
{
	return body->GetMass();
}

float TBody::getInertia() const
{
    return TPhysics::scaleUp(TPhysics::scaleUp(body->GetInertia()));
}

b2MassData TBody::getMassData()
{
    b2MassData data;
    body->GetMassData(&data);
//	b2Vec2 center = Physics::scaleUp(data.center);
//	lua_pushnumber(L, center.x);
//	lua_pushnumber(L, center.y);
//	lua_pushnumber(L, data.mass);
//	lua_pushnumber(L, Physics::scaleUp(Physics::scaleUp(data.I)));
    return data;
}

float TBody::getAngularDamping() const
{
	return body->GetAngularDamping();
}

float TBody::getLinearDamping() const
{
	return body->GetLinearDamping();
}

float TBody::getGravityScale() const
{
	return body->GetGravityScale();
}

TBody::Type TBody::getType() const
{
	switch (body->GetType())
	{
	case b2_staticBody:
		return BODY_STATIC;
		break;
	case b2_dynamicBody:
		return BODY_DYNAMIC;
		break;
	case b2_kinematicBody:
		return BODY_KINEMATIC;
		break;
	default:
		return BODY_INVALID;
		break;
	}
}

void TBody::applyLinearImpulse(float jx, float jy, bool wake)
{
    body->ApplyLinearImpulse(TPhysics::scaleDown(b2Vec2(jx, jy)), body->GetWorldCenter(), wake);
}

void TBody::applyLinearImpulse(float jx, float jy, float rx, float ry, bool wake)
{
    body->ApplyLinearImpulse(TPhysics::scaleDown(b2Vec2(jx, jy)), TPhysics::scaleDown(b2Vec2(rx, ry)), wake);
}

void TBody::applyAngularImpulse(float impulse, bool wake)
{
	// Angular impulse is in kg*m^2/s, meaning it needs to be scaled twice
    body->ApplyAngularImpulse(TPhysics::scaleDown(TPhysics::scaleDown(impulse)), wake);
}

void TBody::applyTorque(float t, bool wake)
{
	// Torque is in N*m, or kg*m^2/s^2, meaning it also needs to be scaled twice
    body->ApplyTorque(TPhysics::scaleDown(TPhysics::scaleDown(t)), wake);
}

void TBody::applyForce(float fx, float fy, float rx, float ry, bool wake)
{
    body->ApplyForce(TPhysics::scaleDown(b2Vec2(fx, fy)), TPhysics::scaleDown(b2Vec2(rx, ry)), wake);
}

void TBody::applyForce(float fx, float fy, bool wake)
{
    body->ApplyForceToCenter(TPhysics::scaleDown(b2Vec2(fx, fy)), wake);
}

void TBody::setX(float x)
{
    body->SetTransform(TPhysics::scaleDown(b2Vec2(x, getY())), getAngle());
}

void TBody::setY(float y)
{
    body->SetTransform(TPhysics::scaleDown(b2Vec2(getX(), y)), getAngle());
}

void TBody::setLinearVelocity(float x, float y)
{
    body->SetLinearVelocity(TPhysics::scaleDown(b2Vec2(x, y)));
}

void TBody::setAngle(float d)
{
	body->SetTransform(body->GetPosition(), d);
}

void TBody::setAngularVelocity(float r)
{
	body->SetAngularVelocity(r);
}

void TBody::setPosition(float x, float y)
{
    body->SetTransform(TPhysics::scaleDown(b2Vec2(x, y)), body->GetAngle());
}

void TBody::setAngularDamping(float d)
{
	body->SetAngularDamping(d);
}

void TBody::setLinearDamping(float d)
{
	body->SetLinearDamping(d);
}

void TBody::resetMassData()
{
	body->ResetMassData();
}

void TBody::setMassData(float x, float y, float m, float i)
{
	b2MassData massData;
    massData.center = TPhysics::scaleDown(b2Vec2(x, y));
	massData.mass = m;
    massData.I = TPhysics::scaleDown(TPhysics::scaleDown(i));
	body->SetMassData(&massData);
}

void TBody::setMass(float m)
{
	b2MassData data;
	body->GetMassData(&data);
	data.mass = m;
	body->SetMassData(&data);
}

void TBody::setInertia(float i)
{
	b2MassData massData;
	massData.center = body->GetLocalCenter();
	massData.mass = body->GetMass();
    massData.I = TPhysics::scaleDown(TPhysics::scaleDown(i));
	body->SetMassData(&massData);
}

void TBody::setGravityScale(float scale)
{
	body->SetGravityScale(scale);
}

void TBody::setType(TBody::Type type)
{
	switch (type)
	{
    case TBody::BODY_STATIC:
		body->SetType(b2_staticBody);
		break;
    case TBody::BODY_DYNAMIC:
		body->SetType(b2_dynamicBody);
		break;
    case TBody::BODY_KINEMATIC:
		body->SetType(b2_kinematicBody);
		break;
	default:
		break;
	}
}

void TBody::getWorldPoint(float x, float y, float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetWorldPoint(TPhysics::scaleDown(b2Vec2(x, y))));
	x_o = v.x;
	y_o = v.y;
}

void TBody::getWorldVector(float x, float y, float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetWorldVector(TPhysics::scaleDown(b2Vec2(x, y))));
	x_o = v.x;
	y_o = v.y;
}

std::vector<b2Vec2> TBody::getWorldPoints(std::vector<b2Vec2> pointsList)
{
    std::vector<b2Vec2> ret;
    for (int i = 0; i<pointsList.size(); i++)
    {
        ret.emplace_back(TPhysics::scaleUp(body->GetWorldPoint(TPhysics::scaleDown(pointsList[i]))));
    }

    return ret;
}

void TBody::getLocalPoint(float x, float y, float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetLocalPoint(TPhysics::scaleDown(b2Vec2(x, y))));
	x_o = v.x;
	y_o = v.y;
}

void TBody::getLocalVector(float x, float y, float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetLocalVector(TPhysics::scaleDown(b2Vec2(x, y))));
	x_o = v.x;
	y_o = v.y;
}

void TBody::getLinearVelocityFromWorldPoint(float x, float y, float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetLinearVelocityFromWorldPoint(TPhysics::scaleDown(b2Vec2(x, y))));
	x_o = v.x;
	y_o = v.y;
}

void TBody::getLinearVelocityFromLocalPoint(float x, float y, float &x_o, float &y_o)
{
    b2Vec2 v = TPhysics::scaleUp(body->GetLinearVelocityFromLocalPoint(TPhysics::scaleDown(b2Vec2(x, y))));
	x_o = v.x;
	y_o = v.y;
}

bool TBody::isBullet() const
{
	return body->IsBullet();
}

void TBody::setBullet(bool bullet)
{
	return body->SetBullet(bullet);
}

bool TBody::isActive() const
{
	return body->IsActive();
}

bool TBody::isAwake() const
{
	return body->IsAwake();
}

void TBody::setSleepingAllowed(bool allow)
{
	body->SetSleepingAllowed(allow);
}

bool TBody::isSleepingAllowed() const
{
	return body->IsSleepingAllowed();
}

void TBody::setActive(bool active)
{
	body->SetActive(active);
}

void TBody::setAwake(bool awake)
{
	body->SetAwake(awake);
}

void TBody::setFixedRotation(bool fixed)
{
	body->SetFixedRotation(fixed);
}

bool TBody::isFixedRotation() const
{
	return body->IsFixedRotation();
}

TWorld *TBody::getWorld() const
{
	return world;
}

std::vector<TFixture *> TBody::getFixtureList() const
{
    std::vector<TFixture*> fixtureList;
    b2Fixture *f = body->GetFixtureList();
    do
    {
        if (!f)
            break;
        TFixture *fixture = (TFixture *)TMemoizer::find(f);
        if (!fixture)
            throw TException("A fixture has escaped Memoizer!");
        fixtureList.emplace_back(fixture);
    }
    while ((f = f->GetNext()));
    return fixtureList;
}

std::vector<TJoint *> TBody::getJointList() const
{
    std::vector<TJoint *> jointList;
    const b2JointEdge *je = body->GetJointList();
    do
    {
        if (!je)
            break;

        TJoint *joint = (TJoint *) TMemoizer::find(je->joint);
        if (!joint)
            throw TException("A joint has escaped Memoizer!");

        jointList.emplace_back(joint);
    }
    while ((je = je->next));

    return jointList;
}

std::vector<TContact *> TBody::getContactList() const
{
    std::vector<TContact *> contactList;
    const b2ContactEdge *ce = body->GetContactList();
    do
    {
        if (!ce)
            break;

        TContact *contact = (TContact *)TMemoizer::find(ce->contact);
        if (!contact)
            contact = new TContact(ce->contact);
        else
            contact->retain();

        contactList.emplace_back(contact);
        contact->release();
    }
    while ((ce = ce->next));
    return contactList;
}

void TBody::destroy()
{
	if (world->world->IsLocked())
	{
		// Called during time step. Save reference for destruction afterwards.
		this->retain();
		world->destructBodies.push_back(this);
		return;
	}

	world->world->DestroyBody(body);
    TMemoizer::remove(body);
	body = NULL;

	// Box2D body destroyed. Release its reference to the love Body.
    this->release();
}

void TBody::setUserData(void *userData)
{
    if(body)
        body->SetUserData(userData);
}

void *TBody::getUserData() const
{
    return body?body->GetUserData():nullptr;
}
