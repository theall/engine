/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "polygonshape.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"

#include "common/Memoizer.h"


TPolygonShape::TPolygonShape(b2PolygonShape *p, bool own)
	: TShape(p, own)
{
}

TPolygonShape::~TPolygonShape()
{
}

std::vector<b2Vec2> TPolygonShape::getPoints()
{
	b2PolygonShape *p = (b2PolygonShape *)shape;
	int count = p->GetVertexCount();
    std::vector<b2Vec2> vecList;
	for (int i = 0; i<count; i++)
	{
        vecList.emplace_back(TPhysics::scaleUp(p->GetVertex(i)));
	}
    return vecList;
}

bool TPolygonShape::validate() const
{
	b2PolygonShape *p = (b2PolygonShape *)shape;
	return p->Validate();
}




