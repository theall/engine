/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "chainshape.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"

#include "common/Memoizer.h"


TChainShape::TChainShape(b2ChainShape *c, bool loop, bool own)
	: TShape(c, own), loop(loop)
{
}

TChainShape::~TChainShape()
{
}

void TChainShape::setNextVertex(float x, float y)
{
	if (loop)
	{
        throw TException("Physics error: Can't call setNextVertex on a loop ChainShape");
		return;
	}
	b2Vec2 v(x, y);
	b2ChainShape *c = (b2ChainShape *)shape;
	c->SetNextVertex(TPhysics::scaleDown(v));
}

void TChainShape::setNextVertex()
{
	b2ChainShape *c = (b2ChainShape *)shape;
	c->m_hasNextVertex = false;
}

void TChainShape::setPreviousVertex(float x, float y)
{
	if (loop)
	{
        throw TException("Physics error: Can't call setPreviousVertex on a loop ChainShape");
		return;
	}
	b2Vec2 v(x, y);
	b2ChainShape *c = (b2ChainShape *)shape;
	c->SetPrevVertex(TPhysics::scaleDown(v));
}

void TChainShape::setPreviousVertex()
{
	b2ChainShape *c = (b2ChainShape *)shape;
	c->m_hasPrevVertex = false;
}

bool TChainShape::getNextVertex(float &x, float &y) const
{
	b2ChainShape *c = (b2ChainShape *)shape;

	if (c->m_hasNextVertex)
	{
		b2Vec2 v = TPhysics::scaleUp(c->m_nextVertex);
		x = v.x;
		y = v.y;
		return true;
	}

	return false;
}

bool TChainShape::getPreviousVertex(float &x, float &y) const
{
	b2ChainShape *c = (b2ChainShape *)shape;

	if (c->m_hasPrevVertex)
	{
		b2Vec2 v = TPhysics::scaleUp(c->m_prevVertex);
		x = v.x;
		y = v.y;
		return true;
	}

	return false;
}

TEdgeShape *TChainShape::getChildEdge(int index) const
{
	b2ChainShape *c = (b2ChainShape *)shape;
	b2EdgeShape *e = new b2EdgeShape;

	try
	{
		c->GetChildEdge(e, index);
	}
	catch (TException &)
	{
		delete e;
		throw;
	}

	return new TEdgeShape(e, true);
}

int TChainShape::getVertexCount() const
{
	b2ChainShape *c = (b2ChainShape *)shape;
	return c->m_count;
}

b2Vec2 TChainShape::getPoint(int index) const
{
	b2ChainShape *c = (b2ChainShape *)shape;
	if (index < 0 || index >= c->m_count)
        throw TException("Physics error: index out of bounds");
	const b2Vec2 &v = c->m_vertices[index];
	return TPhysics::scaleUp(v);
}

const b2Vec2 *TChainShape::getPoints() const
{
	b2ChainShape *c = (b2ChainShape *)shape;
	return c->m_vertices;
}




