/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "motorjoint.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"


TMotorJoint::TMotorJoint(TBody *body1, TBody *body2)
	: TJoint(body1, body2)
	, joint(NULL)
{
	b2MotorJointDef def;

	def.Initialize(body1->body, body2->body);
	joint = (b2MotorJoint *) createJoint(&def);
}

TMotorJoint::TMotorJoint(TBody *body1, TBody *body2, float correctionFactor, bool collideConnected)
	: TJoint(body1, body2)
	, joint(NULL)
{
	b2MotorJointDef def;

	def.Initialize(body1->body, body2->body);
	def.correctionFactor = correctionFactor;
	def.collideConnected = collideConnected;

	joint = (b2MotorJoint *) createJoint(&def);
}

TMotorJoint::~TMotorJoint()
{
}

void TMotorJoint::setLinearOffset(float x, float y)
{
	joint->SetLinearOffset(TPhysics::scaleDown(b2Vec2(x, y)));
}

b2Vec2 TMotorJoint::getLinearOffset() const
{
    return TPhysics::scaleUp(joint->GetLinearOffset());
}

void TMotorJoint::setAngularOffset(float angularOffset)
{
	joint->SetAngularOffset(angularOffset);
}

float TMotorJoint::getAngularOffset() const
{
	return joint->GetAngularOffset();
}

void TMotorJoint::setMaxForce(float force)
{
	joint->SetMaxForce(TPhysics::scaleDown(force));
}

float TMotorJoint::getMaxForce() const
{
	return TPhysics::scaleUp(joint->GetMaxForce());
}

void TMotorJoint::setMaxTorque(float torque)
{
	joint->SetMaxTorque(TPhysics::scaleDown(TPhysics::scaleDown(torque)));
}

float TMotorJoint::getMaxTorque() const
{
	return TPhysics::scaleUp(TPhysics::scaleUp(joint->GetMaxTorque()));
}

void TMotorJoint::setCorrectionFactor(float factor)
{
	joint->SetCorrectionFactor(factor);
}

float TMotorJoint::getCorrectionFactor() const
{
	return joint->GetCorrectionFactor();
}




