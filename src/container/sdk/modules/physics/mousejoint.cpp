/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "mousejoint.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"


TMouseJoint::TMouseJoint(TBody *body1, float x, float y)
	: TJoint(body1)
	, joint(NULL)
{
	if (body1->getType() == TBody::BODY_KINEMATIC)
		throw TException("Cannot attach a TMouseJoint to a kinematic body");

	b2MouseJointDef def;

	def.bodyA = body1->world->getGroundBody();
	def.bodyB = body1->body;
	def.maxForce = 1000.0f * body1->body->GetMass();
	def.target = TPhysics::scaleDown(b2Vec2(x,y));
	joint = (b2MouseJoint *)createJoint(&def);
}

TMouseJoint::~TMouseJoint()
{
}

void TMouseJoint::setTarget(float x, float y)
{
	joint->SetTarget(TPhysics::scaleDown(b2Vec2(x, y)));
}

b2Vec2 TMouseJoint::getTarget()
{
    return TPhysics::scaleUp(joint->GetTarget());
}

void TMouseJoint::setMaxForce(float force)
{
	joint->SetMaxForce(TPhysics::scaleDown(force));
}

float TMouseJoint::getMaxForce() const
{
	return TPhysics::scaleUp(joint->GetMaxForce());
}

void TMouseJoint::setFrequency(float hz)
{
	joint->SetFrequency(hz);
}

float TMouseJoint::getFrequency() const
{
	return joint->GetFrequency();
}

void TMouseJoint::setDampingRatio(float d)
{
	joint->SetDampingRatio(d);
}

float TMouseJoint::getDampingRatio() const
{
	return joint->GetDampingRatio();
}

TBody *TMouseJoint::getBodyA() const
{
	return TJoint::getBodyB();
}

TBody *TMouseJoint::getBodyB() const
{
	return nullptr;
}




