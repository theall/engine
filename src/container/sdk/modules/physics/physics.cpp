/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "physics.h"


#include "common/math.h"
//#include "wrap_body.h"


int TPhysics::meter = TPhysics::DEFAULT_METER;

const char *TPhysics::getName() const
{
	return "love.physics.box2d";
}

TWorld *TPhysics::newWorld(float gx, float gy, bool sleep)
{
	return new TWorld(b2Vec2(gx, gy), sleep);
}

TBody *TPhysics::newBody(TWorld *world, float x, float y, TBody::Type type)
{
	return new TBody(world, b2Vec2(x, y), type);
}

TBody *TPhysics::newBody(TWorld *world, TBody::Type type)
{
	return new TBody(world, b2Vec2(0, 0), type);
}

TCircleShape *TPhysics::newCircleShape(float radius)
{
	return newCircleShape(0, 0, radius);
}

TCircleShape *TPhysics::newCircleShape(float x, float y, float radius)
{
	b2CircleShape *s = new b2CircleShape();
    s->m_p = TPhysics::scaleDown(b2Vec2(x, y));
    s->m_radius = TPhysics::scaleDown(radius);
    return new TCircleShape(s);
}

TPolygonShape *TPhysics::newRectangleShape(float w, float h)
{
	return newRectangleShape(0, 0, w, h, 0);
}

TPolygonShape *TPhysics::newRectangleShape(float x, float y, float w, float h)
{
	return newRectangleShape(x, y, w, h, 0);
}

TPolygonShape *TPhysics::newRectangleShape(float x, float y, float w, float h, float angle)
{
	b2PolygonShape *s = new b2PolygonShape();
    s->SetAsBox(TPhysics::scaleDown(w/2.0f), TPhysics::scaleDown(h/2.0f), TPhysics::scaleDown(b2Vec2(x, y)), angle);
    return new TPolygonShape(s);
}

TEdgeShape *TPhysics::newEdgeShape(float x1, float y1, float x2, float y2)
{
	b2EdgeShape *s = new b2EdgeShape();
    s->Set(TPhysics::scaleDown(b2Vec2(x1, y1)), TPhysics::scaleDown(b2Vec2(x2, y2)));
    return new TEdgeShape(s);
}

TPolygonShape *TPhysics::newPolygonShape(const std::vector<b2Vec2> &vecList)
{
	// 3 to 8 (b2_maxPolygonVertices) vertices
    int vcount = vecList.size();
	if (vcount < 3)
        throw TException("Expected a minimum of 3 vertices, got %d.", vcount);
	else if (vcount > b2_maxPolygonVertices)
        throw TException("Expected a maximum of %d vertices, got %d.", b2_maxPolygonVertices, vcount);

	b2Vec2 vecs[b2_maxPolygonVertices];
    for (int i = 0; i < vcount; i++)
    {
        vecs[i] = TPhysics::scaleDown(vecList[i]);
    }

	b2PolygonShape *s = new b2PolygonShape();
	try
	{
		s->Set(vecs, vcount);
	}
    catch (TException &)
	{
		delete s;
		throw;
	}
    return new TPolygonShape(s);
}

TChainShape *TPhysics::newChainShape(const std::vector<b2Vec2> &vecList, bool loop)
{
    int vcount = vecList.size();
    b2Vec2 *vecs = new b2Vec2[vcount];
    for (int i = 0; i < vcount; i++)
    {
        vecs[i] = TPhysics::scaleDown(vecList[i]);
    }

    b2ChainShape *s = new b2ChainShape();

    try
    {
        if (loop)
            s->CreateLoop(vecs, vcount);
        else
            s->CreateChain(vecs, vcount);
    }
    catch (TException &)
    {
        delete[] vecs;
        delete s;
        throw;
    }

    delete[] vecs;
    return new TChainShape(s);
}

TDistanceJoint *TPhysics::newDistanceJoint(TBody *body1, TBody *body2, float x1, float y1, float x2, float y2, bool collideConnected)
{
    return new TDistanceJoint(body1, body2, x1, y1, x2, y2, collideConnected);
}

TMouseJoint *TPhysics::newMouseJoint(TBody *body, float x, float y)
{
    return new TMouseJoint(body, x, y);
}

TRevoluteJoint *TPhysics::newRevoluteJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected)
{
    return new TRevoluteJoint(body1, body2, xA, yA, xB, yB, collideConnected);
}

TRevoluteJoint *TPhysics::newRevoluteJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected, float referenceAngle)
{
    return new TRevoluteJoint(body1, body2, xA, yA, xB, yB, collideConnected, referenceAngle);
}

TPrismaticJoint *TPhysics::newPrismaticJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, float ax, float ay, bool collideConnected)
{
    return new TPrismaticJoint(body1, body2, xA, yA, xB, yB, ax, ay, collideConnected);
}

TPrismaticJoint *TPhysics::newPrismaticJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, float ax, float ay, bool collideConnected, float referenceAngle)
{
    return new TPrismaticJoint(body1, body2, xA, yA, xB, yB, ax, ay, collideConnected, referenceAngle);
}

TPulleyJoint *TPhysics::newPulleyJoint(TBody *body1, TBody *body2, b2Vec2 groundAnchor1, b2Vec2 groundAnchor2, b2Vec2 anchor1, b2Vec2 anchor2, float ratio, bool collideConnected)
{
    return new TPulleyJoint(body1, body2, groundAnchor1, groundAnchor2, anchor1, anchor2, ratio, collideConnected);
}

TGearJoint *TPhysics::newGearJoint(TJoint *joint1, TJoint *joint2, float ratio, bool collideConnected)
{
    return new TGearJoint(joint1, joint2, ratio, collideConnected);
}

TFrictionJoint *TPhysics::newFrictionJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected)
{
    return new TFrictionJoint(body1, body2, xA, yA, xB, yB, collideConnected);
}

TWeldJoint *TPhysics::newWeldJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected)
{
    return new TWeldJoint(body1, body2, xA, yA, xB, yB, collideConnected);
}

TWeldJoint *TPhysics::newWeldJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected, float referenceAngle)
{
    return new TWeldJoint(body1, body2, xA, yA, xB, yB, collideConnected, referenceAngle);
}

TWheelJoint *TPhysics::newWheelJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, float ax, float ay, bool collideConnected)
{
    return new TWheelJoint(body1, body2, xA, yA, xB, yB, ax, ay, collideConnected);
}

TRopeJoint *TPhysics::newRopeJoint(TBody *body1, TBody *body2, float x1, float y1, float x2, float y2, float maxLength, bool collideConnected)
{
    return new TRopeJoint(body1, body2, x1, y1, x2, y2, maxLength, collideConnected);
}

TMotorJoint *TPhysics::newMotorJoint(TBody *body1, TBody *body2)
{
    return new TMotorJoint(body1, body2);
}

TMotorJoint *TPhysics::newMotorJoint(TBody *body1, TBody *body2, float correctionFactor, bool collideConnected)
{
    return new TMotorJoint(body1, body2, correctionFactor, collideConnected);
}


TFixture *TPhysics::newFixture(TBody *body, TShape *shape, float density)
{
	return new TFixture(body, shape, density);
}

b2DistanceOutput TPhysics::getDistance(TFixture *fixtureA, TFixture *fixtureB)
{
	b2DistanceProxy pA, pB;
	b2DistanceInput i;
	b2DistanceOutput o;
	b2SimplexCache c;
	c.count = 0;

    try {
        pA.Set(fixtureA->fixture->GetShape(), 0);
        pB.Set(fixtureB->fixture->GetShape(), 0);
        i.proxyA = pA;
        i.proxyB = pB;
        i.transformA = fixtureA->fixture->GetBody()->GetTransform();
        i.transformB = fixtureB->fixture->GetBody()->GetTransform();
        i.useRadii = true;
        b2Distance(&o, &c, &i);
    } catch (...) {

    }

    return o;
}

void TPhysics::setMeter(int scale)
{
    if (scale < 1)
        throw TException("Physics error: invalid meter");
    TPhysics::meter = scale;
}

int TPhysics::getMeter()
{
	return meter;
}

void TPhysics::scaleDown(float &x, float &y)
{
	x /= (float)meter;
	y /= (float)meter;
}

void TPhysics::scaleUp(float &x, float &y)
{
	x *= (float)meter;
	y *= (float)meter;
}

float TPhysics::scaleDown(float f)
{
	return f/(float)meter;
}

float TPhysics::scaleUp(float f)
{
	return f*(float)meter;
}

b2Vec2 TPhysics::scaleDown(const b2Vec2 &v)
{
	b2Vec2 t = v;
	scaleDown(t.x, t.y);
	return t;
}

b2Vec2 TPhysics::scaleUp(const b2Vec2 &v)
{
	b2Vec2 t = v;
	scaleUp(t.x, t.y);
	return t;
}

b2AABB TPhysics::scaleDown(const b2AABB &aabb)
{
	b2AABB t;
	t.lowerBound = scaleDown(aabb.lowerBound);
	t.upperBound = scaleDown(aabb.upperBound);
	return t;
}

b2AABB TPhysics::scaleUp(const b2AABB &aabb)
{
	b2AABB t;
	t.lowerBound = scaleUp(aabb.lowerBound);
	t.upperBound = scaleUp(aabb.upperBound);
	return t;
}




