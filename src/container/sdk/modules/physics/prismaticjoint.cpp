/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "prismaticjoint.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"


TPrismaticJoint::TPrismaticJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, float ax, float ay, bool collideConnected)
	: TJoint(body1, body2)
	, joint(NULL)
{
	b2PrismaticJointDef def;
	init(def, body1, body2, xA, yA, xB, yB, ax, ay, collideConnected);
	joint = (b2PrismaticJoint *)createJoint(&def);
}

TPrismaticJoint::TPrismaticJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, float ax, float ay, bool collideConnected, float referenceAngle)
	: TJoint(body1, body2)
	, joint(NULL)
{
	b2PrismaticJointDef def;
	init(def, body1, body2, xA, yA, xB, yB, ax, ay, collideConnected);
	def.referenceAngle = referenceAngle;
	joint = (b2PrismaticJoint *)createJoint(&def);
}

TPrismaticJoint::~TPrismaticJoint()
{
}

void TPrismaticJoint::init(b2PrismaticJointDef &def, TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, float ax, float ay, bool collideConnected)
{
    def.Initialize(body1->body, body2->body, TPhysics::scaleDown(b2Vec2(xA,yA)), b2Vec2(ax,ay));
    def.localAnchorB = body2->body->GetLocalPoint(TPhysics::scaleDown(b2Vec2(xB, yB)));
	def.lowerTranslation = 0.0f;
	def.upperTranslation = 100.0f;
	def.enableLimit = true;
	def.collideConnected = collideConnected;
}

float TPrismaticJoint::getJointTranslation() const
{
    return TPhysics::scaleUp(joint->GetJointTranslation());
}

float TPrismaticJoint::getJointSpeed() const
{
    return TPhysics::scaleUp(joint->GetJointSpeed());
}

void TPrismaticJoint::setMotorEnabled(bool enable)
{
	return joint->EnableMotor(enable);
}

bool TPrismaticJoint::isMotorEnabled() const
{
	return joint->IsMotorEnabled();
}

void TPrismaticJoint::setMaxMotorForce(float force)
{
    joint->SetMaxMotorForce(TPhysics::scaleDown(force));
}

void TPrismaticJoint::setMotorSpeed(float speed)
{
    joint->SetMotorSpeed(TPhysics::scaleDown(speed));
}

float TPrismaticJoint::getMotorSpeed() const
{
    return TPhysics::scaleUp(joint->GetMotorSpeed());
}

float TPrismaticJoint::getMotorForce(float inv_dt) const
{
    return TPhysics::scaleUp(joint->GetMotorForce(inv_dt));
}

float TPrismaticJoint::getMaxMotorForce() const
{
    return TPhysics::scaleUp(joint->GetMaxMotorForce());
}

void TPrismaticJoint::setLimitsEnabled(bool enable)
{
	joint->EnableLimit(enable);
}

bool TPrismaticJoint::hasLimitsEnabled() const
{
	return joint->IsLimitEnabled();
}

void TPrismaticJoint::setUpperLimit(float limit)
{
    joint->SetLimits(joint->GetLowerLimit(), TPhysics::scaleDown(limit));
}

void TPrismaticJoint::setLowerLimit(float limit)
{
    joint->SetLimits(TPhysics::scaleDown(limit), joint->GetUpperLimit());
}

void TPrismaticJoint::setLimits(float lower, float upper)
{
    joint->SetLimits(TPhysics::scaleDown(lower), TPhysics::scaleDown(upper));
}

float TPrismaticJoint::getLowerLimit() const
{
    return TPhysics::scaleUp(joint->GetLowerLimit());
}

float TPrismaticJoint::getUpperLimit() const
{
    return TPhysics::scaleUp(joint->GetUpperLimit());
}

void TPrismaticJoint::getLimits(float32 &lower, float32 &upper)
{
    lower = TPhysics::scaleUp(joint->GetLowerLimit());
    upper = TPhysics::scaleUp(joint->GetUpperLimit());
}

b2Vec2 TPrismaticJoint::getAxis()
{
	b2Vec2 axis = joint->GetLocalAxisA();
	getBodyA()->getWorldVector(axis.x, axis.y, axis.x, axis.y);
    return axis;
}

float TPrismaticJoint::getReferenceAngle() const
{
	return joint->GetReferenceAngle();
}




