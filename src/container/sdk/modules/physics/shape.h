/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_PHYSICS_BOX2D_SHAPE_H
#define THEALL_PHYSICS_BOX2D_SHAPE_H


#include "base/shapebase.h"

#include "body.h"

// Box2D
#include "Box2D/Box2D.h"

/**
 * A Shape is geometry, attached to a Body via a Fixture.
 * A Body has position and orientation, and
 * a Shape's geometry will be affected by the parent
 * body's transformation.
 **/
class TShape : public TShapeBase
{
public:

    friend class TFixture;

	/**
	 * Creates a Shape.
	 **/
    TShape();
    TShape(b2Shape *shape, bool own = true);

    virtual ~TShape();

	/**
	 * Gets the type of Shape. Useful for
	 * debug drawing.
	 **/
	Type getType() const;
	float getRadius() const;
	int getChildCount() const;
	bool testPoint(float x, float y, float r, float px, float py) const;
    bool rayCast(b2RayCastOutput* output, const b2Vec2 &p1, const b2Vec2 &p2, float maxFraction, const b2Vec2& position, const b2Rot &rotation, int childIndex) const;
    b2AABB computeAABB(const b2Vec2 &position, const b2Rot &rotation, int childIndex) const;
    b2MassData computeMass(float density) const;

protected:

	// The Box2D shape.
	b2Shape *shape;
	bool own;
};





#endif // THEALL_PHYSICS_BOX2D_SHAPE_H
