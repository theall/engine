/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "world.h"

#include "fixture.h"
#include "shape.h"
#include "contact.h"
#include "physics.h"
#include "common/Memoizer.h"


TWorld::ContactCallback::ContactCallback()
{
}

TWorld::ContactCallback::~ContactCallback()
{
}

void TWorld::ContactCallback::process(b2Contact *contact, const b2ContactImpulse *impulse)
{
//	// Process contacts.
//	if (ref != nullptr && L != nullptr)
//	{
//		ref->push(L);

//		// Push first fixture.
//		{
//			TFixture *a = (TFixture *)TMemoizer::find(contact->GetFixtureA());
//			if (a != nullptr)
//				luax_pushtype(L, PHYSICS_FIXTURE_ID, a);
//			else
//				throw TException("A fixture has escaped Memoizer!");
//		}

//		// Push second fixture.
//		{
//			TFixture *b = (TFixture *)TMemoizer::find(contact->GetFixtureB());
//			if (b != nullptr)
//				luax_pushtype(L, PHYSICS_FIXTURE_ID, b);
//			else
//				throw TException("A fixture has escaped Memoizer!");
//		}

//        TContact *cobj = (TContact *)TMemoizer::find(contact);
//		if (!cobj)
//            cobj = new TContact(contact);
//		else
//			cobj->retain();

//		luax_pushtype(L, PHYSICS_CONTACT_ID, cobj);
//		cobj->release();

//		int args = 3;
//		if (impulse)
//		{
//			for (int c = 0; c < impulse->count; c++)
//			{
//				lua_pushnumber(L, Physics::scaleUp(impulse->normalImpulses[c]));
//				lua_pushnumber(L, Physics::scaleUp(impulse->tangentImpulses[c]));
//				args += 2;
//			}
//		}
//		lua_call(L, args, 0);
//	}
}

TWorld::ContactFilter::ContactFilter()
{
}

TWorld::ContactFilter::~ContactFilter()
{
}

bool TWorld::ContactFilter::process(TFixture *a, TFixture *b)
{
//	// Handle masks, reimplemented from the manual
//	int filterA[3], filterB[3];
//	// [0] categoryBits
//	// [1] maskBits
//	// [2] groupIndex
//	a->getFilterData(filterA);
//	b->getFilterData(filterB);

//	if (filterA[2] != 0 && // 0 is the default group, so this does not count
//		filterA[2] == filterB[2]) // if they are in the same group
//		return filterA[2] > 0; // Negative indexes mean you don't collide

//	if ((filterA[1] & filterB[0]) == 0 ||
//		(filterB[1] & filterA[0]) == 0)
//		return false; // A and B aren't set to collide

//	if (ref != nullptr && L != nullptr)
//	{
//		ref->push(L);
//		luax_pushtype(L, PHYSICS_FIXTURE_ID, a);
//		luax_pushtype(L, PHYSICS_FIXTURE_ID, b);
//		lua_call(L, 2, 1);
//		return luax_toboolean(L, -1);
//	}
	return true;
}

TWorld::QueryCallback::QueryCallback(int idx) :
    funcidx(idx)
{
//	luaL_checktype(L, funcidx, LUA_TFUNCTION);
}

TWorld::QueryCallback::~QueryCallback()
{
}

bool TWorld::QueryCallback::ReportFixture(b2Fixture *fixture)
{
//	if (L != nullptr)
//	{
//		lua_pushvalue(L, funcidx);
//		TFixture *f = (TFixture *)TMemoizer::find(fixture);
//		if (!f)
//			throw TException("A fixture has escaped Memoizer!");
//		luax_pushtype(L, PHYSICS_FIXTURE_ID, f);
//		lua_call(L, 1, 1);
//		bool cont = luax_toboolean(L, -1);
//		lua_pop(L, 1);
//		return cont;
//	}

	return true;
}

TWorld::RayCastCallback::RayCastCallback(int idx) :
    funcidx(idx)
{
//	luaL_checktype(L, funcidx, LUA_TFUNCTION);
}

TWorld::RayCastCallback::~RayCastCallback()
{
}

float32 TWorld::RayCastCallback::ReportFixture(b2Fixture *fixture, const b2Vec2 &point, const b2Vec2 &normal, float32 fraction)
{
//	if (L != nullptr)
//	{
//		lua_pushvalue(L, funcidx);
//		TFixture *f = (TFixture *)TMemoizer::find(fixture);
//		if (!f)
//			throw TException("A fixture has escaped Memoizer!");
//		luax_pushtype(L, PHYSICS_FIXTURE_ID, f);
//		b2Vec2 scaledPoint = Physics::scaleUp(point);
//		lua_pushnumber(L, scaledPoint.x);
//		lua_pushnumber(L, scaledPoint.y);
//		lua_pushnumber(L, normal.x);
//		lua_pushnumber(L, normal.y);
//		lua_pushnumber(L, fraction);
//		lua_call(L, 6, 1);
//		if (!lua_isnumber(L, -1))
//			luaL_error(L, "Raycast callback didn't return a number!");
//		float32 fraction = (float32) lua_tonumber(L, -1);
//		lua_pop(L, 1);
//		return fraction;
//	}

	return 0;
}

void TWorld::SayGoodbye(b2Fixture *fixture)
{
	TFixture *f = (TFixture *)TMemoizer::find(fixture);
	// Hint implicit destruction with true.
	if (f) f->destroy(true);
}

void TWorld::SayGoodbye(b2Joint *joint)
{
    TJoint *j = (TJoint *)TMemoizer::find(joint);
	// Hint implicit destruction with true.
	if (j) j->destroyJoint(true);
}

TWorld::TWorld() :
    world(nullptr)
  , destructWorld(false)
  , begin(nullptr)
  , end(nullptr)
  , presolve(nullptr)
  , postsolve(nullptr)
  , filter(nullptr)
{
	world = new b2World(b2Vec2(0,0));
	world->SetAllowSleeping(true);
	world->SetContactListener(this);
	world->SetContactFilter(this);
	world->SetDestructionListener(this);
	b2BodyDef def;
	groundBody = world->CreateBody(&def);
	TMemoizer::add(world, this);
}

TWorld::TWorld(b2Vec2 gravity, bool sleep) :
    world(nullptr)
  , destructWorld(false)
  , begin(nullptr)
  , end(nullptr)
  , presolve(nullptr)
  , postsolve(nullptr)
  , filter(nullptr)
{
    world = new b2World(TPhysics::scaleDown(gravity));
	world->SetAllowSleeping(sleep);
	world->SetContactListener(this);
	world->SetContactFilter(this);
	world->SetDestructionListener(this);
	b2BodyDef def;
	groundBody = world->CreateBody(&def);
	TMemoizer::add(world, this);
}

TWorld::~TWorld()
{
	destroy();
}

void TWorld::update(float dt)
{
	world->Step(dt, 8, 6);

	// Destroy all objects marked during the time step.
	for (TBody *b : destructBodies)
	{
		if (b->body != nullptr) b->destroy();
		// Release for reference in vector.
		b->release();
	}
	for (TFixture *f : destructFixtures)
	{
		if (f->isValid()) f->destroy();
		// Release for reference in vector.
		f->release();
	}
    for (TJoint *j : destructJoints)
	{
		if (j->isValid()) j->destroyJoint();
		// Release for reference in vector.
		j->release();
	}
	destructBodies.clear();
	destructFixtures.clear();
	destructJoints.clear();

	if (destructWorld)
		destroy();
}

void TWorld::BeginContact(b2Contact *contact)
{
    if(begin)
        begin->process(contact);
}

void TWorld::EndContact(b2Contact *contact)
{
    if(end)
        end->process(contact);

	// Letting the Contact know that the b2Contact will be destroyed any second.
    TContact *c = (TContact *)TMemoizer::find(contact);
	if (c != NULL)
		c->invalidate();
}

void TWorld::PreSolve(b2Contact *contact, const b2Manifold *oldManifold)
{
	B2_NOT_USED(oldManifold); // not sure what to do with this
    if(presolve)
        presolve->process(contact);
}

void TWorld::PostSolve(b2Contact *contact, const b2ContactImpulse *impulse)
{
    if(postsolve)
        postsolve->process(contact, impulse);
}

bool TWorld::ShouldCollide(b2Fixture *fixtureA, b2Fixture *fixtureB)
{
	// Fixtures should be memoized, if we created them
	TFixture *a = (TFixture *)TMemoizer::find(fixtureA);
	TFixture *b = (TFixture *)TMemoizer::find(fixtureB);
	if (!a || !b)
		throw TException("A fixture has escaped Memoizer!");
    return filter?filter->process(a, b):false;
}

bool TWorld::isValid() const
{
    return world != nullptr;
}

void TWorld::setCallbacks(TWorld::ContactCallback *_begin, TWorld::ContactCallback *_end, TWorld::ContactCallback *_presolve, TWorld::ContactCallback *_postsolve)
{
    begin = _begin;
    end = _end;
    presolve = _presolve;
    postsolve = _postsolve;
}

void TWorld::getCallbacks(TWorld::ContactCallback *&_begin, TWorld::ContactCallback *&_end, TWorld::ContactCallback *&_presolve, TWorld::ContactCallback *&_postsolve)
{
    _begin = begin;
    _end = end;
    _presolve = presolve;
    _postsolve = postsolve;
}

void TWorld::setContactFilter(TWorld::ContactFilter *contactFilter)
{
    filter = contactFilter;
}

TWorld::ContactFilter *TWorld::getContactFilter()
{
    return filter;
}

void TWorld::setGravity(float x, float y)
{
    world->SetGravity(TPhysics::scaleDown(b2Vec2(x, y)));
}

b2Vec2 TWorld::getGravity()
{
    return TPhysics::scaleUp(world->GetGravity());
}

void TWorld::translateOrigin(float x, float y)
{
    world->ShiftOrigin(TPhysics::scaleDown(b2Vec2(x, y)));
}

void TWorld::setSleepingAllowed(bool allow)
{
	world->SetAllowSleeping(allow);
}

bool TWorld::isSleepingAllowed() const
{
	return world->GetAllowSleeping();
}

bool TWorld::isLocked() const
{
	return world->IsLocked();
}

int TWorld::getBodyCount() const
{
	return world->GetBodyCount()-1; // ignore the ground body
}

int TWorld::getJointCount() const
{
	return world->GetJointCount();
}

int TWorld::getContactCount() const
{
	return world->GetContactCount();
}

std::vector<TBody *> TWorld::getBodyList() const
{
    std::vector<TBody *> bodyList;
	b2Body *b = world->GetBodyList();
	do
	{
		if (!b)
			break;
		if (b == groundBody)
			continue;
		TBody *body = (TBody *)TMemoizer::find(b);
		if (!body)
			throw TException("A body has escaped Memoizer!");
        bodyList.emplace_back(body);
	}
	while ((b = b->GetNext()));
    return bodyList;
}

std::vector<TJoint *> TWorld::getJointList() const
{
    std::vector<TJoint *> jointList;
    b2Joint *j = world->GetJointList();
    do
    {
        if (!j) break;
        TJoint *joint = (TJoint *)TMemoizer::find(j);
        if (!joint) throw TException("A joint has escaped Memoizer!");
        jointList.emplace_back(joint);
    }
    while ((j = j->GetNext()));
    return jointList;
}

std::vector<TContact *> TWorld::getContactList() const
{
    std::vector<TContact *> contactList;
	b2Contact *c = world->GetContactList();
	do
	{
		if (!c) break;
        TContact *contact = (TContact *)TMemoizer::find(c);
        if (!contact)
            contact = new TContact(c);
        contactList.emplace_back(contact);
	}
	while ((c = c->GetNext()));
    return contactList;
}

b2Body *TWorld::getGroundBody() const
{
	return groundBody;
}

void TWorld::queryBoundingBox(QueryCallback *queryCallback, const b2Vec2 &lower, const b2Vec2 &upper)
{
	b2AABB box;
    box.lowerBound = TPhysics::scaleDown(lower);
    box.upperBound = TPhysics::scaleDown(upper);
    world->QueryAABB(queryCallback, box);
}

void TWorld::rayCast(RayCastCallback *rayCastCallBack, const b2Vec2 &p1, const b2Vec2 &p2)
{
    b2Vec2 v1 = TPhysics::scaleDown(p1);
    b2Vec2 v2 = TPhysics::scaleDown(p2);
    world->RayCast(rayCastCallBack, v1, v2);
}

void TWorld::destroy()
{
	if (world == nullptr)
		return;

	if (world->IsLocked())
	{
		destructWorld = true;
		return;
	}

	// Cleaning up the world.
	b2Body *b = world->GetBodyList();
	while (b)
	{
		b2Body *t = b;
		b = b->GetNext();
		if (t == groundBody)
			continue;
		TBody *body = (TBody *)TMemoizer::find(t);
		if (!body)
			throw TException("A body has escaped Memoizer!");
		body->destroy();
	}

	world->DestroyBody(groundBody);
	TMemoizer::remove(world);
	delete world;
	world = nullptr;
}
