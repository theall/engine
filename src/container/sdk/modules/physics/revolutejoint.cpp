/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "revolutejoint.h"

#include "common/math.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"


TRevoluteJoint::TRevoluteJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected)
	: TJoint(body1, body2)
	, joint(NULL)
{
	b2RevoluteJointDef def;
	init(def, body1, body2, xA, yA, xB, yB, collideConnected);
	joint = (b2RevoluteJoint *)createJoint(&def);
}

TRevoluteJoint::TRevoluteJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected, float referenceAngle)
	: TJoint(body1, body2)
	, joint(NULL)
{
	b2RevoluteJointDef def;
	init(def, body1, body2, xA, yA, xB, yB, collideConnected);
	def.referenceAngle = referenceAngle;
	joint = (b2RevoluteJoint *)createJoint(&def);
}

TRevoluteJoint::~TRevoluteJoint()
{
}

void TRevoluteJoint::init(b2RevoluteJointDef &def, TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected)
{
	def.Initialize(body1->body, body2->body, TPhysics::scaleDown(b2Vec2(xA,yA)));
	def.localAnchorB = body2->body->GetLocalPoint(TPhysics::scaleDown(b2Vec2(xB, yB)));
	def.collideConnected = collideConnected;
}

float TRevoluteJoint::getJointAngle() const
{
	return joint->GetJointAngle();
}

float TRevoluteJoint::getJointSpeed() const
{
	return joint->GetJointSpeed();
}

void TRevoluteJoint::setMotorEnabled(bool enable)
{
	return joint->EnableMotor(enable);
}

bool TRevoluteJoint::isMotorEnabled() const
{
	return joint->IsMotorEnabled();
}

void TRevoluteJoint::setMaxMotorTorque(float torque)
{
	joint->SetMaxMotorTorque(TPhysics::scaleDown(TPhysics::scaleDown(torque)));
}

void TRevoluteJoint::setMotorSpeed(float speed)
{
	joint->SetMotorSpeed(speed);
}

float TRevoluteJoint::getMotorSpeed() const
{
	return joint->GetMotorSpeed();
}

float TRevoluteJoint::getMotorTorque(float inv_dt) const
{
	return TPhysics::scaleUp(TPhysics::scaleUp(joint->GetMotorTorque(inv_dt)));
}

float TRevoluteJoint::getMaxMotorTorque() const
{
	return TPhysics::scaleUp(TPhysics::scaleUp(joint->GetMaxMotorTorque()));
}

void TRevoluteJoint::setLimitsEnabled(bool enable)
{
	joint->EnableLimit(enable);
}

bool TRevoluteJoint::hasLimitsEnabled() const
{
	return joint->IsLimitEnabled();
}

void TRevoluteJoint::setUpperLimit(float limit)
{
	joint->SetLimits(joint->GetLowerLimit(), limit);
}

void TRevoluteJoint::setLowerLimit(float limit)
{
	joint->SetLimits(limit, joint->GetUpperLimit());
}

void TRevoluteJoint::setLimits(float lower, float upper)
{
	joint->SetLimits(lower, upper);
}

float TRevoluteJoint::getLowerLimit() const
{
	return joint->GetLowerLimit();
}

float TRevoluteJoint::getUpperLimit() const
{
	return joint->GetUpperLimit();
}

void TRevoluteJoint::getLimits(float32 &lower, float32 &upper)
{
    lower = joint->GetLowerLimit();
    upper = joint->GetUpperLimit();
}

float TRevoluteJoint::getReferenceAngle() const
{
	return joint->GetReferenceAngle();
}




