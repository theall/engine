/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "contact.h"
#include "world.h"
#include "physics.h"

#include "common/Memoizer.h"


TContact::TContact(b2Contact *contact)
	: contact(contact)
{
	TMemoizer::add(contact, this);
}

TContact::~TContact()
{
	invalidate();
}

void TContact::invalidate()
{
	if (contact != NULL)
	{
		TMemoizer::remove(contact);
		contact = NULL;
	}
}

bool TContact::isValid()
{
	return contact != NULL ? true : false;
}

std::vector<b2Vec2> TContact::getPositions()
{
	b2WorldManifold manifold;
	contact->GetWorldManifold(&manifold);
	int points = contact->GetManifold()->pointCount;
    std::vector<b2Vec2> ret;
	for (int i = 0; i < points; i++)
	{
        ret.emplace_back(TPhysics::scaleUp(manifold.points[i]));
	}
    return ret;
}

b2Vec2 TContact::getNormal()
{
	b2WorldManifold manifold;
	contact->GetWorldManifold(&manifold);
    return manifold.normal;
}

float TContact::getFriction() const
{
	return contact->GetFriction();
}

float TContact::getRestitution() const
{
	return contact->GetRestitution();
}

bool TContact::isEnabled() const
{
	return contact->IsEnabled();
}

bool TContact::isTouching() const
{
	return contact->IsTouching();
}

void TContact::setFriction(float friction)
{
	contact->SetFriction(friction);
}

void TContact::setRestitution(float restitution)
{
	contact->SetRestitution(restitution);
}

void TContact::setEnabled(bool enabled)
{
	contact->SetEnabled(enabled);
}

void TContact::resetFriction()
{
	contact->ResetFriction();
}

void TContact::resetRestitution()
{
	contact->ResetRestitution();
}

void TContact::setTangentSpeed(float speed)
{
	contact->SetTangentSpeed(speed);
}

float TContact::getTangentSpeed() const
{
	return contact->GetTangentSpeed();
}

void TContact::getChildren(int &childA, int &childB)
{
	childA = contact->GetChildIndexA();
	childB = contact->GetChildIndexB();
}

void TContact::getFixtures(TFixture *&fixtureA, TFixture *&fixtureB)
{
	fixtureA = (TFixture *) TMemoizer::find(contact->GetFixtureA());
	fixtureB = (TFixture *) TMemoizer::find(contact->GetFixtureB());

	if (!fixtureA || !fixtureB)
		throw TException("A fixture has escaped Memoizer!");
}




