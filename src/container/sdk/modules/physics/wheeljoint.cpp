/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "wheeljoint.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"


TWheelJoint::TWheelJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, float ax, float ay, bool collideConnected)
	: TJoint(body1, body2)
	, joint(NULL)
{
	b2WheelJointDef def;

	def.Initialize(body1->body, body2->body, TPhysics::scaleDown(b2Vec2(xA,yA)), b2Vec2(ax,ay));
	def.localAnchorB = body2->body->GetLocalPoint(TPhysics::scaleDown(b2Vec2(xB, yB)));
	def.collideConnected = collideConnected;
	joint = (b2WheelJoint *)createJoint(&def);
}

TWheelJoint::~TWheelJoint()
{
}

float TWheelJoint::getJointTranslation() const
{
	return TPhysics::scaleUp(joint->GetJointTranslation());
}

float TWheelJoint::getJointSpeed() const
{
	return TPhysics::scaleUp(joint->GetJointSpeed());
}

void TWheelJoint::setMotorEnabled(bool enable)
{
	return joint->EnableMotor(enable);
}

bool TWheelJoint::isMotorEnabled() const
{
	return joint->IsMotorEnabled();
}

void TWheelJoint::setMotorSpeed(float speed)
{
	joint->SetMotorSpeed(speed);
}

float TWheelJoint::getMotorSpeed() const
{
	return joint->GetMotorSpeed();
}

void TWheelJoint::setMaxMotorTorque(float torque)
{
	joint->SetMaxMotorTorque(TPhysics::scaleDown(TPhysics::scaleDown(torque)));
}

float TWheelJoint::getMaxMotorTorque() const
{
	return TPhysics::scaleUp(TPhysics::scaleUp(joint->GetMaxMotorTorque()));
}

float TWheelJoint::getMotorTorque(float inv_dt) const
{
	return TPhysics::scaleUp(TPhysics::scaleUp(joint->GetMotorTorque(inv_dt)));
}

void TWheelJoint::setSpringFrequency(float hz)
{
	joint->SetSpringFrequencyHz(hz);
}

float TWheelJoint::getSpringFrequency() const
{
	return joint->GetSpringFrequencyHz();
}

void TWheelJoint::setSpringDampingRatio(float ratio)
{
	joint->SetSpringDampingRatio(ratio);
}

float TWheelJoint::getSpringDampingRatio() const
{
	return joint->GetSpringDampingRatio();
}

b2Vec2 TWheelJoint::getAxis()
{
	b2Vec2 axis = joint->GetLocalAxisA();
	getBodyA()->getWorldVector(axis.x, axis.y, axis.x, axis.y);
    return axis;
}




