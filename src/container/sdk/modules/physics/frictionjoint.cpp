/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "frictionjoint.h"

#include "common/math.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"


TFrictionJoint::TFrictionJoint(TBody *body1, TBody *body2, float xA, float yA, float xB, float yB, bool collideConnected)
	: TJoint(body1, body2)
	, joint(NULL)
{
	b2FrictionJointDef def;
	def.Initialize(body1->body, body2->body, TPhysics::scaleDown(b2Vec2(xA,yA)));
	def.localAnchorB = body2->body->GetLocalPoint(TPhysics::scaleDown(b2Vec2(xB, yB)));
	def.collideConnected = collideConnected;
	joint = (b2FrictionJoint *)createJoint(&def);
}

TFrictionJoint::~TFrictionJoint()
{
}

void TFrictionJoint::setMaxForce(float force)
{
	joint->SetMaxForce(TPhysics::scaleDown(force));
}

float TFrictionJoint::getMaxForce() const
{
	return TPhysics::scaleUp(joint->GetMaxForce());
}

void TFrictionJoint::setMaxTorque(float torque)
{
	joint->SetMaxTorque(TPhysics::scaleDown(TPhysics::scaleDown(torque)));
}

float TFrictionJoint::getMaxTorque() const
{
	return TPhysics::scaleUp(TPhysics::scaleUp(joint->GetMaxTorque()));
}





