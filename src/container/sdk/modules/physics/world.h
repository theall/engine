/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_PHYSICS_BOX2D_WORLD_H
#define THEALL_PHYSICS_BOX2D_WORLD_H


#include "common/object.h"

// STD
#include <vector>

// Box2D
#include "Box2D/Box2D.h"


class TContact;
class TBody;
class TFixture;
class TJoint;

/**
 * The World is the "God" container class,
 * which contains all Bodies and Joints. Shapes
 * are contained in their associated Body.
 *
 * Bodies in different worlds can obviously not
 * collide.
 *
 * The world also controls global parameters, like
 * gravity.
 **/
class TWorld : public TObject, public b2ContactListener, public b2ContactFilter, public b2DestructionListener
{
public:

	// Friends.
    friend class TJoint;
    friend class TDistanceJoint;
    friend class TMouseJoint;
	friend class TBody;
	friend class TFixture;

	class ContactCallback
	{
	public:
		ContactCallback();
		~ContactCallback();
		void process(b2Contact *contact, const b2ContactImpulse *impulse = NULL);
	};

	class ContactFilter
	{
	public:
		ContactFilter();
		~ContactFilter();
		bool process(TFixture *a, TFixture *b);
	};

	class QueryCallback : public b2QueryCallback
	{
	public:
        QueryCallback(int idx);
		~QueryCallback();
		virtual bool ReportFixture(b2Fixture *fixture);
	private:
		int funcidx;
	};

	class RayCastCallback : public b2RayCastCallback
	{
	public:
        RayCastCallback(int idx);
		~RayCastCallback();
		virtual float32 ReportFixture(b2Fixture *fixture, const b2Vec2 &point, const b2Vec2 &normal, float32 fraction);
	private:
		int funcidx;
	};

	/**
	 * Creates a new world.
	 **/
	TWorld();

	/**
	 * Creates a new world with the given gravity
	 * and whether or not the bodies should sleep when appropriate.
	 * @param gravity The gravity of the World.
	 * @param sleep True if the bodies should be able to sleep,
	 * false otherwise.
	 **/
	TWorld(b2Vec2 gravity, bool sleep);

	virtual ~TWorld();

	/**
	 * Updates everything in the world one timestep.
	 * This is called update() and not step() to conform
	 * with all other objects in LOVE.
	 * @param dt The timestep.
	 **/
	void update(float dt);

	// From b2ContactListener
	void BeginContact(b2Contact *contact);
	void EndContact(b2Contact *contact);
	void PreSolve(b2Contact *contact, const b2Manifold *oldManifold);
	void PostSolve(b2Contact *contact, const b2ContactImpulse *impulse);

	// From b2ContactFilter
	bool ShouldCollide(b2Fixture *fixtureA, b2Fixture *fixtureB);

	// From b2DestructionListener
	void SayGoodbye(b2Fixture *fixture);
	void SayGoodbye(b2Joint *joint);

	/**
	 * Returns true if the Box2D world is alive.
	 **/
	bool isValid() const;

	/**
	 * Receives up to four Lua functions as arguments. Each function is
	 * collision callback for the four events (in order): begin, end,
	 * presolve and postsolve. The value "nil" is accepted if one or
	 * more events are uninteresting.
	 **/
    void setCallbacks(ContactCallback *_begin, ContactCallback *_end, ContactCallback *_presolve, ContactCallback *_postsolve);

	/**
	 * Returns the functions previously set by setCallbacks.
	 **/
    void getCallbacks(ContactCallback *&_begin, ContactCallback *&_end, ContactCallback *&_presolve, ContactCallback *&_postsolve);

	/**
	 * Sets the ContactFilter callback.
	 **/
    void setContactFilter(ContactFilter *contactFilter);

	/**
	 * Gets the ContactFilter callback.
	 **/
    ContactFilter *getContactFilter();

	/**
	 * Sets the current gravity of the World.
	 * @param x Gravity in the x-direction.
	 * @param y Gravity in the y-direction.
	 **/
	void setGravity(float x, float y);

	/**
	 * Gets the current gravity.
	 * @returns Gravity in the x-direction.
	 * @returns Gravity in the y-direction.
	 **/
    b2Vec2 getGravity();

	/**
	 * Translate the world origin.
	 * @param x The new world origin's x-coordinate relative to the old origin.
	 * @param y The new world origin's y-coordinate relative to the old origin.
	 **/
	void translateOrigin(float x, float y);

	/**
	 * Sets whether this World allows sleep.
	 * @param allow True to allow, false to disallow.
	 **/
	void setSleepingAllowed(bool allow);

	/**
	 * Returns whether this World allows sleep.
	 * @return True if allowed, false if disallowed.
	 **/
	bool isSleepingAllowed() const;

	/**
	 * Returns whether this World is currently locked.
	 * If it's locked, it's in the middle of a timestep.
	 * @return Whether the World is locked.
	 **/
	bool isLocked() const;

	/**
	 * Get the current body count.
	 * @return The number of bodies.
	 **/
	int getBodyCount() const;

	/**
	 * Get the current joint count.
	 * @return The number of joints.
	 **/
	int getJointCount() const;

	/**
	 * Get the current contact count.
	 * @return The number of contacts.
	 **/
	int getContactCount() const;

	/**
	 * Get an array of all the Bodies in the World.
	 * @return An array of Bodies.
	 **/
    std::vector<TBody *> getBodyList() const;

	/**
	 * Get an array of all the Joints in the World.
	 * @return An array of Joints.
	 **/
    std::vector<TJoint *> getJointList() const;

	/**
	 * Get an array of all the Contacts in the World.
	 * @return An array of Contacts.
	 **/
    std::vector<TContact *> getContactList() const;

	/**
	 * Gets the ground body.
	 * @return The ground body.
	 **/
	b2Body *getGroundBody() const;

	/**
	 * Gets all fixtures that overlap a given bounding box.
	 **/
    void queryBoundingBox(QueryCallback *queryCallback, const b2Vec2 &lower, const b2Vec2 &upper);

	/**
	 * Raycasts the World for all Fixtures in the path of the ray.
	 **/
    void rayCast(RayCastCallback *rayCastCallBack, const b2Vec2 &p1, const b2Vec2 &p2);

	/**
	 * Destroy this world.
	 **/
	void destroy();

private:

	// Pointer to the Box2D world.
	b2World *world;

	// Ground body
	b2Body *groundBody;

	// The list of to be destructed bodies.
	std::vector<TBody *> destructBodies;
	std::vector<TFixture *> destructFixtures;
    std::vector<TJoint *> destructJoints;
	bool destructWorld;

	// Contact callbacks.
    ContactCallback *begin, *end, *presolve, *postsolve;
    ContactFilter *filter;
};

#endif // THEALL_PHYSICS_BOX2D_WORLD_H
