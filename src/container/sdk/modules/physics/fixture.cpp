/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "fixture.h"

// Module
#include "body.h"
#include "world.h"
#include "physics.h"

#include "common/Memoizer.h"

// STD
#include <bitset>


TFixture::TFixture(TBody *body, TShape *shape, float density) :
    body(body)
  , fixture(nullptr)
{
	b2FixtureDef def;
	def.shape = shape->shape;
    def.userData = nullptr;
	def.density = density;
	fixture = body->body->CreateFixture(&def);
	this->retain();
	TMemoizer::add(fixture, this);
}

TFixture::TFixture(b2Fixture *f) :
    body(nullptr)
  , fixture(f)
{
    if(f) {
        body = (TBody *)TMemoizer::find(f->GetBody());
        if (!body)
            body = new TBody(f->GetBody());
    }
	this->retain();
	TMemoizer::add(fixture, this);
}

TFixture::~TFixture()
{

}

TShape::Type TFixture::getType() const
{
	return TShape(fixture->GetShape(), false).getType();
}

void TFixture::setFriction(float friction)
{
	fixture->SetFriction(friction);
}

void TFixture::setRestitution(float restitution)
{
	fixture->SetRestitution(restitution);
}

void TFixture::setDensity(float density)
{
	fixture->SetDensity(density);
}

void TFixture::setSensor(bool sensor)
{
	fixture->SetSensor(sensor);
}

float TFixture::getFriction() const
{
	return fixture->GetFriction();
}

float TFixture::getRestitution() const
{
	return fixture->GetRestitution();
}

float TFixture::getDensity() const
{
	return fixture->GetDensity();
}

bool TFixture::isSensor() const
{
	return fixture->IsSensor();
}

TBody *TFixture::getBody() const
{
	return body;
}

TShape *TFixture::getShape() const
{
	if (!fixture->GetShape())
		return nullptr;

	return new TShape(fixture->GetShape(), false);
}

bool TFixture::isValid() const
{
	return fixture != nullptr;
}

void TFixture::setFilterData(int *v)
{
	b2Filter f;
	f.categoryBits = (uint16) v[0];
	f.maskBits = (uint16) v[1];
	f.groupIndex = (int16) v[2];
	fixture->SetFilterData(f);
}

void TFixture::getFilterData(int *v)
{
	b2Filter f = fixture->GetFilterData();
	v[0] = (int) f.categoryBits;
	v[1] = (int) f.maskBits;
	v[2] = (int) f.groupIndex;
}

void TFixture::setCategory(uint16 bits)
{
	b2Filter f = fixture->GetFilterData();
    f.categoryBits = bits;
	fixture->SetFilterData(f);
}

void TFixture::setMask(uint16 bits)
{
	b2Filter f = fixture->GetFilterData();
    f.maskBits = ~bits;
	fixture->SetFilterData(f);
}

void TFixture::setGroupIndex(int index)
{
	b2Filter f = fixture->GetFilterData();
	f.groupIndex = (uint16)index;
	fixture->SetFilterData(f);
}

int TFixture::getGroupIndex() const
{
	b2Filter f = fixture->GetFilterData();
	return f.groupIndex;
}

uint16 TFixture::getCategory()
{
    return fixture->GetFilterData().categoryBits;
}

uint16 TFixture::getMask()
{
    return ~(fixture->GetFilterData().maskBits);
}

bool TFixture::testPoint(float x, float y) const
{
    return fixture->TestPoint(TPhysics::scaleDown(b2Vec2(x, y)));
}

bool TFixture::rayCast(const b2AABB &aabb, float maxFraction, int childIndex, b2RayCastOutput &output) const
{
    b2AABB b2aabb = TPhysics::scaleDown(aabb);
	b2RayCastInput input;
    input.p1 = b2aabb.lowerBound;
    input.p2 = b2aabb.upperBound;
	input.maxFraction = maxFraction;
	if (!fixture->RayCast(&output, input, childIndex))
        return false; // Nothing hit.
    return true;
}

b2AABB TFixture::getBoundingBox(int childIndex) const
{
    return TPhysics::scaleUp(fixture->GetAABB(childIndex));
}

b2MassData TFixture::getMassData() const
{
	b2MassData data;
	fixture->GetMassData(&data);
    data.center = TPhysics::scaleUp(data.center);
    return data;
}

void TFixture::destroy(bool implicit)
{
	if (body->world->world->IsLocked())
	{
		// Called during time step. Save reference for destruction afterwards.
		this->retain();
		body->world->destructFixtures.push_back(this);
		return;
	}

	if (!implicit && fixture != nullptr)
		body->body->DestroyFixture(fixture);
	TMemoizer::remove(fixture);
	fixture = nullptr;

	// Box2D fixture destroyed. Release its reference to the love Fixture.
    this->release();
}

void TFixture::setUserData(void *userData)
{
    if(fixture)
        fixture->SetUserData(userData);
}

void *TFixture::getUserData() const
{
    return fixture?fixture->GetUserData():nullptr;
}
