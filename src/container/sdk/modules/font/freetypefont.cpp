/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "freetypefont.h"

#include "truetyperasterizer.h"
#include "base/rasterizer/bmfontrasterizer.h"

#include <string.h>

TFreetypeFont *TFreetypeFont::mInstance=NULL;

TFreetypeFont::TFreetypeFont()
{
	if(FT_Init_FreeType(&library))
		throw TException("TrueTypeFont Loading error: FT_Init_FreeType failed");
}

TFreetypeFont::~TFreetypeFont()
{
	FT_Done_FreeType(library);
}

TRasterizer *TFreetypeFont::newRasterizer(TFileData *data)
{
    if(TTrueTypeRasterizer::accepts(library, data))
        return newTrueTypeRasterizer(data, 12, TTrueTypeRasterizer::HINTING_NORMAL);
	else if(BMFontRasterizer::accepts(data))
		return newBMFontRasterizer(data, {});

	throw TException("Invalid font file: %s", data->getFilename().c_str());
}

TRasterizer *TFreetypeFont::newTrueTypeRasterizer(TData *data, int size, TTrueTypeRasterizer::Hinting hinting)
{
    return new TTrueTypeRasterizer(library, data, size, hinting);
}

const char *TFreetypeFont::getName() const
{
	return "theall.font.freetype";
}




