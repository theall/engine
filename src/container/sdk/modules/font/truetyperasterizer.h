/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_FONT_FREETYPE_TRUE_TYPE_RASTERIZER_H
#define THEALL_FONT_FREETYPE_TRUE_TYPE_RASTERIZER_H


#include "modules/filesystem/base/filedata.h"
#include "base/rasterizer/truetyperasterizerbase.h"

// FreeType2
#include <freetype2/ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H


/**
 * Holds data for a font object.
 **/
class TTrueTypeRasterizer : public TTrueTypeRasterizerBase
{
public:

    TTrueTypeRasterizer(FT_Library library, TData *data, int size, Hinting hinting);
    virtual ~TTrueTypeRasterizer();

	// Implement TRasterizer
	virtual int getLineHeight() const;
    virtual TGlyphData *getGlyphData(uint32 glyph) const;
	virtual int getGlyphCount() const;
	virtual bool hasGlyph(uint32 glyph) const;
	virtual float getKerning(uint32 leftglyph, uint32 rightglyph) const;

	static bool accepts(FT_Library library, TData *data);

    virtual void setPixelSize(int size);

private:

	static FT_ULong hintingToLoadOption(Hinting hinting);

	// TrueType face
	FT_Face face;

	// Font data
	StrongRef<TData> data;

	Hinting hinting;

}; // TrueTypeRasterizer





#endif // THEALL_FONT_FREETYPE_TRUE_TYPE_RASTERIZER_H
