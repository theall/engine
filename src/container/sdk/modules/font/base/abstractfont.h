/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_FONT_FONT_H
#define THEALL_FONT_FONT_H

#include "modules/image/imagedata.h"
#include "modules/filesystem/base/filedata.h"

#include "common/object.h"
#include "common/int.h"


#include "rasterizer/rasterizer.h"
#include "rasterizer/truetyperasterizerbase.h"

// C++
#include <string>
#include <vector>


class TAbstractFont : public TObject
{
public:
    virtual ~TAbstractFont() {}

    virtual TRasterizer *newRasterizer(TFileData *data) = 0;

    virtual TRasterizer *newTrueTypeRasterizer(int size, TTrueTypeRasterizerBase::Hinting hinting);
    virtual TRasterizer *newTrueTypeRasterizer(TData *data, int size, TTrueTypeRasterizerBase::Hinting hinting) = 0;

    virtual TRasterizer *newBMFontRasterizer(TFileData *fontdef, const std::vector<TImageData *> &images);

    virtual TRasterizer *newImageRasterizer(TImageData *data, const std::string &glyphs, int extraspacing);
    virtual TRasterizer *newImageRasterizer(TImageData *data, uint32 *glyphs, int length, int extraspacing);

    virtual TGlyphData *newGlyphData(TRasterizer *r, const std::string &glyph);
    virtual TGlyphData *newGlyphData(TRasterizer *r, uint32 glyph);

	// Implement Module.
	virtual const char *getName() const = 0;

}; // TAbstractFont

#endif // THEALL_FONT_FONT_H
