/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/


#include "glyphdata.h"

// stdlib
#include <iostream>
#include <cstddef>


TGlyphData::TGlyphData(uint32 glyph, TGlyphMetrics glyphMetrics, TGlyphData::Format f)
    : mGlyph(glyph)
    , mMetrics(glyphMetrics)
    , mData(nullptr)
    , mFormat(f)
{
    if(mMetrics.width > 0 && mMetrics.height > 0)
        mData = new uint8[mMetrics.width * mMetrics.height * getPixelSize()];
}

TGlyphData::~TGlyphData()
{
    delete[] mData;
}

void *TGlyphData::getData() const
{
    return mData;
}

size_t TGlyphData::getPixelSize() const
{
    switch (mFormat)
	{
	case FORMAT_LUMINANCE_ALPHA:
		return 2;
	case FORMAT_RGBA:
	default:
		return 4;
	}
}

void *TGlyphData::getData(int x, int y) const
{
	size_t offset = (y * getWidth() + x) * getPixelSize();
    return mData + offset;
}

size_t TGlyphData::getSize() const
{
	return size_t(getWidth() * getHeight()) * getPixelSize();
}

int TGlyphData::getHeight() const
{
    return mMetrics.height;
}

int TGlyphData::getWidth() const
{
    return mMetrics.width;
}

uint32 TGlyphData::getGlyph() const
{
    return mGlyph;
}

std::string TGlyphData::getGlyphString() const
{
	char u[5] = {0, 0, 0, 0, 0};
	ptrdiff_t length = 0;

	try
	{
        char *end = utf8::append(mGlyph, u);
		length = end - u;
	}
	catch (utf8::exception &e)
	{
        throw TException("UTF-8 decoding error: %s", e.what());
	}

	// Just in case...
	if(length < 0)
		return "";

	return std::string(u, length);
}

int TGlyphData::getAdvance() const
{
    return mMetrics.advance;
}

int TGlyphData::getBearingX() const
{
    return mMetrics.bearingX;
}

int TGlyphData::getBearingY() const
{
    return mMetrics.bearingY;
}

int TGlyphData::getMinX() const
{
	return getBearingX();
}

int TGlyphData::getMinY() const
{
	return getHeight() - getBearingY();
}

int TGlyphData::getMaxX() const
{
	return getBearingX() + getWidth();
}

int TGlyphData::getMaxY() const
{
	return getBearingY();
}

TGlyphData::Format TGlyphData::getFormat() const
{
    return mFormat;
}

bool TGlyphData::getConstant(const char *in, TGlyphData::Format &out)
{
    return mFormats.find(in, out);
}

bool TGlyphData::getConstant(TGlyphData::Format in, const char *&out)
{
    return mFormats.find(in, out);
}

TStringMap<TGlyphData::Format, TGlyphData::FORMAT_MAX_ENUM>::Entry TGlyphData::mFormatEntries[] =
{
	{"luminancealpha", FORMAT_LUMINANCE_ALPHA},
	{"rgba", FORMAT_RGBA},
};

TStringMap<TGlyphData::Format, TGlyphData::FORMAT_MAX_ENUM> TGlyphData::mFormats(TGlyphData::mFormatEntries, sizeof(TGlyphData::mFormatEntries));



