/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_FONT_BMFONT_RASTERIZER_H
#define THEALL_FONT_BMFONT_RASTERIZER_H


#include "common/config.h"
#include "rasterizer.h"
#include "modules/image/imagedata.h"

// C++
#include <unordered_map>
#include <vector>


/**
 * TRasterizer for BMFont bitmap fonts.
 **/
class BMFontRasterizer : public TRasterizer
{
public:

    BMFontRasterizer(TFileData *fontdef, const std::vector<TImageData *> &imagelist);
	virtual ~BMFontRasterizer();

	// Implements TRasterizer.
	int getLineHeight() const override;
    TGlyphData *getGlyphData(uint32 glyph) const override;
	int getGlyphCount() const override;
	bool hasGlyph(uint32 glyph) const override;
	float getKerning(uint32 leftglyph, uint32 rightglyph) const override;
    void setPixelSize(int size) override;

    static bool accepts(TFileData *fontdef);

private:

	struct BMFontCharacter
	{
		int x;
		int y;
		int page;
        TGlyphMetrics metrics;
	};

	void parseConfig(const std::string &config);

	std::string fontFolder;

	// Image pages, indexed by their page id.
    std::unordered_map<int, StrongRef<TImageData>> images;

	// Glyph characters, indexed by their glyph id.
	std::unordered_map<uint32, BMFontCharacter> characters;

	// Kerning information, indexed by two (packed) characters.
	std::unordered_map<uint64, int> kerning;

	int fontSize;
	bool unicode;

	int lineHeight;

}; // BMFontRasterizer




#endif // THEALL_FONT_BMFONT_RASTERIZER_H
