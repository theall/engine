/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_FONT_IMAGE_RASTERIZER_H
#define THEALL_FONT_IMAGE_RASTERIZER_H


#include "modules/filesystem/file.h"
#include "modules/image/imagedata.h"

#include "rasterizer.h"

#include <map>

/**
 * Holds data for a font object.
 **/
class ImageRasterizer : public TRasterizer
{
public:
    ImageRasterizer(TImageData *imageData, uint32 *glyphs, int numglyphs, int extraspacing);
	virtual ~ImageRasterizer();

	// Implement TRasterizer
	virtual int getLineHeight() const;
    virtual TGlyphData *getGlyphData(uint32 glyph) const;
	virtual int getGlyphCount() const;
	virtual bool hasGlyph(uint32 glyph) const;
    virtual void setPixelSize(int size);

private:

    // Information about a glyph in the TImageData
	struct ImageGlyphData
	{
		int x;
		int width;
	};

	// Load all the glyph positions into memory
	void load();

	// The image data
    StrongRef<TImageData> imageData;

	// The glyphs in the font
	uint32 *glyphs;

	// Number of glyphs in the font
	int numglyphs;

	int extraSpacing;

	std::map<uint32, ImageGlyphData> imageGlyphs;

    // Color used to identify glyph separation in the source TImageData
    TPixel spacer;

}; // ImageRasterizer




#endif // THEALL_FONT_IMAGE_RASTERIZER_H
