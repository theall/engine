/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_SOUND_LULLABY_LIBMPG123_DECODER_H
#define THEALL_SOUND_LULLABY_LIBMPG123_DECODER_H

#include "common/data.h"
#include "../decoder.h"

#ifndef THEALL_NOMPG123

// libmpg123
#ifdef THEALL_APPLE_USE_FRAMEWORKS
#include <mpg123/mpg123.h>
#else
#include <mpg123.h>
#endif

struct TDecoderFile
{
	unsigned char *data;
	size_t size;
	size_t offset;

    TDecoderFile(TData *d)
		: data((unsigned char *) d->getData())
		, size(d->getSize())
		, offset(0)
	{}
};

class TMpg123Decoder : public TDecoder
{
public:

    TMpg123Decoder(TData *data, const std::string &ext, int bufferSize);
    virtual ~TMpg123Decoder();

	static bool accepts(const std::string &ext);
	static void quit();

    TDecoder *clone();
	int decode();
	bool seek(float s);
	bool rewind();
	bool isSeekable();
	int getChannels() const;
	int getBitDepth() const;
	double getDuration();

private:
    TDecoderFile mDecoderFile;
    mpg123_handle *mHandle;
    static bool mInited;
    int mChannels;
    double mDuration;
}; // TDecoder


#endif // THEALL_NOMPG123

#endif // THEALL_SOUND_LULLABY_LIBMPG123_DECODER_H
