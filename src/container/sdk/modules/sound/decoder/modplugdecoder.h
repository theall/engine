/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_SOUND_LULLABY_MODPLUG_DECODER_H
#define THEALL_SOUND_LULLABY_MODPLUG_DECODER_H

#include "common/config.h"

#ifndef THEALL_NO_MODPLUG

#include "common/data.h"
#include "../decoder.h"

// libmodplug
#if defined(THEALL_ANDROID) || defined(THEALL_IOS)
#include <modplug.h>
#else
#include <libmodplug/modplug.h>
#endif

class TModPlugDecoder : public TDecoder
{
public:

    TModPlugDecoder(TData *data, const std::string &ext, int bufferSize);
    virtual ~TModPlugDecoder();

	static bool accepts(const std::string &ext);

    TDecoder *clone();
	int decode();
	bool seek(float s);
	bool rewind();
	bool isSeekable();
	int getChannels() const;
	int getBitDepth() const;
	double getDuration();

private:

	ModPlugFile *plug;
	ModPlug_Settings settings;

	double duration;

}; // TDecoder


#endif // THEALL_NO_MODPLUG

#endif // THEALL_SOUND_LULLABY_MODPLUG_DECODER_H
