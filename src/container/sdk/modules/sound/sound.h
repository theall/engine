/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/
#ifndef THEALL_SOUND_LULLABY_SOUND_H
#define THEALL_SOUND_LULLABY_SOUND_H

#include "common/object.h"

#include "sounddata.h"
#include "decoder.h"

/**
 * The love.sound.lullaby module is the custom sound decoder module for LOVE. Instead
 * of using an intermediate library like SDL_sound, it interfaces with relevant libraries
 * directly (libmpg123, libmodplug, libFLAC, etc).
 *
 * It was Mike that came up with the name Lullaby, which we both instantly recognized as awesome.
 **/
class TSound : public TObject
{
    DECLARE_INSTANCE(TSound)

public:

	/**
	 * Constructor. Initializes relevant libraries.
	 **/
    TSound();

	/**
	 * Destructor. Deinitializes relevant libraries.
	 **/
    virtual ~TSound();

	/// @copydoc Module::getName
	const char *getName() const;

    /**
     * Creates new SoundData from a decoder. Fully expands the
     * encoded sound data into raw sound data. Not recommended
     * on large (long-duration) files.
     * @param decoder The file to decode the data from.
     * @return A SoundData object, or zero if the file type couldn't be handled.
     **/
    TSoundData *newSoundData(TDecoder *decoder);

    /**
     * Creates a new SoundData with the specified number of samples and format.
     * @param samples The number of samples.
     * @param sampleRate Number of samples per second.
     * @param bitDepth Bits per sample (8 or 16).
     * @param channels Either 1 for mono, or 2 for stereo.
     * @return A new SoundData object, or zero in case of errors.
     **/
    TSoundData *newSoundData(int samples, int sampleRate, int bitDepth, int channels);

    /// @copydoc Sound::newDecoder
    TDecoder *newDecoder(TFileData *file, int bufferSize);

}; // Sound

#endif // THEALL_SOUND_LULLABY_SOUND_H
