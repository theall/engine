/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "sounddata.h"

// C
#include <cstdlib>
#include <cstring>

// C++
#include <limits>
#include <iostream>
#include <vector>

TSoundData::TSoundData(TDecoder *decoder)
    : mData(0)
    , mSize(0)
    , mSampleRate(TDecoder::DEFAULT_SAMPLE_RATE)
    , mBitDepth(0)
    , mChannels(0)
{
	if(decoder->getBitDepth() != 8 && decoder->getBitDepth() != 16)
		throw TException("Invalid bit depth: %d", decoder->getBitDepth());

	size_t bufferSize = 524288; // 0x80000
	int decoded = decoder->decode();

	while (decoded > 0)
	{
		// Expand or allocate buffer. Note that realloc may move
		// memory to other locations.
        if(!mData || bufferSize < mSize + decoded)
		{
            while (bufferSize < mSize + decoded)
				bufferSize <<= 1;
            mData = (uint8 *) realloc(mData, bufferSize);
		}

        if(!mData)
			throw TException("Not enough memory.");

		// Copy memory into new part of memory.
        memcpy(mData + mSize, decoder->getBuffer(), decoded);

		// Overflow check.
        if(mSize > std::numeric_limits<size_t>::max() - decoded)
		{
            free(mData);
			throw TException("Not enough memory.");
		}

		// Keep this up to date.
        mSize += decoded;

		decoded = decoder->decode();
	}

	// Shrink buffer if necessary.
    if(mData && bufferSize > mSize)
        mData = (uint8 *) realloc(mData, mSize);

    mChannels = decoder->getChannels();
    mBitDepth = decoder->getBitDepth();
    mSampleRate = decoder->getSampleRate();
}

TSoundData::TSoundData(int samples, int sampleRate, int bitDepth, int channels)
    : mData(0)
    , mSize(0)
    , mSampleRate(0)
    , mBitDepth(0)
    , mChannels(0)
{
	load(samples, sampleRate, bitDepth, channels);
}

TSoundData::TSoundData(void *d, int samples, int sampleRate, int bitDepth, int channels)
    : mData(0)
    , mSize(0)
    , mSampleRate(0)
    , mBitDepth(0)
    , mChannels(0)
{
	load(samples, sampleRate, bitDepth, channels, d);
}

TSoundData::~TSoundData()
{
    if(mData != 0)
        free(mData);
}

void TSoundData::load(int samples, int sampleRate, int bitDepth, int channels, void *newData)
{
	if(samples <= 0)
		throw TException("Invalid sample count: %d", samples);

	if(sampleRate <= 0)
		throw TException("Invalid sample rate: %d", sampleRate);

	if(bitDepth != 8 && bitDepth != 16)
		throw TException("Invalid bit depth: %d", bitDepth);

	if(channels <= 0)
		throw TException("Invalid channel count: %d", channels);

    if(mData != 0)
	{
        free(mData);
        mData = 0;
	}

    mSize = samples * (bitDepth / 8) * channels;
    mSampleRate = sampleRate;
    mBitDepth = bitDepth;
    mChannels = channels;

	double realsize = samples;
	realsize *= (bitDepth / 8) * channels;
	if(realsize > std::numeric_limits<size_t>::max())
		throw TException("Data is too big!");

    mData = (uint8 *) malloc(mSize);
    if(!mData)
		throw TException("Not enough memory.");

	if(newData)
        memcpy(mData, newData, mSize);
	else
        memset(mData, bitDepth == 8 ? 128 : 0, mSize);
}

void *TSoundData::getData() const
{
    return (void *)mData;
}

size_t TSoundData::getSize() const
{
    return mSize;
}

int TSoundData::getChannels() const
{
    return mChannels;
}

int TSoundData::getBitDepth() const
{
    return mBitDepth;
}

int TSoundData::getSampleRate() const
{
    return mSampleRate;
}

int TSoundData::getSampleCount() const
{
    return (int) ((mSize/mChannels)/(mBitDepth/8));
}

float TSoundData::getDuration() const
{
    return float(mSize) / (mChannels*mSampleRate*mBitDepth/8);
}

void TSoundData::setSample(int i, float sample)
{
	// Check range.
    if(i < 0 || (size_t) i >= mSize/(mBitDepth/8))
		throw TException("Attempt to set out-of-range sample!");

    if(mBitDepth == 16)
	{
		// 16-bit sample values are signed.
        int16 *s = (int16 *) mData;
		s[i] = (int16) (sample * (float) THEALL_INT16_MAX);
	}
	else
	{
		// 8-bit sample values are unsigned internally.
        mData[i] = (uint8) ((sample * 127.0f) + 128.0f);
	}
}

float TSoundData::getSample(int i) const
{
	// Check range.
    if(i < 0 || (size_t) i >= mSize/(mBitDepth/8))
		throw TException("Attempt to get out-of-range sample!");

    if(mBitDepth == 16)
	{
		// 16-bit sample values are signed.
        int16 *s = (int16 *) mData;
		return (float) s[i] / (float) THEALL_INT16_MAX;
	}
	else
	{
		// 8-bit sample values are unsigned internally.
        return ((float) mData[i] - 128.0f) / 127.0f;
	}
}


