/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "common/config.h"

#include <algorithm>

#include "sound.h"

#include "decoder/modplugdecoder.h"
#include "decoder/vorbisdecoder.h"
#include "decoder/gmedecoder.h"
#include "decoder/wavedecoder.h"
#include "decoder/flacdecoder.h"

#ifndef THEALL_NOMPG123
#	include "decoder/mpg123decoder.h"
#endif // THEALL_NOMPG123

#ifdef THEALL_SUPPORT_COREAUDIO
#	include "coreaudiodecoder.h"
#endif

TSound *TSound::mInstance=NULL;

TSound::TSound()
{
}

TSound::~TSound()
{
#ifndef THEALL_NOMPG123
    TMpg123Decoder::quit();
#endif // THEALL_NOMPG123
}

const char *TSound::getName() const
{
	return "theall.sound.lullaby";
}

TDecoder *TSound::newDecoder(TFileData *data, int bufferSize)
{
	std::string ext = data->getExtension();
	std::transform(ext.begin(), ext.end(), ext.begin(), tolower);

    TDecoder *decoder = nullptr;

	// Find a suitable decoder here, and return it.
	if(false)
		/* nothing */;
#ifndef THEALL_NO_MODPLUG
    else if(TModPlugDecoder::accepts(ext))
        decoder = new TModPlugDecoder(data, ext, bufferSize);
#endif // THEALL_NO_MODPLUG
#ifndef THEALL_NOMPG123
    else if(TMpg123Decoder::accepts(ext))
        decoder = new TMpg123Decoder(data, ext, bufferSize);
#endif // THEALL_NOMPG123
    else if(TVorbisDecoder::accepts(ext))
        decoder = new TVorbisDecoder(data, ext, bufferSize);
#ifdef THEALL_SUPPORT_GME
    else if(TGmeDecoder::accepts(ext))
		decoder = new GmeDecoder(data, ext, bufferSize);
#endif // THEALL_SUPPORT_GME
#ifdef THEALL_SUPPORT_COREAUDIO
    else if(TCoreAudioDecoder::accepts(ext))
		decoder = new CoreAudioDecoder(data, ext, bufferSize);
#endif
    else if(TWaveDecoder::accepts(ext))
        decoder = new TWaveDecoder(data, ext, bufferSize);
    else if(TFLACDecoder::accepts(ext))
        decoder = new TFLACDecoder(data, ext, bufferSize);

	// else if(OtherDecoder::accept(ext))

	return decoder;
}

TSoundData *TSound::newSoundData(TDecoder *decoder)
{
    return new TSoundData(decoder);
}

TSoundData *TSound::newSoundData(int samples, int sampleRate, int bitDepth, int channels)
{
    return new TSoundData(samples, sampleRate, bitDepth, channels);
}

