/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#include "common/config.h"
#include "window.h"

#include "common/exception.h"
#include "../graphics/graphics.h"

#ifdef THEALL_ANDROID
#include "common/android.h"
#endif

#if defined(THEALL_WINDOWS)
#include <windows.h>
#elif defined(THEALL_MACOSX)
#include "common/macosx.h"
#endif

#ifndef APIENTRY
#define APIENTRY
#endif

#define WINDOW_DEFAULT_WIDTH    800
#define WINDOW_DEFAULT_HEIGHT   600

TWindow *TWindow::mInstance=NULL;

TWindow::TWindow() :
      mTitle("2DCombat")
    , mWindowWidth(WINDOW_DEFAULT_WIDTH)
    , mWindowHeight(WINDOW_DEFAULT_HEIGHT)
    , mPixelWidth(WINDOW_DEFAULT_WIDTH)
    , mPixelHeight(WINDOW_DEFAULT_HEIGHT)
    , mOpen(false)
    , mMouseGrabbed(false)
    , mWindow(nullptr)
    , mContext(nullptr)
    , mDisplayedWindowError(false)
    , mHasSDLLegacy(false)
{
	if(SDL_InitSubSystem(SDL_INIT_VIDEO) < 0)
		throw TException("Could not initialize SDL video subsystem (%s)", SDL_GetError());

	// Make sure the screensaver doesn't activate by default.
	setDisplaySleepEnabled(false);

    SDL_version version;
	SDL_GetVersion(&version);
    mHasSDLLegacy = (version.major == 2 && version.minor == 0 && version.patch <= 3);
}

TWindow::~TWindow()
{
	close();

    SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

void TWindow::setGLFramebufferAttributes(int msaa, bool sRGB)
{
	// Set GL window / framebuffer attributes.
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_RETAINED_BACKING, 0);

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, (msaa > 0) ? 1 : 0);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, (msaa > 0) ? msaa : 0);

	SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, sRGB ? 1 : 0);

	const char *driver = SDL_GetCurrentVideoDriver();
	if(driver && strstr(driver, "x11") == driver)
	{
		// Always disable the sRGB flag when GLX is used with older SDL versions,
		// because of this bug: https://bugzilla.libsdl.org/show_bug.cgi?id=2897
		// In practice GLX will always give an sRGB-capable framebuffer anyway.
        if(mHasSDLLegacy)
			SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 0);
	}

#if defined(THEALL_WINDOWS)
	// Avoid the Microsoft OpenGL 1.1 software renderer on Windows. Apparently
	// older Intel drivers like to use it as a fallback when requesting some
	// unsupported framebuffer attribute values, rather than properly failing.
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
#endif
}

void TWindow::setGLContextAttributes(const ContextAttribs &attribs)
{
	int profilemask = 0;
	int contextflags = 0;

	if(attribs.gles)
		profilemask = SDL_GL_CONTEXT_PROFILE_ES;
	else if(attribs.debug)
		profilemask = SDL_GL_CONTEXT_PROFILE_COMPATIBILITY;

	if(attribs.debug)
		contextflags |= SDL_GL_CONTEXT_DEBUG_FLAG;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, attribs.versionMajor);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, attribs.versionMinor);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, profilemask);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, contextflags);
}

bool TWindow::checkGLVersion(const ContextAttribs &attribs, std::string &outversion)
{
	typedef unsigned char GLubyte;
	typedef unsigned int GLenum;
	typedef const GLubyte *(APIENTRY *glGetStringPtr)(GLenum name);
	const GLenum GL_VENDOR_ENUM   = 0x1F00;
	const GLenum GL_RENDERER_ENUM = 0x1F01;
	const GLenum GL_VERSION_ENUM  = 0x1F02;

	// We don't have OpenGL headers or an automatic OpenGL function loader in
	// this module, so we have to get the glGetString function pointer ourselves.
	glGetStringPtr glGetStringFunc = (glGetStringPtr) SDL_GL_GetProcAddress("glGetString");
	if(!glGetStringFunc)
		return false;

	const char *glversion = (const char *) glGetStringFunc(GL_VERSION_ENUM);
	if(!glversion)
		return false;

	outversion = glversion;

	const char *glrenderer = (const char *) glGetStringFunc(GL_RENDERER_ENUM);
	if(glrenderer)
		outversion += " - " + std::string(glrenderer);

	const char *glvendor = (const char *) glGetStringFunc(GL_VENDOR_ENUM);
	if(glvendor)
		outversion += " (" + std::string(glvendor) + ")";

	int glmajor = 0;
	int glminor = 0;

	// glGetString(GL_VERSION) returns a string with the format "major.minor",
	// or "OpenGL ES major.minor" in GLES contexts.
	const char *format = "%d.%d";
	if(attribs.gles)
		format = "OpenGL ES %d.%d";

	if(sscanf(glversion, format, &glmajor, &glminor) != 2)
		return false;

	if(glmajor < attribs.versionMajor
		|| (glmajor == attribs.versionMajor && glminor < attribs.versionMinor))
		return false;

	return true;
}

bool TWindow::createWindowAndContext(int x, int y, int w, int h, Uint32 windowflags, int msaa)
{
	bool preferGLES = false;

#ifdef THEALL_GRAPHICS_USE_OPENGLES
	preferGLES = true;
#endif

	const char *curdriver = SDL_GetCurrentVideoDriver();
	const char *glesdrivers[] = {"RPI", "Android", "uikit", "winrt", "emscripten"};

	// We always want to try OpenGL ES first on certain video backends.
	for (const char *glesdriver : glesdrivers)
	{
		if(curdriver && strstr(curdriver, glesdriver) == curdriver)
		{
			preferGLES = true;

			// Prior to SDL 2.0.4, backends that use OpenGL ES didn't properly
			// ask for a sRGB framebuffer when requested by SDL_GL_SetAttribute.
			// FIXME: This doesn't account for windowing backends that sometimes
			// use EGL, e.g. the X11 and windows SDL backends.
            if(mHasSDLLegacy)
                TAbstractGraphics::setGammaCorrect(false);

			break;
		}
	}

	if(!preferGLES)
	{
		const char *gleshint = SDL_GetHint("THEALL_GRAPHICS_USE_OPENGLES");
		preferGLES = (gleshint != nullptr && gleshint[0] != '0');
	}

	// Do we want a debug context?
	const char *debughint = SDL_GetHint("THEALL_GRAPHICS_DEBUG");
	bool debug = (debughint != nullptr && debughint[0] != '0');

	// Different context attribute profiles to try.
	std::vector<ContextAttribs> attribslist = {
		{2, 1, false, debug}, // OpenGL 2.1.
		{3, 0, true,  debug}, // OpenGL ES 3.
		{2, 0, true,  debug}, // OpenGL ES 2.
	};

	// OpenGL ES 3+ contexts are only properly supported in SDL 2.0.4+.
    bool removeES3 = mHasSDLLegacy;

	// While UWP SDL is above 2.0.4, it still doesn't support OpenGL ES 3+
#ifdef THEALL_WINDOWS_UWP
	removeES3 = true;
#endif
	
	if(removeES3)
	{
		auto it = attribslist.begin();
		while (it != attribslist.end())
		{
			if(it->gles && it->versionMajor >= 3)
				it = attribslist.erase(it);
			else
				++it;
		}
	}

	// Move OpenGL ES to the front of the list if we should prefer GLES.
	if(preferGLES)
		std::rotate(attribslist.begin(), attribslist.begin() + 1, attribslist.end());

	std::string windowerror;
	std::string contexterror;
	std::string glversion;

	// Unfortunately some OpenGL context settings are part of the internal
	// window state in the Windows and Linux SDL backends, so we have to
	// recreate the window when we want to change those settings...
	// Also, apparently some Intel drivers on Windows give back a Microsoft
	// OpenGL 1.1 software renderer context when high MSAA values are requested!

	const auto create = [&](ContextAttribs attribs) -> bool
	{
        if(mContext)
		{
            SDL_GL_DeleteContext(mContext);
            mContext = nullptr;
		}

        if(mWindow)
		{
            SDL_DestroyWindow(mWindow);
			SDL_FlushEvent(SDL_WINDOWEVENT);
            mWindow = nullptr;
		}

        mWindow = SDL_CreateWindow(mTitle.c_str(), x, y, w, h, windowflags);

        if(!mWindow)
		{
			windowerror = std::string(SDL_GetError());
			return false;
		}

        mContext = SDL_GL_CreateContext(mWindow);

        if(!mContext)
			contexterror = std::string(SDL_GetError());

		// Make sure the context's version is at least what we requested.
        if(mContext && !checkGLVersion(attribs, glversion))
		{
            SDL_GL_DeleteContext(mContext);
            mContext = nullptr;
		}

        if(!mContext)
		{
            SDL_DestroyWindow(mWindow);
            mWindow = nullptr;
			return false;
		}

		return true;
	};

	// Try each context profile in order.
	for (ContextAttribs attribs : attribslist)
	{
		int curMSAA  = msaa;
        bool curSRGB = TAbstractGraphics::isGammaCorrect();

		setGLFramebufferAttributes(curMSAA, curSRGB);
		setGLContextAttributes(attribs);

		windowerror.clear();
		contexterror.clear();

		create(attribs);

        if(!mWindow && curMSAA > 0)
		{
			// The MSAA setting could have caused the failure.
			setGLFramebufferAttributes(0, curSRGB);
			if(create(attribs))
				curMSAA = 0;
		}

        if(!mWindow && curSRGB)
		{
			// same with sRGB.
			setGLFramebufferAttributes(curMSAA, false);
			if(create(attribs))
				curSRGB = false;
		}

        if(!mWindow && curMSAA > 0 && curSRGB)
		{
			// Or both!
			setGLFramebufferAttributes(0, false);
			if(create(attribs))
			{
				curMSAA = 0;
				curSRGB = false;
			}
		}

        if(mWindow && mContext)
		{
            TAbstractGraphics::setGammaCorrect(curSRGB);
			break;
		}
	}

    if(!mContext || !mWindow)
	{
		std::string title = "Unable to create OpenGL window";
		std::string message = "This program requires a graphics card and video drivers which support OpenGL 2.1 or OpenGL ES 2.";

		if(!glversion.empty())
			message += "\n\nDetected OpenGL version:\n" + glversion;
		else if(!contexterror.empty())
			message += "\n\nOpenGL context creation error: " + contexterror;
		else if(!windowerror.empty())
			message += "\n\nSDL window creation error: " + windowerror;

		std::cerr << title << std::endl << message << std::endl;

		// Display a message box with the error, but only once.
        if(!mDisplayedWindowError)
		{
			showMessageBox(title, message, MESSAGEBOX_ERROR, false);
            mDisplayedWindowError = true;
		}

		close();
		return false;
	}

    mOpen = true;
	return true;
}

bool TWindow::setWindow(int width, int height, TWindowSettings *settings)
{
    TWindowSettings f;

	if(settings)
		f = *settings;

	f.minwidth = std::max(f.minwidth, 1);
	f.minheight = std::max(f.minheight, 1);

	f.display = std::min(std::max(f.display, 0), getDisplayCount() - 1);

	// Use the desktop resolution if a width or height of 0 is specified.
	if(width == 0 || height == 0)
	{
        SDL_DisplayMode mode;
		SDL_GetDesktopDisplayMode(f.display, &mode);
		width = mode.w;
		height = mode.h;
	}

	Uint32 sdlflags = SDL_WINDOW_OPENGL;

	// On Android we always must have fullscreen type FULLSCREEN_TYPE_DESKTOP
#ifdef THEALL_ANDROID
	f.fstype = FULLSCREEN_DESKTOP;
#endif

	if(f.fullscreen)
	{
		if(f.fstype == FULLSCREEN_DESKTOP)
			sdlflags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		else
		{
			sdlflags |= SDL_WINDOW_FULLSCREEN;
			SDL_DisplayMode mode = {0, width, height, 0, 0};

            // Fullscreen window creation will bug out if no mode can be used.
			if(SDL_GetClosestDisplayMode(f.display, &mode, &mode) == nullptr)
			{
				// GetClosestDisplayMode will fail if we request a size larger
				// than the largest available display mode, so we'll try to use
				// the largest (first) mode in that case.
				if(SDL_GetDisplayMode(f.display, 0, &mode) < 0)
					return false;
			}

			width = mode.w;
			height = mode.h;
		}
	}

	if(f.resizable)
		sdlflags |= SDL_WINDOW_RESIZABLE;

	if(f.borderless)
		sdlflags |= SDL_WINDOW_BORDERLESS;

	if(f.highdpi)
		sdlflags |= SDL_WINDOW_ALLOW_HIGHDPI;

	int x = f.x;
	int y = f.y;

	if(f.useposition && !f.fullscreen)
	{
		// The position needs to be in the global coordinate space.
        SDL_Rect displaybounds;
		SDL_GetDisplayBounds(f.display, &displaybounds);
		x += displaybounds.x;
		y += displaybounds.y;
	}
	else
	{
		if(f.centered)
			x = y = SDL_WINDOWPOS_CENTERED_DISPLAY(f.display);
		else
			x = y = SDL_WINDOWPOS_UNDEFINED_DISPLAY(f.display);
	}

	close();

	if(!createWindowAndContext(x, y, width, height, sdlflags, f.msaa))
		return false;

	// Make sure the window keeps any previously set icon.
    setIcon(mIcon.get());

	// Make sure the mouse keeps its previous grab setting.
    setMouseGrab(mMouseGrabbed);

	// Enforce minimum window dimensions.
    SDL_SetWindowMinimumSize(mWindow, f.minwidth, f.minheight);

	if((f.useposition || f.centered) && !f.fullscreen)
        SDL_SetWindowPosition(mWindow, x, y);

    SDL_RaiseWindow(mWindow);

	SDL_GL_SetSwapInterval(f.vsync ? 1 : 0);

	updateSettings(f);

    TGraphics::instance()->setMode(mPixelWidth, mPixelHeight);

#ifdef THEALL_ANDROID
		android::setImmersive(f.fullscreen);
#endif

	return true;
}

bool TWindow::onSizeChanged(int width, int height)
{
    if(!mWindow)
		return false;

    mWindowWidth = width;
    mWindowHeight = height;

    SDL_GL_GetDrawableSize(mWindow, &mPixelWidth, &mPixelHeight);

    TGraphics::instance()->setViewportSize(mPixelWidth, mPixelHeight);

	return true;
}

void TWindow::updateSettings(const TWindowSettings &newsettings)
{
    Uint32 wflags = SDL_GetWindowFlags(mWindow);

	// Set the new display mode as the current display mode.
    SDL_GetWindowSize(mWindow, &mWindowWidth, &mWindowHeight);
    SDL_GL_GetDrawableSize(mWindow, &mPixelWidth, &mPixelHeight);

	if((wflags & SDL_WINDOW_FULLSCREEN_DESKTOP) == SDL_WINDOW_FULLSCREEN_DESKTOP)
	{
        mWindowSettings.fullscreen = true;
        mWindowSettings.fstype = FULLSCREEN_DESKTOP;
	}
	else if((wflags & SDL_WINDOW_FULLSCREEN) == SDL_WINDOW_FULLSCREEN)
	{
        mWindowSettings.fullscreen = true;
        mWindowSettings.fstype = FULLSCREEN_EXCLUSIVE;
	}
	else
	{
        mWindowSettings.fullscreen = false;
        mWindowSettings.fstype = newsettings.fstype;
	}

#ifdef THEALL_ANDROID
	settings.fullscreen = android::getImmersive();
#endif

	// SDL_GetWindowMinimumSize gives back 0,0 sometimes...
    mWindowSettings.minwidth = newsettings.minwidth;
    mWindowSettings.minheight = newsettings.minheight;

    mWindowSettings.resizable = (wflags & SDL_WINDOW_RESIZABLE) != 0;
    mWindowSettings.borderless = (wflags & SDL_WINDOW_BORDERLESS) != 0;
    mWindowSettings.centered = newsettings.centered;

    getPosition(mWindowSettings.x, mWindowSettings.y, mWindowSettings.display);

    mWindowSettings.highdpi = (wflags & SDL_WINDOW_ALLOW_HIGHDPI) != 0;

	// Only minimize on focus loss if the window is in exclusive-fullscreen mode
    if(mWindowSettings.fullscreen && mWindowSettings.fstype == FULLSCREEN_EXCLUSIVE)
		SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "1");
	else
		SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "0");

	// Verify MSAA setting.
	int buffers = 0;
	int samples = 0;
	SDL_GL_GetAttribute(SDL_GL_MULTISAMPLEBUFFERS, &buffers);
	SDL_GL_GetAttribute(SDL_GL_MULTISAMPLESAMPLES, &samples);

    mWindowSettings.msaa = (buffers > 0 ? samples : 0);
    mWindowSettings.vsync = SDL_GL_GetSwapInterval() != 0;

    SDL_DisplayMode dmode;
    SDL_GetCurrentDisplayMode(mWindowSettings.display, &dmode);

	// May be 0 if the refresh rate can't be determined.
    mWindowSettings.refreshrate = (double) dmode.refresh_rate;
}

void TWindow::getWindow(int &width, int &height, TWindowSettings &newsettings)
{
	// The window might have been modified (moved, resized, etc.) by the user.
    if(mWindow)
        updateSettings(mWindowSettings);

    width = mWindowWidth;
    height = mWindowHeight;
    newsettings = mWindowSettings;
}

void TWindow::close()
{
    TGraphics::instance()->unSetMode();

    if(mContext)
	{
        SDL_GL_DeleteContext(mContext);
        mContext = nullptr;
	}

    if(mWindow)
	{
        SDL_DestroyWindow(mWindow);
        mWindow = nullptr;

		// The old window may have generated pending events which are no longer
		// relevant. Destroy them all!
		SDL_FlushEvent(SDL_WINDOWEVENT);
	}

    mOpen = false;
}

bool TWindow::setFullscreen(bool fullscreen, FullscreenType fstype)
{
    if(!mWindow)
		return false;

    TWindowSettings newsettings = mWindowSettings;
	newsettings.fullscreen = fullscreen;
	newsettings.fstype = fstype;

	Uint32 sdlflags = 0;

	if(fullscreen)
	{
		if(fstype == FULLSCREEN_DESKTOP)
			sdlflags = SDL_WINDOW_FULLSCREEN_DESKTOP;
		else
		{
			sdlflags = SDL_WINDOW_FULLSCREEN;

            SDL_DisplayMode mode;
            mode.w = mWindowWidth;
            mode.h = mWindowHeight;

            SDL_GetClosestDisplayMode(SDL_GetWindowDisplayIndex(mWindow), &mode, &mode);
            SDL_SetWindowDisplayMode(mWindow, &mode);
		}
	}

#ifdef THEALL_ANDROID
	android::setImmersive(fullscreen);
#endif

    if(SDL_SetWindowFullscreen(mWindow, sdlflags) == 0)
	{
        SDL_GL_MakeCurrent(mWindow, mContext);
		updateSettings(newsettings);

		// Apparently this gets un-set when we exit fullscreen (at least in OS X).
		if(!fullscreen)
            SDL_SetWindowMinimumSize(mWindow, mWindowSettings.minwidth, mWindowSettings.minheight);

		// Update the viewport size now instead of waiting for event polling.
        TGraphics::instance()->setViewportSize(mPixelWidth, mPixelHeight);

		return true;
	}

	return false;
}

bool TWindow::setFullscreen(bool fullscreen)
{
    return setFullscreen(fullscreen, mWindowSettings.fstype);
}

int TWindow::getDisplayCount() const
{
	return SDL_GetNumVideoDisplays();
}

const char *TWindow::getDisplayName(int displayindex) const
{
	const char *name = SDL_GetDisplayName(displayindex);

	if(name == nullptr)
		throw TException("Invalid display index: %d", displayindex + 1);

	return name;
}

std::vector<TWindowSize> TWindow::getFullscreenSizes(int displayindex) const
{
    std::vector<TWindowSize> sizes;

	for (int i = 0; i < SDL_GetNumDisplayModes(displayindex); i++)
	{
        SDL_DisplayMode mode;
		SDL_GetDisplayMode(displayindex, i, &mode);

        TWindowSize w = {mode.w, mode.h};

		// SDL2's display mode list has multiple entries for modes of the same
		// size with different bits per pixel, so we need to filter those out.
		if(std::find(sizes.begin(), sizes.end(), w) == sizes.end())
			sizes.push_back(w);
	}

	return sizes;
}

void TWindow::getDesktopDimensions(int displayindex, int &width, int &height) const
{
	if(displayindex >= 0 && displayindex < getDisplayCount())
	{
        SDL_DisplayMode mode;
		SDL_GetDesktopDisplayMode(displayindex, &mode);
		width = mode.w;
		height = mode.h;
	}
	else
	{
		width = 0;
		height = 0;
    }
}

TWindowSize TWindow::getSize() const
{
    TWindowSize windowSize;
    windowSize.width = mWindowWidth;
    windowSize.height = mWindowHeight;
    return windowSize;
}

void TWindow::setPosition(int x, int y, int displayindex)
{
    if(!mWindow)
		return;

	displayindex = std::min(std::max(displayindex, 0), getDisplayCount() - 1);

    SDL_Rect displaybounds;
	SDL_GetDisplayBounds(displayindex, &displaybounds);

	// The position needs to be in the global coordinate space.
	x += displaybounds.x;
	y += displaybounds.y;

    SDL_SetWindowPosition(mWindow, x, y);

    mWindowSettings.useposition = true;
}

void TWindow::getPosition(int &x, int &y, int &displayindex)
{
    if(!mWindow)
	{
		x = y =  0;
		displayindex = 0;
		return;
	}

    displayindex = std::max(SDL_GetWindowDisplayIndex(mWindow), 0);

    SDL_GetWindowPosition(mWindow, &x, &y);

	// In SDL <= 2.0.3, fullscreen windows are always reported as 0,0. In every
	// other case we need to convert the position from global coordinates to the
	// monitor's coordinate space.
	if(x != 0 || y != 0)
	{
        SDL_Rect displaybounds;
		SDL_GetDisplayBounds(displayindex, &displaybounds);

		x -= displaybounds.x;
		y -= displaybounds.y;
	}
}

bool TWindow::isOpen() const
{
    return mOpen;
}

void TWindow::setWindowTitle(const std::string &title)
{
    mTitle = title;

    if(mWindow)
        SDL_SetWindowTitle(mWindow, title.c_str());
}

const std::string &TWindow::getWindowTitle() const
{
    return mTitle;
}

bool TWindow::setIcon(TImageData *imgd)
{
    if(!imgd)
        return false;

    mIcon.set(imgd);

    if(!mWindow)
        return false;

    Uint32 rmask, gmask, bmask, amask;
#ifdef THEALL_BIG_ENDIAN
    rmask = 0xFF000000;
    gmask = 0x00FF0000;
    bmask = 0x0000FF00;
    amask = 0x000000FF;
#else
    rmask = 0x000000FF;
    gmask = 0x0000FF00;
    bmask = 0x00FF0000;
    amask = 0xFF000000;
#endif

    int w = imgd->getWidth();
    int h = imgd->getHeight();
    int pitch = imgd->getWidth() * 4;

    SDL_Surface *sdlicon = 0;

    {
        // We don't want another thread modifying the TImageData mid-copy.
        TLock lock(imgd->getMutex());
        sdlicon = SDL_CreateRGBSurfaceFrom(imgd->getData(), w, h, 32, pitch, rmask, gmask, bmask, amask);
    }

    if(!sdlicon)
        return false;

    SDL_SetWindowIcon(mWindow, sdlicon);
    SDL_FreeSurface(sdlicon);

    return true;
}

TImageData *TWindow::getIcon()
{
    return mIcon.get();
}

void TWindow::setDisplaySleepEnabled(bool enable)
{
	if(enable)
		SDL_EnableScreenSaver();
	else
		SDL_DisableScreenSaver();
}

bool TWindow::isDisplaySleepEnabled() const
{
	return SDL_IsScreenSaverEnabled() != SDL_FALSE;
}

void TWindow::minimize()
{
    if(mWindow != nullptr)
        SDL_MinimizeWindow(mWindow);
}

void TWindow::maximize()
{
    if(mWindow != nullptr)
        SDL_MaximizeWindow(mWindow);
}

void TWindow::swapBuffers()
{
    SDL_GL_SwapWindow(mWindow);
}

bool TWindow::hasFocus() const
{
    return (mWindow && SDL_GetKeyboardFocus() == mWindow);
}

bool TWindow::hasMouseFocus() const
{
    return (mWindow && SDL_GetMouseFocus() == mWindow);
}

bool TWindow::isVisible() const
{
    return mWindow && (SDL_GetWindowFlags(mWindow) & SDL_WINDOW_SHOWN) != 0;
}

void TWindow::setMouseVisible(bool visible)
{
	SDL_ShowCursor(visible ? SDL_ENABLE : SDL_DISABLE);
}

bool TWindow::getMouseVisible() const
{
	return (SDL_ShowCursor(SDL_QUERY) == SDL_ENABLE);
}

void TWindow::setMouseGrab(bool grab)
{
    mMouseGrabbed = grab;
    if(mWindow)
        SDL_SetWindowGrab(mWindow, (SDL_bool) grab);
}

bool TWindow::isMouseGrabbed() const
{
    if(mWindow)
        return SDL_GetWindowGrab(mWindow) != SDL_FALSE;
	else
        return mMouseGrabbed;
}

void TWindow::getPixelDimensions(int &w, int &h) const
{
    w = mPixelWidth;
    h = mPixelHeight;
}

void TWindow::windowToPixelCoords(double *x, double *y) const
{
	if(x != nullptr)
        *x = (*x) * ((double) mPixelWidth / (double) mWindowWidth);
	if(y != nullptr)
        *y = (*y) * ((double) mPixelHeight / (double) mWindowHeight);
}

void TWindow::pixelToWindowCoords(double *x, double *y) const
{
	if(x != nullptr)
        *x = (*x) * ((double) mWindowWidth / (double) mPixelWidth);
	if(y != nullptr)
        *y = (*y) * ((double) mWindowHeight / (double) mPixelHeight);
}

double TWindow::getPixelScale() const
{
#ifdef THEALL_ANDROID
	return android::getScreenScale();
#else
    return (double) mPixelHeight / (double) mWindowHeight;
#endif
}

double TWindow::toPixels(double x) const
{
	return x * getPixelScale();
}

void TWindow::toPixels(double wx, double wy, double &px, double &py) const
{
	double scale = getPixelScale();
	px = wx * scale;
	py = wy * scale;
}

double TWindow::fromPixels(double x) const
{
	return x / getPixelScale();
}

void TWindow::fromPixels(double px, double py, double &wx, double &wy) const
{
	double scale = getPixelScale();
	wx = px / scale;
	wy = py / scale;
}

const void *TWindow::getHandle() const
{
    return mWindow;
}

bool TWindow::showWarning(const std::string &title, const std::string &message)
{
    return showMessageBox(title, message, MESSAGEBOX_WARNING);
}

bool TWindow::showError(const std::string &title, const std::string &message)
{
    return showMessageBox(title, message, MESSAGEBOX_ERROR);
}

bool TWindow::showMessage(const std::string &title, const std::string &message)
{
    return showMessageBox(title, message, MESSAGEBOX_INFO);
}

SDL_MessageBoxFlags TWindow::convertMessageBoxType(MessageBoxType type) const
{
	switch (type)
	{
	case MESSAGEBOX_ERROR:
		return SDL_MESSAGEBOX_ERROR;
	case MESSAGEBOX_WARNING:
		return SDL_MESSAGEBOX_WARNING;
	case MESSAGEBOX_INFO:
	default:
		return SDL_MESSAGEBOX_INFORMATION;
	}
}

bool TWindow::showMessageBox(const std::string &title, const std::string &message, MessageBoxType type, bool attachtowindow)
{
	SDL_MessageBoxFlags flags = convertMessageBoxType(type);
    SDL_Window *sdlwindow = attachtowindow ? mWindow : nullptr;

	return SDL_ShowSimpleMessageBox(flags, title.c_str(), message.c_str(), sdlwindow) >= 0;
}

int TWindow::showMessageBox(const TMessageBoxData &data)
{
    SDL_MessageBoxData sdldata;

	sdldata.flags = convertMessageBoxType(data.type);
	sdldata.title = data.title.c_str();
	sdldata.message = data.message.c_str();
    sdldata.window = data.attachToWindow ? mWindow : nullptr;

	sdldata.numbuttons = (int) data.buttons.size();

	std::vector<SDL_MessageBoxButtonData> sdlbuttons;

	for (int i = 0; i < (int) data.buttons.size(); i++)
	{
        SDL_MessageBoxButtonData sdlbutton;

		sdlbutton.buttonid = i;
		sdlbutton.text = data.buttons[i].c_str();

		if(i == data.enterButtonIndex)
			sdlbutton.flags |= SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT;

		if(i == data.escapeButtonIndex)
			sdlbutton.flags |= SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT;

		sdlbuttons.push_back(sdlbutton);
	}

	sdldata.buttons = &sdlbuttons[0];

	int pressedbutton = -2;
	SDL_ShowMessageBox(&sdldata, &pressedbutton);

	return pressedbutton;
}

void TWindow::requestAttention(bool continuous)
{
#if defined(THEALL_WINDOWS) && !defined(THEALL_WINDOWS_UWP)

	if(hasFocus())
		return;

    SDL_SysWMinfo wminfo;
	SDL_VERSION(&wminfo.version);

    if(SDL_GetWindowWMInfo(mWindow, &wminfo))
	{
        FLASHWINFO flashinfo;
		flashinfo.cbSize = sizeof(FLASHWINFO);
		flashinfo.hwnd = wminfo.info.win.window;
		flashinfo.uCount = 1;
		flashinfo.dwFlags = FLASHW_ALL;

		if(continuous)
		{
			flashinfo.uCount = 0;
			flashinfo.dwFlags |= FLASHW_TIMERNOFG;
		}

		FlashWindowEx(&flashinfo);
	}

#elif defined(THEALL_MACOSX)

	macosx::requestAttention(continuous);

#else

	THEALL_UNUSED(continuous);
	
#endif
	
	// TODO: Linux?
}
