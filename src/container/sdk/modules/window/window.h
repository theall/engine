/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

#ifndef THEALL_WINDOW_SDL_WINDOW_H
#define THEALL_WINDOW_SDL_WINDOW_H

#include "utils/pch.h"

#include "common/object.h"

#include "../image/imagedata.h"

// Different window settings.
enum SettingType
{
    SETTING_FULLSCREEN,
    SETTING_FULLSCREEN_TYPE,
    SETTING_VSYNC,
    SETTING_MSAA,
    SETTING_RESIZABLE,
    SETTING_MIN_WIDTH,
    SETTING_MIN_HEIGHT,
    SETTING_BORDERLESS,
    SETTING_CENTERED,
    SETTING_DISPLAY,
    SETTING_HIGHDPI,
    SETTING_REFRESHRATE,
    SETTING_X,
    SETTING_Y,
    SETTING_TYPE_COUNT
};

enum FullscreenType
{
    FULLSCREEN_EXCLUSIVE,
    FULLSCREEN_DESKTOP,
    FULLSCREEN_TYPE_COUNT
};

enum MessageBoxType
{
    MESSAGEBOX_ERROR,
    MESSAGEBOX_WARNING,
    MESSAGEBOX_INFO,
    MESSAGEBOX_TYPE_COUNT
};

struct TWindowSize
{
    int width;
    int height;

    bool operator == (const TWindowSize &w) const
    {
        return w.width == width && w.height == height;
    }
};

struct TMessageBoxData
{
    MessageBoxType type;

    std::string title;
    std::string message;

    std::vector<std::string> buttons;
    int enterButtonIndex;
    int escapeButtonIndex;

    bool attachToWindow;
};

struct TWindowSettings
{
    bool fullscreen = false;
    FullscreenType fstype = FULLSCREEN_DESKTOP;
    bool vsync = true;
    int msaa = 0;
    bool resizable = false;
    int minwidth = 1;
    int minheight = 1;
    bool borderless = false;
    bool centered = true;
    int display = 0;
    bool highdpi = false;
    double refreshrate = 0.0;
    bool useposition = false;
    int x = 0;
    int y = 0;
};

class TWindow : public TObject
{
    DECLARE_INSTANCE(TWindow)

public:
    TWindow();
    ~TWindow();

    bool setWindow(int width = 800, int height = 600, TWindowSettings *windowSettings = nullptr);
    void getWindow(int &width, int &height, TWindowSettings &windowSettings);

	void close();

    bool setFullscreen(bool fullscreen, FullscreenType fstype);
    bool setFullscreen(bool fullscreen);

	bool onSizeChanged(int width, int height);

	int getDisplayCount() const;

	const char *getDisplayName(int displayindex) const;

    std::vector<TWindowSize> getFullscreenSizes(int displayindex) const;

	void getDesktopDimensions(int displayindex, int &width, int &height) const;
    TWindowSize getSize() const;

	void setPosition(int x, int y, int displayindex);
	void getPosition(int &x, int &y, int &displayindex);

	bool isOpen() const;

    void setWindowTitle(const std::string &title);
	const std::string &getWindowTitle() const;

    bool setIcon(TImageData *imgd);
    TImageData *getIcon();

	void setDisplaySleepEnabled(bool enable);
	bool isDisplaySleepEnabled() const;

	void minimize();
	void maximize();

	void swapBuffers();

	bool hasFocus() const;
	bool hasMouseFocus() const;

	bool isVisible() const;

	void setMouseVisible(bool visible);
	bool getMouseVisible() const;

	void setMouseGrab(bool grab);
	bool isMouseGrabbed() const;

	void getPixelDimensions(int &w, int &h) const;
	void windowToPixelCoords(double *x, double *y) const;
	void pixelToWindowCoords(double *x, double *y) const;

	double getPixelScale() const;

	double toPixels(double x) const;
	void toPixels(double wx, double wy, double &px, double &py) const;
	double fromPixels(double x) const;
	void fromPixels(double px, double py, double &wx, double &wy) const;

	const void *getHandle() const;

    bool showWarning(const std::string &title, const std::string &message);
    bool showError(const std::string &title, const std::string &message);
    bool showMessage(const std::string &title, const std::string &message);

    int showMessageBox(const TMessageBoxData &data);

	void requestAttention(bool continuous);

private:
	struct ContextAttribs
	{
		int versionMajor;
		int versionMinor;
		bool gles;
		bool debug;
	};

    bool showMessageBox(const std::string &title,
                        const std::string &message,
                        MessageBoxType type,
                        bool attachtowindow=true);
	void setGLFramebufferAttributes(int msaa, bool sRGB);
	void setGLContextAttributes(const ContextAttribs &attribs);
	bool checkGLVersion(const ContextAttribs &attribs, std::string &outversion);
	bool createWindowAndContext(int x, int y, int w, int h, Uint32 windowflags, int msaa);

	// Update the saved window settings based on the window's actual state.
    void updateSettings(const TWindowSettings &newsettings);

	SDL_MessageBoxFlags convertMessageBoxType(MessageBoxType type) const;

    std::string mTitle;

    int mWindowWidth;
    int mWindowHeight;
    int mPixelWidth;
    int mPixelHeight;
    TWindowSettings mWindowSettings;
    StrongRef<TImageData> mIcon;

    bool mOpen;
    bool mMouseGrabbed;
    SDL_Window *mWindow;
    SDL_GLContext mContext;

    bool mDisplayedWindowError;
    bool mHasSDLLegacy;

}; // TWindow

#endif // THEALL_WINDOW_WINDOW_H
