#include "engine.h"

#include <map>
#include <string>

#define DELETE_OBJECT(x) if(x){ delete x;x=NULL; }

static const int DEFULT_FPS = 60;
bool no_game_code = false;

using namespace std;

// Replace any \ with /.
string normalSlashes(string p)
{
    char *pc = (char*)p.c_str();
    while(*pc!=0)
    {
        if(*pc=='\\')
            *pc = '/';
        pc++;
    }
    return p;
}

// Makes sure there is a slash at the }
// of a path.
string endSlash(string p)
{
    if(p.back() != '/')
        p += "/";
    return p;
}

// Checks whether a path is absolute or not.
bool isAbsolutePath(string p)
{
    string tmp = normalSlashes(p);

    // Path is absolute if(it starts with a "/".
    if((p.front()=='/') || (p.length()>2 && p.at(1)==':'))
        return true;

    // Relative.
    return false;
}

// Converts any path into a full path.
string getFullPath(string p)
{
    if(isAbsolutePath(p))
        return normalSlashes(p);

    string cwd = TFileSystem::instance()->getWorkingDirectory();
    cwd = normalSlashes(cwd);
    cwd = endSlash(cwd);

    return cwd + normalSlashes(p);
}

// Returns the leaf of a full path.
string getPathLeaf(string p)
{
    p = normalSlashes(p);
    int a = 1;
    string last = p;

    while(a)
    {
        a = last.find("/", a+1);

        if(a)
            last = p.substr(a+1);
    }

    return last;
}

// Finds the key in the table with the lowest integral index. The lowest
// will typically the executable, for instance "lua5.1.exe".
string getArgLow(map<int, string> a)
{
    int m = 0x7fffffff;
    for(map<int, string>::iterator it=a.begin();it!=a.end();++it)
        if(it->first < m)
            m = it->first;
    return a[m];
}


////////////////////////////////////////////////////////////-
//// Error screen.
////////////////////////////////////////////////////////////-

//local void error_printer(msg, layer) {
//    print((debug.traceback("Error: " + tostring(msg), 1+(layer or 1)):gsub("\n[^\n]+$", "")))
//}

TEngine::TEngine(int argc, char *argv[]) :
    mFps(DEFULT_FPS)
  , mSkipFrames(0)
  , mWidth(800)
  , mHeight(600)
{
    if(argc < 1)
        throw TException("Invalid arguments.");

    setFps(DEFULT_FPS);

    TFileSystem::instance()->init(argv[0]);
    TAudio::instance();
    TGraphics::instance();
    TWindow::instance();
}

TEngine::~TEngine()
{
    TFileSystem::deleteInstance();
    TGraphics::deleteInstance();
    TWindow::deleteInstance();
    TAudio::deleteInstance();
}

void TEngine::boot()
{

}

bool TEngine::init()
{
    TWindow *window = TWindow::instance();

    //createHandlers();

    TWindowSettings windowSettings;
    windowSettings.display = true;
    if(!window->setWindow(mWidth, mHeight, &windowSettings))
        return false;

    // Our first timestep, because window creation can take some time
    TTimer *timer = TTimer::instance();
    timer->step();

    TFileSystem *fileSystem = TFileSystem::instance();
    fileSystem->setAndroidSaveExternal(false);
    fileSystem->setFused(true);
    fileSystem->setIdentity("capture");

    return true;
}

void TEngine::handleError(const string &msg)
{
    printf("%s\n", msg.c_str());

    TGraphics *graphics = TGraphics::instance();
    TWindow *window = TWindow::instance();
    TMouse *mouse = TMouse::instance();
    TJoystickManager *joystickManager = TJoystickManager::instance();
    if(!graphics->isCreated() || !window->isOpen()) {
        if(!window->setWindow(mWidth, mHeight))
            return;
    }

    // Reset state.
    mouse->setVisible(true);
    mouse->setGrabbed(false);
    mouse->setRelativeMode(false);
    if(mouse->hasCursor()) {
        mouse->setCursor();
    }
    for(int i=0;i<joystickManager->getJoystickCount();i++)
    {
        joystickManager->getJoystick(i)->setVibration();
    }

    TAudio::instance()->stop();
    graphics->reset();

    //TFont font = graphics->setNewFont(floor(window->toPixels(14)));
    graphics->setBackgroundColor(Colorf(89, 157, 220));
    graphics->setColor(Colorf(255, 255, 255, 255));

    graphics->clear(graphics->getBackgroundColor());
    graphics->origin();

    TEvent *event = TEvent::instance();
    TMessage *message;
    while(true)
    {
        // Process events.
        event->pump();

        if(event->poll(message))
        {
            if(message->getValue()==EVENT_QUIT)
                break;
        }

        double pos = window->toPixels(70);
        graphics->clear(graphics->getBackgroundColor());
        graphics->print(msg, pos, pos);
        graphics->present();

        TTimer::instance()->sleep(40);
    }
}

int TEngine::run()
{
    TTimer *timer = TTimer::instance();

    try {
        boot();
        init();
        // We don't want the first frame's dt to include time taken by love.load.
        timer->step();

        load();

        double dt = 0;

        TEvent *event = TEvent::instance();
        TMessage *msg;
        TGraphics *graphics = TGraphics::instance();
        graphics->setBackgroundColor(Colorf(0,0,0,0));

        // Main loop time.
        while(true)
        {
            while(true)
            {
                // Process events.
                event->pump();

                if(event->poll(msg))
                {
                    bool continueRun = onMessage(msg);
                    if(!continueRun && msg->value==EVENT_QUIT)
                        return 0;
                } else {
                    break;
                }
            }

            // Update dt, as we'll be passing it to update
            timer->step();
            dt = timer->getAverageDelta();

            // Call update and draw
            update();
            for(int i=0;i<mSkipFrames;i++)
            {
                update();
            }

            graphics->clear(graphics->getBackgroundColor());

            draw();

            graphics->present();
            int currentFps = timer->getFPS();
            if(currentFps != mFps)
            {
                mSleepMicroSecs -= 0.0005 * (mFps - currentFps);
            }
            dt = mSleepMicroSecs - dt;
            if(dt >= 1)
                timer->sleep(dt);
        }

#ifdef THEALL_ANDROID
    SDL_Quit();
#endif
    } catch (TException e) {
        handleError(e.what());
    } catch (std::string e) {
        handleError(e.c_str());
    } catch (const char *str) {
        handleError(str);
    }
#ifndef DEBUG
    catch (...) {
        handleError("Unknown error.");
    }
#endif
    return 0;
}

int TEngine::fps() const
{
    return mFps;
}

void TEngine::setFps(int fps)
{
    if(fps <= 0)
        throw TException("Fps value must not be zero.");

    mFps = fps;
    mSleepMicroSecs = (float)1000/fps;
}

void TEngine::setWindowResolution(int width, int height)
{
    mWidth = width;
    mHeight = height;
}

int TEngine::skipFrames() const
{
    return mSkipFrames;
}

void TEngine::setSkipFrames(int skipFrames)
{
    mSkipFrames = skipFrames;
    if(mSkipFrames < 0)
        mSkipFrames = 0;
    else if(mSkipFrames > 9)
        mSkipFrames = 9;
}

