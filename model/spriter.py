#!/usr/bin/env python

from PyQt5.QtWidgets import QGraphicsScene

class GameScene(QGraphicsScene):
    def __init__(self, parent=None):
        super().__init__(parent)
        
        scene = QGraphicsScene()
        scene.setBackgroundBrush(Qt.darkGreen)
        self.setScene(scene)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    w = GameWindow()
    w.show()
    sys.exit(app.exec_())
