struct Point
    int x
    int y

struct Rect
    int x
    int y
    int width
    int height
    
struct Area
    rect rect
    int fleeDir
    int dangerArea
    int edges
    int samovedBy

struct Areas
    int count
    Area[count] areas
    
struct DArea
    rect rect
    int fleeDir
    int daType
    int daTargetH
    int damovedBy
    
struct DAreas
    int count
    DArea[count] dareas
    
struct Plat
    float xplat
    float yplat
    float platXspeed
    float platYspeed
    int dangerplat
    int drawplat
    int platWidth
    int platHeight
    int platCurPoint
    int platPointsAmount
    int platPic
    int platUseTrigger
    int platEventN
    int platFinalDest
    int platBreak
    int platChunk
    int platSound
    int platBreakable
    int platEventN2
    point[platPointsAmount] point

struct Plats
    int count
    Plat[count] plats

struct Box
    float xbox
    float ybox
    float boxXspeed
    float boxYspeed
    int targetBox
    int drawbox
    int boxWidth
    int boxHeight
    int boxCurPoint
    int boxPointsAmount
    int boxType
    int boxChunkType
    int boxHitMode
    float boxHitTime
    float boxHitSpeed
    float boxHitYSpeed
    int boxDamage
    int boxHitSound
    int boxUseTrigger
    int boxEventN
    int boxFinalDest
    int boxBreak
    int boxSound
    int boxBreakable
    int boxEventN2
    Point[boxPointsAmount] point

struct Boxs
    int count
    Box[count] boxs

struct Walls
    int count
    Rect[count] rect

struct Tile
    float xTile
    float yTile
    int tileNumber
    int tileSetNumber
    // Check If any tile is inexistent And warn
    // If tileSetNumber(b,n) = 0 Or tileNumber(b,n) = 0 Then 
        msg=1:msgseq=0:msgTxt$="Tile# " +n+ " on BG# " +b+ " error!"
    // EndIf
    int tileXend1
    int tileXend2
    int tileXrand1
    int tileXrand2
    float tileXspeed
    int tileYend1
    int tileYend2
    int tileYrand1
    int tileYrand2
    float tileYspeed
    int tileXstart
    int tileYstart
    int tileFollow
    int tileTarget
    int xTile2
    int yTile2
    int tileFollowType
    float xTileScrSpeed
    float yTileScrSpeed

struct Tiles
    int count
    Tile[count] tiles

struct Pawn
    Point start
    Point respawn

struct Pawns
    int count
    Pawn[count] pawn

struct Trigger
    Rect rect
    int way
    int on
    int zAction
    int passBy
    int objhit
    int event
    int draw
    int imageN
    int imgX
    int imgY
    int affect
    int sound
    int follow
    int platX
    int platY
    int onStatus
    int offStatus

struct Triggers
    int count
    Trigger[count triggers

struct AniFrame
    int taniBg
    int taniN
    int tAniTime
    
struct Animation
    int taniseq(n)
    int tAniFramesCount(n)
    int taniBgSel(n)
    int tAniNSel(n)
    int taniCurFrame(n)
    AniFrame[tAniFramesCount] frames

struct Animations
    int count
    Animation[count] animation
    
struct FFac
    int xfac(n,i)
    int yfac(n,i)
    int facDir(n,i)
    int facLife(n,i)
    int facLives(n,i)
    int facTeam(n,i)
    int facDamage(n,i)
    int facAiLevel(n,i)
    int facTeam(n,i)
    int facCategory(n,i)
    int facType(n,i)
    int facDelay(n,i)
    int facDeadEvent(n,i)
    int facWaitEvent(n,i)
    int facChunk(n,i)
    int facSound(n,i)
    int facVar1(n,i)
    int facVar2(n,i)
    int facVar3(n,i)
    int facVar4(n,i)
    int facVar5(n,i)
    
    //If facCategory(n,i)=2 And facAiLevel(n,i) = 0 Then
    //    msg=1:msgseq=0:msgTxt$="objDir=0 on Factory " +n+ " sub "+i
    //End If

struct FFacHead
    int curF(n)
    int FdelaySeq(n)
    int Fevent(n)
    int FfacAmount(n)
    int Floop(n)
    FFac[FfacAmount] ffac
    
struct FFacHeads
    int Famount
    FFacHead[Famount] facs
    
struct map
    Areas areas
    Plats plats
    Boxs boxs
    Walls walls
    int colorR
    int colorG
    int colorB
    Tiles[5] background
    Pawns pawns
    Point flagRed
    Point flagGreen
    int scrollMap
    int[100] EventN
    Triggers triggers
    int[5] nextMap
    int xScrStart
    int yScrStart
    int fightMode
    int curMap
    int ScrLock
    int vsMode
    int yScrCameraLimit
    int uScrLimit
    int noAirStrike
    int var4
    int var5
    int var6
    int var7
    int var8
    int var9
    int var10
    String Stri$
    String Stri$
    String Stri$
    int lScrLimit
    int rScrLimit
    int musicN1
    int musicN2
    Animations anis //executes indepedent animations For tiles
    FFacHeads facs
    
