from PIL import Image
import sys
import os

def printHelp():
	print("python3 merge.py [switch] file/directory\n")
	print("Input parameters for file!")
	print(" -f fileName shuttle image")
	print(" -ft fileName shuttle image with top")
	print(" -fl fileName shuttle image with left")
	print(" -fr fileName shuttle image with right")
	print(" -fb fileName shuttle image with bottom")
	print(" -d directory  merge images in directores")
	print(" -t directory transform images in directores")
	print("-dc merge images in directores,with image center")
	print("-fba filename process snapshot from fba emulator")

def fba(infile):
	try:
		im = Image.open(file)
	except IOError:
		print("cannot open image for", infile)
		return 0
	
	left = 0
	top = -1
	width = im.size[0]
	height = im.size[1]
	newWidth = width
	newHeight = height
	
	#find the top of character
	for i in range(0,height):
		for j in range(0,width):
			rgb = im.getpixel((j,i))
			if rgb[0]==0 and rgb[1]==0 and rgb[2]==0:
				top = i
				break
		if top>=0:
			break
	
	box = (left, top, left+newWidth, top+newHeight)
	boxOrg = (0,0,width,height)
	if box!=boxOrg:
		im = im.crop(box)
		if save:
			print(file)
			im.save(file)
	return im
	
def process(argv):
	if len(argv)==1:
		printHelp()
		return
	param = argv[1]
	inDir = argv[2]
	if param=="-f":
		shuttle(inDir,True)
		return
	elif param=="-ft":
		shuttleTop(inDir,True)
		return
	elif param=="-fb":
		shuttleBottom(inDir,True)
		return	
	
	if param=="-t":
		transform(inDir)
		return
	
	if param!="-d" and param!="-dc":
		printHelp()
		return
	
	if param=="-fba":
		fba(inDir)
		return
	
	files = []
	
	outfilePath = inDir + "\\_out_put.bmp"
	if os.path.exists(outfilePath):
		os.remove(outfilePath)
	for r,d,f in os.walk(inDir):
		files = f
		break
	
	if len(files)<=0:
		print("No file found in directory:" + inDir)
	
	imList = []
	for f in files:
		imList.append(shuttle(inDir+"\\"+f))
		
	#find the maximize of width and height
	width = 0
	height = 0
	for im in imList:
		if im.size[0]>width:
			width = im.size[0]
		if im.size[1]>height:
			height = im.size[1]
	
	#resize all the images to the same size
	imgCounts = len(files)
	hCounts = 10
	if hCounts>imgCounts:
		hCounts = imgCounts
	vCounts = imgCounts/hCounts
	if imgCounts%hCounts>0:
		vCounts += 1
	vCounts *= 2
	
	lWidth = width * hCounts
	lHeight = height * vCounts
	om = Image.new("RGB",(lWidth,lHeight))
	print("imgCounts:"+str(imgCounts)+" hCounts:"+str(hCounts)+" vCounts:"+str(vCounts))
	#draw left images on output image
	for i in range(0,len(imList)):
		x = (i%hCounts) * width - 1
		y = (i/hCounts) * height - 1
		if param == "-d":
			dx = 0#(width - imList[i].size[0]) / 2
			dy = height - imList[i].size[1]
		else:
			dx = (width - imList[i].size[0]) / 2
			dy = (height - imList[i].size[1]) / 2
		x += dx
		y += dy
		r = x + imList[i].size[0]
		b = y + imList[i].size[1]
		om.paste(imList[i], (x,y,r,b))

	for i in range(0,len(imList)):
		x = (i%hCounts) * width - 1
		y = (i/hCounts+vCounts/2) * height - 1
		if param == "-d":
			dx = 0#(width - imList[i].size[0]) / 2
			dy = height - imList[i].size[1]
		else:
			dx = (width - imList[i].size[0]) / 2
			dy = (height - imList[i].size[1]) / 2
		x += dx
		y += dy
		r = x + imList[i].size[0]
		b = y + imList[i].size[1]
		om.paste(imList[i].transpose(Image.FLIP_LEFT_RIGHT), (x,y,r,b))
	
	om.save(outfilePath)
	print(outfilePath)

def merge(files):
	if len(files)<=0:
		return

def transform(file):
	im = Image.open(file)
	im = im.transpose(Image.FLIP_LEFT_RIGHT)
	im.save(file)
	pass

def shuttleBottom(file,save=False):
	try:
		im = Image.open(file)
	except IOError:
		print("cannot open image for", infile)
		return 0
	
	left = 0
	top = -1
	width = im.size[0]
	height = im.size[1]
	newWidth = width
	newHeight = 0
	
	#find the top of character
	for i in range(0,height):
		for j in range(0,width):
			rgb = im.getpixel((j,i))
			if rgb[0]!=0 or rgb[1]!=0 or rgb[2]!=0:
				top = i
				break
		if top>=0:
			break

	#find height of character
	for i in range(height-1,top,-1):
		for j in range(0,width):
			rgb = im.getpixel((j,i))
			#if rgb[0]>10 or rgb[1]>10 or rgb[2]>10:
			#	print(str(rgb[0])+"."+str(rgb[1])+"."+str(rgb[2]))
			#	break
			if rgb[0]==162 and rgb[1]==117 and rgb[2]==29:
				newHeight = i - top + 1
				break
		if newHeight>0:
			break
	
	box = (left, top, left+newWidth, newHeight)
	newHeight = newHeight + 20
	box2 = (left, top, left+newWidth, newHeight)
	boxOrg = (0,0,width,height)
	om = Image.new("RGB",(newWidth,newHeight))
	if box!=boxOrg:
		#im = im.crop(box)
		om.paste(im,boxOrg)
		if save:
			print(file)
			om.save(file)
	return om
	
def shuttleTop(file,save=False):
	try:
		im = Image.open(file)
	except IOError:
		print("cannot open image for", infile)
		return 0
	
	left = 0
	top = -1
	width = im.size[0]
	height = im.size[1]
	newWidth = width
	newHeight = height
	
	#find the top of character
	for i in range(0,height):
		for j in range(0,width):
			rgb = im.getpixel((j,i))
			if rgb[0]!=0 or rgb[1]!=0 or rgb[2]!=0:
				top = i
				break
		if top>=0:
			break
	
	box = (left, top, left+newWidth, newHeight-top)
	boxOrg = (0,0,width,height)
	if box!=boxOrg:
		im = im.crop(box)
		if save:
			print(file)
			im.save(file)
	return im
	
def shuttle(file,save=False):
	try:
		im = Image.open(file)
	except IOError:
		print("cannot open image for", infile)
		return 0
	
	left = -1
	top = -1
	width = im.size[0]
	height = im.size[1]
	newWidth = 0
	newHeight = 0
	
	#find the top of character
	for i in range(0,height):
		for j in range(0,width):
			rgb = im.getpixel((j,i))
			if rgb[0]!=0 or rgb[1]!=0 or rgb[2]!=0:
				top = i
				break
		if top>=0:
			break
	
	#find height of character
	for i in range(height-1,top,-1):
		for j in range(0,width):
			rgb = im.getpixel((j,i))
			if rgb[0]!=0 or rgb[1]!=0 or rgb[2]!=0:
				newHeight = i - top + 1
				break
		if newHeight>0:
			break
	
	#find left of character
	for i in range(0,width):
		for j in range(0,height):
			rgb = im.getpixel((i,j))
			if rgb[0]!=0 or rgb[1]!=0 or rgb[2]!=0:
				left = i
				break
		if left>=0:
			break
	
	#find width of character
	for i in range(width-1,left,-1):
		for j in range(0,height):
			rgb = im.getpixel((i,j))
			if rgb[0]!=0 or rgb[1]!=0 or rgb[2]!=0:
				newWidth = i - left + 1
				break
		if newWidth>0:
			break
	
	box = (left, top, left+newWidth, top+newHeight)
	boxOrg = (0,0,width,height)
	if box!=boxOrg:
		im = im.crop(box)
		if save:
			print(file)
			im.save(file)
	return im

if __name__ == '__main__':
    if len(sys.argv)<2:
        printHelp()
        sys.exit()
    
    process(sys.argv)
    
