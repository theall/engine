# -*- coding: utf-8 -*-

import os, sys
import math
from struct import unpack
import json
import uuid
import shutil
import time
import re

g_appRoot = './'
g_mapsDir = g_appRoot + 'maps/'
g_gfxRoot = g_appRoot+'images/'
g_tilePath = g_gfxRoot+'tiles/'
g_stuffPath = g_gfxRoot+'stuff/'
g_soundPath = g_appRoot+'sounds/'
g_musicPath = g_soundPath
g_outDir = 'scene/'
g_imageDir = g_outDir + 'image'
g_soundDir = g_outDir + 'sound'

def initProjectDir(root):
    characterRoot = root
    characterImageDir = os.path.join(characterRoot, 'image')
    characterSoundDir = os.path.join(characterRoot, 'sound')
    createDir(characterImageDir)
    createDir(characterSoundDir)
    return characterRoot,characterImageDir,characterSoundDir

def getObjectProperties(obj):
    p = {}
    for name in obj.__dir__():
        nameObj = obj.__getattribute__(name)
        if name.startswith('_') or callable(nameObj):
            continue
        nameType = type(nameObj)
        if nameType in [list]:
            continue
        p[name] = nameObj
    return p

def createJsonHeader(type, name):
    jsonObj = {}
    jsonObj['type'] = type
    jsonObj['name'] = name
    jsonObj['uuid'] = str(uuid.uuid1())
    jsonObj['update_time'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
    jsonObj['create_time'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
    jsonObj['author'] = 'bilge theall'
    jsonObj['version'] = '1.0.0.0'
    jsonObj['comment'] = 'Export by program.All rights not reserved.'
    jsonObj['contact'] = 'wazcd_1608@qq.com'
    return jsonObj

def createDir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

def getImageSize(srcfullPath):
    stream = open(srcfullPath, 'rb')
    data = stream.read(18)
    x, y = unpack('<LL', stream.read(8))
    return x, y

def copyImage(src, dest):
    srcPath, name = os.path.split(src)
    destFullPath = os.path.join(dest, name)
    if not os.path.exists(destFullPath):
        shutil.copy(src, destFullPath)
    x, y = getImageSize(src)
    return 'image/' + name, x, y

def copyTile(name, destDir):
    return copyImage(os.path.join(g_tilePath, name), destDir)

def copyPlat(name, destDir):
    return copyImage(os.path.join(g_stuffPath, name), destDir)

def copyMusic(src, destDir):
    srcPath, fileName = os.path.split(src)
    baseName, extName = os.path.splitext(fileName)
    srcWithoutExt = os.path.join(srcPath, baseName)
    findSrc = ''
    for ext in ['mp3','wav']:
        testSrc = os.path.join(g_musicPath, srcWithoutExt + '.' + ext)
        if os.path.exists(testSrc):
            findSrc = testSrc
            break
    if findSrc=='':
        print("Didn't find music with name %s"%src)
        return src
    _,fileName = os.path.split(findSrc)
    destFullPath = os.path.join(destDir, fileName)
    if not os.path.exists(destFullPath):
        shutil.copy(findSrc, destFullPath)
    return 'sound/'+fileName

def generateBoxFrames(boxType):
    boxType = boxType - 1
    boxList = [
        {'name':'Buzz_saw', 'frames':[[1,1,2],[1,2,2]]},
        {'name': 'Lava_chunk', 'frames': [[2, 1, 2], [2, 2, 2]]},
        {'name': 'blue_red_target', 'frames': [[3, 1, 1]]},
        {'name': 'vent', 'frames': [[4, 1, 1]]},
        {'name': 'laser_box', 'frames': [[5, 1, 1]]},
        {'name': 'flower', 'frames': [[6, 1, 15], [6, 2, 15]]}
    ]
    if boxType<0 or boxType>=len(boxList):
        print('No box found.')
        return '', None
    box = boxList[boxType]
    name = box['name']
    frames = []
    for f in box['frames']:
        frame = {}
        frame['image'], width, height = copyImage(g_stuffPath+'box%d_a%d.bmp'%(f[0],f[1]), g_imageDir)
        #frame['anchor'] = [float(width) / 2, height]
        frame['duration'] = f[2]
        frames.append(frame)
    return name, frames

class Point():
    def __init__(self, f):
        self.x      = unpack('i', f.read(4))[0]
        self.y      = unpack('i', f.read(4))[0]

    def toJson(self):
        return [self.x,self.y]
        
class Rect():
    def __init__(self, f):
        self.x      = unpack('i', f.read(4))[0]
        self.y      = unpack('i', f.read(4))[0]
        self.width  = unpack('i', f.read(4))[0]
        self.height = unpack('i', f.read(4))[0]
    
    def toJson(self):
        return [self.x,self.y,self.width,self.height]
        
class Area():
    def __init__(self, f):
        self.rect = Rect(f)
        self.fleeDir    = unpack('i', f.read(4))[0]# face direction
        self.dangerArea = unpack('i', f.read(4))[0]# area is danger?try to escape from it
        self.edges      = unpack('i', f.read(4))[0]# no use
        self.samovedBy  = unpack('i', f.read(4))[0]# moved by plat?
    
    def toJson(self):
        #properties
        prop = {}
        prop['danger'] = self.dangerArea
        prop['edges'] = self.edges
        prop['fleeDir'] = self.fleeDir
        prop['samovedBy'] = self.samovedBy
        prop['rect'] = self.rect.toJson()
        return prop

class Areas():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.areas = []
        for i in range(self.count):
            self.areas.append(Area(f))
    
    def toJson(self):
        arealayer = []
        
        i = 1
        for area in self.areas:
            obj = area.toJson()
            obj['Name'] = 'area%d'%i
            arealayer.append(obj)
            i += 1
            
        return arealayer
        
class DArea():
    def __init__(self, f):
        self.rect = Rect(f)
        self.fleeDir    = unpack('i', f.read(4))[0]# direction,2,4
        self.daType     = unpack('i', f.read(4))[0]# daType 1 is For up special,0 is For jumping
        self.daTargetH  = unpack('i', f.read(4))[0]# Target Height,If daTargetH(aa)=5 Then jumpKey(n)=1:aiJumpedRand(n)=1
        self.damovedBy  = unpack('i', f.read(4))[0]# Moved By platform
        
    def toJson(self):
        property = {}
        property['fleeDir'] = self.fleeDir
        property['daType'] = self.daType
        property['daTargetH'] = self.daTargetH
        property['damovedBy'] = self.damovedBy
        property['rect'] = self.rect.toJson()
        return property
        
class DAreas():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.areas = []
        for i in range(self.count):
            self.areas.append(DArea(f))
    
    def toJson(self):
        arealayer = []

        i = 1
        for area in self.areas:
            obj = area.toJson()
            obj['Name'] = 'area%d'%i
                        
            arealayer.append(obj)
            i += 1
        return arealayer

class Plat():
    def __init__(self, f):
        self.xplat            = unpack('f', f.read(4))[0]
        self.yplat            = unpack('f', f.read(4))[0]
        self.platXspeed       = unpack('f', f.read(4))[0]
        self.platYspeed       = unpack('f', f.read(4))[0]
        self.dangerplat       = unpack('i', f.read(4))[0]# danger platform
        self.drawplat         = unpack('i', f.read(4))[0]
        self.platWidth        = unpack('i', f.read(4))[0]
        self.platHeight       = unpack('i', f.read(4))[0]
        self.platCurPoint     = unpack('i', f.read(4))[0]# index in platPointsAmount
        self.platPointsAmount = unpack('i', f.read(4))[0]
        self.platPic          = unpack('i', f.read(4))[0]# platform image,name by number
        self.platUseTrigger   = unpack('i', f.read(4))[0]# use event
        self.platEventN       = unpack('i', f.read(4))[0]# event number
        self.platFinalDest    = unpack('i', f.read(4))[0]# plat Final Point number,for platCurPoint
        self.platBreak        = unpack('i', f.read(4))[0]# nouse
        self.platChunk        = unpack('i', f.read(4))[0]# nouse
        self.platSound        = unpack('i', f.read(4))[0]# nouse
        self.platBreakable    = unpack('i', f.read(4))[0]# nouse
        self.platEventN2      = unpack('i', f.read(4))[0]# final point Event number

        self.ignore = False
        self.points = []
        for i in range(self.platPointsAmount):
            self.points.append(Point(f))
        
    def toJson(self):
        prop = {}
        prop['xplat'] = self.xplat
        prop['yplat'] = self.yplat
        prop['platXspeed'] = self.platXspeed
        prop['platYspeed'] = self.platYspeed
        prop['dangerplat'] = self.dangerplat
        prop['drawplat'] = self.drawplat
        prop['platCurPoint'] = self.platCurPoint
        prop['platPointsAmount'] = self.platPointsAmount
        prop['platPic'] = self.platPic
        prop['platUseTrigger'] = self.platUseTrigger
        prop['platEventN'] = self.platEventN
        prop['platFinalDest'] = self.platFinalDest
        #prop['platBreak'] = self.platBreak
        #prop['platChunk'] = self.platChunk
        #prop['platSound'] = self.platSound
        #prop['platBreakable'] = self.platBreakable
        prop['platEventN2'] = self.platEventN2
        
        subprop = []
        for point in self.points:
            subprop.append(point.toJson())

        prop['points'] = subprop
        return prop
        
class Plats():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.plats = []
        for i in range(self.count):
            self.plats.append(Plat(f))

    def __iter__(self):
        return self.plats.__iter__()

    def findPlatByTile(self, tile):
        if tile.tileFollowType==1 and tile.tileTarget-1<len(self.plats):
            return self.plats[tile.tileTarget-1]
        return None

    def toJson(self):
        arealayer = []
        i = 1
        for area in self.plats:
            obj = area.toJson()
            obj['Name'] = 'plat%d'%i
            arealayer.append(obj)
            i += 1
        return arealayer

# for attacking
class Box():
    def __init__(self, f):
        self.xbox            = unpack('f', f.read(4))[0]
        self.ybox            = unpack('f', f.read(4))[0]
        self.boxXspeed       = unpack('f', f.read(4))[0]
        self.boxYspeed       = unpack('f', f.read(4))[0]
        self.targetBox       = unpack('i', f.read(4))[0]# box which can attacking
        self.drawbox         = unpack('i', f.read(4))[0]# visiable
        self.boxWidth        = unpack('i', f.read(4))[0]
        self.boxHeight       = unpack('i', f.read(4))[0]
        self.boxCurPoint     = unpack('i', f.read(4))[0]# index of boxPointsAmount
        self.boxPointsAmount = unpack('i', f.read(4))[0]
        self.boxType         = unpack('i', f.read(4))[0]
        self.boxChunkType    = unpack('i', f.read(4))[0]# chunk type
        self.boxHitMode      = unpack('i', f.read(4))[0]# 2 calcBox,else:calc blow
        self.boxHitTime      = unpack('f', f.read(4))[0]# hit last frames counts
        self.boxHitSpeed     = unpack('f', f.read(4))[0]
        self.boxHitYSpeed    = unpack('f', f.read(4))[0]
        self.boxDamage       = unpack('i', f.read(4))[0]# damage
        self.boxHitSound     = unpack('i', f.read(4))[0]# default highpunchsnd
        self.boxUseTrigger   = unpack('i', f.read(4))[0]
        self.boxEventN       = unpack('i', f.read(4))[0]
        self.boxFinalDest    = unpack('i', f.read(4))[0]
        self.boxBreak        = unpack('i', f.read(4))[0]# nouse
        self.boxSound        = unpack('i', f.read(4))[0]# nouse
        self.boxBreakable    = unpack('i', f.read(4))[0]# nouse
        self.boxEventN2      = unpack('i', f.read(4))[0]# trigger while arrive the final point
        
        self.points = []
        for i in range(self.boxPointsAmount):
            self.points.append(Point(f))

        self.ignore = False

    def export(self, root, name):
        pass
#        soundFx(1)=slashSnd
#        soundFx(2)=highpunchsnd
#        soundFx(3)=sDoorSnd
#        soundFx(4)=mDoorSnd
#        soundFx(5)=fireHitSnd
#        soundFx(6)=laserSnd
#        soundFx(7)=shotSnd
#        soundFx(8)=clickSnd
#        soundFx(9)=marioFierceSnd
#        soundFx(10)=batsSnd
#        soundFx(11)=shockSnd
        
class Boxs():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.boxs = []
        for i in range(self.count):
            self.boxs.append(Box(f))

    def __iter__(self):
        return self.boxs.__iter__()

    def remove(self, box):
        self.boxs.remove(box)

    def findBoxByTile(self, tile):
        if tile.tileFollowType==2 and tile.tileTarget-1<len(self.boxs):
            return self.boxs[tile.tileTarget-1]
        return None

    def toJson(self):
        arealayer = []
        i = 1
        for area in self.boxs:
            obj = area.toJson()
            obj['Name'] = 'box%d'%i
               
            arealayer.append(obj)
            i += 1
        return arealayer

class Walls():# nouse?
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.rects = []
        for i in range(self.count):
            self.rects.append(Rect(f))

    def toJson(self):
        arealayer = []
        i = 1
        for area in self.rects:
            obj = {}
            obj['rect'] = area.toJson()
            obj['Name'] = 'wall%d'%i
            arealayer.append(obj)
            i += 1
        return arealayer
        
class Tile():
    def __init__(self, f):
        self.xTile          = unpack('f', f.read(4))[0]
        self.yTile          = unpack('f', f.read(4))[0]
        self.tileNumber     = unpack('i', f.read(4))[0]
        self.tileSetNumber  = unpack('i', f.read(4))[0]
        self.tileXend1      = unpack('i', f.read(4))[0]# tile x bound
        self.tileXend2      = unpack('i', f.read(4))[0]# tile x bound
        self.tileXrand1     = unpack('i', f.read(4))[0]# tile x bound of rand
        self.tileXrand2     = unpack('i', f.read(4))[0]# tile x bound of rand
        self.tileXspeed     = unpack('f', f.read(4))[0]
        self.tileYend1      = unpack('i', f.read(4))[0]
        self.tileYend2      = unpack('i', f.read(4))[0]
        self.tileYrand1     = unpack('i', f.read(4))[0]
        self.tileYrand2     = unpack('i', f.read(4))[0]
        self.tileYspeed     = unpack('f', f.read(4))[0]
        self.tileXstart     = unpack('i', f.read(4))[0]# tile start position x
        self.tileYstart     = unpack('i', f.read(4))[0]
        self.tileFollow     = unpack('i', f.read(4))[0]# follow object?
        self.tileTarget     = unpack('i', f.read(4))[0]# follow tile's number
        self.xTile2         = unpack('i', f.read(4))[0]# the offset of tile in plat or box while tile has followtype
        self.yTile2         = unpack('i', f.read(4))[0]
        self.tileFollowType = unpack('i', f.read(4))[0]
        self.xTileScrSpeed  = unpack('f', f.read(4))[0]#tiles scrolls along with background If set
        self.yTileScrSpeed  = unpack('f', f.read(4))[0]
        
    def toJson(self, root):
        rootImageDir = os.path.join(root, 'image')
        prop = {'type':'image', 'pos': [self.xTile, self.yTile]}
        prop['image'], _, _ = copyTile('%d_%d.bmp' % (self.tileSetNumber, self.tileNumber), rootImageDir)
        return prop

class Tiles():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.tiles = []
        for i in range(self.count):
            self.tiles.append(Tile(f))
    
    def __iter__(self):
        return self.tiles.__iter__()
    
    def toJson(self, root):
        layer = {
            "type": "layer",
            "offset": [
                0,
                0
            ]
        }

        sprites = []
        for tile in self.tiles:
            if tile.tileFollowType in [1,2]:
                continue;
            tileJson = tile.toJson(root)
            sprites.append(tileJson)

        layer['sprites'] = sprites
        return layer

    def minPoint(self):
        minx = 0xffffffff
        miny = 0xffffffff
        for tile in self.tiles:
            if tile.xTile < minx:
                minx = tile.xTile
            if tile.yTile < miny:
                miny = tile.yTile
        return [minx, miny]

    def maxPoint(self):
        maxx = -0xffffffff
        maxy = -0xffffffff
        for tile in self.tiles:
            if tile.xTile > maxx:
                maxx = tile.xTile + Tilesets.tileFromId(tile.tileSetNumber, tile.tileNumber).width()
            if tile.yTile > maxy:
                maxy = tile.yTile + Tilesets.tileFromId(tile.tileSetNumber, tile.tileNumber).height()
        return [maxx, maxy]
        
class Pawn():
    def __init__(self, f):
        self.start      = Point(f)#initialize point
        self.respawn    = Point(f)#respawn point

    def toJson(self):
        p = {}
        p['start'] = self.start.toJson()
        p['respawn'] = self.respawn.toJson()
        return p
        
class Pawns():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.pawns = []
        for i in range(self.count):
            self.pawns.append(Pawn(f))
    
    def __iter__(self):
        return self.pawns.__iter__()

    def toJson(self):
        arealayer = []
        i = 1
        for area in self.pawns:
            obj = area.toJson()
            obj['Name'] = 'start%d'%i
                        
            arealayer.append(obj)
            i += 1
        return arealayer

class Trigger():
    def __init__(self, f):
        self.rect = Rect(f)
        #Case 1	;Triggers only once
        #Case 2	;On/Off Trigger
        #Case 3	;Triggers only on real time action
        #Case 4	;play Next game music
        #Case 10	;change fight mode, damage based or life
        #Case 11	;change screen lock status on/off
        #Case 12 ;toggle For no air-special allowed
        #Case 13 ;toggle For no double jump allowed
        #Case 14 ;secret area discovery
        self.way       = unpack('i', f.read(4))[0]
        
        # sign for this trigger
        self.on        = unpack('i', f.read(4))[0]
        
        # If player action(press up) on trigger area, Then trigger it
        self.zAction   = unpack('i', f.read(4))[0]
        
        # If player is on Trigger Area, Then trigger it
        self.passBy    = unpack('i', f.read(4))[0]
        
        # If item hits trigger area, Then trigger it
        self.objhit    = unpack('i', f.read(4))[0]
        
        # event number
        self.event     = unpack('i', f.read(4))[0]
        
        # true:draw image
        self.draw      = unpack('i', f.read(4))[0]
        
        # image number
        self.imageN    = unpack('i', f.read(4))[0]
        
        # image offset
        # (Tx(i)+TimgX(i))-xscr
        self.imgX      = unpack('i', f.read(4))[0]
        self.imgY      = unpack('i', f.read(4))[0]
        
        # 0:affected by only players
        # else:affected by everyone
        self.affect    = unpack('i', f.read(4))[0]
        
        # playing sound number while be triggered
        self.sound     = unpack('i', f.read(4))[0]
        
        # number of platform be followed
        self.follow    = unpack('i', f.read(4))[0]
        
        # offset plat position if follow set
        self.platX     = unpack('i', f.read(4))[0]
        self.platY     = unpack('i', f.read(4))[0]
        
        # Triggers only on real time action,for event set
        self.onStatus  = unpack('i', f.read(4))[0]
        
        # initial value of eventN if trigger type=3
        # eventN(Tevent(n))=ToffStatus(n) : Ton(n)=ToffStatus(n)
        self.offStatus = unpack('i', f.read(4))[0]

    def toJson(self):
        prop = {}
        prop['way'] = self.way
        prop['on'] = self.on
        prop['zAction'] = self.zAction
        prop['passBy'] = self.passBy
        prop['objhit'] = self.objhit
        prop['event'] = self.event
        prop['draw'] = self.draw
        prop['imageN'] = self.imageN
        prop['imgX'] = self.imgX
        prop['imgY'] = self.imgY
        prop['affect'] = self.affect
        prop['sound'] = self.sound
        prop['follow'] = self.follow
        prop['platX'] = self.platX
        prop['platY'] = self.platY
        prop['onStatus'] = self.onStatus
        prop['offStatus'] = self.offStatus
        return prop
        
class Triggers():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.triggers = []
        for i in range(self.count):
            self.triggers.append(Trigger(f))

    def toJson(self):
        arealayer = []
        i = 1
        for area in self.triggers:
            obj = area.toJson()
            obj['Name'] = 'trigger%d'%i
            arealayer.append(obj)
            i += 1
        return arealayer
        
class AniFrame():
    def __init__(self, f):
        self.taniBg     = unpack('i', f.read(4))[0]
        self.taniN      = unpack('i', f.read(4))[0]
        self.tAniTime   = unpack('i', f.read(4))[0]
    
    def toJson(self):
        f = {}
        f['duration'] = self.tAniTime + 1
        f['image'] = OldMap.getTileImage(self.taniBg, self.taniN-1)
        f['anchor'] = [0,0]
        return f
        
class Animation():
    def __init__(self, f):
        # use for tile animation if tileFollowType is set
        self.taniseq         = unpack('i', f.read(4))[0]
        self.tAniFramesCount = unpack('i', f.read(4))[0]
        self.taniBgSel       = unpack('i', f.read(4))[0] 
        self.tAniNSel        = unpack('i', f.read(4))[0]
        self.taniCurFrame    = unpack('i', f.read(4))[0]

        self.ignore = False
        self.frames = []
        for i in range(self.tAniFramesCount):
            self.frames.append(AniFrame(f))

        if self.taniBgSel>=len(OldMap.background) or self.tAniNSel-1>=len(OldMap.background[self.taniBgSel].tiles):
            print('Warning: cannot find tile(%d,%d) from background'%(self.taniBgSel,self.tAniNSel))
            self.tile = None
        else:
            self.tile = OldMap.background[self.taniBgSel].tiles[self.tAniNSel-1]

    def isValid(self):
        return self.tile!=None

    def toJson(self):
        frames = []
        curFrame = self.taniCurFrame - 1
        for i in range(curFrame, self.tAniFramesCount, 1):
            frames.append(self.frames[i])
        frames += self.frames[:curFrame]
        
        newFrames = []
        for f in frames:
            fj = f.toJson()
            newFrames.append(fj)
            
        return newFrames
  
class Animations():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.animations = []
        for i in range(self.count):
            animation = Animation(f)
            if animation.isValid():
                self.animations.append(animation)

    def __iter__(self):
        return self.animations.__iter__()

    def findAnimationByTile(self, tile):
        for a in self.animations:
            if a.tile==tile:
                return a
        return None

    def toJson(self):
        j = {}
        l = []
        for a in self.animations:
            l.append(a.toJson())
        j['animations'] = l
        
class FFac():
    def __init__(self, f):
        self.xfac         = unpack('i', f.read(4))[0]
        self.yfac         = unpack('i', f.read(4))[0]
        self.facDir       = unpack('i', f.read(4))[0]
        self.facLife      = unpack('i', f.read(4))[0]
        self.facLives     = unpack('i', f.read(4))[0]
        self.facTeam      = unpack('i', f.read(4))[0]
        self.facDamage    = unpack('i', f.read(4))[0]
        self.facAiLevel   = unpack('i', f.read(4))[0]
        self.facTeam      = unpack('i', f.read(4))[0]
        
        # Case 1	;players, enemies...
        # Case 2	;Itens
        # Case 3	;shots
        # Case 4 ;chunks
        self.facCategory  = unpack('i', f.read(4))[0]
        
        # chunk type
        self.facType      = unpack('i', f.read(4))[0]
        
        # delay frames
        self.facDelay     = unpack('i', f.read(4))[0]
        
        self.facDeadEvent = unpack('i', f.read(4))[0]
        self.facWaitEvent = unpack('i', f.read(4))[0]
        
        # along with chunk's type
        self.facChunk     = unpack('i', f.read(4))[0]
        
        # sound
        self.facSound     = unpack('i', f.read(4))[0]
        
        self.facVar1      = unpack('i', f.read(4))[0]
        self.facVar2      = unpack('i', f.read(4))[0]
        self.facVar3      = unpack('i', f.read(4))[0]
        self.facVar4      = unpack('i', f.read(4))[0]
        self.facVar5      = unpack('i', f.read(4))[0]
        
    def toJson(self):
        prop = {}
        prop['xfac'] = self.xfac
        prop['yfac'] = self.yfac
        prop['facDir'] = self.facDir
        prop['facLife'] = self.facLife
        prop['facLives'] = self.facLives
        prop['facTeam'] = self.facTeam
        prop['facDamage'] = self.facDamage
        prop['facAiLevel'] = self.facAiLevel
        prop['facTeam'] = self.facTeam
        prop['facCategory'] = self.facCategory
        prop['facType'] = self.facType
        prop['facDelay'] = self.facDelay
        prop['facDeadEvent'] = self.facDeadEvent
        prop['facWaitEvent'] = self.facWaitEvent
        prop['facChunk'] = self.facChunk
        prop['facSound'] = self.facSound
        prop['facVar1'] = self.facVar1
        prop['facVar2'] = self.facVar2
        prop['facVar3'] = self.facVar3
        prop['facVar4'] = self.facVar4
        prop['facVar5'] = self.facVar5
        return prop

class FFacHead():
    def __init__(self, f):
        self.curF       = unpack('i', f.read(4))[0]
        self.FdelaySeq  = unpack('i', f.read(4))[0]
        self.Fevent     = unpack('i', f.read(4))[0]
        self.FfacAmount = unpack('i', f.read(4))[0]
        self.Floop      = unpack('i', f.read(4))[0]
        
        self.ffacs = []
        for i in range(self.FfacAmount):
            self.ffacs.append(FFac(f))
    
    def toJson(self):
        facs = []
        i = 1
        for fac in self.ffacs:
            tmxTile = Tilesets.tileFromFactory(fac.facCategory, fac.facType)
            obj = fac.toJson()
            obj['Name'] = 'object%d'%i
            obj['Type'] = 'object'
            obj['image'] = tmxTile
            facs.append(obj)
            i += 1
        prop = {}
        prop['curF'] = self.curF
        prop['FdelaySeq'] = self.FdelaySeq
        prop['Fevent'] = self.Fevent
        prop['FfacAmount'] = self.FfacAmount
        prop['Floop'] = self.Floop
        prop['facs'] = facs
        return prop
        
class FFacHeads():
    def __init__(self, f):
        self.Famount = unpack('i', f.read(4))[0]
        self.facs = []
        for i in range(self.Famount):
            self.facs.append(FFacHead(f))

    def __iter__(self):
        return self.facs.__iter__()

    def toJson(self):
        p = []
        for a in self.facs:
            p.append(a.toJson())
        return p

class Tileset():
    def __init__(self, name):
        self.name = name
        self.images = []
    def add(self, imgName):
        self.images.append(imgName)

    def toJson(self):
        p = {}
        p['name'] = self.name
        p['images'] = self.images
        return p

    def __iter__(self):
        return self.images.__iter__()

class Tilesets():
    mTileset = None

    def tilesets():
        if Tilesets.mTileset is None:
            tilesets = []
            # tiles
            for i in range(1, 0xffff):
                tileset = Tileset('tile%d'%i)
                for j in range(1, 0xffff):
                    imgName = g_tilePath+'/%d_%d.bmp'%(i, j)
                    if not os.path.isfile(imgName):
                        break
                    tileset.add(imgName)
                if j<2:
                    break

                tilesets.append(tileset)
            # character,52 chars
            tileset = Tileset('character')
            for i in range(1, 53):
                imgName = g_gfxRoot+str(i)+'/zwalk0.bmp'
                if not os.path.isfile(imgName):
                    continue
                tileset.add(imgName)
            tilesets.append(tileset)
            # stuff,obj
            tileset = Tileset('obj')
            for i in range(1, 99):
                imgName = g_stuffPath+'/obj%d_1.bmp'%i
                if not os.path.isfile(imgName):
                    continue
                tileset.add(imgName)
            tilesets.append(tileset)
            # stuff,shot
            tileset = Tileset('shot')
            for i in range(1, 99):
                imgName = g_stuffPath+'/shot%d.bmp'%i
                if not os.path.isfile(imgName):
                    continue
                tileset.add(imgName)
            tilesets.append(tileset)
            # stuff,pt
            tileset = Tileset('obj')
            for i in range(1, 99):
                imgName = g_stuffPath+'/pt%d_a1.bmp'%i
                if not os.path.isfile(imgName):
                    continue
                tileset.add(imgName)
            tilesets.append(tileset)
            # stuff,plat
            tileset = Tileset('plat')
            for i in range(1, 99):
                imgName = g_stuffPath+'/plat%d.bmp'%i
                if not os.path.isfile(imgName):
                    continue
                tileset.add(imgName)
            tilesets.append(tileset)
            Tilesets.mTileset = tilesets
        return Tilesets.mTileset

    def tileFromId(tileSetNo, tileNo):
        imgName = g_tilePath+'/%d_%d.bmp'%(tileSetNo, tileNo)
        return Tilesets.tileFromPath(imgName)

    def tileFromPath(imgName):
        for tileset in Tilesets.tilesets():
            for image in tileset:
                if imgName==image:
                    return image

    def tileFromFactory(facId, facType):
        if facId==1:
            fileName = g_gfxRoot + str(facType) + '/zwalk0.bmp'
        elif facId==2:
            fileName = g_stuffPath + '/obj%d_1.bmp'%facType
        elif facId==3:
            fileName = g_stuffPath + '/shot%d.bmp'%facType
        elif facId==4:
            fileName = g_stuffPath + '/pt%d_a1.bmp'%facType
        else:
            fileName = ""
        return Tilesets.tileFromPath(fileName)
            
class OldMap():
    background = []

    def __init__(self, f):
        self.areas      = Areas(f)
        self.dareas     = DAreas(f)
        self.plats      = Plats(f)
        self.boxs       = Boxs(f)
        self.walls      = Walls(f)
        self.colorR     = unpack('i', f.read(4))[0]
        self.colorG     = unpack('i', f.read(4))[0]
        self.colorB     = unpack('i', f.read(4))[0]
        
        for i in range(6):
            ts = Tiles(f)
            OldMap.background.append(ts)

        self.pawns      = Pawns(f) 
        self.flagRed    = Point(f)
        self.flagGreen  = Point(f)
        
        self.scrollMap  = unpack('i', f.read(4))[0]
        self.EventN     = unpack('100i', f.read(100*4))
        
        self.triggers   = Triggers(f)
        
        self.nextMap    = unpack('5i', f.read(5*4))
        # Camera starting X
        self.xScrStart       = unpack('i', f.read(4))[0]
        self.yScrStart       = unpack('i', f.read(4))[0]
        self.fightMode       = unpack('i', f.read(4))[0]
        self.curMap          = unpack('i', f.read(4))[0]
        
        # screen lock status on/off
        self.ScrLock         = unpack('i', f.read(4))[0]
        self.vsMode          = unpack('i', f.read(4))[0]
        self.yScrCameraLimit = unpack('i', f.read(4))[0]
        self.uScrLimit       = unpack('i', f.read(4))[0]
        
        # no use
        self.noAirStrike     = unpack('i', f.read(4))[0]
        
        self.var4            = unpack('i', f.read(4))[0]
        self.var5            = unpack('i', f.read(4))[0]
        self.var6            = unpack('i', f.read(4))[0]
        self.var7            = unpack('i', f.read(4))[0]
        self.var8            = unpack('i', f.read(4))[0]
        self.var9            = unpack('i', f.read(4))[0]
        self.var10           = unpack('i', f.read(4))[0]
        
        self.str1 = ''
        while(True):
            b = f.read(4)
            x = unpack('i', b)[0]
            if x==0:
                break
            self.str1 += b.decode()
            
        self.str2 = ''
        while(True):
            b = f.read(4)
            x = unpack('i', b)[0]
            if x==0:
                break
            self.str2 += b.decode()
        
        self.str3 = ''
        while(True):
            b = f.read(4)
            x = unpack('i', b)[0]
            if x==0:
                break
            self.str3 += b.decode()

        self.lScrLimit = unpack('i', f.read(4))[0]
        self.rScrLimit = unpack('i', f.read(4))[0]
        self.musicN1   = unpack('i', f.read(4))[0]
        
        # trigger the next music
        self.musicN2   = unpack('i', f.read(4))[0]
        self.anis      = Animations(f) # executes indepedent animations For tiles
        self.facs      = FFacHeads(f)
        self.characterCache = []

    def getTileImage(backNo, tileNo):
        tile = OldMap.background[backNo].tiles[tileNo]
        tmxTile = "%d_%d.bmp"%(tile.tileSetNumber, tile.tileNumber)
        return tmxTile

    def exportBox(self, root, mapName, box):
        characterRoot, characterImageDir, characterSoundDir = initProjectDir(root)

        action = {'triggers':[{'event': 'None', 'value': ''}]}
        boxName, frames = generateBoxFrames(box.boxType)
        if boxName=='':
            return None
        character = createJsonHeader('character', boxName)
        character['icon'] = frames[0]['image']
        action['frames'] = frames
        character['actions'] = [action]
        fileName = '%s_box_%d_%d.json' % (mapName, box.xbox, box.ybox)
        fileName = self.saveCharacter(character, os.path.join(characterRoot, fileName))

        speedModel = {'velocity': math.sqrt(box.boxXspeed * box.boxXspeed + box.boxYspeed * box.boxYspeed)}
        realCurrentPoint = box.boxCurPoint - 1
        boxPoints = box.points[realCurrentPoint:] + box.points[:realCurrentPoint]
        moveModel = self.exportMoveModel(boxPoints, speedModel)
        pos = [boxPoints[0].x, boxPoints[0].y]
        characterSprite = {'type': 'character', 'uuid': fileName, 'pos': pos, 'move_model':moveModel}
        return characterSprite

    def saveCharacter(self, character, fileName):
        if character is None:
            print('Character must not be none')
            return ''

        for i in range(0,len(self.characterCache),2):
            if self.characterCache[i]['actions'] == character['actions']:
                return self.characterCache[i+1]

        basename, ext = os.path.splitext(fileName)
        if ext.lower() != '.json':
            fileName = basename + '.json'
        fp = open(fileName, 'w')
        fp.write(json.dumps(character, indent=4))
        fp.close()

        if fileName.startswith(g_outDir):
            fileName = fileName[len(g_outDir):]
        fileName = fileName.replace('\\', '/')
        self.characterCache.append(character)
        self.characterCache.append(fileName)
        return fileName

    def exportPlat(self, root, mapName, plat):
        if plat.drawplat == 0:
            return {}

        characterRoot, characterImageDir, characterSoundDir = initProjectDir(root)
        character = createJsonHeader('character', 'new character')
        platFileName = 'plat%d.bmp'%plat.platPic
        platImage, platWidth, platHeight = copyPlat(platFileName, characterImageDir)
        character['icon'] = platImage
        action = {}
        action['frames'] = [{'image': platImage, 'vector': [0,0]}]
        action['triggers'] = [{'event': 'None', 'value': ''}]
        character['actions'] = [action]
        fileName, _ = os.path.splitext(platFileName)
        fileName = '%s_%s_%d_%d.json' % (mapName, fileName, plat.xplat, plat.yplat)
        fileName = self.saveCharacter(character, os.path.join(characterRoot, fileName))
        speedModel = {'velocity': math.sqrt(plat.platXspeed * plat.platXspeed + plat.platYspeed * plat.platYspeed)}
        platPoints = plat.points[plat.platCurPoint:] + plat.points[:plat.platCurPoint]
        moveModel = self.exportMoveModel(platPoints, speedModel)
        characterSprite = {'type': 'character', 'uuid': fileName, 'pos': [plat.xplat, plat.yplat]}
        if plat.platPointsAmount>0:
            characterSprite['pos'] = [platPoints[0].x, platPoints[0].y]
        characterSprite['move_model'] = moveModel
        return characterSprite

    def exportRandomSegment(self, tile):
        randomSegment = {'type': 'random'}
        # door
        entryX = tile.tileXstart
        if tile.tileXend1!=0 and tile.tileXend2!=0:
            entryX = (tile.tileXend1+tile.tileXend2) / 2
        entryY = tile.tileYstart
        if tile.tileYend1 != 0 and tile.tileYend2 != 0:
            entryY = (tile.tileYend1 + tile.tileYend2) / 2
        entryX = entryX if entryX != 0 else tile.xTile
        entryY = entryY if entryY != 0 else tile.yTile

        exitX = tile.xTile
        exitY = tile.yTile
        exitWidth = 0
        exitHeight = 0
        if tile.tileXend2 != 0 or tile.tileXend1 != 0:
            if tile.tileXrand1 != 0:
                exitX = tile.tileXstart + tile.tileXrand1
                exitWidth = tile.tileXrand2 - tile.tileXrand1
            else:
                if tile.tileFollow == 0:
                    exitX = tile.tileXstart
                    exitWidth = 0

        if tile.tileYend2 != 0 or tile.tileYend1 != 0:
            if tile.tileYrand1 != 0:
                exitY = tile.tileYstart + tile.tileYrand1
                exitHeight = tile.tileYrand2 - tile.tileYrand1
            else:
                if tile.tileFollow == 0:
                    exitY = tile.tileYstart
                    exitHeight = 0

        if tile.tileFollow == 1:
            print('need extra process!')
            exit = {}
            exit['type'] = 'follow'
            exit['target'] = tile.tileTarget
            exit['left'] = 'true' if tile.tileYspeed > 0 else 'false'
            exit['top'] = 'true' if tile.tileYspeed > 0 else 'false'
        randomSegment['rect'] = [exitX-entryX,exitY-entryY,exitWidth,exitHeight]
        return randomSegment

    def exportTileSegments(self, tile):
        segments = []
        if tile.tileXend1 != 0 or tile.tileXend2 != 0:
            lineSegment = self.exportLineSegment(tile)
            segments.append(lineSegment)
            vector = lineSegment['vector']
            velocity = lineSegment['speed_model']['velocity']
            if tile.tileXrand1 != 0 or tile.tileXrand2 != 0:
                randomSegment = self.exportRandomSegment(tile)
                randomSegment['return_point'] = [-vector[0], -vector[1]]
                randomSegment['return_velocity'] = velocity
                segments.append(randomSegment)
            else:
                pass
        return segments

    def exportLineSegment(self, tile):
        speedModel = {'velocity': 1*math.sqrt(tile.tileXspeed * tile.tileXspeed + tile.tileYspeed * tile.tileYspeed)}
        x = 0
        y = 0
        if tile.tileXend1!=0 \
            or tile.tileXend2!=0 \
            or tile.tileYend1!=0 \
            or tile.tileYend2!=0:
            x = (tile.tileXend1 + tile.tileXend2) / 2 - tile.xTile
            y = (tile.tileYend1 + tile.tileYend2) / 2 - tile.yTile
        else:
            print('No line segment in tile')
        lineSegment = {'type': 'line', 'speed_model': speedModel, 'vector': [x, y]}
        return  lineSegment

    def exportSprite(self, root, mapName, tile, animation=None, bindObject=None):
        characterRoot,characterImageDir,characterSoundDir = initProjectDir(root)
        imageName = '%d_%d.bmp'%(tile.tileSetNumber,tile.tileNumber)
        frameImage, imageWidth, imageHeight = copyTile(imageName, characterImageDir)
        character = createJsonHeader('character', 'new character')
        character['icon'] = frameImage
        action = {'triggers': [{'event': 'None', 'value':''}]}
        if animation:
            animationJson = animation.toJson()
            for ani in animationJson:
                ani['image'], _, _ = copyTile(ani['image'], characterImageDir)
            action['frames'] = animationJson
        else:
            action['frames'] = [{'image': frameImage, 'vector': [0, 0]}]
        character['actions'] = [action]
        fileName, _ = os.path.splitext(imageName)
        fileName = '%s_%s_%d_%d.json'%(mapName, fileName, tile.xTile, tile.yTile)
        fileFullName = os.path.join(characterRoot, fileName)
        fileName = self.saveCharacter(character, fileFullName)

        pos = None
        moveModel = None
        if bindObject:
            box = bindObject if isinstance(bindObject, Box) else None
            plat = bindObject if isinstance(bindObject, Plat) else None
            if box and box.boxPointsAmount>0:
                speedModel = {'velocity': math.sqrt(box.boxXspeed * box.boxXspeed + box.boxYspeed * box.boxYspeed)}
                realCurrentPoint = box.boxCurPoint - 1
                boxPoints = box.points[realCurrentPoint:] + box.points[:realCurrentPoint]
                moveModel = self.exportMoveModel(boxPoints, speedModel)
                pos = [boxPoints[0].x+tile.xTile2, boxPoints[0].y+tile.yTile2]
            elif plat and plat.platPointsAmount>0:
                speedModel = {'velocity': math.sqrt(plat.platXspeed * plat.platXspeed + plat.platYspeed * plat.platYspeed)}
                realCurrentPoint = plat.platCurPoint - 1
                platPoints = plat.points[realCurrentPoint:] + plat.points[:realCurrentPoint]
                moveModel = self.exportMoveModel(platPoints, speedModel)
                pos = [platPoints[0].x+tile.xTile2, platPoints[0].y+tile.yTile2]

        if pos is None:
            if tile.tileXspeed != 0 or tile.tileYspeed != 0:
                pos = [tile.xTile, tile.yTile]
                moveModel = {'loop_count': -1, 'segments': self.exportTileSegments(tile)}
            else:
                pos = [tile.xTile, tile.yTile]

        characterSprite = {'type': 'character', 'uuid': fileName, 'pos': pos}
        if moveModel:
            characterSprite['move_model'] = moveModel
        return [characterSprite]

    def exportMoveModel(self, points, speedModel):
        segments = []
        lastPoint = [points[0].x, points[0].y]
        newPoints = points[1:] + points[:1]
        for i in range(0, len(newPoints), 1):
            currentPoint = [newPoints[i].x, newPoints[i].y]
            if lastPoint == currentPoint:
                continue
            lineSegment = {'type': 'line', 'speed_model': speedModel}
            lineSegment['vector'] = [currentPoint[0]-lastPoint[0], currentPoint[1]-lastPoint[1]]
            lastPoint = currentPoint
            segments.append(lineSegment)
        moveModel = {'loop_count': -1, 'segments': segments}
        return moveModel

    def export(self, outDir, mapName):
        createDir(outDir)
        map = createJsonHeader('Scene', mapName)

        #tiles
        layers = []
        index = 0
        characterOutDir = os.path.join(g_outDir,'character')
        # prepare tile->animation map
        tileAnimationMap = {}
        for ani in self.anis:
            tileAnimationMap[ani.tile] = ani

        for tiles in OldMap.background:
            layer = {'offset': [0,0]}
            sprites = []
            for tile in tiles:
                tileObj = {}
                bindAnimation = self.anis.findAnimationByTile(tile)
                bindObject = None
                if tile.tileFollowType==1:
                    bindObject = self.plats.findPlatByTile(tile)
                elif tile.tileFollowType==2:
                    bindObject = self.boxs.findBoxByTile(tile)

                if tile.tileTarget > 10:
                    tileObj = {}
                elif tile.tileFollow==1 and tile.tileXrand1==0:
                    tileObj = [tile.toJson(g_outDir)]
                elif tile.tileXend1!=0 \
                    or tile.tileXend2!=0 \
                    or tile.tileYend1!=0 \
                    or tile.tileYend2!=0 \
                    or bindObject:
                    # has move model
                    tileObj = self.exportSprite(characterOutDir, mapName, tile, bindAnimation, bindObject)
                    if bindObject:
                        bindObject.ignore = True
                    if bindAnimation:
                        bindAnimation.ignore = True
                elif tileAnimationMap.get(tile):
                    tileObj = self.exportSprite(characterOutDir, mapName, tile, tileAnimationMap.get(tile))
                else:
                    tileObj = [tile.toJson(g_outDir)]
                sprites += tileObj
            layer['sprites'] = sprites
            layers.append(layer)

        mainLayer = {'offset': [0,0]}
        mainLayerSprites = []
        for plat in self.plats:
            if plat.drawplat==1:
                mainLayerSprites.append(self.exportPlat(characterOutDir, mapName, plat))
        for box in self.boxs:
            if box.boxType!=0 and box.drawbox==1 and box.ignore==False:
                mainLayerSprites.append(self.exportBox(characterOutDir, mapName, box))
        mainLayer['sprites'] = mainLayerSprites
        layers.insert(3, mainLayer)
        map['main_layer'] = 3
        map['layers'] = layers
            
        #miscellaneous
        players = []
        for pawn in self.pawns:
            p = pawn.toJson()
            player = {
                "pos": p['start'],
                "direction": "East",
                "respawn": {
                    "type": "absolute",
                    "pos": p['respawn']
                }
            }
            players.append(player)
            
        map['players'] = players
        map['color'] = [self.colorR, self.colorG, self.colorB]
        cx = self.xScrStart if self.xScrStart!=0 else -80
        cy = self.yScrStart if self.yScrStart!=0 else -60
        camera = {'rect':[cx,cy,800,600]}
        if self.scrollMap == 0:
            camera['move_type'] = 'fixed'
        else:
            camera['move_type'] = 'float'

        map['camera'] = camera
        map['scene_type'] = 'combat' if self.vsMode==1 else 'adventure'
        map['sound'] = {"items":[{'file': copyMusic('music%d'%self.musicN1, g_soundDir), "loop_count": -1}]}
        if self.musicN2 > 0:
            map['musicN2'] = 'music%d.mp3'%self.musicN2
        
        map['fps'] = 52
        fileName = mapName+'.json'
        fp = open(os.path.join(outDir, fileName), 'w')
        fp.write(json.dumps(map, sort_keys=True, indent=4))
        fp.close()

def transform(fileName):
    createDir(g_outDir)
    createDir(g_imageDir)
    createDir(g_soundDir)
    filePath, ext = os.path.splitext(fileName)
    if ext == '':
        fileName += '.dat'
    if not os.path.exists(fileName):
        fileName = g_mapsDir + fileName
        if not os.path.exists(fileName):
            print('%s is not exists in maps directory.'%fileName)
            return

    filePath, mapName = os.path.split(filePath)
    fp = open(fileName,'rb')
    map = OldMap(fp)
    map.export(g_outDir, mapName)
    fp.close()
    
if __name__ == '__main__':
    if len(sys.argv)<2:
        print("No file name")
        sys.exit()
    
    transform(sys.argv[1])
