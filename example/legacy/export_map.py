# -*- coding: utf-8 -*-
# export primitive data from map, bilge theall ,all rights giveup
import os, sys
import math
from struct import unpack
import json
import uuid
import shutil
import time
import re
import types

g_mapsDir = 'maps/'
g_outDir = 'scene/'

def getObjectProperties(obj):
    p = {}
    for name in obj.__dir__():
        nameObj = obj.__getattribute__(name)
        if name.startswith('_') or callable(nameObj):
            continue

        if isinstance(nameObj, (int,float)):
            p[name] = nameObj
        elif isinstance(nameObj,(list)):
            l = []
            for o in nameObj:
                l.append(getObjectProperties(o))
            p[name] = l
        elif isinstance(nameObj, (Point,Rect,Area,Areas,DArea,DAreas,Plat,Plats,Box,Boxs,Walls,Tile,Tiles,Pawn,Pawns,Trigger,Triggers,AniFrame,Animation,Animations,FFac,FFacHead,FFacHeads)):
            p[name] = getObjectProperties(nameObj)
    return p

class Point():
    def __init__(self, f):
        self.x      = unpack('i', f.read(4))[0]
        self.y      = unpack('i', f.read(4))[0]

class Rect():
    def __init__(self, f):
        self.x = unpack('i', f.read(4))[0]
        self.y = unpack('i', f.read(4))[0]
        self.width = unpack('i', f.read(4))[0]
        self.height = unpack('i', f.read(4))[0]

class Area():
    def __init__(self, f):
        self.rect = Rect(f)
        self.fleeDir = unpack('i', f.read(4))[0]  # face direction
        self.dangerArea = unpack('i', f.read(4))[0]  # area is danger?try to escape from it
        self.edges = unpack('i', f.read(4))[0]  # no use
        self.samovedBy = unpack('i', f.read(4))[0]  # moved by plat?

class Areas():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.areas = []
        for i in range(self.count):
            self.areas.append(Area(f))
    
        
class DArea():
    def __init__(self, f):
        self.rect = Rect(f)
        self.fleeDir    = unpack('i', f.read(4))[0]# direction,2,4
        self.daType     = unpack('i', f.read(4))[0]# daType 1 is For up special,0 is For jumping
        self.daTargetH  = unpack('i', f.read(4))[0]# Target Height,If daTargetH(aa)=5 Then jumpKey(n)=1:aiJumpedRand(n)=1
        self.damovedBy  = unpack('i', f.read(4))[0]# Moved By platform
        
class DAreas():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.areas = []
        for i in range(self.count):
            self.areas.append(DArea(f))

class Plat():
    def __init__(self, f):
        self.xplat            = unpack('f', f.read(4))[0]
        self.yplat            = unpack('f', f.read(4))[0]
        self.platXspeed       = unpack('f', f.read(4))[0]
        self.platYspeed       = unpack('f', f.read(4))[0]
        self.dangerplat       = unpack('i', f.read(4))[0]# danger platform
        self.drawplat         = unpack('i', f.read(4))[0]
        self.platWidth        = unpack('i', f.read(4))[0]
        self.platHeight       = unpack('i', f.read(4))[0]
        self.platCurPoint     = unpack('i', f.read(4))[0]# index in platPointsAmount
        self.platPointsAmount = unpack('i', f.read(4))[0]
        self.platPic          = unpack('i', f.read(4))[0]# platform image,name by number
        self.platUseTrigger   = unpack('i', f.read(4))[0]# use event
        self.platEventN       = unpack('i', f.read(4))[0]# event number
        self.platFinalDest    = unpack('i', f.read(4))[0]# plat Final Point number,for platCurPoint
        self.platBreak        = unpack('i', f.read(4))[0]# nouse
        self.platChunk        = unpack('i', f.read(4))[0]# nouse
        self.platSound        = unpack('i', f.read(4))[0]# nouse
        self.platBreakable    = unpack('i', f.read(4))[0]# nouse
        self.platEventN2      = unpack('i', f.read(4))[0]# final point Event number

        self.points = []
        for i in range(self.platPointsAmount):
            self.points.append(Point(f))
        
    def toJson(self):
        prop = {}
        prop['xplat'] = self.xplat
        prop['yplat'] = self.yplat
        prop['platXspeed'] = self.platXspeed
        prop['platYspeed'] = self.platYspeed
        prop['dangerplat'] = self.dangerplat
        prop['drawplat'] = self.drawplat
        prop['platCurPoint'] = self.platCurPoint
        prop['platPointsAmount'] = self.platPointsAmount
        prop['platPic'] = self.platPic
        prop['platUseTrigger'] = self.platUseTrigger
        prop['platEventN'] = self.platEventN
        prop['platFinalDest'] = self.platFinalDest
        #prop['platBreak'] = self.platBreak
        #prop['platChunk'] = self.platChunk
        #prop['platSound'] = self.platSound
        #prop['platBreakable'] = self.platBreakable
        prop['platEventN2'] = self.platEventN2
        
        subprop = []
        for point in self.points:
            subprop.append(point.toJson())

        prop['points'] = subprop
        return prop
        
class Plats():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.plats = []
        for i in range(self.count):
            self.plats.append(Plat(f))

    def __iter__(self):
        return self.plats.__iter__()

# for attacking
class Box():
    def __init__(self, f):
        self.xbox            = unpack('f', f.read(4))[0]
        self.ybox            = unpack('f', f.read(4))[0]
        self.boxXspeed       = unpack('f', f.read(4))[0]
        self.boxYspeed       = unpack('f', f.read(4))[0]
        self.targetBox       = unpack('i', f.read(4))[0]# box which can attacking
        self.drawbox         = unpack('i', f.read(4))[0]# visiable
        self.boxWidth        = unpack('i', f.read(4))[0]
        self.boxHeight       = unpack('i', f.read(4))[0]
        self.boxCurPoint     = unpack('i', f.read(4))[0]# index of boxPointsAmount
        self.boxPointsAmount = unpack('i', f.read(4))[0]
        self.boxType         = unpack('i', f.read(4))[0]
        self.boxChunkType    = unpack('i', f.read(4))[0]# chunk type
        self.boxHitMode      = unpack('i', f.read(4))[0]# 2 calcBox,else:calc blow
        self.boxHitTime      = unpack('f', f.read(4))[0]# hit last frames counts
        self.boxHitSpeed     = unpack('f', f.read(4))[0]
        self.boxHitYSpeed    = unpack('f', f.read(4))[0]
        self.boxDamage       = unpack('i', f.read(4))[0]# damage
        self.boxHitSound     = unpack('i', f.read(4))[0]# default highpunchsnd
        self.boxUseTrigger   = unpack('i', f.read(4))[0]
        self.boxEventN       = unpack('i', f.read(4))[0]
        self.boxFinalDest    = unpack('i', f.read(4))[0]
        self.boxBreak        = unpack('i', f.read(4))[0]# nouse
        self.boxSound        = unpack('i', f.read(4))[0]# nouse
        self.boxBreakable    = unpack('i', f.read(4))[0]# nouse
        self.boxEventN2      = unpack('i', f.read(4))[0]# trigger while arrive the final point
        
        self.points = []
        for i in range(self.boxPointsAmount):
            self.points.append(Point(f))
    
    def export(self, root, name):
        pass
#        soundFx(1)=slashSnd
#        soundFx(2)=highpunchsnd
#        soundFx(3)=sDoorSnd
#        soundFx(4)=mDoorSnd
#        soundFx(5)=fireHitSnd
#        soundFx(6)=laserSnd
#        soundFx(7)=shotSnd
#        soundFx(8)=clickSnd
#        soundFx(9)=marioFierceSnd
#        soundFx(10)=batsSnd
#        soundFx(11)=shockSnd
    
    def toJson(self):
        prop = {}
        prop['xbox'] = self.xbox
        prop['ybox'] = self.ybox
        prop['boxXspeed'] = self.boxXspeed
        prop['boxYspeed'] = self.boxYspeed
        prop['targetBox'] = self.targetBox
        prop['drawbox'] = self.drawbox
        prop['boxWidth'] = self.boxWidth
        prop['boxHeight'] = self.boxHeight
        prop['boxCurPoint'] = self.boxCurPoint
        prop['boxPointsAmount'] = self.boxPointsAmount
        prop['boxType'] = self.boxType
        prop['boxChunkType'] = self.boxChunkType
        prop['boxHitMode'] = self.boxHitMode
        prop['boxHitTime'] = self.boxHitTime
        prop['boxHitSpeed'] = self.boxHitSpeed
        prop['boxHitYSpeed'] = self.boxHitYSpeed
        prop['boxDamage'] = self.boxDamage
        prop['boxHitSound'] = self.boxHitSound
        prop['boxUseTrigger'] = self.boxUseTrigger
        prop['boxEventN'] = self.boxEventN
        prop['boxFinalDest'] = self.boxFinalDest
        prop['boxBreak'] = self.boxBreak
        #prop['boxSound'] = self.boxSound
        #prop['boxBreakable'] = self.boxBreakable
        prop['boxEventN2'] = self.boxEventN2
        
        subprop = []
        for point in self.points:
            subprop.append(point.toJson())

        prop['points'] = subprop
        return prop
        
class Boxs():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.boxs = []
        for i in range(self.count):
            self.boxs.append(Box(f))

class Walls():# nouse?
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.rects = []
        for i in range(self.count):
            self.rects.append(Rect(f))

        
class Tile():
    def __init__(self, f):
        self.xTile          = unpack('f', f.read(4))[0]
        self.yTile          = unpack('f', f.read(4))[0]
        self.tileNumber     = unpack('i', f.read(4))[0]
        self.tileSetNumber  = unpack('i', f.read(4))[0]
        self.tileXend1      = unpack('i', f.read(4))[0]# tile x bound
        self.tileXend2      = unpack('i', f.read(4))[0]# tile x bound
        self.tileXrand1     = unpack('i', f.read(4))[0]# tile x bound of rand
        self.tileXrand2     = unpack('i', f.read(4))[0]# tile x bound of rand
        self.tileXspeed     = unpack('f', f.read(4))[0]
        self.tileYend1      = unpack('i', f.read(4))[0]
        self.tileYend2      = unpack('i', f.read(4))[0]
        self.tileYrand1     = unpack('i', f.read(4))[0]
        self.tileYrand2     = unpack('i', f.read(4))[0]
        self.tileYspeed     = unpack('f', f.read(4))[0]
        self.tileXstart     = unpack('i', f.read(4))[0]# tile start position x
        self.tileYstart     = unpack('i', f.read(4))[0]
        self.tileFollow     = unpack('i', f.read(4))[0]# follow object?
        self.tileTarget     = unpack('i', f.read(4))[0]# follow tile's number
        self.xTile2         = unpack('i', f.read(4))[0]# the offset of tile in plat or box while tile has followtype
        self.yTile2         = unpack('i', f.read(4))[0]
        self.tileFollowType = unpack('i', f.read(4))[0]
        self.xTileScrSpeed  = unpack('f', f.read(4))[0]#tiles scrolls along with background If set
        self.yTileScrSpeed  = unpack('f', f.read(4))[0]

class Tiles():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.tiles = []
        for i in range(self.count):
            self.tiles.append(Tile(f))
    
    def __iter__(self):
        return self.tiles.__iter__()
        
    def minPoint(self):
        minx = 0xffffffff
        miny = 0xffffffff
        for tile in self.tiles:
            if tile.xTile < minx:
                minx = tile.xTile
            if tile.yTile < miny:
                miny = tile.yTile
        return [minx, miny]
    
    def maxPoint(self):
        maxx = -0xffffffff
        maxy = -0xffffffff
        for tile in self.tiles:
            if tile.xTile > maxx:
                maxx = tile.xTile + Tilesets.tileFromId(tile.tileSetNumber, tile.tileNumber).width()
            if tile.yTile > maxy:
                maxy = tile.yTile + Tilesets.tileFromId(tile.tileSetNumber, tile.tileNumber).height()
        return [maxx, maxy]
        
class Pawn():
    def __init__(self, f):
        self.start      = Point(f)#initialize point
        self.respawn    = Point(f)#respawn point
        
class Pawns():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.pawns = []
        for i in range(self.count):
            self.pawns.append(Pawn(f))
    
    def __iter__(self):
        return self.pawns.__iter__()
    
class Trigger():
    def __init__(self, f):
        self.rect = Rect(f)
        #Case 1	;Triggers only once
        #Case 2	;On/Off Trigger
        #Case 3	;Triggers only on real time action
        #Case 4	;play Next game music
        #Case 10	;change fight mode, damage based or life
        #Case 11	;change screen lock status on/off
        #Case 12 ;toggle For no air-special allowed
        #Case 13 ;toggle For no double jump allowed
        #Case 14 ;secret area discovery
        self.way       = unpack('i', f.read(4))[0]
        
        # sign for this trigger
        self.on        = unpack('i', f.read(4))[0]
        
        # If player action(press up) on trigger area, Then trigger it
        self.zAction   = unpack('i', f.read(4))[0]
        
        # If player is on Trigger Area, Then trigger it
        self.passBy    = unpack('i', f.read(4))[0]
        
        # If item hits trigger area, Then trigger it
        self.objhit    = unpack('i', f.read(4))[0]
        
        # event number
        self.event     = unpack('i', f.read(4))[0]
        
        # true:draw image
        self.draw      = unpack('i', f.read(4))[0]
        
        # image number
        self.imageN    = unpack('i', f.read(4))[0]
        
        # image offset
        # (Tx(i)+TimgX(i))-xscr
        self.imgX      = unpack('i', f.read(4))[0]
        self.imgY      = unpack('i', f.read(4))[0]
        
        # 0:affected by only players
        # else:affected by everyone
        self.affect    = unpack('i', f.read(4))[0]
        
        # playing sound number while be triggered
        self.sound     = unpack('i', f.read(4))[0]
        
        # number of platform be followed
        self.follow    = unpack('i', f.read(4))[0]
        
        # offset plat position if follow set
        self.platX     = unpack('i', f.read(4))[0]
        self.platY     = unpack('i', f.read(4))[0]
        
        # Triggers only on real time action,for event set
        self.onStatus  = unpack('i', f.read(4))[0]
        
        # initial value of eventN if trigger type=3
        # eventN(Tevent(n))=ToffStatus(n) : Ton(n)=ToffStatus(n)
        self.offStatus = unpack('i', f.read(4))[0]
        
class Triggers():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.triggers = []
        for i in range(self.count):
            self.triggers.append(Trigger(f))
        
class AniFrame():
    def __init__(self, f):
        self.taniBg     = unpack('i', f.read(4))[0]
        self.taniN      = unpack('i', f.read(4))[0]
        self.tAniTime   = unpack('i', f.read(4))[0]
        
class Animation():
    def __init__(self, f):
        # use for tile animation if tileFollowType is set
        self.taniseq         = unpack('i', f.read(4))[0]
        self.tAniFramesCount = unpack('i', f.read(4))[0]
        self.taniBgSel       = unpack('i', f.read(4))[0] 
        self.tAniNSel        = unpack('i', f.read(4))[0]
        self.taniCurFrame    = unpack('i', f.read(4))[0]
        
        self.frames = []
        for i in range(self.tAniFramesCount):
            self.frames.append(AniFrame(f))

class Animations():
    def __init__(self, f):
        self.count = unpack('i', f.read(4))[0]
        self.animations = []
        for i in range(self.count):
            animation = Animation(f)
            self.animations.append(animation)

    def __iter__(self):
        return self.animations.__iter__()

class FFac():
    def __init__(self, f):
        self.xfac = unpack('i', f.read(4))[0]
        self.yfac = unpack('i', f.read(4))[0]
        self.facDir = unpack('i', f.read(4))[0]
        self.facLife = unpack('i', f.read(4))[0]
        self.facLives = unpack('i', f.read(4))[0]
        self.facTeam = unpack('i', f.read(4))[0]
        self.facDamage = unpack('i', f.read(4))[0]
        self.facAiLevel = unpack('i', f.read(4))[0]
        self.facTeam = unpack('i', f.read(4))[0]

        # Case 1	;players, enemies...
        # Case 2	;Itens
        # Case 3	;shots
        # Case 4 ;chunks
        self.facCategory = unpack('i', f.read(4))[0]

        # chunk type
        self.facType = unpack('i', f.read(4))[0]

        # delay frames
        self.facDelay = unpack('i', f.read(4))[0]

        self.facDeadEvent = unpack('i', f.read(4))[0]
        self.facWaitEvent = unpack('i', f.read(4))[0]

        # along with chunk's type
        self.facChunk = unpack('i', f.read(4))[0]

        # sound
        self.facSound = unpack('i', f.read(4))[0]

        self.facVar1 = unpack('i', f.read(4))[0]
        self.facVar2 = unpack('i', f.read(4))[0]
        self.facVar3 = unpack('i', f.read(4))[0]
        self.facVar4 = unpack('i', f.read(4))[0]
        self.facVar5 = unpack('i', f.read(4))[0]

class FFacHead():
    def __init__(self, f):
        self.curF       = unpack('i', f.read(4))[0]
        self.FdelaySeq  = unpack('i', f.read(4))[0]
        self.Fevent     = unpack('i', f.read(4))[0]
        self.FfacAmount = unpack('i', f.read(4))[0]
        self.Floop      = unpack('i', f.read(4))[0]
        
        self.ffacs = []
        for i in range(self.FfacAmount):
            self.ffacs.append(FFac(f))

class FFacHeads():
    def __init__(self, f):
        self.Famount = unpack('i', f.read(4))[0]
        self.facs = []
        for i in range(self.Famount):
            self.facs.append(FFacHead(f))

    def __iter__(self):
        return self.facs.__iter__()

            
class OldMap():
    background = []
    
    def __init__(self, f):
        self.areas      = Areas(f)
        self.dareas     = DAreas(f)
        self.plats      = Plats(f)
        self.boxs       = Boxs(f)
        self.walls      = Walls(f)
        self.colorR     = unpack('i', f.read(4))[0]
        self.colorG     = unpack('i', f.read(4))[0]
        self.colorB     = unpack('i', f.read(4))[0]
        
        for i in range(6):
            ts = Tiles(f)
            OldMap.background.append(ts)

        self.pawns      = Pawns(f) 
        self.flagRed    = Point(f)
        self.flagGreen  = Point(f)
        
        self.scrollMap  = unpack('i', f.read(4))[0]
        self.EventN     = unpack('100i', f.read(100*4))
        
        self.triggers   = Triggers(f)
        
        self.nextMap    = unpack('5i', f.read(5*4))
        # Camera starting X
        self.xScrStart       = unpack('i', f.read(4))[0]
        self.yScrStart       = unpack('i', f.read(4))[0]
        self.fightMode       = unpack('i', f.read(4))[0]
        self.curMap          = unpack('i', f.read(4))[0]
        
        # screen lock status on/off
        self.ScrLock         = unpack('i', f.read(4))[0]
        self.vsMode          = unpack('i', f.read(4))[0]
        self.yScrCameraLimit = unpack('i', f.read(4))[0]
        self.uScrLimit       = unpack('i', f.read(4))[0]
        
        # no use
        self.noAirStrike     = unpack('i', f.read(4))[0]
        
        self.var4            = unpack('i', f.read(4))[0]
        self.var5            = unpack('i', f.read(4))[0]
        self.var6            = unpack('i', f.read(4))[0]
        self.var7            = unpack('i', f.read(4))[0]
        self.var8            = unpack('i', f.read(4))[0]
        self.var9            = unpack('i', f.read(4))[0]
        self.var10           = unpack('i', f.read(4))[0]
        
        self.str1 = ''
        while(True):
            b = f.read(4)
            x = unpack('i', b)[0]
            if x==0:
                break
            self.str1 += b.decode()
            
        self.str2 = ''
        while(True):
            b = f.read(4)
            x = unpack('i', b)[0]
            if x==0:
                break
            self.str2 += b.decode()
        
        self.str3 = ''
        while(True):
            b = f.read(4)
            x = unpack('i', b)[0]
            if x==0:
                break
            self.str3 += b.decode()

        self.lScrLimit = unpack('i', f.read(4))[0]
        self.rScrLimit = unpack('i', f.read(4))[0]
        self.musicN1   = unpack('i', f.read(4))[0]
        
        # trigger the next music
        self.musicN2   = unpack('i', f.read(4))[0]
        self.anis      = Animations(f) # executes indepedent animations For tiles
        self.facs      = FFacHeads(f)

    def getTileImage(backNo, tileNo):
        tile = OldMap.background[backNo].tiles[tileNo]
        tmxTile = "%d_%d.bmp"%(tile.tileSetNumber, tile.tileNumber)
        return tmxTile

    def toJson(self, baseName):
        map = getObjectProperties(self)
        fp = open(g_outDir+baseName+'_.json', 'w')
        fp.write(json.dumps(map, sort_keys=True, indent=4))
        fp.close()
    
def export(fileName):
    filePath, ext = os.path.splitext(fileName)
    if ext == '':
        fileName += '.dat'
    if not os.path.exists(fileName):
        fileName = g_mapsDir + fileName
        if not os.path.exists(fileName):
            print('%s is not exists in maps directory.'%fileName)
            return

    fp = open(fileName,'rb')
    map = OldMap(fp)
    fp.close()

    filePath, baseName = os.path.split(filePath)
    map.toJson(baseName)
    
if __name__ == '__main__':
    if len(sys.argv)<2:
        print("No file name")
        sys.exit()
    
    export(sys.argv[1])
