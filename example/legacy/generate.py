# generate box and effect from array, bilge theall, all rights giveup
import os, sys
import math
import json
import uuid
import shutil
import time
import re
from struct import unpack

g_outDir = 'scene/effect/'
g_imageDir = g_outDir + 'image'
g_soundDir = g_outDir + 'sound'
g_appRoot = './'
g_stuffPath = g_appRoot+'images/stuff/'

def createDir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

g_effectList = [
	{'index':0, 'name':'Test', 'frames':[[3,1,5],[3,1,5],[3,1,4]]},
    {'index':1, 'name':'Blocking', 'frames':[[3,1,5],[1,1,5],[1,2,4]]},
    #{'index':2, 'name':'Round Introduction', 'frames':[[2,1,100,(30,'ready.mp3'),(0,300)],[2,2,140,(80,'fight.mp3',NoUserInput=0),(0,-600)]]},
	{'index':3, 'name':'ryu blue ball impact', 'frames':[[3,1,10],[3,2,10]]},
    {'index':4, 'name':'explosion 40', 'frames':[[4,1,5],[4,2,5],[4,1,5],[4,2,5]]},
    {'index':5, 'name':'white star hit', 'frames':[[5,1,5],[5,2,5],[5,3,5]]},
	{'index':6,'name':'web shot impact','frames':[[6,1,10],[6,2,10]]},
	{'index':7,'name':'fire ball impact','frames':[[7,1,10],[7,2,10]]},
	{'index':8,'name':'coins','frames':[[8,1,3],[8,2,3],[8,3,3]]},
	{'index':9,'name':'lava rock breaking','frames':[[9,1,10],[9,2,7]]},
	{'index':10,'name':'M vs C hit','frames':[[10,1,3],[10,2,3],[10,3,3]]},
	{'index':11,'name':'vulcano explosion','frames':[[11,1,10],[11,2,25]]},
	{'index':12,'name':'air Trail going up','frames':[[1,2,14,(0,-1)]]},
    {'index':13,'name':'bright dot','frames':[[13,1,7]]},
	{'index':14,'name':'blood','frames':[[14,1,6],[14,2,6]]},
	{'index':15,'name':'Green pick up sign','frames':[[15,1,5],[15,2,5],[15,3,4]]},
	{'index':16,'name':'smoke','frames':[[16,1,5],[16,2,5],[16,3,4]]},
	{'index':17,'name':'red ray impact','frames':[[17,1,1],[17,2,2]]},
	{'index':18,'name':'blueRay','frames':[[18,1,1]]},
	{'index':19,'name':'blueRay 2','frames':[[19,1,1]]},
	{'index':20,'name':'rash hit','frames':[[20,1,5],[20,2,5],[20,3,5]]},
	{'index':21,'name':'blueRay Impact','frames':[[21,1,1]]},
	{'index':22,'name':'blueRay Impact 2','frames':[[22,1,1]]},
	{'index':23,'name':'Power ball','frames':[[23,1,1]]},
	{'index':24,'name':'fire going up','frames':[[24,1,2,(0,-1)],[24,2,3]]},
	{'index':25,'name':'4 way explosion','frames':[[25,1,2],[25,2,3],[25,3,4],[25,4,3]]},
	{'index':26,'name':'batman bomb smoke','frames':[[26,1,4],[26,2,3],[26,3,4],[26,4,3]]},
	{'index':27,'name':'green ray impact','frames':[[27,1,8],[27,2,8]]},
	{'index':28,'name':'little smoke','frames':[[28,1,5],[28,2,5],[28,3,5]]},
	{'index':29,'name':'little smoke going up','frames':[[28,1,5,(0,-1)],[28,2,5],[28,3,5,(0,1)]]},
	{'index':30,'name':'Ray ball impact','frames':[[29,1,5],[29,2,5],[29,3,5]]},
	{'index':31,'name':'electricity','frames':[[30,1,4],[30,2,4],[30,3,4]]},
	{'index':32,'name':'Red Ray','frames':[[31,1,1]]},
	{'index':33,'name':'Red Ray 2','frames':[[32,1,1]]},
	{'index':34,'name':'big rock breaking','frames':[[33,1,6],[33,2,6]]},
	{'index':35,'name':'little rock breaking','frames':[[34,1,6],[34,2,6]]},
    {'index':36,'name':'water splash','frames':[[35,1,5],[35,2,5],[35,3,5],[35,4,5],[35,5,5],[35,6,5]]},
	{'index':37,'name':'big web','frames':[[69,1,5],[69,2,5],[69,3,10]]},
    {'index':38,'name':'yellow ray','frames':[[36,1,1]]},
    {'index':39,'name':'yellow ray 2','frames':[[37,1,1]]},
	{'index':50,'name':'TMNT hit','frames':[[50,1,7],[50,2,8]]},
    {'index':51,'name':'yellow ray','frames':[[38,1,1]]},
    {'index':52,'name':'yellow ray 2','frames':[[39,1,1]]},
    {'index':61,'name':'dragon ball','frames':[[51,1,6],[51,2,6],[51,3,6],[51,4,6],[51,5,6],[51,6,6]]},
    {'index':62,'name':'jimmy_attack_effect','frames':[[52,1,1],[52,2,1],[52,3,1],[52,4,1],[52,5,1],[52,6,1],[52,7,1],[52,8,1],[52,9,1],[52,10,1],[52,11,1],[52,12,1],[52,13,1],[52,14,1],[52,15,1],[52,16,1],[52,17,1],[52,18,1],[52,19,1]]},
    {'index':63,'name':'chunk_type_zhaoyun_knife','frames':[[53,1,1],[53,2,1],[53,3,1],[53,4,1],[53,5,1],[53,6,1]]},
	{'index':64,'name':'chunk_type_dragon_double','frames':[[54,1,5],[54,2,5],[54,3,5],[54,4,5],[54,5,5],[54,6,5],[54,7,5]]},
]

def createJsonHeader(name):
    jsonObj = {}
    jsonObj['type'] = 'character'
    jsonObj['name'] = name
    jsonObj['uuid'] = str(uuid.uuid1())
    jsonObj['update_time'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
    jsonObj['create_time'] = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time()))
    jsonObj['author'] = 'bilge theall'
    jsonObj['version'] = '1.0.0.0'
    jsonObj['comment'] = 'Export by program.All rights not reserved.'
    jsonObj['contact'] = 'wazcd_1608@qq.com'
    return jsonObj

def getImageSize(srcfullPath):
    stream = open(srcfullPath, 'rb')
    data = stream.read(18)
    x, y = unpack('<LL', stream.read(8))
    return x, y

def copyImage(src, dest):
    srcPath, name = os.path.split(src)
    destFullPath = os.path.join(dest, name)
    if not os.path.exists(destFullPath):
        shutil.copy(src, destFullPath)
    x, y = getImageSize(src)
    return 'image/' + name, x, y

def saveJson(j, fileName):
    basename, ext = os.path.splitext(fileName)
    if ext.lower()!='json':
        fileName = basename + '.json'
    fp = open(fileName,'w')
    fp.write(json.dumps(j, indent=4))
    fp.close()

def getImageFileName(main,sub):
    return g_stuffPath+'pt%d_a%d.bmp'%(main,sub)

def export(file):
    fileName = file['name']
    frames = []
    for f in file['frames']:
        frame = {}
        imageName,width,height = copyImage(getImageFileName(f[0], f[1]), g_imageDir)
        frame['anchor'] = [float(width)/2,height]
        frame['image'] = imageName
        frame['duration'] = f[2]
        if len(f) >= 4:
            frame['vector'] = list(f[3])
        frames.append(frame)
    character = createJsonHeader(fileName)
    character['icon'] = frames[0]['image']
    action = {}
    action['frames'] = frames
    action['triggers'] = [{'event': 'None', 'value': ''}]
    character['actions'] = [action]
    saveJson(character, g_outDir+fileName)

def generate():
    createDir(g_outDir)
    createDir(g_imageDir)
    createDir(g_soundDir)
    for effect in g_effectList:
        export(effect)

if __name__ == '__main__':
    generate()
